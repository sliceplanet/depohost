<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "АДата центр (ЦОД). ArtexTelecom – предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("title", "Услуги дата центра в Москве | Услуги ЦОД по хранению данных, цены, тарифы");
$APPLICATION->SetTitle("Дата центр (ЦОД)");
?>
<div class="container">
  

<p>В бизнесе особое внимание уделяется конфиденциальности и сохранности корпоративных сведений. Компания ArtexTelecom предлагает <a href="/">услуги дата-центра</a> по всей России. В нашем <a href="/">ЦОД</a> мы размещаем, храним и обрабатываем данные любого формата, гарантируем тщательную защиту информации, доверенной нашими клиентами.</p>

<p align="center"><img title="Дата-центр (изображение 1)" src="/images/pages/data-center/1.jpg" alt="Дата центр" width=""></p>

<h2>Преимущества аренды нашего дата-центра</h2>

<ul class="data_center">
  <li>Экономия. Нет необходимости покупать и обслуживать дорогостоящее оборудование.</li>
  <li>Удобство. Обновление ПО и техническая поддержка происходят своевременно. </li>
  <li>Безопасность. Доступ к данным ограничен – для получения информации необходимо преодолеть несколько уровней защиты. А эффективные антивирусные программы защищают от вредоносных ПО и хакерских атак. </li>
  <li>Сохранность. В случае удаления важных сведений с серверов мы сможем восстановить их благодаря регулярному резервному копированию. </li>
  <li>Бесперебойность. Все оборудование подключено к резервным источникам питания и высокоскоростному интернету.</li>
  <li>Разделение. Права доступа к определенным видам информации можно настроить в соответствии с должностью сотрудника или конкретным отделом. </li>
  <li>Надежность. Ресурсоемкое оборудование и надежные жесткие накопители не подведут в самый неподходящий момент.</li>
</ul>

<p align="center"><img src="/images/pages/data-center/2.jpg" title="Дата-центр (изображение 2)" alt="Дата центр" width=""></p>

<h2>3 причины для сотрудничества с ArtexTelecom</h2>

<div class="advantage-img-wrap">
  <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item" style="width: 100%;">
      <div class="advantage__item_icon calc"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Мы предоставляем только надежные серверы, которые имеют процессоры с высокой производительностью, и обеспечивают бесперебойную работу сайтов независимо от системы управления.
        </div>
      </div>
    </li>

    <li class="advantage__item" style="width: 100%;">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">Благодаря высокой мощности серверы подходят для оптимизации сайтов под устройства с различным разрешением экранов – смартфоны, планшеты, ноутбуки и компьютеры.
        </div>
      </div>
    </li>

    <li class="advantage__item" style="width: 100%;">
      <div class="advantage__item_icon day"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Наши серверы подходят для всех известных CMS. Они поддерживают как популярные MODx, 1С-Битрикс, WordPress и Joomla, так и менее распространенные Net.Cat, Opencart, CS-Cart и проч. 
        </div>
      </div>
    </li>
  </ul>
</div>  

</div>
<div class="band-blue wrapper wide visible-lg" style="margin-top: 0px !important;"></div>
<section class="section callback_bottom">
  <div class="container">
    <div class="subtitle subtitle-custom text-center">
      Остались вопросы? Оставьте номер телефона и мы подробно на них ответим
    </div>
    <div class="row">
      <?
      $APPLICATION->IncludeComponent(
              "bitrix:form.result.new",
              "new_any-quest-form",
              Array(
                  "SEF_MODE" => "N",
                  "WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
                  "LIST_URL" => "result_list.php",
                  "EDIT_URL" => "result_edit.php",
                  "SUCCESS_URL" => "",
                  "CHAIN_ITEM_TEXT" => "",
                  "CHAIN_ITEM_LINK" => "",
                  "IGNORE_CUSTOM_TEMPLATE" => "Y",
                  "USE_EXTENDED_ERRORS" => "Y",
                  "CACHE_TYPE" => "A",
                  "CACHE_TIME" => "3600",
                  "AJAX_MODE" => "Y",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "N",
                  "AJAX_OPTION_HISTORY" => "N",
                  "SEF_FOLDER" => "/",
                  "VARIABLE_ALIASES" => Array(
                  )
              )
          );?>
    </div>
  </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>