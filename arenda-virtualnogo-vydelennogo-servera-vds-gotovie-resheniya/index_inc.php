<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<section class="page-section section-graybg choose-vvds-section">
	
		<div class="page-section__h">Выберите готовое решение</div>
		<p class="page-section__desc">Готовые решения будут вам предоставлены в течении 15 минут после оплаты.</p>
	
		<div class="container">
			<? $APPLICATION->IncludeComponent(
			  "bitrix:catalog.section.list",
			  "vds-turnkey2020",
			  array(
				"ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
				"CACHE_GROUPS" => "Y",  // Учитывать права доступа
				"CACHE_TIME" => "36000000",  // Время кеширования (сек.)
				"CACHE_TYPE" => "A",  // Тип кеширования
				"COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
				"IBLOCK_ID" => "51",  // Инфоблок
				"IBLOCK_TYPE" => "xmlcatalog",  // Тип инфоблока
				"SECTION_CODE" => "",  // Код раздела
				"SECTION_FIELDS" => "",  // Поля разделов
				"SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
				"SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => "",  // Свойства разделов
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "2",  // Максимальная отображаемая глубина разделов
				"VIEW_MODE" => "LINE"
			  )
			); ?>
			
			<p class="after-solutions-text">
Компания «ArtexTelecom» предоставляет в аренду виртуальные выделенные сервера VPS/VDS (от англ. «Virtual Private/Dedicated Server») по всей России. Эта услуга пользуется сегодня огромной популярностью у множества IT-организаций, владельцев веб-сайтов и сетевых проектов. Для пользователей, арендовать VDS - означает получить услуги удаленного сервера, но при этом, существенно сэкономить на приобретении и обслуживании собственного оборудования.			
			</p>
			
			
		</div>
</div>
<section class="page-section how-ssl-works-section vvds-osob">
	
	<div class="container">
	
		<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__h">Основные особенности</div>
				<div class="hsw-block__content">
					<p>Суть технологии VPS заключается в том, что физический сервер делится на несколько виртуальных, которые впоследствии и предоставляются клиентам. На практике управление виртуальным сервером осуществляется точно так же, как и физическим, пользователь получает:
					</p>
					
					<ul class="ul50p fw500">
						<li>Гарантированные серверные ресурсы, соответствующие выбранной конфигурации</li>
						<li>Собственный сетевой IP-адрес, высокоскоростной доступ к Интернету</li>
						<li>Полный root-доступ с возможностью установки и использования любой ОС и программного обеспечения</li>
						<li>Полная изоляция от соседей по серверному оборудованию</li>
						<li>Стабильное бесперебойное питание, профессиональное обслуживание и техподдержка	</li>
					</ul>
					
					<p>При этом становится возможным в любое время при необходимости увеличить или уменьшить производительность и другие ресурсы, выбирать наиболее подходящие в текущий момент параметры и не переплачивать за излишние.</p>
					
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Ключевые преимущества VPS/VDS серверов</div>
				<div class="hsw-block__content">
				
					<p>Если сравнивать виртуальный сервер с физическим, то можно выделить следующий ряд достоинств:</p>

					<ul class="ul50p fw500">
						<li>Повышенная отказоустойчивость</li>
						<li>Возможность быстрого изменения конфигурации</li>
						<li>Простота создания бэкапов</li>
						<li>Дешевизна обслуживания</li>
						<li>Возможность переноса на другое оборудование</li>
					</ul>
					
					</div>
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h">Возможности</div>
				<div class="hsw-block__content">
					<p>Сервер VPS функционирует практически как полноценный физический сервер, на котором клиент-администратор может произвести установку Windows, Linux или любой другой ОС, установку необходимого для работы софта, управлять групповыми политиками и доступом, создавать, изменять, удалять файлы и папки, в общем выполнять все те действия, что и на обычном компьютере. При этом в любое время и в минимальные сроки можно увеличить объемы жестких дисков и оперативной памяти, изменить частоту процессора. Все эти действия выполняются программным путем, абсолютно не затрагивая имеющиеся данные. Также стоит отметить, что виртуальный VPS / VDS-хостинг – это полностью изолированная среда, клиент может быть уверен в том, что получит гарантированные серверные ресурсы, на которые не будут влиять соседние проекты, размещенные на том же физическом оборудовании.</p>
				
				</div>
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h">Обращайтесь к нам</div>
				<div class="hsw-block__content">
				
					<p>Есть несколько причин, почему вам стоит арендовать виртуальный сервер VPS/VDS у нас:</p>

					<ul class="ul50p fw500">
						<li>Собственный современный дата-центр в Москве</li>
						<li>Новейшее оборудование с соответствующим уровнем надежности</li>
						<li>Круглосуточное наблюдение за состоянием оборудования</li>
						<li>Надежная защита данных от несанкционированного доступа и взлома</li>
						<li>Квалифицированная техническая поддержка</li>
						<li>Безлимитный трафик</li>
						<li>Широкий спектр предоставляемых услуг</li>
						<li>Оперативное предоставление доступа</li>
						<li>Удобные способы оплаты</li>
					</ul>
					
				</div>
			</div>
			
		</div>
		
	</div>
		
</section>

<section class="page-section services-for-you-section dark-blue-bg">
	<div class="container">
		<div class="page-section__h">Услуги для вас</div>
		<div class="sfy-block">
			<div class="row">
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon1.png"></div>
						<a href="/arenda-dedicated_server/">Аренда<br>выделенного<br>сервера</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon2.png"></div>
						<a href="/arenda-vydelennogo-servera-na-windows/">Выделенный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon3.png"></div>
						<a href="/arenda-virtualnogo-vydelennogo-servera-na-windows/">Виртуальный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-4.png"></div>
						<a href="/domain/">Регистрация<br>доменов</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?global $relatedNews; 
$relatedNews = [ "ID" => [13082, 13084, 13288, 13286] ];?>
<div class="articles-block main-new__wrap">
  <div class="container">
    <div class="main-new__title">
      Статьи по теме:
    </div>
    <? $APPLICATION->IncludeComponent(
      "bitrix:news.list",
      "related-articles",
      Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_URL" => "/news/#ELEMENT_CODE#/",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("", ""),
        "FILTER_NAME" => "relatedNews",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "4",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
      ),
      $component
    ); ?>
  </div>
</div>

<section class="page-section custom-service-form-section">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service2020",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального VDS сервера'
      )
    ); ?>
  </div>
</section>
