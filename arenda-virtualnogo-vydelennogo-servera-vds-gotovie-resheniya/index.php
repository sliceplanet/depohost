<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "У нас вы можете взять в аренду подготовленные готовые решения выделенных виртуальных серверов на выгодных условиях: мощное и производительное оборудование, профессиональная техподдержка, возможность установки любых ОС Windows, Linux, гарантия безопасности и конфиденциальности данных");
$APPLICATION->SetPageProperty("keywords_inner", "VDS");
$APPLICATION->SetPageProperty("title", "    Аренда готовых решений виртуальных серверов в Москве | подобрать готовое решение виртуального сервера (ВДС) по недорогой цене");
$APPLICATION->SetTitle("Аренда готовых решений виртуальных серверов");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/sprites/vds.css');
?>

<div id="virtual-vds-page">
	
	<section class="page-section whyssl-section why-vvds">
		
		<div class="page-section__icon">
			<img src="/images/services/2.svg">
		</div>
		<div class="page-section__h">Готовые решения виртуального сервера</div>
		
		<div class="container">
			
			<div class="wug__wrapper">
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds1.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Преимущества аренды VDS</div>
						<p>Виртуальные выделенные сервера (VPS) – один из наиболее выгодных вариантов размещения веб-ресурса.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds2.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Бесплатная настройка<br>и консультации</div>
						<p>Каждый виртуальный выделенный сервер уже настроен, панель ISPmanager в подарок бесплатно!</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds3.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Высокопроизводительные СХД</div>
						<p>Для виртуализации и облачных решений мы используем высокопроизводительные СХД, HP, Infortrend</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds4.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Дополнительно 15 дней бесплатно</div>
						<p> 15 дней бесплатно после оплаты к выбранному виртуальному серверу<br><a href="#">Подробнее</a></p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds5.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Аппаратная виртуализация KVM<br>и HUPER-V</div>
						<p>Аппаратная виртуализация VDS сервера гарантирует честное распределение ресурсов</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds6.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Большой выбор OS Windows и Linux</div>
						<p>Взяв в аренду сервер мы можем установить Вам любую операционную систему </p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds7.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Круглосуточная поддержка 24/7</div>
						<p>Пользуясь нашими услугами вы всегда можете рассчитывать на круглосуточную техническую поддержку</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds8.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Вы приобретаете услуги<br>из первых рук</div>
						<p>Важно знать! Мы как российский оператор предоставляем услуги из первых рук, мы отвечаем за свое качество!</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/wug/icon_9.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Снижение ваших затрат</div>
						<p>Аренда сервера это экономия ваших средств не только на покупке, обслуживании и администрировании сервера</p>
					</div>
				</div>				
			</div>
			
		</div>
		
	</section>	
	
</div>




<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>