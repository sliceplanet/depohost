<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контакты Депо Телеком, адрес офиса");
$APPLICATION->SetPageProperty("keywords", "Контакты ООО Депо Телеком");
$APPLICATION->SetPageProperty("title", "Контакты Депо Телеком адреса и телефоны");
$APPLICATION->SetTitle("Контакты Депо Телеком");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/contacts.css');
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/contacts-media.css');
?>


<section class="page-section how-ssl-works-section dns-hosting-text" itemscope="" itemtype="http://schema.org/Organization">
	
	<div class="container">
		<h1 class="page-section__h">Контакты</h1>
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__h">Центральный офис обслуживания:</div>
				<div class="hsw-block__content">
					  <p class="contacts__item-desc" itemprop="address"
						   itemtype="http://schema.org/PostalAddress">г. Москва, ул. Искры, д. 31, корп 1, офис 709 В
					  </p>
					  <p>
						Время обслуживание клиентов по будням с 09-00 до 18-00
					  </p>
					  <p>
						Тел. / Факс <span itemprop="telephone">+7 (495) 797-8-500</span> (многоканальный)
					  </p>
					  <p>
						Тел. <span itemprop="telephone">8 800 700-4036 </span>
					  </p>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Адрес для доставки писем:</div>
				<div class="hsw-block__content">
					<p class="contacts__item-desc"> 129344, г. Москва, ул. Искры, д. 31, корп 1, офис 709 В</p>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Отдел обслуживания клиентов:</div>
				<div class="hsw-block__content">
				  <p>Тел. +7 (495) 797-8-500 (многоканальный)</p>

				  <p>Тел. 8 800 700-4036</p>

				  <p>E-mail: <a href="mailto:info@depohost.ru" itemprop="email">info@depohost.ru</a></p>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Отдел технической поддержки клиентов работает круглосуточно:</div>
				<div class="hsw-block__content">
					  <p>Тел. +7 (495) 797-8-500 (многоканальный)</p>

					  <p>Тел. 8 800 700-4036</p>

					  <p class="contacts__item-link">E-mail: <a href="mailto:support@depohost.ru">support@depohost.ru</a></p>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__content">
				
						<?/*
					  <p><img class="contacts-img" src="/upload/medialibrary/33b/sxema_proezda.jpg" title="Схема проезда"
							  alt="Схема проезда" width="710"
							  height="369">

					  </p>*/?>
					  <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Af2307d982cc4073b4a0a6d2913f24328aa7a0b2b8db0f06789b69ba177e8c994&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
				</div>
				
			</div>
			<div class="hsw-block">
				<h2 class="hsw-block__h">Обратная связь</h2>
				<div class="hsw-block__content">
				  <p>Самый быстрый и гарантированный способ доставки электронного сообщения это отправить электронное письмо,
					Ваше
					сообщение будет гарантированно доставленно нам в Техническую службу поддержки клиентов, которая работает
					круглосуточно. </p>

				  <p>Если Ваше сообщение не связано с обращением в Техническую службу, не останавливайтесь, пишите, Ваше
					сообщение
					гарантированно переадресуют нужному специалисту и Вы получите свой ответ. </p>
				</div>
				
			</div>

    </div>
  </div>
</section>

<section class="page-section section-graybg">

          <? $APPLICATION->IncludeComponent(
            "webs:main.feedback",
            "depohost",
            array(
              "USE_CAPTCHA" => "Y",
              "OK_TEXT" => "Спасибо, ваше сообщение принято.",
              "EMAIL_TO" => "info@depohost.ru",
              "REQUIRED_FIELDS" => array(
                0 => "NAME",
                1 => "EMAIL",
                3 => "SUBJECT",
                4 => "MESSAGE",
              ),
              "EVENT_MESSAGE_ID" => "",
              "AJAX_MODE" => 'Y'
            ),
            false
          ); ?>
  
</section>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "http://<?=$_SERVER['SERVER_NAME']?>",
  "name": "Artextelecom",
  "availableLanguage": "Russian",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+7-495-797-8-500",
    "contactType": "Customer service"
  }
}
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>