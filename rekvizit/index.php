<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Реквизиты компании ООО «Депо Телеком»");
$APPLICATION->SetPageProperty("keywords", "Реквизиты компании ООО «Депо Телеком»");
$APPLICATION->SetPageProperty("title", "Реквизиты компании ООО «Депо Телеком»");
$APPLICATION->SetTitle("Реквизиты");
?>

<section class="page-section how-ssl-works-section dns-hosting-text">
	
	<div class="container">
	
		<h1 class="page-section__h"><? $APPLICATION->ShowTitle(false) ?></h1>
	     
	
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<h3 class="hsw-block__h">Реквизиты компании ООО «Депо Телеком»</h3>
				<div class="hsw-block__content">
					<p>
						<div>
				 <b>ИНН:</b> 7716588900
						</div>
						<div>
				 <b>КПП:</b> 771601001
						</div>
						<div>
				 <b>Юридический адрес:</b> 129344 г.Москва, ул. Искры д.31, корп. 1
						</div>
						<div>
				 <b>Почтовый адрес:</b> 129344 г.Москва, ул. Искры д.31, корп. 1
						</div>
						<div>
				 <b>Тел/факс:</b> +7 (495) 797-8-500
						</div>
						<div>
				 <b>ОКВЭД:</b> 61.10
						</div>
						<div>
				 <b>ОКПО:</b> 83152374
						</div>
						<div>
				 <b>ОГРН:</b> 1077761511794
						</div>
						<div>
				 <b>Банк:</b> АО "ОТП Банк" «Отделение Марксистская, 3» г.Москва
						</div>
						<div>
				 <b>Р/с:</b> 40702810900510000033
						</div>
						<div>
				 <b>БИК:</b> 044525311
						</div>
						<div>
				 <b>К/с:</b> 30101810000000000311
						</div>
						</p>
				</div>
				
			</div>
			

			<div class="hsw-block">
				<h2 class="hsw-block__h">Договора для физических и юридических лиц:</h2>
				<div class="hsw-block__content">
				
					<p>
						 Если вы желаете стать нашим клиентом вам необходимо ознакомиться с формами наших договоров на основании которых вы можете воспользоваться нашими услугами.
					</p>
					<p>
							<div>
					 <b>Для физических лиц</b> - <a href="/doc/Dogovor_oferta.pdf" target="_blank">Публичная оферта</a>
							</div>
							<div>
					 <b>Для юридических лиц</b> - <a href="/doc/server_ur.pdf" target="_blank">Двухсторонний договор</a>
							</div>
							<div>
					 <b>Правила предоставления услуг</b> - <a href="/doc/pravila.pdf" target="_blank">Скачать правила</a>
							</div>
							<div>
					 <b>Что такое Публичная оферта</b> - <a href="/doc/that_oferta.pdf" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Образец соглашения SLA</b> - <a href="/doc/Depo_Telecom_SLA.pdf" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b> Шаблон письма на перезачет средств</b> - <a href="/doc/Письмо_на_перезачет_средств_в_пользу_другого_плательщика.doc" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Шаблон письма на расторжение договора </b> - <a href="/doc/Шаблон_письма_на_расторжение_договора.doc" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Что такое персональные данные</b> - <a href="/fz152.php" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Федеральный закон 152 о Персональных данных</b> - <a href="/doc/fz152.pdf" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Политика конфиденциальности</b> - <a href="/rekvizit/private-policy/" target="_blank">Ссылка</a>
							</div>
							<div>
					 <b>Правила предоставления тестового доступа к серверу VDS</b> - <a href="/rules-test-vds.php" target="_blank">Ссылка</a>
							</div>
							
					</p>
				</div>
			</div>
			
	</div>
	
	</div>
	
</section>
	
			
			
			
			

<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>