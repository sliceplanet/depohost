<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Yandex Market");
//global $arrFilter; $arrFilter = array("!PROPERTY_WF_SALE" => false);
?> 
<?$APPLICATION->IncludeComponent(
	"webfly:yandex.market", 
	".default", 
	array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID_IN" => array(
			0 => "21",
			1 => "30",
			2 => "51",
		),
		"IBLOCK_ID_EX" => array(
			0 => "0",
		),
		"IBLOCK_SECTION" => array(
			0 => "0",
		),
		"SITE" => "www.depohost.ru",
		"COMPANY" => "ООО «Депо Телеком»",
		"FILTER_NAME" => "arrFilter",
		"MORE_PHOTO" => "wf_fields",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"CACHE_FILTER" => "Y",
		"PRICE_CODE" => array(
			0 => "Розничная",
		),
		"IBLOCK_ORDER" => "N",
		"CURRENCY" => "RUR",
		"LOCAL_DELIVERY_COST" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"AGENT_CHECK" => "N",
		"IBLOCK_TYPE_LIST" => array(
			0 => "xmlcatalog",
		),
		"SAVE_IN_FILE" => "N",
		"IBLOCK_CATALOG" => "Y",
		"DO_NOT_INCLUDE_SUBSECTIONS" => "N",
		"IBLOCK_AS_CATEGORY" => "Y",
		"BIG_CATALOG_PROP" => "",
		"HTTPS_CHECK" => "Y",
		"CACHE_NON_MANAGED" => "N",
		"SKU_NAME" => "PRODUCT_AND_SKU_NAME",
		"SKU_PROPERTY" => "",
		"SALES_NOTES" => "0",
		"SALES_NOTES_TEXT" => "",
		"OLD_PRICE" => "N",
		"PRICE_ROUND" => "Y",
		"AVAILABLE_ALGORITHM" => "BITRIX_ALGORITHM",
		"CURRENCIES_CONVERT" => "NOT_CONVERT",
		"LOCAL_DELIVERY_COST_OFFER" => "0",
		"STORE_OFFER" => "0",
		"STORE_PICKUP" => "0",
		"NAME_PROP" => "0",
		"NAME_CUT" => "",
		"PROPDUCT_PROP" => array(
		),
		"OFFER_PROP" => array(
		),
		"PREFIX_PROP" => "0",
		"DESCRIPTION" => "0",
		"DETAIL_TEXT_PRIORITET" => "N",
		"NO_DESCRIPTION" => "N",
		"PARAMS" => array(
			0 => "ADVANCE",
			1 => "CONFIGURATION",
			2 => "CPU",
			3 => "GENERATION",
			4 => "RAID",
			5 => "RAM",
		),
		"COND_PARAMS" => array(
		),
		"DISCOUNTS" => "PRICE_ONLY",
		"UTM_CHECK" => "Y",
		"UTM_SOURCE" => "YandexMarket",
		"UTM_CAMPAIGN" => "",
		"UTM_MEDIUM" => "cpc",
		"UTM_TERM" => "",
		"MARKET_CATEGORY_PROP" => "",
		"DEVELOPER" => "",
		"VENDOR_CODE" => "",
		"COUNTRY" => "Россия",
		"MANUFACTURER_WARRANTY" => ""
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
