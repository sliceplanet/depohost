<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Способы оплаты Депо Телеком");
$APPLICATION->SetPageProperty("keywords", "Способы оплаты Депо Телеком");
$APPLICATION->SetPageProperty("title", "Способы оплаты Депо Телеком | ArtexTelecom");
$APPLICATION->SetTitle("Способы оплаты Депо Телеком");
?>

<div id="payment-methods-page">

	<div class="page-top-desc">
	<div class="page-top-desc__wrapper">
	
		<h1>Способы оплаты</h1>
		<div class="ptd-desc">Вы можете оплачивать наши услуги наиболее<br>удобным для Вас способом</div>
		<div class="ptd-content">
			<p>Организации и индивидуальные предприниматели как правило предпочитают использовать форму безналичного расчета, с заключением договора и выставлением счета. Предоставляются все необходимые бухгалтерские документы. Частные лица могут оплатить услуги с помощью банковского перевода, с помощью пластиковой карты или с помощью системы Webmoney и других электронных платежных систем.</p>
			
		</div>
	</div>
	</div>
	
	<div class="payment-methods-desc">
		<div class="container">
	
			<div class="row jcsb">
			
				<div class="col-md-4">
					<div class="payment-methods-desc__item">
						<div class="pmd__item_icon">
							<img src="/images/2020/1.png" />
						</div>
						<div class="pmd__item_name">По квитанции «Сбербанка»<br>Форма ПД-4</div>
						<div class="pmd__item_desc">После выбора необходимой Вам услуги получите возможность распечатать бланк квитанции с нашими реквизитами и суммой платежа. С помощью этой квитанции вы сможете оплатить наши услуги в ближайшем отделении «Сбербанка». Кроме того, мы участвуем в бонусной системе «Сбербанка» и принимаем в счет оплаты бонусы «Спасибо», если они у Вас есть. Напомним, что оплачивая товары с помощью карты VISA «Сбербанка», Вы накапливаете бонусные «Спасибо», которые затем можно потратить на товары или услуги организаций, участвующих в системе.</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="payment-methods-desc__item">
						<div class="pmd__item_icon">
							<img src="/images/2020/2.png" />
						</div>
						<div class="pmd__item_name">Электронные деньги<br>Webmoney</div>
						<div class="pmd__item_desc">Для того, чтобы оплатить наши услуги в виртуальной валюте, сообщите нам о том, что Вы хотите сделать это, и после оформления заказа мы выставим Вам счет, оплатив который, Вы получите доступ к той или иной услуге. Для этого нужно будет перейти в Ваш личный кабинет. Принимается также оплата по Webmoney картам.</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="payment-methods-desc__item">
						<div class="pmd__item_icon">
							<img src="/images/2020/3.png" />
						</div>
						<div class="pmd__item_name">Пластиковые<br>карты</div>
						<div class="pmd__item_desc">Пожалуй, один из самых удобных и оперативных способов оплаты. Мы принимаем к оплате пластиковые карты VISA\MasterCard, выпущенные любым банком, а также можем принять платеж, осуществленный с помощью интернет-банкинга Альфа Банк или Банка Русский Стандарт.</div>
					</div>
				</div>
			
			</div>
	
		</div>
	</div>


	<div class="section-graybg">
	
		<section class="page-section payment-logo-section">
		
			<div class="container">
			
				<div class="page-section__h">Все перечисленные способы оплаты<br>доступны личном кабинете</div>
				
				<div class="payment-logo-items">
					<div class="flex-row jcsb fww">
						<div class="payment-logo-item">
							<img src="/images/2020/payment/1.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/2.png" />
						</div>							
						<div class="payment-logo-item">
							<img src="/images/2020/payment/3.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/4.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/5.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/6.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/7.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/8.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/9.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/10.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/11.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/12.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/13.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/14.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/16.png" />
						</div>
						<div class="payment-logo-item">
							<img src="/images/2020/payment/15.png" />
							<div class="payment-name">Безналичный<br>платеж</div>
						</div>
						
						
					</div>
				</div>
			
			</div>
		
		</div>
		
	
	</div>


</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>