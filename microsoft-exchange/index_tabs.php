<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<div class="advantage-img-wrap">
  <h2 class="subtitle">Возможности Microsoft Exchange</h2>
  <ul class="row advantage">
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon gb-10"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Размер почтового ящика 10 GB</div>
        <div class="advantage__item_description">Каждый почтовый ящик имеет размер 10GB и это не предел он может быть
          увеличен по вашему запросу
        </div>
      </div>
    </li>
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon planet-refresh"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Возможности Microsoft Exchange</div>
        <div class="advantage__item_description">Общие ресурсы: календарь, адресная книга, общие папки, возможность
          планировать, назначать встречи, совещания и многое другое.
        </div>
      </div>
    </li>
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon trash"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная защита от спама</div>
        <div class="advantage__item_description">Каждый почтовый ящик и домен надежно защищен от спама</div>
      </div>
    </li>
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon shield-big"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная Антивирусная защита</div>
        <div class="advantage__item_description">Каждый почтовый ящик находится под надежной защитой антивирусного ПО
        </div>
      </div>
    </li>
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon page"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Удобство и доступность</div>
        <div class="advantage__item_description">Использование Microsoft Exchange дает огромные преимущества и большие
          возможности корпоративной почтовой системы.
        </div>
      </div>
    </li>
    <li class="col-sm-6 advantage__item">
      <div class="advantage__item_icon calc"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Снижение затрат</div>
        <div class="advantage__item_description">Арендуя почтовый сервер MS Exchange вы экономите и на обслуживании
          почтовой системы
        </div>
      </div>
    </li>
  </ul>
</div>