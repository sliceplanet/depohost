<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>

<?/*
<div class="advantage-img-wrap">
  <h2 class="subtitle">Возможности ISP панели</h2>
  <ul class="advantage">
    <li class="advantage__item">
      <div class="advantage__item_icon page"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Удобство использования</div>
        <div class="advantage__item_description"> Защищенный доступ по протоколу HTTPS, поддержка всех браузеров,
          дружелюбный интерфейс
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Преимущество</div>
        <div class="advantage__item_description">Интуитивно понятный интерфейс, простота использования, облегчает
          администрирование при минимальных знаниях
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon star"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Возможности</div>
        <div class="advantage__item_description">Настройка Веб сервера и пользователей, резервное копирование, установка
          дополнительных компонентов мониторинг нагрузки и многое другое
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon operator"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная поддержка и консультация</div>
        <div class="advantage__item_description">Оказываем бесплатную поддержку и консультируем наших клиентов как
          пользоваться данным продуктом
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon os"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Кросплатформенность</div>
        <div class="advantage__item_description"> Панель может быть установлена на любую OS Windows или Linux.
          Рекомендуем использовать CentOS или Debian
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная установка и настройка</div>
        <div class="advantage__item_description">Все панели управления устанавливаем и настраиваем бесплатно</div>
      </div>
    </li>
  </ul>
</div>

*/?>