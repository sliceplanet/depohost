<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Панель управления ISPmanager, ISPmanager, DNSmanager, VMmanager, BILLmanager, DCImanager 5, панель управления сервером, панель управления веб сервером, панель управления web сервером");
$APPLICATION->SetPageProperty("description", "Вас интересует Аренда или покупка панелей управления веб-серверами от ArtexTelecom? Предоставляем услуги Web хостинга с 2007 года.");
$APPLICATION->SetPageProperty("title", "Арендовать панель управления веб-серверами от ArtexTelecom");
$APPLICATION->SetTitle("Панели управления ISPmanager");
?>
<div id="ispmanager-page">

<div class="page-top-desc">
	<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/isp-icon.png">
			</div>
			<h1>Панели управления ISPmanager</h1>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>			
			<div class="ptd-content">
				<div class="ptd-content__left">
					<p>Данный вид хостинга подразумевает предоставление клиенту отдельного полноценного физического веб сервера (Dedicated Server) в дата-центре. Это оптимальное решение для размещения крупных и стремительно развивающихся проектов, для которых
не будет достаточно ресурсов виртуального сервера, а также в тех случаях, если предъявляются серьезные требования к безопасности, бесперебойности и производительности работы серверного оборудования.</p>
				</div>
				<div class="ptd-content__right df aic">
					<div>
						<img src="/images/2020/ispmanager1.png">
					</div>
				</div>
			</div>
		</div>	
		
	</div>
	
	
	<div class="section-graybg">
		<div class="container">
		<h2 class="page-section__h">Наши тарифы</h2>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog", 
			"ispmanager2020", 
			array(
				"IBLOCK_TYPE" => "xmlcatalog",
				"IBLOCK_ID" => "29",
				"HIDE_NOT_AVAILABLE" => "N",
				"BASKET_URL" => "/personal/cart/",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"SEF_MODE" => "Y",
				"SEF_FOLDER" => "/ispmanager/",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"SET_TITLE" => "Y",
				"SET_STATUS_404" => "Y",
				"USE_ELEMENT_COUNTER" => "Y",
				"USE_FILTER" => "N",
				"FILTER_VIEW_MODE" => "HORIZONTAL",
				"USE_REVIEW" => "N",
				"USE_COMPARE" => "Y",
				"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
				"COMPARE_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"COMPARE_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"COMPARE_ELEMENT_SORT_FIELD" => "sort",
				"COMPARE_ELEMENT_SORT_ORDER" => "asc",
				"DISPLAY_ELEMENT_SELECT_BOX" => "N",
				"PRICE_CODE" => array(
					0 => "Розничная",
				),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"PRICE_VAT_SHOW_VALUE" => "N",
				"PRODUCT_PROPERTIES" => array(
				),
				"USE_PRODUCT_QUANTITY" => "Y",
				"CONVERT_CURRENCY" => "Y",
				"SHOW_TOP_ELEMENTS" => "Y",
				"TOP_ELEMENT_COUNT" => "30",
				"TOP_LINE_ELEMENT_COUNT" => "1",
				"TOP_ELEMENT_SORT_FIELD" => "id",
				"TOP_ELEMENT_SORT_ORDER" => "desc",
				"TOP_ELEMENT_SORT_FIELD2" => "id",
				"TOP_ELEMENT_SORT_ORDER2" => "desc",
				"TOP_PROPERTY_CODE" => array(
					0 => "",
					1 => "CMN_1",
					2 => "CMN_2",
					3 => "CMN_3",
					4 => "CMN_4",
					5 => "",
				),
				"SECTION_COUNT_ELEMENTS" => "Y",
				"SECTION_TOP_DEPTH" => "2",
				"SECTIONS_VIEW_MODE" => "TEXT",
				"SECTIONS_SHOW_PARENT_NAME" => "Y",
				"PAGE_ELEMENT_COUNT" => "30",
				"LINE_ELEMENT_COUNT" => "1",
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER2" => "desc",
				"LIST_PROPERTY_CODE" => array(
					0 => "",
					1 => "CMN_1",
					2 => "CMN_2",
					3 => "CMN_3",
					4 => "CMN_4",
					5 => "CMN_5",
					6 => "",
				),
				"INCLUDE_SUBSECTIONS" => "Y",
				"LIST_META_KEYWORDS" => "-",
				"LIST_META_DESCRIPTION" => "UF_SEO_DESCRIPTION",
				"LIST_BROWSER_TITLE" => "-",
				"DETAIL_PROPERTY_CODE" => array(
					0 => "",
					1 => "CML2_ARTICLE",
					2 => "CML2_BASE_UNIT",
					3 => "CML2_TRAITS",
					4 => "CML2_ATTRIBUTES",
					5 => "CML2_BAR_CODE",
					6 => "",
				),
				"DETAIL_META_KEYWORDS" => "-",
				"DETAIL_META_DESCRIPTION" => "-",
				"DETAIL_BROWSER_TITLE" => "-",
				"LINK_IBLOCK_TYPE" => "",
				"LINK_IBLOCK_ID" => "",
				"LINK_PROPERTY_SID" => "",
				"LINK_ELEMENTS_URL" => "",
				"USE_ALSO_BUY" => "Y",
				"ALSO_BUY_ELEMENT_COUNT" => "5",
				"ALSO_BUY_MIN_BUYES" => "2",
				"USE_STORE" => "N",
				"PAGER_TEMPLATE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"ADD_PICT_PROP" => "-",
				"LABEL_PROP" => "-",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_OLD_PRICE" => "N",
				"DETAIL_SHOW_MAX_QUANTITY" => "N",
				"MESS_BTN_BUY" => "Заказать",
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",
				"MESS_BTN_COMPARE" => "Сравнение",
				"MESS_BTN_DETAIL" => "Подробнее",
				"MESS_NOT_AVAILABLE" => "Нет в наличии",
				"DETAIL_USE_VOTE_RATING" => "N",
				"DETAIL_USE_COMMENTS" => "N",
				"DETAIL_BRAND_USE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"ADD_SECTIONS_CHAIN" => "Y",
				"ADD_ELEMENT_CHAIN" => "N",
				"CURRENCY_ID" => "RUB",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
				"SEF_URL_TEMPLATES" => array(
					"sections" => "",
					"section" => "#SECTION_CODE#/",
					"element" => "#SECTION_CODE#/#ELEMENT_ID#/",
					"compare" => "compare.php?action=#ACTION_CODE#",
				),
				"VARIABLE_ALIASES" => array(
					"compare" => array(
						"ACTION_CODE" => "action",
					),
				)
			),
			false
		);?> 	
		</div>
	
	</div>
	
	
</div>
<section class="page-section whyssl-section isp-can">
	
	<div class="page-section__h">Возможности ISP панели</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/1.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Удобство пользования</div>
					<p>Защищенный доступ по протоколу HTTPS, поддержка всех браузеров, дружелюбный интерфейс</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/2.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущество</div>
					<p>Интуитивно понятный интерфейс, простота использования, облегчает администрирование при минимальных знаниях</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/3.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Возможности</div>
					<p>Настройка Веб сервера и пользователей, резервное копирование, установка дополнительных компонентов мониторинг нагрузки и многое другое</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/4.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатная поддержка и консультация</div>
					<p>Оказываем бесплатную поддержку и консультируем наших клиентов как пользоваться данным продуктом</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/5.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Кросплатформенность</div>
					<p>Панель может быть установлена на любую OS Windows или Linux. Рекомендуем использовать CentOS или Debian</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/isp/6.svg">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатая установка и настройка</div>
					<p>Все панели управления устанавливаем и настраиваем бесплатно</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>
<section class="page-section whyssl-section isp-can">
	<div class="container">
		<div class="hsw__wrapper">
			<div class="hsw-block">
				<h2 class="hsw-block__h">Установка ISPmanager</h2>
				<div class="hsw-block__content">
					<p>На первом этапе необходимо проверить, соответствует ли виртуальный или выделенный сервер минимальным требованиям. Также следует разрешить входящие подключения на порты, указанные в инструкции.</p>
					<p>Второй этап — активация продукта. Обратите внимание, что любую коммерческую лицензию необходимо приобрести до начала установки. При покупке необходимо указать внешний IP сервера, на котором будет работать панель управления. Если не функционирует NAT, активация произойдет автоматически. Если вы не знаете, как узнать необходимые данные, обратитесь в службу техподдержки.</p>
					<div class="blue-note">
						<b>Важно!</b><br> Если сервер защищен NAT или у лицензии не указан IP адрес, активацию можно произвести с помощью ключа. Его можно сгенерировать в личном кабинете. 
					</div>
					<br/>
					<p>Для установки панели управления ISPmanager необходимо получить доступ к серверу по SSH с правами суперпользователя. После этого необходимо загрузить и запустить скрипт, указанный в инструкции.</p>
					<p>После этого необходимо выбрать одну из веток обновлений:</p>
					<p><span class="bold">Stable</span> - будут устанавливаться только официальные обновления от разработчика.</p>
					<p><span class="bold">Beta</span> - будут устанавливаться как официальные обновления, так и апдейты, находящиеся на этапе тестирования.</p>
					<p>На следующем этапе необходимо выбрать версию программы:</p>
					<p>1. <span class="bold">Lite with recommended software</span>. Эта версия содержит рекомендуемый набор программ.</p>
					<p>2. <span class="bold">Lite minimal version</span>. Эта версия содержит минимальный набор программ, необходимый для работы. Она занимает меньше дискового пространства.</p>
					<p>3. <span class="bold">Business</span>. Эта версия содержит расширенный функционал и занимает больше свободного пространства на диске.</p>
				</div>
				<h2 class="hsw-block__h">Кому подойдет панель управления веб-хостингом?</h2> 
				<div class="hsw-block__content">
					<p>Ее стоит выбрать, если вы</p>
					<ul class="marker">
						<li>никогда раньше не занимались администрированием. В этом случае вам придется осваивать новые навыки или нанимать дополнительного специалиста в штат. Панель управления ISPmanager поможет вам сэкономить время, силы и деньги. С ее помощью вы сможете получить и настроить понятное окружение для хостинга и сайта;</li>
						<li>управляете веб-студией. С панелью управления вы существенно сокращаете время, необходимо для развертывания новых сайтов и управления ими. Вы также получаете возможность оперативной настройки почты. Благодаря этому вы сможете направить все ресурсы на разработку новых проектов;</li>
						<li>заботитесь о безопасности данных. Программа обеспечивает высокую степень защиты информации. Благодаря регулярным обновлениям вы всегда получаете последнюю версию программы с исправлениями и улучшениями.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопрос-ответ</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли изменить IP-адрес, на который оформлена лицензия?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. Для этого зайдите в соответствующий раздел в личном кабинете и нажмите кнопку «Изменить IP-адрес». В появившейся форме укажите новые значения. Менять адрес можно не чаще одного раза в месяц. Если это необходимо делать чаще, обращайтесь в техническую поддержку.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли добавить дополнительные серверы для панели управления веб-хостингом?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, причем сделать это можно в рамках одной лицензии. Для добавления дополнительных серверов необходимо подключить дополнительные узлы в разделе «товары и услуги»</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли перейти с версии программы Lite на Busines 5?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, для этого в разделе «Товары и услуги» личного кабинета необходимо изменить тип лицензии или купить новую. Срок ее действия сокращается пропорционально разнице цен.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли получить программу бесплатно?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Пробный период (Trial-версия) предоставляется каждому новому пользователю. Многие компании, занимающиеся хостингом и предоставлением выделенных и виртуальных серверов, включая Depohost, дают возможность получить ISPmanager по специальной цене.</span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-section anyq-section anyq-black">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	


<?
	foreach ($APPLICATION->arAdditionalChain as $key => $chain) {
		if($chain["LINK"] == "/ispmanager/paneli-upravleniya-serverom/"){
			unset($APPLICATION->arAdditionalChain[$key]);
		}
	}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>