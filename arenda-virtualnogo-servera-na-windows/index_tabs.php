<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<div class="advantage-img-wrap">
  <h2 class="subtitle">Продукты Microsoft для крупных проектов</h2>
  <ul class="advantage">
    <li class="advantage__item">
      <div class="advantage__item_icon operator"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name"> Бесплатная поддержка и консультация</div>
        <div class="advantage__item_description">Проконсультируем Вас по любым вопросам связанным с выбором ПО, поможем
          сделать правильный выбор
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная установка и настройка</div>
        <div class="advantage__item_description">Бесплатно установим и настроим арендованный Windows сервер или любое
          другое ПО Microsoft
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon server-check"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Преимущество использования</div>
        <div class="advantage__item_description">Экономия, большой выбор, доступ к новым приложениям в течении часа при
          наличии
          заключенного договора
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon thumb-click"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Большой выбор</div>
        <div class="advantage__item_description"> Для заказа аренды приложений Microsoft доступны почти вся линейка
          продуктов
          компании
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon star"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Большие возможности</div>
        <div class="advantage__item_description">Вы можете попробовать арендовать любой продукт Microsoft прежде чем его
          приобрести
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon stack"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Выгодная экономия</div>
        <div class="advantage__item_description"> Аренда Windows сервера выгодно отличается от лицензии приобретенной в
          розницу,
          экономьте заказывайте аренду сервера Windows
        </div>
      </div>
    </li>
  </ul>
</div>
