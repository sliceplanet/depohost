<? use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/microsoft.css');
?>
<div class="container block_text">
  <div class="text_subtitle">
    <span class="icon icon-rent"></span>
  </div>
  <br>
  <p>
    Аренда программного обеспечения Microsoft позволяет решить любые организационные задачи. Разработчик предлагает
    разные ПО, поэтому любая компания или организация может подобрать оптимальные программные продукты. При этом
    эффективность работы компании не будет снижена.<br>
  </p>
  <p>
    Аренда сервера Windows Server представлена нами в рамках программы SPLA. Подобное ПО можно взять в аренду вместе с
    1С или любым другим решением. Если же подходящего варианта на нашем сайте Вы не нашли, то <a
        href="https://www.depohost.ru/contacts/">наши менеджеры</a> с удовольствием подберут для Вас более оптимальное
    решение.
  </p>
  <p style="text-align: center;">
    <img alt="аренда сервера windows" src="https://www.depohost.ru/bitrix/images/news/windows_server.jpg"
         data-cke-saved-src="https://www.depohost.ru/bitrix/images/news/windows_server.jpg"
         title="аренда сервера windows" style="height: 289px; width: 550px;">
  </p>
  <h2>Windows Server</h2>
  <p>
    Достаточно интересным вариантом для компаний можно назвать Server 2008 R2, обладающий гибкой настройкой сервера, а
    также Server 2012 R2, имеющий расширенный функционал.
  </p>
  <p>
    Среди преимуществ Windows <a href="https://www.depohost.ru/">серверов в аренду</a> стоит назвать бесплатную
    поддержку и консультации.
  </p>
  <p>
    Кроме самой операционной системы можно взять в аренду Microsoft Office. Вряд ли при покупке окупаемости можно будет
    достичь за короткий срок, особенно, если это развивающийся бизнес.
  </p>
  <p style="text-align: center;">
    <img alt="аренда виндовс сервера"
         src="https://www.depohost.ru/bitrix/images/news/microsoft-windows-server-logo-color.jpg"
         data-cke-saved-src="https://www.depohost.ru/bitrix/images/news/microsoft-windows-server-logo-color.jpg"
         title="аренда виндовс сервера" style="height: 136px; width: 550px;">
  </p>
</div>
