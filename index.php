<?

use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Аренда сервера в Москве в дата-центре (ЦОД) | Арендовать выделенный сервер, стоимость хостинга");
$APPLICATION->SetPageProperty("keywords", "Аренда сервера, хостинг сервера, дешевый хостинг сервера, аренда сервера в Москве");
$APPLICATION->SetPageProperty("description", "Аренда сервера — это самый надежный хостинг из всех доступных. Мощности арендованного сервера используется исключительно вашим проектом");
$APPLICATION->SetPageProperty("keywords",
  "Аренда сервера, хостинг сервера, дешевый хостинг сервера, аренда сервера в Москве");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Аренда сервера, Аренда сервера в Москве, хостинг сервера");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/main-slider.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick-theme.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick.css');


$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.selectbox-m.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic-media.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/select-small.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/dedic.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.prettyCheckboxes.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scripts_bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jQueryUITouchPunch.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');




Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/main.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/main-media.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/sprites/main-page.css');


$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/new_main.css');


CJSCore::Init();
$step = (int)$_REQUEST['step'];
?>
   <?
   if ($_REQUEST['isAjax'] == 'Y') {
      $APPLICATION->RestartBuffer();
    }
   ?> 
  <?
  if ($step == 2 && $_REQUEST['isAjax'] == 'Y'):
    $APPLICATION->IncludeComponent(
      "itin:dedicated.advance.options",
      "v2020",
      Array(
        "IBLOCK_ID" => "50",
        "IBLOCK_TYPE" => "catalog"
      )
    );
  endif;
  ?>
  <?
  if ($_REQUEST['isAjax'] == 'Y') {
    die();
  }?>
  
<div class="main-page">
	
<?/*$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"mainpage-slider",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", ""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "66",
		"IBLOCK_TYPE" => "multimedia",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("", "*", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);*/?>

<div class="mainpage-first-screen">
	<div class="mfs-h">Хостинг и услуги<br>на любой вкус</div>
	<div class="container">
		<div class="mainpage-links-blocks">
			<div class="row">
				<div class="col-md-4">
					<div class="mainpage-links-block">
						<div class="mlb-h">Для бизнеса</div>
						<div class="mlb-desc">Аренда сервера для бизнеса, бесплатная настройка и администрирование</div>
						<div class="mlb-items">
							<a href="/arenda-virtualnogo-vydelennogo-servera-vds-gotovie-resheniya/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/2.svg" />
									<img src="" />
									<div class="gift-icon">
										<span class="css-sprite-gift-icon"></span>
									</div>
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Виртуальный сервер</div>
									<div class="mlb-item__price">от 600 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/arenda-dedicated_server/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/1.svg" />
									<img src="" />
									<div class="gift-icon">
										<span class="css-sprite-gift-icon"></span>
									</div>
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Выделенный сервер</div>
									<div class="mlb-item__price">от 5 100 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/microsoft/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/5.svg" />
									<img src="" />
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Лицензия Microsoft<br>Windows Server</div>
									<div class="mlb-item__price">от 1320 р/мес <span>></span></div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mainpage-links-block">
						<div class="mlb-h">Для бухгалтерии</div>
						<div class="mlb-desc">1С сервер для бухгалтерии, бесплатная настройка и администрирование</div>
						<div class="mlb-items">
							<a href="/1c-server/arenda-virtualnogo-servera-1c/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/6.svg" />
									<img src="" />
									<div class="gift-icon">
										<span class="css-sprite-gift-icon"></span>
									</div>
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Виртуальный 1С<br>сервер <span>от 2х пользователей</span></div>
									<div class="mlb-item__price">от 2 620 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/1c-server/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/6.svg" />
									<img src="" />
									<div class="gift-icon">
										<span class="css-sprite-gift-icon"></span>
									</div>
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Выделенный 1С<br>сервер <span>от 10 пользователей</span></div>
									<div class="mlb-item__price">от 14 900 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/microsoft/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/3.svg" />
									<img src="" />
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Доп. лицензия на<br>рабочий стол</div>
									<div class="mlb-item__price">360 р/мес <span>></span></div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mainpage-links-block">
						<div class="mlb-h">Для вебмастера</div>
						<div class="mlb-desc">Веб сервер с бесплатным доменом<br>и SSL сертификатом</div>
						<div class="mlb-items">
							<a href="/domain/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/9.svg" />
									<img src="" />
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Регистрация доменов</div>
									<div class="mlb-item__price">от 400 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/ispmanager/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/7.svg" />
									<img src="" />
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">Панель управления </div>
									<div class="mlb-item__price">от 450 р/мес <span>></span></div>
								</div>
							</a>
							<a href="/ssl-sertifikat/" class="mlb-item">
								<div class="mlb-item__icon">
									<img src="/images/services/8.svg" />
									<img src="" />
								</div>
								<div class="mlb-item__data">
									<div class="mlb-item__name">SSL сертификаты</div>
									<div class="mlb-item__price">от 890 р/мес <span>></span></div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="mfs-bottom">
						<a href="/free-hosting/" style="text-decoration:none;">
							<span class="css-sprite-gift-label"></span>
						</a>
						При аренде сервера, SSL сертификат и домен (RU, РФ) в подарок
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="narrow-banner">
	<div class="container">
		<div class="narrow-banner__wrapper">
			<div class="narrow-banner__left">
			
			</div>
			<div class="narrow-banner__right">
				<div class="narrow-banner__h">Бесплатный тестовый сервер</div>
				<div class="narrow-banner__desc">Мы уверены в наших услугах и готовы бесплатно<br>предоставить вам сервер на 7 дней!</div>
				<div class="narrow-banner__actions">
					<a href="/free-hosting/test-access/" class="button-blue2">Выбрать сервер</a>
				</div>
			</div>
		</div>
	</div>
</div>


<?/* $APPLICATION->IncludeFile(
	"/include/mainpage/services.php"
  ); */?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/service-item-detail.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/configurator.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/rent.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/service-consist.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/why-we.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/contact-us.php"
  ); ?>
<?
global $faqFilter;
$faqFilter["!PREVIEW_TEXT"] = false;
?>
<section class="mainpage-block faq-block">
    <div class="container">
        <h3 class="hsw-block__h">Вопросы и ответы</h3>
        <div class="faq-items" itemscope itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Как я могу увеличить производительность сервера?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Для этого достаточно отправить нам заявку через личный кабинет пользователя или по телефону. Все работы выполнят наши сотрудники.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Сколько стоит хостинг для моего сайта?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Стоимость услуги определяется в зависимости от мощности оборудования. Если вы не знаете, какой вариант будет оптимальным, обратитесь в службу техподдержки. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Могу ли я получить пробный период?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, мы предоставляем возможность протестировать dedicated- и виртуальные серверы в течение определенного срока, после которого вы можете продолжить пользоваться ими или отказаться от услуг. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Сколько стоят бэкапы данных при заказе хостинга?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Мы предоставляем клиентам 100 GB свободного пространства для создания резервных копий бесплатно. График создания бэкапов вы можете обсудить с нашими сотрудниками. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Могу ли я устанавливать программы на свой сервер?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, при необходимости вы также можете изменить установленную ОС, загрузив образ диска. При заказе некоторых услуг вы можете получить программы на специальных условиях. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/our-tech.php"
  ); ?>


</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>