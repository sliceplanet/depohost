<?

\CHTTP::SetStatus('200 OK');
include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog.php';
$APPLICATION->RestartBuffer();
\CHTTP::SetStatus('404 Not Found');
include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH. '/header.php';
?>

<div class="container">
  <h1 class="h1">Страница не найдена или не существует</h1>
  <p style="font-size: 18px">
    Перейдите, пожалуйста, на <a href="/">Главную страницу</a> или воспользуйтесь меню сайта
  </p>
  <div>
    <img style="margin: 0 auto" class="img-responsive" src="/images/404.jpg" alt="">
  </div>
  <div class="category-menu" style="margin: 0 auto 30px; ">
    <? $APPLICATION->IncludeFile(
      $APPLICATION->GetTemplatePath("include_areas/catalog_icons.php"),
      Array(),
      Array("MODE" => "html")
    ); ?>
  </div>
</div>
<?
include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH. '/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog.php';
die();
?>