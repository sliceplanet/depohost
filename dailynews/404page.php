<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Страница 404");
$APPLICATION->SetPageProperty("keywords", "Страница 404");
$APPLICATION->SetPageProperty("description", "Страница 404");
$APPLICATION->SetTitle("404");
?><?include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");?>
<!DOCTYPE html>
<html>
<head>
 <title></title>
 <meta http-equiv="Content-Type" content="text/html">
 <meta name="KeyWords" content="Страница не найдена - Error 404">
 <meta name="Description" content="Страница не найдена - Error 404">
 <meta name="Author" content="DepoTelecom">
</head>
<body>
<br><br>
    <div align="Center">
        <font color="##FFFF00">
            <h1><span style="font-family: Rockwell Extra Bold;">ERROR 404, not found!</span></h1>
        </font>
    </div>
    <div align="Left"></div>
   <font face="Rockwell Extra Bold" size="+1"><br>
        Запрашиваемая страница не найдена или не существует<br>
        Перейдите пожалуйста на <a href="/"><b>Главную страницу</b></a>
   </font>
    <br><br><br><br><br><br>
    <p align="Center"><img src="/images/404.jpg" height="450" width="615" alt="404.jpg (52,1kb)" border="0"></p>
    <font color="##FFFF00">ArtexTelecom&copy;</font>
</body>
</html><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>