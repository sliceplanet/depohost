<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Стоимость трафика");
$APPLICATION->SetPageProperty("keywords", "Стоимость трафика");
$APPLICATION->SetPageProperty("title", "Стоимость трафика | ArtexTelecom");
$APPLICATION->SetTitle("Стоимость трафика");
?>

<section class="page-section how-ssl-works-section dns-hosting-text">
	
	<div class="container">
		<h1 class="page-section__h"><? $APPLICATION->ShowTitle(false) ?></h1>
		<h2 class="page-section__h">Стоимость трафика для услуги <a href="/"><strong>Аренда сервер</strong></a></h2>
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__h"><strong>Первый</strong> - тарифный план</div>
				<div class="hsw-block__content">
					<ul class="ul50p fw500">
						<li>Скорость - 10 Мбит.с.</li>

						<li>Ширина канала - 10Мбит.с FullDuplex.</li>

						<li><strong>Трафик</strong> - <strong>безлимитный</strong>.</li>

						<li>Стоимость - 0 руб. по умолчанию на всех тарифных планах VDS и выделенных серверах.</li>

						<li>Как показывает практика, ширина канала при скорости 10 Мбит.с Full Duplex позволяет потреблять 3Тб
						  трафика в месяц бесплатно.
						</li>
					</ul>
				</div>
				
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h"><strong>Второй</strong> - тарифный план</div>
				<div class="hsw-block__content">
					<ul class="ul50p fw500">
						<li>Скорость - 100 Мбит.с</li>

						<li>Ширина канала - не гарантированная полоса пропускания трафика.</li>

						<li><strong>Трафик</strong> - <strong>безлимитный.</strong></li>

						<li>Стоимость - 8000 руб. в месяц.</li>

						<li>Как показывает статистика реальная скорость на данном канале связи в пик нагрузки может доходить до 75
						  Мбит.с Full Duplex что более чем достаточно для большинства очень крупных проектов.
						</li>
					</ul>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h"><strong>Третий</strong> - тарифный план</div>
				<div class="hsw-block__content">
					<ul class="ul50p fw500">
						<li>Скорость 1 Гбит.с.</li>

						<li>Ширина канала - не гарантированная полоса пропускания трафика.</li>

						<li>Трафик - безлимитный.</li>

						<li>Стоимость - 18000 руб. в месяц.</li>
					</ul>
				</div>
				
			</div>
			<div class="hsw-block">
				<h2 class="hsw-block__h">Стоимость трафика для услуги <a href="/hosting/"><strong>хостинг</strong></a></h2>
				<div class="hsw-block__content">
					<ul class="ul50p fw500">
        <li>Данный тариф предназначен для услуги виртуальный хостинг.</li>

        <li>По умолчанию все Web сервера подключены к  тарифу 10 Мбит.с Full Duplex с нелимитируемым трафиком.</li>

        <li>Интернет трафик к услуге Web хостинг предоставляется бесплатно.</li>

        <li>Без соотношений трафика входящий к исходящему.</li>

        <li>Без разделения Российский - Зарубежный.</li>
					</ul>
				</div>
				
			</div>
		
    </div>
	
  </div>
 </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>