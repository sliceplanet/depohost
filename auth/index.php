<?
if (!array_key_exists('register', $_REQUEST))
{
  define("NEED_AUTH", true);
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>

<?

if (array_key_exists('register', $_REQUEST))
{
  LocalRedirect('/auth/');
}
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"]) > 0)
{
  LocalRedirect($backurl);
}

$APPLICATION->SetTitle("Авторизация");

if (in_array(11, $USER->GetUserGroupArray()))
{
  LocalRedirect('/personal/');
  ?>
  <p>Вы зарегистрированы и успешно авторизовались. <?= $arUser["ID"] ?></p>
  <p><a href="<?= SITE_DIR ?>">Вернуться на главную страницу</a></p>
  <?
}
else
{
  if (isset($_GET['forgot_password']) && $_GET['forgot_password'] == 'yes')
  {
    $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "", array());
  }
  else
  {
    ?>

    <? $APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array()); ?>

    <?
  }
}
?>
            <?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

