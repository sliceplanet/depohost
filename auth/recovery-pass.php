<?
/*if(!array_key_exists('register', $_REQUEST))
{
    define("NEED_AUTH", true);
}*/
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<link href="/local/templates/depohost/css/pages/auth.css" type="text/css" rel="stylesheet">
<link href="/local/templates/depohost/css/pages/auth-media.css" type="text/css" rel="stylesheet">
<div class="n-page-wrapper">
 <div class="aut-l"></div>
 <div class="aut-r"></div>
 <div class="container n-auth">
  <div class="row">
   <div class="col-xs-12">
    <section class="block">
     <div class="block-txt">
      <h1 class="h1">Авторизация<span>Восстановление</span></h1>
      <form name="bform" method="post" target="_top" action="/auth/?forgot_password=yes">
       <input type="hidden" name="backurl" value="/auth/">
       <input type="hidden" name="AUTH_FORM" value="Y">
       <input type="hidden" name="TYPE" value="SEND_PWD">
       <p>
        Если вы забыли пароль, введите номер договора.<br>Пароль будет выслан на Ваш E-Mail. </p>

       <table class="data-table bx-forgotpass-table bx-auth-table">
        <thead>
        <tr class="bx-auth__wrap">
         <td colspan="2"><b>Выслать пароль</b></td>
        </tr>
        </thead>
        <tbody>
        <tr class="bx-auth__wrap">
         <td class="bx-auth-label">Номер договора:</td>
         <td><input type="text" name="USER_LOGIN" class="bx-auth-input" maxlength="50" value=""></td>
        </tr>
        </tbody>
        <tfoot>
        <tr class="bx-auth__wrap">
         <td></td>
         <td class="authorize-submit-cell">
          <input type="submit" name="send_account_info"  value="Выслать">
         </td>
        </tr>
        <tr class="bx-auth__wrap">
         <td></td>
         <td>
          <a class="forgot-password-auth" href="/auth/?login=yes"><b>Авторизация</b></a>
         </td>
        </tr>
        </tfoot>
       </table>
      </form>
     </div>
     <div class="block-pm auth">
      <div class="block-pm-ttl">Вы находитесь на странице восстановления пароля входа в Личный кабинет</div>
      <div class="block-pm-item">В Личном кабинете вы можете полноценно управлять своими услугами.</div>
      <div class="block-pm-item">Обращаться в финансовую службу.</div>
      <div class="block-pm-item">Получать доступ к закрывающим документам.</div>
      <div class="block-pm-item">Получить доступ к договору и приложения к нему на оказываемые услуги.</div>

      <div class="block-pm-item">Обращаться в техническую поддержку.</div>
      <div class="block-pm-item">Получить доступ в панель управления DNS записями вашего домена.</div>
      <div class="block-pm-item">Получить доступ к FTP серверу прикрепленного к вашему аккаунту.</div>
      <div class="block-pm-item">Управлять своими данными и многое другое.</div>
     </div>
    </section>
    <!--<section class="block">-->
    <!--<div class="block-txt">-->

    <!--<h1 class="h1">Авторизация<span>Востановление</span></h1>-->
    <!--<p>Укажите Ваш номер договора и пароль</p>-->

    <!--<form name="form_auth" method="post" target="_top" action="/auth/?login=yes">-->

    <!--<input type="hidden" name="AUTH_FORM" value="Y">-->
    <!--<input type="hidden" name="TYPE" value="AUTH">-->
    <!--&lt;!&ndash;<input type="hidden" name="backurl" value="/personal/" />&ndash;&gt;-->
    <!--<input type="hidden" name="backurl" value="/personal/">-->

    <!--<table class="bx-auth-table">-->
    <!--<tbody>-->
    <!--<tr class="bx-auth__wrap">-->
    <!--<td class="bx-auth-label">Номер договора:</td>-->
    <!--<td><input class="bx-auth-input" type="text" name="USER_LOGIN" maxlength="255" value="1215"></td>-->
    <!--</tr>-->
    <!--<tr class="bx-auth__wrap">-->
    <!--<td class="bx-auth-label">Пароль:</td>-->
    <!--<td><input class="bx-auth-input" type="password" name="USER_PASSWORD" maxlength="255">-->
    <!--</td>-->
    <!--</tr>-->
    <!--<tr class="bx-auth__wrap">-->
    <!--<td></td>-->
    <!--<td class="authorize-submit-cell"><input type="submit" name="Login" value="Войти">-->
    <!--<div class="forgot-password"><a href="/auth/?forgot_password=yes" rel="nofollow">Забыли свой пароль?</a></div>-->
    <!--</td>-->
    <!--</tr>-->
    <!--&lt;!&ndash;noindex&ndash;&gt;-->
    <!--&lt;!&ndash;<tr>&ndash;&gt;-->
    <!--&lt;!&ndash;<td></td>&ndash;&gt;-->
    <!--&lt;!&ndash;<td class="forgot-password"><a href="/auth/?forgot_password=yes" rel="nofollow">Забыли свой пароль?</a></td>&ndash;&gt;-->
    <!--&lt;!&ndash;</tr>&ndash;&gt;-->
    <!--&lt;!&ndash;/noindex&ndash;&gt;-->
    <!--</tbody>-->
    <!--</table>-->
    <!--</form>-->
    <!--</div>-->
    <!--<div class="block-pm auth">-->
    <!--<div class="block-pm-ttl">Вы находитесь на странице входа в Личный кабинет</div>-->
    <!--<div class="block-pm-item">В Личном кабинете вы можете-->
    <!--полноценно управлять своими услугами.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Обращаться в финансовую-->
    <!--службу.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Получать доступ к-->
    <!--закрывающим документам.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Получить доступ к договору и-->
    <!--приложения к нему на оказываемые услуги.-->
    <!--</div>-->

    <!--<div class="block-pm-item"> Обращаться в техническую-->
    <!--поддержку.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Получить доступ в панель-->
    <!--управления DNS записями вашего домена.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Получить доступ к FTP-->
    <!--серверу прикрепленного к вашему аккаунту.-->
    <!--</div>-->
    <!--<div class="block-pm-item"> Управлять своими данными и-->
    <!--многое другое.-->
    <!--</div>-->


    <!--</div>-->

    <!--</section>-->
   </div>
  </div>
 </div>
</div>

<?
/*
if(array_key_exists('register', $_REQUEST))
{
    LocalRedirect('/auth/');
}
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"]) > 0)
LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");

if (in_array(11, $USER->GetUserGroupArray()))
{
LocalRedirect('/personal/');
?>
<p>Вы зарегистрированы и успешно авторизовались. <?= $arUser["ID"] ?></p>
<p><a href="<?= SITE_DIR ?>">Вернуться на главную страницу</a></p>
<?
} else
{
    if (isset($_GET['forgot_password']) && $_GET['forgot_password'] == 'yes')
    {
        $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "", array());
} else
{
?>

<? $APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array()); ?>

<?
    }
}



*/require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

