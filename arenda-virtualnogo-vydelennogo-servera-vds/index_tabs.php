<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?/*
<div class="advantage-img-wrap">
  <h2 class="subtitle">Преимущества аренды VDS сервера</h2>
  <ul class="advantage">
    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатный трафик</div>
        <div class="advantage__item_description">Арендуя виртуальный сервер VDS, вы получаете бесплатный безлимитный трафик
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon path"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная настройка и консультации</div>
        <div class="advantage__item_description">Каждый виртуальный выделенный сервер уже настроен, панель ISPmanager в подарок бесплатно!
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon server-top"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Высокопроизводительные СХД</div>
        <div class="advantage__item_description">Для виртуализации и облачных решений мы используем
          высокопроизводительные СХД, HP, Infortrend
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon mouse"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Дополнительно 15 дней бесплатно</div>
        <div class="advantage__item_description">После оплаты вы получаете дополнительно 15 дней бесплатно к выбранному
          виртуальному серверу <a href="/rules-test-vds.php">Подробнее</a></div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon kvm"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Аппаратная виртуализация KVM и Huper-V</div>
        <div class="advantage__item_description">Аппаратная виртуализация VDS сервера гарантирует честное распределение
          ресурсов
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon os"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Большой выбор OS Windows и Linux</div>
        <div class="advantage__item_description">Взяв в аренду виртуальный сервер мы можем установить Вам любую
          операционную систему
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon day"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Круглосуточная поддержка 24/7</div>
        <div class="advantage__item_description">Пользуясь нашими услугами вы всегда можете рассчитывать на
          круглосуточную техническую поддержку
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon hand"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Вы приобретаете услуги из первых рук</div>
        <div class="advantage__item_description">Важно знать! Мы как российский оператор предоставляем услуги из первых
          рук, мы отвечаем за свое качество!
        </div>
      </div>
    </li>
  </ul>
</div>
*/?>
