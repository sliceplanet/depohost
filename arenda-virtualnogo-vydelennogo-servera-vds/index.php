<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "У нас вы можете взять в аренду выделенный виртуальный сервер на самых выгодных условиях: минимальные цены, мощное производительное оборудование, профессиональная техподдержка, возможность установки любых ОС (Windows, Linux) и программ, гарантия безопасности и конфиденциальности данных");
$APPLICATION->SetPageProperty("keywords_inner", "VDS");
$APPLICATION->SetPageProperty("title", "Аренда виртуального сервера (VDS) в Москве | Арендовать удаленный виртуальный сервер (ВДС) по недорогой цене");
$APPLICATION->SetTitle("Аренда виртуального сервера");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/sprites/vds.css');
?>

<div id="virtual-vds-page">
	
	<section class="page-section whyssl-section why-vvds">
		
		<div class="page-section__icon">
			<img src="/images/services/2.svg">
		</div>
		<div class="page-section__h">Преимущества аренды VDS сервера</div>
		
		<div class="container">
			
			<div class="wug__wrapper">
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds1.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Преимущества аренды VDS сервера</div>
						<p>Хостинг виртуальных выделенных серверов (VPS) – один из наиболее выгодных вариантов размещения веб-ресурса для нового бизнеса...</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds2.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Бесплатная настройка<br>и консультации</div>
						<p>Каждый виртуальный выделенный сервер уже настроен, панель ISPmanager в подарок бесплатно!</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds3.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Высокопроизводительные СХД</div>
						<p>Для виртуализации и облачных решений мы используем высокопроизводительные СХД, HP, Infortrend</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds4.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Дополнительно 15 дней бесплатно</div>
						<p>После оплаты вы получаете дополнительно 15 дней бесплатно к выбранному виртуальному серверу<br><a href="#">Подробнее</a></p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds5.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Аппаратная виртуализация KVM<br>и HUPER-V</div>
						<p>Аппаратная виртуализация VDS сервера гарантирует честное распределение ресурсов</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds6.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Большой выбор OS Windows и Linux</div>
						<p>Взяв в аренду виртуальный сервер мы можем установить Вам любую операционную систему </p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds7.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Круглосуточная поддержка 24/7</div>
						<p>Пользуясь нашими услугами вы всегда можете рассчитывать на круглосуточную техническую поддержку</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds8.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Вы приобретаете услуги<br>из первых рук</div>
						<p>Важно знать! Мы как российский оператор предоставляем услуги из первых рук, мы отвечаем за свое качество!</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/wug/icon_9.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Снижение ваших затрат</div>
						<p>Аренда сервера это экономия ваших средств не только на покупке, обслуживании и администрировании сервера</p>
					</div>
				</div>				
			</div>
			
		</div>
		
	</section>	
	
</div>





<? $APPLICATION->IncludeComponent(
  "bitrix:catalog",
  "vds_flexible2020",
  array(
    "IBLOCK_TYPE" => "xmlcatalog",
    "IBLOCK_ID" => "22",
    "HIDE_NOT_AVAILABLE" => "N",
    "SECTION_ID_VARIABLE" => "SECTION_ID",
    "SEF_MODE" => "N",
    "SEF_FOLDER" => "/vds/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "Y",
    "CACHE_TIME" => "360000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_STATUS_404" => "Y",
    "SET_TITLE" => "Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "ADD_ELEMENT_CHAIN" => "N",
    "USE_ELEMENT_COUNTER" => "Y",
    "USE_FILTER" => "N",
    "FILTER_VIEW_MODE" => "VERTICAL",
    "USE_REVIEW" => "N",
    "USE_COMPARE" => "N",
    "PRICE_CODE" => array(
      0 => "Розничная",
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRICE_VAT_SHOW_VALUE" => "N",
    "CONVERT_CURRENCY" => "N",
    "BASKET_URL" => "/personal/cart/",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "USE_PRODUCT_QUANTITY" => "Y",
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    "ADD_PROPERTIES_TO_BASKET" => "Y",
    "PRODUCT_PROPS_VARIABLE" => "prop",
    "PARTIAL_PRODUCT_PROPERTIES" => "N",
    "PRODUCT_PROPERTIES" => array(
      0 => "CPU_QTY",
      1 => "RAM",
      2 => "HDD",
      3 => "FTP",
      4 => "IP_QTY",
      5 => "OS",
    ),
    "SHOW_TOP_ELEMENTS" => "Y",
    "TOP_ELEMENT_COUNT" => "1",
    "TOP_LINE_ELEMENT_COUNT" => "1",
    "TOP_ELEMENT_SORT_FIELD" => "id",
    "TOP_ELEMENT_SORT_ORDER" => "desc",
    "TOP_ELEMENT_SORT_FIELD2" => "id",
    "TOP_ELEMENT_SORT_ORDER2" => "desc",
    "TOP_PROPERTY_CODE" => array(
      0 => "CPU_QTY",
      1 => "CPU_QTY_MAX",
      2 => "CPU_QTY_MIN",
      3 => "CPU_QTY_PRICE",
      4 => "CPU_QTY_STEP",
      5 => "RAM",
      6 => "RAM_MAX",
      7 => "RAM_MIN",
      8 => "RAM_PRICE",
      9 => "RAM_STEP",
      10 => "HDD",
      11 => "HDD_MAX",
      12 => "HDD_MIN",
      13 => "HDD_PRICE",
      14 => "HDD_STEP",
      15 => "FTP",
      16 => "FTP_MAX",
      17 => "FTP_MIN",
      18 => "FTP_PRICE",
      19 => "FTP_STEP",
      20 => "IP_QTY",
      21 => "IP_QTY_MAX",
      22 => "IP_QTY_MIN",
      23 => "IP_QTY_PRICE",
      24 => "IP_QTY_STEP",
      25 => "OS",
      26 => "OS_PRICE",
      27 => "TEST_PERIOD_14",
      28 => "CMN_1",
      29 => "CMN_2",
      30 => "CMN_3",
      31 => "ADD_2",
      32 => "ADD_1",
      33 => "ADD_3",
      34 => "CMN_10",
      35 => "CMN_8",
      36 => "CMN_9",
      37 => "CMN_4",
      38 => "CMN_5",
      39 => "CMN_6",
      40 => "CML2_ARTICLE",
      41 => "CML2_BASE_UNIT",
      42 => "CML2_TRAITS",
      43 => "CML2_ATTRIBUTES",
      44 => "TEST_PERIOD_14",
      45 => "",
    ),
    "SECTION_COUNT_ELEMENTS" => "Y",
    "SECTION_TOP_DEPTH" => "2",
    "SECTIONS_VIEW_MODE" => "TEXT",
    "SECTIONS_SHOW_PARENT_NAME" => "Y",
    "PAGE_ELEMENT_COUNT" => "1",
    "LINE_ELEMENT_COUNT" => "1",
    "ELEMENT_SORT_FIELD" => "sort",
    "ELEMENT_SORT_ORDER" => "asc",
    "ELEMENT_SORT_FIELD2" => "id",
    "ELEMENT_SORT_ORDER2" => "desc",
    "LIST_PROPERTY_CODE" => array(
      0 => "CPU_QTY",
      1 => "RAM",
      2 => "HDD",
      3 => "FTP",
      4 => "IP_QTY",
      5 => "OS",
      6 => "TEST_PERIOD_14",
      7 => "CMN_1",
      8 => "CMN_2",
      9 => "CMN_3",
      10 => "ADD_2",
      11 => "ADD_1",
      12 => "ADD_3",
      13 => "CMN_10",
      14 => "CMN_8",
      15 => "CMN_9",
      16 => "CMN_4",
      17 => "CMN_5",
      18 => "CMN_6",
      19 => "CML2_ARTICLE",
      20 => "CML2_BASE_UNIT",
      21 => "CML2_TRAITS",
      22 => "CML2_ATTRIBUTES",
      23 => "CML2_BAR_CODE",
      24 => "TEST_PERIOD_14",
      25 => "",
    ),
    "INCLUDE_SUBSECTIONS" => "Y",
    "LIST_META_KEYWORDS" => "-",
    "LIST_META_DESCRIPTION" => "-",
    "LIST_BROWSER_TITLE" => "-",
    "DETAIL_PROPERTY_CODE" => array(
      0 => "CPU_QTY",
      1 => "RAM",
      2 => "HDD",
      3 => "FTP",
      4 => "IP_QTY",
      5 => "OS",
      6 => "TEST_PERIOD_14",
      7 => "CMN_1",
      8 => "CMN_2",
      9 => "CMN_3",
      10 => "ADD_2",
      11 => "ADD_1",
      12 => "ADD_3",
      13 => "CMN_10",
      14 => "CMN_8",
      15 => "CMN_9",
      16 => "CMN_4",
      17 => "CMN_5",
      18 => "CMN_6",
      19 => "CML2_ARTICLE",
      20 => "CML2_BASE_UNIT",
      21 => "CML2_TRAITS",
      22 => "CML2_ATTRIBUTES",
      23 => "CML2_BAR_CODE",
      24 => "TEST_PERIOD_14",
    ),
    "DETAIL_META_KEYWORDS" => "-",
    "DETAIL_META_DESCRIPTION" => "-",
    "DETAIL_BROWSER_TITLE" => "-",
    "LINK_IBLOCK_TYPE" => "lists",
    "LINK_IBLOCK_ID" => "33",
    "LINK_PROPERTY_SID" => "HDD_VDS",
    "LINK_ELEMENTS_URL" => "",
    "USE_ALSO_BUY" => "N",
    "USE_STORE" => "N",
    "PAGER_TEMPLATE" => "",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "ADD_PICT_PROP" => "-",
    "LABEL_PROP" => "-",
    "SHOW_DISCOUNT_PERCENT" => "N",
    "SHOW_OLD_PRICE" => "N",
    "DETAIL_SHOW_MAX_QUANTITY" => "N",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_COMPARE" => "Сравнение",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Нет в наличии",
    "DETAIL_USE_VOTE_RATING" => "N",
    "DETAIL_USE_COMMENTS" => "N",
    "DETAIL_BRAND_USE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
    "SEF_URL_TEMPLATES" => array(
      "sections" => "",
      "section" => "#SECTION_CODE#/",
      "element" => "#SECTION_CODE#/#ELEMENT_ID#/",
      "compare" => "compare.php?action=#ACTION_CODE#",
    ),
    "VARIABLE_ALIASES" => array(
      "compare" => array(
        "ACTION_CODE" => "action",
      ),
    )
  ),
  false
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>