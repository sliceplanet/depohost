<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<section class="page-section section-graybg choose-vvds-section">
		<h2 class="page-section__h">Наши тарифы</h2>
		<div class="page-section__h">Выберите готовое решение</div>
		<p class="page-section__desc">Готовые решения будут вам предоставлены в течении 15 минут после оплаты.</p>
	
		<div class="container">
			<? $APPLICATION->IncludeComponent(
			  "bitrix:catalog.section.list",
			  "vds-turnkey2020",
			  array(
				"ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
				"CACHE_GROUPS" => "Y",  // Учитывать права доступа
				"CACHE_TIME" => "36000000",  // Время кеширования (сек.)
				"CACHE_TYPE" => "A",  // Тип кеширования
				"COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
				"IBLOCK_ID" => "51",  // Инфоблок
				"IBLOCK_TYPE" => "xmlcatalog",  // Тип инфоблока
				"SECTION_CODE" => "",  // Код раздела
				"SECTION_FIELDS" => "",  // Поля разделов
				"SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
				"SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => "",  // Свойства разделов
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "2",  // Максимальная отображаемая глубина разделов
				"VIEW_MODE" => "LINE"
			  )
			); ?>
			
			<p class="after-solutions-text">
Компания «ArtexTelecom» предоставляет в аренду виртуальные выделенные сервера VPS/VDS (от англ. «Virtual Private/Dedicated Server») по всей России. Эта услуга пользуется сегодня огромной популярностью у множества IT-организаций, владельцев веб-сайтов и сетевых проектов. Для пользователей, арендовать VDS - означает получить услуги удаленного сервера, но при этом, существенно сэкономить на приобретении и обслуживании собственного оборудования.			
			</p>
			
			
		</div>
</div>
<section class="page-section how-ssl-works-section vvds-osob">
	
	<div class="container">
	
		<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<h2 class="hsw-block__h">Когда требуются наши услуги?</h2>
				<div class="hsw-block__content">
					<p>Аренду виртуального сервера выбирают в случаях, когда <span class="bold">ресурсов собственного хостинга недостаточно</span>. Явными показателями можно считать, например, снизившийся аптайм сайта или увеличение времени загрузки. Чаще всего эти проблемы возникают из-за того, что серверы компании просто не справляются с увеличившейся нагрузкой.</p>
					<p>Вариант с арендой подходит и <span class="bold">для молодых компаний, которые не имеют возможности вкладывать ресурсы в покупку и установку физического оборудования</span>. К сопутствующим расходам в этом случае следует отнести и наем сотрудника, который будет отвечать за стабильную работу техники. Аренда сервера VDS позволяет снизить затраты, так как вместо покупки дорогостоящего оборудования компания вносит небольшие ежемесячные платежи. За обслуживание техники отвечают сотрудники нашей компании, а администрирование сайта не потребует найма сотрудников в штат.</p>
					<p>Еще одно важное преимущество виртуального удаленного сервера заключается в <span class="bold">возможности гибкого масштабирования</span>. По запросу вы можете увеличивать или уменьшать вычислительную мощность. За счет этого ваш сайт всегда будет открываться быстро вне зависимости от количества посетителей, а ваши сервисы будут работать без задержек.</p>
					<p>Наконец, аренда виртуального сервера <span class="bold">требуется компаниям, которые работают с ценными и конфиденциальными данными</span>. Все дата-центры нашей компании расположены в России (в Москве и Санкт-Петербурге), а также в Европе, поэтому хранить персональную информацию у нас разрешено законом. Кроме того, наша компания гарантирует каждому клиенту возможность экстренного восстановления данных и защиту от DDoS-атак и других видов сетевых угроз.</p>
				</div>
			</div>
			<div class="hsw-block">
				<h2 class="hsw-block__h">Архитектура системы</h2>
				<div class="hsw-block__content">
					<p>Технология виртуализации подразумевает размещение нескольких виртуальных серверов на одном физическом. При этом они полностью изолированы друг от друга системой KVM. Аренда сервера VDS в этом плане значительно выгоднее, ведь вы делите оборудование с другими клиентами. При этом вам не приходится жертвовать безопасностью.</p>
					<p>Технологии, используемые для виртуализации, позволяют делить ресурсы физического оборудования между виртуальными серверами. При этом все необходимые устройства эмулируются для каждого клиента. Благодаря этому аренда ВДС позволяет работать с большим количеством сайтов, почтовых ящиков, баз данных, FTP-сервисов. В отличие от обычного хостинга, каждому клиенту присваивается собственный IP-адрес. Благодаря этому пользователи виртуальных машин не влияют друг на друга и не расходуют чужие ресурсы.</p>
				</div>
			</div>
			
			<div class="hsw-block">
				<h3 class="hsw-block__h">Администрирование и установка ПО</h3>
				<div class="hsw-block__content">
					<p>Управление виртуальным сервером происходит с помощью панели Timeweb. Каждый клиент получает root-доступ к системе, поэтому может устанавливать любой софт по своему желанию, например, VestaCP, BrainyCP, Ubuntu, Debian, CentOS, 1С-Bitrix, Windows. При необходимости вы всегда можете создавать бэкапы данных, используя доступное вам дисковое пространство.</p>
					<p>Timeweb отличается простым и интуитивно понятным интерфейсом. При этом панель обладает широким функционалом и позволяет контролировать все аспекты работы сервера. Если вы никогда раньше не занимались администрированием, наши сотрудники помогут вам разобраться в деталях и всегда будут на связи, если у вас возникнут вопросы. По запросу мы дешево установим все необходимые вам программные решения и настроим их в соответствии с вашими пожеланиями.</p>
				</div>
			</div>
			
			<div class="hsw-block">
				<h3 class="hsw-block__h">ISPmanager</h3>
				<div class="hsw-block__content">
					<p>Это специальная панель управления, которая устанавливается на виртуальные и выделенные серверы. Она позволяет существенно упростить процесс администрирования. Программа платная и распространяется в двух версиях:</p>
					
					<div class="row">
						<div class="col-md-6">
							<p class="bold text-center">Lite</p>
							<ul class="marker">
								<li>функционал для управления DNS, создания и управления почтовыми серверами и другими сервисами;</li>
								<li>не имеет ограничений по количеству пользователей и доменов</li>
							</ul>
						</div>
						<div class="col-md-6">
							<p class="bold text-center">Business</p>	
							<ul class="marker">
								<li>весь функционал Lite версии;</li>
								<li>возможность создания реселлеров;</li>
								<li>сбор статистики об использовании ресурсов сервера;</li>
								<li>возможность хостинга разнообразных сервисов.</li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
			<div class="hsw-block">
				<h3 class="hsw-block__h">Дополнительные услуги</h3>
				<div class="hsw-block__content">
					<ul class="marker">
						<li><span class="bold">40 руб./мес.</span> Цена за каждые следующие 5 ГБ диска VPS/VDS 2.0</li>
						<li><span class="bold">80 руб./мес.</span> Дополнительный IP адрес VPS/VDS</li>
						<li><span class="bold">20 руб./мес.</span> Дополнительный 1 ГБ места для почты</li>
						<li><span class="bold">99 руб./мес.</span> 5 ГБ места для почты</li>
						<li><span class="bold">4 руб./мес.</span> 1 ГБ снапшота (резервной копии)</li>
						<li><span class="bold">900 руб./мес.</span> Увеличение гарантированной скорости на 100 Мбит/c</li>
						<li><span class="bold">2000 руб./мес.</span> Сертификат безопасности Sectigo Positive SSL</li>
						<li><span class="bold">13000 руб./мес.</span> Сертификат безопасности Sectigo Positive Wildcard SSL</li>
					</ul>
				</div>
			</div>
			
		</div>
		
	</div>
		
</section>
<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопрос-ответ</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Защищены ли серверы фаерволом?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Нет, его параметры вы можете настраивать по своему усмотрению.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Могу ли я самостоятельно выбрать конфигурацию сервера?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. Вы можете выбрать отдельные компоненты или же заказать одно из готовых решений. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Даете ли вы гарантии работоспособности оборудования?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. В договоре мы прописываем гарантию аптайма не меньше 99,98% в месяц. Если мы по своей вине превышаем этот показатель, вы получаете компенсацию. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section services-for-you-section dark-blue-bg">
	<div class="container">
		<div class="page-section__h">Услуги для вас</div>
		<div class="sfy-block">
			<div class="row">
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon1.png"></div>
						<a href="/arenda-dedicated_server/">Аренда<br>выделенного<br>сервера</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon2.png"></div>
						<a href="/arenda-vydelennogo-servera-na-windows/">Выделенный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon3.png"></div>
						<a href="/arenda-virtualnogo-vydelennogo-servera-na-windows/">Виртуальный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-4.png"></div>
						<a href="/domain/">Регистрация<br>доменов</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?global $relatedNews; 
$relatedNews = [ "ID" => [13082, 13084, 13288, 13286] ];?>
<div class="articles-block main-new__wrap">
  <div class="container">
    <div class="main-new__title">
      Статьи по теме:
    </div>
    <? $APPLICATION->IncludeComponent(
      "bitrix:news.list",
      "related-articles",
      Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_URL" => "/news/#ELEMENT_CODE#/",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("", ""),
        "FILTER_NAME" => "relatedNews",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "4",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
      ),
      $component
    ); ?>
  </div>
</div>

<section class="page-section custom-service-form-section">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service2020",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального VDS сервера'
      )
    ); ?>
  </div>
</section>
