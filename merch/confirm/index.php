<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Invoices;
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Loc::loadMessages(__FILE__);
global $APPLICATION;
Loader::includeModule('clients');

$APPLICATION->RestartBuffer();

$invoice_id = (int) $_POST['LMI_PAYMENT_NO'];

$invoice = new Invoices($invoice_id);
$arInvoice = $invoice->getById();
if (empty($arInvoice))
{
    die('ERR: Заявки с этим номером не обнаружено');
} else
{
    if ($_POST['LMI_CURRENCY'] != 'RUB' && floatval($_POST['LMI_PAYMENT_AMOUNT']) != floatval($arInvoice['SUMM']['VALUE']))
    {
        die("ERR: неверная сумма " . htmlspecialcharsbx($_POST['LMI_PAYMENT_AMOUNT']));
    }
    $merchant_id = Option::get('clients', 'paymaster_merchant_id');
    if (trim($_POST['LMI_MERCHANT_ID']) != trim($merchant_id))
    {
        die("ERR: НЕВЕРНЫЙ КОШЕЛЕК ПОЛУЧАТЕЛЯ " . htmlspecialcharsbx($_POST['LMI_PAYEE_PURSE']));
    }
    if ($_POST['LMI_PREREQUEST'] == 1)
    {
        die('YES');
    }
    $aArray = array(
        $_POST['LMI_MERCHANT_ID'],
        $_POST['LMI_PAYMENT_NO'],
        $_POST['LMI_SYS_PAYMENT_ID'],
        $_POST['LMI_SYS_PAYMENT_DATE'],
        $_POST['LMI_PAYMENT_AMOUNT'],
        $_POST['LMI_CURRENCY'],
        $_POST['LMI_PAID_AMOUNT'],
        $_POST['LMI_PAID_CURRENCY'],
        $_POST['LMI_PAYMENT_SYSTEM'],
        $_POST['LMI_SIM_MODE']
    );
    $secret = Option::get('clients', 'paymaster_secret', 'hello0My0Money');
    $sArray = implode(';', $aArray) . ';' . $secret;
    $hash = base64_encode(md5($sArray, true));
    if ($_POST['LMI_HASH'] == $hash)
    {
        $status_id = $invoice->setStatus(array('status' => 'paid'));
    }
}
?>