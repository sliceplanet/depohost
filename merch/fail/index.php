<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Ошибка оплаты");
$APPLICATION->SetTitle("Ошибка оплаты");
global $USER;
if ($USER->IsAuthorized())
{
    LocalRedirect('/personal/invoices/?paid=fail');
}
?>
<div class="content">
    <h1><?$APPLICATION->ShowTitle(false);?></h1>
    <?ShowError('Оплата не прошла. Возможно, вы отказались от платежа или возникла другая ошибка');?>
</div>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
