<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Подтверждение оплаты");
$APPLICATION->SetTitle("Подтверждение оплаты");
global $USER;
if ($USER->IsAuthorized())
{
    LocalRedirect('/personal/invoices/?paid=success');
}
?>
<div class="content">
    <h1><?$APPLICATION->ShowTitle(false);?></h1>
    <?  ShowNote('Платеж успешно совершен');
    ?>
</div>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>