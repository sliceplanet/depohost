<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h2 class="subtitle">Преимущества аренды 1С решений</h2>
<ul class="row advantage">
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon calc"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Снижение ваших затрат</div>
      <div class="advantage__item_description">Аренда программных продуктов 1С позволяет экономить не только на покупке, но и на обновлениях ИТС и администрировании</div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon day"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Доступ 24/7</div>
      <div class="advantage__item_description">Безусловное преимущество круглосуточный доступ из любой точки мира, офиса и филиала. домашнего компьютера или коммандировки </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon send"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Бесплатное резервное копирование до 15 дней</div>
      <div class="advantage__item_description">Каждому клиенту предоставляется бесплатно 20 GB места на отдельном сервере для резервных копий с возможностью расширения </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon refresh"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Бесплатное обновление ИТС</div>
      <div class="advantage__item_description">Беря в аренду 1С вы получаете на весь срок пользования программным продуктом подписку ИТС бесплатно </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon shield"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Хранение и защита ваших данных ФЗ 152</div>
      <div class="advantage__item_description">Защита от несанкционированного доступа, передача данных по шифрованным каналам, 3-я и 4-я категория, 4-й класс защиты </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon ipod"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Работа с большой нагрузкой</div>
      <div class="advantage__item_description">Мы знаем как правильно использовать аппаратные комплексы, чтобы рассчитать максимальную производительность, отвечающую &quot;цена/качество&quot; </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon server-5"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Для пользователей Web App ограничение 5 ГБ</div>
      <div class="advantage__item_description">Мы предлагаем больше конкурентов: по 5 ГБ места для одно пользовательских режимов и доступа через Remoute App </div>
    </div>
  </li>
  <li class="col-sm-6 advantage__item">
    <div class="advantage__item_icon server-infinity"></div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Для пользователей VDS нет ограничений</div>
      <div class="advantage__item_description">Для пользователей использующих VDS и выделенный сервер нет ограничений на персональное место на диске</div>
    </div>
  </li>
</ul>
