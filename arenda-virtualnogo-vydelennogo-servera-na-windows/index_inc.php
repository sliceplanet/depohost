<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<section class="page-section section-graybg choose-vvds-section">
	
		<div class="page-section__h">Выберите готовое решение</div>
		<p class="page-section__desc">Готовые решения будут вам предоставлены в течении 15 минут после оплаты.</p>
	
		<div class="container">
			<? $APPLICATION->IncludeComponent(
			  "bitrix:catalog.section.list",
			  "vds-turnkey2020",
			  array(
				"ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
				"CACHE_GROUPS" => "Y",  // Учитывать права доступа
				"CACHE_TIME" => "36000000",  // Время кеширования (сек.)
				"CACHE_TYPE" => "A",  // Тип кеширования
				"COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
				"IBLOCK_ID" => "64",  // Инфоблок
				"IBLOCK_TYPE" => "xmlcatalog",  // Тип инфоблока
				"SECTION_CODE" => "",  // Код раздела
				"SECTION_FIELDS" => "",  // Поля разделов
				"SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
				"SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => "",  // Свойства разделов
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "2",  // Максимальная отображаемая глубина разделов
				"VIEW_MODE" => "LINE"
			  )
			); ?>
			
			<p class="after-solutions-text">
				Компания Artex Telecom предлагает услуги аренды виртуального сервера с ОС Windows для клиентов со всей России. С VPS/VDS вы платите только за те вычислительные мощности, которые используете. Технологии виртуализации обеспечивают возможность гибкого масштабирования ресурсов. В любой момент вы сможете увеличить или уменьшить используемые вычислительные мощности. В цену услуги также включена стоимость лицензии на операционную систему. На виртуальный сервер можно установить любую версию ОС: Windows 7, Windows 10, Windows Server 2008R2, Windows Server 2012, Windows Server 2016, Windows Server 2019. Благодаря этому вы сможете работать в удобной среде со знакомым интерфейсом. 
			</p>

			<div class="bordered-text after-solutions-text" style="margin: 15px auto;max-width: 790px">Управление сервером осуществляется в режиме удаленного рабочего стола по протоколу RDP. Работа с VPS/VDS в этом случае полностью аналогична работе с локальной версией Windows. </div>
			
			
		</div>
</div>
<section class="page-section how-ssl-works-section vvds-osob">
	
	<div class="container">
	
		<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<h2 class="hsw-block__h">Ключевые особенности</h2>
				<div class="hsw-block__content">
					<p>Технология виртуализации VPS позволяет «разделить» физический сервер на несколько виртуальных, доступ к которым предоставляется клиентам. Такой подход позволяет существенно уменьшить стоимость аренды за счет сокращения издержек на покупку нового оборудования. На практике виртуальный сервер управляется так же, как и физическая машина. При этом пользователь получает:</p>
					
					<ul class="ul50p fw500">
						<li><span class="bold">Вычислительные ресурсы, соответствующие выбранному тарифу</span> <br> При этом их объем в любой момент можно изменить с помощью личного кабинета пользователя.</li>
						<li><span class="bold">Собственный статический IP-адрес</span>, <br> неограниченный трафик, высокую скорость соединения, низкий пинг, защиту от DDoS-атак. </li>
						<li><span class="bold">Полный root-доступ</span><br>Благодаря этому клиент может устанавливать на сервер любое программное обеспечение.</li>
						<li><span class="bold">Полную изоляцию от соседей по серверному оборудованию</span><br>Даже если нагрузка на оборудование будет большой, вы всегда сможете получить доступ к ресурсам, за которые платите. </li>
						<li><span class="bold">Профессиональное обслуживание оборудования</span><br>Мы гарантируем аптайм не менее 99,9%. Если не обеспечиваем результат — начисляем компенсацию. </li>
						<li><span class="bold">Регулярные бэкапы данных</span><br>По умолчанию мы делаем резервные копии раз в неделю, но по согласованию можем предоставить вам индивидуальный график. </li>
					</ul>
					
					<div class="bordered-text">В услугу аренды VPS/VDS-сервера уже включена стоимость лицензии выбранной версии Windows.</div>
					
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Ключевые преимущества VPS/VDS серверов</div>
				<div class="hsw-block__content">
				
					<p>Если сравнивать виртуальный сервер с физическим, то можно выделить следующий ряд достоинств:</p>

					<ul class="ul50p fw500">
						<li>Повышенная отказоустойчивость</li>
						<li>Возможность быстрого изменения конфигурации</li>
						<li>Простота создания бэкапов</li>
						<li>Дешевизна обслуживания</li>
						<li>Возможность переноса на другое оборудование</li>
					</ul>
					
				</div>
			</div>
			
			<div class="hsw-block">
				<h2 class="hsw-block__h">Возможности VPS/VDS серверов</h2>
				<div class="hsw-block__content">
					<p>Традиционно виртуальные машины используются для хостинга сайтов и других ресурсов, но этим сфера их применения не ограничивается. VPS также может обслуживать базы данных, FTP, DNS и другие сервисы. Пользователи также получают RDP-доступ и возможность использовать серверы для интернет-торговли и Forex. </p>

					<p>Виртуальные машины также можно использовать для организации удаленных рабочих мест. Доступ к ним можно настроить для любого количества сотрудников, каждый из которых будет иметь доступ ко всем установленным программам и данным или к заранее определенной их части. При этом приобретение дополнительных лицензионных копий ПО не потребуется. </p>

					<p>Отличительной чертой виртуальных серверов является возможность гибкого масштабирования. С помощью личного кабинета вы можете в любой момент получить доступ к дополнительным вычислительным ресурсам или дисковому пространству для хранения файлов. Когда необходимость в добавленной мощности отпадет, вы можете вернуть все показатели к исходным значениям. При этом другие клиенты, размещенные на одном физическом сервере с вами, будут полностью изолированы от вас и не будут претендовать на ваши ресурсы.</p>
				
				</div>
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h">Обращайтесь к нам</div>
				<div class="hsw-block__content">
				
					<p>Есть несколько причин, почему вам стоит арендовать виртуальный сервер VPS/VDS у нас:</p>

					<ul class="ul50p fw500">
						<li>Собственный современный дата-центр в Москве</li>
						<li>Новейшее оборудование с соответствующим уровнем надежности</li>
						<li>Круглосуточное наблюдение за состоянием оборудования</li>
						<li>Надежная защита данных от несанкционированного доступа и взлома</li>
						<li>Квалифицированная техническая поддержка</li>
						<li>Безлимитный трафик</li>
						<li>Широкий спектр предоставляемых услуг</li>
						<li>Оперативное предоставление доступа</li>
						<li>Удобные способы оплаты</li>
					</ul>
					
				</div>
			</div>
			
		</div>
		
	</div>
		
</section>

<section class="faq-block">
    <div class="container">
        <h3 class="hsw-block__h">Вопрос-ответ</h3>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">В чем преимущества Windows VPS перед другими вариантами?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">ОС семейства Windows знакомы большинству пользователей персональных компьютеров. Это значит, что вам не придется привыкать к новому интерфейсу и вы легко освоите все функции, необходимые для хостинга сайта и других задач. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Сколько времени нужно на создание виртуального сервера?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Процесс занимает всего лишь около 5 минут. После этого вы получите доступ и сможете использовать сервер.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Смогу ли я организовать удаленные рабочие места для сотрудников?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, при этом они могут получить доступ к ресурсам сервера с домашних компьютеров. Им будут доступны все программы и файлы или же ограниченная их часть. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section services-for-you-section dark-blue-bg">
	<div class="container">
		<div class="page-section__h">Услуги для вас</div>
		<div class="sfy-block">
			<div class="row">
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon1.png"></div>
						<a href="#">Аренда<br>выделенного<br>сервера</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon2.png"></div>
						<a href="#">Выделенный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon3.png"></div>
						<a href="#">Виртуальный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-4.png"></div>
						<a href="#">Регистрация<br>доменов</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?global $relatedNews; 
$relatedNews = [ "ID" => [13082, 13084, 13288, 13286] ];?>
<div class="articles-block main-new__wrap">
  <div class="container">
    <div class="main-new__title">
      Статьи по теме:
    </div>
    <? $APPLICATION->IncludeComponent(
      "bitrix:news.list",
      "related-articles",
      Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_URL" => "/news/#ELEMENT_CODE#/",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("", ""),
        "FILTER_NAME" => "relatedNews",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "4",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
      ),
      $component
    ); ?>
  </div>
</div>

<section class="page-section custom-service-form-section">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service2020",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального VDS сервера'
      )
    ); ?>
  </div>
</section>
