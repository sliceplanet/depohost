<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Аренда виртуального сервера на Windows");
$APPLICATION->SetPageProperty("description", "У нас вы можете взять в аренду выделенный виртуальный сервер с операционной системой Windows на самых выгодных условиях: минимальные цены, мощное производительное оборудование, профессиональная техподдержка, возможность установки любых  и программ, гарантия безопасности и конфиденциальности данных.");
$APPLICATION->SetPageProperty("keywords_inner", "VDS");
$APPLICATION->SetPageProperty("title", "Аренда виртуального сервера с Windows | Арендовать удаленный виртуальный сервер с операционной системой Windows");
$APPLICATION->SetTitle("Аренда виртуального сервера с Windows");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/sprites/vds.css');
?>

<div id="virtual-vds-page">
	
	<section class="page-section whyssl-section why-vvds">
		
		<div class="page-section__icon">
			<img src="/images/services/2.svg">
		</div>
		<div class="page-section__h">Аренда виртуального сервера с OS Windows</div>
		
		<div class="container">
			
			<div class="wug__wrapper">
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds1.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Аренда виртуального сервера с операционной системой Windows</div>
						<p>Это один из наиболее выгодных вариантов хостинга для молодого бизнеса, когда нет возможности выделить средства на покупку и содержание оборудования.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds2.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Бесплатная настройка<br>и консультации</div>
						<p>Каждый VDS, который вы арендуете, уже настроен. На него также установлена панель управления ISPmanager. Платить за нее не придется. </p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds3.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Высокопроизводительные СХД</div>
						<p>Для виртуализации и облачных решений мы используем высокопроизводительные СХД, HP, Infortrend.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds4.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">15 дней бесплатно</div>
						<p>После оплаты вы получаете 15 дней бесплатного пользования выбранным VDS.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds5.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Аппаратная виртуализация KVM<br>и HUPER-V</div>
						<p>Благодаря им мы можем гарантировать справедливое распределение ресурсов.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds6.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Большой выбор OS </div>
						<p>По вашему заказу мы можем установить на виртуальные серверы Windows, Linux или другую операционную систему. </p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds7.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Круглосуточная поддержка 24/7</div>
						<p>Если вы столкнулись с техническими проблемами или сложностями в работе, наши сотрудники помогут вам разобраться в любое удобное время. </p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/vds8.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Вы приобретаете услуги<br>из первых рук</div>
						<p>Artex Telecom — российский оператор. Мы полагаемся на собственные ЦОДы в Москве, поэтому предлагаем услуги из первых рук.</p>
					</div>
				</div>
				<div class="wug__item">
					<div class="wug__item_left">
						<img src="/images/2020/wug/icon_9.png">
					</div>
					<div class="wug__item_right">
						<div class="wug__item_h">Снижение ваших затрат</div>
						<p>Арендуя виртуальный сервер на Windows, вы экономите средства на покупке, настройке и содержании оборудования.</p>
					</div>
				</div>				
			</div>
			
		</div>
		
	</section>	
	
</div>




<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>