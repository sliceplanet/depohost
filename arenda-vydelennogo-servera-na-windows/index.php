<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "Аренда выделенного сервера на Windows. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("title", "Аренда выделенного сервера на Windows | Взять в аренду удаленный сервер на Windows");
$APPLICATION->SetTitle("Аренда выделенного сервера на Windows");
?>
<?
$step = (int)$_REQUEST['step'];
?>
   <?
   if ($_REQUEST['isAjax'] == 'Y') {
      $APPLICATION->RestartBuffer();
    }
   ?> 
  <?
  if ($step == 2 && $_REQUEST['isAjax'] == 'Y'):
    $APPLICATION->IncludeComponent(
      "itin:dedicated.advance.options",
      "v2020",
      Array(
        "IBLOCK_ID" => "50",
        "IBLOCK_TYPE" => "catalog"
      )
    );
  endif;
  ?>
  <?
  if ($_REQUEST['isAjax'] == 'Y') {
    die();
  }?>
  

<div class="main-page">
	
	
<div class="page-top-desc">
	<div class="container">
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
			</div>
			<h1>АРЕНДА ВЫДЕЛЕННОГО СЕРВЕРА НА WINDOWS</h1>
			<div class="ptd-desc"></div>
			<div class="ptd-content">
<p>Для организаций, работающих с большим количеством сервисов Microsoft, наиболее выгодным и удобным вариантом является сервер на Windows. Благодаря ему можно значительно сэкономить на покупке приложений и иметь доступ к ним из любой точки мира. В компании ArtexTelecom вы можете взять в аренду выделенный сервер на Windows на максимально выгодных для вас условиях. У нас большой выбор готовых решений. А если вы не смогли найти подходящий вариант среди предложенных, менеджер нашей компании поможет подобрать индивидуальное решение. </p>
	
			</div>
		</div>
	</div>
</div>	
	
<section class="page-section wug-section">
	
	<div class="page-section__h">Преимущества выделенного сервера для Windows от ArtexTelecom</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Аренда места на отдельных физических машинах без деления пространства с другими пользователями позволяет значительно сэкономить на покупке лицензий. Кроме того, ПО можно взять в аренду вместе с 1С или любым другим решением.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Физические VDS терминалы нашей компании находятся в защищенном ЦОД. Мы обеспечиваем постоянное резервное копирование данных, благодаря чему вся информация с сайта сохраняется и при внезапных неполадках может быть восстановлена в самые кратчайшие сроки.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Вы получаете надежный сервер, оснащенный процессором с высокой производительностью. Он обеспечивает бесперебойную работу любого сайта независимо от системы управления и нагрузок.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>При необходимости вы можете модернизировать взятый в аренду сервер. Например, если вы решите расширить функционал сайта, добавить больше каталожных позиций или опций, требующих большей мощности, это не составит особого труда.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>При необходимости вы можете модернизировать взятый в аренду сервер. Например, если вы решите расширить функционал сайта, добавить больше каталожных позиций или опций, требующих большей мощности, это не составит особого труда.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>При необходимости вы можете модернизировать взятый в аренду сервер. Например, если вы решите расширить функционал сайта, добавить больше каталожных позиций или опций, требующих большей мощности, это не составит особого труда.</p>
				</div>
			</div>
		</div>
	</div>
	
</section>


<div class="page-section section-graybg">

	<div class="container">
	
		<div class="page-section__h">Аренда выделенного сервера на Windows</div>

		<?$APPLICATION->IncludeComponent( "itin:dedicated.solutions", "v2020", Array( "IBLOCK_ID" => "21", "IBLOCK_TYPE" => "xmlcatalog", "PAGE_COUNT" => "99" ,'SECTION_ID'=>266) );?>
	
	</div>
	
</div>

<section class="page-section wug-section">
	
	<div class="page-section__h">6 причин для сотрудничества с ArtexTelecom</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Мы ценим продолжительное сотрудничество, поэтому предоставляем приятные скидки своим постоянным клиентам. Для получения дополнительной информации обратитесь к нашему менеджеру.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Арендуя выделенный сервер на «Виндовс» в ArtexTelecom, вы получаете бесплатный перенос данных (если у вас уже есть сайт, находящийся на другом сервере), а также профессиональную настройку и консультацию специалиста.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Вы получаете 15 дней бесплатного размещения сайта на нашем выделенном сервере на Windows после внесения аванса за долгосрочное пользование услугой.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Мы не требуем дополнительных комиссий. Вы перечисляете на наш счет только абонентскую плату. В исключительных случаях возможна рассрочка на период до одного месяца.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>При обнаружении ошибки или затруднениях во время работы с панелью администратора вы можете обратиться в нашу техподдержку. Сотрудники сразу же приступят к настройке и проконсультируют вас по любому вопросу.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<p>Беря в ArtexTelecom в аренду сервер на Windows, вы получаете бесплатные интернет-трафик и 100 Гб для хранения резервных копий на внутреннем FTP-сервере. С нами выгодно! </p>
				</div>
			</div>
		</div>
		
		<p>Чтобы взять в аренду удаленный сервер на Windows или приобрести дополнительные опции к нему, свяжитесь с менеджером нашей компании, заполнив форму обратной связи на странице «Контакты». По всем вопросам, касающимся тарифов, возможностей и комплектаций, вы можете обратиться к специалисту. Позвоните по круглосуточному номеру 8 (495) 797-8-500, и менеджер поможет подобрать подходящий по всем параметрам сервер с учетом текущих нагрузок. </p>

		
	</div>
	
</section>

<section class="page-section services-for-you-section dark-blue-bg">
	<div class="container">
		<div class="page-section__h">Услуги для вас</div>
		<div class="sfy-block">
			<div class="row">
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon1.png"></div>
						<a href="/arenda-dedicated_server/">Аренда<br>выделенного<br>сервера</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon2.png"></div>
						<a href="/arenda-vydelennogo-servera-na-windows/">Выделенный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/s2-icon3.png"></div>
						<a href="/arenda-virtualnogo-vydelennogo-servera-na-windows/">Виртуальный<br>сервер на<br>Windows</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-4.png"></div>
						<a href="/domain/">Регистрация<br>доменов</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


  
<div class="help-conf-block nomt">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального выделенного сервера для 1С'
      )
    ); ?>
  </div>
  
</div>


</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>