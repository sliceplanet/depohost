<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Купить SSL-сертификат Geotrust True BusinessID для сайта | SSL-сертификат по недорогой цене в Москве");
$APPLICATION->SetPageProperty("description", "SSL-сертификаты Geotrust True BusinessID. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "SSL-сертификат GeoTrust SSL сертификаты");
$APPLICATION->SetTitle("SSL-сертификаты Geotrust True BusinessID");
CJSCore::Init('jquery');
?>

<div id="ssl-wilcard-page">
	
	<div class="page-top-desc__wrapper">
		<div class="ptd-icon">
			<img src="/images/2020/icon-ssl.png">
		</div>
		<div class="like_h1">Подбор SSL-сертификата Geotrust <br> True BusinessID</div>
		<div class="ptd-content">
			<div class="ptd-content__left">
				<p>Geotrust True BusinessID — это SSL-сертификат от Symantec LLC. Эта корпорация сегодня является одним из мировых лидеров в области защиты данных. Сертификаты Symantec сегодня используются на большинстве сайтов банковских организаций, а также ресурсов, входящих в «ТОП-500 сайтов». Компания Artex Telecom предлагает вам купить SSL-сертификаты True BusinessID для сайта и его поддоменов. У нас вы также можете заказать услугу аренды выделенных и виртуальных серверов на базе дата-центра в Москве.</p>
			</div>
			<div class="ptd-content__right">
				<img src="/images/2020/ssl-geotrust.png">
			</div>
		</div>
	</div>	

	<?global $wildcraftFilter;
	$wildcraftFilter = [ "?NAME" => 'True BusinessID' ];?>

	<?$intSectionID=$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", "filter-ssl-list2020", array(
		"IBLOCK_TYPE" => "ssl",
		"IBLOCK_ID" => "30",
		"SECTION_ID" => "162",
		"SECTION_CODE" => "ssl-sertifikaty",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "wildcraftFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => [
			"PLUS_DOMEN",
			"PLUS_IDN",
			"PLUS_EV",
			"PLUS_WC",
			"PLUS_SGC",
			"PLUS_GR",
			"PLUS_MD",
		],
		"SECTION_URL" => "/ssl-sertifikat/",
		"DETAIL_URL" => "/ssl-sertifikat/#ELEMENT_ID#/",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => [	// Тип цены
			"Розничная",
		],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"TITLE_BLOCK" => "SSL-сертификаты Wildcard"
		),
		$component
	);
	?>	
	
<section class="page-section ssl-what-we-offer-section">
	<div class="container">
		<h2 class="page-section__h">О GeoTrust</h2> 
		<p>С 2001 года продуктами компании воспользовались более 100 000 предприятий из 150 стран мира. Популярность SSL Geotrust True BusinessID обеспечивает высокий уровень надежности, свойственный всем решениям Symantec, а также широкий выбор сертификатов. Продукты компании предназначены исключительно для юридических лиц и требуют проведения проверки. Их защита распространяется сразу на несколько доменов, что особенно актуально для крупных организаций. </p>
		
		<p>К плюсам SSL-сертификатов True BusinessID можно отнести:</p>
		<ul class="ul" style="max-width: 768px; margin-left: auto;margin-right: auto;">
			<li>быструю выдачу за 1-2 дня;</li>
			<li>большое количество доменных имен: по умолчанию 1 сертификат распространяется на 5 имен, но за доплату список можно расширить;</li>
			<li>бесплатная цифровая «печать доверия», которую вы можете разместить на сайте.</li>
		</ul>
		
		<p>Минусами можно назвать достаточно высокую стоимость и длительный процесс выдачи, который может занимать до 7 дней.</p>
		
		<h2 class="page-section__h">Что мы предлагаем?</h2>

		<p>У нас вы можете заказать следующие виды SSL-сертификатов Geotrust True BusinessID:</p>

        <p><span class="bold">Geotrust TrueBusiness ID</span>. Он доступен только юридическим лицам и может устанавливаться на неограниченное число выделенных серверов. К плюсам сертификата можно отнести автоматическую защиту поддомена без www., если при получении был указан домен с www. Сертификат выдается за 1-2 дня после базовой проверки OV (organization validation). </p>
        <p><span class="bold">Geotrust TrueBusiness ID EV</span>. Версия сертификата с расширенной проверкой организации. Срок его выдачи составляет от 5 дней. Для проверки потребуется предоставить документы, подтверждающие юридический и налоговый статус компании, а также подтвердить права на домен. После установки сертификата адрес сайта в браузерах будет выделяться зеленым цветом. По умолчанию SSL защищает поддомены второго уровня с www. и без него. </p>
        <p><span class="bold">Geotrust TrueBusiness ID SAN</span>. Бюджетный вариант сертификата, который обеспечивает защиту до 25 доменных имен. Подходит для Microsoft Exchange или Microsoft Communications Servers. Может устанавливаться как на один, так и на несколько серверов. </p>
        <p><span class="bold">Geotrust True BusinessID WildCard</span>. Сертификат, который выдается юридическим лицам после базовой проверки. Защищает любое количество поддоменов одного уровня. Может устанавливаться на неограниченное количество выделенных серверов без дополнительных лицензий. </p>

		<p>С 2020 года все сертификаты сроком действия на 1 год выдаются на 365 дней, на 2 года — сроком на 13 месяцев с возможностью автоматического перевыпуска. </p>

		
		<div class="ssl-wwo__img">
			<img src="/images/2020/ssl-wildcard-img-1.png" />
		</div>
		
		<p>Также доступно 128/256-битное шифрование, отвечающее всем стандартам в области IT-безопасности. Устанавливая SSL Wildcard, вы получаете знак «Защищено сертификатом», который можете разместить на любой странице вашего сайта.</p>
		
			
		<div class="blue-note">
		<b>Обратите внимание!</b> Как и для любого другого продукта, для правильной установки и полноценного функционирования Wildcard требуется выделенный IP-адрес.
		</div>
	</div>
</section>

<section class="page-section why-artex-telecom">
	
	<div class="container">
	
		<div class="page-section__h">Почему стоит обратиться в ArtexTelecom?</div>
		
		<div class="why-at-items">
			<div class="why-at-item icon_1">Постоянные клиенты Artex Telecom получают специальные предложения и участвуют в бонусной системе. </div>
			<div class="why-at-item icon_2">Мы обеспечиваем прозрачные платежи и предлагаем рассрочку на срок до 30 дней. </div>
			<div class="why-at-item icon_3">Предоставляем техническую поддержку в режиме 24/7/365.</div>
		</div>
	
	</div>
	
</section>
	
<section class="page-section anyq-section">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	

<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопросы и ответы</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Есть ли бесплатные версии SSL Geotrust True BusinessID для защиты сайта?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Нет, таких услуг компания не предоставляет. При необходимости вы можете выбрать другой сертификат. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Для каких сайтов нужен Geotrust True BusinessID SSL-сертификат?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Он подходит для любых сайтов, но наиболее рационально устанавливать его на крупные ресурсы. Если на сайте есть большое количество поддоменов, следует выбрать Wildcard SSL. Мы также можем порекомендовать True BusinessID для предприятий, размещающих несколько сайтов на сервере.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name"> Как работает SSL-сертификат?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Он обеспечивает шифрование данных, которыми обмениваются сайт и пользователь. В этом случае злоумышленники не смогут получить персональную информацию: номера банковских карт, телефоны. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Мой сайт не принимает платежей. Нужен ли мне SSL?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">SSL не только защищает обмен данными между сайтом и посетителем, но и повышает доверие к вашему ресурсу. Ваши пользователи будут знать, что их данные всегда под защитой, а поисковые системы будут размещать ссылку на вас ближе к первой позиции в выдаче.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Смогу ли я самостоятельно установить SSL сертификат на выделенный сервер?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, обычно этот процесс не вызывает сложности. Если же вы столкнулись с проблемой, обратитесь к сотрудникам технической поддержки Artex Telecom. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>		
		
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>