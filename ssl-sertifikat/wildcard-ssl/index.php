<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "SSL-сертификат Wildcard SSL для сайта - купить по недорогой цене");
$APPLICATION->SetPageProperty("description", "⭐⭐⭐⭐⭐SSL-сертификат Wildcard SSL сертификаты для сайта - купить по недорогой цене. ArtexTelecom - предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "SSL-сертификат Wildcard SSL сертификаты");
$APPLICATION->SetTitle("Wildcard SSL-сертификат");
CJSCore::Init('jquery');
?>

<div id="ssl-wilcard-page">
	
	<div class="page-top-desc__wrapper">
		<div class="ptd-icon">
			<img src="/images/2020/icon-ssl.png">
		</div>
		<h1>Подбор SSL-сертификата Wildcard</h1>
		<div class="ptd-content">
			<div class="ptd-content__left">
				<p>SSL Wildcard — сертификат, обеспечивающий защиту основного домена и всех его поддоменов. Он поддерживает безопасное соединение между сайтами и браузерами их пользователей по протоколу https. В компании ArtexTelecom вы можете купить сертификат Wildcard SSL для неограниченного количества поддоменов по разумной цене. Мы предлагаем выгодные условия и большой выбор продукции под любые нужды и предпочтения наших клиентов.</p>
			</div>
			<div class="ptd-content__right">
				<img src="/images/2020/ssl-wildcard.png">
			</div>
		</div>
	</div>	

	<?global $wildcraftFilter;
	$wildcraftFilter = [ "!PROPERTY_PLUS_WC" => false ];?>

	<?$intSectionID=$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", "filter-ssl-list2020", array(
		"IBLOCK_TYPE" => "ssl",
		"IBLOCK_ID" => "30",
		"SECTION_ID" => "162",
		"SECTION_CODE" => "ssl-sertifikaty",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "wildcraftFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => [
			"PLUS_DOMEN",
			"PLUS_IDN",
			"PLUS_EV",
			"PLUS_WC",
			"PLUS_SGC",
			"PLUS_GR",
			"PLUS_MD",
		],
		"SECTION_URL" => "/ssl-sertifikat/",
		"DETAIL_URL" => "/ssl-sertifikat/#ELEMENT_ID#/",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => [	// Тип цены
			"Розничная",
		],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"TITLE_BLOCK" => "SSL-сертификаты Wildcard"
		),
		$component
	);
	?>	
	
<? $APPLICATION->IncludeFile(
	"/include/ssl/wildcard/what-we-offer.php"
  ); ?>
<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопрос-ответ</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Нужен ли Wildcard сертификат для сайта, если я не собираю данные клиентов?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Устанавливать сертификат в этом случае необязательно, но крайне желательно. Его наличие поднимет ваш сайт в выдаче, а пользователям на нем будет более комфортно.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли получить сертификат недорого или бесплатно?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, но в этом случае вы сможете рассчитывать только на базовый функционал. Такие решения предлагает Let's encrypt и ряд других компаний. Или взять в аренду сервер и SSL сертификат Comodo вы получите бесплатно.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">На какой срок выдается сертификат?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Это зависит от политики конкретных организаций. Обычно срок действия SSL кратен одному году. При этом многие компании предлагают существенные скидки при покупке сертификата на длительный срок. Помните, что при повторном оформлении SSL у вас могут повторно запросить документы. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<? $APPLICATION->IncludeFile(
	"/include/ssl/wildcard/why-artex-telecom.php"
  ); ?>	
	
<section class="page-section anyq-section">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	
		
		
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>