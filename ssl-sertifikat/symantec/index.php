<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Купить SSL-сертификат Symantec для сайта | SSL-сертификат по недорогой цене в Москве");
$APPLICATION->SetPageProperty("description", "SSL-сертификаты Symantec. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "SSL-сертификат Symantec SSL сертификаты");
$APPLICATION->SetTitle("SSL-сертификаты Symantec");
CJSCore::Init('jquery');
?>

<div id="ssl-wilcard-page">
	
	<div class="page-top-desc__wrapper">
		<div class="ptd-icon">
			<img src="/images/2020/icon-ssl.png">
		</div>
		<div class="like_h1">Подбор SSL сертификата Symantec</div>
		<div class="ptd-content">
			<div class="ptd-content__left">
				<p>SSL-сертификаты от Symantec — одни из наиболее популярных и востребованных во всем мире. Именно они установлены на большинстве ресурсов, входящих в ТОП-500 сайтов и ТОП-100 банков. Причиной такой популярности можно считать высокий уровень безопасности, продвинутое шифрование данных и большой выбор решений для любого бюджета. Компания Artex Telecom предлагает вам купить и установить на свой сайт SSL-сертификаты Symantec. Мы гарантируем доступные цены и круглосуточную техподдержку, а также предлагает услуги хостинга и аренды серверов в дата-центрах в Москве и Европе. </p>
			</div>
			<div class="ptd-content__right">
				<img src="/images/2020/ssl-symantec.png">
			</div>
		</div>
	</div>	

	<?global $wildcraftFilter;
	$wildcraftFilter = [ "?NAME" => 'Symantec ' ];?>

	<?$intSectionID=$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", "filter-ssl-list2020", array(
		"IBLOCK_TYPE" => "ssl",
		"IBLOCK_ID" => "30",
		"SECTION_ID" => "162",
		"SECTION_CODE" => "ssl-sertifikaty",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "wildcraftFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => [
			"PLUS_DOMEN",
			"PLUS_IDN",
			"PLUS_EV",
			"PLUS_WC",
			"PLUS_SGC",
			"PLUS_GR",
			"PLUS_MD",
		],
		"SECTION_URL" => "/ssl-sertifikat/",
		"DETAIL_URL" => "/ssl-sertifikat/#ELEMENT_ID#/",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => [	// Тип цены
			"Розничная",
		],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"TITLE_BLOCK" => "SSL-сертификаты Wildcard"
		),
		$component
	);
	?>	
	
<section class="page-section ssl-what-we-offer-section">
	<div class="container">
		<h2 class="page-section__h">О компании Symantec</h2> 
		<p>Ее история началась в 1982 году. Долгое время компания занималась проектами, связанными с искусственным интеллектом и базами данных. К 2010 году «Симантек» была лидером в области сетевой безопасности, хранения данных и сетевого менеджмента. В том же году корпорация приобрела часть компании VeriSign, занимающуюся выпуском сертификатов. Включив в себя клиентскую базу, составляющую на тот момент более 3 000 000 компаний, «Симантек» стала крупнейшим центром сертификации в мире.  </p>
		<p>До 2017 года корпорация совершенствовала все основные решения и разрабатывала новые. После конфликта с Google Symantec объявила о продаже компании, занимающейся выпуском SSL-сертификатов. С 2017 года они выпускаются в соответствии с DigiCert Trusted Root TLS. Тем не менее, на сегодняшний день SSL-сертификаты Symantec занимают крупную долю рынка и задают стандарты для других решений.</p>

        <h2 class="page-section__h">Что мы предлагаем?</h2>
        
		<p>Сегодня Symantec оказывает комплексную услугу, в которую входит выдача SSL-сертификата и программное обеспечение для управления ими. В дополнение к этому корпорация дает пользователям возможность использовать сервис для проверки ресурса на наличие вредоносных программ и уязвимостей. </p>
		<p>К преимуществами SSL-сертификатов Symantec можно отнести</p>
		<ul class="ul" style="max-width: 768px;margin: 20px auto;">
			<li>поддержку всех современных браузеров;</li>
			<li>крупные компенсации в случае, если сайт будет взломан;</li>
			<li>поддержку IDN-доменов, содержащих в названии буквы национального алфавита, в том числе кириллицу;</li>
			<li>выдачу печатей доверия Norton Secured. Каждый пользователь Symantec SSL имеет право разместить ее на сайте. Она повышает доверие посетителей и подтверждает факт использования продукта корпорации. </li>
		</ul>

		<p>Symantec предлагает следующие виды SSL-сертификатов:</p>
		<p><span class="bold blue-text">Secure Site Starter Pro</span>. Это сертификат, который выдается после подтверждения компанией прав на данный домен. Проверка проводится центром корпорации «Симантек» на русском языке. Владельцу компании необходимо предоставить пакет документов и подтвердить возможность доступа к домену. Secure Site Pro обеспечивает высокий уровень защиты данных и поддерживает 256-битное шифрование даже в старых браузерах. Сертификат выдается только юридическим лицам и подходит для сайтов, которые собирают и хранят данные пользователей. </p>
		<p><span class="bold blue-text">Secure Site with EV</span>. Этот сертификат обеспечивает максимальную защиту передаваемых между сайтом и клиентом данных. Для его получения необходимо пройти расширенную проверку, в ходе которой необходимо подтвердить существование компании, ее финансовый статус и права на домен. Обычно на это уходит от 5 дней. Сертификаты такого уровня устанавливают на сайты крупных интернет-магазинов, финансовых и государственных организаций. Пользователи этих ресурсов видят в адресной строке уведомление о том, что соединение полностью безопасно. </p>
		<p><span class="bold blue-text">Secure Site Pro with EV</span>. Представляет собой улучшенную версию Secure Site с технологией SGC. Благодаря ей сертификат обеспечивает шифрование данных даже в старых браузерах. Для его получения также необходимо пройти расширенную проверку компании. </p>
		<p>Купить Symantec SSL можно на различные сроки, начиная от 6 месяцев. </p>
		
		<div class="ssl-wwo__img">
			<img src="/images/2020/ssl-wildcard-img-1.png" />
		</div>
		
		<p>Также доступно 128/256-битное шифрование, отвечающее всем стандартам в области IT-безопасности. Устанавливая SSL Wildcard, вы получаете знак «Защищено сертификатом», который можете разместить на любой странице вашего сайта.</p>

	
		<div class="blue-note">
		<b>Обратите внимание!</b> Как и для любого другого продукта, для правильной установки и полноценного функционирования Wildcard требуется выделенный IP-адрес.
		</div>



	</div>
</section>

<section class="page-section why-artex-telecom">
	
	<div class="container">
	
		<div class="page-section__h">Почему стоит обратиться в ArtexTelecom?</div>
		
		<div class="why-at-items">
			<div class="why-at-item icon_1">Постоянные клиенты нашей компании получают бонусы и персональные предложения. </div>
			<div class="why-at-item icon_2">Мы делаем все платежи прозрачными и работаем без скрытых комиссий. При необходимости мы даем клиентам возможность оформить рассрочку платежей до 30 дней.  </div>
			<div class="why-at-item icon_3">Наши клиенты всегда могут обратиться в техническую поддержку и получить консультацию по всем подключенным услугам. </div>
		</div>
	
	</div>
	
</section>
	
<section class="page-section anyq-section">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	
	
<section class="faq-block">
    <div class="container">
        <h3 class="hsw-block__h">Вопросы и ответы</h3>
        <div class="faq-items" itemscope itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question" style="min-height: unset;">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Есть ли у «Симантек» бесплатные сертификаты? </span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Нет, компания не предоставляет таких услуг. Тем не менее, корпорация обязуется вернуть средства и выплатить компенсацию, если ее клиента взломают.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question" style="min-height: unset;">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Wildcard-сертификаты — что это?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Они предназначены для защиты любого количества поддоменов сайта.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question" style="min-height: unset;">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Зачем нужны сертификаты для сайта с максимальной степенью защиты? </span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Они не только обеспечивают полную защиту данных от похищения, но и повышают позицию сайта в поисковой выдаче. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>			
		
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>