<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?/*
<div class="advantage-img-wrap advantage-img-wrap_ssl">
  <h2 class="subtitle">Для чего нужен SSL сертификат</h2>
  <ul class="advantage">
    <li class="advantage__item">
      <div class="advantage__item_icon cart"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Быстрая покупка за 15 минут</div>
        <div class="advantage__item_description"> Оформите заказ, произведите оплату и получите зарегистрированный
          сертификат за 15 минут
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon planet-refresh"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Область применений</div>
        <div class="advantage__item_description"> Веб сайт, интернет магазин, интернет торговля, бизнес приложения,
          почта, сервера приложений с использованием протокола RDP и многое другое
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon shield-big"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Безопасность</div>
        <div class="advantage__item_description"> SSL сертификат гарантирует вам безопасность передачи данных и
          стойкость шифрования в общественных и открытых сетях, обеспечивает защиту обмена данными между клиент/сервером
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Преимущество использования</div>
        <div class="advantage__item_description"> Защита персональных данных, стойкость шифрования, легкость в
          использовании и настройке
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon folder"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Задачи которые призван решать SSL</div>
        <div class="advantage__item_description"> В первую очередь сохранность персональных данных, конфиденциальность и
          гарантию целостности, а так же шифрование
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon paper-check"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Как выбрать SSL сертификат</div>
        <div class="advantage__item_description"> Выберите тип сертификата из списка, используя удобный фильтр, введите
          имя домена и обязательный e-mail (он должен соответствовать предлагаемому шаблону), нажмите Заказать
        </div>
      </div>
    </li>
  </ul>
  <ul class="relink-ssl-certs">
    <li><a href="/ssl-sertifikat/wildcard-ssl/">SSL-сертификат Wildcard</a></li>
    <li><a href="/ssl-sertifikat/comodo/">SSL-сертификат Comodo</a></li>
  </ul>
</div>
*/?>