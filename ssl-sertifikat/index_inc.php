<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>

<?if($APPLICATION->GetCurPage() == "/ssl-sertifikat/"):?>
<?/*
<div class="container">
  <div class="block_text">
    <br>
    <p>
      Для того чтобы полноценно защитить свои данные и предотвратить утечку информации рекомендуется купить SSL сертификат для сайта. Данный вид протокола Secure Sockets Layer обладает огромной популярностью среди миллионов пользователей обладающих своими сайтами. Его применение является полноценной гарантией безопасного взаимодействия
      пользовательского браузера и сервера. В процессе использования закодированная информация передается по HTTPS. При
      поступлении к пользователю раскодировка совершается специальным ключом, каким является этот сертификат.
    </p>
    <p class="text-center">
      <img class="ssl-cert-img-content" alt="ssl сертификат" src="/bitrix/images/news/SSL_Certifficate-696x271.png" title="ssl сертификат"><br>
    </p>
    <h2>В этой программе находится информация:</h2>
    <br>
    <ul>
		<li>Имя домена;</li>
      <li>Юридический владелец;</li>
      <li>Местонахождение;</li>
      <li>Сроки действия;</li>
      <li>Данные о поставщике;</li>
    </ul>
    <p>
      За свою надежность эта уникальная цифровая подпись получила огромное распространение в различных финансовых
      учреждениях. Прекрасно предотвращает несанкционированные действия с конфиденциальной информацией. <br>
    </p>
    <p class="text-center">
      <img class="ssl-cert-img-content" alt="получить ssl сертификат" src="/bitrix/images/news/ssl_Certificate.png"
           title="получить ssl сертификат">
    </p>
    <h2>Как купить ssl сертификат для сайта в artex telecom:</h2>
    <br>
    <ul class="list">
      <li>Обратиться в нашу компанию;</li>
      <li>Выбрать подходящий вариант;</li>
      <li>При необходимости обратиться к нашим консультантам;</li>
      <li>Для некоторых видов, не требуется наличие документов;</li>
    </ul>
    <p>
      Он является подтверждением законной принадлежности домена определенной компании или человеку. <a
          href="https://www.depohost.ru/">У нас на сайте</a>, вы можете найти большой выбор разновидностей ключей, с
      разными уровнями безопасности и цены.<br>
    </p>
    <section class="services dedic-section">
      <div class="subtitle subtitle-custom">
          УСЛУГИ ДЛЯ ВАС
      </div>
      <ul class="services-list-custom">
        <li><a href="/ssl-sertifikat/wildcard-ssl/">SSL-сертификат Wildcard</a></li>
        <li><a href="/ssl-sertifikat/comodo/">SSL-сертификат Comodo</a></li>
        <li><a href="/hosting/">Хостинг</a></li>
        <li><a href="/domain/">Регистрация домена</a></li>
      </ul>
    </section>
  </div>
</div>
<div class="band-blue wrapper wide visible-lg" style="margin-top: 0 !important;"></div>
<section class="section callback_bottom">
  <div class="container">
    <div class="subtitle subtitle-custom text-center">
      Остались вопросы? Оставьте номер телефона и мы подробно на них ответим
    </div>
    <div class="row">
      <?
      $APPLICATION->IncludeComponent(
              "bitrix:form.result.new",
              "new_any-quest-form",
              Array(
                  "SEF_MODE" => "N",
                  "WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
                  "LIST_URL" => "result_list.php",
                  "EDIT_URL" => "result_edit.php",
                  "SUCCESS_URL" => "",
                  "CHAIN_ITEM_TEXT" => "",
                  "CHAIN_ITEM_LINK" => "",
                  "IGNORE_CUSTOM_TEMPLATE" => "Y",
                  "USE_EXTENDED_ERRORS" => "Y",
                  "CACHE_TYPE" => "A",
                  "CACHE_TIME" => "3600",
                  "AJAX_MODE" => "Y",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "N",
                  "AJAX_OPTION_HISTORY" => "N",
                  "SEF_FOLDER" => "/",
                  "VARIABLE_ALIASES" => Array(
                  )
              )
          );?>
    </div>
  </div>
  <?$APPLICATION->IncludeComponent("skobeeff:page.vote", "",[
    "HL_BLOCK_ID" => 3,
    "MAX_VOTE" => 5
  ]);?>
</section>
*/?>
<?endif;?>