<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Купить comodo SSL-сертификат для сайта | SSL-сертификат по недорогой цене в Москве");
$APPLICATION->SetPageProperty("description", "SSL-сертификаты Comodo от ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "SSL-сертификаты Comodo");
$APPLICATION->SetTitle("SSL-сертификаты Comodo");
CJSCore::Init('jquery');
?>

<div id="ssl-wilcard-page">
	
	<div class="page-top-desc__wrapper">
		<div class="ptd-icon">
			<img src="/images/2020/icon-ssl.png">
		</div>
		<div class="like_h1">Подбор SSL-сертификата Comodo</div>
		<div class="ptd-content">
			<div class="ptd-content__left">
				<p>На сегодняшний день SSL-сертификаты Comodo (Sectigo) являются одними из наиболее востребованных во всем мире, а центр сертификации компании занимает порядка 20% от общей доли рынка. Такая популярность связана с большим количеством решений, которые предоставляет корпорация, и высоким уровнем защиты. Компания Artex Telecom предлагает вам купить SSL-сертификаты Comodo для физических и юридических лиц. У нас вы также можете заказать  услуги по аренде серверов на базе дата-центра в Москве. </p>
			</div>
			<div class="ptd-content__right">
				<img src="/images/pages/comodo/comodo.png" style="max-height:170px;">
			</div>
		</div>
	</div>	


	<?global $comodoFilter;
	$comodoFilter = [ "ID" => [530, 537, 12127] ];?>

	<?$intSectionID=$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", "filter-ssl-list2020", array(
		"IBLOCK_TYPE" => "ssl",
		"IBLOCK_ID" => "30",
		"SECTION_ID" => "162",
		"SECTION_CODE" => "ssl-sertifikaty",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "comodoFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => [
			"PLUS_DOMEN",
			"PLUS_IDN",
			"PLUS_EV",
			"PLUS_WC",
			"PLUS_SGC",
			"PLUS_GR",
			"PLUS_MD",
		],
		"SECTION_URL" => "/ssl-sertifikat/",
		"DETAIL_URL" => "/ssl-sertifikat/#ELEMENT_ID#/",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => [	// Тип цены
			"Розничная",
		],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"TITLE_BLOCK" => "SSL-сертификаты Comodo"
		),
		$component
	);
	?>


<section class="page-section ssl-what-we-offer-section">
	<div class="container">
		<h2 class="page-section__h">О Comodo</h2> 
		<p>История компании началась в 1998 году. С момента основания Comodo (Sectigo) Group специализировалась на разработке решений для защиты данных в интернете. Компании принадлежат права на программные комплексы для частных лиц и предприятий, которые сегодня установлены более чем на 25 миллионах устройств по всему миру. </p>
		<p>Comodo (Sectigo) Group также занимается выпуском SSL-сертификатов. Они установлены на 2 миллионах ресурсов по всему миру. Сертификаты имеют различные уровни защиты данных и предоставляются как физическим, так и юридическим лицам. Также существуют решения для цифровой валидации программ. </p>

		<h2 class="page-section__h">Что мы предлагаем?</h2> 
		<p>Comodo (Sectigo) Group выпускает SSL-сертификаты следующих видов:</p>
		<p><span class="bold blue-text">1. Базовые сертификаты</span>. Они обеспечивают стандартный уровень защиты, который подходит для небольших ресурсов, которые не собирают персональные данные и не используются для проведения транзакций. К числу базовых относятся Sectigo (Comodo) Positive SSL и Sectigo (Comodo) Positive SSL Wildcard. Первый обеспечивает защиту одного домена, второй также распространяется на любое количество поддоменов одного уровня. </p>

		<p><span class="bold blue-text">2. OV SSL</span>. Это сертификаты с более высоким уровнем защиты. Они выдаются только юридическим лицам и только после проверки. Для ее прохождения необходимо предоставить данные о компании и подтвердить права на домен. К категории OV относятся Comodo Instant SSL, Sectigo (Comodo) Premium Wildcard SSL и Sectigo (Comodo) Multi-Domain SSL. Первый предназначен для одного домена, второй защищает все поддомены одного уровня, а третий может использоваться для защиты нескольких доменов. </p>

		<p><span class="bold blue-text">3. EV SSL</span>. Это сертификаты, которые выдаются только юридическим лицам после расширенной проверки. Для ее прохождения компании необходимо не только подтвердить свое существование и права на домен, но и предоставить данные о налоговом и юридическом статусе, сфере деятельности и так далее. Проверка может занимать до 7 дней. Сертификаты EV имеют ряд преимуществ перед другими: они не только обеспечивают максимальную защиту, но и обеспечивают сайту зеленую подсветку в адресной строке. К этой категории относятся Sectigo (Comodo) EV SSL и Sectigo (Comodo) EV MDC SSL для защиты одного и нескольких доменов соответственно. </p>

		<p>К плюсам SSL-сертификатов Comodo относятся</p>
		<ul class="ul" style="max-width: 768px; margin-left: auto;margin-right: auto;">
			<li>поддержка подавляющего большинства известных браузеров, включая мобильные;</li>
			<li>оперативная выдача. Она занимает от нескольких минут до нескольких дней в зависимости от типа сертификата;</li>
			<li>доступные цены и положительные отзывы на многих отечественных и зарубежных сайтах;</li>
			<li>гарантия на сумму в 250 000 $;</li>
			<li>высокий уровень безопасности за счет 256-битного шифрования и других  решений.</li>
		</ul>

		<p>В качестве минуса можно выделить невозможность прохождения процедуры сертификации на русском языке. </p>


		
		<div class="ssl-wwo__img">
			<img src="/images/2020/ssl-wildcard-img-1.png" />
		</div>

	</div>
</section>

<section class="page-section why-artex-telecom">
	
	<div class="container">
	
		<div class="page-section__h">Почему стоит обратиться в ArtexTelecom?</div>
		
		<div class="why-at-items">
			<div class="why-at-item icon_1">Мы обеспечиваем особые условия сотрудничества для постоянных клиентов и бонусную программу для всех пользователей наших услуг. </div>
			<div class="why-at-item icon_2">Мы работаем без скрытых платежей, принимаем все виды оплаты и предоставляем рассрочку на срок до 1 месяца.</div>
			<div class="why-at-item icon_3">Вы всегда можете получить техническую поддержку по телефону или с помощью личного кабинета пользователя. </div>
		</div>
	
	</div>
	
</section>
	
<section class="page-section anyq-section">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	
	
<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопросы и ответы</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли установить сертификат Comodo на сервер с Bitrix?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. В этом случае будет достаточно сертификата с базовыми функциями.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Нужен ли SSL для интернет-магазинов?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, так как сайт работает с персональными данными, в том числе информацией о банковских картах. Многие современные браузеры предупреждают пользователя о том, что на сайте без сертификата нельзя оставлять платежные реквизиты. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Какой тип шифрования используется в сертификатах Comodo?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">RSA или ECC</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Есть ли сертификаты для почтовых серверов?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">К ним относится, например, Sectigo Unified Communications Certificate DV. Он подходит для серверов Exchange, сервисов OWA, autodiscover, POP/IMAP.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Для каких сайтов нужен Wildcard SSL?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Его стоит выбрать в случае, если у сайта есть поддомены одного уровня, например, news.site.ru, files.site.ru и так далее. Wildcard позволит существенно упростить администрирование и настройку. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>		
		
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>