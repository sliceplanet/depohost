<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Купить SSL-сертификат Thawte для сайта | SSL-сертификат по недорогой цене в Москве");
$APPLICATION->SetPageProperty("description", "SSL-сертификаты Thawte. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "SSL-сертификат Thawte SSL сертификаты");
$APPLICATION->SetTitle("SSL-сертификаты Thawte");
CJSCore::Init('jquery');
?>

<div id="ssl-wilcard-page">
	
	<div class="page-top-desc__wrapper">
		<div class="ptd-icon">
			<img src="/images/2020/icon-ssl.png">
		</div>
		<div class="like_h1">Подбор SSL сертификата Thawte</div>
		<div class="ptd-content">
			<div class="ptd-content__left">
				<p>Thawte — один из крупнейших и наиболее известных центров сертификации в мире. В России его можно считать одним из наиболее доверенных. Центр Thawte предоставляет различные типы SSL-сертификатов, включая EV с расширенной проверкой на русском языке, для одного домена или поддоменов. Сегодня получить их можно более чем в 200 странах мира. Компания Artex Telecom предлагает вам услуги по установке и настройке Thawte SSL-сертификатов в Москве. Мы гарантируем доступные цены и полную техподдержку.</p>
			</div>
			<div class="ptd-content__right">
				<img src="/images/2020/ssl-thawte.png">
			</div>
		</div>
	</div>	

	<?global $wildcraftFilter;
	$wildcraftFilter = [ "?NAME" => 'Thawte' ];?>

	<?$intSectionID=$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", "filter-ssl-list2020", array(
		"IBLOCK_TYPE" => "ssl",
		"IBLOCK_ID" => "30",
		"SECTION_ID" => "162",
		"SECTION_CODE" => "ssl-sertifikaty",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "wildcraftFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => [
			"PLUS_DOMEN",
			"PLUS_IDN",
			"PLUS_EV",
			"PLUS_WC",
			"PLUS_SGC",
			"PLUS_GR",
			"PLUS_MD",
		],
		"SECTION_URL" => "/ssl-sertifikat/",
		"DETAIL_URL" => "/ssl-sertifikat/#ELEMENT_ID#/",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => [	// Тип цены
			"Розничная",
		],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"TITLE_BLOCK" => "SSL-сертификаты Wildcard"
		),
		$component
	);
	?>	
	
<section class="page-section ssl-what-we-offer-section">
	<div class="container">
		<h2 class="page-section__h">О компании Thawte</h2> 
		<p>Ее история началась в 1995 году. Компания Thawte была основана в Южной Африке и стала пионером в области выдачи SSL-сертификатов компаниям за пределами США. Ближе к концу десятилетия ее доля на рынке приближалась к 50%. В 2000 году Thawte стала частью компании VeriSign. На тот момент число ее клиентов уже превысило 1 миллион. SSL-сертификат Thawte использовались по всей Европе и за ее пределами. Компания одной из первых начала предоставлять цифровые подписи для разработчиков ПО. </p>
		<p>В 2004 году Thawte превратилась в полноценный центр сертификации и первой в мире добавила поддержку IDN доменов.</p>
		<p>В 2010 году VeriSign вошла в состав Symantec и продолжила работу под ее эгидой до спора с компанией Google. В результате разногласий Thawte стала частью DigiCert и с 2017 года выпускает SSL-сертификаты в соответствии с DigiCert Trusted Root TLS.</p>

        <h2 class="page-section__h">Что мы предлагаем?</h2>
        
        <p>Сегодня компания Thawte выпускает SSL следующих видов: </p>
        <p><span class="bold blue-text">SSL123</span>. Базовый DV-сертификат, который подтверждает ваши права на домен сроком на 1 год. Как и другие продукты компании, он доступен как для юридических, так и для физических лиц. Сертификат обеспечивают базовую защиту данных, которой достаточно для внутренних сетей и небольших ресурсов, не собирающих данные пользователей и не работающих с денежными переводами. SSL123 выдается в течение нескольких минут после подтверждения по электронной почте. Дополнительных документов для его получения не требуется. </p>
        <p><span class="bold blue-text">SSL Web Server</span>. Сертификат с более высоким уровнем безопасности, предполагающий проверку компании (OV). Для его получения необходимо подтвердить принадлежность домена и вашей организации. Процесс проверки в Thawte обычно занимает около 1-2 дней. Сертификат может выдаваться только юридическим лицам. Он не только защищает данные, которыми пользователи обмениваются с сайтом, но и повышает доверие к вашему ресурсу посетителей и роботов поисковых систем. </p>
        <p><span class="bold blue-text">SSL Web Server with EV</span>. Thawte SSL с расширенной проверкой организации. Его получение обычно занимает около недели. В ходе проверки заказчику потребуется предоставить ряд документов, подтверждающих существование компании, ее права на домен и так далее. SSL Web Server with EV обеспечивает максимальную степень защиты данных. Кроме того, сайт получает печать защиты Trusted Site Seal, а в адресной строке его адрес подсвечивается зеленым. </p>
        <p><span class="bold blue-text">SSL Web Server WildCard</span>. Это сертификат, предполагающий проверку, аналогичную OV. Его отличием является возможность защиты не только основного сайта, но и любого количества его поддоменов. Такой вариант предпочтителен для крупных ресурсов, так как позволяет сэкономить средства при покупке и время для настройки SSL. Для прохождения проверки потребуется предоставить документы, подтверждающие существование компании. </p>
        <p>Кроме перечисленных выше, Thawte выпускает сертификаты SSL Code Signing для разработчиков программного обеспечения. С его помощью компания может подтвердить оригинальность софта, отсутствие вредоносного кода и свои права на разработку. </p>
		
		<div class="ssl-wwo__img">
			<img src="/images/2020/ssl-wildcard-img-1.png" />
		</div>
		
		<p>Также доступно 128/256-битное шифрование, отвечающее всем стандартам в области IT-безопасности. Устанавливая SSL Wildcard, вы получаете знак «Защищено сертификатом», который можете разместить на любой странице вашего сайта.</p>

	
		<div class="blue-note">
		<b>Обратите внимание!</b> Как и для любого другого продукта, для правильной установки и полноценного функционирования Wildcard требуется выделенный IP-адрес.
		</div>



	</div>
</section>


<section class="page-section why-artex-telecom">
	
	<div class="container">
	
		<div class="page-section__h">Почему стоит обратиться в ArtexTelecom?</div>
		
		<div class="why-at-items">
			<div class="why-at-item icon_1">Мы предлагаем специальные условия сотрудничества и бонусы для постоянных клиентов.</div>
			<div class="why-at-item icon_2">Вы всегда знаете, за что платите — мы не начисляем дополнительных комиссий. При необходимости мы предоставляем клиентам возможность рассрочки платежа на срок до 1 месяца. </div>
			<div class="why-at-item icon_3">Каждый клиент может получить консультацию в службе технической поддержки. У нас вы также можете заказать все необходимые услуги по настройке хостинга и других продуктов нашей компании. </div>
		</div>
	
	</div>
	
</section>	
	
<section class="page-section anyq-section">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	

<section class="faq-block">
    <div class="container">
        <h3 class="hsw-block__h">Вопросы и ответы</h3>
        <div class="faq-items" itemscope itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Обязательно ли покупать Thawte SSL?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Компания не предоставляет бесплатных сертификатов. При необходимости вы можете воспользоваться решениями других сертификационных центров. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Обязателен ли SSL для сайтов?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Нет, но сертификат обеспечивает шифрование данных, которыми клиент обменивается с сайтом. Его наличие также повышает позицию вашего ресурса в выдаче поисковых систем.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Wildcard SSL — что это и зачем он нужен?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Это специальный сертификат, который защищает сам сайт и все его поддомены третьего уровня</span></p>
                </div>
            </div>
        </div>
    </div>
</section>			
		
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>