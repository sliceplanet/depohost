<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Регистрация доменов в зоне .COM за рубежом дешево и без паспорта | Купить доменное имя .COM");
$APPLICATION->SetPageProperty("description", "Регистрация доменов .COM. ArtexTelecom - предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetTitle("Регистрация доменов .COM");
?>
<div class="container">
<p>Регистрация домена .COM — это отличная возможность продвинуть сайт на одно из топовых мест в популярных поисковых системах. Кроме того, имена в этой зоне являются одними из самых востребованных, поэтому целевая аудитория точно запомнит название веб-ресурса. Компания ArtexTelecom предлагает регистрацию и продление срока действия доменов в зоне .COM по одним из самых доступных цен.</p>
<h2 class="text-center">Правила подбора названия сайта</h2>
<p>Доменное имя — одна из самых важных составляющих при создании интернет-представительства бизнеса. Оно должно в полной мере отражать направление деятельности компании или тематику сайта.</p>
<p>Чтобы подобрать подходящее название, ориентируйтесь на домены конкурентов. Благодаря изучению имен сайтов схожей тематики вы сможете создать уникальное и запоминающееся имя.</p>
<p>Не забывайте о том, что домен должен быть кратким и емким. Допускается использование ключевых слов на английском и на русском, написанных латиницей. Например, korean-cosmetics или kosmetika-korea — одни из наиболее подходящих для интернет-магазинов, занимающихся продажей популярной сегодня косметики из Кореи.</p>
<h2 class="text-center">Стоимость регистрации доменного имени .COM</h2>
<p>Чтобы проверить, свободен ли интересующий вас домен, введите желаемое название будущего сайта в форму ниже. Если интересующий вас домен занят, или вы испытываете сложности с выбором имени для веб-представительства своей компании, свяжитесь с нашим специалистом. Он предложит наиболее предпочтительные варианты для сайта любой тематики.</p>
<div style="margin-top: 40px;">
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"domain", 
	array(
		"IBLOCK_TYPE" => "xmlcatalog",
		"IBLOCK_ID" => "25",
		"HIDE_NOT_AVAILABLE" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/domain/com/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "360000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "FILTER",
		"FILTER_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "Розничная",
		),
		"FILTER_VIEW_MODE" => "HORIZONTAL",
		"USE_REVIEW" => "N",
		"USE_COMPARE" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"PRICE_CODE" => array(
			0 => "Розничная",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"SHOW_TOP_ELEMENTS" => "Y",
		"TOP_ELEMENT_COUNT" => "30",
		"TOP_LINE_ELEMENT_COUNT" => "1",
		"TOP_ELEMENT_SORT_FIELD" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "desc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "CMN_5",
			6 => "",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "2",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Заказать",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_ID#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<p>Мы сотрудничаем с аккредитованным регистратором, поэтому наши клиенты получают услугу регистрации доменов по минимальной цене.</p>
<p>При покупке имени сайта в нашей компании вы получаете полный доступ в панель администратора. С ее помощью вы сможете выполнять любые действия, связанные с использованием названия интернет-представительства. Продлевать доменное имя сайта необходимо 1 раз в год. Стоимость услуги может отличаться от указанной на сайте. Более подробно о ценообразовании вы можете узнать у нашего менеджера.</p>
<h2 class="text-center">Условия продления имени сайта </h2>
<p>Если у вас еще нет веб-представительства, мы ждем заявок на регистрацию и <a href="/domain/">продление домена</a>. Также к нам часто обращаются предприниматели и компании, домены сайтов которых были куплены у других регистраторов. Мы открыты к сотрудничеству и готовы продлить срок действия имени интернет-ресурса даже в этом случае. Для того, чтобы воспользоваться услугами ArtexTelecom, выберите нашу компанию в качестве регистратора, заключив типовой договор.</p>
<p>Мы гарантируем бесперебойную работу панели администратора и берем на себя всю ответственность за своевременную техническую поддержку. При возникновении трудностей сразу же свяжитесь с нашим специалистом для получения исчерпывающей консультации. Он предоставит подробную инструкцию по взаимодействию с сайтом через панель управления доменов и поможет продлить срок его действия в случае окончания периода пользования именем.</p>
<p>Если же вопрос находится вне компетенции нашего менеджера, мы передадим вопрос на рассмотрение официальному представителю компании-регистратора. В любом случае ваша проблема будет решена максимально оперативно.</p>
<p><span class="bold">Обратите внимание!</span> Продлить дополнительные домены сайта возможно только в том случае, если основной домен находится на обслуживании у нашего аккредитованного партнера-регистратора.</p> 
<div class="why-choose-us">
    <p class="why-choose-us__title">Почему выбирают компанию ArtexTelecom?</p>
    <div class="why-choose-us__wrapper">
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/podderzka.png" alt="">
            </div>
            <div class="item-text">Бесплатная поддержка DNS</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/registracya.png" alt="">
            </div>
            <div class="item-text">Регистрация в любой зоне</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/individ.png" alt="">
            </div>
            <div class="item-text">Индивидуальность и уникальность доменного имени</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/proverka.png" alt="">
            </div>
            <div class="item-text">Проверка доменного имени сразу в нескольких зонах</div>
        </div>
    </div>
</div>
<p>Зарегистрировать или продлить срок действия домена в зоне .COM с помощью компании ArtexTelecom можно одним из следующих способов:</p>
<ul>
    <li>обратитесь к нашему специалисту, позвонив по телефону 8 (495) 797-8-500;</li>
    <li>заполните онлайн-форму обратной связи на странице «Контакты»;</li>
    <li>закажите обратный звонок.</li>
</ul>
<p>Все вопросы, связанные со стоимостью наших услуг, условиями сотрудничества вы можете задать нашему менеджеру. Он проведет подробную консультацию и поможет с подбором доменного имени.</p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>