<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<?/*
<div class="advantage-img-wrap">

  <h2 class="subtitle">Регистрация доменного имени</h2>

  <ul class="advantage" style="padding-bottom: 30px;">
    <li class="advantage__item">
      <div class="advantage__item_icon www"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Регистрация в любой доменной зоне</div>
        <div class="advantage__item_description">Регистрация доменов в популярных доменных зонах RU, COM, NET, ORG и
          т.д.
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon server-plus"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Бесплатная поддержка DNS</div>
        <div class="advantage__item_description">При регистрации домена вы получаете поддержку NS бесплатно, ресурсные
          записи не ограничены
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon star"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">Возможности</div>
        <div class="advantage__item_description">Подчеркните свою индивидуальность, выберите подходящее доменное имя
        </div>
      </div>
    </li>
    <li class="advantage__item">
      <div class="advantage__item_icon planet"></div>
      <div class="advantage__item_teaser">
        <div class="advantage__item_name">За 1 клик проверка в 7 зонах</div>
        <div class="advantage__item_description"> При проверке доменного имени в одной зоне, автоматически проверяем его
          доступность в других
        </div>
      </div>
    </li>
  </ul>
</div>
<div class="relink">
  <div class="link-item">
    <a href="/domain/com/">Домены зоны .com</a>
  </div>
  <div class="link-item">
    <a href="/domain/ru/">Домены зоны .ru</a>
  </div>
  <div class="link-item">
    <a href="/domain/rf/">Домены зоны .рф</a>
  </div>
  <div class="link-item">
    <a href="/domain/besplatnye/">Бесплатные домены</a>
  </div>
</div>

*/?>