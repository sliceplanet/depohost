<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Регистрация доменов в зоне .РФ | Подобрать, купить или продлить доменное имя для сайта");
$APPLICATION->SetPageProperty("description", "Регистрация доменов .РФ. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "Регистрация доменов .РФ");
$APPLICATION->SetTitle("Регистрация доменов .РФ");
?>
<div class="container">
<p>У любого сайта должно быть уникальное, запоминающееся доменное имя, по которому его смогут найти клиенты. Если ваш веб-ресурс находится на стадии разработки, необходимо продумать все детали, которые могут повлиять на вид домена. Одним из самых популярных и доступных является имя в зоне .РФ. Такой домен внесет существенный вклад в успешное развитие вашего бизнес-проекта и поможет перейти на новый этап в короткие сроки.</p>
<p>В компании ArtexTelecom вы можете заказать услуги регистрации и продления домена .РФ. Закажите ее уже сейчас, ведь чем старше имя сайта, тем больше возможностей продвижения получает ваша компания в Интернете. Даже самая простая страница-лендинг будет постепенно набирать просмотры и значимость для пользователей, что способствует быстрому развитию полноценного многостраничного сайта в будущем.</p> 
<h2 class="text-center">Как грамотно подобрать домен?</h2>
<p>Просто присвоить понравившееся имя сайту недостаточно. Необходимо учесть несколько важных нюансов, которые также включают в себя некоторые законодательные акты. Остановимся на основных правилах без разбора юридической составляющей:</p>
<ol class="text-color">
	<li>Адрес должен максимально точно отражать специфику деятельности вашей компании или тематику сайта.</li>
	<li>Он должен быть коротким, емким и запоминающимся.</li>
	<li>Имя сайта лучше подбирать в соответствии с основным видом предлагаемых товаров или услуг.</li> 
</ol>
<p>Чтобы подобрать подходящий домен, проанализируйте веб-ресурсы конкурентов вашей организации. Вы сможете найти интересные варианты, добавить к ним что-то свое и получить идеальное имя, по которому клиенты смогут найти ваш сайт легко и быстро. Если понравившийся вам домен уже занят, подбирайте синонимичные выражения. Зачастую они выглядят небанально и прочно врезаются в память, что положительно сказывается на узнаваемости в Сети.</p> 
<h2 class="text-center">Сколько стоит регистрация домена?</h2>
<p>В форме ниже вы можете проверить, свободен ли понравившийся вам домен. Введите имя в поисковую строку и нажмите «Проверить».</p>
<div style="margin-top: 40px;">
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"domain", 
	array(
		"IBLOCK_TYPE" => "xmlcatalog",
		"IBLOCK_ID" => "25",
		"HIDE_NOT_AVAILABLE" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/domain/rf/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "360000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "FILTER",
		"FILTER_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "Розничная",
		),
		"FILTER_VIEW_MODE" => "HORIZONTAL",
		"USE_REVIEW" => "N",
		"USE_COMPARE" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"PRICE_CODE" => array(
			0 => "Розничная",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"SHOW_TOP_ELEMENTS" => "Y",
		"TOP_ELEMENT_COUNT" => "30",
		"TOP_LINE_ELEMENT_COUNT" => "1",
		"TOP_ELEMENT_SORT_FIELD" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "desc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "CMN_5",
			6 => "",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "2",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Заказать",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_ID#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<p>У вас возникли сложности с выбором домена? Специалисты ArtexTelecom учтут особенности вашего бизнеса и личные требования к имени сайта и подберут интересный запоминающийся вариант, который будет полностью соответствовать концепции компании и отражать основные моменты ее существования.</p>
<p>При обращении в компанию ArtexTelecom вы получаете полный доступ к функциональной панели управления доменным именем .РФ. В ней вы можете любыми способами взаимодействовать с именами сайта. Продлевать регистрацию домена можно 1 раз в год за максимально приемлемую сумму.</p>
<p>
	<span class="bold">Обратите внимание!</span> Стоимость услуги может отличаться от указанной на данной странице. Подробную информацию о ценообразовании и точной сумме для оплаты уточняйте у нашего менеджера.
</p>
<h2 clas="text-center">Продление доменов</h2>
<p>Если вы хотите продлить срок действия имени для уже работающего сайта, но являетесь клиентом другой компании, вы все равно можете обратиться к нам. Для этого необходимо выбрать ArtexTelecom в качестве регистратора и подписать договор на переход к нам на обслуживание.</p>
<p>Наша организация является партнером аккредитованного регистратора доменов. Поэтому мы можем предложить своим клиентам одни из самых выгодных условий <a href="/domain/">продления регистрации домена</a> для сайта.  Мы обеспечиваем бесперебойное функционирование домена с постоянным доступом клиента в панель управления. При возникновении неполадок, связанных с именем сайта, наши сотрудники сделают все возможное, чтобы восстановить работоспособность вашего веб-ресурса в кратчайшие сроки. Мы предоставим вам подробные инструкции, где будет описаны возможные причины проблемы и способы ее решения. Если вопрос не входит в наши компетенции, специалист передаст его нашим партнерам-регистраторам, которые свяжутся с вами и исправят возникшие неполадки.</p> 
<p><span class="bold">Важно!</span></p>
<p>Продлить дополнительные домены сайта возможно только при наличии основных доменов, находящихся на обслуживании у регистратора-партнера нашей компании.</p> 
<div class="why-choose-us">
    <p class="why-choose-us__title">4 причины для покупки домена в компании ArtexTelecom:</p>
    <div class="why-choose-us__wrapper">
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/podderzka.png" alt="">
            </div>
            <div class="item-text">Бесплатная поддержка DNS</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/registracya.png" alt="">
            </div>
            <div class="item-text">Регистрация в любой зоне</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/individ.png" alt="">
            </div>
            <div class="item-text">Индивидуальность и уникальность доменного имени</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/proverka.png" alt="">
            </div>
            <div class="item-text">Проверка доменного имени сразу в нескольких зонах</div>
        </div>
    </div>
</div>
<p>Чтобы зарегистрировать или продлить срок действия домена в зоне .РФ, свяжитесь с представителем компании ArtexTelecom одним из следующих способов:</p>
    <ul>
		<li>позвоните по телефону 8 (495) 797-8-500;</li>
		<li>заполните форму обратной связи на странице «Контакты»;</li>
		<li>закажите обратный звонок.</li>
	</ul>
<p>Все вопросы, связанные с ценами на доменные имена, условиями сотрудничества, вы можете задать нашему менеджеру</p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>