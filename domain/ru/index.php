<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Дешевые домены .RU для сайта | Подобрать и купить доменное имя в доменной зоне .RU");
$APPLICATION->SetPageProperty("description", "Регистрация доменов .RU. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "Регистрация доменов .RU");
$APPLICATION->SetTitle("Регистрация доменов .RU");
?>
<div class="container">
<p>Регистрация домена в зоне .RU позволит четко позиционировать сайт вашей компании, сделать его имя более запоминающимся для пользователей и подчеркнуть область деятельности. Это отличный способ привлечь аудиторию без лишних затрат. В компании ArtexTelecom вы можете приобрести дешевый домен .RU, а также продлить срок его действия. Если у вас возникли сложности с подбором имени для сайта, обратитесь к менеджеру нашей компании. Он подберет удачное решение для любого направления деятельности.</p>
<h2 class="text-center">Как выбрать правильное имя для вашего сайта?</h2>
<p>Домен должен отвечать не только вашим пожеланиям, но и требованиям законодательства. Выбирайте короткое имя сайта, которое сможет полностью отражать специфику деятельности вашей компании и тематику веб-ресурса. Рекомендуем провести небольшое исследование сайтов ваших конкурентов. Также не забывайте о синонимах ключевых слов. Часто используется написание не только на английском, но и на русском латиницей. Например, интернет-магазины для охоты подбирают домены, содержащие слово hunter или ohota.</p>
<p>Так вы сможете выбрать наиболее емкое и индивидуальное имя для своего сайта.</p> 
<h2 class="text-center">Сколько стоит регистрация домена .RU?</h2>
<p>Вы можете проверить, свободен ли интересующий вас домен. Введите будущее имя вашего сайта в форму ниже.</p>
<p>При покупке домена вы становитесь его полноправным владельцем. Вам предоставляется доступ в панель управления, откуда вы сможете проводить любые манипуляции с сайтом. Продлевать регистрацию домена достаточно 1 раз в год. Мы работаем напрямую с аккредитованным регистратором, поэтому предлагаем максимально низкие цены.</p>
<div style="margin-top: 40px;">
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"domain", 
	array(
		"IBLOCK_TYPE" => "xmlcatalog",
		"IBLOCK_ID" => "25",
		"HIDE_NOT_AVAILABLE" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/domain/ru/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "360000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "FILTER",
		"FILTER_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "Розничная",
		),
		"FILTER_VIEW_MODE" => "HORIZONTAL",
		"USE_REVIEW" => "N",
		"USE_COMPARE" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"PRICE_CODE" => array(
			0 => "Розничная",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"SHOW_TOP_ELEMENTS" => "Y",
		"TOP_ELEMENT_COUNT" => "30",
		"TOP_LINE_ELEMENT_COUNT" => "1",
		"TOP_ELEMENT_SORT_FIELD" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "desc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "CMN_1",
			2 => "CMN_2",
			3 => "CMN_3",
			4 => "CMN_4",
			5 => "CMN_5",
			6 => "",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "CML2_TRAITS",
			4 => "CML2_ATTRIBUTES",
			5 => "CML2_BAR_CODE",
			6 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "2",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Заказать",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_ID#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<p>Точную стоимость продления имени сайта может отличаться от указанной на данной странице. Более подробно о формировании цен на домены вы можете узнать у нашего специалиста.</p>
<h2 clas="text-center">Продление срока использования</h2>
<p>Если у вас уже есть сайт, и его домен обслуживается не в нашей компании, вы все равно можете продлить его у нас. Для этого выберите ArtexTelecom в качестве регистратора и заключите с нами договор на оказание соответствующих услуг.</p>
<p>Мы сотрудничаем с аккредитованным регистратором доменов во многих зонах, в том числе в .RU/.РФ. Благодаря этому мы гарантируем полную функциональность панели управления доменами и несем ответственность за полноценную работу имени сайта. Если у вас возникнут какие-либо проблемы, сразу же обращайтесь к нашему менеджеру. Если мы можем помочь своими силами, он напишет подробную инструкцию, как вернуть домену вашего сайта работоспособность. Если же вопрос находится не в нашей компетенции, сотрудник свяжется с партнером-регистратором для оперативного решения возникших трудностей.</p> 
<p><span class="bold">Обратите внимание!</span></p>
<p>Вы можете использовать дополнительные домены для продления только для доменов, которые находятся на обслуживании у нашего аккредитованного партнера-регистратора.</p> 
<div class="why-choose-us">
    <p class="why-choose-us__title">Преимущества обращения в компанию ArtexTelecom:</p>
    <div class="why-choose-us__wrapper">
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/podderzka.png" alt="">
            </div>
            <div class="item-text">Бесплатная поддержка DNS</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/registracya.png" alt="">
            </div>
            <div class="item-text">Регистрация в любой зоне</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/individ.png" alt="">
            </div>
            <div class="item-text">Индивидуальность и уникальность доменного имени</div>
        </div>
        <div class="why-choose-us__item">
            <div class="item-img">
                <img src="/local/templates/depohost/images/proverka.png" alt="">
            </div>
            <div class="item-text">Проверка доменного имени сразу в нескольких зонах</div>
        </div>
    </div>
</div>
<p>Чтобы купить или продлить доменное имя в зоне .RU с помощью компании ArtexTelecom:</p>
<ul>
	<li>свяжитесь с нашим менеджером по номеру 8 (495) 797-8-500;</li>
	<li>заполните форму обратной связи на странице «Контакты»;</li>
	<li>закажите обратный звонок.</li>
</ul>
<p>Если у вас остались вопросы по стоимости регистрации и продления доменного имени, условиям сотрудничества, обратитесь за консультацией к нашему специалисту.</p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>