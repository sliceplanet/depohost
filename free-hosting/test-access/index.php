<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("KEYWORDS", "Выделенный сервер с тестовым периодом");
$APPLICATION->SetPageProperty("DESCRIPTION", "Выделенный сервер с тестовым периодом");
$APPLICATION->SetPageProperty("tags", "Аренда выделенного сервера - Dedicated");
$APPLICATION->SetPageProperty("NOT_SHOW_TABS", "Y");
$APPLICATION->SetPageProperty("title", "Выделенный сервер с тестовым периодом в Москве | Арендовать выделенный сервер по недорогой цене");
$APPLICATION->SetTitle("Выделенный сервер с тестовым периодом");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.selectbox-m.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic-media.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/select-small.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick-theme.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.prettyCheckboxes.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scripts_bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jQueryUITouchPunch.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');
CJSCore::Init();
$step = (int)$_REQUEST['step'];
?>
   <?
   if ($_REQUEST['isAjax'] == 'Y') {
      $APPLICATION->RestartBuffer();
    }
   ?> 
  <?
  if ($step == 2 && $_REQUEST['isAjax'] == 'Y'):
    $APPLICATION->IncludeComponent(
      "itin:dedicated.advance.options",
      "v2020",
      Array(
        "IBLOCK_ID" => "50",
        "IBLOCK_TYPE" => "catalog",
		'TEST_ACCESS'=>'Y',
      )
    );
	echo '<br><br><br>';
  endif;
  ?>
  <?
  if ($_REQUEST['isAjax'] == 'Y') {
    die();
  }?>


  
<div class="main-page">
	
<div class="page-top-desc vs-top-desc">
	<div class="container">
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/icon_vs.png" />
			</div>
			<h1>Выделенный сервер с тестовым периодом</h1>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>
			<div class="ptd-content">
				<p>Выделенный сервер с тестовым периодом подразумевает предоставление клиенту отдельного полноценного  сервера в дата-центре.
 Это оптимальное решение для размещения существующих и развивающихся проектов, для которых
не будет достаточно ресурсов виртуального сервера, а также в тех случаях, если предъявляются серьезные требования к производительности работы серверного оборудования.</p>
				
			</div>
		</div>
	</div>
</div>


<div class="section-graybg"> 
	
	<div class="container">
		<?$APPLICATION->IncludeComponent( "itin:dedicated.solutions", "v2020", Array( "IBLOCK_ID" => "21", "IBLOCK_TYPE" => "xmlcatalog", "PAGE_COUNT" => "99", 'visible'=>true ,'SECTION_ID'=>278 ) );?>
	</div>

	<? $APPLICATION->IncludeFile(
		"/include/arenda-dedicated_server/wug.php"
	  ); 
	?>

</div>


<section class="page-block services-consist-block services1c-consist-block">
	
	<div class="container">

		<div class="mpb_h">К аренде 1С-сервера предоставляем сервис</div>

		<ul class="services-consist__list">
			<li>Первоначальная настройка сервера, приложений и программ 1С</li> 
			<li>Установка SSL сертификатов на опубликованные ресурсы RDP или HTTPS</li>
			<li>Настройка резервного копирования (локально или на внешний FTP )</li>
			<li>Публикация 1С через Web</li>
			<li>Настройка сервера печати</li> 
			<li>Доступ к серверу по протоколу RDP</li>
			<li>Настройка VPN соединения</li>
			<li>Создание системных пользователей и раздача прав доступа</li>
		</ul>

	</div>

</section>


<section class="page-block advanatages-block-1">
	
	<div class="page-section__h">Аренда удаленного 1С-сервера<br>для вашей компании</div> 
	 
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/1.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это удобно</div>
					<p>Аренда терминального сервера является актуальным IT-решением, для удобной работы в удобном программном окружении. Базы данных компании всегда доступны для сотрудников в любом месте и с любого мобильного, а также стационарного устройства.</p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/2.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это выгодно</div>
					<p>Мы предлагаем выгодные условия аренды. На сегодняшний день это один из наиболее простых способов перехода с коробочной версии 1С-продукта на облачную, с возможностью полного переноса собственных баз данных.</p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/3.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это надежно</div>
					<p>Наш сервер расположен в собственном помещении с обеспечением круглосуточной охраны.</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>

<section class="page-block advanatages-block-2">
	
	<div class="page-section__h">Преимущества аренды сервера у нас</div>
	
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item2">
				<img src="/images/2020/adv1.png" />
				<p>Безопасноe хранение и передача инфо-данных по защищенным каналам в рамках виртуальной сети (VPN)</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv2.png" />
				<p>Круглосуточный доступ к серверу из любого места с возможностью выхода в Интернет</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv3.png" />
				<p>Возможность самостоятельного выбора и установки требуемого ПО</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv4.png" />
				<p>Обеспечение надежной технической поддержки в круглосуточном режиме</p>
				
			</div>
		</div>
		
	</div>
	
</section>
  
<? $APPLICATION->IncludeFile(
	"/include/mainpage/our-tech.php"
  ); ?>
  
  
<div class="help-conf-block">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального выделенного сервера для 1С'
      )
    ); ?>
  </div>
  
</div>


</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>