<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Бесплатный хостинг, SSL сертификаты, домены вы получаете  вместе с арендой сервера");
$APPLICATION->SetPageProperty("keywords", "Бесплатно вы получаете");
$APPLICATION->SetPageProperty("title", "Бесплатно вы получаете вместе с арендой сервера SSL сертификат и домен");
$APPLICATION->SetTitle("Бесплатно вы получаете");
?>
 
<div id="free-page">

	<div class="page-top-desc">
	<div class="page-top-desc__wrapper">
	
		<h1>Бесплатно вы получаете</h1>
		<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>
		<div class="ptd-content">
		
			<div class="pdt-right-icon">
				<img src="/images/2020/icon-gift.png" />
			</div>
			<p>Мы гарантируем надежное и безопасное решение, для организации удобной и простой работы. Приобретая сервер в аренду, компания избавляет себя от необходимости покупать и устанавливать серверное оборудование, а так же получает приятные подарки от нас</p>
			
		</div>
	</div>
	</div>
	
	<div class="section-graybg">
	
		<div class="container">
		
			<div class="what-you-get-block">
				<div class="what-you-get-block__h">При аренде выделенного сервера</div>
				<ul class="ul50p">
					<li>2- IP адреса</li>
					<li>2 SSL сертификата  Comodo PositiveSSL</li>
					<li>2 домена РФ/RU</li>
					<li>Панель управления ISPmanager v5 для Веб сервера бесплатно 1-й месяц</li>
					<li>Установка и переустановка ОС</li>
					<li>Настройка приложений</li>
					<li>Перенос приложений и Веб сайтов</li>
					<li>Установка SSL сертификатов</li>
					<li>Канал 100 Мб/с</li>
					<li>Локальная гигабитная сеть</li>
					<li>KVM/IP или IPMI</li>
					<li>Мониторинг доступности сервера по протоколам HTTP, ICMP с смс  и e-mail уведомлениями	</li>					
				</ul>
			
			</div>
			<div class="what-you-get-block">
				<div class="what-you-get-block__h">При аренде сервера под 1С </div>
				<ul class="ul50p">
					<li>2- IP адреса</li>
					<li>2 SSL сертификата  Comodo PositiveSSL</li>
					<li>2 домена РФ/RU</li>
					<li>Панель управления ISPmanager v5 для Веб сервера бесплатно 1-й месяц</li>
					<li>Установка и переустановка ОС</li>
					<li>Настройка приложений</li>
					<li>Перенос приложений и Веб сайтов</li>
					<li>Установка SSL сертификатов</li>
					<li>Канал 100 Мб/с</li>
					<li>Локальная гигабитная сеть</li>
					<li>KVM/IP или IPMI</li>
					<li>Мониторинг доступности сервера по протоколам HTTP, ICMP с смс  и e-mail уведомлениями	</li>						
				</ul>
			
			</div>
			<div class="what-you-get-block">
				<div class="what-you-get-block__h">При аренде виртуального сервера</div>
				<ul class="ul50p">
					<li>2- IP адреса</li>
					<li>2 SSL сертификата  Comodo PositiveSSL</li>
					<li>2 домена РФ/RU</li>
					<li>Панель управления ISPmanager v5 для Веб сервера бесплатно 1-й месяц</li>
					<li>Установка и переустановка ОС</li>
					<li>Настройка приложений</li>
					<li>Перенос приложений и Веб сайтов</li>
					<li>Установка SSL сертификатов</li>
					<li>Канал 100 Мб/с</li>
					<li>Локальная гигабитная сеть</li>
					<li>KVM/IP или IPMI</li>
					<li>Мониторинг доступности сервера по протоколам HTTP, ICMP с смс  и e-mail уведомлениями	</li>					
				</ul>
			
			</div>
		
		</div>
	
	</div>
	
	<section class="mainpage-block why-we-block">

		<div class="container">

			<div class="mpb_h">Почему вам стоит выбрать именно нас</div>

			<div class="why-we__items">
				<div class="why-we__item wwi1">
					Подготовка и установка сервера в день подачи заявки
				</div>
				<div class="why-we__item wwi2">
					Собственный дата-центр, оборудованный по последнему слову техники
				</div>
				<div class="why-we__item wwi3">
					Широкий спектр предоставляемых услуг
				</div>
				<div class="why-we__item wwi4">
					Комплексное решение поставленных задач
				</div>
				<div class="why-we__item wwi5">
					Профессональная техническая поддержка 24/7
				</div>
				<div class="why-we__item wwi6">
					Надежный и стабильный круглосуточный доступ
				</div>
				<div class="why-we__item wwi7">
					Независимые каналы связи
				</div>
				<div class="why-we__item wwi8">
					Индивидуальный подход<br>к каждому клиенту
				</div>
				<div class="why-we__item wwi9">
					Низкая стоимость услуг, конкурентноспособные цены и скидки
				</div>
			</div>


		</div>

	</section>	
	

<? $APPLICATION->IncludeFile(
	"/include/mainpage/contact-us.php"
  ); ?>


</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>