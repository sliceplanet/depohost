<?php
$MESS['TAB_MAIN_TAB'] = 'Основные настройки';
$MESS['TAB_MAIN_TITLE'] = 'Основные настройки модуля';
$MESS['PARAMS_DEBUG'] = 'Режим отладки';
$MESS['MAIN_SAVE'] = 'Сохранить';
$MESS['PARAMS_FILE_LOG'] = 'Путь и имя к файлу лога';
$MESS['HINT_PARAMS_FILE_LOG'] = 'Путь и имя к файлу лога может быть указан как от корня сайта, так от корня файловой системы сервера';

