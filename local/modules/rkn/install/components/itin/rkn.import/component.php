<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
@set_time_limit(0);
ini_set('memory_limit', '3048M');
@ignore_user_abort(true);
$moduleId = 'rkn';
$settings['dumpdate'] = Option::get($moduleId, 'dumpdate');
$settings['code'] =  Option::get($moduleId, 'code');
$arParams = json_decode(Option::get($moduleId, 'PARAMS'),true);

$bDebug = $arParams['DEBUG'] == 'Y' ? true : false;
$logFilename = strlen($arParams['FILE_LOG'])>0 ? $arParams['FILE_LOG'] : $_SERVER['DOCUMENT_ROOT'].'/local/modules/rkn/logs/response.log';
$logTime = FormatDate('FULL',time());


$soap = new SoapClient('http://vigruzki.rkn.gov.ru/services/OperatorRequest/?wsdl');
$data = $soap->getLastDumpDateEx();

$isValid = false;
if (empty($settings['dumpdate'])) {
    $settings['dumpdate'] = intval($data->lastDumpDate);
    $isValid = true;
} elseif (intval($settings['dumpdate']) < intval($data->lastDumpDate) || intval($settings['dumpdate']) < intval($data->lastDumpDateUrgently)) {
    $settings['dumpdate'] = $data->lastDumpDate;
    $isValid = true;
}
elseif (intval($settings['dumpdate']) < intval($data->lastDumpDateUrgently)) {
    $settings['dumpdate'] = $data->lastDumpDateUrgently;
    $isValid = true;
}

// Если пора делать новый запрос
if ($isValid) {
    $requestFile           = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/modules/rkn/files/Depo_Telecom.xml');
    $signatureFile         = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/modules/rkn/files/Depo_Telecom.xml.sig');
    $params                = new stdClass();
    $params->requestFile   = new SoapVar($requestFile, XSD_BASE64BINARY, 'xsd:base64Binary');
    $params->signatureFile = new SoapVar($signatureFile, XSD_BASE64BINARY, 'xsd:base64Binary');
    $params->dumpFormatVersion = "2.0";
    $data                  = $soap->sendRequest($params);
    if ($data->result) {
        $settings['code'] = $data->code;
    }
    
    $arLog[] = Loc::getMessage('LOG_UPDATE_AVAIBLE');
}
elseif(!$isValid && empty($settings['code']))
{
    $arLog[] = Loc::getMessage('LOG_NO_UPDATE');
}

if (!empty($settings['code'])) {
    $bResponse = false;
    while (!$bResponse)
    {
        sleep(60);
        $params       = new stdClass();
        $params->code = new SoapVar($settings['code'], XSD_STRING, 'xsd:string');
        $data         = $soap->getResult($params);                    
        if ($data->result) {
            $arLog[] = Loc::getMessage('LOG_SUCCESS_RESPONSE');
            $path = $_SERVER['DOCUMENT_ROOT'].'/upload/rkn/';
            if (!(\Bitrix\Main\IO\Directory::isDirectoryExists($path)))
            {
                \Bitrix\Main\IO\Directory::createDirectory($path);
            }            
            $filenName =  $path. md5(microtime(true));        
            file_put_contents($filenName, $data->registerZipArchive);
            $zip = zip_open($filenName);
            $entryContent = '';
            if ($zip) {
                $entry = null;
                do {
                    $entry = zip_read($zip);
                } while ($entry && zip_entry_name($entry) != "dump.xml");
                if ($entry) {
                    zip_entry_open($zip, $entry, "r");
                    $entryContent = zip_entry_read($entry, zip_entry_filesize($entry));
                    zip_entry_close($entry);
                }
                zip_close($zip);
                unlink($filenName);
            }
            if ($entryContent) {
                /** @var SimpleXMLElement $content */
                $entryXML = new SimpleXMLElement($entryContent);
                $table = '';
                
                foreach ($entryXML->children() as $content)
                {                   
                    $ar = array();
                    foreach ($content->children() as $field_name => $field)
                    {                    
                        switch ($field_name)
                        {
                            case 'ip':                        
                            case 'domain':
                            case 'ipSubnet':
                                $ar[$field_name][] = $field;
                                break;
                            case 'url':
                                $url_name = $field;
                                $ar[$field_name][] = '<a href="'.$field.'">'.$url_name.'</a>';
                            default:
                                break;
                        }
                    }
                    $itemAttr = $content->attributes();
                    $includeTime = $itemAttr->includeTime;
                    $includeTime = ConvertTimeStamp(MakeTimeStamp($includeTime,'YYYY-MM-DDTHH:MI:SS'),'FULL');
                    $urgencyType = intval($itemAttr->urgencyType) > 0 ? 'Незамедлительное' : 'Обычное';
                    $urgencyType = iconv(LANG_CHARSET,'utf-8', $urgencyType);
                    $ip = implode('<br>', $ar['ip']);
                    $url = implode('<br>', $ar['url']);
                    $domain = implode('<br>', $ar['domain']);
                    $ipSubnet = implode('<br>', $ar['ipSubnet']);
                    switch ($itemAttr->entryType)
                    {
                        case '1':
                            $entryType = Loc::getMessage('ENTRY_TYPE_VALUE_1');
                            break;
                        case '2':
                            $entryType = Loc::getMessage('ENTRY_TYPE_VALUE_2');
                            break;
                        case '3':
                            $entryType = Loc::getMessage('ENTRY_TYPE_VALUE_3');
                            break;
                        case '4':
                            $entryType = Loc::getMessage('ENTRY_TYPE_VALUE_4');
                            break;
                        default:
                            $entryType = Loc::getMessage('ENTRY_TYPE_VALUE_DEFAULT');
                            break;
                    }
                    
                    $table .= '<tr>';
                    $table .= '<td>' . $content->decision->attributes()->date . '</td>';
                    $table .= '<td>' . $content->decision->attributes()->number . '</td>';
                    $table .= '<td>' . $content->decision->attributes()->org . '</td>';
                    $table .= '<td>' . $domain . '</td>';
                    $table .= '<td width="100">' . $url . '</td>';
                    $table .= '<td>' . $ip . '</td>';
                    $table .= '<td>' . $ipSubnet . '</td>';
                    $table .= '<td>' . $urgencyType  . '</td>';
                    $table .= '<td>' . $includeTime . '</td>';
                    $table .= '<td>' . $entryType . '</td>';
                    $table .= '</tr>';
                    
                }  
                $table = '<table border="1" cellspacing="0" cellpadding="5">'
                            . '<tr>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_DATE')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_NUMBER')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_NUMBER')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_DOMAIN')
                                . '</th>'
                                . '<th width="100">'
                                . Loc::getMessage('HEADER_TABLE_URL')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_IP')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_IP_SUBNET')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_URGENCY')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_INCLUDE_TIME')
                                . '</th>'
                                . '<th>'
                                . Loc::getMessage('HEADER_TABLE_ENTRY_TYPE')
                                . '</th>'
                            . '</tr>' 
                            . CharsetConverter::ConvertCharset($table,'utf-8', LANG_CHARSET)
                        . '</table>';
                
                $table .= Loc::getMessage('TABLE_COMMENT');
                $style = ''
                        . '<style>'
                        . 'table{'
                        . ' border-collapse:collapse'
                        . '}'
                        . '</style>';
                $table = $style.$table;
      
                $fileName = $path.'rkn-'. date('Y-m-d', round($settings['dumpdate'] / 1000)). '.xml';
                $io = new \Bitrix\Main\IO\File($fileName);
                $io->putContents($entryContent);

                // $arFieldsEvent['TABLE_LIST_SITES'] = $table;
                $arFieldsEvent['TABLE_LIST_SITES'] = "";
                $arFieldsEvent['ATTACH'] = $fileName;
                $ob  = new \Bitrix\Main\SiteTable;
                $res = $ob->getList();
                $arSites = array();
                while ($ar = $res->fetch())
                {
                    $arSites[] = $ar['LID'];
                }

                \CEvent::SendImmediate('RKN_SEND_LIST_SITES',$arSites,$arFieldsEvent);
//                mail('terentev_a@it-in.ru', 'выгрузка РКН', 'была выполнена выгрузка РКН'.  date('d.m H:i:s'));
                $settings['code'] = '';
                $bResponse = true;
            }
        }
        else
        {
            $arLog[] = FormatDate('FULL',time()).': '.Loc::getMessage('LOG_FAIL_RESPONSE').(iconv('utf-8', LANG_CHARSET, $data->resultComment));
            $bResponse = false;
        }
    }
    
}
if ($bDebug && strlen($logFilename)>0 && !empty($arLog))
{
    $path = new \Bitrix\Main\IO\Path;
    if (strpos($logFilename,$_SERVER['DOCUMENT_ROOT'])!==0)
    {        
        $logFilename = $path->combine($_SERVER['DOCUMENT_ROOT'],$logFilename);
        
    }
    $messages = $logTime
            ."\r\n"
            ."--------------"
            ."\r\n";
    foreach ($arLog as $message)
    {
        $messages .= $message."\r\n\r\n";
    }
    file_put_contents($logFilename, $messages, FILE_APPEND);  
    echo $messages;
}
foreach ($settings as $k => $v) {
     Option::set($moduleId, $k, $v);
}