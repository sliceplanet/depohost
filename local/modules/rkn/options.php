<?php
if(!$USER->IsAdmin())
	return;

$module_id = "rkn";

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");

$aTabs = array();
$aTabs[] = array("DIV" => "edit_main", "TAB" => Loc::getMessage('TAB_MAIN_TAB'), "ICON"=>"iblock_element", "TITLE"=>Loc::getMessage('TAB_MAIN_TITLE'));
$form = new CAdminForm("rkn_settings", $aTabs);
if (is_array($_REQUEST['PARAMS']))
{
    $arParams = $_REQUEST['PARAMS'];
    $arParams = json_encode($arParams);
    Option::set($module_id, 'PARAMS', $arParams);
}

$temp = Option::get($module_id, 'PARAMS');
if (!empty($temp))
{
    $arParams = json_decode($temp, true);
}
else
{
    $arParams = array();
}

$form->BeginPrologContent();
echo bitrix_sessid_post();
$form->EndPrologContent();
$form->Begin(array('FORM_ACTION'=>$APPLICATION->GetCurPage().'?mid='.htmlspecialcharsbx($mid).'&lang='.LANGUAGE_ID));?>
<?php
foreach ($aTabs as $aTab)
{
    $form->BeginNextFormTab();
    switch ($aTab['DIV']) {
        case 'edit_main':
            $checked = $arParams['DEBUG'] == 'Y' ? true : false;
            $form->AddCheckBoxField('PARAMS[DEBUG]', Loc::getMessage('PARAMS_DEBUG'), false, array('Y','N'), $checked); 
            $value = strlen($arParams['FILE_LOG'])>0 ? $arParams['FILE_LOG'] : $_SERVER['DOCUMENT_ROOT'].'/local/modules/rkn/logs/response.log';
            $form->BeginCustomField('PARAMS[FILE_LOG]', '');?>
                <td class="adm-detail-content-cell-l" width="40%">
                <?
                    ShowJSHint(Loc::getMessage('HINT_PARAMS_FILE_LOG'));
                    echo '&nbsp;';
                    echo Loc::getMessage('PARAMS_FILE_LOG');
                ?>
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" name="PARAMS[FILE_LOG]" value="<?=$value?>" size="150" />
                </td>
             <?                 
            $form->EndCustomField('PARAMS[FILE_LOG]');
            break;
        default:
            break;
    }
}
ob_start();
?>
	
<input type="submit" class="adm-btn-save" name="save" value="<?echo GetMessage("MAIN_SAVE")?>">
<input type="hidden" name="save" value="Y">
<?
$buttons_add_html = ob_get_contents();
ob_end_clean();
$form->Buttons(false,$buttons_add_html);
$form->Show();?>
<?/*?></form><?*/?>
