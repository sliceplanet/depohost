<?php
use Bitrix\Main;
IncludeModuleLangFile(__FILE__);
//$strPath2Lang = str_replace("\\", "/", __FILE__);
//$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
//IncludeModuleLangFile($strPath2Lang."/install.php");

class dedicated extends CModule
{
	var $MODULE_ID = 'dedicated';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	var $errors;

	function dedicated()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("DEDICATED_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("DEDICATED_INSTALL_DESCRIPTION");
	}


	function InstallDB()
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}

		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$arSQLErrors = array();


		return true;
	}

	function InstallEvents()
	{

            $eventManager = Main\EventManager::getInstance();
//            $eventManager->registerEventHandler("", "ClientsServiceOnBeforeUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
//            $eventManager->registerEventHandler("", "ClientsServiceOnBeforeAdd", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');


            return true;
	}

	function UnInstallEvents()
	{
            $eventManager = Main\EventManager::getInstance();
//            $eventManager->unRegisterEventHandler("", "ClientsServiceOnBeforeUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
//            $eventManager->unRegisterEventHandler("", "ClientsServiceOnBeforeAdd", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');


            return true;
	}

	function InstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/install/admin', $_SERVER['DOCUMENT_ROOT']."/bitrix/admin",true,true);
                        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/install/components', $_SERVER['DOCUMENT_ROOT']."/bitrix/components",true,true);
		}
		return true;
	}

	function UnInstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		}
		return true;
	}


	function DoInstall()
	{
		global $APPLICATION, $step, $obModule;
		$step = IntVal($step);
                Main\ModuleManager::registerModule($this->MODULE_ID);

                $this->InstallFiles();
                $this->InstallEvents();
                $this->InstallTasks();
                $obModule = $this;

		$APPLICATION->IncludeAdminFile(GetMessage("DEDICATED_INSTALL_NAME"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/step1.php");
	}

	function DoUninstall()
	{
		global $APPLICATION, $step, $obModule;
		$step = IntVal($step);

		$GLOBALS["CACHE_MANAGER"]->CleanAll();
                $this->UnInstallFiles();
                $this->UnInstallEvents();
                $this->UnInstallTasks();
                $obModule = $this;

                Main\Config\Option::delete($this->MODULE_ID);
		Main\ModuleManager::unRegisterModule($this->MODULE_ID);

                $APPLICATION->IncludeAdminFile(GetMessage("DEDICATED_INSTALL_NAME"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/unstep1.php");

	}

	function GetModuleTasks()
	{
            return array(
//                'clients_deny' => array(
//                    'LETTER' => 'D',
//                    'BINDING' => $this->MODULE_ID,
//                    'TITLE' => '',
//                    'DESCRIPTION' => '',
//                    'OPERATIONS' => array(
//                    )
//                ),
//                'clients_read' => array(
//                    'LETTER' => 'R',
//                    'BINDING' => $this->MODULE_ID,
//                    'TITLE' => '',
//                    'DESCRIPTION' => '',
//                    'OPERATIONS' => array(
//                        'clients_read',
//                        'acts_read',
//                        'ivoices_read',
//                        'bill_read'
//                    )
//                ),
//                'clients_add' => array(
//                    'LETTER' => 'W',
//                    'BINDING' => $this->MODULE_ID,
//                    'TITLE' => '',
//                    'DESCRIPTION' => '',
//                    'OPERATIONS' => array(
//                        'clients_add',
//                        'acts_add',
//                        'ivoices_add',
//                        'bill_add'
//                    )
//                ),
            );
	}
        function GetModuleRightList()
        {
            $arr = array(
            "reference_id" => array("D","R","W"),
            "reference" => array(
                "[D] ".GetMessage("MODULE_DENIED"),
                "[R] ".GetMessage("MODULE_READ"),
                "[W] ".GetMessage("MODULE_WRITE"))
            );
            return $arr;
        }
}
?>