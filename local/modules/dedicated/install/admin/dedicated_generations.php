<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
\Bitrix\Main\Loader::includeModule('dedicated');
$moduleDir = Itin\Depohost\Dedicated::getModuleDir();
require $moduleDir.'/admin/dedicated_generations.php';