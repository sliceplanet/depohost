<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . "/template.php");
// удаляем лишние значения свойств
$arResult['ITEMS'] = array_slice($arResult['ITEMS'], 0, 5, true);
foreach ($arResult['ITEMS'] as $id => $arItem)
{
    foreach ($arItem['PROPERTIES'] as $code => $arValues)
    {
        if ($code == 'CPU' || $code == 'RAID')
        {
            // Оставляем три значения
            $arValues = array_slice($arValues, 0, 3, true);
            $arResult['ITEMS'][$id]['PROPERTIES'][$code] = $arValues;
        }

        if ($code == 'RAID')
        {
            foreach ($arValues as $key => $arValue)
            {
                $arResult['ITEMS'][$id]['PROPERTIES'][$code][$key]['NAME'] = $arValue['BREND'];
                $arResult['ITEMS'][$id]['PROPERTIES'][$code][$key]['DESCRIPTION'] = $arValue['NAME'];
            }
        }
        if ($code == 'HDD')
        {
            // Оставляем по максимальному количеству HDD значения
            $arValues = array_slice($arValues, 0, $arItem['COUNT_HDD'], true);
            $arResult['ITEMS'][$id]['PROPERTIES'][$code] = $arValues;
        }
    }
}
//printr($arResult['PROPERTIES']['RAID']);
foreach ($arResult['PROPERTIES']['RAID'] as $generation_id => $arValues)
{
    foreach ($arValues as $key => $arValue)
    {
        $arResult['PROPERTIES']['RAID'][$generation_id][$key]['NAME'] = $arValue['BREND'];
        $arResult['PROPERTIES']['RAID'][$generation_id][$key]['DESCRIPTION'] = $arValue['NAME'];
    }
}

$arResult['DEFAULT'] = array(
    'COUNT_HDD' => 1,
);
$arResult['FIRST_ITEM'] = array_shift($arResult['ITEMS']);    // first element
?>