<?php
if(!$USER->IsAdmin())
	return;

$module_id = ADMIN_MODULE_NAME;
$CAT_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($SERVER['REQUEST_METHOD']=="GET" && strlen($RestoreDefaults)>0 && $CAT_RIGHT=="W" && check_bitrix_sessid())
{
	COption::RemoveOption($module_id);
	$z = CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
	while($zr = $z->Fetch())
		$APPLICATION->DelGroupRight($module_id, array($zr["ID"]));

	LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANG."&mid=".urlencode($mid));
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");

$aTabs = array();
$aTabs[] = array("DIV" => "edit_rights", "TAB" => Loc::getMessage('TAB_RIGHTS_TAB'), "ICON"=>"iblock_element", "TITLE"=>Loc::getMessage('TAB_RIGHTS_TITLE'));
$tabControl = new CAdminTabControl("tabControl", $aTabs);


$tabControl->Begin();?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?echo LANGUAGE_ID?>">
<?php
echo bitrix_sessid_post();
foreach ($aTabs as $aTab)
{
    $tabControl->BeginNextTab();
    switch ($aTab['DIV']) {
        case 'edit_rights':
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
            break;
        case 'edit_isp':
            require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/panel/options/tabs/isp_settings.php");
            break;
        default:
            break;
    }
}
$tabControl->Buttons();?>
<script type="text/javascript">
function RestoreDefaults()
{
	if (confirm('<? echo GetMessageJS("MAIN_HINT_RESTORE_DEFAULTS_WARNING"); ?>'))
		window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANGUAGE_ID; ?>&mid=<?echo urlencode($mid)?>&<?=bitrix_sessid_get()?>";
}
</script>
<input type="submit" <?if ($CAT_RIGHT<"W") echo "disabled" ?> name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
<input type="hidden" name="Update" value="Y">
<input type="reset" name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
<input type="button" <?if ($CAT_RIGHT<"W") echo "disabled" ?> title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="RestoreDefaults();" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
<?$tabControl->End();?>
</form>




