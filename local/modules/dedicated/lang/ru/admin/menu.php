<?php
$MESS ['main_menu_text'] = "Серверы";
$MESS ['main_menu_title'] = "Серверы";
$MESS ['generator_solution_text'] = "Генератор готовых решений";
$MESS ['generator_solution_title'] = "Генератор готовых решени";
$MESS ['solution_reload_price'] = "Обновление цен готовых решений";
$MESS ['solution_reload_price_title'] = "Обновление цен готовых решени";
$MESS ['configurator_menu_text'] = "Конфигуратор";
$MESS ['configurator_menu_title'] = "Конфигуратор";
$MESS ['hardware_calc_menu_text'] = "Комплектующие калькулятора";
$MESS ['hardware_calc_menu_title'] = "Комплектующие калькулятора";
$MESS ['hardware_solution_menu_text'] = "Комплектующие готовых решений";
$MESS ['hardware_solution_menu_title'] = "Комплектующие готовых решений";
$MESS ['advance_options_menu_text'] = "Дополнительные опции";
$MESS ['advance_options_menu_title'] = "Дополнительные опции";
$MESS ['config_menu_text'] = "Настройки";
$MESS ['config_menu_title'] = "Настройки";
