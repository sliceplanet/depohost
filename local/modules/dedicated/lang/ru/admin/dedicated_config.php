<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$MESS['TAB_DISCOUNT'] = 'Скидки';
$MESS['TAB_DISCOUNT_TITLE'] = 'Размер скидок';
$MESS['TAB_HINT_CALC'] = 'Подсказки калькулятора';
$MESS['TAB_HINT_CALC_TITLE'] = 'Подсказки калькулятора';
$MESS['TAB_HINT_ADVANCE_OPTIONS'] = 'Подсказки доп опций';
$MESS['TAB_HINT_ADVANCE_OPTIONS_TITLE'] = 'Подсказки доп опций';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['FIELDS_NAME_CALC_3M'] = '3 месяца';
$MESS['FIELDS_NAME_CALC_6M'] = '6 месяцев';
$MESS['FIELDS_NAME_CALC_12M'] = '12 месяцев';
$MESS['FIELDS_NAME_SOLUTIONS_3M'] = '3 месяца';
$MESS['FIELDS_NAME_SOLUTIONS_6M'] = '6 месяцев';
$MESS['FIELDS_NAME_SOLUTIONS_12M'] = '12 месяцев';
$MESS['SECTION_SOLUTION_NAME'] = 'Скидки готовых решений, %';
$MESS['SECTION_CALC_NAME'] = 'Скидки калькулятора, %';
$MESS['SAVE'] = 'Сохранить';
$MESS['APPLY'] = 'Применить';
$MESS['CANCEL'] = 'Отменить';
$MESS['FIELDS_NAME_HINT_CALC_TYPE_SERVER'] = 'Вид сервера';
$MESS['FIELDS_NAME_HINT_CALC_GENERATION'] = 'Поколение процессора';
$MESS['FIELDS_NAME_HINT_CALC_CPU'] = 'Частота процессора';
$MESS['FIELDS_NAME_HINT_CALC_RAM'] = 'Оперативная память';
$MESS['FIELDS_NAME_HINT_CALC_RAID'] = 'RAID контролер';
$MESS['FIELDS_NAME_HINT_CALC_HDD'] = 'Жесткие диски';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';
$MESS['PAGE_TITLE'] = 'Настройки';

?>