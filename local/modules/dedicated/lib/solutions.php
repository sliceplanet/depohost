<?php
/**
 * Created by PhpStorm.
 * User: uk1
 * Date: 02.04.2018
 * Time: 18:12
 */

namespace Itin\Depohost;

use Bitrix\Main\Loader;


class Solutions
{
    /**
     * @var array
     * 'CPU'   => int,
     * 'RAID'  => int,
     * 'RAM'   => int,
     * 'HDD' => array
     *
     * 'GENERATION'   => int,
     * 'QUANTITY'   => int,
     * 'ADVANCE'   => string,
     * 'ALT'   => string,
     *
     */
    private $fields;

    private $price_fields = array('CPU','RAM','RAID','HDD');

    private $summ;

    private $name;

    private $catalog_id;


    public function __construct($fields = array()){

        Loader::includeModule('iblock');

        $this->fields = $fields;

    }

    public function setCatalogId($id){

        $this->catalog_id = $id;

    }

    public function pushFields($fields){

        $this->fields = $fields;

    }

    /**
     * @param $id - елемент с ИБ комплектующих калькулятора
     * @return int - цена детали
     */

    private function getDetailPrice($id){

        $price = 0;

        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('ACTIVE' => 'Y', 'ID' => $id);
        $arSelect = array('ID', 'IBLOCK_ID', 'PROPERTY_PRICE');

        $db = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if ($ob = $db->GetNext()) {

            $price = $ob['PROPERTY_PRICE_VALUE'];

        }

        return $price;
    }

    /**
     * @return int|mixed
     */

    public function getSumOfDetail(){

        $price = array();
        $summ = 0;

        foreach($this->fields as $f=>$value){

            if(in_array($f,$this->price_fields)) {

                if ($f == 'HDD' && is_array($value)) {

                    foreach ($value as $hdd) {

                        if (!isset($price[$f][$hdd])) {

                            $price[$f][$hdd] = $this->getDetailPrice($hdd);

                        }

                        $summ += $price[$f][$hdd];

                    }

                } else {

                    $price[$f] = $this->getDetailPrice($value);
                    $summ += $price[$f];

                }
            }

        }

        $this->summ = $summ;

        return $summ;

    }

    public function generateNewSolution(){

        $el = new \CIBlockElement;

        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => $this->catalog_id,
            "IBLOCK_ID"      => 21,
            "NAME"           => $this->getName(),
            "ACTIVE"         => "Y",
            "PROPERTY_VALUES"=> $this->fields,
            'CODE' => $this->getCode()
        );


        if($PRODUCT_ID = $el->Add($arLoadProductArray)){


            $this->setPrice($PRODUCT_ID);

            return $PRODUCT_ID;

        }else{


            return false;

        }

    }

    private function setPrice($id){

        Loader::includeModule('catalog');

        $arFields = array(
            "ID" => $id,
        );
        if(\CCatalogProduct::Add($arFields)){

            $arFields = array(
                "PRODUCT_ID" => $id,
                "CATALOG_GROUP_ID" => 5,
                "PRICE" => $this->summ,
                "CURRENCY" => "RUB",
            );

            $obPrice = new \CPrice();
            $obPrice->Add($arFields);

        }


    }

    public function updatePrice($id){

        Loader::includeModule('catalog');

        $arFields = array(
            "CATALOG_GROUP_ID" => 5,
            "PRICE" => $this->summ,
            "CURRENCY" => "RUB",
        );

        $obPrice = new \CPrice();
        $obPrice->Update($id, $arFields);

    }

    private function getName(){

        $name = '';

        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('ID' => $this->fields['GENERATION']);
        $arSelect = array('ID', 'IBLOCK_ID', 'NAME');

        $db = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if ($ob = $db->GetNext()) {

            $name = $ob['NAME'];

        }

        $this->name = $name;

        return $name;


    }

    private function getCode(){

        $params = Array(
            "max_len" => "100", // обрезает символьный код до 100 символов
            "change_case" => "L", // буквы преобразуются к нижнему регистру
            "replace_space" => "_", // меняем пробелы на нижнее подчеркивание
            "replace_other" => "_", // меняем левые символы на нижнее подчеркивание
            "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
            "use_google" => "false", // отключаем использование google
        );


        return \CUtil::translit($this->name, "ru" , $params).'_'.time();

    }

    public function getAllSolutions(){

        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('IBLOCK_ID' => 21);
        $arSelect = array('ID', 'IBLOCK_ID', 'NAME');

//

        $db = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        $result = array();
        while ($ob = $db->GetNextElement()) {

            $arFields = $ob->GetFields();
            //$prop =
            $prop = $ob->GetProperties();

            foreach ($prop as $k=>$f){

                if(in_array($k,$this->price_fields)){

                    $arFields['PROPERTIES'][$k] = $f['VALUE'];

                }

            }
            $arFields['PRICE'] = $this->getPriceSolution($arFields['ID']);
            $result[] = $arFields;

        }

        return $result;
    }

    private function getPriceSolution($id){

        Loader::includeModule('catalog');

        $price = 0;
        $res = \CPrice::GetList(
            array(),
            array(
                "PRODUCT_ID" => $id,
                "CATALOG_GROUP_ID" => 5
            )
        );

        if ($arr = $res->Fetch()) {

            $price = $arr;

        }

        return $price;

    }

}