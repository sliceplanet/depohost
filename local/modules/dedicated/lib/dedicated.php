<?php

/**
 * Tools and properties dedicated module's
 *
 * @package itin
 * @subpackage depohost
 * @author Alexandr Terentev
 */

namespace Itin\Depohost;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use CIBlock;
use CIBlockElement;

Loc::loadMessages(__FILE__);

class Dedicated
{

    /** @var string directory module's path */
    private $moduleDir;

    /**
     * Return full path to module
     *
     * @return string
     */
    static function getModuleDir()
    {
        $DOCUMENT_ROOT = self::getDocumentRoot();

        return $DOCUMENT_ROOT . '/local/modules/' . ADMIN_MODULE_NAME;
    }

    /**
     * Return DOCUMENT_ROOT
     */
    static function getDocumentRoot()
    {
        $DOCUMENT_ROOT = !empty($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : __DIR__ . "/../../../../";
        return $DOCUMENT_ROOT;
    }

    static function getHardwareIblockIds()
    {
        $arTypes = array('dedicated', 'hardwares_solutions');

        foreach ($arTypes as $type)
        {
            // List iblocks for hardwares
            $arFilter = array(
                '=TYPE' => $type,
                'ACTIVE' => 'Y',
            );
            $res = CIBlock::GetList(array('SORT' => 'ASC'), $arFilter);
            while ($ar = $res->Fetch())
            {

                $arHardware[] = $ar['ID'];
            }
        }
        return $arHardware;
    }

    public static function GetAdminElementEditLink($IBLOCK_ID, $ELEMENT_ID, $arParams = array(), $strAdd = "")
    {
        $hardware_iblock_ids = self::getHardwareIblockIds();
        if (in_array($IBLOCK_ID, $hardware_iblock_ids))
        {
            $url = "dedicated_hardware.php";
        } elseif ($IBLOCK_ID == 50)
        {
            $url = "dedicated_advance_options.php";
        } elseif ($IBLOCK_ID == 21)
        {
            $url = "dedicated_configurator.php";
        }
        $arFilter = array(
            '=ID' => $IBLOCK_ID,
        );
        $res = CIBlock::GetList(array('SORT' => 'ASC'), $arFilter);
        if ($ar = $res->Fetch())
        {
            $type = $ar['IBLOCK_TYPE_ID'];
        }

        $url.= "?IBLOCK_ID=" . intval($IBLOCK_ID);
        $url.= "&type=" . $type;
        if ($ELEMENT_ID !== null)
            $url.= "&ID=" . intval($ELEMENT_ID);
        $url.= "&lang=" . urlencode(LANGUAGE_ID);
        $url.= "&act=el_edit";

        foreach ($arParams as $name => $value)
            if (isset($value))
                $url.= "&" . urlencode($name) . "=" . urlencode($value);

        return $url . $strAdd;
    }

    /*
     * Модифицированный метод получения ссылки списка элементов
     *
     * @rerurn string
     */

    public static function GetAdminSectionListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
    {

        $hardware_iblock_ids = self::getHardwareIblockIds();
        if (in_array($IBLOCK_ID, $hardware_iblock_ids))
        {
            $url = "dedicated_hardware.php";
        } elseif ($IBLOCK_ID == 50)
        {
            $url = "dedicated_advance_options.php";
        } elseif ($IBLOCK_ID == 21)
        {
            $url = "dedicated_configurator.php";
        }
        $arFilter = array(
            '=ID' => $IBLOCK_ID,
        );
        $res = CIBlock::GetList(array('SORT' => 'ASC'), $arFilter);
        if ($ar = $res->Fetch())
        {
            $type = $ar['IBLOCK_TYPE_ID'];
        }

        $url.= "?IBLOCK_ID=" . intval($IBLOCK_ID);
        $url.= "&type=" . $type;
        $url.= "&lang=" . urlencode(LANGUAGE_ID);
        $url.= "&act=sect_list";

        foreach ($arParams as $name => $value)
            if (isset($value))
                $url.= "&" . urlencode($name) . "=" . urlencode($value);

        return $url . $strAdd;
    }

    /*
     * Модифицированный метод получения ссылки списка элементов
     *
     * @return string
     */

    public static function GetAdminElementListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
    {
        $hardware_iblock_ids = self::getHardwareIblockIds();
        if (in_array($IBLOCK_ID, $hardware_iblock_ids))
        {
            $url = "dedicated_hardware.php";
        } elseif ($IBLOCK_ID == 50)
        {
            $url = "dedicated_advance_options.php";
        } elseif ($IBLOCK_ID == 21)
        {
            $url = "dedicated_configurator.php";
        }
        $arFilter = array(
            '=ID' => $IBLOCK_ID,
        );
        $res = CIBlock::GetList(array('SORT' => 'ASC'), $arFilter);
        if ($ar = $res->Fetch())
        {
            $type = $ar['IBLOCK_TYPE_ID'];
        }

        $url.= "?IBLOCK_ID=" . intval($IBLOCK_ID);
        $url.= "&type=" . $type;
        $url.= "&lang=" . urlencode(LANGUAGE_ID);
        $url.= "&act=el_list";

        foreach ($arParams as $name => $value)
            if (isset($value))
                $url.= "&" . urlencode($name) . "=" . urlencode($value);

        return $url . $strAdd;
    }

  /**
   * Возвращает инфу о готовом решении
   *
   * @param int $id ID элемента инфоблока
   * @return array
   * @throws \Bitrix\Main\ArgumentNullException
   * @throws \Bitrix\Main\ArgumentOutOfRangeException
   * @throws \Bitrix\Main\LoaderException
   */
    public static function getSolution($id)
    {
        Loader::includeModule('iblock');
        $arResult = array();
        $base_price_id = 5; // ID base type price

        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('ACTIVE' => 'Y', 'ID' => $id);
        $arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'CATALOG_GROUP_' . $base_price_id, 'PROPERTY_*','DETAIL_PAGE_URL');

        $db = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        if ($ob = $db->GetNextElement())
        {
            $arDiscount = unserialize(Option::get('dedicated', 'discounts_values'));
            $arResult['DISCOUNTS'] = array(
                1 => 0,
                3 => $arDiscount['SOLUTIONS_3M'],
                6 => $arDiscount['SOLUTIONS_6M'],
                12 => $arDiscount['SOLUTIONS_12M'],
            );
            $arFields = $ob->GetFields();
            $arProperties = $ob->GetProperties(array('SORT' => 'ASC'));
            $arPropValues = array();
            $arIds = array();
            foreach ($arProperties as $property_code => $arProperty)
            {
                if (($arProperty['PROPERTY_TYPE'] == 'E' || $property_code == 'HDD') && $property_code != 'GENERATION')
                {
                    // Собираем ID всех значений свойств типа привязка к элементу, чтобы разом достать всю инфу о них
                    if (!is_array($arProperty['VALUE']))
                    {
                        $arProperty['VALUE'] = array($arProperty['VALUE']);
                    }
                    $arIds = array_merge($arProperty['VALUE'], $arIds);
                    $relationCodeProperty[$arProperty['LINK_IBLOCK_ID']] = $property_code;
                }
                $arPropValues[$property_code]['NAME'] = $arProperty['NAME'];
                $arPropValues[$property_code]['VALUE'] = $arProperty['VALUE'];
            }

            $arFilter = array(
                'ID' => array_unique(array_values($arIds)),
            );
            $res_el = CIBlockElement::GetList($arOrder, $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_ID', 'SORT', 'PROPERTY_PRICE'));

            $arProps = array();

            while ($ar_el = $res_el->Fetch())
            {
                if (isset($relationCodeProperty[$ar_el['IBLOCK_ID']]))
                {

                    $code = $relationCodeProperty[$ar_el['IBLOCK_ID']];

                    $arElements[$ar_el['ID']] = $ar_el;
                    $arPropValues[$code]['DISPLAY_VALUE'] = $ar_el['NAME'];
                }
            }

            foreach ($arPropValues as $code => $arProp)
            {

                if (count($arProp['VALUE']) > 1 || $code == 'HDD')
                {
                    $ar_counts = array_count_values($arProp['VALUE']);

                    $arNames = array();
                    foreach ($ar_counts as $element_id => $count)
                    {
                        $arNames[] = $count . " x " . $arElements[$element_id]['NAME'];
                    }
                    // $arPropValues[$code]['DISPLAY_VALUE'] = implode('<br>', $arNames);
					          $arPropValues[$code]['DISPLAY_VALUE'] = implode(', ', $arNames);
					
                }
            }
            // prices
            $arPrices = array();
            $price = (int) $arFields['CATALOG_PRICE_' . $base_price_id];
            foreach ($arResult['DISCOUNTS'] as $count_month => $discount_value)
            {
                $discount = intval($price * $discount_value / 100);
                $arPrices[$count_month] = array(
                    'DISCOUNT' => intval($discount * $count_month),
                    'PRICE' => intval($price - $discount),
                    'SUM' => intval(($price - $discount) * $count_month)
                );
            }

            $arItem = array(
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
                'PROPERTIES' => $arPropValues,
            );
            $arResult = array_merge($arResult, $arItem);

            $arPropertiesForString = array();
            $arProperties = array();
            foreach ($arResult['PROPERTIES'] as $key => $arProperty)
            {
                if (!empty($arProperty['DISPLAY_VALUE']))
                {
                    $arProperties[] = $arProperty['DISPLAY_VALUE'];
//                    if ($key!='GENERATION')
//                    {
                        $arPropertiesForString[] = $arProperty['DISPLAY_VALUE'];
//                    }
                }
            }

            $arResult['CHARACTERISTICS_STRING'] = implode(', ', $arPropertiesForString);
            $arResult['DISPLAY_PROPERTIES'] = $arProperties;
            $arResult['PRICES'] = $arPrices;
            return $arResult;
        }
        else
        {
            $APPLICATION->ThrowException(Loc::getMessage('ERROR_NO_SOLUTION_IN_DATABASE'));
        }
    }

}
