<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

function _ShowUserPropertyField_Custom($name, $property_fields, $values, $bInitDef = false, $bVarsFromForm = false, $max_file_size_show = 50000, $form_name = "form_element", $bCopy = false)
{
    global $bCopy;
    $start = 0;

    if (!is_array($property_fields["~VALUE"]))
        $values = array();
    else
        $values = $property_fields["~VALUE"];
    unset($property_fields["VALUE"]);
    unset($property_fields["~VALUE"]);

    $html = '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb' . md5($name) . '">';
    $arUserType = CIBlockProperty::GetUserType($property_fields["USER_TYPE"]);
    $bMultiple = $property_fields["MULTIPLE"] == "Y" && array_key_exists("GetPropertyFieldHtmlMulty", $arUserType);
    $max_val = -1;
    if (($arUserType["PROPERTY_TYPE"] !== "F") || (!$bCopy))
    {
        if ($bMultiple)
        {
            $html .= '<tr><td>';
            $html .= call_user_func_array($arUserType["GetPropertyFieldHtmlMulty"], array(
                $property_fields,
                $values,
                array(
                    "VALUE" => 'PROP[' . $property_fields["ID"] . ']',
                    "FORM_NAME" => $form_name,
                    "MODE" => "FORM_FILL",
                    "COPY" => $bCopy,
                ),
            ));
            $html .= '</td></tr>';
        }
        else
        {
            foreach ($values as $key => $val)
            {
                if ($bCopy)
                {
                    $key = "n" . $start;
                    $start++;
                }

                if (!is_array($val) || !array_key_exists("VALUE", $val))
                    $val = array("VALUE" => $val, "DESCRIPTION" => "");

                $html .= '<tr><td>';
                if (array_key_exists("GetPropertyFieldHtml", $arUserType))
                    $html .= call_user_func_array($arUserType["GetPropertyFieldHtml"], array(
                        $property_fields,
                        $val,
                        array(
                            "VALUE" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][VALUE]',
                            "DESCRIPTION" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][DESCRIPTION]',
                            "FORM_NAME" => $form_name,
                            "MODE" => "FORM_FILL",
                            "COPY" => $bCopy,
                        ),
                    ));
                else
                    $html .= '&nbsp;';
                $html .= '</td></tr>';

                if (substr($key, -1, 1) == 'n' && $max_val < intval(substr($key, 1)))
                    $max_val = intval(substr($key, 1));
                if ($property_fields["MULTIPLE"] != "Y")
                {
                    $bVarsFromForm = true;
                    break;
                }
            }
        }
    }

    if (!$bVarsFromForm && !$bMultiple)
    {
        $bDefaultValue = is_array($property_fields["DEFAULT_VALUE"]) || strlen($property_fields["DEFAULT_VALUE"]);

        if ($property_fields["MULTIPLE"] == "Y")
        {
            $cnt = IntVal($property_fields["MULTIPLE_CNT"]);
            if ($cnt <= 0 || $cnt > 30)
                $cnt = 5;

            if ($bInitDef && $bDefaultValue)
                $cnt++;
        }
        else
        {
            $cnt = 1;
        }

        for ($i = $max_val + 1; $i < $max_val + 1 + $cnt; $i++)
        {
            if ($i == 0 && $bInitDef && $bDefaultValue)
                $val = array(
                    "VALUE" => $property_fields["DEFAULT_VALUE"],
                    "DESCRIPTION" => "",
                );
            else
                $val = array(
                    "VALUE" => "",
                    "DESCRIPTION" => "",
                );

            $key = "n" . ($start + $i);

            $html .= '<tr><td>';
            if (array_key_exists("GetPropertyFieldHtml", $arUserType))
                $html .= call_user_func_array($arUserType["GetPropertyFieldHtml"], array(
                    $property_fields,
                    $val,
                    array(
                        "VALUE" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][VALUE]',
                        "DESCRIPTION" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][DESCRIPTION]',
                        "FORM_NAME" => $form_name,
                        "MODE" => "FORM_FILL",
                        "COPY" => $bCopy,
                    ),
                ));
            else
                $html .= '&nbsp;';
            $html .= '</td></tr>';
        }
        $max_val += $cnt;
    }
    if (
            $property_fields["MULTIPLE"] == "Y" && $arUserType["USER_TYPE"] !== "HTML" && $arUserType["USER_TYPE"] !== "employee" && !$bMultiple
    )
    {
        $html .= '<tr><td><input type="button" value="' . GetMessage("IBLOCK_AT_PROP_ADD") . '" onClick="addNewRow(\'tb' . md5($name) . '\')"></td></tr>';
    }
    $html .= '</table>';
    echo $html;
}

function _ShowPropertyField_Custom($name, $property_fields, $values, $bInitDef = false, $bVarsFromForm = false, $max_file_size_show = 50000, $form_name = "form_element", $bCopy = false)
{
    $type = $property_fields["PROPERTY_TYPE"];
    if ($property_fields["USER_TYPE"] != "")
        _ShowUserPropertyField_Custom($name, $property_fields, $values, $bInitDef, $bVarsFromForm, $max_file_size_show, $form_name, $bCopy);
    elseif ($type == "L") //list property
        _ShowListPropertyField($name, $property_fields, $values, $bInitDef);
    elseif ($type == "F") //file property
        _ShowFilePropertyField($name, $property_fields, $values, $max_file_size_show, $bVarsFromForm);
    elseif ($type == "G") //section link
    {
        if (function_exists("_ShowGroupPropertyField_custom"))
            _ShowGroupPropertyField_custom($name, $property_fields, $values, $bVarsFromForm);
        else
            _ShowGroupPropertyField($name, $property_fields, $values, $bVarsFromForm);
    }
    elseif ($type == "E") //element link
    {
        _ShowElementPropertyField($name, $property_fields, $values, $bVarsFromForm);
    } else
        _ShowStringPropertyField($name, $property_fields, $values, $bInitDef, $bVarsFromForm);
}
