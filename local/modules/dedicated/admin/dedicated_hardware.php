<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;
use \CIBlock as CIBlock;

Loader::includeModule('dedicated');
$moduleDir = Dedicated::getModuleDir();
include_once $moduleDir.'/include/denied.php';
switch ($_REQUEST['act'])
{
    case 'el_edit':
        require __DIR__.'/admin_pages/servers/hardware/elements_edit.php';
//        require Dedicated::getDocumentRoot().BX_ROOT."/modules/iblock/admin/iblock_element_edit.php";
        break;

    default:
        require __DIR__.'/admin_pages/servers/hardware/elements_list.php';
        break;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");