<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($moduleDir . '/admin/dedicated_config.php');
Loader::includeModule('iblock');
$arFilter = array(
    'IBLOCK_ID'=> 50
);
$res = CIBlockSection::GetList(array('SORT'=>'ASC','ID'=>'ASC'), $arFilter);
$values = array();
while($ar = $res->Fetch())
{
    $arFields['SECTIONS'][$ar['ID']] = $ar['NAME'];
    $values[$ar['ID']] = $ar['DESCRIPTION'];
}

if (!empty($_REQUEST['FIELDS']['SECTIONS']) && is_array($_REQUEST['FIELDS']['SECTIONS']))
{
    $values = $_REQUEST['FIELDS']['SECTIONS'];
    $ob = new CIBlockSection;
    foreach($values as $id => $value)
    {
        $ob->Update($id,array('DESCRIPTION'=>$value));
    }
}


foreach ($arFields['SECTIONS'] as $key => $name)
{
    if (strpos($key, 'SECTION_') === 0)
    {
        $tabControl->AddSection($key, $name, false);
    } else
    {
        $tabControl->AddTextField('FIELDS[SECTIONS][' . $key . ']', $name . ':', $values[$key], array('cols'=>100, 'rows'=>10), false);
    }
}

?>