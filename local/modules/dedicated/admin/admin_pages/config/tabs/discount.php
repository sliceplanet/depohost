<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($moduleDir . '/admin/dedicated_config.php');
$arFields = array(
    'SECTION_CALC' => Loc::getMessage('SECTION_CALC_NAME'),
    'CALC_3M' => Loc::getMessage('FIELDS_NAME_CALC_3M'),
    'CALC_6M' => Loc::getMessage('FIELDS_NAME_CALC_6M'),
    'CALC_12M' => Loc::getMessage('FIELDS_NAME_CALC_12M'),
    'SECTION_SOLUTION' => Loc::getMessage('SECTION_SOLUTION_NAME'),
    'SOLUTIONS_3M' => Loc::getMessage('FIELDS_NAME_SOLUTIONS_3M'),
    'SOLUTIONS_6M' => Loc::getMessage('FIELDS_NAME_SOLUTIONS_6M'),
    'SOLUTIONS_12M' => Loc::getMessage('FIELDS_NAME_SOLUTIONS_12M'),
);
$default_values = array(
    'CALC_3M' => 3,
    'CALC_6M' => 5,
    'CALC_12M' => 8,
    'SOLUTIONS_3M' => 3,
    'SOLUTIONS_6M' => 5,
    'SOLUTIONS_12M' => 8,
);

$values = array();

if (!empty($_REQUEST['FIELDS']['DISCOUNTS']) && is_array($_REQUEST['FIELDS']['DISCOUNTS']))
{
    $values = $_REQUEST['FIELDS']['DISCOUNTS'];
    Option::set(ADMIN_MODULE_NAME, 'discounts_values', serialize($values));
}

$values = unserialize(Option::get(ADMIN_MODULE_NAME, 'discounts_values', serialize($default_values)));

foreach ($arFields as $key => $name)
{
    if (strpos($key, 'SECTION_') === 0)
    {
        $tabControl->AddSection($key, $name, false);
    } else
    {
        $tabControl->AddEditField('FIELDS[DISCOUNTS][' . $key . ']', $name . ':', false, array(), $values[$key]);
    }
}
?>