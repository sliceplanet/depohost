<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($moduleDir . '/admin/dedicated_config.php');
$arFields = array(
    'HINT_CALC_TYPE_SERVER' => Loc::getMessage('FIELDS_NAME_HINT_CALC_TYPE_SERVER'),
    'HINT_CALC_GENERATION' => Loc::getMessage('FIELDS_NAME_HINT_CALC_GENERATION'),
    'HINT_CALC_CPU' => Loc::getMessage('FIELDS_NAME_HINT_CALC_CPU'),
    'HINT_CALC_RAM' => Loc::getMessage('FIELDS_NAME_HINT_CALC_RAM'),
    'HINT_CALC_RAID' => Loc::getMessage('FIELDS_NAME_HINT_CALC_RAID'),
    'HINT_CALC_HDD' => Loc::getMessage('FIELDS_NAME_HINT_CALC_HDD'),
);
$default_values = array(
    'HINT_CALC_TYPE_SERVER' => "",
    'HINT_CALC_GENERATION' => "",
    'HINT_CALC_CPU' => "",
    'HINT_CALC_RAM' => "",
    'HINT_CALC_RAID' => "",
    'HINT_CALC_HDD' => "",
);

$values = array();

if (!empty($_REQUEST['FIELDS']['HINTS_CALC']) && is_array($_REQUEST['FIELDS']['HINTS_CALC']))
{
    $values = $_REQUEST['FIELDS']['HINTS_CALC'];
    foreach ($values as $key => $value)
    {
        Option::set(ADMIN_MODULE_NAME, strtolower($key), $value);
    }

}
foreach ($default_values as $key => $value)
{
    $values[$key] = Option::get(ADMIN_MODULE_NAME, strtolower($key), $value);
}


foreach ($arFields as $key => $name)
{
    if (strpos($key, 'SECTION_') === 0)
    {
        $tabControl->AddSection($key, $name, false);
    } else
    {
        $tabControl->AddTextField('FIELDS[HINTS_CALC][' . $key . ']', $name . ':', $values[$key], array('cols'=>100, 'rows'=>10), false);
    }
}
?>