<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

use Bitrix\Iblock\InheritedProperty\ElementTemplates;
use Bitrix\Iblock\InheritedProperty\ElementValues;
use Bitrix\Iblock\Template\Helper;
use Itin\Depohost\Dedicated;

CJSCore::Init(array('jquery'));

CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
require(Dedicated::getModuleDir() . "/admin/admin_tools.php");


/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

$io = CBXVirtualIo::GetInstance();

/* Change any language identifiers carefully */
/* because of user customized forms! */
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/iblock/admin/iblock_element_edit_compat.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/iblock/admin/iblock_element_edit.php");
IncludeModuleLangFile(__FILE__);


$IBLOCK_ID = 0;
if (isset($_REQUEST['IBLOCK_ID']))
    $IBLOCK_ID = (int) $_REQUEST["IBLOCK_ID"]; //information block ID
define("MODULE_ID", "iblock");
define("ENTITY", "CIBlockDocument");
define("DOCUMENT_TYPE", "iblock_" . $IBLOCK_ID);

/* autocomplete */
$strLookup = '';
if (isset($_REQUEST['lookup']))
    $strLookup = preg_replace("/[^a-zA-Z0-9_:]/", "", $_REQUEST["lookup"]);
if ('' != $strLookup)
{
    define('BT_UT_AUTOCOMPLETE', 1);
}
$bAutocomplete = defined('BT_UT_AUTOCOMPLETE') && (BT_UT_AUTOCOMPLETE == 1);

/* property ajax */
$bPropertyAjax = (isset($_REQUEST["ajax_action"]) && $_REQUEST["ajax_action"] === "section_property");

$strWarning = '';
$bVarsFromForm = false;

$ID = 0;
if (isset($_REQUEST['ID']))
    $ID = (int) $_REQUEST['ID']; //ID of the persistent record

    /* copy element */
$bCopy = false;
$copyID = 0;
if (!$bAutocomplete)
{
    $bCopy = (isset($_REQUEST['action']) && $_REQUEST["action"] == "copy");
}
$copyID = (int) (isset($_REQUEST['copyID']) ? $_REQUEST['copyID'] : 0);

if ($ID <= 0 && intval($PID) > 0)
    $ID = intval($PID);

$PREV_ID = intval($PREV_ID);

$WF_ID = $ID; //This is ID of the current copy

$arShowTabs = array(
    'edit_rights' => false,
    'sections' => false,
    'catalog' => false,
);

$bWorkflow = CModule::IncludeModule("workflow") && (CIBlock::GetArrayByID($IBLOCK_ID, "WORKFLOW") != "N");
$bBizproc = CModule::IncludeModule("bizproc") && (CIBlock::GetArrayByID($IBLOCK_ID, "BIZPROC") != "N");

$bCatalog = CModule::IncludeModule('catalog');
$arMainCatalog = false;
$arCatalogTabs = false;
$bOffers = false;
$boolCatalogRead = false;
$boolCatalogPrice = false;
if ($bCatalog)
{
    $boolCatalogRead = $USER->CanDoOperation('catalog_read');
    $boolCatalogPrice = $USER->CanDoOperation('catalog_price');
    $arMainCatalog = CCatalogSKU::GetInfoByIBlock($IBLOCK_ID);
    if (!empty($arMainCatalog))
    {
        if (CCatalogSKU::TYPE_PRODUCT == $arMainCatalog['CATALOG_TYPE'] || CCatalogSKU::TYPE_FULL == $arMainCatalog['CATALOG_TYPE'])
            $bOffers = true;
        CCatalogAdminTools::setProductFormParams();

        $arCatalogTabs = CCatalogAdminTools::getShowTabs($IBLOCK_ID, ($copyID > 0 && ID == 0 ? $copyID : $ID), $arMainCatalog);
        if (!empty($arCatalogTabs))
        {
            $arShowTabs['catalog'] = $arCatalogTabs[CCatalogAdminTools::TAB_CATALOG];
            $arShowTabs['sku'] = $arCatalogTabs[CCatalogAdminTools::TAB_SKU];
            $arShowTabs['product_set'] = $arCatalogTabs[CCatalogAdminTools::TAB_SET];
            $arShowTabs['product_group'] = $arCatalogTabs[CCatalogAdminTools::TAB_GROUP];
        }
    }
}
$str_TMP_ID = 0;
if ($bOffers && (0 == $ID || $bCopy))
{
    if ('GET' == $_SERVER['REQUEST_METHOD'] && 0 >= $ID)
    {
        $str_TMP_ID = CIBlockOffersTmp::Add($IBLOCK_ID, $arMainCatalog['IBLOCK_ID']);
    } else
    {
        if (isset($_REQUEST['TMP_ID']))
            $str_TMP_ID = (int) $_REQUEST['TMP_ID'];
    }
}
$TMP_ID = $str_TMP_ID;

if (($ID <= 0 || $bCopy) && $bWorkflow)
    $WF = "Y";
elseif (!$bWorkflow)
    $WF = "N";
else
    $WF = ($_REQUEST["WF"] === "Y") ? "Y" : "N";

$historyId = intval($history_id);
if ($historyId > 0 && $bBizproc)
    $view = "Y";
else
    $historyId = 0;

$APPLICATION->AddHeadScript('/bitrix/js/iblock/iblock_edit.js');
$APPLICATION->AddHeadScript("/local/js/tools.js");


$error = false;

$view = ($view == "Y") ? "Y" : "N"; //view mode

$return_url = (isset($return_url) ? (string) $return_url : '');

if ($bAutocomplete)
{
    $return_url = '';
} else
{
    if ($return_url != '' && strtolower(substr($return_url, strlen($APPLICATION->GetCurPage()))) == strtolower($APPLICATION->GetCurPage()))
        $return_url = '';
    if ($return_url == '')
    {
        if ($from == "iblock_section_admin")
            $return_url = Dedicated::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section' => intval($find_section_section)));
    }
}

do
{ //one iteration loop
    $errorTriger = false;
    if ($historyId > 0)
    {
        $arErrorsTmp = array();
        $arResult = CBPDocument::GetDocumentFromHistory($historyId, $arErrorsTmp);

        if (!empty($arErrorsTmp))
        {
            foreach ($arErrorsTmp as $e)
            {
                $error = new _CIBlockError(1, $e["code"], $e["message"]);
                break;
            }
        }

        $canWrite = CBPDocument::CanUserOperateDocument(
                        CBPCanUserOperateOperation::WriteDocument, $USER->GetID(), $arResult["DOCUMENT_ID"], array("UserGroups" => $USER->GetUserGroupArray())
        );
        if (!$canWrite)
        {
            $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
            break;
        }

        $type = $arResult["DOCUMENT"]["FIELDS"]["IBLOCK_TYPE_ID"];
        $IBLOCK_ID = $arResult["DOCUMENT"]["FIELDS"]["IBLOCK_ID"];
    }

    $arIBTYPE = CIBlockType::GetByIDLang($type, LANGUAGE_ID);
    if ($arIBTYPE === false)
    {
        $error = new _CIBlockError(1, "BAD_IBLOCK_TYPE", GetMessage("IBLOCK_BAD_BLOCK_TYPE_ID"));
        break;
    }

    $MENU_SECTION_ID = intval($IBLOCK_SECTION_ID) ? intval($IBLOCK_SECTION_ID) : intval($find_section_section);

    $bBadBlock = true;
    $arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);
    if ($arIBlock)
    {
        if (($ID > 0 && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "iblock_admin_display"))
            $bBadBlock = true;
        elseif (($ID <= 0 || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "iblock_admin_display"))
            $bBadBlock = true;
        elseif (CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit_any_wf_status"))
            $bBadBlock = false;
        elseif (!$bWorkflow && CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit"))
            $bBadBlock = false;
        elseif ($bWorkflow && ($WF == "Y" || $view == "Y"))
            $bBadBlock = false;
        elseif ($bBizproc)
            $bBadBlock = false;
        elseif (
                (($ID <= 0) || $bCopy) && CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
        )
            $bBadBlock = false;
        elseif (($ID > 0 && !$bCopy) && CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "iblock_admin_display"))
            $bBadBlock = false;
    }

    if ($bBadBlock)
    {
        $error = new _CIBlockError(1, "BAD_IBLOCK", GetMessage("IBLOCK_BAD_IBLOCK"));
        $APPLICATION->SetTitle($arIBTYPE["ELEMENT_NAME"] . ": " . GetMessage("IBLOCK_EDIT_TITLE"));
        break;
    }

    $bEditRights = $arIBlock["RIGHTS_MODE"] === "E" && (
            CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_rights_edit") || (($ID <= 0 || $bCopy) && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "element_rights_edit"))
            );

    $arShowTabs['sections'] = ('Y' == $arIBTYPE["SECTIONS"]);
    $arShowTabs['workflow'] = $bWorkflow;
    $arShowTabs['bizproc'] = $bBizproc && (0 >= $historyId);
    $arShowTabs['edit_rights'] = $bEditRights;
    if ('Y' == $view || $bPropertyAjax)
    {
        $arShowTabs['sku'] = false;
        $arShowTabs['product_set'] = false;
        $arShowTabs['product_group'] = false;
    }

    $aTabs = array(
        array(
            "DIV" => "edit1",
            "TAB" => $arIBlock["ELEMENT_NAME"],
            "ICON" => "iblock_element",
            "TITLE" => htmlspecialcharsex($arIBlock["ELEMENT_NAME"])
        ),
    );
    if ($arShowTabs['sections'])
        $aTabs[] = array(
            "DIV" => "edit2",
            "TAB" => $arIBlock["SECTIONS_NAME"],
            "ICON" => "iblock_element_section",
            "TITLE" => htmlspecialcharsex($arIBlock["SECTIONS_NAME"]),
        );
    if ($arShowTabs['edit_rights'])
        $aTabs[] = array(
            "DIV" => "edit9",
            "TAB" => GetMessage("IBEL_E_TAB_RIGHTS"),
            "ICON" => "iblock_element_rights",
            "TITLE" => GetMessage("IBEL_E_TAB_RIGHTS_TITLE")
        );

    $bCustomForm = (strlen($arIBlock["EDIT_FILE_AFTER"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBlock["EDIT_FILE_AFTER"])) || (strlen($arIBTYPE["EDIT_FILE_AFTER"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBTYPE["EDIT_FILE_AFTER"]));

    if ($ID > 0)
    {
        $rsElement = CIBlockElement::GetList(array(), array("ID" => $ID, "IBLOCK_ID" => $IBLOCK_ID, "SHOW_HISTORY" => "Y"), false, false, array("ID", "CREATED_BY"));
        if (!($arElement = $rsElement->Fetch()))
        {
            $error = new _CIBlockError(1, "BAD_ELEMENT", GetMessage("IBLOCK_BAD_ELEMENT"));
            $APPLICATION->SetTitle($arIBTYPE["ELEMENT_NAME"] . ": " . GetMessage("IBLOCK_EDIT_TITLE"));
            $errorTriger = true;
        }
    }

    if (!$errorTriger)
    {
        // workflow mode
        $isLocked = false;
        if ($ID > 0 && $WF == "Y")
        {
            // get ID of the last record in workflow
            $WF_ID = CIBlockElement::WF_GetLast($ID);

            // check for edit permissions
            $STATUS_ID = CIBlockElement::WF_GetCurrentStatus($WF_ID, $STATUS_TITLE);
            $STATUS_PERMISSION = CIBlockElement::WF_GetStatusPermission($STATUS_ID);

            if ($STATUS_ID > 1 && $STATUS_PERMISSION < 2)
            {
                $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
                $errorTriger = true;
            } elseif ($STATUS_ID == 1)
            {
                $WF_ID = $ID;
                $STATUS_ID = CIBlockElement::WF_GetCurrentStatus($WF_ID, $STATUS_TITLE);
                $STATUS_PERMISSION = CIBlockElement::WF_GetStatusPermission($STATUS_ID);
            }

            if (!$errorTriger)
            {
                // check if document is locked
                $isLocked = CIBlockElement::WF_IsLocked($ID, $locked_by, $date_lock);
                if ($isLocked)
                {
                    if ($locked_by > 0)
                    {
                        $rsUser = CUser::GetList(($by = "ID"), ($order = "ASC"), array("ID_EQUAL_EXACT" => $locked_by));
                        if ($arUser = $rsUser->GetNext())
                            $locked_by = rtrim("[" . $arUser["ID"] . "] (" . $arUser["LOGIN"] . ") " . $arUser["NAME"] . " " . $arUser["LAST_NAME"]);
                    }
                    $error = new _CIBlockError(2, "BLOCKED", GetMessage("IBLOCK_DOCUMENT_LOCKED", array("#ID#" => $locked_by, "#DATE#" => $date_lock)));
                    $errorTriger = true;
                }
            }
        }
        elseif ($bBizproc)
        {
            $arDocumentStates = CBPDocument::GetDocumentStates(
                            array(MODULE_ID, ENTITY, DOCUMENT_TYPE), ($ID > 0) ? array(MODULE_ID, ENTITY, $ID) : null, "Y"
            );

            $arCurrentUserGroups = $USER->GetUserGroupArray();
            if ($ID > 0 && is_array($arElement))
            {
                if ($USER->GetID() == $arElement["CREATED_BY"])
                    $arCurrentUserGroups[] = "Author";
            }
            else
            {
                $arCurrentUserGroups[] = "Author";
            }

            if ($ID > 0)
            {
                $canWrite = CBPDocument::CanUserOperateDocument(
                                CBPCanUserOperateOperation::WriteDocument, $USER->GetID(), array(MODULE_ID, ENTITY, $ID), array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates)
                );
                $canRead = CBPDocument::CanUserOperateDocument(
                                CBPCanUserOperateOperation::ReadDocument, $USER->GetID(), array(MODULE_ID, ENTITY, $ID), array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates)
                );
            } else
            {
                $canWrite = CBPDocument::CanUserOperateDocumentType(
                                CBPCanUserOperateOperation::WriteDocument, $USER->GetID(), array(MODULE_ID, ENTITY, DOCUMENT_TYPE), array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates)
                );
                $canRead = false;
            }

            if (!$canWrite && !$canRead)
            {
                $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
                $errorTriger = true;
            }
        }
    }

    $denyAutosave = false;
    if ($bWorkflow)
    {
        $denyAutosave = CIBlockElement::WF_IsLocked($ID, $locked_by1, $date_lock1);
    } else
    {
        $denyAutosave = ($view == "Y") || (
                (($ID <= 0) || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
                ) || (
                (($ID > 0) && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
                ) || (
                $bBizproc && !$canWrite
                );
    }

    $tabControl = new CAdminForm($bCustomForm ? "tabControl" : "form_element_" . $IBLOCK_ID, $aTabs, true, $denyAutosave);
    $customTabber = new CAdminTabEngine("OnAdminIBlockElementEdit", array("ID" => $ID, "IBLOCK" => $arIBlock, "IBLOCK_TYPE" => $arIBTYPE));
    $tabControl->AddTabs($customTabber);

    if ($bCustomForm)
    {
        $tabControl->SetShowSettings(false);
        if ($bCatalog && !empty($arMainCatalog))
        {
            $arMainCatalog['OFFERS_PROPERTY_ID'] = 0;
            $arMainCatalog['OFFERS_IBLOCK_ID'] = 0;
            if ($arMainCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL || $arMainCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT)
            {
                $arMainCatalog['OFFERS_PROPERTY_ID'] = $arMainCatalog['SKU_PROPERTY_ID'];
                $arMainCatalog['OFFERS_IBLOCK_ID'] = $arMainCatalog['IBLOCK_ID'];
            }
        }
    }

    if (!$errorTriger)
    {
        //Find out files properties
        $arFileProps = array();
        $properties = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_TYPE" => "F", "ACTIVE" => "Y"));
        while ($prop_fields = $properties->Fetch())
            $arFileProps[] = $prop_fields['ID'];

        //Assembly properties values from $_POST and $_FILES
        $PROP = array();
        if (isset($_POST['PROP']))
            $PROP = $_POST['PROP'];

        //Recover some user defined properties
        if (is_array($PROP))
        {
            foreach ($PROP as $k1 => $val1)
            {
                if (is_array($val1))
                {
                    foreach ($val1 as $k2 => $val2)
                    {
                        $text_name = preg_replace("/([^a-z0-9])/is", "_", "PROP[" . $k1 . "][" . $k2 . "][VALUE][TEXT]");
                        if (array_key_exists($text_name, $_POST))
                        {
                            $type_name = preg_replace("/([^a-z0-9])/is", "_", "PROP[" . $k1 . "][" . $k2 . "][VALUE][TYPE]");
                            $PROP[$k1][$k2]["VALUE"] = array(
                                "TEXT" => $_POST[$text_name],
                                "TYPE" => $_POST[$type_name],
                            );
                        }
                    }
                }
            }

            foreach ($PROP as $k1 => $val1)
            {
                if (is_array($val1))
                {
                    foreach ($val1 as $k2 => $val2)
                    {
                        if (!is_array($val2))
                            $PROP[$k1][$k2] = array("VALUE" => $val2);
                    }
                }
            }
        }

        //transpose files array
        // [property id] [value id] = file array (name, type, tmp_name, error, size)
        $files = $_FILES["PROP"];
        if (is_array($files))
        {
            if (!is_array($PROP))
                $PROP = array();
            CAllFile::ConvertFilesToPost($_FILES["PROP"], $PROP);
        }

        foreach ($arFileProps as $k1)
        {
            if (isset($PROP_del[$k1]) && is_array($PROP_del[$k1]))
            {
                foreach ($PROP_del[$k1] as $prop_value_id => $tmp)
                {
                    if (!array_key_exists($prop_value_id, $PROP[$k1]))
                        $PROP[$k1][$prop_value_id] = null;
                }
            }

            if (isset($PROP[$k1]) && is_array($PROP[$k1]))
            {
                foreach ($PROP[$k1] as $prop_value_id => $prop_value)
                {
                    $PROP[$k1][$prop_value_id] = CIBlock::makeFilePropArray(
                                    $PROP[$k1][$prop_value_id], $PROP_del[$k1][$prop_value_id] === "Y", isset($_POST["DESCRIPTION_PROP"][$k1][$prop_value_id]) ? $_POST["DESCRIPTION_PROP"][$k1][$prop_value_id] : $_POST["PROP_descr"][$k1][$prop_value_id]
                    );
                }
            }
        }

        $DESCRIPTION_PROP = $_POST["DESCRIPTION_PROP"];
        if (is_array($DESCRIPTION_PROP))
        {
            foreach ($DESCRIPTION_PROP as $k1 => $val1)
            {
                foreach ($val1 as $k2 => $val2)
                {
                    if (is_set($PROP[$k1], $k2) && is_array($PROP[$k1][$k2]) && is_set($PROP[$k1][$k2], "DESCRIPTION"))
                        $PROP[$k1][$k2]["DESCRIPTION"] = $val2;
                    else
                        $PROP[$k1][$k2] = Array("VALUE" => $PROP[$k1][$k2], "DESCRIPTION" => $val2);
                }
            }
        }

        function _prop_value_id_cmp($a, $b)
        {
            if (substr($a, 0, 1) === "n")
            {
                $a = intval(substr($a, 1));
                if (substr($b, 0, 1) === "n")
                {
                    $b = intval(substr($b, 1));
                    if ($a < $b)
                        return -1;
                    elseif ($a > $b)
                        return 1;
                    else
                        return 0;
                }
                else
                {
                    return 1;
                }
            } else
            {
                if (substr($b, 0, 1) === "n")
                {
                    return -1;
                } else
                {
                    if (preg_match("/^(\\d+):(\\d+)$/", $a, $a_match))
                        $a = intval($a_match[2]);
                    else
                        $a = intval($a);

                    if (preg_match("/^(\\d+):(\\d+)$/", $b, $b_match))
                        $b = intval($b_match[2]);
                    else
                        $b = intval($b);

                    if ($a < $b)
                        return -1;
                    elseif ($a > $b)
                        return 1;
                    else
                        return 0;
                }
            }
        }

        //Now reorder property values
        if (is_array($PROP) && !empty($arFileProps))
        {
            foreach ($arFileProps as $id)
            {
                if (is_array($PROP[$id]))
                    uksort($PROP[$id], "_prop_value_id_cmp");
            }
        }

        if (strlen($arIBlock["EDIT_FILE_BEFORE"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBlock["EDIT_FILE_BEFORE"]))
        {
            include($_SERVER["DOCUMENT_ROOT"] . $arIBlock["EDIT_FILE_BEFORE"]);
        } elseif (strlen($arIBTYPE["EDIT_FILE_BEFORE"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBTYPE["EDIT_FILE_BEFORE"]))
        {
            include($_SERVER["DOCUMENT_ROOT"] . $arIBTYPE["EDIT_FILE_BEFORE"]);
        }

        if (
                $bBizproc && $canWrite && $historyId <= 0 && $ID > 0 && $REQUEST_METHOD == "GET" && isset($_REQUEST["stop_bizproc"]) && strlen($_REQUEST["stop_bizproc"]) > 0 && check_bitrix_sessid()
        )
        {
            CBPDocument::TerminateWorkflow(
                    $_REQUEST["stop_bizproc"], array(MODULE_ID, ENTITY, $ID), $ar
            );

            if (!empty($ar))
            {
                $str = "";
                foreach ($ar as $a)
                    $str .= $a["message"];
                $error = new _CIBlockError(2, "STOP_BP_ERROR", $str);
            } else
            {
                LocalRedirect($APPLICATION->GetCurPageParam("", array("stop_bizproc", "sessid")));
            }
        }

        if (
                $historyId <= 0 && 'GET' == $_SERVER['REQUEST_METHOD'] && $bCatalog && 0 < $ID && check_bitrix_sessid()
        )
        {
            if (CCatalogAdminTools::changeTabs($IBLOCK_ID, $ID, $arMainCatalog))
            {
                $arUrlParams = array(
                    "find_section_section" => intval($find_section_section)
                );
                if ('Y' == $WF)
                    $arUrlParams['WF'] = 'Y';
                if ('' != $return_url)
                    $arUrlParams['return_url'] = $return_url;
                if ($bAutocomplete)
                    $arUrlParams['lookup'] = $strLookup;
                CCatalogAdminTools::addTabParams($arUrlParams);
                LocalRedirect("/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, $ID, $arUrlParams, "&" . $tabControl->ActiveTabParam()));
            }
        }

        if (
                $historyId <= 0 && 'POST' == $_SERVER['REQUEST_METHOD'] && strlen($Update) > 0 && $view != "Y" && (!$error) && empty($dontsave)
        )
        {
            $DB->StartTransaction();

            if (isset($_POST["IBLOCK_SECTION"]))
            {
                if (is_array($_POST["IBLOCK_SECTION"]))
                {
                    foreach ($_POST["IBLOCK_SECTION"] as $i => $parent_section_id)
                    {
                        if (!CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $parent_section_id, "section_element_bind"))
                            unset($_POST["IBLOCK_SECTION"][$i]);
                    }

                    if (empty($_POST["IBLOCK_SECTION"]))
                        unset($_POST["IBLOCK_SECTION"]);
                }
                else
                {
                    if (!CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $_POST["IBLOCK_SECTION"], "section_element_bind"))
                        unset($_POST["IBLOCK_SECTION"]);
                }
            }

            if (!(check_bitrix_sessid() || $_SESSION['IBLOCK_CUSTOM_FORM'] === true))
            {
                $strWarning .= GetMessage("IBLOCK_WRONG_SESSION") . "<br>";
                $error = new _CIBlockError(2, "BAD_SAVE", $strWarning);
                $bVarsFromForm = true;
            } elseif ($WF == "Y" && $bWorkflow && intval($_POST["WF_STATUS_ID"]) <= 0)
                $strWarning .= GetMessage("IBLOCK_WRONG_WF_STATUS") . "<br>";
            elseif ($WF == "Y" && $bWorkflow && CIBlockElement::WF_GetStatusPermission($_POST["WF_STATUS_ID"]) < 1)
                $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_STATUS") . " [" . $_POST["WF_STATUS_ID"] . "]." . "<br>";
            elseif (0 >= $ID && !isset($_POST["IBLOCK_SECTION"]) && !CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "section_element_bind"))
                $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_SECTION") . "<br>";
            elseif (!$customTabber->Check())
            {
                if ($ex = $APPLICATION->GetException())
                    $strWarning .= $ex->GetString();
                else
                    $strWarning .= "Error. ";
            }
            else
            {
                if ($bCatalog)
                {
                    $arCatalogItem = array(
                        'IBLOCK_ID' => $IBLOCK_ID,
                        'SECTION_ID' => $MENU_SECTION_ID,
                        'ID' => $ID,
                        'PRODUCT_ID' => (0 < $ID ? CIBlockElement::GetRealElement($ID) : 0)
                    );
                    if (
                            $arShowTabs['catalog'] && file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/catalog/admin/templates/product_edit_validator.php")
                    )
                    {
                        // errors'll be appended to $strWarning;
                        $boolSKUExists = false;
                        if (CCatalogSKU::TYPE_FULL == $arMainCatalog['CATALOG_TYPE'])
                        {
                            $boolSKUExists = CCatalogSKU::IsExistOffers(($ID > 0 ? $ID : '-' . $str_TMP_ID), $IBLOCK_ID);
                        }
                        include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/catalog/admin/templates/product_edit_validator.php");
                    }
                    if ($arShowTabs['product_set'])
                    {
                        CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_SET));
                        if (!CCatalogAdminProductSetEdit::checkFormValues($arCatalogItem))
                        {
                            $strWarning .= implode('<br>', CCatalogAdminProductSetEdit::getErrors());
                        }
                    }
                    if ($arShowTabs['product_group'])
                    {
                        CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_GROUP));
                        if (!CCatalogAdminProductSetEdit::checkFormValues($arCatalogItem))
                        {
                            $strWarning .= implode('<br>', CCatalogAdminProductSetEdit::getErrors());
                        }
                    }
                }
                if ($bBizproc)
                {
                    if ($canWrite)
                    {
                        $arBizProcParametersValues = array();
                        foreach ($arDocumentStates as $arDocumentState)
                        {
                            if (strlen($arDocumentState["ID"]) <= 0)
                            {
                                $arErrorsTmp = array();

                                $arBizProcParametersValues[$arDocumentState["TEMPLATE_ID"]] = CBPDocument::StartWorkflowParametersValidate(
                                                $arDocumentState["TEMPLATE_ID"], $arDocumentState["TEMPLATE_PARAMETERS"], array(MODULE_ID, ENTITY, DOCUMENT_TYPE), $arErrorsTmp
                                );

                                if (!empty($arErrorsTmp))
                                {
                                    foreach ($arErrorsTmp as $e)
                                        $strWarning .= $e["message"] . "<br />";
                                }
                            }
                        }
                    } else
                    {
                        $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_STATUS") . "<br />";
                    }
                }

                if ($strWarning == '')
                {
                    $bs = new CIBlockElement;

                    $arPREVIEW_PICTURE = CIBlock::makeFileArray(
                                    array_key_exists("PREVIEW_PICTURE", $_FILES) ? $_FILES["PREVIEW_PICTURE"] : $_REQUEST["PREVIEW_PICTURE"], ${"PREVIEW_PICTURE_del"} === "Y", ${"PREVIEW_PICTURE_descr"}
                    );
                    if ($arPREVIEW_PICTURE["error"] == 0)
                        $arPREVIEW_PICTURE["COPY_FILE"] = "Y";

                    $arDETAIL_PICTURE = CIBlock::makeFileArray(
                                    array_key_exists("DETAIL_PICTURE", $_FILES) ? $_FILES["DETAIL_PICTURE"] : $_REQUEST["DETAIL_PICTURE"], ${"DETAIL_PICTURE_del"} === "Y", ${"DETAIL_PICTURE_descr"}
                    );
                    if ($arDETAIL_PICTURE["error"] == 0)
                        $arDETAIL_PICTURE["COPY_FILE"] = "Y";

                    $arFields = array(
                        "ACTIVE" => $_POST["ACTIVE"],
                        "MODIFIED_BY" => $USER->GetID(),
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ACTIVE_FROM" => $_POST["ACTIVE_FROM"],
                        "ACTIVE_TO" => $_POST["ACTIVE_TO"],
                        "SORT" => $_POST["SORT"],
                        "NAME" => $_POST["NAME"],
                        "CODE" => trim($_POST["CODE"], " \t\n\r"),
                        "TAGS" => $_POST["TAGS"],
                        "PREVIEW_PICTURE" => $arPREVIEW_PICTURE,
                        "PREVIEW_TEXT" => $_POST["PREVIEW_TEXT"],
                        "PREVIEW_TEXT_TYPE" => $_POST["PREVIEW_TEXT_TYPE"],
                        "DETAIL_PICTURE" => $arDETAIL_PICTURE,
                        "DETAIL_TEXT" => $_POST["DETAIL_TEXT"],
                        "DETAIL_TEXT_TYPE" => $_POST["DETAIL_TEXT_TYPE"],
                        "TMP_ID" => $str_TMP_ID,
                        "PROPERTY_VALUES" => $PROP,
                    );

                    if (isset($_POST["IBLOCK_SECTION"]) && is_array($_POST["IBLOCK_SECTION"]))
                        $arFields["IBLOCK_SECTION"] = $_POST["IBLOCK_SECTION"];

                    if (COption::GetOptionString("iblock", "show_xml_id", "N") == "Y" && is_set($_POST, "XML_ID"))
                        $arFields["XML_ID"] = trim($_POST["XML_ID"], " \t\n\r");

                    if ($bEditRights)
                    {
                        if (is_array($_POST["RIGHTS"]))
                            $arFields["RIGHTS"] = CIBlockRights::Post2Array($_POST["RIGHTS"]);
                        else
                            $arFields["RIGHTS"] = array();
                    }

                    if (is_array($_POST["IPROPERTY_TEMPLATES"]))
                    {
                        $ELEMENT_PREVIEW_PICTURE_FILE_NAME = Helper::convertArrayToModifiers($_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]);
                        $ELEMENT_DETAIL_PICTURE_FILE_NAME = Helper::convertArrayToModifiers($_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_NAME"]);

                        $arFields["IPROPERTY_TEMPLATES"] = array(
                            "ELEMENT_META_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_TITLE"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_TITLE"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_META_KEYWORDS" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_KEYWORDS"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_KEYWORDS"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_META_DESCRIPTION" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_DESCRIPTION"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_DESCRIPTION"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_PAGE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PAGE_TITLE"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PAGE_TITLE"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_ALT" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_NAME" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]["INHERITED"] === "N" ?
                                    $ELEMENT_PREVIEW_PICTURE_FILE_NAME :
                                    ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_ALT" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]["INHERITED"] === "N" ?
                                    $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]["TEMPLATE"] :
                                    ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_NAME" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_NAME"]["INHERITED"] === "N" ?
                                    $ELEMENT_DETAIL_PICTURE_FILE_NAME :
                                    ""
                            ),
                        );
                    }

                    if ($bWorkflow)
                    {
                        $arFields["WF_COMMENTS"] = $_POST["WF_COMMENTS"];
                        if (intval($_POST["WF_STATUS_ID"]) > 0)
                        {
                            $arFields["WF_STATUS_ID"] = $_POST["WF_STATUS_ID"];
                        }
                    }

                    if ($bBizproc)
                    {
                        $BP_HISTORY_NAME = $arFields["NAME"];
                        if ($ID <= 0)
                            $arFields["BP_PUBLISHED"] = "N";
                    }

                    if ($ID > 0)
                    {
                        $bCreateRecord = false;
                        $res = $bs->Update($ID, $arFields, $WF == "Y", true, true);
                    } else
                    {
                        $bCreateRecord = true;
                        $ID = $bs->Add($arFields, $bWorkflow, true, true);
                        $res = ($ID > 0);
                        $PARENT_ID = $ID;

                        if ($res)
                        {
                            if ($arShowTabs['sku'])
                            {
                                $arFilter = array('IBLOCK_ID' => $arMainCatalog['IBLOCK_ID'], '=PROPERTY_' . $arMainCatalog['SKU_PROPERTY_ID'] => '-' . $str_TMP_ID);
                                $rsOffersItems = CIBlockElement::GetList(
                                                array(), $arFilter, false, false, array('ID')
                                );
                                while ($arOfferItem = $rsOffersItems->Fetch())
                                {
                                    CIBlockElement::SetPropertyValues(
                                            $arOfferItem['ID'], $arMainCatalog['IBLOCK_ID'], $ID, $arMainCatalog['SKU_PROPERTY_ID']
                                    );
                                }
                                $boolFlagClear = CIBlockOffersTmp::Delete($str_TMP_ID);
                                $boolFlagClearAll = CIBlockOffersTmp::DeleteOldID($IBLOCK_ID);
                            }
                        }
                    }

                    if (!$res)
                    {
                        $strWarning .= $bs->LAST_ERROR . "<br>";
                    } else
                    {
                        $ipropValues = new ElementValues($IBLOCK_ID, $ID);
                        $ipropValues->clearValues();
                        CIBlockElement::RecalcSections($ID);
                    }

                    if ('' == $strWarning && $bCatalog)
                    {
                        $arCatalogItem = array(
                            'IBLOCK_ID' => $IBLOCK_ID,
                            'SECTION_ID' => $MENU_SECTION_ID,
                            'ID' => $ID,
                            'PRODUCT_ID' => CIBlockElement::GetRealElement($ID)
                        );
                        if ($arShowTabs['catalog'])
                        {
                            include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/catalog/admin/templates/product_edit_action.php");
                        } elseif ($arShowTabs['sku'])
                        {
                            if (!CCatalogProduct::IsExistProduct($arCatalogItem['PRODUCT_ID']))
                            {
                                $arEmptyProduct = array(
                                    'ID' => $arCatalogItem['PRODUCT_ID'],
                                    'QUANTITY' => 0,
                                    'QUANTITY_TRACE' => 'N',
                                    'CAN_BUY_ZERO' => 'N',
                                    'NEGATIVE_AMOUNT_TRACE' => 'N',
                                    'TYPE' => CCatalogProduct::TYPE_SKU
                                );
                                CCatalogProduct::Add($arEmptyProduct, false);
                            }
                        }
                        if ($arShowTabs['product_set'])
                        {
                            CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_SET));
                            CCatalogAdminProductSetEdit::saveFormValues($arCatalogItem);
                        }
                        if ($arShowTabs['product_group'])
                        {
                            CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_GROUP));
                            CCatalogAdminProductSetEdit::saveFormValues($arCatalogItem);
                        }
                    }
                } // if ($strWarning)

                if ($bBizproc)
                {
                    if ($strWarning == '')
                    {
                        $arBizProcWorkflowId = array();
                        foreach ($arDocumentStates as $arDocumentState)
                        {
                            if (strlen($arDocumentState["ID"]) <= 0)
                            {
                                $arErrorsTmp = array();

                                $arBizProcWorkflowId[$arDocumentState["TEMPLATE_ID"]] = CBPDocument::StartWorkflow(
                                                $arDocumentState["TEMPLATE_ID"], array(MODULE_ID, ENTITY, $ID), $arBizProcParametersValues[$arDocumentState["TEMPLATE_ID"]], $arErrorsTmp
                                );

                                if (!empty($arErrorsTmp))
                                {
                                    foreach ($arErrorsTmp as $e)
                                        $strWarning .= $e["message"] . "<br />";
                                }
                            }
                        }
                    }

                    if ($strWarning == '')
                    {
                        $bizprocIndex = intval($_REQUEST["bizproc_index"]);
                        if ($bizprocIndex > 0)
                        {
                            for ($i = 1; $i <= $bizprocIndex; $i++)
                            {
                                $bpId = trim($_REQUEST["bizproc_id_" . $i]);
                                $bpTemplateId = intval($_REQUEST["bizproc_template_id_" . $i]);
                                $bpEvent = trim($_REQUEST["bizproc_event_" . $i]);

                                if (strlen($bpEvent) > 0)
                                {
                                    if (strlen($bpId) > 0)
                                    {
                                        if (!array_key_exists($bpId, $arDocumentStates))
                                            continue;
                                    }
                                    else
                                    {
                                        if (!array_key_exists($bpTemplateId, $arDocumentStates))
                                            continue;
                                        $bpId = $arBizProcWorkflowId[$bpTemplateId];
                                    }

                                    $arErrorTmp = array();
                                    CBPDocument::SendExternalEvent(
                                            $bpId, $bpEvent, array("Groups" => $arCurrentUserGroups, "User" => $USER->GetID()), $arErrorTmp
                                    );

                                    if (!empty($arErrorsTmp))
                                    {
                                        foreach ($arErrorsTmp as $e)
                                            $strWarning .= $e["message"] . "<br />";
                                    }
                                }
                            }
                        }

                        $arDocumentStates = null;
                        CBPDocument::AddDocumentToHistory(array(MODULE_ID, ENTITY, $ID), $BP_HISTORY_NAME, $GLOBALS["USER"]->GetID());
                    }
                }
            }

            if ($strWarning == '')
            {
                if (!$customTabber->Action())
                {
                    if ($ex = $APPLICATION->GetException())
                        $strWarning .= $ex->GetString();
                    else
                        $strWarning .= "Error. ";
                }
            }

            if ($strWarning != '')
            {
                $error = new _CIBlockError(2, "BAD_SAVE", $strWarning);
                $bVarsFromForm = true;
                $DB->Rollback();
            } else
            {
                if ($bWorkflow)
                    CIBlockElement::WF_UnLock($ID);

                $arFields['ID'] = $ID;
                if (function_exists('BXIBlockAfterSave'))
                    BXIBlockAfterSave($arFields);

                $DB->Commit();

                if (strlen($apply) <= 0 && strlen($save_and_add) <= 0)
                {
                    if ($bAutocomplete)
                    {
                        if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                        {
                            ?><script type="text/javascript">
                                                            top.<? echo $strLookup; ?>.AddValue(<? echo $ID; ?>);
                                                            top.BX.WindowManager.Get().AllowClose();
                                                            top.BX.WindowManager.Get().Close();
                            </script><?
                            die();
                        } else
                        {
                            ?><script type="text/javascript">
                                                            window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID; ?>);
                                                            window.close();
                            </script><?
                        }
                    } elseif (strlen($return_url) > 0)
                    {
                        if (strpos($return_url, "#") !== false)
                        {
                            $rsElement = CIBlockElement::GetList(array(), array("ID" => $ID), false, false, array("DETAIL_PAGE_URL"));
                            $arElement = $rsElement->Fetch();
                            if ($arElement)
                                $return_url = CIBlock::ReplaceDetailUrl($return_url, $arElement, true, "E");
                        }

                        if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                        {
                            if ($return_url === "reload_absence_calendar")
                            {
                                echo '<script type="text/javascript">top.jsBXAC.__reloadCurrentView();</script>';
                                die();
                            } else
                            {
                                LocalRedirect($return_url);
                            }
                        } else
                        {
                            LocalRedirect($return_url);
                        }
                    } else
                    {
                        LocalRedirect("/bitrix/admin/" . Dedicated::GetAdminElementListLink($IBLOCK_ID, array('find_section_section' => intval($find_section_section))));
                    }
                } elseif (strlen($save_and_add) > 0)
                {
                    $params = array(
                        "WF" => ($WF == "Y" ? "Y" : null),
                        "find_section_section" => intval($find_section_section),
                        "return_url" => (strlen($return_url) > 0 ? $return_url : null),
                    );
                    if ($IBLOCK_SECTION_ID > 0)
                    {
                        $params["IBLOCK_SECTION_ID"] = intval($IBLOCK_SECTION_ID);
                    } elseif (isset($arFields["IBLOCK_SECTION"]) && !empty($arFields["IBLOCK_SECTION"]))
                    {
                        foreach ($arFields["IBLOCK_SECTION"] as $i => $id)
                            $params["IBLOCK_SECTION_ID[" . $i . "]"] = $id;
                    }

                    if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                    {
                        while (ob_end_clean());
                        $l = CUtil::JSEscape(Dedicated::GetAdminElementEditLink($IBLOCK_ID, 0, array_merge($params, array(
                                            "from_module" => "iblock",
                                            "bxpublic" => "Y",
                                            "nobuttons" => "Y",
                                                )), "&" . $tabControl->ActiveTabParam()));
                        ?>
                        <script type="text/javascript">
                            top.BX.ajax.get(
                                    '/bitrix/admin/<? echo $l; ?>',
                                    function (result) {
                                        top.BX.closeWait();
                                        top.window.reloadAfterClose = true;
                                        top.BX.WindowManager.Get().SetContent(result);
                                    }
                            );
                        </script>
                        <?
                        die();
                    } else
                    {
                        LocalRedirect("/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, 0, $params, "&" . $tabControl->ActiveTabParam()));
                    }
                } else
                {
                    LocalRedirect("/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, $ID, array(
                                "WF" => ($WF == "Y" ? "Y" : null),
                                "find_section_section" => intval($find_section_section),
                                "return_url" => (strlen($return_url) > 0 ? $return_url : null),
                                "lookup" => $bAutocomplete ? $strLookup : null,
                                    ), "&" . $tabControl->ActiveTabParam()));
                }
            }
        }

        if (!empty($dontsave) && check_bitrix_sessid())
        {
            if ($bWorkflow)
                CIBlockElement::WF_UnLock($ID);

            if (strlen($return_url) > 0)
            {
                if ($bAutocomplete)
                {
                    ?><script type="text/javascript">
                                            window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID; ?>);
                                            window.close();
                    </script><?
                } elseif (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                {
                    echo '<script type="text/javascript">top.BX.closeWait(); top.BX.WindowManager.Get().AllowClose(); top.BX.WindowManager.Get().Close();</script>';
                    die();
                } else
                {
                    $rsElement = CIBlockElement::GetList(array(), array("=ID" => $ID), false, array("nTopCount" => 1), array("DETAIL_PAGE_URL"));
                    $arElement = $rsElement->Fetch();
                    if ($arElement)
                        $return_url = CIBlock::ReplaceDetailUrl($return_url, $arElement, true, "E");
                    LocalRedirect($return_url);
                }
            }
            else
            {
                if ($bAutocomplete)
                {
                    ?><script type="text/javascript">
                                            window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID; ?>);
                                            window.close();
                    </script><?
                } else
                {
                    LocalRedirect("/bitrix/admin/" . Dedicated::GetAdminElementListLink($IBLOCK_ID, array('find_section_section' => intval($find_section_section))));
                }
            }
        }
    }
} while (false);

if ($error && $error->err_level == 1)
{
    if ($bAutocomplete)
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
    else
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    CAdminMessage::ShowOldStyleError($error->GetErrorText());
}
else
{
    if (!$arIBlock["ELEMENT_NAME"])
        $arIBlock["ELEMENT_NAME"] = $arIBTYPE["ELEMENT_NAME"] ? $arIBTYPE["ELEMENT_NAME"] : GetMessage("IBEL_E_IBLOCK_ELEMENT");
    if (!$arIBlock["SECTIONS_NAME"])
        $arIBlock["SECTIONS_NAME"] = $arIBTYPE["SECTION_NAME"] ? $arIBTYPE["SECTION_NAME"] : GetMessage("IBEL_E_IBLOCK_SECTIONS");

    ClearVars("str_");
    ClearVars("str_prev_");
    ClearVars("prn_");
    $str_SORT = "500";

    if (!$error && $bWorkflow && $view != "Y")
    {
        if (!$bCopy)
            CIBlockElement::WF_Lock($ID);
        else
            CIBlockElement::WF_UnLock($ID);
    }

    if ($historyId <= 0 && $view == "Y")
    {
        $WF_ID = $ID;
        $ID = CIBlockElement::GetRealElement($ID);

        if ($PREV_ID)
        {
            $prev_result = CIBlockElement::GetByID($PREV_ID);
            $prev_arElement = $prev_result->ExtractFields("str_prev_");
            if (!$prev_arElement)
                $PREV_ID = 0;
        }
    }

    $str_IBLOCK_ELEMENT_SECTION = Array();
    $str_ACTIVE = $arIBlock["FIELDS"]["ACTIVE"]["DEFAULT_VALUE"] === "N" ? "N" : "Y";
    $str_NAME = htmlspecialcharsbx($arIBlock["FIELDS"]["NAME"]["DEFAULT_VALUE"]);
    if ($arIBlock["FIELDS"]["ACTIVE_FROM"]["DEFAULT_VALUE"] === "=now")
        $str_ACTIVE_FROM = ConvertTimeStamp(time() + CTimeZone::GetOffset(), "FULL");
    elseif ($arIBlock["FIELDS"]["ACTIVE_FROM"]["DEFAULT_VALUE"] === "=today")
        $str_ACTIVE_FROM = ConvertTimeStamp(time() + CTimeZone::GetOffset(), "SHORT");

    if (intval($arIBlock["FIELDS"]["ACTIVE_TO"]["DEFAULT_VALUE"]) > 0)
        $str_ACTIVE_TO = ConvertTimeStamp(time() + intval($arIBlock["FIELDS"]["ACTIVE_TO"]["DEFAULT_VALUE"]) * 24 * 60 * 60 + CTimeZone::GetOffset(), "FULL");

    $str_PREVIEW_TEXT_TYPE = $arIBlock["FIELDS"]["PREVIEW_TEXT_TYPE"]["DEFAULT_VALUE"] !== "html" ? "text" : "html";
    $str_PREVIEW_TEXT = htmlspecialcharsbx($arIBlock["FIELDS"]["PREVIEW_TEXT"]["DEFAULT_VALUE"]);
    $str_DETAIL_TEXT_TYPE = $arIBlock["FIELDS"]["DETAIL_TEXT_TYPE"]["DEFAULT_VALUE"] !== "html" ? "text" : "html";
    $str_DETAIL_TEXT = htmlspecialcharsbx($arIBlock["FIELDS"]["DETAIL_TEXT"]["DEFAULT_VALUE"]);

    if ($historyId > 0)
    {
        $view = "Y";
        foreach ($arResult["DOCUMENT"]["FIELDS"] as $k => $v)
            ${"str_" . $k} = $v;
    } else
    {
        $result = CIBlockElement::GetByID($WF_ID);

        if ($arElement = $result->ExtractFields("str_"))
        {
            if ($str_IN_SECTIONS == "N")
            {
                $str_IBLOCK_ELEMENT_SECTION[] = 0;
            } else
            {
                $result = CIBlockElement::GetElementGroups($WF_ID, true, array('ID', 'IBLOCK_ELEMENT_ID'));
                while ($ar = $result->Fetch())
                    $str_IBLOCK_ELEMENT_SECTION[] = $ar["ID"];
            }
            $ipropTemlates = new ElementTemplates($IBLOCK_ID, $WF_ID);
        } else
        {
            $WF_ID = 0;
            $ID = 0;
            if (is_array($IBLOCK_SECTION_ID))
            {
                foreach ($IBLOCK_SECTION_ID as $id)
                    if ($id > 0)
                        $str_IBLOCK_ELEMENT_SECTION[] = $id;
            }
            elseif ($IBLOCK_SECTION_ID > 0)
            {
                $str_IBLOCK_ELEMENT_SECTION[] = $IBLOCK_SECTION_ID;
            }
            $ipropTemlates = new ElementTemplates($IBLOCK_ID, 0);
            $ipropTemlates->getValuesEntity()->setParents($str_IBLOCK_ELEMENT_SECTION);
        }
        $str_IPROPERTY_TEMPLATES = $ipropTemlates->findTemplates();
        $str_IPROPERTY_TEMPLATES["ELEMENT_PREVIEW_PICTURE_FILE_NAME"] = Helper::convertModifiersToArray($str_IPROPERTY_TEMPLATES["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]);
        $str_IPROPERTY_TEMPLATES["ELEMENT_DETAIL_PICTURE_FILE_NAME"] = Helper::convertModifiersToArray($str_IPROPERTY_TEMPLATES["ELEMENT_DETAIL_PICTURE_FILE_NAME"]);
    }

    if ($bCopy)
    {
        $str_XML_ID = "";
    }

    if ($ID > 0 && !$bCopy)
    {
        if ($view == "Y" || ($bBizproc && !$canWrite))
            $APPLICATION->SetTitle($arIBlock["NAME"] . ": " . $arIBlock["ELEMENT_NAME"] . ": " . $arElement["NAME"] . " - " . GetMessage("IBLOCK_ELEMENT_EDIT_VIEW"));
        else
            $APPLICATION->SetTitle($arIBlock["NAME"] . ": " . $arIBlock["ELEMENT_NAME"] . ": " . $arElement["NAME"] . " - " . GetMessage("IBLOCK_EDIT_TITLE"));
    }
    else
    {
        $APPLICATION->SetTitle($arIBlock["NAME"] . ": " . $arIBlock["ELEMENT_NAME"] . ": " . GetMessage("IBLOCK_NEW_TITLE"));
    }

    if ($arIBTYPE["SECTIONS"] == "Y")
        $sSectionUrl = CIBlock::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section' => 0));
    else
        $sSectionUrl = Dedicated::GetAdminElementListLink($IBLOCK_ID, array('find_section_section' => 0));

    if (!defined("CATALOG_PRODUCT"))
    {
        $adminChain->AddItem(array(
            "TEXT" => htmlspecialcharsex($arIBlock["NAME"]),
            "LINK" => htmlspecialcharsbx($sSectionUrl),
        ));

        if ($find_section_section > 0)
            $sLastFolder = $sSectionUrl;
        else
            $sLastFolder = '';

        if ($find_section_section > 0)
        {
            $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $find_section_section);
            while ($ar_nav = $nav->GetNext())
            {
                $sSectionUrl = Dedicated::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section' => $ar_nav["ID"]));
                $adminChain->AddItem(array(
                    "TEXT" => $ar_nav["NAME"],
                    "LINK" => htmlspecialcharsbx($sSectionUrl),
                ));

                if ($ar_nav["ID"] != $find_section_section)
                    $sLastFolder = $sSectionUrl;
            }
        }
    }

    if ($bAutocomplete)
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
    elseif ($bPropertyAjax)
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");
    else
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if ($bVarsFromForm)
    {
        if (!isset($ACTIVE))
            $ACTIVE = "N"; //It is checkbox. So it is not set in POST.
        $DB->InitTableVarsForEdit("b_iblock_element", "", "str_");
        $str_IBLOCK_ELEMENT_SECTION = $IBLOCK_SECTION;
        $str_IPROPERTY_TEMPLATES = $_POST["IPROPERTY_TEMPLATES"];
    }
    elseif ($bPropertyAjax)
    {
        CUtil::JSPostUnescape();
    }

    if ($bPropertyAjax)
        $str_IBLOCK_ELEMENT_SECTION = $_REQUEST["IBLOCK_SECTION"];

    if (is_array($str_IBLOCK_ELEMENT_SECTION))
        $str_IBLOCK_ELEMENT_SECTION = array_unique($str_IBLOCK_ELEMENT_SECTION);

    $clearedByCopyProperties = array();
    if ($bCopy)
    {
        $clearedByCopyProperties = CIBlockPropertyTools::getClearedPropertiesID($IBLOCK_ID);
    }
    $arPROP_tmp = array();
    $properties = CIBlockProperty::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y", "CHECK_PERMISSIONS" => "N"));
    while ($prop_fields = $properties->Fetch())
    {
        $prop_values = array();
        $prop_values_with_descr = array();
        if (
                $bPropertyAjax && is_array($_POST["PROP"]) && array_key_exists($prop_fields["ID"], $_POST["PROP"])
        )
        {
            $prop_values = $_POST["PROP"][$prop_fields["ID"]];
            $prop_values_with_descr = $prop_values;
        } elseif ($bVarsFromForm)
        {
            if ($prop_fields["PROPERTY_TYPE"] == "F")
            {
                $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $WF_ID, "id", "asc", Array("ID" => $prop_fields["ID"], "EMPTY" => "N"));
                while ($res = $db_prop_values->Fetch())
                {
                    $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                    $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
                }
            } elseif (is_array($PROP))
            {
                if (array_key_exists($prop_fields["ID"], $PROP))
                    $prop_values = $PROP[$prop_fields["ID"]];
                else
                    $prop_values = $PROP[$prop_fields["CODE"]];
                $prop_values_with_descr = $prop_values;
            }
            else
            {
                $prop_values = "";
                $prop_values_with_descr = $prop_values;
            }
        } else
        {
            if ($historyId > 0)
            {
                $vx = $arResult["DOCUMENT"]["PROPERTIES"][(strlen(trim($prop_fields["CODE"])) > 0) ? $prop_fields["CODE"] : $prop_fields["ID"]];

                $prop_values = array();
                if (is_array($vx["VALUE"]) && is_array($vx["DESCRIPTION"]))
                {
                    for ($i = 0, $cnt = count($vx["VALUE"]); $i < $cnt; $i++)
                        $prop_values[] = array("VALUE" => $vx["VALUE"][$i], "DESCRIPTION" => $vx["DESCRIPTION"][$i]);
                } else
                {
                    $prop_values[] = array("VALUE" => $vx["VALUE"], "DESCRIPTION" => $vx["DESCRIPTION"]);
                }

                $prop_values_with_descr = $prop_values;
            } elseif ($ID > 0)
            {
                if (empty($clearedByCopyProperties) || !in_array($prop_fields["ID"], $clearedByCopyProperties))
                {
                    $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $WF_ID, "id", "asc", array("ID" => $prop_fields["ID"], "EMPTY" => "N"));
                    while ($res = $db_prop_values->Fetch())
                    {
                        if ($res["WITH_DESCRIPTION"] == "Y")
                            $prop_values[$res["PROPERTY_VALUE_ID"]] = array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
                        else
                            $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                        $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
                    }
                }
            }
        }

        $prop_fields["VALUE"] = $prop_values;
        $prop_fields["~VALUE"] = $prop_values_with_descr;
        if (strlen(trim($prop_fields["CODE"])) > 0)
            $arPROP_tmp[$prop_fields["CODE"]] = $prop_fields;
        else
            $arPROP_tmp[$prop_fields["ID"]] = $prop_fields;
    }
    $PROP = $arPROP_tmp;
    if ((int) $_REQUEST['generation_change'] > 0)
    {
        $generation_id = (int) $_REQUEST['generation_change'];
        $arFilter = array('=ID' => $generation_id);
    } elseif (!empty($PROP['GENERATION']['VALUE']))
    {
        $generation_id = current($PROP['GENERATION']['VALUE']);
        $arFilter = array('=ID' => $generation_id);
    } else
    {
        $arFilter = array('IBLOCK_ID' => $PROP['GENERATION']['LINK_IBLOCK_ID']);
    }

    $PROP['HDD']['PROPERTY_TYPE'] = 'E';
    $PROP['HDD']['USER_TYPE'] = 'EList';
    $PROP['HDD']['LIST_TYPE'] = 'L';
    $PROP['HDD']['USER_TYPE_SETTINGS'] = array(
         'size' => 1,
         'width' => 0,
         'group' => 'N',
         'multiple' => 'N',
    );
    $res = CIBlockElement::GetList(
                    array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter, false, array('nTopCount' => 1)
    );
    if ($ob = $res->GetNextElement())
    {
        $arPoperties = $ob->GetProperties();

        foreach ($PROP as $prop_code => $propCustom)
        {
            if ($arPoperties[$prop_code]['PROPERTY_TYPE'] == 'E')
            {
                $arPropValues[$propCustom['ID']] = array_values($arPoperties[$prop_code]['VALUE']);
                foreach ($arPropValues[$propCustom['ID']] as $value)
                {
                    $res_el = CIBlockElement::GetList(array(), array('=ID' => $value), false, false, array('ID', 'PROPERTY_PRICE'));
                    $ar_el = $res_el->Fetch();
                    $arPrices[$propCustom['ID']][$value] = $ar_el['PROPERTY_PRICE_VALUE'];
                }
            }
        }
        $form_factor = array(
            'VALUE' => $arPoperties['FORM_FACTOR']['VALUE'],
            'COUNT_HDD' => $arPoperties['FORM_FACTOR']['VALUE_SORT'],
        );
    }
    /*
    * В связи с тем, что свойство привязка к элементу не позволяет нам дублировать значения (1,1,2,3,3 сохраняется как 1,2,3)
    * используем свойство типа число и внешний вид придаем как у свойства типа привязка к элементу в виде списка
    */

    if ((int) $_REQUEST['generation_change'] > 0)
    {
        unset($PROP['GENERATION']);
        $APPLICATION->RestartBuffer();
        ?>
        <tr id="tr_FORM_FACTOR">
            <td width="40%" class="adm-detail-content-cell-l">
                <?= GetMessage('FORM_FACTOR_LABEL') ?>:
            </td>
            <td class="adm-detail-content-cell-r">
                <?= $form_factor['VALUE'] ?>
            </td>
        </tr>
        <?
        /*         * Characteristics */
        foreach ($PROP as $prop_code => $prop_fields):
            $prop_values = $prop_fields["VALUE"];
            ?>
            <tr id="tr_PROPERTY_<? echo $prop_fields["ID"]; ?>"<? if ($prop_fields["PROPERTY_TYPE"] == "F"): ?> class="adm-detail-file-row"<? endif ?>>
                <td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%"><? if ($prop_fields["HINT"] != ""):
                ?><span id="hint_<? echo $prop_fields["ID"]; ?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<? echo $prop_fields["ID"]; ?>'), '<? echo CUtil::JSEscape($prop_fields["HINT"]) ?>');</script>&nbsp;<? endif;
            ?>
                    <? if ($prop_fields['IS_REQUIRED'] == 'Y'): ?>
                        <span class="adm-required-field"><?= $prop_fields['NAME'] ?>:</span>
                    <? else: ?>
                        <?= $prop_fields['NAME'] ?>:
                    <? endif ?>
                </td>
                <td width="60%" class="adm-detail-content-cell-r">
                    <?_ShowPropertyField('PROP[' . $prop_fields["ID"] . ']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm || $bPropertyAjax, 50000, $tabControl->GetFormName(), $bCopy);?>
                </td>
            </tr>
            <?
            if ($prop_fields["PROPERTY_TYPE"] == "E" && $prop_fields['USER_TYPE'] == 'EList' && !empty($arPropValues[$prop_fields["ID"]])):
            // Удаляем лишние значения, чтобы не переписывать функцию вывода полей.
            ?>
            <script>
                var select = $('select[name^="PROP[<?= $prop_fields["ID"] ?>]"]'),
                        all_values = select.find('option');
                var ar_value = JSON.parse('<?= json_encode($arPropValues[$prop_fields["ID"]]) ?>');
                var arPrice = JSON.parse('<?= json_encode($arPrices[$prop_fields["ID"]]) ?>');
                all_values.each(function (index, value) {
                    var val = $(value).val();
                    $(value).attr('data-price', arPrice[val]);
                    if (!inArray(val, ar_value) && val != "") {
                        $(value).remove();
                    }
                });
            <? if ($prop_fields['CODE'] == 'HDD'): ?>
                var count_hdd = parseInt('<?= $form_factor['COUNT_HDD'] ?>');
                var container_hdd = $('#tr_PROPERTY_<?= $prop_fields["ID"] ?>');
                var button_add_hdd = container_hdd.find('input[type=button]');
                var selects_hdd = container_hdd.find('select');
                if (selects_hdd.length >= count_hdd)
                {
                    button_add_hdd.remove();
                }
                if (selects_hdd.length > count_hdd)
                {

                }
            <? endif ?>


            </script>
            <? endif ?>
            <?
            $hidden = "";
            if (!is_array($prop_fields["~VALUE"]))
            $values = Array(); else
            $values = $prop_fields["~VALUE"];
            $start = 1;
            foreach ($values as $key => $val)
            {
            if ($bCopy)
            {
            $key = "n" . $start;
            $start++;
        }

        if (is_array($val) && array_key_exists("VALUE", $val))
        {
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val["VALUE"]);
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', $val["DESCRIPTION"]);
        } else
        {
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val);
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', "");
        }
        }
    endforeach;
    die();
}


$aMenu = array();
if (!$bAutocomplete && !$bPropertyAjax)
{
    $aMenu = array(
        array(
            "TEXT" => htmlspecialcharsex($arIBlock["ELEMENTS_NAME"]),
            "LINK" => Dedicated::GetAdminElementListLink($IBLOCK_ID, array('find_section_section' => intval($find_section_section))),
            "ICON" => "btn_list",
        )
    );

    if (!$bCopy)
    {
        $aMenu[] = array(
            "TEXT" => GetMessage("IBEL_E_COPY_ELEMENT"),
            "TITLE" => GetMessage("IBEL_E_COPY_ELEMENT_TITLE"),
            "LINK" => "/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, $ID, array(
                "IBLOCK_SECTION_ID" => $MENU_SECTION_ID,
                "find_section_section" => intval($find_section_section),
                "action" => "copy",
            )),
            "ICON" => "btn_copy",
        );
    }

    if ($ID > 0 && !$bCopy)
    {
        $arSubMenu = array();
        $arSubMenu[] = array(
            "TEXT" => htmlspecialcharsex($arIBlock["ELEMENT_ADD"]),
            "LINK" => "/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, 0, array(
                "IBLOCK_SECTION_ID" => $MENU_SECTION_ID,
                "find_section_section" => intval($find_section_section),
            )),
            'ICON' => '',
        );
        if (CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_delete"))
        {
            $urlDelete = Dedicated::GetAdminElementListLink($IBLOCK_ID, array('find_section_section' => intval($find_section_section), 'action' => 'delete'));
            $urlDelete .= '&' . bitrix_sessid_get();
            $urlDelete .= '&ID=' . (preg_match('/^iblock_list_admin\.php/', $urlDelete) ? "E" : "") . $ID;
            $arSubMenu[] = array(
                "TEXT" => htmlspecialcharsex($arIBlock["ELEMENT_DELETE"]),
                "ACTION" => "if(confirm('" . GetMessageJS("IBLOCK_ELEMENT_DEL_CONF") . "'))window.location='" . CUtil::JSEscape($urlDelete) . "';",
                'ICON' => 'delete',
            );
        }

        if ($bWorkflow && !defined("CATALOG_PRODUCT"))
        {
            $arSubMenu[] = array(
                "TEXT" => GetMessage("IBEL_HIST"),
                "LINK" => '/bitrix/admin/iblock_history_list.php?ELEMENT_ID=' . $ID . '&type=' . urlencode($arIBlock["IBLOCK_TYPE_ID"]) . '&lang=' . LANGUAGE_ID . '&IBLOCK_ID=' . $IBLOCK_ID . '&find_section_section=' . intval($find_section_section),
            );
        }

        if (!empty($arSubMenu))
        {
            $aMenu[] = array("SEPARATOR" => "Y");
            $aMenu[] = array(
                "TEXT" => GetMessage('IBEL_E_ACTIONS'),
                "TITLE" => GetMessage('IBEL_E_ACTIONS_TITLE'),
                "MENU" => $arSubMenu,
                'ICON' => 'btn_new'
            );
        }
    }

    $context = new CAdminContextMenu($aMenu);
    $context->Show();
}

if ($error)
    CAdminMessage::ShowOldStyleError($error->GetErrorText());

$bFileman = CModule::IncludeModule("fileman");
$arTranslit = $arIBlock["FIELDS"]["CODE"]["DEFAULT_VALUE"];
$bLinked = (!strlen($str_TIMESTAMP_X) || $bCopy) && $_POST["linked_state"] !== 'N';

if (strlen($arIBlock["EDIT_FILE_AFTER"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBlock["EDIT_FILE_AFTER"])):
    include($_SERVER["DOCUMENT_ROOT"] . $arIBlock["EDIT_FILE_AFTER"]);
    $_SESSION['IBLOCK_CUSTOM_FORM'] = true;
elseif (strlen($arIBTYPE["EDIT_FILE_AFTER"]) > 0 && is_file($_SERVER["DOCUMENT_ROOT"] . $arIBTYPE["EDIT_FILE_AFTER"])):
    include($_SERVER["DOCUMENT_ROOT"] . $arIBTYPE["EDIT_FILE_AFTER"]);
    $_SESSION['IBLOCK_CUSTOM_FORM'] = true;
else:
    ?>

    <?
    //////////////////////////
    //START of the custom form
    //////////////////////////
    //We have to explicitly call calendar and editor functions because
    //first output may be discarded by form settings

    $tabControl->BeginPrologContent();
    echo CAdminCalendar::ShowScript();

    if (COption::GetOptionString("iblock", "use_htmledit", "Y") == "Y" && $bFileman)
    {
        //TODO:This dirty hack will be replaced by special method like calendar do
        echo '<div style="display:none">';
        CFileMan::AddHTMLEditorFrame(
                "SOME_TEXT", "", "SOME_TEXT_TYPE", "text", array(
            'height' => 450,
            'width' => '100%'
                ), "N", 0, "", "", $arIBlock["LID"]
        );
        echo '</div>';
    }

    if ($arTranslit["TRANSLITERATION"] == "Y")
    {
        CJSCore::Init(array('translit'));
        ?>
        <script type="text/javascript">
            var linked =<?
        if ($bLinked)
            echo 'true';
        else
            echo 'false';
        ?>;
            function set_linked()
            {
                linked = !linked;

                var name_link = document.getElementById('name_link');
                if (name_link)
                {
                    if (linked)
                        name_link.src = '/bitrix/themes/.default/icons/iblock/link.gif';
                    else
                        name_link.src = '/bitrix/themes/.default/icons/iblock/unlink.gif';
                }
                var code_link = document.getElementById('code_link');
                if (code_link)
                {
                    if (linked)
                        code_link.src = '/bitrix/themes/.default/icons/iblock/link.gif';
                    else
                        code_link.src = '/bitrix/themes/.default/icons/iblock/unlink.gif';
                }
                var linked_state = document.getElementById('linked_state');
                if (linked_state)
                {
                    if (linked)
                        linked_state.value = 'Y';
                    else
                        linked_state.value = 'N';
                }
            }
            var oldValue = '';
            function transliterate()
            {
                if (linked)
                {
                    var from = document.getElementById('NAME');
                    var to = document.getElementById('CODE');
                    if (from && to && oldValue != from.value)
                    {
                        BX.translit(from.value, {
                            'max_len': <? echo intval($arTranslit['TRANS_LEN']) ?>,
                            'change_case': '<? echo $arTranslit['TRANS_CASE'] ?>',
                            'replace_space': '<? echo $arTranslit['TRANS_SPACE'] ?>',
                            'replace_other': '<? echo $arTranslit['TRANS_OTHER'] ?>',
                            'delete_repeat_replace': <? echo $arTranslit['TRANS_EAT'] == 'Y' ? 'true' : 'false' ?>,
                            'use_google': <? echo $arTranslit['USE_GOOGLE'] == 'Y' ? 'true' : 'false' ?>,
                            'callback': function (result) {
                                to.value = result;
                                setTimeout('transliterate()', 250);
                            }
                        });
                        oldValue = from.value;
                    }
                    else
                    {
                        setTimeout('transliterate()', 250);
                    }
                }
                else
                {
                    setTimeout('transliterate()', 250);
                }
            }
            transliterate();
        </script>
        <?
    }
    ?>
    <script type="text/javascript">
        var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
                '<? echo $tabControl->GetName() ?>_form',
                '/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<? echo intval($IBLOCK_ID) ?>&ENTITY_ID=<? echo intval($ID) ?>'
                );
        BX.ready(function () {
            setTimeout(function () {
                InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
            }, 1000);
        });
    </script>
    <?
    $tabControl->EndPrologContent();

    $tabControl->BeginEpilogContent();

    echo bitrix_sessid_post();
    echo GetFilterHiddens("find_");
    ?>
    <input type="hidden" name="linked_state" id="linked_state" value="<?
    if ($bLinked)
        echo 'Y';
    else
        echo 'N';
    ?>">
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="from" value="<? echo htmlspecialcharsbx($from) ?>">
    <input type="hidden" name="WF" value="<? echo htmlspecialcharsbx($WF) ?>">
    <input type="hidden" name="return_url" value="<? echo htmlspecialcharsbx($return_url) ?>">
    <?
    if ($ID > 0 && !$bCopy)
    {
        ?><input type="hidden" name="ID" value="<? echo $ID ?>"><?
    }
    if ($bCopy)
    {
        ?><input type="hidden" name="copyID" value="<? echo $ID; ?>"><?
    }
    if ($bCatalog)
        CCatalogAdminTools::showFormParams();
    ?>
    <input type="hidden" name="IBLOCK_SECTION_ID" value="<? echo intval($IBLOCK_SECTION_ID) ?>">
    <input type="hidden" name="TMP_ID" value="<? echo intval($TMP_ID) ?>">
    <script>

        jQuery(document).ready(function ($) {
            var section_delimentr = $('[id*="IBLOCK_ELEMENT_PROP_VALUE"]');
            /* ajax load property*/
            $('#tr_PROPERTY_<?= $PROP['GENERATION']['ID'] ?> select').on('change', '', function (event) {
                var properties_form = section_delimentr.nextAll();
                var generation_id = $(this).val();
                $.ajax({
                    url: '<?= $APPLICATION->GetCurPageParam('generation_change=\'+generation_id+\''); ?>',
                    success: function (res) {
                        properties_form.remove();
                        $(res).insertAfter(section_delimentr);
                    }
                }
                );
            });
            //recalc summary
            function recalc() {
                var prices = $('select[name^=PROP] option[data-price]:selected');
                var sum = 0;
                prices.each(function (i, val) {
                    var value = parseInt($(val).attr('data-price'));
                    sum += value;

                });
                $('#CAT_BASE_PRICE').text(sum);
                $('#CAT_BASE_PRICE_HIDDEN').val(sum);
            }
            recalc();
            $(document).on('change', 'select[name^="PROP["]', function (event) {
                recalc();
            });
        });
    </script>
    <?
    $tabControl->EndEpilogContent();

    $customTabber->SetErrorState($bVarsFromForm);

    $arEditLinkParams = array(
        "find_section_section" => intval($find_section_section)
    );
    if ($bAutocomplete)
    {
        $arEditLinkParams['lookup'] = $strLookup;
    }

    $tabControl->Begin(array(
        "FORM_ACTION" => "/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
    ));

    $tabControl->BeginNextFormTab();
    if ($ID > 0 && !$bCopy)
    {
        $p = CIblockElement::GetByID($ID);
        $pr = $p->ExtractFields("prn_");
    } else
    {
        $pr = array();
    }
    $tabControl->AddCheckBoxField("ACTIVE", GetMessage("IBLOCK_FIELD_ACTIVE") . ":", false, array("Y", "N"), $str_ACTIVE == "Y");

    if ($arTranslit["TRANSLITERATION"] == "Y")
    {
        $tabControl->BeginCustomField("NAME", GetMessage("IBLOCK_FIELD_NAME") . ":", true);
        ?>
        <tr id="tr_NAME">
            <td><? echo $tabControl->GetCustomLabelHTML() ?></td>
            <td style="white-space: nowrap;">
                <input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<? echo $str_NAME ?>"><image id="name_link" title="<? echo GetMessage("IBEL_E_LINK_TIP") ?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?
                if ($bLinked)
                    echo 'link.gif';
                else
                    echo 'unlink.gif';
                ?>" onclick="set_linked()" />
            </td>
        </tr>
        <?
        $tabControl->EndCustomField("NAME", '<input type="hidden" name="NAME" id="NAME" value="' . $str_NAME . '">'
        );

        $tabControl->BeginCustomField("CODE", GetMessage("IBLOCK_FIELD_CODE") . ":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y");
        ?>
        <tr id="tr_CODE">
            <td><? echo $tabControl->GetCustomLabelHTML() ?></td>
            <td style="white-space: nowrap;">
                <input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<? echo $str_CODE ?>"><image id="code_link" title="<? echo GetMessage("IBEL_E_LINK_TIP") ?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?
                if ($bLinked)
                    echo 'link.gif';
                else
                    echo 'unlink.gif';
                ?>" onclick="set_linked()" />
            </td>
        </tr>
        <?
        $tabControl->EndCustomField("CODE", '<input type="hidden" name="CODE" id="CODE" value="' . $str_CODE . '">'
        );
    }
    else
    {
        $tabControl->AddEditField("NAME", GetMessage("IBLOCK_FIELD_NAME") . ":", true, array("size" => 50, "maxlength" => 255), $str_NAME);
        $tabControl->AddEditField("CODE", GetMessage("IBLOCK_FIELD_CODE") . ":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_CODE);
    }

//        if (COption::GetOptionString("iblock", "show_xml_id", "N") == "Y")
//            $tabControl->AddEditField("XML_ID", GetMessage("IBLOCK_FIELD_XML_ID") . ":", $arIBlock["FIELDS"]["XML_ID"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_XML_ID);

    $tabControl->AddEditField("SORT", GetMessage("IBLOCK_FIELD_SORT") . ":", $arIBlock["FIELDS"]["SORT"]["IS_REQUIRED"] === "Y", array("size" => 7, "maxlength" => 10), $str_SORT);
    $tabControl->BeginCustomField("CAT_BASE_PRICE", GetMessage("BASE_PRICE") . ":", true);
    ?>
    <tr id="tr_BASE_PRICE" style="display: <? echo ($bUseExtendedPrice ? 'none' : 'table-row'); ?>;">
        <td width="40%">
            <?
            $PRODUCT_ID = ($ID > 0 ? CIBlockElement::GetRealElement($ID) : 0);
            $arBaseGroup = CCatalogGroup::GetBaseGroup();
            $arBasePrice = CPrice::GetBasePrice($PRODUCT_ID);
            echo GetMessage("BASE_PRICE");
            ?>:
        </td>
        <td width="60%">
            <?
            $boolBaseExistPrice = false;
            $str_CAT_BASE_PRICE_5 = "";
            if ($arBasePrice)
                $str_CAT_BASE_PRICE = $arBasePrice["PRICE"];
            if ($bVarsFromForm)
                $str_CAT_BASE_PRICE = $CAT_PRICE_5;
            if (trim($str_CAT_BASE_PRICE_5) != '' && doubleval($str_CAT_BASE_PRICE_5) >= 0)
                $boolBaseExistPrice = true;
            ?>
            <input type="hidden" id="CAT_BASE_PRICE_HIDDEN" name="CAT_BASE_PRICE_5" value="<? echo htmlspecialcharsbx($str_CAT_BASE_PRICE) ?>" size="30">
            <div id="CAT_BASE_PRICE" style="font-weight:700">
                <? echo htmlspecialcharsbx($str_CAT_BASE_PRICE) ?>
            </div>
            <?
            if ($arBasePrice)
                $str_CAT_BASE_CURRENCY = $arBasePrice["CURRENCY"];
            if ($bVarsFromForm)
                $str_CAT_BASE_CURRENCY = $CAT_BASE_CURRENCY_5;
            if (empty($str_CAT_BASE_CURRENCY))
            {
                $str_CAT_BASE_CURRENCY = "RUB";
            }
            ?>

            <input type="hidden" name="CAT_BASE_CURRENCY_5" value="<?= htmlspecialcharsbx($str_CAT_BASE_CURRENCY) ?>">
            <input type="hidden" name="price_useextform" value="Y">
            <input type="hidden" name="CAT_ROW_COUNTER" value="5">

            <?
            if ($arBasePrice)
            {
                $str_CAT_BASE_ID = $arBasePrice["ID"];
            }
            $boolBaseExistPrice = true;
            ?>
            <input type="hidden" name="CAT_BASE_QUANTITY_FROM_5" value="0">
            <input type="hidden" name="CAT_BASE_QUANTITY_TO_5" value="">

            <input type="hidden" name="CAT_BASE_ID[5]" value="<?= intval($str_CAT_BASE_ID) ?>">
            <input type="hidden" name="CAT_PRICE_EXIST_5" id="CAT_PRICE_EXIST" value="<? echo ($boolBaseExistPrice == true ? 'Y' : 'N'); ?>">
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("CAT_BASE_PRICE");
    if (!empty($PROP)):
        if ($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT"))
        {
            $arPropLinks = array("IBLOCK_ELEMENT_PROP_VALUE");
            if (is_array($str_IBLOCK_ELEMENT_SECTION) && !empty($str_IBLOCK_ELEMENT_SECTION))
            {
                foreach ($str_IBLOCK_ELEMENT_SECTION as $section_id)
                {
                    foreach (CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, $section_id) as $PID => $arLink)
                        $arPropLinks[$PID] = "PROPERTY_" . $PID;
                }
            } else
            {
                foreach (CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, 0) as $PID => $arLink)
                    $arPropLinks[$PID] = "PROPERTY_" . $PID;
            }
            $tabControl->AddFieldGroup("IBLOCK_ELEMENT_PROPERTY", GetMessage("CHARACTERISTICS"), array_merge($arPropLinks, 'FORM_FACTOR'), $bPropertyAjax);
        }
        $tabControl->AddSection("GENERATION", GetMessage("GENERATION"));
        $prop_fields = $PROP['GENERATION'];
        $prop_values = $prop_fields["VALUE"];
        $tabControl->BeginCustomField("PROPERTY_" . $prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"] === "Y");

        $tabControl->EndCustomField("PROPERTY_" . $prop_fields["ID"], $hidden);

        $tabControl->AddSection("IBLOCK_ELEMENT_PROP_VALUE", GetMessage("CHARACTERISTICS"));
        $tabControl->AddViewField('FORM_FACTOR', GetMessage('FORM_FACTOR_LABEL') . ":", $form_factor['VALUE']);
        
        foreach ($PROP as $prop_code => $prop_fields):
            $prop_values = $prop_fields["VALUE"];
            $tabControl->BeginCustomField("PROPERTY_" . $prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"] === "Y");
            ?>
            <tr id="tr_PROPERTY_<? echo $prop_fields["ID"]; ?>"<? if ($prop_fields["PROPERTY_TYPE"] == "F"): ?> class="adm-detail-file-row"<? endif ?>>
                <td class="adm-detail-valign-top" width="40%"><? if ($prop_fields["HINT"] != ""):
                ?><span id="hint_<? echo $prop_fields["ID"]; ?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<? echo $prop_fields["ID"]; ?>'), '<? echo CUtil::JSEscape($prop_fields["HINT"]) ?>');</script>&nbsp;<? endif;
            ?><? echo $tabControl->GetCustomLabelHTML(); ?>:</td>
                <td width="60%"><? _ShowPropertyField('PROP[' . $prop_fields["ID"] . ']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm || $bPropertyAjax, 50000, $tabControl->GetFormName(), $bCopy); ?></td>
            </tr>
            <?
            $hidden = "";
            if (!is_array($prop_fields["~VALUE"]))
                $values = Array();
            else
                $values = $prop_fields["~VALUE"];
            $start = 1;
            foreach ($values as $key => $val)
            {
                if ($bCopy)
                {
                    $key = "n" . $start;
                    $start++;
                }

                if (is_array($val) && array_key_exists("VALUE", $val))
                {
                    $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val["VALUE"]);
                    $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', $val["DESCRIPTION"]);
                } else
                {
                    $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val);
                    $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', "");
                }
            }
            if ($prop_fields["PROPERTY_TYPE"] == "E" && $prop_fields['USER_TYPE'] == 'EList' && !empty($arPropValues[$prop_fields["ID"]])):
                // Удаляем лишние значения, чтобы не переписывать функцию вывода полей.
                ?>
                <script>
                    var select = $('select[name^="PROP[<?= $prop_fields["ID"] ?>]"]'),
                            all_values = select.find('option');
                    var ar_value = JSON.parse('<?= json_encode($arPropValues[$prop_fields["ID"]]) ?>');
                    var arPrice = JSON.parse('<?= json_encode($arPrices[$prop_fields["ID"]]) ?>');
                    all_values.each(function (index, value) {
                        var val = $(value).val();
                        $(value).attr('data-price', arPrice[val]);
                        if (!inArray(val, ar_value) && val != "") {
                            $(value).remove();
                        }

                    });
                <?
                if ($prop_fields['CODE'] == 'HDD'):
                    //ограничиваем количество hdd
                    ?>
                        var count_hdd = parseInt('<?= $form_factor['COUNT_HDD'] ?>');
                        var container_hdd = $('#tr_PROPERTY_<?= $prop_fields["ID"] ?>');
                        var button_add_hdd = container_hdd.find('input[type=button]');
                        var selects_hdd = container_hdd.find('select');
                        if (selects_hdd.length >= count_hdd)
                        {
                            button_add_hdd.remove();
                        }
                        if (selects_hdd.length > count_hdd)
                        {

                        }
                <? endif ?>
                </script>
                <?
            endif;
            $tabControl->EndCustomField("PROPERTY_" . $prop_fields["ID"], $hidden);
        endforeach;
        ?>
        <?
    endif;

    if (!$bAutocomplete)
    {
        $rsLinkedProps = CIBlockProperty::GetList(array(), array(
                    "PROPERTY_TYPE" => "E",
                    "LINK_IBLOCK_ID" => $IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "FILTRABLE" => "Y",
        ));
        $arLinkedProp = $rsLinkedProps->GetNext();
        if ($arLinkedProp)
        {
            $tabControl->BeginCustomField("LINKED_PROP", GetMessage("IBLOCK_ELEMENT_EDIT_LINKED"));
            ?>
            <tr class="heading" id="tr_LINKED_PROP">
                <td colspan="2"><? echo $tabControl->GetCustomLabelHTML(); ?></td>
            </tr>
            <?
            do
            {
                $elements_name = CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "ELEMENTS_NAME");
                if (strlen($elements_name) <= 0)
                    $elements_name = GetMessage("IBLOCK_ELEMENT_EDIT_ELEMENTS");
                ?>
                <tr id="tr_LINKED_PROP<? echo $arLinkedProp["ID"] ?>">
                    <td colspan="2"><a href="<? echo htmlspecialcharsbx(Dedicated::GetAdminElementListLink($arLinkedProp["IBLOCK_ID"], array('set_filter' => 'Y', 'find_el_property_' . $arLinkedProp["ID"] => $ID, 'find_section_section' => -1))) ?>"><? echo CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "NAME") . ": " . $elements_name ?></a></td>
                </tr>
                <?
            } while ($arLinkedProp = $rsLinkedProps->GetNext());
            $tabControl->EndCustomField("LINKED_PROP", "");
        }
    }
    if ($arShowTabs['sections']):
        $tabControl->BeginNextFormTab();
        require $moduleDir . '/admin/admin_pages/form_elements/sections.php';

    endif;


    if ($arShowTabs['edit_rights']):
        $tabControl->BeginNextFormTab();
        if ($ID > 0)
        {
            $obRights = new CIBlockElementRights($IBLOCK_ID, $ID);
            $htmlHidden = '';
            foreach ($obRights->GetRights() as $RIGHT_ID => $arRight)
                $htmlHidden .= '
				<input type="hidden" name="RIGHTS[][RIGHT_ID]" value="' . htmlspecialcharsbx($RIGHT_ID) . '">
				<input type="hidden" name="RIGHTS[][GROUP_CODE]" value="' . htmlspecialcharsbx($arRight["GROUP_CODE"]) . '">
				<input type="hidden" name="RIGHTS[][TASK_ID]" value="' . htmlspecialcharsbx($arRight["TASK_ID"]) . '">
			';
        } else
        {
            $obRights = new CIBlockSectionRights($IBLOCK_ID, $MENU_SECTION_ID);
            $htmlHidden = '';
        }

        $tabControl->BeginCustomField("RIGHTS", GetMessage("IBEL_E_RIGHTS_FIELD"));
        IBlockShowRights(
                'element', $IBLOCK_ID, $ID, GetMessage("IBEL_E_RIGHTS_SECTION_TITLE"), "RIGHTS", $obRights->GetRightsList(), $obRights->GetRights(array("count_overwrited" => true, "parents" => $str_IBLOCK_ELEMENT_SECTION)), false, /* $bForceInherited= */ ($ID <= 0) || $bCopy
        );
        $tabControl->EndCustomField("RIGHTS", $htmlHidden);
    endif;

    $bDisabled = ($view == "Y") || ($bWorkflow && $prn_LOCK_STATUS == "red") || (
            (($ID <= 0) || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
            ) || (
            (($ID > 0) && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
            ) || (
            $bBizproc && !$canWrite
            )
    ;

    if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):
        ob_start();
        ?>
        <input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="adm-btn-save" name="save" id="save" value="<? echo GetMessage("IBLOCK_EL_SAVE") ?>">
        <?
        if (!$bAutocomplete)
        {
            ?><input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="button" name="apply" id="apply" value="<? echo GetMessage('IBLOCK_APPLY') ?>"><?
        }
        ?>
        <input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="button" name="dontsave" id="dontsave" value="<? echo GetMessage("IBLOCK_EL_CANC") ?>">
        <?
        if (!$bAutocomplete)
        {
            ?><input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<? echo GetMessage("IBLOCK_EL_SAVE_AND_ADD") ?>"><?
        }
        $buttons_add_html = ob_get_contents();
        ob_end_clean();
        $tabControl->Buttons(false, $buttons_add_html);
    elseif (!$bPropertyAjax && $nobuttons !== "Y"):

        $wfClose = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC")) . "',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
        $save_and_add = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_SAVE_AND_ADD")) . "',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
        $cancel = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC")) . "',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
        $tabControl->ButtonsPublic(array(
            '.btnSave',
            ($ID > 0 && $bWorkflow ? $wfClose : $cancel),
            $save_and_add,
        ));
    endif;

    $tabControl->Show();
    if (
            (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1) && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_edit") && !$bAutocomplete
    )
    {

        echo
        BeginNote(),
        GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
        ' <a href="/bitrix/admin/iblock_edit.php?type=' . htmlspecialcharsbx($type) . '&amp;lang=' . LANGUAGE_ID . '&amp;ID=' . $IBLOCK_ID . '&amp;admin=Y&amp;return_url=' . urlencode("/bitrix/admin/" . Dedicated::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF == "Y" ? "Y" : null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url) > 0 ? $return_url : null)))) . '">',
        GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
        '</a>',
        EndNote()
        ;
    }
//////////////////////////
//END of the custom form
//////////////////////////
endif;
}
if ($bAutocomplete)
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
elseif ($bPropertyAjax)
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin_js.php");
else
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
