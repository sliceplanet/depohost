<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$tabControl->BeginCustomField("SECTIONS", GetMessage("IBLOCK_SECTION"), $arIBlock["FIELDS"]["IBLOCK_SECTION"]["IS_REQUIRED"] === "Y");
?>
<tr id="tr_SECTIONS">
    <? if ($arIBlock["SECTION_CHOOSER"] != "D" && $arIBlock["SECTION_CHOOSER"] != "P"): ?>

        <? $l = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL")); ?>
        <td width="40%" class="adm-detail-valign-top"><? echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%">
            <select name="IBLOCK_SECTION[]" size="14" multiple onchange="onSectionChanged()">
                <option value="0"<? if (is_array($str_IBLOCK_ELEMENT_SECTION) && in_array(0, $str_IBLOCK_ELEMENT_SECTION)) echo " selected" ?>><? echo GetMessage("IBLOCK_UPPER_LEVEL") ?></option>
                <?
                while ($ar_l = $l->GetNext()):
                    ?><option value="<? echo $ar_l["ID"] ?>"<? if (is_array($str_IBLOCK_ELEMENT_SECTION) && in_array($ar_l["ID"], $str_IBLOCK_ELEMENT_SECTION)) echo " selected" ?>><? echo str_repeat(" . ", $ar_l["DEPTH_LEVEL"]) ?><? echo $ar_l["NAME"] ?></option><?
                endwhile;
                ?>
            </select>
        </td>

    <? elseif ($arIBlock["SECTION_CHOOSER"] == "D"): ?>
        <td width="40%" class="adm-detail-valign-top"><? echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%">
            <table class="internal" id="sections">
                <?
                if (is_array($str_IBLOCK_ELEMENT_SECTION))
                {
                    $i = 0;
                    foreach ($str_IBLOCK_ELEMENT_SECTION as $section_id)
                    {
                        $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                        $strPath = "";
                        while ($arChain = $rsChain->Fetch())
                            $strPath .= $arChain["NAME"] . "&nbsp;/&nbsp;";
                        if (strlen($strPath) > 0)
                        {
                            ?><tr>
                                <td nowrap><? echo $strPath ?></td>
                                <td>
                                    <input type="button" value="<? echo GetMessage("MAIN_DELETE") ?>" OnClick="deleteRow(this)">
                                    <input type="hidden" name="IBLOCK_SECTION[]" value="<? echo intval($section_id) ?>">
                                </td>
                            </tr><?
                        }
                        $i++;
                    }
                }
                ?>
                <tr>
                    <td>
                        <script type="text/javascript">
                            function deleteRow(button)
                            {
                                var my_row = button.parentNode.parentNode;
                                var table = document.getElementById('sections');
                                if (table)
                                {
                                    for (var i = 0; i < table.rows.length; i++)
                                    {
                                        if (table.rows[i] == my_row)
                                        {
                                            table.deleteRow(i);
                                            onSectionChanged();
                                        }
                                    }
                                }
                            }
                            function addPathRow()
                            {
                                var table = document.getElementById('sections');
                                if (table)
                                {
                                    var section_id = 0;
                                    var html = '';
                                    var lev = 0;
                                    var oSelect;
                                    while (oSelect = document.getElementById('select_IBLOCK_SECTION_' + lev))
                                    {
                                        if (oSelect.value < 1)
                                            break;
                                        html += oSelect.options[oSelect.selectedIndex].text + '&nbsp;/&nbsp;';
                                        section_id = oSelect.value;
                                        lev++;
                                    }
                                    if (section_id > 0)
                                    {
                                        var cnt = table.rows.length;
                                        var oRow = table.insertRow(cnt - 1);

                                        var i = 0;
                                        var oCell = oRow.insertCell(i++);
                                        oCell.innerHTML = html;

                                        var oCell = oRow.insertCell(i++);
                                        oCell.innerHTML =
                                                '<input type="button" value="<? echo GetMessage("MAIN_DELETE") ?>" OnClick="deleteRow(this)">' +
                                                '<input type="hidden" name="IBLOCK_SECTION[]" value="' + section_id + '">';
                                        onSectionChanged();
                                    }
                                }
                            }
                            function find_path(item, value)
                            {
                                if (item.id == value)
                                {
                                    var a = Array(1);
                                    a[0] = item.id;
                                    return a;
                                }
                                else
                                {
                                    for (var s in item.children)
                                    {
                                        if (ar = find_path(item.children[s], value))
                                        {
                                            var a = Array(1);
                                            a[0] = item.id;
                                            return a.concat(ar);
                                        }
                                    }
                                    return null;
                                }
                            }
                            function find_children(level, value, item)
                            {
                                if (level == -1 && item.id == value)
                                    return item;
                                else
                                {
                                    for (var s in item.children)
                                    {
                                        if (ch = find_children(level - 1, value, item.children[s]))
                                            return ch;
                                    }
                                    return null;
                                }
                            }
                            function change_selection(name_prefix, prop_id, value, level, id)
                            {
                                var lev = level + 1;
                                var oSelect;

                                while (oSelect = document.getElementById(name_prefix + lev))
                                {
                                    jsSelectUtils.deleteAllOptions(oSelect);
                                    jsSelectUtils.addNewOption(oSelect, '0', '(<? echo GetMessage("MAIN_NO") ?>)');
                                    lev++;
                                }

                                oSelect = document.getElementById(name_prefix + (level + 1))
                                if (oSelect && (value != 0 || level == -1))
                                {
                                    var item = find_children(level, value, window['sectionListsFor' + prop_id]);
                                    for (var s in item.children)
                                    {
                                        var obj = item.children[s];
                                        jsSelectUtils.addNewOption(oSelect, obj.id, obj.name, true);
                                    }
                                }
                                if (document.getElementById(id))
                                    document.getElementById(id).value = value;
                            }
                            function init_selection(name_prefix, prop_id, value, id)
                            {
                                var a = find_path(window['sectionListsFor' + prop_id], value);
                                change_selection(name_prefix, prop_id, 0, -1, id);
                                for (var i = 1; i < a.length; i++)
                                {
                                    if (oSelect = document.getElementById(name_prefix + (i - 1)))
                                    {
                                        for (var j = 0; j < oSelect.length; j++)
                                        {
                                            if (oSelect[j].value == a[i])
                                            {
                                                oSelect[j].selected = true;
                                                break;
                                            }
                                        }
                                    }
                                    change_selection(name_prefix, prop_id, a[i], i - 1, id);
                                }
                            }
                            var sectionListsFor0 = {id: 0, name: '', children: Array()};

    <?
    $rsItems = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
    $depth = 0;
    $max_depth = 0;
    $arChain = array();
    while ($arItem = $rsItems->GetNext())
    {
        if ($max_depth < $arItem["DEPTH_LEVEL"])
        {
            $max_depth = $arItem["DEPTH_LEVEL"];
        }
        if ($depth < $arItem["DEPTH_LEVEL"])
        {
            $arChain[] = $arItem["ID"];
        }
        while ($depth > $arItem["DEPTH_LEVEL"])
        {
            array_pop($arChain);
            $depth--;
        }
        $arChain[count($arChain) - 1] = $arItem["ID"];
        echo "sectionListsFor0";
        foreach ($arChain as $i)
            echo ".children['" . intval($i) . "']";

        echo " = { id : " . $arItem["ID"] . ", name : '" . CUtil::JSEscape($arItem["NAME"]) . "', children : Array() };\n";
        $depth = $arItem["DEPTH_LEVEL"];
    }
    ?>
                        </script>
                        <?
                        for ($i = 0; $i < $max_depth; $i++)
                            echo '<select id="select_IBLOCK_SECTION_' . $i . '" onchange="change_selection(\'select_IBLOCK_SECTION_\',  0, this.value, ' . $i . ', \'IBLOCK_SECTION[n' . $key . ']\')"><option value="0">(' . GetMessage("MAIN_NO") . ')</option></select>&nbsp;';
                        ?>
                        <script type="text/javascript">
                            init_selection('select_IBLOCK_SECTION_', 0, '', 0);
                        </script>
                    </td>
                    <td><input type="button" value="<? echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD") ?>" onClick="addPathRow()"></td>
                </tr>
            </table>
        </td>

    <? else: ?>
        <td width="40%" class="adm-detail-valign-top"><? echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%">
            <table id="sections" class="internal">
                <?
                if (is_array($str_IBLOCK_ELEMENT_SECTION))
                {
                    $i = 0;
                    foreach ($str_IBLOCK_ELEMENT_SECTION as $section_id)
                    {
                        $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                        $strPath = "";
                        while ($arChain = $rsChain->GetNext())
                            $strPath .= $arChain["NAME"] . "&nbsp;/&nbsp;";
                        if (strlen($strPath) > 0)
                        {
                            ?><tr>
                                <td><? echo $strPath ?></td>
                                <td>
                                    <input type="button" value="<? echo GetMessage("MAIN_DELETE") ?>" OnClick="deleteRow(this)">
                                    <input type="hidden" name="IBLOCK_SECTION[]" value="<? echo intval($section_id) ?>">
                                </td>
                            </tr><?
                        }
                        $i++;
                    }
                }
                ?>
            </table>
            <script type="text/javascript">
                function deleteRow(button)
                {
                    var my_row = button.parentNode.parentNode;
                    var table = document.getElementById('sections');
                    if (table)
                    {
                        for (var i = 0; i < table.rows.length; i++)
                        {
                            if (table.rows[i] == my_row)
                            {
                                table.deleteRow(i);
                                onSectionChanged();
                            }
                        }
                    }
                }
                function InS<? echo md5("input_IBLOCK_SECTION") ?>(section_id, html)
                {
                    var table = document.getElementById('sections');
                    if (table)
                    {
                        if (section_id > 0 && html)
                        {
                            var cnt = table.rows.length;
                            var oRow = table.insertRow(cnt - 1);

                            var i = 0;
                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML = html;

                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML =
                                    '<input type="button" value="<? echo GetMessage("MAIN_DELETE") ?>" OnClick="deleteRow(this)">' +
                                    '<input type="hidden" name="IBLOCK_SECTION[]" value="' + section_id + '">';
                            onSectionChanged();
                        }
                    }
                }
            </script>
            <input name="input_IBLOCK_SECTION" id="input_IBLOCK_SECTION" type="hidden">
            <input type="button" value="<? echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD") ?>..." onClick="jsUtils.OpenWindow('/bitrix/admin/iblock_section_search.php?lang=<? echo LANGUAGE_ID ?>&amp;IBLOCK_ID=<? echo $IBLOCK_ID ?>&amp;n=input_IBLOCK_SECTION&amp;m=y', 600, 500);">
        </td>
    <? endif; ?>
</tr>
<input type="hidden" name="IBLOCK_SECTION[]" value="">
<script type="text/javascript">
    function onSectionChanged()
    {
        var form = BX('<? echo CUtil::JSEscape($tabControl->GetFormName()) ?>');
        var url = '<? echo CUtil::JSEscape($APPLICATION->GetCurPageParam()) ?>';
<? if ($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT")): ?>
            var groupField = new JCIBlockGroupField(form, 'tr_IBLOCK_ELEMENT_PROPERTY', url);
            groupField.reload();
<? endif ?>
        return;
    }
</script>
<?
$hidden = "";
if (is_array($str_IBLOCK_ELEMENT_SECTION))
    foreach ($str_IBLOCK_ELEMENT_SECTION as $section_id)
        $hidden .= '<input type="hidden" name="IBLOCK_SECTION[]" value="' . intval($section_id) . '">';
$tabControl->EndCustomField("SECTIONS", $hidden);
