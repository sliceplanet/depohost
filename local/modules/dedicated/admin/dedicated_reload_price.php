<?php
use Itin\Depohost\Solutions;
use Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/dedicated/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/dedicated/prolog.php");

\CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php");

\CModule::IncludeModule("dedicated");
// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("dedicated");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


if($REQUEST_METHOD == "POST" && check_bitrix_sessid()){

    Loader::includeModule('iblock');

    $solution = new Solutions();
    $result = $solution->getAllSolutions();

    $RESULT['SUCCESS'] = 0;

    foreach ($result as &$el){

        $solution->pushFields($el['PROPERTIES']);
        $summ = $solution->getSumOfDetail();
        $el['NEW_SUMM'] = $summ;
        if($summ != $el['PRICE']['PRICE']){

            $solution->updatePrice($el['PRICE']['ID']);

            $RESULT['SUCCESS']++;

        }


    }

//    echo '<pre>';print_r($result);echo '</pre>';
//    echo '<pre>';print_r($solution);echo '</pre>';

}



$APPLICATION->SetTitle('Генератор готовых решений');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>

<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" id="reload_price" name="reload_price">
    <?echo bitrix_sessid_post();?>


    <?
    if (!empty($RESULT['SUCCESS'])){
        ShowMessage(Array("TYPE"=>"OK", "MESSAGE" => 'Успешно обновлено: '.$RESULT['SUCCESS']));
    }
    ?>


    <input type="submit" value="Обновить">


</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
