<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;
use Itin\Depohost\Solutions;



require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/dedicated/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/dedicated/prolog.php");

CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php");


CModule::IncludeModule("dedicated");

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("dedicated");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if($REQUEST_METHOD == "POST" && check_bitrix_sessid()){

//    echo '<pre>';print_r($_POST);echo'</pre>';
    $error = '';
    $result = array();

    $required = array('select_kam1', 'select_kam2', 'select_ram', 'select_hdd', 'QUANTITY');
    foreach($required as $req){

        if(isset($_POST[$req]) && empty($_POST[$req])){

            $error = $req;
            break;
        }

    }

    if(empty($error)){

        $fields = array(
            'CPU'   => intval($_POST['select_kam2']),
            'RAID'  => intval($_POST['select_kam3']),
            'RAM'   => intval($_POST['select_ram']),
            'HDD' => json_decode($_POST['select_hdd'], true),

            'GENERATION' => intval($_POST['select_kam1']),
            'QUANTITY' => intval($_POST['QUANTITY']),
            'ALT' => strip_tags(trim($_POST['ALT'])),
            'ADVANCE' => strip_tags(trim($_POST['ADVANCE']))
        );

        $solutions = new Solutions($fields);
        $summ = $solutions->getSumOfDetail();

        $solutions->setCatalogId(intval($_POST['select_groups']));

        $el_id = $solutions->generateNewSolution();

        if($el_id > 0){

            $result = array(
                'RESULT' => 'OK',
                'MESSAGE' => 'element created - №'.$el_id
            );

        }else{

            $result = array(
                'RESULT' => 'ERROR',
                'MESSAGE' => 'ERROR'
            );

        }

        //$result['SUMM'] = $summ;


    }else{

        $result = array(
            'RESULT' => 'ERROR',
            'NAME' => $error,
        );

    }

    $fields = array(
        'select_kam1' => 'GENERATION'
    );


    $APPLICATION->RestartBuffer();
    echo json_encode($result);
    die();
}



$APPLICATION->SetTitle('Генератор готовых решений');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>

<?
$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery-1.7.2.min.js');
// $APPLICATION->AddHeadScript('/local/templates/depohost/script.min.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/backend/script.js');
$APPLICATION->SetAdditionalCSS('/local/templates/depohost/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS('/local/templates/depohost/css/jquery.selectbox-m.css');
// $APPLICATION->SetAdditionalCSS('/local/templates/depohost/css/dedic.min.css');
$APPLICATION->SetAdditionalCSS('/local/templates/depohost/backend/dedic.css');
$APPLICATION->SetAdditionalCSS('/local/templates/depohost/css/select-small.css');

$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery.prettyCheckboxes.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery.selectbox.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/js/jquery-ui-1.10.4.custom.min.js');

$APPLICATION->AddHeadScript('/local/templates/depohost/js/bootstrap.min.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/js/scripts_bootstrap.min.js');
// $APPLICATION->AddHeadScript('/local/templates/depohost/js/dedic.js');
$APPLICATION->AddHeadScript('/local/templates/depohost/backend/dedic.js');
?>

<div id="page-dedic" class="wrapper server-container">

<div class="clearFix"></div>
    <div class="tab-content">

<?$APPLICATION->IncludeComponent("itin:dedicated.calc.admin", "", array(
    "IBLOCK_TYPE" => "dedicated",
    "IBLOCK_ID" => "44",
    "IBLOCK_ID_SOLUTIONS" => "21",
), false
);
?>

    </div>
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
