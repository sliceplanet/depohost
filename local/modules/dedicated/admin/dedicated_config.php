<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin.php");

Loader::includeModule('dedicated');

$moduleDir = Dedicated::getModuleDir();
include_once $moduleDir.'/include/denied.php';


Loc::loadMessages(__FILE__);
$denyAutosave = false;
$aTabs = array();
$aTabs[] = array(
        "DIV" => "discount",
        "TAB" => Loc::getMessage("TAB_DISCOUNT"),
        "ICON" => "",
        "TITLE" => Loc::getMessage("TAB_DISCOUNT_TITLE")
    );
$aTabs[] = array(
        "DIV" => "hints_calc",
        "TAB" => Loc::getMessage("TAB_HINT_CALC"),
        "ICON" => "",
        "TITLE" => Loc::getMessage("TAB_HINT_CALC_TITLE")
    );
$aTabs[] = array(
        "DIV" => "hints_advance_options",
        "TAB" => Loc::getMessage("TAB_HINT_ADVANCE_OPTIONS"),
        "ICON" => "",
        "TITLE" => Loc::getMessage("TAB_HINT_ADVANCE_OPTIONS_TITLE")
    );
$tabControl = new CAdminForm("tabControl", $aTabs, true, $denyAutosave);
$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));
$tabControl->SetShowSettings(false);
$tabControl->BeginPrologContent();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
$tabControl->EndEpilogContent();
$tabControl->Begin(array('FORM_ACTION'=>''));
foreach ($aTabs as $arTab)
{
    switch($arTab['DIV'])
    {
        case 'discount':
            $tabControl->BeginNextFormTab();
            require 'admin_pages/config/tabs/discount.php';
            break;
        case 'hints_calc':
            $tabControl->BeginNextFormTab();
             require 'admin_pages/config/tabs/hints_calc.php';
            break;
        case 'hints_advance_options':
            $tabControl->BeginNextFormTab();
             require 'admin_pages/config/tabs/hints_advance_options.php';
            break;
    }
}
ob_start();
?>
<input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="adm-btn-save" name="save" id="save" value="<? echo Loc::getMessage("SAVE") ?>">
<input <? if ($bDisabled) echo "disabled"; ?> type="submit" class="button" name="dontsave" id="dontsave" value="<? echo Loc::getMessage("CANCEL") ?>">
<?
$buttons_add_html = ob_get_contents();
ob_end_clean();
$tabControl->Buttons(false, $buttons_add_html);
$tabControl->Show();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");