<?php
require_once 'prolog.php';
\Bitrix\Main\Loader::registerAutoLoadClasses(ADMIN_MODULE_NAME,
    array(
        'Itin\\Depohost\\Whois' => 'lib/whois.php',
    )
);