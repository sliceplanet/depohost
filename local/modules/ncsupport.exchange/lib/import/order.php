<?php

namespace NcSupport\Exchange\Import;

use Bitrix\Highloadblock as HL;

class Order
{

  private $user;
  private $pass;


  function __construct()
  {
    $this->user = \COption::GetOptionString("exchange", "isp_user");
    $this->pass = \COption::GetOptionString("exchange", "isp_password");
  }

  public function InsertInfoBlock($array)
  {
    if (\CModule::IncludeModule("highloadblock"))
    {

      $hlblock = HL\HighloadBlockTable::getById(1)->fetch();

      $entity = HL\HighloadBlockTable::compileEntity($hlblock);

      $entity_data_class = $entity->getDataClass();

      $data = array(
        "UF_CLIENT" => $array['order_param']['uf_client'],
        "UF_TERMINATE" => false,
        "UF_EXPIRATION" => date('d.m.Y', strtotime('' . date('d.m.Y') . ' +' . $array['order_param']['period'] . ' month')),
        "UF_DIVISION" => true,
        "UF_NAME_SHORT" => $array['order_param']['licname'],
        "UF_CATALOG_XML_ID" => '33',
        "UF_PERIOD" => $array['order_param']['period'],
        "UF_IPKVM" => '33',
        "UF_REBOOT" => '33',
        "UF_FULL_DESCRIPTION" => $array['order_param']['licname'],
        "UF_DESCRIPTION" => $array['order_param']['licname'],
        "UF_BASKET_ID" => null,
        "UF_SERVER_ID" => null,
        "UF_IP_ADDR" => array($array['order_param']['ip']),
        "UF_QUANTITY" => 1,
        "UF_ORDER_ID" => null,
        "UF_NAME" => $array['order_param']['licname'],
        "UF_ACTIVE" => true,
        "UF_SUMM" => $array['order_param']['pricelist'],
        "UF_PRICE" => $array['order_param']['pricelist'],
        "UF_DATE_TO" => null,
        "UF_DATE_FROM" => null,
        "UF_MEASURE" => null,
        "UF_PROPERTIES" => null
      );
      if (isset($array['UF_SSL_ELID']))
      {
        $data['UF_SSL_ELID'] = $array['UF_SSL_ELID'];
      }
      if (isset($array['UF_IS_SSL']))
      {
        $data['UF_IS_SSL'] = $array['UF_IS_SSL'];
      }
      if (isset($array['UF_SSL_DOMAIN']))
      {
        $data['UF_SSL_DOMAIN'] = $array['UF_SSL_DOMAIN'];
      }
      if (isset($array['UF_SSL_CITY']))
      {
        $data['UF_SSL_CITY'] = $array['UF_SSL_CITY'];
      }
      if (isset($array['UF_SSL_O']))
      {
        $data['UF_SSL_O'] = $array['UF_SSL_O'];
      }
      if (isset($array['UF_SSL_OU']))
      {
        $data['UF_SSL_OU'] = $array['UF_SSL_OU'];
      }
      if (isset($array['UF_SSL_ST']))
      {
        $data['UF_SSL_ST'] = $array['UF_SSL_ST'];
      }
      if (isset($array['UF_SSL_ADM_MAIL']))
      {
        $data['UF_SSL_ADM_MAIL'] = $array['UF_SSL_ADM_MAIL'];
      }
      if (isset($array['UF_SSL_ADM_FNAME']))
      {
        $data['UF_SSL_ADM_FNAME'] = $array['UF_SSL_ADM_FNAME'];
      }
      if (isset($array['UF_SSL_ADM_JTTL']))
      {
        $data['UF_SSL_ADM_JTTL'] = $array['UF_SSL_ADM_JTTL'];
      }
      if (isset($array['UF_SSL_ADM_LNAME']))
      {
        $data['UF_SSL_ADM_LNAME'] = $array['UF_SSL_ADM_LNAME'];
      }
      if (isset($array['UF_SSL_ADM_PHONE']))
      {
        $data['UF_SSL_ADM_PHONE'] = $array['UF_SSL_ADM_PHONE'];
      }
      if (isset($array['UF_SSL_APPRV_MAIL']))
      {
        $data['UF_SSL_APPRV_MAIL'] = $array['UF_SSL_APPRV_MAIL'];
      }
      if (isset($array['UF_SSL_MAIL_ADDR']))
      {
        $data['UF_SSL_MAIL_ADDR'] = $array['UF_SSL_MAIL_ADDR'];
      }
      if (isset($array['UF_SSL_FIELD_COUNTRY']))
      {
        $data['UF_SSL_FIELD_COUNTRY'] = $array['UF_SSL_FIELD_COUNTRY'];
      }
      if (isset($array['UF_SSL_PERIOD']))
      {
        $data['UF_SSL_PERIOD'] = $array['UF_SSL_PERIOD'];
      }
      if (isset($array['UF_SSL_PRICELIST']))
      {
        $data['UF_SSL_PRICELIST'] = $array['UF_SSL_PRICELIST'];
      }
      if (isset($array['UF_SSL_TECH_MAIL']))
      {
        $data['UF_SSL_TECH_MAIL'] = $array['UF_SSL_TECH_MAIL'];
      }
      if (isset($array['UF_SSL_TECH_FNAME']))
      {
        $data['UF_SSL_TECH_FNAME'] = $array['UF_SSL_TECH_FNAME'];
      }
      if (isset($array['UF_SSL_TECH_FTTL']))
      {
        $data['UF_SSL_TECH_FTTL'] = $array['UF_SSL_TECH_FTTL'];
      }
      if (isset($array['UF_SSL_TECH_LNAME']))
      {
        $data['UF_SSL_TECH_LNAME'] = $array['UF_SSL_TECH_LNAME'];
      }
      if (isset($array['UF_SSL_TECH_PHONE']))
      {
        $data['UF_SSL_TECH_PHONE'] = $array['UF_SSL_TECH_PHONE'];
      }
      if (isset($array['UF_SSL_AUTH']))
      {
        $data['UF_SSL_AUTH'] = $array['UF_SSL_AUTH'];
      }
      if (isset($array['UF_SSL_OP_CNTNAME']))
      {
        $data['UF_SSL_OP_CNTNAME'] = $array['UF_SSL_OP_CNTNAME'];
      }
      if (isset($array['UF_SSL_OP_SOPNAME']))
      {
        $data['UF_SSL_OP_SOPNAME'] = $array['UF_SSL_OP_SOPNAME'];
      }
      if (isset($array['UF_SSL_OP_LOCALNAME']))
      {
        $data['UF_SSL_OP_LOCALNAME'] = $array['UF_SSL_OP_LOCALNAME'];
      }
      if (isset($array['UF_SSL_OP_ORGNAME']))
      {
        $data['UF_SSL_OP_ORGNAME'] = $array['UF_SSL_OP_ORGNAME'];
      }
      if (isset($array['UF_SSL_OP_ORGUTNAME']))
      {
        $data['UF_SSL_OP_ORGUTNAME'] = $array['UF_SSL_OP_ORGUTNAME'];
      }
      if (isset($array['UF_SSL_OP_COMMNAME']))
      {
        $data['UF_SSL_OP_COMMNAME'] = $array['UF_SSL_OP_COMMNAME'];
      }

      if ($result = $entity_data_class::add($data))
      {
        return true;
      }
      else
      {
        return false;
      }

    }
  }

  public function UpdateInfoBlock(array $array)
  {
    if (\CModule::IncludeModule("highloadblock"))
    {

      $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
      $entity = HL\HighloadBlockTable::compileEntity($hlblock);
      $entity_data_class = $entity->getDataClass();

      return !empty($entity_data_class::update($array['ID'], $array));
    }
  }

  public function SyncInfoBlock(array $param)
  {
    $resultList = false;

    if (\CModule::IncludeModule("highloadblock"))
    {
      if (!isset($param['order']))
      {
        if (isset($param['ip']) && !empty($param['ip']))
        {
          $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
          $entity = HL\HighloadBlockTable::compileEntity($hlblock);
          $entity_data_class = $entity->getDataClass();

          $rsData = $entity_data_class::getList(array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
            "filter" => array("UF_IP_ADDR" => $param['ip'])
          ));

          while ($arData = $rsData->Fetch())
          {
            $resultList[] = $arData;
          }

        }

      }

      if (isset($resultList[0]))
      {
        if (isset($param['period']))
        {

          if (empty($resultList[0]['UF_EXPIRATION']))
          {
            $date = date('d.m.Y');

          }
          else
          {
            $current = explode(' ', $resultList[0]['UF_EXPIRATION']);
            $date = date('d.m.Y', strtotime('' . $current[0] . ' +' . $param['period'] . ' month'));;
          }

          $resultList[0]['UF_EXPIRATION'] = $date;
          $resultList[0]['UF_TERMINATE'] = false;
          $resultList[0]['UF_DIVISION'] = true;

        }
        if (isset($param['activeate']))
        {
          $resultList[0]['UF_TERMINATE'] = false;
          $resultList[0]['UF_DIVISION'] = false;
        }
        if (isset($param['deactiveate']))
        {
          $resultList[0]['UF_TERMINATE'] = true;
        }
        if (isset($param['delete']))
        {
          $resultList[0]['UF_TERMINATE'] = true;
          $resultList[0]['UF_IP_ADDR'] = array('0.0.0.0');
        }
      }

      if (!isset($param['order']))
      {
        $resultList = $this->UpdateInfoBlock($resultList[0]);
      }
      else
      {
        if (isset($param['order_param']['ip']))
        {
          $resultList = $this->InsertInfoBlock($param);
        }
      }
    }

    return $resultList;

  }

  public function Handling(string $function, array $param)
  {
    try
    {
      $res = $this->$function($param);

      return $res;
    }
    catch (Exception $exception)
    {
      echo $exception->getMessage();
    }
  }

  public function SoftOrderParam($param)
  {
    $path = '';
    $path .= 'https://api.ispsystem.com/manager/';
    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';
    $path .= '&func=soft.order.param&clicked_button=finish';

    foreach ($param as $name_param => $value_param)
    {
      if (!empty($value_param))
      {
        $path .= '&' . $name_param . '=' . $value_param;
      }
    }

    return $this->Curl(__FUNCTION__, $path, $param);
  }

  public function ServiceProlong($param)
  {
    $path = '';
    $path .= 'https://api.ispsystem.com/manager/';
    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';
    $path .= '&func=service.prolong&clicked_button=finish&sok=ok';

    foreach ($param as $name_param => $value_param)
    {
      if (!empty($value_param))
      {
        $path .= '&' . $name_param . '=' . $value_param;
      }
    }

    $this->SyncInfoBlock($param);

    return $this->Curl(__FUNCTION__, $path);
  }

  public function SoftSuspend($param)
  {
    $path = '';
    $path .= 'https://api.ispsystem.com/manager/';
    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';
    $path .= '&func=soft.suspend&clicked_button=finish';
    $path .= '&ip=' . $param['elid'];
    $param['delete'] = true;

    $this->SyncInfoBlock($param);

    return $this->Curl(__FUNCTION__, $path);
  }

  public function SoftDelete($param)
  {
    $path = '';
    $path .= 'https://api.ispsystem.com/manager/';
    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';
    $path .= '&func=soft.delete&clicked_button=finish';

    foreach ($param as $name_param => $value_param)
    {
      if (!empty($value_param))
      {
        $path .= '&' . $name_param . '=' . $value_param;
      }
    }

    $param['delete'] = true;
    $this->SyncInfoBlock($param);

    return $this->Curl(__FUNCTION__, $path);
  }

  public function SoftDeactivation($param)
  {
    $path = '';
    $path .= 'https://api.ispsystem.com/manager/';
    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';
    $path .= '&func=soft.edit&clicked_button=finish';
    $path .= '&elid=' . $param['elid'];
    $path .= '&licname=' . $param['licname'];
    $path .= '&ip=' . $param['new_ip'];
    $path .= '&sok=ok';
    $param['deactiveate'] = true;

    $this->SyncInfoBlock($param);

    $this->Curl(__FUNCTION__, $path);

    return $this->SoftSuspend(array('elid' => $param['elid']));

  }

  public function Curl($func, $path, $dta = null)
  {

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $path);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);

    if ($result === false)
    {
      die('Ошибка при загрузке данных: ' . curl_error($curl));
    }
    curl_close($curl);

    $xml = simplexml_load_string($result);

    if ($xml === false)
    {
      die('Ошибка чтения XML');
    }

    if (isset($xml->error))
    {
      die('Ошибка действия на стороне ISP: ' . $xml->error->msg);
    }
    else
    {
      if ($func == 'SoftOrderParam')
      {
        $param['order'] = $xml;
        $param['order_param'] = $dta;
        $this->SyncInfoBlock($param);
      }

      return array('res' => true, 'data' => $xml);
    }
  }
}