<?php

namespace NcSupport\Exchange\Import;
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main;
use Bitrix\Sale;
use Bitrix\Highloadblock as HL;

class SSL
{

  private $user;
  private $pass;

  function __construct()
  {
    $loader = new Main\Loader;
    $loader->includeModule('iblock');

    $this->user = \COption::GetOptionString("exchange", "isp_user");
    $this->pass = \COption::GetOptionString("exchange", "isp_password");
  }

  public function Handling(string $function, $param = null)
  {
    try
    {

      $res = $this->$function($param);

      return $res;

    }
    catch (Exception $exception)
    {

      echo $exception->getMessage();

    }
  }

  public function FetchSSLProlong($elid = null)
  {
    if (!empty($elid))
    {

      $resArrSSL = $this->GetDataSSLFromDB($elid);

    }
    else
    {

      $resArrSSL = $this->GetAllFetchSSLFromDB();

    }

    $this->ProlongCert($resArrSSL);

  }

  public function ProlongCert($array)
  {
    foreach ($array as $item)
    {

      $result = $this->CurlFromISP($item);

      $this->EvaluationDataArray($result);

    }

  }

  public function EvaluationDataArray($array)
  {
    foreach ($array as $item)
    {

      if ($item->real_expiredate < date("Y-m-d"))
      {

         $this->certificateRetry($item->id); // id = $item->id

      }

    }
  }

  public function CurlFromISP($element)
  {
    $path = '';

    $path .= 'https://api.ispsystem.com/manager/';

    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';

    $path .= '&func=certificate&clicked_button=finish';

    $path .= '&elid=' . $element['UF_SSL_ELID'];

    $path .= '&sok=ok';

    return $this->Curl(__FUNCTION__, $path);
  }

  public function isPaid($id)
  {
    if (\Bitrix\Main\Loader::includeModule("clients"))
    {

      $Paid = (new \Itin\Depohost\Invoices())->isChangeStatus($id);

      if ($Paid)
      {

        return true;

      }
      else
      {

        return false;
      }

    }
    else
    {

      die('Не доступен модуль clients');

    }
  }

  public function certificateOrder($element, $isPaid = false)
  {

    if ($isPaid === false)
    {
      $isPaid = $this->isPaid($element['AccountNumber']);
    }

    if ($isPaid === true)
    {

      if (isset($element[0]))
      {
        $element = $element[0];
      }

      $path = '';

      $path .= 'https://api.ispsystem.com/manager/';

      $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass;

		$domain = $element['UF_SSL_DOMAIN'];

		if(!$element['UF_SSL_DOMAIN']) {
			$domain = str_replace("Домен: ", "", $element["UF_PROPERTIES"][1]);
		}
		
		if($element["UF_PROPERTIES"][0]) {
			$approvedEmail = str_replace("Email: ", "", $element["UF_PROPERTIES"][0]);
		} else {
			$approvedEmail = $element['UF_SSL_APPRV_MAIL'];
		}

		if (!$element['UF_ELID']) {
			exit("Не удалось определить elid сертификата!");
		}
		
		
		$period = $element['UF_SSL_PERIOD'];
		
		if(!$element['UF_SSL_DOMAIN']) {
			$period = preg_replace('~\D+~','', $element["UF_PROPERTIES"][2]) * 12;
		}
		

      $dn = array(
        "countryName" => "RU",
        "stateOrProvinceName" => "MOSCOW",
        "localityName" => "MOSCOW",
        "organizationName" => $domain,
        "organizationalUnitName" => $domain,
        "commonName" => $domain,
        "emailAddress" => $element['UF_SSL_TECH_MAIL']
      );


      $privkey = openssl_pkey_new(array(
        "private_key_bits" => 2048,
        "private_key_type" => OPENSSL_KEYTYPE_RSA,
      ));

      $csr = openssl_csr_new($dn, $privkey, array('digest_alg' => 'sha256'));

      $x509 = openssl_csr_sign($csr, null, $privkey, $days = 365, array('digest_alg' => 'sha256'));

      openssl_csr_export($csr, $csrout);

      openssl_x509_export($x509, $certout);

      openssl_pkey_export($privkey, $pkeyout, "mypassword");


      $path .= '&CN=' . rawurlencode($domain) . '&out=xml';
      $path .= '&L=' . rawurlencode("Moscow");
      $path .= '&O=' . rawurlencode($element['UF_SSL_OP_ORGNAME']);
      $path .= '&OU=' . rawurlencode('');
      $path .= '&ST=' . rawurlencode("Moscow");
      $path .= '&adm_email=' . rawurlencode($element['UF_SSL_TECH_MAIL']);
      $path .= '&adm_fname=' . rawurlencode($element['UF_SSL_TECH_FNAME']);
      $path .= '&adm_jtitle=' . rawurlencode($element['UF_SSL_TECH_FTTL']);
      $path .= '&adm_lname=' . rawurlencode($element['UF_SSL_TECH_LNAME']);
      $path .= '&adm_phone=' . rawurlencode($element['UF_SSL_TECH_PHONE']);
      $path .= '&approver_email=' . rawurlencode($approvedEmail);
      $path .= '&clicked_button=finish';
      $path .= '&csr=' . rawurlencode($csrout);
      $path .= '&delete_private_key=off';
      $path .= '&emailAddress=' . rawurlencode($element['UF_SSL_MAIL_ADDR']);
	  $path .= '&field_country=182'/* . rawurlencode($element['UF_SSL_FIELD_COUNTRY'])*/;
      $path .= '&field_cn_www_show=';
      $path .= '&func=certificate.order.param';
      $path .= '&hfields=csr';
      $path .= '&key=' . rawurlencode($pkeyout);
      $path .= '&like_admin=on';
      $path .= '&period='.rawurlencode($period);
      //$path .= '&period=' . rawurlencode(12);
      $path .= '&pricelist=' . rawurlencode($element['UF_ELID']);
      $path .= '&sok=ok';
      $path .= '&tech_email=' . rawurlencode($element['UF_SSL_TECH_MAIL']);
      $path .= '&tech_fname=' . rawurlencode($element['UF_SSL_TECH_FNAME']);
      $path .= '&tech_jtitle=' . rawurlencode($element['UF_SSL_TECH_FTTL']);
      $path .= '&tech_lname=' . rawurlencode($element['UF_SSL_TECH_LNAME']);
      $path .= '&tech_phone=' . rawurlencode($element['UF_SSL_TECH_PHONE']);
      $path .= '&auth=' . rawurlencode($element['UF_SSL_AUTH']);
      $path .= '&skipbasket=on';
      //сохраняем приватный ключ в ЛК
      $path .= '&crt_type=generate';
	  
	  
      if(!empty($element['UF_SSL_OP_ORGNAME'])) {
		  $path .= '&org_name=' . rawurlencode($element['UF_SSL_OP_ORGNAME']);
		  $path .= '&org_state=' . rawurlencode($element['UF_SSL_FIELD_REGION']);
		  $path .= '&org_city=' . rawurlencode($element['UF_SSL_FIELD_CITY']);
		  $path .= '&org_postcode=' . rawurlencode($element['UF_SSL_FIELD_INDEX']);
		  $path .= '&org_address=' . rawurlencode($element['UF_SSL_FIELD_ADDRESS']);
		  $path .= '&org_phone=' . rawurlencode($element['UF_SSL_TECH_PHONE']);
		  $path .= '&org_country=182'/* . rawurlencode($element['UF_SSL_FIELD_COUNTRY'])*/;
	  }

      $response = $this->Curl(__FUNCTION__, $path);

	  if(!empty($response->error->msg)) {
	  print_r($path);
	  echo '<br >';
		die($response->error->msg);
	  }
	  
      //добавляем номер заказа в ISP к свойствам товара в заказах Битрикс
      $this->addOrderNubmer($element['UF_ORDER_ID'], $domain, $response->id);

      //сохраняем приватный ключ у себя на хостинге
      $orderId = $response->id;
      if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/upload/certificates/' . $orderId . '/'))
      {
        mkdir($_SERVER['DOCUMENT_ROOT'] . '/upload/certificates/' . $orderId . '/');
      }
      @file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/certificates/' . $orderId . '/' . $domain . '_private.key', $pkeyout);

      //добавляем запись в реестр
      if (\CModule::IncludeModule("highloadblock") && !empty($orderId))
      {

        $hlblock = HL\HighloadBlockTable::getById(4)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);

        $entity_data_class = $entity->getDataClass();

		$time = strtotime('now');
		if($period == 24) {
			$time = strtotime('now + 1 year');
		}

        $data = array(
          "UF_ISP" => $orderId,
          "UF_DOMAIN" => $domain,
          "UF_EXPIRE" => time(),
          "UF_ELID" => $element['UF_ELID'],
          "UF_INVOICE" => $element['ID'],
          "UF_USER" => $element['UF_CLIENT'],
          "UF_NAME" => $element['UF_NAME'],
          "UF_SERVICE" => 'Регистрация'
        );

        $entity_data_class::add($data);

      }

      return $response;

    }
    else
    {

      die('Сертификат не оплачен');

    }

  }

  public function certificateSync($elid_ssl)
  {
    $path = '';

    $path .= 'https://api.ispsystem.com/manager/';

    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';

    $path .= '&func=certificate.sync&clicked_button=finish';

    $path .= '&elid=' . $elid_ssl;

    $path .= '&sok=ok';

    var_dump($this->Curl(__FUNCTION__, $path));
  }

  public function certificateRetry($elid_ssl)
  {
    $path = '';

    $path .= 'https://api.ispsystem.com/manager/';

    $path .= 'billmgr?authinfo=' . $this->user . ':' . $this->pass . '&out=xml';

    $path .= '&func=certificate.reissue&clicked_button=finish';

    $path .= '&elid=' . $elid_ssl;

    $path .= '&sok=ok';

    return $this->Curl(__FUNCTION__, $path);
  }

  public function GetAllFetchSSLFromDB()
  {

    $resultList = false;

    if (\CModule::IncludeModule("highloadblock"))
    {

      $hlblock = HL\HighloadBlockTable::getById(1)->fetch();

      $entity = HL\HighloadBlockTable::compileEntity($hlblock);

      $entity_data_class = $entity->getDataClass();

      $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
        "filter" => array("UF_IS_SSL" => true, "UF_DIVISION" => true)
      ));

      while ($arData = $rsData->Fetch())
      {
        $resultList[] = $arData;
      }

    }

    return $resultList;

  }

  public function GetDataSSLFromDB($elid)
  {
    $resultList = false;

    if (\CModule::IncludeModule("highloadblock"))
    {

      if (isset($elid) && !empty($elid))
      {


        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);

        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
          "select" => array("*"),
          "order" => array("ID" => "ASC"),
          "filter" => array("UF_SSL_ELID" => $elid)
        ));

        while ($arData = $rsData->Fetch())
        {
          $resultList[] = $arData;
        }

      }

      if (!$resultList)
      {

        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);

        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
          "select" => array("*"),
          "order" => array("ID" => "ASC"),
          "filter" => array("UF_CLIENT" => $elid)
        ));

        while ($arData = $rsData->Fetch())
        {
          $resultList[] = $arData;
        }

      }


    }
    print_r($resultList);
    exit;
    return $resultList;

  }

  public function addOrderNubmer($order, $domain, $value)
  {
    $order = Sale\Order::load($order);
    $basket = $order->getBasket();
    foreach ($basket as $basketItem)
    {
      $set = false;
      $properties = array();
      $properties[] = array(
        'NAME' => 'Номер заказа в ISP',
        'CODE' => 'isp_order',
        'VALUE' => $value,
        'SORT' => 100,
      );

      $basketPropertyCollection = $basketItem->getPropertyCollection();
      foreach ($basketPropertyCollection as $propertyItem)
      {
        $propertyName = $propertyItem->getField('NAME');
        $propertyValue = $propertyItem->getField('VALUE');
        $propertyCode = $propertyItem->getField('CODE');
        $propertySort = $propertyItem->getField('SORT');
        $properties[] = array(
          'NAME' => $propertyName,
          'CODE' => $propertyCode,
          'VALUE' => $propertyValue,
          'SORT' => $propertySort,
        );
        if ($propertyItem->getField('VALUE') == $domain)
        {
          $set = true;
        }
      }
      if ($set)
      {
        $basketPropertyCollection->setProperty($properties);
        $basketPropertyCollection->save();
      }
    }
  }

  public function addOrderSendData($order, $domain, $value)
  {
    $order = Sale\Order::load($order);
    $basket = $order->getBasket();
    foreach ($basket as $basketItem)
    {
      $set = false;
      $properties = array();
      $properties[] = array(
        'NAME' => 'Сертификат отправлен пользователю',
        'CODE' => 'crt_send',
        'VALUE' => $value,
        'SORT' => 100,
      );

      $basketPropertyCollection = $basketItem->getPropertyCollection();
      foreach ($basketPropertyCollection as $propertyItem)
      {
        $propertyName = $propertyItem->getField('NAME');
        $propertyValue = $propertyItem->getField('VALUE');
        $propertyCode = $propertyItem->getField('CODE');
        $propertySort = $propertyItem->getField('SORT');
        $properties[] = array(
          'NAME' => $propertyName,
          'CODE' => $propertyCode,
          'VALUE' => $propertyValue,
          'SORT' => $propertySort,
        );
        if ($propertyItem->getField('VALUE') == $domain)
        {
          $set = true;
        }
      }
      if ($set)
      {
        $basketPropertyCollection->setProperty($properties);
        $basketPropertyCollection->save();
      }
    }
  }

  public function certificateGet($elid_ssl)
  {
    $path = '';

    $path .= 'https://api.ispsystem.com/manager/';

    $path .= '?authinfo=' . $this->user . ':' . $this->pass;

    $path .= '&func=certificate.file';

    $path .= '&elid=' . $elid_ssl;

    $path .= '&type=crt';

    $file = file_get_contents($path);

    if (!empty($file))
    {
      return $path;
    }
    else
    {
      return false;
    }

  }

  public function certificateListGet()
  {
    $path = '';

    $path .= 'https://api.ispsystem.com/manager/';

    $path .= '?authinfo=' . $this->user . ':' . $this->pass;

    $path .= '&func=certificate';

    $path .= '&item_status=2';

    $path .= '&out=json';

    $file = file_get_contents($path);

    if (!empty($file))
    {
      return json_decode($file);
    }
    else
    {
      return false;
    }

  }

  public function certificateProlong($element, $isPaid = false)
  {
  
    if ($isPaid === false)
    {
      $isPaid = $this->isPaid($element['AccountNumber']);
    }
    if ($isPaid === true)
    {

      if (!$element['UF_SSL_DOMAIN'])
      {

        $domain = str_replace("Домен: ", "", $element["UF_PROPERTIES"][1]);

      }
	  
	  $get = false;
	  

      $certificateList = $this->certificateListGet();
      $certificates = $certificateList->doc->elem;

      foreach ($certificates as $certificate):
        $currentDomain = current($certificate->CN);
        if ($domain == $currentDomain):
			$get = true;
          $elid = current($certificate->id);
          $path = '';

          $path .= 'https://api.ispsystem.com/manager/';

          $path .= '?authinfo=' . $this->user . ':' . $this->pass;

          $path .= '&func=service.prolong';

          $path .= '&elid=' . $elid;

          $path .= '&period=1';

          $path .= '&sok=ok';

          $path .= '&out=json';

          $file = file_get_contents($path);

          if (!empty($file))
          {
            if (\CModule::IncludeModule("highloadblock") && !empty($orderId))
            {

              $hlblock = HL\HighloadBlockTable::getById(4)->fetch();

              $entity = HL\HighloadBlockTable::compileEntity($hlblock);

              $entity_data_class = $entity->getDataClass();

              $data = array(
                "UF_ISP" => $elid,
                "UF_DOMAIN" => $domain,
                "UF_EXPIRE" => time(),
                "UF_ELID" => $element['UF_ELID'],
                "UF_INVOICE" => $element['ID'],
                "UF_USER" => $element['UF_CLIENT'],
                "UF_NAME" => $element['UF_NAME'],
                "UF_SERVICE" => 'Продление',
              );

              $entity_data_class::add($data);

            }
          }
          else
          {
            return false;
          }
        endif;
      endforeach;
	  
	  if(!$get) {
		return 'Сертификат не найден в ISP, продление невозможно';
	  }
    }
    else
    {
      return 'Сертификат не оплачен';
    }
  }

  public function Curl($func, $path, $dta = null)
  {

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $path);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    if ($result === false)
    {
      die('Ошибка при загрузке данных: ' . curl_error($curl));
    }

    curl_close($curl);

    $xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);

    if ($xml === false)
    {
      echo '<pre>';
      print_r($result);
      echo '</pre>';
      echo '<pre>';
      var_dump($xml);
      echo '</pre>';
      die('Ошибка чтения XML');
    }

    if (isset($xml->error))
    {
      return $xml;
    }
    else
    {
      return $xml;
    }

  }
}