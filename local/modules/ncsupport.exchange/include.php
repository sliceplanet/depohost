<?php
\Bitrix\Main\Loader::registerAutoLoadClasses('ncsupport.exchange',
    array(
		'NcSupport\\Exchange\\Import\\SSL' => 'lib/import/ssl.php',
		'NcSupport\\Exchange\\Import\\Order' => 'lib/import/order.php',
    )
);