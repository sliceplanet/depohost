<?

Class ncsupport_exchange extends CModule
{
  var $MODULE_ID = "ncsupport.exchange";
  var $MODULE_VERSION;
  var $MODULE_VERSION_DATE;
  var $MODULE_NAME;
  var $MODULE_DESCRIPTION;
  var $MODULE_CSS;

  function ncsupport_exchange()
  {
    $arModuleVersion = array();

    $path = str_replace("\\", "/", __FILE__);
    $path = substr($path, 0, strlen($path) - strlen("/index.php"));
    include($path . "/version.php");

    if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
    {
      $this->MODULE_VERSION = $arModuleVersion["VERSION"];
      $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
    }

    $this->MODULE_NAME = "ncsupport_exchange – модуль для работы с объетом заказа в ISP ";
    $this->MODULE_DESCRIPTION = "После установки вы сможете пользоваться обменом с ISP";
  }

  function InstallFiles()
  {
    return true;
  }

  function UnInstallFiles()
  {
    return true;
  }

  function DoInstall()
  {
    global $DOCUMENT_ROOT, $APPLICATION;
    $this->InstallFiles();
    RegisterModule("ncsupport.exchange");
    $APPLICATION->IncludeAdminFile("Установка модуля ncsupport_exchange", $DOCUMENT_ROOT . "/local/modules/ncsupport.exchange/install/step.php");
  }

  function DoUninstall()
  {
    global $DOCUMENT_ROOT, $APPLICATION;
    $this->UnInstallFiles();
    UnRegisterModule("ncsupport.exchange");
    $APPLICATION->IncludeAdminFile("Деинсталляция модуля ncsupport_exchange", $DOCUMENT_ROOT . "/local/modules/ncsupport_exchange/install/unstep.php");
  }
}

?>