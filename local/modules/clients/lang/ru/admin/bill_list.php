<?php
$MESS['SEND_TO_EMAIL'] = 'Создать и отправить счет';
$MESS['SEND_TO_EMAIL_ALT'] = 'Создать и отправить счет клиенту на E-mail';
$MESS['SEND_INVOICE_SUCCESS'] = 'Счет успешно создан и отправлен';
$MESS['SEND_INVOICE_ERROR'] = 'Счет не был создан и отправлен';
$MESS['IBLIST_A_SERVICES'] = 'Услуги';
$MESS['IBLIST_A_CONTRACT_ID'] = 'Номер договора';
$MESS['SEND_TO_EMAIL_REMINDER'] = 'Отправить напоминание';
$MESS['SEND_TO_EMAIL_REMINDER_ALT'] = 'Отправить напоминание об оплате клиенту на E-mail';
$MESS['SEND_TO_EMAIL_SUCCESS'] = 'Отправлено успешно';
$MESS['SEND_TO_EMAIL_NO_REMINDER'] = 'У клиента #NAME# нет неоплаченных счетов';
$MESS['DENIED_SECTION'] = 'Доступ к разделу запрещен';
$MESS['ADMIN_BILL_LIST_BILL_DATE_HEADER'] = 'Дата последнего счета';