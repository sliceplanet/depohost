<?php
$MESS ['clients_menu_text'] = "Клиенты";
$MESS ['clients_menu_title'] = "Клиенты";
$MESS ['clients_list_menu_text'] = "Карточки клиентов";
$MESS ['clients_list_menu_title'] = "Карточки клиентов";
$MESS ['clients_sale_menu_text'] = "Заказы";
$MESS ['clients_sale_menu_title'] = "Заказы";
$MESS ['clients_invoice_menu_text'] = "Счета";
$MESS ['clients_invoice_menu_title'] = "Счета";
$MESS ['clients_bill_menu_text'] = "Расчетная часть";
$MESS ['clients_bill_menu_title'] = "Расчетная часть";
$MESS ['clients_acts_menu_text'] = "Акты";
$MESS ['clients_acts_menu_title'] = "Акты";
$MESS ['clients_menu_title'] = "Клиенты";
$MESS ['clients_menu_title'] = "Клиенты";
