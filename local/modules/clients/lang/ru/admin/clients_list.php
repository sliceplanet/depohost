<?php
$MESS['SEND_TO_EMAIL'] = 'Создать и отправить счет';
$MESS['SEND_TO_EMAIL_ALT'] = 'Создать и отправить счет клиенту на E-mail';
$MESS['SEND_INVOICE_SUCCESS'] = 'Счет успешно создан и отправлен';
$MESS['SEND_INVOICE_ERROR'] = 'Счет не был создан и отправлен';
$MESS['IBLIST_A_SERVICES'] = 'Услуги';
$MESS['IBLIST_A_CONTRACT_ID'] = 'Номер договора';
$MESS['IBLIST_A_SERVER'] = 'Сервера';