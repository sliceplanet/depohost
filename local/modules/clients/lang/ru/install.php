<?php
$MESS['CLIENT_INSTALL_NAME'] = 'Клиенты';
$MESS['CLIENT_INSTALL_DESCRIPTION'] = 'Модуль для работы с клиентами';
$MESS['CLIENTS_DENIED'] = 'Доступ запрещен';
$MESS['CLIENTS_READ'] = 'Просмотр';
$MESS['CLIENTS_WRITE'] = 'Редактирование';
