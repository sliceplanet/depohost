<?php
$MESS['TICKET_REBOOT_TITLE'] = 'Требуется перезагруза сервера №#SERVER_ID# ip-адреса: #IP_ADDR#';
$MESS['TICKET_REBOOT_MESSAGE'] = 'Требуется перезагруза сервера №#SERVER_ID# ip-адреса: #IP_ADDR#';
$MESS['TICKET_IPKVM_TITLE'] = 'Требуется IP KVM сервера №#SERVER_ID# ip-адреса: #IP_ADDR#';
$MESS['TICKET_IPKVM_MESSAGE'] = 'Требуется IP KVM сервера №#SERVER_ID# ip-адреса: #IP_ADDR#';