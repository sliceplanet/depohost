<?php
$MESS['ERROR_AUTH'] = 'Ошибка авторизациии';
$MESS['ERROR_AUTH_ADMIN'] = 'Ошибка авторизациии админа';
$MESS['ERROR_CREATE_USER'] = 'Ошибка создания пользователя';
$MESS['ERROR_CREATE_FTP_USER'] = 'Ошибка создания ftp пользователя';
$MESS['ERROR_CREATE_DNS_USER'] = 'Ошибка создания dns пользователя';
$MESS['ERROR_AUTH_SERVICE_NOT_AVAILABLE'] = 'Веб-сервис временно недоступен';