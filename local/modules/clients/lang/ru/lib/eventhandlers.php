<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$MESS['SERVICE_NAME_u1'] = 'Аренда выделенного сервера #NAME#';
$MESS['SERVICE_NAME_u2'] = '#NAME#';
$MESS['SERVICE_NAME_u3'] = '#NAME#';
$MESS['SERVICE_NAME_u4'] = 'Хостинг DNS, тарифный план #NAME#';
$MESS['SERVICE_NAME_u5'] = '#NAME#';
$MESS['SERVICE_NAME_u6'] = 'Аренда программного обеспечения Microsoft #NAME#';
$MESS['SERVICE_NAME_u7'] = 'Аренда П/П 1С #NAME#';
$MESS['SERVICE_NAME_u8'] = 'Панель управления #NAME#';
$MESS['SERVICE_NAME_u9'] = 'SSL сертификат';
$MESS['SERVICE_NAME_u10'] = 'Услуги Web хостинга #NAME#';
$MESS['SERVICE_NAME_u11'] = '#NAME#';
$MESS['AUTO_CLOSE_MESSAGE_TEXT'] = '(Обращение было закрыто автоматически)';
?>