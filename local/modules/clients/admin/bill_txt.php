<?php

use Itin\Depohost\Invoices;
use Itin\Depohost\Clients;
use Itin\Depohost\BillTxt;
use Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");



if(isset($_GET['from']) && isset($_GET['to'])){

    Loader::includeModule('clients');
    Loader::includeModule("iblock");

    global $DB;

    $invoice = new Invoices();

    $arParams = array(
        'filter' => array(
            ">=PROPERTY_DATE" => trim($DB->CharToDateFunction($_GET['from']),"\'"),
            "<=PROPERTY_DATE" => trim($DB->CharToDateFunction($_GET['to']),"\'"),
            "PROPERTY_STATUS" => 278
        ),
        'select' => array('ID','PROPERTY_NUMBER','PROPERTY_DATE','PROPERTY_STATUS','PROPERTY_CLIENT')
    );
    $bills = array();
    $bills = $invoice->getList($arParams);

    $return = array();

    foreach ($bills as $key=>&$bill){

        $client = new Clients();
        $client->setElementId($bill['PROPERTY_CLIENT_VALUE']);
        $profail_id = $client->getPersonTypeId();

        $bill['PROFAIL_ID'] = $profail_id;

        if($profail_id > 1){

            unset($bills[$key]);

        }else{

            $no_rewrite = (isset($_GET['rewrite']) && $_GET['rewrite'] == true) ? false : true;
            $bill_txt = new BillTxt($bill['PROPERTY_NUMBER_VALUE']);
            $bill_txt->generateBillTxt($no_rewrite);

            $return[] = $bill['PROPERTY_NUMBER_VALUE'];

        }


    }

    echo implode(', ',$return);

//    echo '<pre>';print_r($arParams);echo "</pre>";
//    echo '<pre>';print_r($bills);echo "</pre>";
//    echo '<pre>';print_r($invoice);echo "</pre>";

}else{


    echo 'Нужны гетовые данные периода от(from) до(to)';

}
