<?php
define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);

ini_set('memory_limit','2048M');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
logi($_REQUEST,'request','log.log');

if ($type == 'invoices')
{
    $APPLICATION->IncludeComponent("itin:clients.invoices.export.1c", "", Array(
        "SITE_LIST" => COption::GetOptionString("sale", "1C_SALE_SITE_LIST", ""),
        "EXPORT_PAYED_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_PAYED_ORDERS", ""),
        "EXPORT_ALLOW_DELIVERY_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_ALLOW_DELIVERY_ORDERS", ""),
        "EXPORT_FINAL_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_FINAL_ORDERS", ""),
        "FINAL_STATUS_ON_DELIVERY" => COption::GetOptionString("sale", "1C_FINAL_STATUS_ON_DELIVERY", "F"),
        "REPLACE_CURRENCY" => COption::GetOptionString("sale", "1C_REPLACE_CURRENCY", ""),
        "GROUP_PERMISSIONS" => explode(",", COption::GetOptionString("sale", "1C_SALE_GROUP_PERMISSIONS", "1")),
        "USE_ZIP" => COption::GetOptionString("sale", "1C_SALE_USE_ZIP", "N"),
        "INTERVAL" => COption::GetOptionString("sale", "1C_INTERVAL", 30),
        "FILE_SIZE_LIMIT" => COption::GetOptionString("sale", "1C_FILE_SIZE_LIMIT", 200*1024),
        "SITE_NEW_ORDERS" => COption::GetOptionString("sale", "1C_SITE_NEW_ORDERS", "s1"),
        "IMPORT_NEW_ORDERS" => COption::GetOptionString("sale", "1C_IMPORT_NEW_ORDERS", "N"),
        'SKIP_SOURCE_CHECK' => 'Y',
        )
    );
}
if ($type == 'import_acts')
{
    $APPLICATION->IncludeComponent("itin:clients.acts.import.1c", "", Array(
        "SITE_LIST" => COption::GetOptionString("sale", "1C_SALE_SITE_LIST", ""),
        "EXPORT_PAYED_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_PAYED_ORDERS", ""),
        "EXPORT_ALLOW_DELIVERY_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_ALLOW_DELIVERY_ORDERS", ""),
        "EXPORT_FINAL_ORDERS" => COption::GetOptionString("sale", "1C_EXPORT_FINAL_ORDERS", ""),
        "FINAL_STATUS_ON_DELIVERY" => COption::GetOptionString("sale", "1C_FINAL_STATUS_ON_DELIVERY", "F"),
        "REPLACE_CURRENCY" => COption::GetOptionString("sale", "1C_REPLACE_CURRENCY", ""),
        "GROUP_PERMISSIONS" => explode(",", COption::GetOptionString("sale", "1C_SALE_GROUP_PERMISSIONS", "1")),
        "USE_ZIP" => COption::GetOptionString("sale", "1C_SALE_USE_ZIP", "N"),
        "INTERVAL" => COption::GetOptionString("sale", "1C_INTERVAL", 30),
        "FILE_SIZE_LIMIT" => COption::GetOptionString("sale", "1C_FILE_SIZE_LIMIT", 200*1024),
        "SITE_NEW_ORDERS" => COption::GetOptionString("sale", "1C_SITE_NEW_ORDERS", "s1"),
        "IMPORT_NEW_ORDERS" => COption::GetOptionString("sale", "1C_IMPORT_NEW_ORDERS", "N"),
        'SKIP_SOURCE_CHECK' => 'Y',
        )
    );
}
else
{
    $APPLICATION->RestartBuffer();
	echo "failure\n";
	echo "Unknown command type.";
}

