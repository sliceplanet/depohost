<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
global $APPLICATION;
$APPLICATION->RestartBuffer();

$ID = intval($_REQUEST['ID']);
if ($ID>0)
{
    
    \Bitrix\Main\Loader::includeModule('clients');
    $client = new \Itin\Depohost\Clients;
    $user_id = $client->getUserId($ID);
    $user = new \CUser;
    $res = $user->GetByID($user_id);
    $password = $client->getPassword($user_id);
    if($password!==false && $ar_user = $res->Fetch())
    {
        
        $ob  = new \Bitrix\Main\SiteTable;
        $res = $ob->getList();
        $arSites = array();
        while ($ar_site = $res->fetch())
        {
            $arSites[] = $ar_site['LID'];
        }
        $arEmail = array();
        $arEmail[] = $ar_user['EMAIL'];
        $arProfile = $client->getProfile();
        foreach ($arProfile as $prop)
        {
            if (strpos($prop['PROP_CODE'], 'EMAIL')!==false)
            {
                $arEmail[] = $prop['VALUE'];
            }
        }
        $arEmail = array_unique($arEmail);
        $email = implode(', ', $arEmail);
        
        $arFields = array('PASSWORD'=>$password, 'LOGIN'=>$ar_user['LOGIN'],'EMAIL'=>$email);
        CAdminMessage::ShowNote('Пароль отправлен');

        \CEvent::SendImmediate('CLIENT_SEND_PASSWORD',$arSites,$arFields);
//        \CEvent::Send('CLIENT_SEND_PASSWORD',$arSites,$arFields);

    }
    else
    {
        CAdminMessage::ShowMessage('Ошибка: Пароль не может быть отправлен');
    }

}