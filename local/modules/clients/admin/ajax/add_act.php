<?php
define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/iblock/admin_tools.php");

use Bitrix\Main\Loader;

Loader::includeModule('clients');
Loader::includeModule('iblock');

global $APPLICATION;
if (isset($_REQUEST['isAjax']) || isset($_REQUEST['ajax']))
{
    $GLOBALS['APPLICATION']->RestartBuffer();
}
// --- MAKE FIELDS

$arFields = array();

$LINK_IBLOCK_ID = intval($_REQUEST['LINK_IBLOCK_ID']);
$IBLOCK_ID = intval($_REQUEST['IBLOCK_ID']);
$PROPERTY_ID = intval($_REQUEST['PROPERTY_ID']);
$ID = intval($_REQUEST['ID']);
$return_url = $_REQUEST['return_url'];
$isAjax = $_REQUEST['isAjax'];
$invoice_id = intval($_REQUEST['invoice_id']);
// если на основании счета, то заполняем данными из счета
if ($invoice_id>0)
{
    $invoice_ob = new \Itin\Depohost\Invoices($invoice_id);
    $arInvoice = $invoice_ob->getById();
    $arInvoice = $invoice_ob->getFormat($arInvoice);
    $values['SUMM'] = $arInvoice['SUMM'];
//    $values['DESCRIPTION'] = $arInvoice['DESCRIPTION'];
    $values['SERVICES'] = $arInvoice['SERVICES'];
    $values['LINK_INVOICE'] = $invoice_id;
}
else
{
    $ob = new \CIBlockElement;
    $res = $ob->GetList(array(),array('ID'=>$ID,'IBLOCK_ID'=>$IBLOCK_ID),false,false,array('PROPERTY_CC_ABONPLATA'));
    if ($ar = $res->Fetch())
    {
        $abonplata = $ar['PROPERTY_CC_ABONPLATA_VALUE'];
//        $description = $ar['PROPERTY_CC_BILL_DESCRIPTION_VALUE'];
    }
}

$ob = new \Bitrix\Iblock\PropertyTable;

$arParams = array(
    'select' => array('*'),
    'filter' => array('IBLOCK_ID'=>$LINK_IBLOCK_ID,'ACTIVE'=>'Y'),
    'order' => array('SORT'=>'ASC')
);
$res = $ob->getList($arParams);

while($ar = $res->fetch())
{
    if ($ar['USER_TYPE']=='DateTime')
    {
        $ar['PROPERTY_TYPE'] = 'DateTime';
    }
    $arFields[$ar['CODE']] = array(
        'REQUIRED' => $ar['IS_REQUIRED'] == 'Y' ? true : false,
        'TYPE' => $ar['PROPERTY_TYPE'],
        'LABEL' => $ar['NAME'],
        'DEFAULT_VALUE' => $ar['DEFAULT_VALUE'],
        'HINT' => $ar['HINT'],
    );
}

// --- END MAKE FIELDS
$aTabs = array();
$aTabs[] = array(
    'DIV'=>'edit_main',
    'TAB' => 'Добавления акта',
    "ICON"=>"iblock_element",
);
// --- HANDLER REQUEST

CAllFile::ConvertFilesToPost($_FILES['FIELDS'], $FILE);

$fields = $_REQUEST['FIELDS'];
if (is_array($FILE))
{
    $fields = array_merge($fields,$FILE);
}
if(!empty($fields))
{
    if (!function_exists('bx_checked'))
    {
        function bx_checked($value)
        {
            return $value=='Y';
        }
    }
    $fields['SERVICES'] = array_keys(array_filter($fields['SERVICES'], "bx_checked"));
    foreach ($fields as $key => $value)
    {
        if (isset($_REQUEST['ajax']) || isset($_REQUEST['isAjax']))
        {
            if (defined('LANG_CHARSET') && strtoupper(LANG_CHARSET)!='UTF-8')
            {
                $value = !is_array($value) ? iconv('UTF-8', LANG_CHARSET, $value) : $value;
            }
            $values[$key] = $value;
        }
        else
        {
            $values[$key] = $value;
        }
        if ($arFields[$key]['TYPE']!='DateTime')
        {
            // @todo проверять регуляркой или на соответствие формату даты сайта
        }
        if ($arFields[$key]['TYPE']!='F')
        {
            //$values[$key] = htmlspecialcharsbx($values[$key]);
        }
    }
    $values['CLIENT'] = $ID;
}
else
{
    $act_ob = new \Itin\Depohost\Acts;
    $arParams = array(
        'order'=>array('property_NUMBER'=>'DESC'),
        'select' => array('PROPERTY_NUMBER'),
        'filter' => array('PROPERTY_CLIENT'=>$ID),
        'limit' => 1,
    );
    $ar = $act_ob->getList($arParams);
    $ar_temp = current($ar);
    $next_number = intval($ar_temp['PROPERTY_NUMBER_VALUE']) + 1;
    $values['NUMBER'] = $next_number;
}

// --- SHOW ERRORS
$error_message = array();

foreach ($arFields as $code => $arField)
{
    if ($arField['REQUIRED'] && empty($values[$code]) && isset($_REQUEST['FIELDS'][$code]))
    {
        $error = new CAdminException(array(array('text'=>'Не заполнено поле '.$arField['LABEL'],'id'=>$code)));
        $GLOBALS["APPLICATION"]->ThrowException($error);
        $message = new CAdminMessage(array(),$error);
        echo $message->Show();
    }
}

// --- END SHOW ERRORS
/*if (empty($error_message))
{


}*/
// --- END HANDLER REQUEST
if (!empty($fields) && empty($error))
{
    $ob = new \CIBlockElement;
    $name = empty($values['NUMBER']) ? 'б/н' : '№'.$values['NUMBER'];
    $name .= ' от '. $values['DATE'];
    $element_id = $ob->Add(array('NAME'=>$name,'IBLOCK_ID'=>$LINK_IBLOCK_ID, 'ACTIVE'=>'Y', 'PROPERTY_VALUES'=>$values));

    $res = $ob->GetProperty($IBLOCK_ID,$ID,array(),array('ID'=>$PROPERTY_ID,'EMPTY'=>'N'));

    while ($ar = $res->Fetch())
    {
        $ar_element_id[] = $ar['VALUE'];
    }

    $ar_element_id[] = $element_id;
    $ob->SetPropertyValuesEx($ID,$IBLOCK_ID,array($PROPERTY_ID=>$ar_element_id));
//    $ob->SetPropertyValuesEx($ID,$IBLOCK_ID,array($PROPERTY_ID=>array('VALUE'=>$ar_element_id)));

    CUtil::InitJSCore(array('jquery'));
    echo '<script>'
    . 'BX(\'BUFF_'.$PROPERTY_ID.'\').value=\''.$element_id.'\';'
    . 'BX(\'BUFF_'.$PROPERTY_ID.'\').onchange();'
    . 'BX.WindowManager.Get().Close();'
    . '</script>';
}
else
{
    $form = new CAdminForm('add_act',$aTabs);

    $form->SetShowSettings(false);
    $form->Begin();
    $form->BeginPrologContent();
    $form->EndPrologContent();
    $form->BeginEpilogContent();
    if (isset($isAjax))
    {
        echo '<input type="hidden" name="isAjax" value="'.($isAjax=='Y' ? 'Y' : 'N').'">';
    }
    echo '<input type="hidden" name="IBLOCK_ID" value="'.$IBLOCK_ID.'">'
        . '<input type="hidden" name="LINK_IBLOCK_ID" value="'.$LINK_IBLOCK_ID.'">'
        . '<input type="hidden" name="PROPERTY_ID" value="'.$PROPERTY_ID.'">'
        . '<input type="hidden" name="ID" value="'.$ID.'">'
        . '<input type="hidden" name="invoice_id" value="'.$invoice_id.'">'
        . '<input type="hidden" name="return_url" value="'.htmlspecialcharsbx($return_url).'">';
    if ($invoice_id>0)
    {
        echo '<input type="hidden" name="FIELDS[LINK_INVOICE]" value="'.$invoice_id.'">';
    }
    $form->EndEpilogContent();
    $form->BeginNextFormTab();


    foreach ($arFields as $code => $arField)
    {
        if ($code!='DOCUMENT' && $code!='DATE'
                && $code!='SUMM' && $code!='DESCRIPTION'
                && $code!='SERVICES')
        {
            switch ($arField['TYPE'])
            {
                case 'S':
                case 'N':
                    $form->AddEditField('FIELDS['.$code.']',$arField['LABEL'],$arField['REQUIRED'],array(),$values[$code]);
                    break;
                case 'DateTime':
                    $form->AddCalendarField('FIELDS['.$code.']',$arField['LABEL'],$values[$code],$arField['REQUIRED']);
                    break;
                case 'F':
                    $form->AddFileField('FIELDS['.$code.']',$arField['LABEL'],$FILE,$arField['REQUIRED']);
                    break;
                default:
                    break;
            }
        }
        elseif($code=='DATE')
        {
            // ставим по значение дефолту
            $values[$code] = !empty($values[$code]) ? $values[$code] : FormatDate('SHORT',time());
            $form->AddCalendarField('FIELDS['.$code.']',$arField['LABEL'],$values[$code],$arField['REQUIRED']);

        }
        elseif ($code=='SUMM')
        {
            // ставим по значение дефолту
            $values[$code] = !empty($values[$code]) ? $values[$code] : $abonplata;
            $form->AddEditField('FIELDS['.$code.']',$arField['LABEL'],$arField['REQUIRED'],array(),$values[$code]);

        }
    }
    $arPropLinks = array();
    $bPropertyAjax = false;
    \Bitrix\Main\Loader::includeModule('clients');
    $service = \Itin\Depohost\Clients::getHL('service');
    $arParams = array(
        'select' => array('UF_NAME','ID','UF_BILL'),
        'filter' => array('=UF_CLIENT'=>$ID)
    );
    $res = $service->getList($arParams);
    $arServices = array();
    while ($ar = $res->fetch())
    {
        $arPropLinks[] = 'FIELDS[SERVICES]['.$ar['ID'].']';
        $arServices[] = $ar;
    }

    $form->AddFieldGroup("SERVICES", 'Перечень услуг', $arPropLinks, $bPropertyAjax);
    foreach ($arServices as $arService)
    {
        if (isset($values['SERVICES']))
        {
            $boolChecked = in_array($arService['ID'],$values['SERVICES']);
        }
        else
        {
            $boolChecked = intval($arService['UF_BILL'])==0 ? false : true;
        }

        $form->AddCheckBoxField('FIELDS[SERVICES]['.$arService['ID'].']',$arService['UF_NAME'],false, $arService['ID'],$boolChecked);
    }

    $form->Show();
?>
<script>
    BX.WindowManager.Get().SetButtons([
        BX.CDialog.prototype.btnSave,
        ,BX.CDialog.prototype.btnClose]);
</script>
<?php
}
?>


