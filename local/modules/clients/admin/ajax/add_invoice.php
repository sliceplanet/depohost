<?php
use Itin\Depohost\Invoices;

define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/iblock/admin_tools.php");
global $APPLICATION;
if (isset($_REQUEST['isAjax']) || isset($_REQUEST['ajax']))
{
    $GLOBALS['APPLICATION']->RestartBuffer();
}
// --- MAKE FIELDS

$arFields = array();

$LINK_IBLOCK_ID = intval($_REQUEST['LINK_IBLOCK_ID']);
$IBLOCK_ID = intval($_REQUEST['IBLOCK_ID']);
$PROPERTY_ID = intval($_REQUEST['PROPERTY_ID']);
$ID = intval($_REQUEST['ID']);
$return_url = $_REQUEST['return_url'];
$isAjax = $_REQUEST['isAjax'];
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('clients');
$ob = new \Bitrix\Iblock\PropertyTable;

$arParams = array(
    'select' => array('*'),
    'filter' => array('IBLOCK_ID'=>$LINK_IBLOCK_ID,'ACTIVE'=>'Y'),
    'order' => array('SORT'=>'ASC')
);
$res = $ob->getList($arParams);
$arLastInvoice = Invoices::getLastInvoice($ID);

while($ar = $res->fetch())
{
    if ($ar['USER_TYPE']=='DateTime')
    {
        $ar['PROPERTY_TYPE'] = 'DateTime';
    }

    $code = $ar['CODE'];
    if ($code == 'COUNT_MONTHS' || $code=='DISCOUNT')
    {
        $ar['DEFAULT_VALUE'] = doubleval($arLastInvoice[$code]['VALUE'])>0 ? $arLastInvoice[$code]['VALUE'] : $ar['DEFAULT_VALUE'];
    }

    $arFields[$code] = array(
        'REQUIRED' => $ar['IS_REQUIRED'] == 'Y' ? true : false,
        'TYPE' => $ar['PROPERTY_TYPE'],
        'LABEL' => $ar['NAME'],
        'DEFAULT_VALUE' => $ar['DEFAULT_VALUE'],
        'HINT' => $ar['HINT'],
    );
    $values[$code] = $ar['DEFAULT_VALUE'];
}

// --- END MAKE FIELDS
$aTabs = array();
$aTabs[] = array(
    'DIV'=>'edit_main',
    'TAB' => 'Добавления счета',
    "ICON"=>"iblock_element",
);
// --- HANDLER REQUEST

CAllFile::ConvertFilesToPost($_FILES['FIELDS'], $FILE);
$fields = $_REQUEST['FIELDS'];
if (is_array($FILE))
{
    $fields = array_merge($fields,$FILE);
}
if(!empty($fields))
{
    function bx_checked($value)
    {
        return $value=='Y';
    }
    $fields['SERVICES'] = array_keys(array_filter($fields['SERVICES'], "bx_checked"));
    foreach ($fields as $key => $value)
    {
        if (isset($_REQUEST['ajax']) || isset($_REQUEST['isAjax']))
        {
            if (defined('LANG_CHARSET') && strtoupper(LANG_CHARSET)!='UTF-8')
            {
                $value = !is_array($value) ? iconv('UTF-8', LANG_CHARSET, $value) : $value;
            }
            $values[$key] = $value;
        }
        else
        {
            $values[$key] = $value;
        }
        if ($arFields[$key]['TYPE']!='DateTime')
        {
            // @todo проверять регуляркой или на соответствие формату даты сайта
        }
        if ($arFields[$key]['TYPE']!='F')
        {
            //$values[$key] = htmlspecialcharsbx($values[$key]);
        }
    }

    $values['CLIENT'] = $ID;
}

// --- SHOW ERRORS
$error_message = array();

foreach ($arFields as $code => $arField)
{
    if ($arField['REQUIRED'] && empty($values[$code]) && isset($_REQUEST['FIELDS'][$code]))
    {
        $error = new CAdminException(array(array('text'=>'Не заполнено поле '.$arField['LABEL'],'id'=>$code)));
        $GLOBALS["APPLICATION"]->ThrowException($error);
        $message = new CAdminMessage(array(),$error);
        echo $message->Show();
    }
}

// --- END SHOW ERRORS
/*if (empty($error_message))
{


}*/
if (!is_array($arServices))
{
    \Bitrix\Main\Loader::includeModule('clients');
    $service = \Itin\Depohost\Clients::getHL('service');
    $arParams = array(
        'select' => array('UF_NAME','ID','UF_BILL','UF_PRICE','UF_QUANTITY'),
        'filter' => array('=UF_CLIENT'=>$ID)
    );
    $res = $service->getList($arParams);
    $arServices = array();
    while ($ar = $res->fetch())
    {
        $arPropLinks[] = 'FIELDS[SERVICES]['.$ar['ID'].']';
        $arServices[] = $ar;
    }
}

$sum = 0;
foreach ($arServices as $key => $arService)
{
    if((!empty($values['SERVICES']) && in_array($arService['ID'],$values['SERVICES']))
        || (empty($_REQUEST['FIELDS']['SERVICES']) && $arService['UF_BILL'] == 1)
    )
    {
        $price = $arService['UF_PRICE']*$arService['UF_QUANTITY'];
        $priceWithDiscount = $price - $price*$values['DISCOUNT']/100;
        $sum += $priceWithDiscount*$values['COUNT_MONTHS'];
        $arServices[$key]['CHECKED'] = true;
    }
}
$values['SUMM'] = $sum;

// --- END HANDLER REQUEST
if (!empty($fields) && empty($error) && !array_key_exists('calc',$_GET))
{
//    $ob = new \CIBlockElement;
    \Bitrix\Main\Loader::includeModule('clients');
    $name = empty($values['NUMBER']) ? 'б/н' : '№'.$values['NUMBER'];
    $name .= ' от '. $values['DATE'];
    $invoice = new \Itin\Depohost\Invoices;
    $element_id = $invoice->add(array('NAME'=>$name,'IBLOCK_ID'=>$LINK_IBLOCK_ID, 'ACTIVE'=>'Y', 'PROPERTY_VALUES'=>$values));
    CUtil::InitJSCore(array('jquery'));
    echo '<script>'
    . 'BX(\'BUFF_'.$PROPERTY_ID.'\').value=\''.$element_id.'\';'
    . 'BX(\'BUFF_'.$PROPERTY_ID.'\').onchange();'
    . 'BX.WindowManager.Get().Close();'
    . '</script>';
}
else
{

    $ob = new \CIBlockElement;
    $res = $ob->GetList(array(),array('ID'=>$ID,'IBLOCK_ID'=>$IBLOCK_ID),false,false,array('PROPERTY_CC_BILL_DESCRIPTION'));

    if ($ar = $res->Fetch())
    {
        $description = $ar['PROPERTY_CC_BILL_DESCRIPTION_VALUE'];
    }

    $form = new CAdminForm('add_invoice',$aTabs);

    $form->SetShowSettings(false);
    $form->Begin();
    $form->BeginPrologContent();
    $form->EndPrologContent();
    $form->BeginEpilogContent();
    if (isset($isAjax))
    {
        echo '<input type="hidden" name="isAjax" value="'.($isAjax=='Y' ? 'Y' : 'N').'">';
    }
    echo '<input type="hidden" name="IBLOCK_ID" value="'.$IBLOCK_ID.'">'
            . '<input type="hidden" name="LINK_IBLOCK_ID" value="'.$LINK_IBLOCK_ID.'">'
            . '<input type="hidden" name="PROPERTY_ID" value="'.$PROPERTY_ID.'">'
            . '<input type="hidden" name="ID" value="'.$ID.'">'
            . '<input type="hidden" name="return_url" value="'.htmlspecialcharsbx($return_url).'">';
    $form->EndEpilogContent();
    $form->BeginNextFormTab();


    foreach ($arFields as $code => $arField)
    {
        if ($code!='NUMBER' && $code!='DOCUMENT' && $code!='DATE'
                && $code!='SUMM' && $code!='DESCRIPTION'
                && $code!='SERVICES' && $code!='CHANGE_STATUS')
        {
            switch ($arField['TYPE'])
            {
                case 'S':
                case 'N':
                    $form->AddEditField('FIELDS['.$code.']',$arField['LABEL'],$arField['REQUIRED'],array(),$values[$code]);
                    break;
                case 'DateTime':
                    $form->AddCalendarField('FIELDS['.$code.']',$arField['LABEL'],$values[$code],$arField['REQUIRED']);
                    break;
                case 'F':
                    $form->AddFileField('FIELDS['.$code.']',$arField['LABEL'],$FILE,$arField['REQUIRED']);
                    break;
                default:
                    break;
            }
        }
        elseif($code=='DATE')
        {
            // ставим по значение дефолту
            $values[$code] = !empty($values[$code]) ? $values[$code] : FormatDate('SHORT',time());
            $form->AddCalendarField('FIELDS['.$code.']',$arField['LABEL'],$values[$code],$arField['REQUIRED']);

        }
        elseif ($code=='SUMM')
        {
            // ставим по значение дефолту
            $values[$code] = !empty($values[$code]) ? $values[$code] : $sum;
            $form->AddEditField('FIELDS['.$code.']',$arField['LABEL'],$arField['REQUIRED'],array(),$values[$code]);

        }
        elseif ($code=='DESCRIPTION')
        {
            // ставим значение по дефолту
            $values[$code] = !empty($values[$code]) ? $values[$code] : $description;
            $form->AddEditField('FIELDS['.$code.']',$arField['LABEL'],$arField['REQUIRED'],array(),$values[$code]);
        }
    }
    /** Services view -->*/
    $arPropLinks = array();
    $bPropertyAjax = false;

    $form->AddFieldGroup("SERVICES", 'Перечень услуг', $arPropLinks, $bPropertyAjax);
    foreach ($arServices as $arService)
    {
        $label = $arService['UF_NAME'];
        $price = $arService['UF_PRICE'];
        $priceWithDiscount = $price - $price*$values['DISCOUNT']/100;

        $price = '<b>'.CurrencyFormat($priceWithDiscount,'RUB').'</b>';
        $checked = $arService['CHECKED'];
        $id = 'FIELDS[SERVICES]['.$arService['ID'].']';
        $html = '<input type="checkbox" name="'.$id.'" value="'.htmlspecialcharsbx($arService['ID']).'"'.($checked? ' checked': '').'>';
        $hidden = '<input type="hidden" name="'.$id.'" value="'.htmlspecialcharsbx($arService['ID']).'">';
        $form->BeginCustomField($id,$label);
            echo '            <tr>
                <td width="40%">' ,$label,'</td><td>' ,$html.'&nbsp;&nbsp;',$price, '</td>
            </tr>    ';
        $form->EndCustomField($id,$hidden);
    }
    /** <--Services*/
    $form->Show();
?>
<script>
    BX.CDialog.prototype.btnCalc = BX.CDialog.btnCalc = {
        title: 'Рассчитать',
        id: 'calcbtn',
        name: 'calcbtn',
        className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-calc',
        action: function () {
            this.disableUntilError();
            this.parentWindow.PostParameters('calc=yes');
        }
    };
    BX.WindowManager.Get().SetButtons([
        BX.CDialog.prototype.btnSave,
        BX.CDialog.prototype.btnClose,
        BX.CDialog.prototype.btnCalc]);
</script>
<style>
    .adm-btn-calc{
        float: right;
    }
</style>
<?php
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");

?>


