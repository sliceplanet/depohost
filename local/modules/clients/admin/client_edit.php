<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

\Bitrix\Main\Loader::includeModule('clients');
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/prolog.php");
IncludeModuleLangFile(__FILE__);

include_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/iblock/admin/iblock_element_edit.php");
