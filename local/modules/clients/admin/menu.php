<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Bitrix\Main\Loader::includeModule('iblock');
global $USER;

$ar = CIBlockRights::LetterToOperations('S');
$rights_read = current($ar);
$hasRightsClients = CIBlockRights::UserHasRightTo(34, 34, $rights_read);
$hasRightsInvoice = CIBlockRights::UserHasRightTo(38, 38, $rights_read);
$hasRightsActs = CIBlockRights::UserHasRightTo(39, 39, $rights_read);

$aMenu = array(
    "parent_menu" => "global_menu_services",
    "section" => "clients",
    "sort" => 1,
    "text" => Loc::getMessage('clients_menu_text'),
    "title" => Loc::getMessage('clients_menu_title'),
    "icon" => "sonet_menu_icon",
    "page_icon" => "blog_page_icon",
    "items_id" => "clients_menu",
    "items" =>
    array()
);
if ($hasRightsClients)
{
    $aMenu['items'][] = array(
        "text" => Loc::getMessage('clients_list_menu_text'),
        "url" => "/bitrix/admin/clients_list.php?lang=ru&IBLOCK_ID=34&type=personal", //@todo брать из настроек модуля
        "more_url" => array('client_edit.php'),
        "title" => Loc::getMessage('clients_list_menu_title'),
        "sort" => 100
    );
}

if ($hasRightsInvoice)
{
    $aMenu['items'][] = array(
        "text" => Loc::getMessage('clients_invoice_menu_text'),
        "url" => "/bitrix/admin/clients_invoice_list.php?IBLOCK_ID=38&type=personal&lang=ru&find_section_section=0",
        "more_url" => array('clients_invoice_edit.php?IBLOCK_ID=38&type=personal'),
        "title" => Loc::getMessage('clients_invoice_menu_title'),
        "sort" => 300
    );
}
if ($hasRightsActs)
{
    $aMenu['items'][] = array(
        "text" => Loc::getMessage('clients_acts_menu_text'),
        "url" => "/bitrix/admin/clients_acts_list.php?IBLOCK_ID=39&type=personal&lang=ru&find_section_section=0",
        "more_url" => array('clients_acts_edit.php?IBLOCK_ID=39&type=personal'),
        "title" => Loc::getMessage('clients_acts_menu_title'),
        "sort" => 400
    );
}

if ($APPLICATION->GetGroupRight("sale") != "D")
{
    $aMenu['items'][] = array(
        "text" => Loc::getMessage('clients_sale_menu_text'),
        "url" => "/bitrix/admin/sale_order.php?lang=ru",
        "more_url" => array(),
        "title" => Loc::getMessage('clients_sale_menu_title'),
        "sort" => 500
    );
}
if ($APPLICATION->GetGroupRight("clients") != "D")
{
    $aMenu['items'][] = array(
        "text" => Loc::getMessage('clients_bill_menu_text'),
        "url" => "clients_bill_list.php?lang=ru&IBLOCK_ID=34&type=personal", //@todo брать из настроек модуля
        "more_url" => array('client_bill_edit.php'),
        "title" => Loc::getMessage('clients_bill_menu_title'),
        "sort" => 200
    );
}
foreach ($aMenu['items'] as $key => $value)
{
    $arSort[$key] = $value['sort'];
}
array_multisort($arSort, SORT_ASC, SORT_NUMERIC, $aMenu['items']);
if (empty($aMenu['items']))
{
    unset($aMenu);
}
return $aMenu;
