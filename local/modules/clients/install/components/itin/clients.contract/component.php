<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Clients;
use Itin\Depohost\Info;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('clients');
$IBLOCK_ID = Info::getIblockId('acts');

$user_id = intval($USER->GetID());
$client_ob = new Clients($user_id);

$client_id = $client_ob->getElementId();
$ar = $client_ob->getById($client_id);
$arFiles = $ar['CC_CONTRACT']['VALUE'];

foreach ($arFiles as $key => $file_id)
{   
    $arFile = CFile::GetFileArray($file_id);
    $arResult['ITEMS'][$key] = array(
        'NAME' => $arFile['FILE_NAME'],
        'SRC' => $arFile['SRC'],
    );
}
$arResult['ITEMS'] = array_values($arResult['ITEMS']);
$this->IncludeComponentTemplate();
