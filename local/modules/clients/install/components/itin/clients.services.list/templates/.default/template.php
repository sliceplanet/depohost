<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);  
//echo '<pre>';
//var_dump($arResult['ITEMS']);
//return;
?>
<table width="100%" class="blue">
    <tbody>
        <tr>
            <th><?=Loc::getMessage('TH_NAME');?></th>
            <th><?=Loc::getMessage('TH_PROPERTIES');?></th>
            <th width="80"><?=Loc::getMessage('TH_QUANTITY');?></th>
            <th><?=Loc::getMessage('TH_PRICE');?></th>
            <th><?=Loc::getMessage('TH_SUMM');?></th>
        <?if ($arResult['IS_DEDICATED']):?>
            <th><?=Loc::getMessage('TH_IP_ADDR');?></th>
            <th><?=Loc::getMessage('TH_SERVER_ID');?></th>
        <?endif;?>
        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="5"><?=Loc::getMessage('EMPTY_RESULT')?></td> 
        </tr> 
    <?
else:
    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        ?>
        <tr id="el_<?=$element_id?>">
            <td><?=$arItem['NAME']?></td> 
            <td><?=implode('<br />',$arItem['PROPERTIES'])?></td>            
            <td align="right"><?=$arItem['QUANTITY']?></td>                      
            <td align="right"><?=CurrencyFormat($arItem['PRICE'],'RUB')?></td>            
            <td align="right"><?=CurrencyFormat($arItem['SUMM'],'RUB')?></td>  
         <?if ($arResult['IS_DEDICATED']):?>
            <td><?=implode('<br />',$arItem['IP_ADDR'])?></td>
            <td align="center"><?=$arItem['SERVER_ID']?></td>
        <?endif;?>
        </tr>  
    <?endforeach;?>
        <tr>
            <td colspan="<?=$arResult['IS_DEDICATED'] ? 6 : 4;?>" align="right"><?=Loc::getMessage('FOOT_SUMM');?></td>
            <td align="right"><?=CurrencyFormat($arResult['SUMM'],'RUB')?></td>
        </tr>
    <?
endif;
?>
    </tbody>
</table>    
