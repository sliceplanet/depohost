<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);    
?>
<table width="100%" class="blue">
    <tbody>
        <tr>
            <th><?=Loc::getMessage('TH_NUMBER');?></th>
            <th><?=Loc::getMessage('TH_DATE');?></th>
            <th><?=Loc::getMessage('TH_DESCRIPTION');?></th>
            <th><?=Loc::getMessage('TH_SUMM');?></th>
            <th><?=Loc::getMessage('TH_DOCUMENT');?></th>
        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="5"><?=Loc::getMessage('EMPTY_RESULT')?></td> 
        </tr> 
    <?
else:
    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        $fileName = 'Акт '.$arItem['NAME'].'.pdf';
        ?>
        <tr id="el_<?=$element_id?>">
            <td align="right"><?=$arItem['NUMBER']?></td> 
            <td align="center"><?=$arItem['DATE']?></td>            
            <td><?=$arItem['DESCRIPTION']?></td>                      
            <td align="right"><?=CurrencyFormat($arItem['SUMM'],'RUB')?></td>            
            <td><a href="<?=$arItem['DOCUMENT']?>" target="_blank" download="<?=$fileName?>"><?=$fileName?></a></td>
        </tr>  
    <?endforeach;
endif;
?>
    </tbody>
</table>    
