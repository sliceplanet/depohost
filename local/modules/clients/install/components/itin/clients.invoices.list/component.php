<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Invoices;
use Itin\Depohost\Info;
use Itin\Depohost\Clients;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('clients');
$IBLOCK_ID = Info::getIblockId('invoices');
$user_id = intval($USER->GetID());
$client_ob = new Clients($user_id);

$client_id = $client_ob->getElementId();
$ar = $client_ob->getById($client_id);
$arElements = $ar['CC_BILLING']['VALUE'];

if (!empty($_REQUEST['invoice_id']) && !empty($_REQUEST['invoice_type']))
{
    $element_id = intval($_REQUEST['invoice_id']);
    $invoice = new Invoices(intval($element_id));
    $arInvoice = $invoice->getById();
    $client_card_id = $arInvoice['CLIENT']['VALUE'];
    
    if ($client_card_id == $client_id || $USER->IsAdmin())
    {
        switch ($_REQUEST['invoice_type'])
        {
            case 'sb_bill':
                // ��������� ��� ������ � �������� �������
                $arProp = \CIBlockPropertyEnum::GetList(array("SORT"=>"ASC", "VALUE"=>"ASC"),array('EXTERNAL_ID'=>'sb_bill','CODE'=>'TYPE_PAYMENT','IBLOCK_ID'=>$IBLOCK_ID = Info::getIblockId('ccards')))->Fetch();

                // ��������� ��� ������ � �������� �������
                $client_ob->updateProperties(array($arProp['PROPERTY_ID']=>$arProp['ID']));
                $file_id = $invoice->generateSbPdfEx();
                break;

            default:
                $arProp = \CIBlockPropertyEnum::GetList(array("SORT"=>"ASC", "VALUE"=>"ASC"),array('EXTERNAL_ID'=>'bill','CODE'=>'TYPE_PAYMENT','IBLOCK_ID'=>$IBLOCK_ID = Info::getIblockId('ccards')))->Fetch();
                $client_ob->updateProperties(array($arProp['PROPERTY_ID']=>$arProp['ID']));
                $file_id = $invoice->generatePdfEx();
                break;
        }
        $file = $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($file_id);
        $file_ob = new \Bitrix\Main\IO\File($file);
        if ($file_ob->isExists()) {
            // ���������� ��� ������ ������������  ������� �� �� �� ����        
            while (ob_get_level()) ob_end_clean();    
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'. basename($file).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            $length = $file_ob->readFile();
            \CFile::Delete($file_id);
            exit;
        }
    }
    else
    {
        $arResult['ERRORS'] = Loc::getMessage('ERROR_USER_ID')."\r\n";
    }
        
}
$profile_id = intval($client_ob->getPersonTypeId());
// ��� ����
$arResult['IS_FIZ'] = $profile_id==1;

foreach ($arElements as $element_id)
{
    $invoice = new Invoices(intval($element_id));
    $arTemp = $invoice->getById($element_id);
    $arData = $invoice->getFormat($arTemp);
    $arResult['ITEMS'][$element_id] = $arData;
    $contract_date = $arData['CLIENT']['DATE_CREATE'];
    $arResult['ITEMS'][$element_id]['DESCRIPTION'] = Loc::getMessage('TEMPLATE_DESCRIPTION',
        array(
            '#CONTRACT_ID#'=>$arData['CLIENT']['NAME'],
            '#CONTRACT_DATE#'=>FormatDateEx($contract_date,false,'dd.mm.YYYY')
        )
    )." ".FormatDateEx($arData['DATE'],false,'f YYYY');
}

$this->IncludeComponentTemplate();
