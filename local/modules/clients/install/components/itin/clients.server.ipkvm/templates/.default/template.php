<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']))
{
    foreach ($arResult['ERRORS'] as $message)
    {
        ShowError($message);
    }
}
if (!empty($arResult['SUCCESS']))
{
    foreach ($arResult['SUCCESS'] as $message)
    {
        ShowNote($message);
    }
}
if (empty($arResult['ERRORS']) && empty($arResult['SUCCESS']))
{
    echo "<p>".Loc::getMessage('DESCRIPTION')."</p>";
}

//echo '<pre>';
//var_dump($arResult);
//return;
?>

<table width="100%" class="blue">
    <tbody>
        <tr>
            <th><?=Loc::getMessage('TH_NAME');?></th>
            <th><?=Loc::getMessage('TH_PROPERTIES');?></th>
            <th><?=Loc::getMessage('TH_IP_ADDR');?></th>
            <th><?=Loc::getMessage('TH_SERVER_ID');?></th>
            <th><?=Loc::getMessage('TH_ACTION');?></th>
        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="5"><?=Loc::getMessage('EMPTY_RESULT')?></td> 
        </tr> 
    <?
else:?>
    <form action="" method="post" id="action">    
    <?
    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        ?>
        <tr id="el_<?=$element_id?>">
            <td><?=$arItem['NAME']?></td> 
            <td><?=implode('<br />',$arItem['PROPERTIES'])?></td>   
            <td><?=implode('<br />',$arItem['IP_ADDR'])?></td>
            <td align="center"><?=$arItem['SERVER_ID']?></td>
            <td align="center"><a class="btn" onclick="sendRequest(this)" data-server="<?=$element_id?>" href="javascript:void(0);"><?=Loc::getMessage('BUTTON_ACTION');?></a></td>
        </tr>  
    <?endforeach;?>
        <input type="hidden" name="server_id" id="server_id" value="" />
    </form>
    <?
endif;
?>
    </tbody>
</table> 
<script>
    function sendRequest(data)
    {
        var server = data.getAttribute('data-server'),
            server_input = document.getElementById('server_id');
        server_input.setAttribute('value',server);
        document.getElementById('action').submit();        
    }
</script>
