<?php

use Bitrix\Main;

$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen("/install/index.php"));
IncludeModuleLangFile($strPath2Lang . "/install.php");

class clients extends CModule
{

  var $MODULE_ID = 'clients';
  var $MODULE_VERSION;
  var $MODULE_VERSION_DATE;
  var $MODULE_NAME;
  var $MODULE_DESCRIPTION;
  var $MODULE_CSS;
  var $errors;

  function clients()
  {
    $arModuleVersion = array();

    $path = str_replace("\\", "/", __FILE__);
    $path = substr($path, 0, strlen($path) - strlen("/index.php"));
    include($path . "/version.php");

    if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
    {
      $this->MODULE_VERSION = $arModuleVersion["VERSION"];
      $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
    }

    $this->MODULE_NAME = GetMessage("CLIENT_INSTALL_NAME");
    $this->MODULE_DESCRIPTION = GetMessage("CLIENT_INSTALL_DESCRIPTION");
  }

  function InstallDB()
  {
    global $DB, $DBType, $APPLICATION;
    $this->errors = false;
    die('213');

    if ($this->errors !== false)
    {
      $APPLICATION->ThrowException(implode("", $this->errors));
      return false;
    }

    return true;
  }

  function UnInstallDB($arParams = array())
  {
    global $DB, $DBType, $APPLICATION;
    $this->errors = false;
    $arSQLErrors = array();


    return true;
  }

  function InstallEvents()
  {

    $eventManager = Main\EventManager::getInstance();
    $eventManager->registerEventHandler("", "ClientsServiceOnAfterAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
    $eventManager->registerEventHandler("", "ClientsServiceOnAfterUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
    $eventManager->registerEventHandler("", "ClientsServiceOnBeforeUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');

    RegisterModuleDependences("sale", "OnBasketOrder", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "BasketOrderHandler", 200);
//    RegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OrderNewSendEmailHandler", 200);
    RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnSaleComponentOrderOneStepComplete", 200);
    RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepOrderProps", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnSaleComponentOrderOneStepOrderPropsHandler", 200);

    RegisterModuleDependences("sale", "OnBasketAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnAfterBasketAddHandler", 200);

    RegisterModuleDependences("main", "OnBeforeUserAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave", 200);
    RegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave", 200);
    RegisterModuleDependences("main", "OnBeforeUserChangePassword", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave", 200);

    RegisterModuleDependences("main", "OnBeforeEventAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "MailAttach", 200);

    RegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "InvoiceHandler", 200);
    RegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "InvoiceHandler", 200);


    RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "addInvoiceToClientHandler", 200);
//            RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", "addInvoiceToClientHandler",200);

    RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "addActToClientHandler", 300);
//            RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", "addActToClientHandler",300);

    RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementAdd", 500);

    RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementUpdate", 500);
    RegisterModuleDependences("iblock", "OnBeforeIBlockElementDelete", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnBeforeIBlockElementDeleteHandler", 500);

    RegisterModuleDependences("support", "OnBeforeTicketUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "handlerBeforeUpdateTicket", 100);

    RegisterModuleDependences("iblock", "OnAfterIBlockElementSetPropertyValuesEx", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementGenerateBillTxt", 100);

    RegisterModuleDependences("iblock", "OnAfterIBlockElementSetPropertyValuesEx", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementSetPropertyValuesEx", 200);

    RegisterModuleDependences("main", "OnBeforeEventSend", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerOnBeforeEventSend", 100);

    return true;
  }

  function UnInstallEvents()
  {
    $eventManager = Main\EventManager::getInstance();
    $eventManager->unRegisterEventHandler("", "ClientsServiceOnAfterAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
    $eventManager->unRegisterEventHandler("", "ClientsServiceOnAfterUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');
    $eventManager->unRegisterEventHandler("", "ClientsServiceOnBeforeUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", 'ClientsServiceHandler');

    UnRegisterModuleDependences("sale", "OnBasketOrder", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "BasketOrderHandler");
//    UnRegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OrderNewSendEmailHandler");
    UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnSaleComponentOrderOneStepComplete");
    UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepOrderProps", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnSaleComponentOrderOneStepOrderPropsHandler");

    UnRegisterModuleDependences("sale", "OnBasketAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnAfterBasketAddHandler");

    UnRegisterModuleDependences("main", "OnBeforeUserAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave");
    UnRegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave");
    UnRegisterModuleDependences("main", "OnBeforeUserChangePassword", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "PasswordSave");

    UnRegisterModuleDependences("main", "OnBeforeEventAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "MailAttach");

    UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "InvoiceHandler");
    UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "InvoiceHandler");

    UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "addInvoiceToClientHandler");
//            UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", "addInvoiceToClientHandler");

    UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "addActToClientHandler");
//            UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID , "\\Itin\\Depohost\\EventHandlers", "addActToClientHandler");

    UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementAdd");

    UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementUpdate");
    UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementDelete", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "OnBeforeIBlockElementDeleteHandler", 500);

    UnRegisterModuleDependences("support", "OnBeforeTicketUpdate", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "handlerBeforeUpdateTicket");

      UnRegisterModuleDependences("iblock", "OnAfterIBlockElementSetPropertyValuesEx", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementGenerateBillTxt");

    UnRegisterModuleDependences("iblock", "OnAfterIBlockElementSetPropertyValuesEx", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerAfterIBlockElementSetPropertyValuesEx");

    UnRegisterModuleDependences("main", "OnBeforeEventSend", $this->MODULE_ID, "\\Itin\\Depohost\\EventHandlers", "HandlerOnBeforeEventSend");

    return true;
  }

  function InstallFiles()
  {
    if ($_ENV["COMPUTERNAME"] != 'BX')
    {
      CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/admin', $_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin", true, true);
      CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components', $_SERVER['DOCUMENT_ROOT'] . "/bitrix/components", true, true);
    }
    return true;
  }

  function UnInstallFiles()
  {
    if ($_ENV["COMPUTERNAME"] != 'BX')
    {
      DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
    }
    return true;
  }

  function DoInstall()
  {
    global $APPLICATION, $step, $obModule;
    $step = IntVal($step);
    Main\ModuleManager::registerModule($this->MODULE_ID);

    $this->InstallFiles();
    $this->InstallEvents();
    $this->InstallTasks();
    $obModule = $this;

    $APPLICATION->IncludeAdminFile(GetMessage("CLIENT_INSTALL_NAME"), $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/step1.php");
  }

  function DoUninstall()
  {
    global $APPLICATION, $step, $obModule;
    $step = IntVal($step);

    $GLOBALS["CACHE_MANAGER"]->CleanAll();
    $this->UnInstallFiles();
    $this->UnInstallEvents();
    $this->UnInstallTasks();
    $obModule = $this;

    Main\Config\Option::delete($this->MODULE_ID);
    Main\ModuleManager::unRegisterModule($this->MODULE_ID);

    $APPLICATION->IncludeAdminFile(GetMessage("CLIENT_INSTALL_NAME"), $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/unstep1.php");
  }

  function GetModuleTasks()
  {
    return array(
        'clients_deny' => array(
            'LETTER' => 'D',
            'BINDING' => $this->MODULE_ID,
            'TITLE' => '������ ��������',
            'DESCRIPTION' => '',
            'OPERATIONS' => array()
        ),
        'clients_read' => array(
            'LETTER' => 'R',
            'BINDING' => $this->MODULE_ID,
            'TITLE' => '��������',
            'DESCRIPTION' => '',
            'OPERATIONS' => array(
                'clients_read',
                'acts_read',
                'ivoices_read',
                'bill_read'
            )
        ),
        'clients_add' => array(
            'LETTER' => 'W',
            'BINDING' => $this->MODULE_ID,
            'TITLE' => '��������������/����������',
            'DESCRIPTION' => '',
            'OPERATIONS' => array(
                'clients_add',
                'acts_add',
                'ivoices_add',
                'bill_add'
            )
        ),
    );
  }

  function GetModuleRightList()
  {
    $arr = array(
        "reference_id" => array("D",
            "R",
            "W"),
        "reference" => array(
            "[D] " . GetMessage("CLIENTS_DENIED"),
            "[R] " . GetMessage("CLIENTS_READ"),
            "[W] " . GetMessage("CLIENTS_WRITE"))
    );
    return $arr;
  }

}

?>