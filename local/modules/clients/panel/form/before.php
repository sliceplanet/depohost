<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__.'/buyers_profile.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/panel/form/admin_tools.php");

global $APPLICATION;
$APPLICATION->AddHeadScript('/bitrix/js/main/ajax.js');


if (intval($_REQUEST['PROFILE_ID'])>0)
{
    if($arProfile = CSaleOrderUserProps::GetByID($PROFILE_ID))
    {        
        $arPerson = CSalePersonType::GetByID($arProfile['PERSON_TYPE_ID']);
        $titleDataClient = $arPerson["NAME"];
    }
    else
    {
        $titleDataClient = 'Данные o клиентe';
    }
}
else
{
    $titleDataClient = 'Данные o клиентe';
    if(!empty($PROP['CC_CLIENT']))
    {
        $USER_ID = $PROP['CC_CLIENT']['VALUE'];
        \Bitrix\Main\Loader::includeModule('clients');
        $clients = new Itin\Depohost\Clients($USER_ID);
        $PROFILE_ID = $clients->getProfileId();
    }
}

$aTabs = array_slice($aTabs,0,1);  // Оставляем только первую закладку
unset($aTabs);
$aTabs[] = array("DIV" => "edit_services", "TAB" => 'Услуги', "ICON"=>"iblock_element", "TITLE"=>'Подключенные услуги клиента');
$aTabs[] = array("DIV" => "edit_billing", "TAB" => 'Счета', "ICON"=>"iblock_element", "TITLE"=>'Счета выставленные клиенту');
$aTabs[] = array("DIV" => "edit_acts", "TAB" => 'Акты', "ICON"=>"iblock_element", "TITLE"=>'Акты выполненных работ');
$aTabs[] = array("DIV" => "edit_data_client", "TAB" => 'Данные o клиентe', "ICON"=>"iblock_element", "TITLE"=>$titleDataClient);
$aTabs[] = array("DIV" => "edit_comment", "TAB" => 'Блокнот', "ICON"=>"iblock_element", "TITLE"=>'Примечания о клиенте');
$aTabs[] = array("DIV" => "edit_support", "TAB" => 'Отправка пароля', "ICON"=>"iblock_element", "TITLE"=>'Отправка пароля');
$aTabs[] = array("DIV" => "edit_sorm", "TAB" => 'SORM', "ICON"=>"iblock_element", "TITLE"=>'Поля для выгрузки в SORM');



$tabControl = new CAdminForm($bCustomForm? "tabControl": "form_".$IBLOCK_ID, $aTabs);
$tabControl->SetShowSettings(false);

