<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/panel/form/admin_tools.php");

$arParams = array(
    'table_id' => 'tbl_billing_list',
    'params' => array(
        'ID' => !empty($ID) ? $ID : $_REQUEST['ID'],
        'IBLOCK_ID' => !empty($IBLOCK_ID) ? $IBLOCK_ID : $_REQUEST['IBLOCK_ID'],
        'PROFILE_ID' => !empty($PROFILE_ID) ? $PROFILE_ID : $_REQUEST['PROFILE_ID'],
        'PROPERTY_CODE' => !empty($ar_fields['CODE']) ? $ar_fields['CODE'] : $_REQUEST['PROPERTY_CODE'],
        'PROPERTY_ID' => !empty($ar_fields['ID']) ? $ar_fields['ID'] : $_REQUEST['PROPERTY_ID'],
        'isAjax' => 'Y',
        'WF' => 'Y'
    ),
    'link' => '/local/modules/clients/panel/form/tabs/billing.php',
//    'link' => '/bitrix/admin/client_edit.php',
    'ajax' => $_REQUEST['isAjax'] == 'Y' ? 'Y' : 'N',
    'map' => array(
        'PROPERTY_STATUS' => 'Статус',
        'PROPERTY_NUMBER' => 'Номер счета',
        'PROPERTY_DATE' => 'Дата счета', 
        'PROPERTY_DESCRIPTION' => 'Назначение платежа',
        'PROPERTY_COUNT_ACTS' => 'Кол-во актов к счету',
        'PROPERTY_SERVICES' => 'Перечень услуг',
        'PROPERTY_SUMM' => 'Сумма',
        'PROPERTY_DOCUMENT' => 'Файл',
        
        
    ),
    'edit_fields' => array(
        'PROPERTY_DATE','PROPERTY_SUMM','PROPERTY_DESCRIPTION','PROPERTY_STATUS','PROPERTY_COUNT_ACTS'
    ),
    'actions' => array(
        array(
            'ICON' => '',
            'TEXT' => 'Отправить',
            'TITLE' => 'Отправить клиенту',
            'ACTION' => 'send_invoice',
        ),
        array(
            'ICON' => '',
            'TEXT' => 'Создать акт',
            'TITLE' => 'Создать акт на основании счета',
            'ACTION' => 'create_act',
        ),        
    ),
    'context_menu' => array(
        array(
            'ICON' => 'btn_new',
            'TEXT' => 'Создать счет',
            'TITLE' => 'Создать новый счет',
            'ACTION' => 'add',
        )
    ),
    'footer' => '/local/modules/clients/interface/invoice_window.php',    
);
// Если физ лицо, добавляем квитанцию
\Bitrix\Main\Loader::includeModule('clients');
$client = new \Itin\Depohost\Clients;
$client->getUserId($ID);
$person_type_id = $client->getPersonTypeId();
if ($person_type_id == 1)
{
    $arParams['actions'][] = array(
        'ICON' => '',
        'TEXT' => 'Создать квитанцию',
        'TITLE' => 'Создать файл квитанции',
        'ACTION' => 'invoice_sb',
    );
}
showTableElement($arParams);
