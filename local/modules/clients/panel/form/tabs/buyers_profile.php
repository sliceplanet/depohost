<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");

global $APPLICATION;

IncludeModuleLangFile(__FILE__);

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if ($saleModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("BUYER_PE_ACCESS_DENIED"));

if(!CBXFeatures::IsFeatureEnabled('SaleAccounts'))
{
	require_once($DOCUMENT_ROOT."/bitrix/modules/main/include/prolog_admin_after.php");

	ShowError(GetMessage("SALE_FEATURE_NOT_ALLOW"));

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	die();
}

//ClearVars();

if (empty($PROFILE_ID))
{
    $PROFILE_ID = intval(array_key_exists('PROFILE_ID', $_REQUEST) ? $_REQUEST["PROFILE_ID"] : 0);
}

$USER_ID = 0;
$PERSON_TYPE = 0;
$profileName = '';
$arErrors = array();

if($arProfile = CSaleOrderUserProps::GetByID($PROFILE_ID))
{
	$USER_ID = intval($arProfile["USER_ID"]);
	$PERSON_TYPE = intval($arProfile["PERSON_TYPE_ID"]);
	$profileName = $arProfile["NAME"];
}
else
	$arErrors[] = GetMessage("BUYER_PE_NO_PROFILE");

/*****************************************************************************/
/***************************** FORM EDIT *************************************/
/*****************************************************************************/

if($USER_ID > 0)
{
	$dbUser = CUser::GetByID($USER_ID);
	if($arUser = $dbUser->Fetch())
	{
		$userFIO = $arUser["NAME"];
		if (strlen($arUser["LAST_NAME"]) > 0)
		{
			if (strlen($userFIO) > 0)
				$userFIO .= " ";
			$userFIO .= $arUser["LAST_NAME"];
		}
	}
	else
		$arErrors[] = GetMessage("BUYER_PE_NO_USER");
}
else
	$arErrors[] = GetMessage("BUYER_PE_NO_USER");


if (!empty($arErrors))
	CAdminMessage::ShowMessage(implode("<br>", $arErrors));


//TAB EDIT PROFILE
$arFilterProps = array();
$dbProperties = CSaleOrderProps::GetList(
    array("GROUP_SORT" => "ASC", "PROPS_GROUP_ID" => "ASC", "SORT" => "ASC", "NAME" => "ASC"),
    $arFilterProps,
    false,
    false,
    array("*")
);
$userProfile = CSaleOrderUserProps::DoLoadProfiles($USER_ID, $PERSON_TYPE);
$curVal = "";
$arProfileLinks = array('CODE_USER');
$propertyGroupID = "";
while ($arProperties = $dbProperties->Fetch())
{
    if (intval($arProperties["PROPS_GROUP_ID"]) != $propertyGroupID)
    {
        $arProfileLinks[] = "SECTION_".$arProperties["PROPS_GROUP_ID"];
    }

    $arProfileLinks[] = "CODE_".$arProperties["ID"];
    $propertyGroupID =  intval($arProperties["PROPS_GROUP_ID"]);
}

//$tabControl->AddFieldGroup("ELEMENTS_PROFILE", 'Данные о клиенте', $arProfileLinks);

if(!empty($arProfile) && !empty($arUser))
{
	$dbPersonType = CSalePersonType::GetList(array(), Array("ACTIVE" => "Y", "ID" => $PERSON_TYPE));
	$arPersonType = $dbPersonType->GetNext();
	$LID = $arPersonType["LID"];

	$arFilterProps = array("PERSON_TYPE_ID" => $PERSON_TYPE, "ACTIVE" => "Y", "USER_PROPS" => "Y", "UTIL" => "N");
        echo bitrix_sessid_post();
	$tabControl->AddViewField("CODE_USER", GetMessage("BUYER_PE_USER").":", "[<a href=\"/bitrix/admin/user_edit.php?ID=".$arUser["ID"]."&lang=".LANGUAGE_ID."\">".$arUser["ID"]."</a>] (".$arUser["LOGIN"].") ".$userFIO);
	//$tabControl->AddEditField("CODE_PROFILE_NAME", GetMessage("BUYER_PE_PROFILE_NAME").":", false, array("size"=>30, "maxlength"=>255), htmlspecialcharsEx($profileName));

	$propertyGroupID = "";
	$dbProperties = CSaleOrderProps::GetList(
			array("GROUP_SORT" => "ASC", "PROPS_GROUP_ID" => "ASC", "SORT" => "ASC", "NAME" => "ASC"),
			$arFilterProps,
			false,
			false,
			array("*")
	);
	$userProfile = CSaleOrderUserProps::DoLoadProfiles($USER_ID, $PERSON_TYPE);
	$curVal = "";
	while ($arProperties = $dbProperties->Fetch())
	{
		$arProperties["ID"] = intval($arProperties["ID"]);
		$curVal = $userProfile[$PROFILE_ID]["VALUES"][$arProperties["ID"]];
		$fieldValue = (($curVal!="") ? $curVal : $arProperties["DEFAULT_VALUE"]);
                $arDebug[] = $arProperties;
		if (intval($arProperties["PROPS_GROUP_ID"]) != $propertyGroupID)
			$tabControl->AddSection("SECTION_".$arProperties["PROPS_GROUP_ID"], $arProperties["GROUP_NAME"]);

		$shure = false;
		if ($arProperties["REQUIED"] == "Y" || $arProperties["IS_PROFILE_NAME"] == "Y" || $arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y" || $arProperties["IS_PAYER"] == "Y" || $arProperties["IS_ZIP"] == "Y")
			$shure = true;

		/*fields*/
		if ($arProperties["TYPE"] == "TEXT")
		{
            $size = !empty($arProperties['SIZE1']) ? $arProperties['SIZE1'] : 30;
			$tabControl->AddEditField("CODE_".$arProperties["ID"], $arProperties["NAME"].":", $shure, array("size"=>$size, "maxlength"=>255), $fieldValue);
		}
		elseif ($arProperties["TYPE"] == "CHECKBOX")
		{
			$checked = ($fieldValue == "Y") ? true : false;

			$tabControl->AddCheckBoxField("CODE_".$arProperties["ID"], $arProperties["NAME"].":", $shure, "Y", $checked);
		}
		elseif ($arProperties["TYPE"] == "SELECT")
		{
			$tabControl->BeginCustomField("CODE_".$arProperties["ID"], $arProperties["NAME"], $shure);
			?>
			<tr<? ($shure) ? " class=\"adm-detail-required-field\"" : "" ?>>
				<td width="40%">
					<?echo htmlspecialcharsbx($arProperties["NAME"]);?>:
				</td>
				<td width="60%">
					<select name="<?echo "CODE_".$arProperties["ID"];?>">
					<?
					$dbVariants = CSaleOrderPropsVariant::GetList(
						array("SORT" => "ASC"),
						array("ORDER_PROPS_ID" => $arProperties["ID"]),
						false,
						false,
						array("*")
					);
					while ($arVariants = $dbVariants->Fetch())
					{
						$selected = "";
						if ($arVariants["VALUE"] == $fieldValue)
							$selected .= " selected";
					?>
						<option <?echo $selected;?> value="<?echo htmlspecialcharsbx($arVariants["VALUE"]);?>"><?echo htmlspecialcharsbx($arVariants["NAME"]);?></option>
					<?
					}
					?>
					</select>
				</td>
			</tr>
			<?
			$tabControl->EndCustomField("CODE_".$arProperties["ID"]);
		}
		elseif ($arProperties["TYPE"] == "MULTISELECT")
		{
			$tabControl->BeginCustomField("CODE_".$arProperties["ID"], $arProperties["NAME"], $shure);
			?>
			<tr<? ($shure) ? " class=\"adm-detail-required-field\"" : "" ?>>
				<td width="40%">
					<?echo htmlspecialcharsbx($arProperties["NAME"]);?>:
				</td>
				<td width="60%">
					<select multiple size="5" name="<?echo "CODE_".$arProperties["ID"];?>[]">
					<?
					if (strlen($fieldValue) > 0)
					{
						$curVal = explode(",", $fieldValue);

						$arCurVal = array();
						$curValCount = count($curVal);
						for ($i = 0; $i < $curValCount; $i++)
							$arCurVal[$i] = trim($curVal[$i]);
					}

					$dbVariants = CSaleOrderPropsVariant::GetList(
						array("SORT" => "ASC"),
						array("ORDER_PROPS_ID" => intval($arProperties["ID"])),
						false,
						false,
						array("*")
					);
					while ($arVariants = $dbVariants->Fetch())
					{
						$selected = "";
						if (in_array($arVariants["VALUE"], $arCurVal))
							$selected .= " selected";
					?>
						<option <?echo $selected;?> value="<?echo htmlspecialcharsbx($arVariants["VALUE"]);?>"><?echo htmlspecialcharsbx($arVariants["NAME"]);?></option>
					<?
					}
					?>
					</select>
				</td>
			</tr>
			<?
			$tabControl->EndCustomField("CODE_".$arProperties["ID"]);
		}

		elseif ($arProperties["TYPE"] == "TEXTAREA"){
            $size1 = !empty($arProperties['SIZE1']) ? $arProperties['SIZE1'] : 30;
            $size2 = !empty($arProperties['SIZE2']) ? $arProperties['SIZE2'] : 5;
            $tabControl->AddTextField("CODE_".$arProperties["ID"],$arProperties["NAME"].":", $fieldValue, array("cols" => $size1, "rows" => $size2), $shure);
        }


		elseif ($arProperties["TYPE"] == "RADIO")
		{

			$tabControl->BeginCustomField("CODE_".$arProperties["ID"], $arProperties["NAME"], $shure);
			?>
			<tr<? ($shure) ? " class=\"adm-detail-required-field\"" : "" ?>>
				<td width="40%">
					<?echo htmlspecialcharsEx($arProperties["NAME"]);?>:
				</td>
				<td width="60%">
			<?
			$dbVariants = CSaleOrderPropsVariant::GetList(
					array("SORT" => "ASC"),
					array("ORDER_PROPS_ID" => $arProperties["ID"]),
					false,
					false,
					array("*")
			);
			while ($arVariants = $dbVariants->Fetch())
			{
				$selected = "";
				if ($arVariants["VALUE"] == $fieldValue)
					$selected .= " checked";
			?>
				<input <?echo $selected?> id="radio_<?echo $arVariants["ID"];?>" type="radio" name="CODE_<?echo $arProperties["ID"];?>" value="<?echo htmlspecialcharsex($arVariants["VALUE"]);?>" />
				<label for="radio_<?echo $arVariants["ID"];?>"><?echo htmlspecialcharsEx($arVariants["NAME"])?></label><br />
			<?
			}
			?>
				</td>
			</tr>
			<?
			$tabControl->EndCustomField("CODE_".$arProperties["ID"]);
		}
		elseif ($arProperties["TYPE"] == "LOCATION")
		{
			$tabControl->BeginCustomField("CODE_".$arProperties["ID"], $arProperties["NAME"], $shure);
		?>
			<tr<? ($shure) ? " class=\"adm-detail-required-field\"" : "" ?>>
				<td width="40%">
					<?echo htmlspecialcharsEx($arProperties["NAME"]);?>:
				</td>
				<td width="60%">
		<?
			$APPLICATION->IncludeComponent(
				'bitrix:sale.ajax.locations',
				'',
				array(
					"SITE_ID" => $LID,
					"AJAX_CALL" => "N",
					"COUNTRY_INPUT_NAME" => "COUNTRY_".$arProperties["ID"],
					"REGION_INPUT_NAME" => "REGION_".$arProperties["ID"],
					"CITY_INPUT_NAME" => "LOCATION_".$arProperties["ID"],
					"CITY_OUT_LOCATION" => "Y",
					"ALLOW_EMPTY_CITY" => "Y",
					"LOCATION_VALUE" => $fieldValue,
					"COUNTRY" => "",
					"ONCITYCHANGE" => "",
					"PUBLIC" => "N",
				),
				null,
				array('HIDE_ICONS' => 'Y')
			);
		?>
				</td>
			</tr>
		<?
			$tabControl->EndCustomField("CODE_".$arProperties["ID"]);
		}
	}
}
?>