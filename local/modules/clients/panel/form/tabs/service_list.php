<?php

use Bitrix\Main\Loader;
use Itin\Depohost\Clients;
use Itin\Depohost\Services;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);
Loader::includeModule("highloadblock"); 
Loader::includeModule('catalog');
Loader::includeModule('iblock');
Loader::includeModule('clients');

use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

global $APPLICATION;
$sTableID = 'service_table';
$ajaxLink = '/local/modules/clients/panel/form/tabs/service_list.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');
$table = new CAdminSubList($sTableID,false,array(
    'LINK'=>$ajaxLink,
    'PARAMS'=>array(
        'IBLOCK_ID'=>$_REQUEST['IBLOCK_ID'],
        'type'=>$_REQUEST['type'],
        'ID'=>$_REQUEST['ID'],
        'lang'=>'ru',
        'PROFILE_ID'=>$_REQUEST['PROFILE_ID'],
        'find_section_section'=>0,
        'tabControl_active_tab'=>'edit_services',
        'PROPERTY_CODE' => 'CC_SERVICE',
        'isAjax' => 'Y',
        )
    ));
//    $table = new \CAdminList($sTableID);

    $IBLOCK_ID = intval($_REQUEST['IBLOCK_ID']);
    $type = htmlspecialchars($_REQUEST['type']);
    $ID = intval($_REQUEST['ID']);
    $PROFILE_ID = intval($_REQUEST['PROFILE_ID']);
    $PROPERTY_CODE = htmlspecialchars($_REQUEST['PROPERTY_CODE']);

    $Client = new Clients;
    $ClientService = $Client->getHL('service');
    $arParams = array(
        'filter'=>array('UF_CLIENT'=>$ID)
    );
    if (!empty($_REQUEST['by']) && !empty($_REQUEST['order']))
    {
        $arParams['order'] = array($_REQUEST['by']=>$_REQUEST['order']);
    }
    else
    {
        $arParams['order'] = array('UF_NAME'=>'ASC');
    }
    $res = $ClientService->getList(
            $arParams
    );
    while ($ar = $res->fetch())
    {
        $arProp[$ar['ID']] = $ar;
        $arPropID[] =$ar['ID'];
    }

    // -------- ACTION-------------
    if ($_REQUEST['save']=='yes')
    {
		$isRealPaid = false;
        foreach ($_REQUEST['FIELDS'] as $id => $data)
        {
            $data['UF_ACTIVE'] = $data['UF_ACTIVE']=='Y' ? 1 : 0;
            $data['UF_BILL'] = $data['UF_BILL']=='Y' ? 1 : 0;
            $ClientService->update($id,$data);
        }
    }
    if ($_REQUEST['action']=='delete')
    {
        $arService = Services::getById(intval($_REQUEST['SUB_ID']));
        $ClientService->delete(intval($_REQUEST['SUB_ID']));
        Clients::recalcAbonplata($arService['UF_CLIENT']);
		
		//Удаляем связанную услугу из реестра
		$hlbl = 4;
		$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 

		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entity_data_class = $entity->getDataClass(); 

		$rsData = $entity_data_class::getList(array(
		   "select" => array("*"),
		   "order" => array("ID"=>"ASC"),
		   "filter" => array("UF_INVOICE"=>$_REQUEST['SUB_ID'])
		));

		while($arData = $rsData->Fetch()){
			$entity_data_class::Delete($arData['ID']);
		}
    }
    if (($arID = $table->GroupAction()))
    {

        if ($_REQUEST['action_target']=='selected')
        {
            $arFilter = array('ID'=>$arID);
            $rsData = CIBlockElement::GetList($arOrder, $arFilter);
            while($arRes = $rsData->Fetch())
                    $arID[] = $arRes['ID'];
        }
        foreach ($arID as $subID)
        {
            if (strlen($subID)<=0)
                    continue;

            $subID = intval($subID);
            $arRes = CIBlockElement::GetByID($subID);
            $arRes = $arRes->Fetch();
            if (!$arRes)
                    continue;

            $bPermissions = true;


            if (!$bPermissions)
            {
                    $table->AddGroupError(GetMessage("IBEL_A_UPDERR3")." (ID:".$subID.")", $subID);
                    continue;
            }

            switch($_REQUEST['action'])
            {
            case "add":
                $ob = new \CIBlockElement;
                $res = $ob->GetList(array(),array('ID'=>$subID),false,false,array('ID','IBLOCK_ID','IBLOCK_EXTERNAL_ID','NAME','CATALOG_GROUP_5'));
                if ($ar = $res->Fetch())
                {
                    $fields = array(
                        'UF_NAME_SHORT' => Services::getName($ar['IBLOCK_EXTERNAL_ID'],$ar),
                        'UF_PRICE' => (int) $ar['CATALOG_PRICE_5'],
                        'UF_QUANTITY' => 1,
                        'UF_SUMM' => (int) $ar['CATALOG_PRICE_5'],
                        'UF_CLIENT' => $ID,
                        'UF_BILL' => 1,
                        'UF_ACTIVE' => 0,
                        'UF_CATALOG_XML_ID' => $ar['IBLOCK_EXTERNAL_ID'],
                        'UF_PROPERTIES' => array(),
                    );
                    $fields['UF_PERIOD'] = $ar['IBLOCK_ID'] == 30 || $ar['IBLOCK_ID'] == 25 ? 'year' : 'month'; // ежегодная - если SSL-сертификат или домен
                    $fields['UF_NAME'] = Services::getNameString($fields['UF_CATALOG_XML_ID'], $fields);
                }
                $ClientService->add($fields);
                Services::sendToMailList(array($fields), array('client_id'=>$ID));   // отправляем письмо о добавлении услуги

                break;
            case "delete":
                $arService = Services::getById(intval($subID));
                $ClientService->delete($subID);
                Clients::recalcAbonplata($arService['UF_CLIENT']);
                    break;
            case "reboot":
                if($Client->rebootServer($_REQUEST['SUB_ID']))
                {
                    $arMessages[] = array('TYPE' => 'SUCCESS', 'MESSAGE' => GetMessage('REBOOT_SUCCESS'));
                }
                else
                {
                    $arMessages[] = array('TYPE' => 'ERROR', 'MESSAGE' => GetMessage('REBOOT_DOUBLE'));
                }
                break;
            case "ipkvm":
                if($Client->ipkvm($_REQUEST['SUB_ID']))
                {
                    $arMessages[] = array('TYPE' => 'SUCCESS', 'MESSAGE' => GetMessage('IPKVM_SUCCESS'));
                }
                else
                {
                    $arMessages[] = array('TYPE' => 'ERROR', 'MESSAGE' => GetMessage('IPKVM_DOUBLE'));
                }
                break;
            case "activate":
            case "deactivate":
                    if (CIBlockElementRights::UserHasRightTo($intSubIBlockID, $subID, "element_edit"))
                    {
                            $ob = new CIBlockElement();
                            $arFields = Array("ACTIVE"=>($_REQUEST['action']=="activate"?"Y":"N"));
                            if (!$ob->Update($subID, $arFields, true))
                                    $table->AddGroupError(GetMessage("IBEL_A_UPDERR").$ob->LAST_ERROR, $subID);
                    }
                    else
                    {
                            $table->AddGroupError(GetMessage("IBEL_A_UPDERR3")." (ID:".$subID.")", $subID);
                    }
                    break;
            }
	}
    }
    //------END ACTION
    //
    // --------HEADER
    $arHeader=array(
        array(
            "id" => "UF_NAME",
            "content" => "Наименование услуги",
            "sort" => "UF_NAME",
            "default" => true
        ),
        array(
            "id" => "UF_BILL",
            "content" => "Выставлять счет",
            "sort" => "UF_BILL",
            "default" => true
        ),
        array(
            "id" => "UF_ACTIVE",
            "content" => "Оплата",
            "sort" => "UF_ACTIVE",
            "default" => true
        ),
        array(
            "id" => "PROPERTIES",
            "content" => "Состав",
            "default" => true
        ),
        array(
            "id" => "UF_PRICE",
            "content" => "Цена услуги",
            "sort" => "UF_PRICE",
            "default" => true
        ),
        array(
            "id" => "UF_QUANTITY",
            "content" => "Количество",
            "sort" => "UF_QUANTITY",
            "default" => true
        ),
        array(
            "id" => "UF_SUMM",
            "content" => "Стоимость услуги",
            "sort" => "UF_SUMM",
            "default" => true
        ),
        array(
            "id" => "UF_IP_ADDR",
            "content" => "IP адрес",
            "default" => true
        ),
        array(
            "id" => "UF_SERVER_ID",
            "content" => "Номер сервера",
            "default" => true
        ),
        array(
            "id" => "UF_DATE_TO",
            "content" => "Дата окончания",
            "default" => true
        )
    );
    $table->AddHeaders($arHeader);
    // -----------END HEADERS
    //
    // ----------- ROWS

    $res = $ClientService->getList(
            $arParams
    );
    $isSuccess = false;
    while ($ar = $res->fetch())
    {
        $arEditHTML = array();
        $isSuccess = true;
        foreach ($ar as $code => $value)
        {
            if (is_array($value) || $code=='UF_IP_ADDR')
            {
              if (!is_array($value))
              {
                $value = empty($value) ? array() : array($value);
              }
                $arEditHTML[$code] = '';
                $ar[$code] = implode('<br>',$value);
                $c=0;
              if (!empty($value))
              {
                foreach ($value as $val)
                {
                  $c++;
                  $arEditHTML[$code] .='<input type="text" name="FIELDS['.$ar['ID'].']['.$code.']['.$c.']" value="'.$val.'"><br>';
                }
              }

              for($i=$c+1;$i<$c+4;$i++)
              {
                  $arEditHTML[$code] .='<input type="text" name="FIELDS['.$ar['ID'].']['.$code.']['.$i.']" value=""><br>';
              }
            }
          else
          {
            $arEditHTML[$code] .='<input type="text" name="FIELDS['.$ar['ID'].']['.$code.']" value="'.$value.'"><br>';
          }
            if ($code=='UF_PRICE' || $code=='UF_SUMM')
            {
                $arEditHTML[$code] = '<input type="text" name="FIELDS['.$ar['ID'].']['.$code.']" value="'.$value.'"><br>';

            }
            if ($code=='UF_DATE_TO')
            {
                //$arEditHTML[$code] = '<input type="text" name="FIELDS['.$ar['ID'].']['.$code.']" value="'.$value.'"><br>';
                $arEditHTML[$code] = '<div class="adm-input-wrap adm-input-wrap-calendar"><input class="adm-input adm-input-calendar" type="text" name="FIELDS['.$ar['ID'].']['.$code.']" size="23" value="'.$value.'"><span class="adm-calendar-icon" title="Нажмите для выбора даты" onclick="BX.calendar({node:this, field:\'FIELDS['.$ar['ID'].']['.$code.']\', form: \'\', bTime: false, bHideTime: false});"></span></div>';
            }

        }
        $ar['UF_BILL'] = intval($ar['UF_BILL'])==0 ? 'N' : 'Y';
        $ar['UF_ACTIVE'] = intval($ar['UF_ACTIVE'])==0 ? 'N' : 'Y'; // @hack Не стыковка булевого типа в HL и простых инфоблоках

        $arRes = array(
            'UF_NAME'=>$ar['UF_NAME'],
            'UF_BILL'=>$ar['UF_BILL'],
            'UF_ACTIVE'=>$ar['UF_ACTIVE'],
            'UF_IP_ADDR'=>$ar['UF_IP_ADDR'],
            'UF_SERVER_ID'=>$ar['UF_SERVER_ID'],
            'PROPERTIES'=> $ar['UF_PROPERTIES'],
            'UF_QUANTITY'=>$ar['UF_QUANTITY'],
            'UF_DATE_TO'=>$ar['UF_DATE_TO'],
            'UF_PRICE' => FormatCurrency($ar['UF_PRICE'],'RUB'),
            'UF_SUMM' => FormatCurrency($ar['UF_SUMM'],'RUB')
        );
        $HL_ID = 1;
        $row = $table->AddRow($ar['ID'], $arRes, '/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID='.$HL_ID.'&ID='.$ar['ID'].'&lang=ru');
        $row->AddViewField('UF_NAME',$ar['UF_NAME']);
        $row->AddEditField('UF_NAME',$arEditHTML['UF_NAME']);
        $row->AddCheckField('UF_ACTIVE');
        $row->AddCheckField('UF_BILL');
         $row->AddViewField('PROPERTIES',$ar['UF_PROPERTIES']);
        $row->AddEditField('PROPERTIES',$arEditHTML['UF_PROPERTIES']);
        $row->AddViewField('UF_IP_ADDR',$ar['UF_IP_ADDR']);
        $row->AddEditField('UF_IP_ADDR',$arEditHTML['UF_IP_ADDR']);
        $row->AddInputField('UF_QUANTITY',array('placeholder'=>$ar['UF_MEASURE']));
        $row->AddInputField('UF_SERVER_ID');
        $row->AddEditField('UF_DATE_TO',$arEditHTML['UF_DATE_TO']);
        $row->AddEditField('UF_PRICE',$arEditHTML['UF_PRICE']);

        $row->AddViewField('UF_SUMM',FormatCurrency($ar['UF_SUMM'],'RUB'));


        $arActions = array();

        if (!empty($ar['UF_SERVER_ID']) && !empty($ar['UF_IP_ADDR']))
        {
            $arActions[] = array(
                "ICON" => "",
                "TEXT" => 'Перезагрузка',
                "TITLE" => 'Запрос на перезагрузку сервера',
                "ACTION" => $table->ActionDoGroup($ar['ID'], "reboot"),
            );
            $arActions[] = array(
                "ICON" => "",
                "TEXT" => 'Подключение IP KVM',
                "TITLE" => 'Запрос на подключение IP KVM',
                "ACTION" => $table->ActionDoGroup($ar['ID'], "ipkvm"),
            );

        }
        if (!empty($arActions))
        {
            $arActions[] = array("SEPARATOR"=>true);
        }
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => 'Удалить',
            "TITLE" => GetMessage("IBLOCK_DELETE_ALT"),
            "ACTION" => "if(confirm('Вы действительно хотите удалить?')) ".$table->ActionDoGroup($ar['ID'], "delete"),
        );
        $row->AddActions($arActions);
    }
    if ($isSuccess===false)
    {
        $arRes=array();
    }
    $table->AddFooter(
        array(
                array("counter"=>true, "title"=>"footer", "value"=>"0"),
        )
    );
    $table->BeginPrologContent();
        // show messages
        if (!empty($arMessages))
        {
            foreach ($arMessages as $arMessage)
            {
                if ($arMessage['TYPE'] == 'ERROR')
                {
                    CAdminMessage::ShowMessage($arMessage['MESSAGE']);
                }
                else
                {
                    CAdminMessage::ShowNote($arMessage['MESSAGE']);
                }
            }
        }
        // END show messages
        // Абонентская плата

            if (empty($PROP['CC_ABONPLATA']))
            {
                $ob = new CIBlockElement;
                $res = $ob->GetProperty($IBLOCK_ID, $ID, array(),array('CODE'=>'CC_ABONPLATA'));
                if ($ar = $res->Fetch())
                {
                    $prop_fields = $ar;
                }
            }
            else
            {
                $prop_fields = $PROP['CC_ABONPLATA'];
            }
            $abonplata_price = is_array($prop_fields["VALUE"]) ? current($prop_fields["VALUE"]) : $prop_fields["VALUE"];
            $abonplata_price_format = FormatCurrency($abonplata_price,'RUB');
            ?>
            <div style="font-size: 14px; margin-bottom: 15px;" align="right"><?=$prop_fields['NAME']?>: <b><?=$abonplata_price_format?></b></div>
            <input type="hidden" name="PROP[<?=$prop_fields['ID']?>]" value="<?=$abonplata_price?>">
            <?php
        // END Абонентская плата

       $name = 'PROP[181]';
       ?>
        <input type="hidden" onchange="<?= str_replace("this.value","'+this.value+'",$table->ActionDoGroup('this.value', "add"))?>" name="<?=$name?>" id="<?=$name?>" value="" >
    <?php
    $table->EndPrologContent();
    CUtil::InitJSCore(array('window','ajax'));
    $aContext = array();
    $aContext[] = array(
        'TEXT' => 'Добавить услугу',
        'TITLE' => 'Добавить услугу',
        'ICON' => 'btn_new',
        'ONCLICK' => 'jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=ru&type=xmlcatalog&IBLOCK_ID=21&n='.$name.'\', 900, 500);'
    );
    if (!empty($aContext))
    {
        $table->AddAdminContextMenu($aContext,false,false);
    }
    $abonplata_price = is_array($PROP['CC_ABONPLATA']["VALUE"]) ? current($PROP['CC_ABONPLATA']["VALUE"]) : $PROP['CC_ABONPLATA']["VALUE"];
    $abonplata_price = FormatCurrency($abonplata_price,'RUB');

    $aFooter = array();
    $aFooter[] = array(
        'title' => $PROP['CC_ABONPLATA']['NAME'],
        'value' => $abonplata_price
    );
    $aFooter[] = array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0");
    $table->AddFooter($aFooter);
    $table->BeginEpilogContent();

    $table->EndEpilogContent();
    $table->CheckListMode();
    $table->DisplayList();
    // -------END ROWS
    ?>


