<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/clients/panel/form/admin_tools.php");

$arParams = array(
    'table_id' => 'tbl_acts_list',
    'params' => array(
        'ID' => !empty($ID) ? $ID : $_REQUEST['ID'],
        'IBLOCK_ID' => !empty($IBLOCK_ID) ? $IBLOCK_ID : $_REQUEST['IBLOCK_ID'],
        'PROFILE_ID' => !empty($PROFILE_ID) ? $PROFILE_ID : $_REQUEST['PROFILE_ID'],
        'PROPERTY_CODE' => !empty($ar_fields['CODE']) ? $ar_fields['CODE'] : $_REQUEST['PROPERTY_CODE'],
        'PROPERTY_ID' => !empty($ar_fields['ID']) ? $ar_fields['ID'] : $_REQUEST['PROPERTY_ID'],
        'isAjax' => 'Y',
        'WF' => 'Y',        
    ),
    'link' => '/local/modules/clients/panel/form/tabs/acts.php',
//    'link' => '/bitrix/admin/client_edit.php',
    'ajax' => $_REQUEST['isAjax'] == 'Y' ? 'Y' : 'N',
    'map' => array(
        'PROPERTY_NUMBER' => 'Номер акта',
        'PROPERTY_DATE' => 'Дата акта', 
        'PROPERTY_SERVICES' => 'Перечень услуг',
        'PROPERTY_SUMM' => 'Сумма',
        'PROPERTY_DOCUMENT' => 'Файл', 
    ),
    'edit_fields' => array(
        'PROPERTY_DATE','PROPERTY_SUMM','PROPERTY_NUMBER'
    ),
    'actions' => array(
        array(
            'ICON' => '',
            'TEXT' => 'Отправить',
            'TITLE' => 'Отправить клиенту',
            'ACTION' => 'send_acts',
        )
    ),
    'context_menu' => array(
//         array(
//            'ICON' => '',
//            'TEXT' => 'Выбрать счет',
//            'TITLE' => 'Выбрать счет из созданных ранее',
//            'ACTION' => 'change',
//        ),
        array(
            'ICON' => 'btn_new',
            'TEXT' => 'Создать акт',
            'TITLE' => 'Создать новый акт',
            'ACTION' => 'add',
        )
    ),
    'footer' => '/local/modules/clients/interface/acts_window.php',
    
);
showTableElement($arParams);
