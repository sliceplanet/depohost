<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
/*
 * Выводит элементы в табличном виде
 *
 * @params array $arParams
 * @var string $arParams['table_id'] - Индефикатор таблицы
 * @var array $arParams['edit_fields'] - массив редактируемых полей
 * @var array $arParams['map'] - массив заголовков
 *      для примера, array('ID'=>'ИД','NAME'=>'Название элемента')
 * @var array $arParams['params'] - массив параметров
 *
 * @return bool
 */
function showTableElement($arParams)
{
    global $APPLICATION;
    IncludeModuleLangFile(__FILE__);
    \Bitrix\Main\Loader::includeModule('catalog');
    \Bitrix\Main\Loader::includeModule('iblock');

    if ($arParams['ajax'] == 'Y' || $_REQUEST['isAjax']=='Y')
    {
        $APPLICATION->RestartBuffer();
    }
    // --- VARIABLES
    $sTableID = $arParams['table_id'];
    $arEditFields = $arParams['edit_fields'];
    $ID = intval($arParams['params']['ID']);
    $PROPERTY_CODE = $arParams['params']['PROPERTY_CODE'];
    $PROPERTY_ID = $arParams['params']['PROPERTY_ID'];
    $BUFF = 'BUFF_'.$PROPERTY_ID;

    $ob = new \CIBlockProperty;
    $res = $ob->GetByID($PROPERTY_ID,$arParams['params']['IBLOCK_ID']);
    if ($ar = $res->Fetch())
    {
        $LINK_IBLOCK_ID = $ar['LINK_IBLOCK_ID'];
        $PROPERTY_ID = $ar['ID'];
    }
    // --- END VARIABLES


    $ob_el = new \CIBlockElement;
    if (empty($arParams['params']['IBLOCK_ID']))
    {
        $res = $ob_el->GetList(array(),array('ID'=>$ID),false,false,array('IBLOCK_ID','IBLOCK_TYPE_ID'));
        $ar_element = $res->Fetch();
        $arParams['params']['IBLOCK_ID'] = intval($ar_element['IBLOCK_ID']);
    }

    $arParams['params']['lang'] = 'ru';
    $arParams['params']['find_section_section'] = 0;

    $res = CIBlockElement::GetProperty($arParams['params']['IBLOCK_ID'],$ID,array('sort'=>'asc'),array('CODE'=>$PROPERTY_CODE,'EMPTY'=>'N'));
    $ar_property_value = array();
    while ($ar = $res->Fetch())
    {
        $ar_property_value[] = $ar['VALUE'];
    }
    $ar_property_value = array_unique($ar_property_value);
    $ajaxLink = $arParams['link'];
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');
    $table = new \CAdminSubList($sTableID,false,
        array(
            'LINK'=>$ajaxLink,
            'PARAMS'=>$arParams['params']
        )
    );

    $table->BeginPrologContent();
        $footer_file = $_SERVER['DOCUMENT_ROOT'].$arParams['footer'];
        if (file_exists($footer_file))
        {
           include $footer_file;

        }
    $table->EndPrologContent();
    $table->BeginEpilogContent();
    echo '<input type="hidden" id="'.$BUFF.'" name="'.$BUFF.'" value="" onchange="'.$table->ActionDoGroup($ID, "reload").'">';
    $table->EndEpilogContent();
    // --- HANDLER ACTION

    switch ($_REQUEST['action'])
    {
        case 'delete':


            $SubID = intval($_REQUEST['SUB_ID']);
            if (CIBlockElementRights::UserHasRightTo($LINK_IBLOCK_ID, $SubID, "section_element_bind")){
                $key = array_search($SubID, $ar_property_value);
                if (!empty($key))
                {
                    unset($ar_property_value[$key]);
                }
                $ob_el->Delete($SubID);
                $ob_el->SetPropertyValuesEx($ID,$arParams['params']['IBLOCK_ID'],array($PROPERTY_CODE=>$ar_property_value));
            }
            break;

        case 'add':
            $SubID = intval($_REQUEST['SUB_ID']);
            $ar_property_value = array_unique($ar_property_value);
            $ar_property_value[] = $SubID;
            $ob_el->SetPropertyValuesEx($ID,$arParams['params']['IBLOCK_ID'],array($PROPERTY_CODE=>$ar_property_value));

           break;
        case 'invoice_sb':
            $SubID = intval($_REQUEST['SUB_ID']);
            \Bitrix\Main\Loader::includeModule('clients');
            $invoice = new \Itin\Depohost\Invoices($SubID);
            $invoice->generateSbPdf();

           break;
        case 'create_act':
            $SubID = intval($_REQUEST['SUB_ID']);
            $ob = new \CIBlockProperty;
            $res = $ob->GetByID('CC_ACTS',$arParams['params']['IBLOCK_ID']);
            if ($ar = $res->Fetch())
            {
                $ACTS_LINK_IBLOCK_ID = $ar['LINK_IBLOCK_ID'];
                $ACTS_PROPERTY_ID = $ar['ID'];
            }
            ?>
            <script>
                var popupAdd_<?=$arParams['table_id']?>_<?=$SubID?> = function(){
                return new BX.CAdminDialog({
                        'title': 'Добавление акта',
                        'content_url': '/local/modules/clients/admin/ajax/add_act.php',
                        'content_post': {
                            'LINK_IBLOCK_ID':<?=$ACTS_LINK_IBLOCK_ID?>,
                            'IBLOCK_ID':<?=$_REQUEST['IBLOCK_ID']?>,
                            'PROPERTY_ID':<?=$ACTS_PROPERTY_ID?>,
                            'ID':<?=$ID?>,
                            'return_url':'<?=$GLOBALS['APPLICATION']->GetCurPageParam('tabControl_active_tab=edit_acts',array('tabControl_active_tab'))?>',
                            'isAjax':'Y'
                            <?=!empty($SubID) ? ",'invoice_id':".$SubID : ""?>
                        },
                        'draggable': true,
                        'resizable': true
                    });
                }
                popupAdd_<?=$arParams['table_id']?>_<?=$SubID?>().Show();
            </script>
            <?php
            break;
        case 'send_invoice':
            $SubID = intval($_REQUEST['SUB_ID']);
            \Bitrix\Main\Loader::includeModule('clients');
            \Itin\Depohost\Invoices::sendToMail($SubID);
           break;
        case 'send_act':
            $SubID = intval($_REQUEST['SUB_ID']);
            \Bitrix\Main\Loader::includeModule('clients');
            \Itin\Depohost\Acts::sendToMail($SubID);
           break;
        case 'reload':
           break;
    }
    if ($table->EditAction())
	{

            if (is_array($_FILES['FIELDS']))
                    CAllFile::ConvertFilesToPost($_FILES['FIELDS'], $_POST['FIELDS']);
            if (is_array($FIELDS_del))
                    CAllFile::ConvertFilesToPost($FIELDS_del, $_POST['FIELDS'], "del");

			//проверяем прошлый статус счета, чтоб принять решение
			//понятия не имею зачем SUB_ID в массиве, и вообще зачем FIELDSы тоже в массивах разместили, работаю пока с этим
			//также хреново, что нет старого в формате ID, с печальным набором данных работаю
			//Не оплачен - 277, Оплачен - 278
			if($_REQUEST['FIELDS'][$_REQUEST['SUB_ID'][0]]['PROPERTY_STATUS'] == 278 && $_REQUEST['FIELDS_OLD'][$_REQUEST['SUB_ID'][0]]['PROPERTY_STATUS'] == 'Не оплачен') {
				$isRealPaid = 'paid';
			} elseif($_REQUEST['FIELDS'][$_REQUEST['SUB_ID'][0]]['PROPERTY_STATUS'] == 'Оплачен' && $_REQUEST['FIELDS_OLD'][$_REQUEST['SUB_ID'][0]]['PROPERTY_STATUS'] == 277) {
				$isRealPaid = 'unpaid';
			}
			
			//отправляем запрос на создание ssl сертификата
			if(!empty($_REQUEST['SUB_ID'][0]) && !empty($_REQUEST['ID']) && $isRealPaid == 'paid'):
				$invoice_ob = new \Itin\Depohost\Invoices($_REQUEST['SUB_ID'][0]);
				$invoice_ob->setClientCardId($_REQUEST['ID']);
				$invoice_ob->paid();
				print_r($a);
			elseif(!empty($_REQUEST['SUB_ID'][0]) && !empty($_REQUEST['ID']) && $isRealPaid == 'unpaid'):
				$invoice_ob = new \Itin\Depohost\Invoices($_REQUEST['SUB_ID'][0]);
				$invoice_ob->setClientCardId($_REQUEST['ID']);
				$invoice_ob->unPaid();
			endif;

            $el = new \CIBlockElement;

            foreach ($_REQUEST['FIELDS'] as $id => $arValues)
            {
                $arFields = array();
                $arPropertires = array();
                // Отделим поля от свойств
                foreach ($arValues as $code => $value)
                {
                    if (!is_array($value) && (isset($_REQUEST['ajax']) || isset($_REQUEST['isAjax'])))
                    {
                        $value = iconv('UTF-8', 'WINDOWS-1251', $value);
                    }

                    if (strpos($code, 'PROPERTY_')===false)
                    {
                        $arFields[$code] = $value;
                    }
                    else
                    {
                        $code = str_replace('PROPERTY_', '', $code);
                        $arPropertires[$code] = $value;
                    }
                }

                if (!empty($arFields))
                {
                    $el->Update($id,$arFields);
                }
                if (!empty($arPropertires))
                {
                    $el->SetPropertyValuesEx($id,$LINK_IBLOCK_ID,$arPropertires);
                }
            }


        }
    if (($arIDs = $table->GroupAction()))
    {

    }
    // --- END HANDLER ACTION

    // --- HEADER
    $arHeader = array();
    foreach ($arParams['map'] as $code => $name)
    {
        // формируем заголовки
        $arHeader[] = array(
            "id" => $code,
            "content" => $name,
            "sort" => $code,
            "default" => true
        );

    }
    $table->AddHeaders($arHeader);
    // --- END HEADER


    if (!empty($ar_property_value) && $LINK_IBLOCK_ID>0)
    {
        foreach ($arParams['map'] as $code => $name)
        {
           // запишем типы свойств, дальше пригодятся при формировании типов полей
            if (strpos($code, 'PROPERTY_')!==false){
                $id = str_replace('PROPERTY_', '', $code);
                $rsProperty = $ob->GetByID($id,$LINK_IBLOCK_ID);
                if ($arProperty = $rsProperty->Fetch())
                {
                    $propertyInfo[$code] = $arProperty;
                }
            }
        }
        $arSelect = array_keys($arParams['map']);
        $arSelect[] = 'CODE';
        $arSelect[] = 'ID';
        $ob_el = new \CIBlockElement;
        $res = $ob_el->GetList(
            array('DATE_ACTIVE_FROM'=>'DESC','SORT'=>'ASC'),
            array('ID'=>$ar_property_value),
            false,
            false,
            $arSelect
        );
        $i = 0;
        $ar_prop_values_temp = array();
        while($ar = $res->Fetch())
        {
            if (!empty($ar_prop_values_temp[$ar['ID']]))
            {
                foreach ($ar_prop_values_temp[$ar['ID']] as $key => $value)
                {
                    if (is_array($value))
                    {
                        array_push($ar_prop_values_temp[$ar['ID']][$key], $ar[$key]);
                    }
                    else
                    {
                        $ar_prop_values_temp[$ar['ID']][$key] = array($value, $ar[$key]);
                    }
                }
            }
            else
            {
                 $ar_prop_values_temp[$ar['ID']] = $ar;
            }

        }
        if (!function_exists('uniq_func'))
        {
            function uniq_func($arValue)
            {
                foreach ($arValue as $key => $value)
                {
                    if (is_array($value))
                    {
                        $arValue[$key] = array_unique($value);
                        if (count($arValue[$key])==1)
                        {
                            $arValue[$key] = current($arValue[$key]);
                        }
                    }
                }

                return $arValue;
            }
        }
        $ar_prop_values = array_map('uniq_func', $ar_prop_values_temp);
        foreach ($ar_prop_values as $ar_prop_value)
        {

            $arRes = array();
            foreach ($ar_prop_value as $code => $value)
            {
                if (strpos($code, 'PROPERTY_')!==false){
                    $code = str_replace('_VALUE', '', $code);
                }
                if (array_key_exists($code, $arParams['map']))
                {
                    $arRes[$code] = $value;
                }
            }
    // --- ADD ROW
            $row = $table->AddRow($ar_prop_value['ID'], $arRes, '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$LINK_IBLOCK_ID.'&type=personal&ID='.$ar_prop_value['ID'].'&lang=ru&find_section_section=0');
            echo '<input type="hidden" name="PROP['.$PROPERTY_ID.']['.$i.']" value="'.$ar_prop_value['ID'].'">';
            $i++;
            foreach($arRes as $key => $result)
            {
                if ($propertyInfo[$key]['USER_TYPE']== 'directory')
                {
                    $table_name = $propertyInfo[$key]['USER_TYPE_SETTINGS']['TABLE_NAME'];
                    \Bitrix\Main\Loader::includeModule('highloadblock');
                    $hldata = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
                        'select'=>array('*'),
                        'filter'=>array('TABLE_NAME'=>$table_name),
                        'limit'=>1)
                    )->fetch();
                    $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
                    $hlDataClass = $hldata['NAME'].'Table';

                    $hl_ob = new $hlDataClass;
                    $res = $hl_ob->getList(array(
                        'select'=>array('*'),
                        'filter' => array('=ID'=>$result)
                    ));
                    $result = array();
                    while($ar = $res->fetch())
                    {
                        $result[] = $ar['UF_NAME'];
                    }
                }
                $result_html = is_array($result)? implode('<br>', $result) : $result;

                if (in_array($key, $arParams['edit_fields']))
                {
                    if ($propertyInfo[$key]['PROPERTY_TYPE']=='F')
                    {
                        $row->AddFileField($key,$result);
                    }
                    elseif ($propertyInfo[$key]['USER_TYPE']=='DateTime')
                    {
                        $row->AddCalendarField($key);
                    }
                    elseif ($propertyInfo[$key]['PROPERTY_TYPE']=='L')
                    {
                        $res = \CIBlockPropertyEnum::GetList(array('SORT'=>'ASC'),array('PROPERTY_ID'=>$propertyInfo[$key]['ID']));
                        $arValues = array();
                        while ($ar = $res->Fetch())
                        {
                            $arValues[$ar['ID']] = $ar['VALUE'];
                        }
                        $row->AddSelectField($key,$arValues);
                        $row->AddViewField($key,$result);
                    }
                    else
                    {
                        $arAttr = array();
                        if ($propertyInfo[$key]['CODE']=='DESCRIPTION')
                        {
                            // Для назначения платежа побольше поле
                            $arAttr = array('size'=>100);
                        }
                        $row->AddInputField($key,$arAttr);

                        if ($propertyInfo[$key]['CODE']=='SUMM')
                        {
                            // Вывожу форматированное сумму
                            $row->AddViewField($key,FormatCurrency($result,'RUB'));
                        }
                    }
                }
                else
                {
                    if ($propertyInfo[$key]['PROPERTY_TYPE']=='F')
                    {
                        $arFile = \CFile::GetFileArray($result);
                        $result_html = '<b><a href="'.$arFile['SRC'].'" download="'.$arFile['FILE_NAME'].'" target="_blank">'.$arFile['FILE_NAME'].'</a></b>';

                        $row->AddViewField($key,$result_html);
                    }
                    else
                    {
                        $row->AddViewField($key,$result_html);
                    }

                }

    // --- END ADD ROW
    // --- ADD ACTIONS
                $arActions = !empty($arParams['actions']) ? $arParams['actions'] : array();
                foreach ($arActions as $key => $action)
                {
                    $arActions[$key]['ACTION'] =  $table->ActionDoGroup($ar_prop_value['ID'], $action['ACTION']);
                }

                if (CIBlockElementRights::UserHasRightTo($LINK_IBLOCK_ID, $ar_prop_value['ID'], "element_delete")){
                    if (!empty($arActions))
                    {
                        $arActions[] = array("SEPARATOR"=>true);
                    }
                    $arActions[] = array(
                        "ICON" => "delete",
                        "TEXT" => 'Удалить',
                        "TITLE" => GetMessage("IBLOCK_DELETE_ALT"),
                        "ACTION" => "if(confirm('Вы действительно хотите удалить?')) ".$table->ActionDoGroup($ar_prop_value['ID'], "delete"),
                    );
                }

                $row->AddActions($arActions);
        // --- END ADD ACTIONS
            }
        }

    }
    else
    {
        $arRes = array();
    }
    $aContext = array();
    foreach ($arParams['context_menu'] as $arMenu)
    {
        switch ($arMenu['ACTION'])
        {
            case 'add':
                $arMenu['ONCLICK'] = 'popupAdd_'.$arParams['table_id'].'().Show();';
                break;
             case 'change':
                $arMenu['ONCLICK'] = 'jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=ru&IBLOCK_ID='.$LINK_IBLOCK_ID.'&n='.$BUFF.'\', 900, 500);';
                break;
            default:
                break;
        }
        unset($arMenu['ACTION']);
        $aContext[] = $arMenu;
    }
    if (!empty($aContext))
    {
        $table->AddAdminContextMenu($aContext,false,false);
    }

    $table->AddFooter(
        array(
                array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
        )
    );
    $table->CheckListMode();
    $table->DisplayList();
    if (!empty($arParams['init_js']))
    {

    }

}

