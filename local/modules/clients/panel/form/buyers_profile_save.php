<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");

global $APPLICATION;

IncludeModuleLangFile(__FILE__);

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if ($saleModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("BUYER_PE_ACCESS_DENIED"));

if(!CBXFeatures::IsFeatureEnabled('SaleAccounts'))
{
	require_once($DOCUMENT_ROOT."/bitrix/modules/main/include/prolog_admin_after.php");

	ShowError(GetMessage("SALE_FEATURE_NOT_ALLOW"));

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	die();
}

//ClearVars();
if (!empty($_REQUEST['PROFILE_ID']))
{
    $PROFILE_ID = intval($_REQUEST['PROFILE_ID']);
}
elseif(!empty($_REQUEST['USER_ID']))
{
    $PROFILE_ID = $clients->getProfileId(intval($_REQUEST['USER_ID']));
}
//$user_id = array_pop($PROP['CC_CLIENT']['VALUE']);
//$clients = new \Itin\Depohost\Clients;
//


if ($PROFILE_ID>0)
{
    $USER_ID = 0;
    $PERSON_TYPE = 0;
    $profileName = '';
    $arErrors = array();
    if($arProfile = CSaleOrderUserProps::GetByID($PROFILE_ID))
    {
            $USER_ID = intval($arProfile["USER_ID"]);
            $PERSON_TYPE = intval($arProfile["PERSON_TYPE_ID"]);
            $profileName = $arProfile["NAME"];
    }
    else
            $arErrors[] = GetMessage("BUYER_PE_NO_PROFILE");

    /*****************************************************************************/
    /**************************** SAVE PROFILE ***********************************/
    /*****************************************************************************/
    if ($REQUEST_METHOD == "POST" && $saleModulePermissions >= "U" && check_bitrix_sessid() && !empty($arProfile))
    {


            $CODE_PROFILE_NAME = trim($_REQUEST["CODE_PROFILE_NAME"]);
            if (strlen($CODE_PROFILE_NAME) > 0)
                    $profileName = $CODE_PROFILE_NAME;

            $arOrderPropsValues = array();
            $dbProperties = CSaleOrderProps::GetList(
                            array("GROUP_SORT" => "ASC", "PROPS_GROUP_ID" => "ASC", "SORT" => "ASC", "NAME" => "ASC"),
                            array("PERSON_TYPE_ID" => $PERSON_TYPE, "ACTIVE" => "Y", "USER_PROPS" => "Y", "UTIL" => "N"),
                            false,
                            false,
                            array("*")
            );
            while ($arOrderProps = $dbProperties->Fetch())
            {
                    $arOrderProps["ID"] = intval($arOrderProps["ID"]);
                    $curVal = trim($_REQUEST["CODE_".$arOrderProps["ID"]]);

                    if ($arOrderProps["TYPE"]=="LOCATION")
                    {
                            $curVal = trim($_REQUEST["LOCATION_".$arOrderProps["ID"]]);
                    }

                    if ($arOrderProps["TYPE"] == "MULTISELECT")
                    {
                            $curVal = "";
                            if (is_array($_REQUEST["CODE_".$arOrderProps["ID"]]))
                            {
                                    foreach ($_REQUEST["CODE_".$arOrderProps["ID"]] as $key => $val)
                                    {
                                            $curVal .= trim($val);
                                            if ($key < (count($_REQUEST["CODE_".$arOrderProps["ID"]]) - 1))
                                                    $curVal .= ",";
                                    }
                            }
                    }

                    if (
                            ($arOrderProps["IS_LOCATION"]=="Y" || $arOrderProps["IS_LOCATION4TAX"]=="Y")
                            && intval($curVal) <= 0
                            ||
                            ($arOrderProps["IS_ZIP"] == "Y" && strlen($curVal) <= 0)
                            ||
                            ($arOrderProps["IS_PROFILE_NAME"]=="Y" || $arOrderProps["IS_PAYER"]=="Y")
                            && strlen($curVal) <= 0
                            ||
                            $arOrderProps["REQUIED"]=="Y"
                            && $arOrderProps["TYPE"]=="LOCATION"
                            && intval($curVal) <= 0
                            ||
                            $arOrderProps["REQUIED"]=="Y"
                            && ($arOrderProps["TYPE"]=="TEXT" || $arOrderProps["TYPE"]=="TEXTAREA" || $arOrderProps["TYPE"]=="RADIO" || $arOrderProps["TYPE"]=="SELECT")
                            && strlen($curVal) <= 0
                            ||
                            ($arOrderProps["REQUIED"]=="Y"
                            && $arOrderProps["TYPE"]=="MULTISELECT"
                            && strlen($curVal) <= 0)
                            )
                    {
                            $arErrors[] = str_replace("#NAME#", $arOrderProps["NAME"], GetMessage("BUYER_PE_EMPTY_PROPS"));
                    }
                    $arProfile[$arOrderProps["ID"]] = $arOrderProps;
                    if ($arOrderProps['CODE'] == 'EMAIL')
                    {
                        $new_email = $curVal;
                    }
                    $arProfile[$arOrderProps["ID"]]['VALUE'] = $curVal;
                    $arOrderPropsValues[$arOrderProps["ID"]] = $curVal;
            }

            if (count($arErrors) <= 0)
            {
                CSaleOrderUserProps::DoSaveUserProfile($USER_ID, $PROFILE_ID, $profileName, $PERSON_TYPE, $arOrderPropsValues, $arErrors);
                $user = new CUser;
                $user->Update($USER_ID, array('EMAIL'=>$new_email));
            }


    }
}


