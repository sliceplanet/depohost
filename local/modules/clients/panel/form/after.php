<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
//use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/local/modules/clients/iblock/admin/iblock_element_edit.php');
\Bitrix\Main\Loader::includeModule('clients');
//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings

$tabControl->BeginPrologContent();


echo CAdminCalendar::ShowScript();

if (COption::GetOptionString("iblock", "use_htmledit", "Y") == "Y" && $bFileman)
{
  //TODO:This dirty hack will be replaced by special method like calendar do
  echo '<div style="display:none">';
  CFileMan::AddHTMLEditorFrame(
    "SOME_TEXT",
    "",
    "SOME_TEXT_TYPE",
    "text",
    array(
      'height' => 450,
      'width' => '100%'
    ),
    "N",
    0,
    "",
    "",
    $arIBlock["LID"]
  );
  echo '</div>';
}

if ($arTranslit["TRANSLITERATION"] == "Y")
{
  CJSCore::Init(array('translit'));
  ?>
  <script type="text/javascript">
    var linked =<?if ($bLinked)
    {
      echo 'true';
    }
    else
    {
      echo 'false';
    }?>;

    function set_linked() {
      linked = !linked;

      var name_link = document.getElementById('name_link');
      if (name_link) {
        if (linked)
          name_link.src = '/bitrix/themes/.default/icons/iblock/link.gif';
        else
          name_link.src = '/bitrix/themes/.default/icons/iblock/unlink.gif';
      }
      var code_link = document.getElementById('code_link');
      if (code_link) {
        if (linked)
          code_link.src = '/bitrix/themes/.default/icons/iblock/link.gif';
        else
          code_link.src = '/bitrix/themes/.default/icons/iblock/unlink.gif';
      }
      var linked_state = document.getElementById('linked_state');
      if (linked_state) {
        if (linked)
          linked_state.value = 'Y';
        else
          linked_state.value = 'N';
      }
    }

    var oldValue = '';

    function transliterate() {
      if (linked) {
        var from = document.getElementById('NAME');
        var to = document.getElementById('CODE');
        if (from && to && oldValue != from.value) {
          BX.translit(from.value, {
            'max_len': <?echo intval($arTranslit['TRANS_LEN'])?>,
            'change_case': '<?echo $arTranslit['TRANS_CASE']?>',
            'replace_space': '<?echo $arTranslit['TRANS_SPACE']?>',
            'replace_other': '<?echo $arTranslit['TRANS_OTHER']?>',
            'delete_repeat_replace': <?echo $arTranslit['TRANS_EAT'] == 'Y' ? 'true' : 'false'?>,
            'use_google': <?echo $arTranslit['USE_GOOGLE'] == 'Y' ? 'true' : 'false'?>,
            'callback': function (result) {
              to.value = result;
              setTimeout('transliterate()', 250);
            }
          });
          oldValue = from.value;
        }
        else {
          setTimeout('transliterate()', 250);
        }
      }
      else {
        setTimeout('transliterate()', 250);
      }
    }

    transliterate();
  </script>
  <?
}
?>
  <script type="text/javascript">
    var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
      '<?echo $tabControl->GetName()?>_form',
      '/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?echo intval($IBLOCK_ID)?>&ENTITY_ID=<?echo intval($ID)?>'
    );
    BX.ready(function () {
      setTimeout(function () {
        InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
      }, 1000);
    });
  </script>
  <div id="adm-message"></div>
<?
$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();
?>
<?= bitrix_sessid_post() ?>
<? echo GetFilterHiddens("find_"); ?>

  <input type="hidden" name="linked_state" id="linked_state" value="<? if ($bLinked)
  {
    echo 'Y';
  }
  else
  {
    echo 'N';
  } ?>">
  <input type="hidden" name="Update" value="Y">
  <input type="hidden" name="from" value="<? echo htmlspecialcharsbx($from) ?>">
  <input type="hidden" name="WF" value="<? echo htmlspecialcharsbx($WF) ?>">
  <input type="hidden" name="return_url" value="<? echo htmlspecialcharsbx($return_url) ?>">
<?php
// выводим скрытые свойства, которые не выводятся
$ar_fields = $PROP['TYPE_PAYMENT'];
$arHidden[] = array('NAME' => 'PROP[' . $ar_fields['ID'] . ']', 'VALUE' => current($ar_fields['VALUE']));
$arHidden[] = array('NAME' => 'ACTIVE', 'VALUE' => $str_ACTIVE);

foreach ($arHidden as $value)
{
  echo '<input type="hidden" name="' . $value['NAME'] . '" value="' . $value['VALUE'] . '" />';
}
$USER_ID = array_slice($PROP['CC_CLIENT']['VALUE'], 0, 1);
$USER_ID = $USER_ID[0];
$rsUser = CUser::GetById($USER_ID);
$arUser = $rsUser->Fetch();
$propComment = $PROP['CC_COMMENT'];

echo '<input type="hidden" name="NAME" id="NAME" value="' . $arUser['LOGIN'] . '">'
  . '<input type="hidden" name="PROP[' . $PROP['CC_CLIENT']['ID'] . ']" id="PROP[' . $PROP['CC_CLIENT']['ID'] . ']" value="' . $arUser['ID'] . '">';
?>
<? if ($ID > 0 && !$bCopy): ?>
  <input type="hidden" name="ID" value="<? echo $ID ?>">
<? endif; ?>
<? if ($bCopy)
{
  ?><input type="hidden" name="copyID" value="<? echo intval($ID); ?>"><?
}
if ($bCatalog)
{
  CCatalogAdminTools::showFormParams();
}
?>
  <input type="hidden" name="IBLOCK_SECTION_ID" value="<? echo intval($IBLOCK_SECTION_ID) ?>">
  <input type="hidden" name="TMP_ID" value="<? echo intval($TMP_ID) ?>">
<?php
$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);
//$tabControl->AddTabs($customTabber);

$arEditLinkParams = array(
  "find_section_section" => intval($find_section_section)
);
if ($bAutocomplete)
{
  $arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(array(
  "FORM_ACTION" => "/bitrix/admin/" . \Itin\Depohost\Clients::getAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
));


//          -------------My tabs----------------------
foreach ($aTabs as $aTab)
{
  switch ($aTab['DIV'])
  {
    case 'edit_support':
      // --- Техподдержка

      $tabControl->BeginNextFormTab();
      $tabControl->BeginCustomField("password", "Отправить пароль");
      ?>
      <tr class="adm-detail-valign-top">
        <td width="40%"><?
          echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%"><input type="button"
                               onclick="jsAjaxUtil.InsertDataToNode('/local/modules/clients/admin/ajax/password.php?ID=<?= $ID ?>','adm-message')"
                               class="adm-btn" value="Отправить"></td>
      </tr>
      <tr>
        <td width="40%"></td>
        <td id="message_password" width="60%">

        </td>
      </tr>
      <?php
      $tabControl->EndCustomField("password");
      break;
    case 'edit_data_client':
      // --- Данные о клиенте
      $tabControl->BeginNextFormTab();
      // Договор
      $ar_fields = $PROP['CC_CONTRACT'];
      $tabControl->BeginCustomField('PROPERTY_' . $ar_fields['ID'], $ar_fields['NAME'] . ":", false);
      ?>
      <tr class="adm-detail-valign-top">
        <td width="40%"><?
          echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%"><?
          _ShowPropertyField('PROP[' . $ar_fields["ID"] . ']', $ar_fields, $ar_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy); ?></td>
      </tr>
      <?php
      $tabControl->EndCustomField("PROPERTY_" . $ar_fields['ID'], "");
      // Профиль клиента, берется из последнего профиля покупателя
      include_once 'tabs/buyers_profile.php';
      break;
    case 'edit_services':
      // --- Подключенные услуги
      $tabControl->BeginNextFormTab();
      $tabControl->BeginCustomField('SERVICES', "Услуги", false);
      ?>
      <tr id="tr_SERVICES">
        <td colspan="2">
          <?php
          include_once 'tabs/service_list.php';
          ?>
        </td>
      </tr>
      <?php
      $tabControl->EndCustomField("SERVICES", "");
      break;
    case 'edit_comment':
      // --- Блокнот
      $tabControl->BeginNextFormTab();
      $tabControl->BeginCustomField('PROPERTY_' . $propComment["ID"], "", false);
      ?>
      <tr id="tr_PROPERTY_<?= $propComment["ID"] ?>">
        <td colspan="2">
          <?
          _ShowPropertyField('PROP[' . $propComment["ID"] . ']', $propComment, $propComment["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy); ?>
        </td>
      </tr>
      <?php
      $tabControl->EndCustomField("PROPERTY_" . $propComment["ID"], "");
      break;
    case 'edit1':
      // -- Главная закладка
      include_once 'tabs/main_tab.php';
      break;
    case 'edit_billing':
      // -- Счета
      $tabControl->BeginNextFormTab();
      // Назначение платежа
      $ar_fields = $PROP['CC_BILL_DESCRIPTION'];
      $tabControl->BeginCustomField('PROPERTY_' . $ar_fields['ID'], $ar_fields['NAME'] . ":", false);
      ?>
      <tr class="adm-detail-valign-top">
        <td width="40%"><?
          echo $tabControl->GetCustomLabelHTML() ?></td>
        <td width="60%"><?
          _ShowPropertyField('PROP[' . $ar_fields["ID"] . ']', $ar_fields, $ar_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy); ?></td>
      </tr>
      <?php
      $tabControl->EndCustomField("PROPERTY_" . $ar_fields['ID'], "");
      // Счета
      $ar_fields = $PROP['CC_BILLING'];
      $tabControl->BeginCustomField('PROPERTY_' . $ar_fields['ID'], "Счета", false); ?>
      <tr id="tr_BILLING">
        <td colspan="2">
          <?php include_once 'tabs/billing.php'; ?>
        </td>
      </tr>
      <?php
      $tabControl->EndCustomField("PROPERTY_" . $ar_fields['ID'], "");
      break;
    case 'edit_acts':
      // -- Акты
      $ar_fields = $PROP['CC_ACTS'];
      $tabControl->BeginNextFormTab();
      $tabControl->BeginCustomField('PROPERTY_' . $ar_fields['ID'], "", false); ?>
      <tr id="tr_BILLING">
        <td colspan="2">
          <?php include_once 'tabs/acts.php'; ?>
        </td>
      </tr>
      <?php
      $tabControl->EndCustomField("PROPERTY_" . $ar_fields['ID'], "");
      break;
    case 'edit_sorm':
      $tabControl->BeginNextFormTab();
      $sorm = new \Itin\Depohost\Sorm;
      $hiddenHeaders = $sorm->hiddenHeaders;
      $arHeaders = array_filter($sorm->arHeaders, function ($key) use ($hiddenHeaders) {
        return !in_array($key, $hiddenHeaders);
      }, ARRAY_FILTER_USE_KEY);

      $arItem = $sorm->getItem($ID);
      if (!$arItem)
      {
        break;
      }

      $mapItem = $sorm->mapItem($arItem);

      $externalFields = [
        'end_contract_date', 'birthday', 'document'
      ];

      foreach ($arHeaders as $fieldId => $fieldName)
      {
        $fieldLabel = $fieldName. '('.$fieldId.')';
        if(!in_array($fieldId, $externalFields)) {
          $tabControl->BeginCustomField($fieldId, $fieldName);

          $html = '<input type="text" value="' . $mapItem[$fieldId] . '" disabled />';
          echo '<tr><td width="40%">' . ($tabControl->GetCustomLabelHTML($fieldId, $fieldLabel)) . '</td><td width="60%">' . $html . '</td></tr>';

          $tabControl->EndCustomField($fieldId);
        }
        else {
          if($fieldId === 'end_contract_date') {
            $prop_fields = $PROP['SORM_END_DATE'];
          }
          elseif($fieldId === 'birthday') {
            $prop_fields = $PROP['SORM_BIRTHDAY'];
          }
          elseif($fieldId === 'document') {
            $prop_fields = $PROP['SORM_DOCUMENT'];
          }

          $prop_values = $prop_fields["VALUE"];
          $tabControl->BeginCustomField("PROPERTY_".$prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"]==="Y");
          ?>
          <tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>"<?if ($prop_fields["PROPERTY_TYPE"]=="F"):?> class="adm-detail-file-row"<?endif?>>
            <td class="adm-detail-valign-top" width="40%"><?if($prop_fields["HINT"]!=""):
                ?><span id="hint_<?echo $prop_fields["ID"];?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
              endif;?><?echo $tabControl->GetCustomLabelHTML();?>:</td>
            <td width="60%"><?_ShowPropertyField('PROP['.$prop_fields["ID"].']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID<=0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);?></td>
          </tr>
          <?
          $hidden = "";
          if(!is_array($prop_fields["~VALUE"]))
            $values = Array();
          else
            $values = $prop_fields["~VALUE"];
          $start = 1;
          foreach($values as $key=>$val)
          {
            if($bCopy)
            {
              $key = "n".$start;
              $start++;
            }

            if(is_array($val) && array_key_exists("VALUE",$val))
            {
              $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val["VALUE"]);
              $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', $val["DESCRIPTION"]);
            }
            else
            {
              $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val);
              $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', "");
            }
          }
          $tabControl->EndCustomField("PROPERTY_".$prop_fields["ID"], $hidden);

        }
      }

      break;

    default:
      break;
  }
}

//          -------------END My tabs----------------------


$bDisabled =
  ($view == "Y")
  || ($bWorkflow && $prn_LOCK_STATUS == "red")
  || (
    (($ID <= 0) || $bCopy)
    && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
  )
  || (
    (($ID > 0) && !$bCopy)
    && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "section_element_bind") && $APPLICATION->GetGroupRight("clients") == "D"
  )
  || (
    $bBizproc
    && !$canWrite
  );

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):
  ob_start();
  ?>
  <input <?
  if ($bDisabled)
  {
    echo "disabled";
  } ?> type="submit" class="adm-btn-save" name="save" id="save" value="<?
  echo GetMessage("IBLOCK_EL_SAVE") ?>">
  <? if (!$bAutocomplete)
{
  ?><input <?
  if ($bDisabled)
  {
    echo "disabled";
  } ?> type="submit" class="button" name="apply" id="apply" value="<?
echo GetMessage('IBLOCK_APPLY') ?>"><?
}
  ?>
  <input <?
  if ($bDisabled)
  {
    echo "disabled";
  } ?> type="submit" class="button" name="dontsave" id="dontsave" value="<?
  echo GetMessage("IBLOCK_EL_CANC") ?>">
  <?php if (!$bAutocomplete)
{
  ?><input <?
  if ($bDisabled)
  {
    echo "disabled";
  } ?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?
echo GetMessage("IBLOCK_EL_SAVE_AND_ADD") ?>">
  <?php
}
  $buttons_add_html = ob_get_contents();
  ob_end_clean();
  $tabControl->Buttons(false, $buttons_add_html);
elseif (!$bPropertyAjax && $nobuttons !== "Y"):

  $wfClose = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC")) . "',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
  $save_and_add = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_SAVE_AND_ADD")) . "',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
  $cancel = "{
		title: '" . CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC")) . "',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
  $tabControl->ButtonsPublic(array(
    '.btnSave',
    ($ID > 0 && $bWorkflow ? $wfClose : $cancel),
    $save_and_add,
  ));
endif;

$tabControl->Show();

if (
  (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1)
  && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_edit")
  && !$bAutocomplete
)
{

  echo
  BeginNote(),
  GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
    ' <a href="/bitrix/admin/iblock_edit.php?type=' . htmlspecialcharsbx($type) . '&amp;lang=' . LANGUAGE_ID . '&amp;ID=' . $IBLOCK_ID . '&amp;admin=Y&amp;return_url=' . urlencode("/bitrix/admin/" . \Itin\Depohost\Clients::getAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF == "Y" ? "Y" : null),
                                                                                                                                                                                                                                                                              "find_section_section" => intval($find_section_section),
                                                                                                                                                                                                                                                                              "return_url" => (strlen($return_url) > 0 ? $return_url : null)))) . '">',
  GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
  '</a>',
  EndNote();
}