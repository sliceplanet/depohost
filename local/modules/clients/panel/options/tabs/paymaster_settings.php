<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/local/modules/clients/options.php');
$moduleId = 'clients';
// handler request
if (is_array($_REQUEST['FIELDS']['paymaster']))
{
    if (!isset($_REQUEST['FIELDS']['paymaster']['paymaster_test']))
    {
        $_REQUEST['FIELDS']['paymaster']['paymaster_test'] = 'N';
    }
    foreach ($_REQUEST['FIELDS']['paymaster'] as $key => $value)
    {
        Option::set($moduleId, $key, (string) $value);
    }
}
$merchant_id = Option::get($moduleId, 'paymaster_merchant_id', '');
$test = Option::get($moduleId, 'paymaster_test', 'N');
$secret = Option::get($moduleId, 'paymaster_secret', '');

// END handler request
?>
<tr>
    <td width="40%" class="adm-detail-content-cell-l">
        <?= Loc::getMessage('MERCHANT_ID') ?>
    </td>
    <td class="adm-detail-content-cell-r">
        <input type="text" name="FIELDS[paymaster][paymaster_merchant_id]" value="<?= $merchant_id ?>" size="30" />
    </td>
</tr>
<tr>
    <td width="40%" class="adm-detail-content-cell-l">
        <?= Loc::getMessage('SECRET') ?>
    </td>
    <td class="adm-detail-content-cell-r">
        <input type="text" name="FIELDS[paymaster][paymaster_secret]" value="<?= $secret ?>" size="30" />
    </td>
</tr>
<tr>
    <td width="40%" class="adm-detail-content-cell-l">
        <label><?= Loc::getMessage('TEST') ?></label>
    </td>
    <td class="adm-detail-content-cell-r">
        <input type="checkbox" name="FIELDS[paymaster][paymaster_test]" value="Y" <? if ($test == 'Y') echo 'checked' ?> />
    </td>
</tr>