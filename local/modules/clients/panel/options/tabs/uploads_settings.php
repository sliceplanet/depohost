<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/local/modules/clients/options.php');
$moduleId = 'clients';
// handler request
if (is_array($_REQUEST['FIELDS']['uploads']))
{
  foreach ($_REQUEST['FIELDS']['uploads'] as $key => $value)
  {
    Option::set($moduleId, $key, (string)$value);
  }
}
$sorm_agent_id = Option::get($moduleId, 'sorm_agent_id', '3390');
$sorm_region_id = Option::get($moduleId, 'sorm_region_id', 'CFO');
$sorm_only_active = Option::get($moduleId, 'sorm_only_active', 'N');
\Bitrix\Main\Loader::includeModule('clients');
$sorm = new \Itin\Depohost\Sorm();

// END handler request

?>
<tr class="heading">
  <td colspan="2"><?= Loc::getMessage('UPLOADS_HEADER_GROUP_SORM') ?></td>
</tr>

<tr>
  <td colspan="2"><?= Loc::getMessage('UPLOADS_SORM_FILENAME', ['#FILENAME#' => $sorm->getFileName()]) ?></td>
</tr>
<tr>
  <td colspan="2"><?= Loc::getMessage('UPLOADS_SORM_CRON_PATH') ?></td>
</tr>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('UPLOADS_SORM_AGENT_ID') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <input type="text" name="FIELDS[uploads][sorm_agent_id]" value="<?= $sorm_agent_id ?>" size="5"/>
  </td>
</tr>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('UPLOADS_SORM_REGION_ID') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <input type="text" name="FIELDS[uploads][sorm_region_id]" value="<?= $sorm_region_id ?>" size="5"/>
  </td>
</tr>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('UPLOADS_SORM_ONLY_ACTIVE') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <input type="hidden" name="FIELDS[uploads][sorm_only_active]" value="N">
    <input type="checkbox" name="FIELDS[uploads][sorm_only_active]" value="Y" <?if($sorm_only_active==='Y') {echo 'checked';}?>/>
  </td>
</tr>


