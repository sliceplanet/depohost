<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/local/modules/clients/options.php');
$moduleId = 'clients';
// handler request
if (is_array($_REQUEST['FIELDS']['sms']))
{
  foreach ($_REQUEST['FIELDS']['sms'] as $key => $value)
  {
    Option::set($moduleId, $key, (string)$value);
  }
}
$sms_api_key = Option::get($moduleId, 'sms_api_key');
$sms_api_key_valid_date = Option::get($moduleId, 'sms_api_key_valid_date');
$sms_source_address = Option::get($moduleId, 'sms_source_address', 'DepoTelecom');
// END handler request
?>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('SMS_API_KEY') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <input type="text" name="FIELDS[sms][sms_api_key]" value="<?= $sms_api_key ?>" size="30"/>
  </td>
</tr>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('SMS_API_KEY_VALID_DATE') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <?=CAdminCalendar::CalendarDate('FIELDS[sms][sms_api_key_valid_date]', $sms_api_key_valid_date, 20);?>
  </td>
</tr>
<tr>
  <td width="40%" class="adm-detail-content-cell-l">
    <?= Loc::getMessage('SMS_SOURCE_ADDRESS') ?>
  </td>
  <td class="adm-detail-content-cell-r">
    <input type="text" name="FIELDS[sms][sms_source_address]" value="<?= $sms_source_address ?>" size="30"/>
  </td>
</tr>