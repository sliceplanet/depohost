<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/local/modules/clients/options.php');
$moduleId = 'clients';
// handler request
if (is_array($_REQUEST['FIELDS']['isp']))
{
    foreach ($_REQUEST['FIELDS']['isp'] as $key => $value)
    {
        Option::set($moduleId, $key, (string) $value);
    }
}
$url_dns = Option::get($moduleId, 'url_dns');
$login_dns = Option::get($moduleId, 'login_dns');
$password_dns = Option::get($moduleId, 'password_dns');
$url_ftp = Option::get($moduleId, 'url_ftp');
$login_ftp = Option::getRealValue($moduleId, 'login_ftp');
$password_ftp = Option::get($moduleId, 'password_ftp');

// END handler request

?>
    <tr class="heading"><td colspan="2"><?=Loc::getMessage('ISP_HEADER_GROUP_DNS')?></td></tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_URL_DNS')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text" name="FIELDS[isp][url_dns]" value="<?=$url_dns?>" size="30" />
        </td>
    </tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_ADMIN_LOGIN')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text" name="FIELDS[isp][login_dns]" value="<?=$login_dns?>" size="30" />
        </td>
    </tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_ADMIN_PASSWORD')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="password" name="FIELDS[isp][password_dns]" value="<?=$password_dns?>" size="30" />
        </td>
    </tr>
    <tr class="heading"><td colspan="2"><?=Loc::getMessage('ISP_HEADER_GROUP_FTP')?></td></tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_URL_FTP')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text" name="FIELDS[isp][url_ftp]" value="<?=$url_ftp?>" size="30" />
        </td>
    </tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_ADMIN_LOGIN')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text" name="FIELDS[isp][login_ftp]" value="<?=$login_ftp?>" size="30" />
        </td>
    </tr>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage('ISP_ADMIN_PASSWORD')?>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="password" name="FIELDS[isp][password_ftp]" value="<?=$password_ftp?>" size="30" />
        </td>
    </tr>

