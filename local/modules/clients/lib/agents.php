<?php
/**
 * Created by PhpStorm.
 * User: uk1
 * Date: 21.03.2018
 * Time: 10:07
 */

namespace Itin\Depohost;


/**
 * Скрипты которые используються в некоторый период.
 *
 * Class СlientsAgents
 * @package Itin\Depohost
 */
class СlientsAgents
{
    /**
     * Метод находит все услуги которые имею такие услувоия:
     * 1.) Период - год
     * 2.) Через месяц они закончатся
     * 3.) На них можно выставлять счет
     * 4.) Сумма больше нуля
     *
     * Если все условия совпадают Формируется счет с этой услугой и отсылается пользователю.
     * Плюс отсылается письмо на почту клиента с предупреждением об окончании услуги
     *
     */
    public static function getAllYearServicesAndProlongation(){

        $arParams = array(
            'filter' => array(
                'ACTIVE' => 'Y'
            )
        );

        $result = Clients::getList($arParams);

        $date = new \Bitrix\Main\Type\Date();
        $date->add("+1 month");

        $filter = array(
            'UF_PERIOD' => 'year',
            'UF_DATE_TO' => $date,
            'UF_BILL' => 1,
            '>UF_SUMM' => 0
        );

        $new = array();
        $new['DATE'] = $date;

        $invoice = new Invoices;
        $default_services = $invoice->getDefaultValueProperties();


        foreach($result as &$client){

            $item = array(
                'ID' => $client['ID'],
                'NAME' => $client['NAME'],
            );

            $clients = new Clients($client['CC_CLIENT']['VALUE']);
            $services = $clients->getClientServicesList($client['ID'],$filter);

//            $item['SER'] = $services;

            if(!empty($services)){

                $invoice->setClientCardId($client['ID']);

                foreach($services as $s){

                    $values = $default_services;
                    $values['SUMM'] = $s['UF_SUMM'];
                    $values['CLIENT'] = $client['ID'];
                    $values['SERVICES'][] = $s['ID'];
                    $values['DATE'] = FormatDate('SHORT',time());

                    $values['BILL_ID'] = $invoice->add(
                        array(
                            'ACTIVE'=>'Y',
                            'PROPERTY_VALUES'=>$values
                        )
                    );
                    $item['NEW_BILL'][] = $values;

                    if($values['BILL_ID'] > 0){

                            $type = 'SSL-сертификат';
                            if($s['UF_CATALOG_XML_ID'] == 'u3'){
                                $type = 'Домен';
                            }

                            $fields = array(
                                'SERVICE' => $s['UF_NAME'],
                                'TYPE' => $type
                            );

                        $clients->sendMail('NEW_YEAR_SERVICE_BILL', $fields);
                    }

                }

                $new[] = $item;

            }



        }

//        define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/local/cron/log.txt");
//        AddMessage2Log($new, "clients");
//
//        echo $date;
//        echo '<pre>';print_r($new);echo "</pre>";

    }

  /**
   * Создание и отправка выгрузки СОРМ
   *
   * @return \Itin\Depohost\Sorm
   */
  public static function sormCSV()
  {
    $sorm = new \Itin\Depohost\Sorm;
    $sorm->createAndSendUpload();

    return $sorm;
  }
}