<?php

namespace Itin\Depohost;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader as Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;
use CEvent;
use CFile;
use CIBlockElement;
use CIBlockPropertyEnum;
use CUsefull;

Loc::loadMessages(__FILE__);

/**
 * Класс для работы со счетами клиентов
 *
 * @author Alexandr Terentev
 */
class Invoices
{

  /** @var int    ID инфоблока счетов */
  private $iblock_id;

  /** @var int    ID элемента инфоблока Счетов */
  private $id;

  /** @var array  Буффер для контроля рекурсии в методе getFormat */
  private $buffIblockId;

  /** @var int    ID элемента Карточки клиента относящейся к счету */
  private $client_id;

  /** @var array  текущий уровень вложенности массива данных в методе getFormat */
  private $current_level;

  /**
   * @param int $id ID элемента инфлока Счетов
   */

  function __construct($id = false)
  {
    $this->id = (int)$id;
    $this->iblock_id = Info::getIblockId('invoices');
    $this->buffIblockId = array();
    $this->current_level = 0;

    if ($this->id > 0)
    {
      $this->getClientCardId();
    }

//        if (defined('ADMIN_SECTION') && ADMIN_SECTION===true)
//        {
//            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
//        }
//        else
//        {
//            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//        }
  }

  /**
   * Отправка счета на E-mail
   *
   * @param int $id ID элемента инфоблока Счета
   *
   * @return null
   */

  public function sendToMail($id = false)
  {
    Loader::includeModule('iblock');
    $client = new Clients;
    $id = intval($id) > 0 ? intval($id) : $this->getId();
    $ob = new CIBlockElement;
    $res = $ob->GetList(array(), array('ID' => $id), false, false, array('ID',
                                                                         'IBLOCK_ID',
                                                                         'PROPERTY_CLIENT',
                                                                         'PROPERTY_DOCUMENT',
                                                                         'PROPERTY_NUMBER',
                                                                         'PROPERTY_DATE',
                                                                         'PROPERTY_SUMM'));
    if ($ar = $res->Fetch())
    {

      $doc_url = !empty($ar['PROPERTY_DOCUMENT_VALUE']) ? CFile::GetPath($ar['PROPERTY_DOCUMENT_VALUE']) : false;

      $client_card_id = $ar['PROPERTY_CLIENT_VALUE'];
      $profile_id = $client->getProfileId(array('element_id' => $client_card_id));
      $arProfile = $client->getProfile($profile_id);
      $arClient = $client->getList(array(
        'select' => array('CC_ABONPLATA',
                          'DATE_CREATE',
                          'NAME',),
        'filter' => array('=ID' => $client_card_id),
      ));
      $arClient = current($arClient);
      $email = array();
      foreach ($arProfile
               as
               $prop)
      {
        if (strpos($prop['PROP_CODE'], 'EMAIL') !== false)
        {
          $email[] = $prop['VALUE'];
        }
      }
      $email = array_unique($email);
      $email = implode(', ', $email);
    }
    $arFieldsEvent = array(
      'EMAIL' => $email,
      'NUMBER' => $ar['PROPERTY_NUMBER_VALUE'],
      'DATE' => $ar['PROPERTY_DATE_VALUE'],
      'ABONPLATA' => number_format($arClient['CC_ABONPLATA']['VALUE'], 0, ',', ' '),
      'SUMM' => number_format($ar['PROPERTY_SUMM_VALUE'], 0, ',', ' '),
      'NUMBER_CONTRACT' => $arClient['NAME'],
      'DATE_CONTRACT' => FormatDateEx($arClient['DATE_CREATE'], false, 'dd.mm.YYYY'),
    );
    if (!empty($doc_url))
    {
      $arFieldsEvent['ATTACH'] = $doc_url;
    }
    $ob = new SiteTable;
    $res = $ob->getList();
    $arSites = array();
    while ($ar = $res->fetch())
    {
      $arSites[] = $ar['LID'];
    }

    return CEvent::Send('CLIENT_SEND_INVOICE', $arSites, $arFieldsEvent);
//        return \CEvent::SendImmediate('CLIENT_SEND_INVOICE',$arSites,$arFieldsEvent);
  }

  /**
   * Получение ID инфоблока счетов
   *
   * @return int
   */

  public function getIblockId()
  {
    if (intval($this->iblock_id) > 0)
    {
      return $this->iblock_id;
    }
    else
    {
      $iblock_id = Info::getIblockId('invoices');
      $this->iblock_id = $iblock_id;
      return $iblock_id;
    }
  }

  /**
   * Добавление счета
   *
   * @param array $arFields Массив полей и свойств счета
   *
   * @return bool|int               Возвращает ID элемента инфоблока
   */

  public function add($arFields = array())
  {
    if (empty($arFields))
    {
      // Формирую поля и свойства для добавления
      $cCardId = $this->getClientCardId();
      global $DB;
      $date = FormatDate('SHORT', time());
      $arFields['IBLOCK_ID'] = Info::getIblockId('invoices');
      $arFields['PROPERTY_VALUES'] = array('CLIENT' => $cCardId,
                                           'DATE' => $date);
      $ar_client = Clients::getById($cCardId);
      $arFields['PROPERTY_VALUES']['SUMM'] = $ar_client['CC_ABONPLATA']['VALUE'];
      $arFields['PROPERTY_VALUES']['DESCRIPTION'] = $ar_client['CC_BILL_DESCRIPTION']['VALUE'];
      $res = CIBlockPropertyEnum::GetList(array("SORT" => "ASC",
                                                "VALUE" => "ASC"), array(
          'PROPERTY_ID' => 'STATUS',
          'IBLOCK_ID' => $arFields['IBLOCK_ID'],
          'DEF' => 'Y'
        )
      );
      $ar = $res->Fetch();
      $arFields['PROPERTY_VALUES']['STATUS'] = $ar['ID'];
      // Берем только услуги имеющие флаг требования оплаты
      $arServicePaying = Clients::getCSListReqPay($cCardId);
      $arServiceIds = array_keys($arServicePaying);
      $arFields['PROPERTY_VALUES']['SERVICES'] = $arServiceIds;
      $arFields['NAME'] = '№';

      $ob = new CIBlockElement;
      $result = $ob->Add($arFields);

      // Обновляем инфу о счетах в карточке клиента
      $CLIENT_IBLOCK_ID = Info::getIblockId('ccards');
      $res = $ob->GetProperty($CLIENT_IBLOCK_ID, $cCardId, array(), array('CODE' => 'CC_BILLING',
                                                                          'EMPTY' => 'N'));
      $ar_element_ids = array();
      while ($ar = $res->Fetch())
      {
        $ar_element_ids[] = $ar['VALUE'];
      }

      $ar_element_ids[] = $result;
      $ob->SetPropertyValuesEx($cCardId, $CLIENT_IBLOCK_ID, array('CC_BILLING' => $ar_element_ids));
    }
    else
    {
      global $DB;
      $date = FormatDate('SHORT', time());
      $arFields['IBLOCK_ID'] = Info::getIblockId('invoices');
      $cCardId = !empty($arFields['PROPERTY_VALUES']['CLIENT']) ? $arFields['PROPERTY_VALUES']['CLIENT'] : $this->getClientCardId();
      $res = CIBlockPropertyEnum::GetList(array("SORT" => "ASC",
                                                "VALUE" => "ASC"), array(
          'PROPERTY_ID' => 'STATUS',
          'IBLOCK_ID' => $arFields['IBLOCK_ID'],
          'DEF' => 'Y'
        )
      );
      $ar = $res->Fetch();
      $arFields['PROPERTY_VALUES']['STATUS'] = $ar['ID'];
      // Берем только услуги имеющие флаг требования оплаты
      $arServicePaying = Clients::getCSListReqPay($cCardId);
      $arServiceIds = array_keys($arServicePaying);
      $arFields['NAME'] = !empty($arFields['NAME']) ? $arFields['NAME'] : '№';
      $ob = new CIBlockElement;
      $result = $ob->Add($arFields);

      // Обновляем инфу о счетах в карточке клиента
      $CLIENT_IBLOCK_ID = Info::getIblockId('ccards');
      $res = $ob->GetProperty($CLIENT_IBLOCK_ID, $cCardId, array(), array('CODE' => 'CC_BILLING',
                                                                          'EMPTY' => 'N'));
      $ar_element_ids = array();
      while ($ar = $res->Fetch())
      {
        $ar_element_ids[] = $ar['VALUE'];
      }

      $ar_element_ids[] = $result;
      $ob->SetPropertyValuesEx($cCardId, $CLIENT_IBLOCK_ID, array('CC_BILLING' => $ar_element_ids));
    }
    return $result;
  }

  /**
   * Обновление счета
   *
   * @params array $arProperties      Структура массива такая же, как у CIBlockElement::SetPropertyValuesEx()
   *
   * @return boolean
   */

  function Update($arProperties = array())
  {
    $ID = $this->id;
    $IBLOCK_ID = $this->iblock_id;
    $ob = new CIBlockElement;
    $result = $ob->SetPropertyValuesEx($ID, $IBLOCK_ID, $arProperties);
  }

  /**
   * Получение номера счета
   */

  public function getNumber($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    // @todo
  }

  /*
   * Установка ID элемента инфоблока счета
   */

  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Получение ID элемента инфоблока счета
   */

  public function getId()
  {
    return $this->id;
  }

  /**
   * Генерирует pdf файл счета
   *
   * @param int $id ID элемента инфоблока Счета
   */

  public function generatePdf($id = false, $elid = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->id = $id;
    // Получаем все данные для счета из инфоблоков
    $ar = $this->getById($id);
    $arData = $this->getFormat($ar);

	// Актуализируем цены SSL
	foreach($arData['SERVICES'] as $serviceKey=>$serviceArr) {
		if(!empty($serviceArr['UF_ELID']) || $elid) {
		$re = $elid;
			if(!empty($serviceArr['UF_ELID'])) {
				$elid = $serviceArr['UF_ELID'];
			}
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "catalog_PRICE_5");
			$arFilter = Array("IBLOCK_ID"=>30, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_ELID"=>$elid);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				if(!empty($arFields['CATALOG_PRICE_5'])) {
					$arData['SERVICES'][$serviceKey]['UF_PRICE'] = $arFields['CATALOG_PRICE_5'];
					$arData['SERVICES'][$serviceKey]['UF_SUMM'] = $serviceArr['UF_QUANTITY'] * $arFields['CATALOG_PRICE_5'];
				}
			}
		}
	}

    $client = new Clients($arData['CLIENT']['CC_CLIENT']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();
    // Инфа о клиенте
    $arClient = $client->getById($client->getElementId());
    $type_payment = $arClient['TYPE_PAYMENT']['VALUE_XML_ID'];
    switch ($type_payment)
    {
      case 'sb_bill':
        return $this->generateSbPdf();
        break;
      case 'emoney':
        return false;
        break;
      default:
        if (!empty($arProfile['ADDRESS_FULL']))
        {
          $address = $arProfile['ADDRESS_FULL'];
        }
        else
        {
          // формирую адрес из индекса, города, и самого адреса
          $arAddress = array();
          $arAddress[] = $arProfile['POST_INDEX'];
          $arAddress[] = $arProfile['LOCATION'];
          $arAddress[] = $arProfile['ADDRESS'];
          $address = implode(', ', $arAddress);
        }
        // добавляю точку в конец Назначения платежа, если ее нет.
        $arData['DESCRIPTION'] .= strpos('.', $arData['DESCRIPTION']) == strlen($arData['DESCRIPTION']) - 1 || empty($arData['DESCRIPTION']) ? '' : '.';
        $arProfile['KPP'] = !empty($arProfile['KPP']) ? '/ ' . $arProfile['KPP'] : '';
        $contract_date = $arData['CLIENT']['DATE_CREATE'];

        Loader::includeModule('sale');
        // скидка и месяцы для списка услуг
        if (doubleval($arData['DISCOUNT']) > 0 || intval($arData['COUNT_MONTHS']) > 0)
        {
          $sum = 0;
          foreach ($arData['SERVICES']
                   as
                   $key
          =>
                   $arService)
          {
            $price = $arService['UF_PRICE'];
            $price -= $price * $arData['DISCOUNT'] / 100;
            $countMonths = $arData['COUNT_MONTHS'] > 0 ? $arData['COUNT_MONTHS'] : 1;

            $arService['UF_PRICE'] = $price;
            $arService['UF_QUANTITY'] *= $countMonths;

            $sum += $arService['UF_SUMM'] = $price * $arService['UF_QUANTITY'];

            $arData['SERVICES'][$key] = $arService;

          }
          $arData['SUMM'] = $sum;

        }

        $formatPeriodService = intval($arData['COUNT_MONTHS']) > 0 ? $arData['DESCRIPTION'] : FormatDateEx($arData['DATE'], false, 'f_YYYY');

        // Формируем маркеры для pdf
        $table_services = $this->getTableListServices($arData['SERVICES'], Loc::getMessage('TEMPLATE_SERVICE_NAME', array(
              '#CONTRACT_ID#' => $arData['CLIENT']['NAME'],
              '#CONTRACT_DATE#' => FormatDateEx($contract_date, false, 'dd.mm.YYYY')
            )
          ) . " "
          . $formatPeriodService
        );
        $arValues = array(
          'INVOICE_ID' => $arData['NUMBER'],
          'INVOICE_DATE' => FormatDateEx($arData['DATE'], false, 'd F YYYY'),
          'INVOICE_DATE_SAMPLE' => FormatDateEx($arData['DATE'], false, 'dd.mm.YYYY'),
          'CONTRACT_ID' => $arData['CLIENT']['NAME'],
          'CONTRACT_DATE' => FormatDateEx($contract_date, false, 'dd.mm.YYYY'),
          'NAME' => $arProfile['NAME'],
          'INN' => $arProfile['INN'],
          'KPP' => $arProfile['KPP'],
          'ADDRESS' => $address,
          'DESCRIPTION' => $arData['DESCRIPTION'],
          'TABLE_LIST_SERVICES' => $table_services,
          'TOTAL_SUM' => number_format($arData['SUMM'], 2, ',', ' '),
          'TOTAL_SUM_STRING' => Number2Word_Rus($arData['SUMM']),
        );
        // формируем pdf
        $pdf = new Pdf;
        $pdf->setMarkers($arValues);
        $path_module = Info::getPath();
        $pdf->setTemplateFromFolder($path_module . '/templates/pdf/invoice/');

        // Если указано количество месяцев, то счет выставляется из карточки клиента и вместо месяца в имя файла дописываем назначение платежа
        $date_name = $arData['COUNT_MONTHS'] > 0 ? $arData['DESCRIPTION'] : FormatDateEx($arData['DATE'], false, 'f YYYY');
        $arFilename = array($arProfile['NAME'],
                            $arData['CLIENT']['NAME'],
                            $date_name);
        //        $filename = implode('_', $arFilename).'.pdf';
        $filename = 'Счет по договору №' . $arData['CLIENT']['NAME']
          . ' от ' . FormatDateEx($contract_date, false, 'dd.mm.YYYY')
          . ', период ' . $date_name
          . ' ' . $arProfile['NAME'] . '.pdf';
        $filename = str_replace('"', '', $filename);
        $arParams = array(
          'path' => '/upload/clients/pdf/invoices/tmp/',
          'filename' => $filename
        );
        $temp_file = $_SERVER['DOCUMENT_ROOT'] . $arParams['path'] . $arParams['filename'];
        $file = $pdf->createPdf($arParams);
        $arFile = CFile::MakeFileArray($file['PATH']);
        $arFile['name'] = $filename;
        $arFile['MODULE_ID'] = 'clients';
        Loader::includeModule('iblock');
        $save_path = '/clients/pdf/invoices/';
//                $file_id = CFile::SaveFile($arFile, $save_path);
//                CIBlockElement::SetPropertyValueCode($arData['ID'], "DOCUMENT", $file_id);
        CIBlockElement::SetPropertyValueCode($arData['ID'], "DOCUMENT", $arFile);
        $res = CIBlockElement::GetList(array(), array('=ID' => $arData['ID']), false, false, array('ID',
                                                                                                   'IBLOCK_ID',
                                                                                                   'PROPERTY_DOCUMENT'));
        $ar = $res->Fetch();
        $file_id = $ar['PROPERTY_DOCUMENT_VALUE'];

//                \CIBlockElement::SetPropertyValuesEx($arData['ID'],$arData['IBLOCK_ID'],array('DOCUMENT'=>array(
//                    'VALUE'=>\CFile::MakeFileArray($file_id),'DESCRIPTION'=>$arFile['name']
//                )));
        unlink($temp_file);
        return $file_id;
        break;
    }
  }

  /**
   * Генерирует pdf файл квитанции
   *
   * @param int $id ID элемента инфоблока Счета
   */

  public function generateSbPdf($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->id = $id;
    // Получаем все данные для счета из инфоблоков
    $ar = $this->getById($id);
    $arData = $this->getFormat($ar);
    $client = new Clients($arData['CLIENT']['CC_CLIENT']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();
    if (!empty($arProfile['ADDRESS_FULL']))
    {
      $address = $arProfile['ADDRESS_FULL'];
    }
    else
    {
      // формирую адрес из индекса, города, и самого адреса
      $arAddress = array();
      $arAddress[] = $arProfile['POST_INDEX'];
      $arAddress[] = $arProfile['LOCATION'];
      $arAddress[] = $arProfile['ADDRESS'];
      $address = implode(', ', $arAddress);
    }
    // добавляю точку в конец Назначения платежа, если ее нет.
    $arData['DESCRIPTION'] .= strpos('.', $arData['DESCRIPTION']) == strlen($arData['DESCRIPTION']) - 1 || empty($arData['DESCRIPTION']) ? '' : '.';
    $arProfile['KPP'] = !empty($arProfile['KPP']) ? '/ ' . $arProfile['KPP'] : '';
    $contract_date = $arData['CLIENT']['DATE_CREATE'];

    $ar_temp = explode(',', number_format($arData['SUMM'], 2, ',', ' '));
    $kopeiki = $ar_temp[1];


    Loader::includeModule('sale');
    // Формируем маркеры для pdf
    $arValues = array(
      'INVOICE_ID' => $arData['NUMBER'],
      'INVOICE_DATE' => FormatDateEx($arData['DATE'], false, 'd F YYYY'),
      'CONTRACT_ID' => $arData['CLIENT']['NAME'],
      'CONTRACT_DATE' => FormatDateEx($contract_date, false, 'dd.mm.YYYY'),
      'NAME' => $arProfile['NAME'],
      'INN' => $arProfile['INN'],
      'KPP' => $arProfile['KPP'],
      'ADDRESS' => $address,
      'DESCRIPTION' => $arData['DESCRIPTION'],
      'TOTAL_SUM_RUB' => str_replace(' ', '&nbsp;', number_format($arData['SUMM'], 0, ',', ' ')),
      'TOTAL_SUM_KOP' => $kopeiki,
    );
    // формируем pdf
    $pdf = new Pdf;
    $pdf->setMarkers($arValues);
    $path_module = Info::getPath();
    $pdf->setTemplateFromFolder($path_module . '/templates/pdf/invoice-sb/');

    $date_name = FormatDateEx($arData['DATE'], false, 'f_YYYY');
    $arFilename = array($arProfile['NAME'],
                        $arData['CLIENT']['NAME'],
                        $date_name);
//        $filename = implode('_', $arFilename).'.pdf';
    $filename = 'Квитанция к договору №' . $arData['CLIENT']['NAME']
      . ' от ' . FormatDateEx($contract_date, false, 'dd.mm.YYYY')
      . ', период ' . FormatDateEx($arData['DATE'], false, 'f YYYY')
      . ' ' . $arProfile['NAME'] . '.pdf';
    $filename = str_replace('"', '', $filename);
    $arParams = array(
      'path' => '/upload/clients/pdf/invoices/',
      'filename' => $filename
    );
    $temp_file = $_SERVER['DOCUMENT_ROOT'] . $arParams['path'] . $arParams['filename'];
    $file = $pdf->createPdf($arParams);
    $arFile = CFile::MakeFileArray($file['PATH']);
    $arFile['name'] = $filename;
    $arFile['MODULE_ID'] = 'clients';
    Loader::includeModule('iblock');
//        $save_path = '/clients/pdf/invoices/';
//        $file_id = CFile::SaveFile($arFile, $save_path);
    CIBlockElement::SetPropertyValueCode($arData['ID'], "DOCUMENT", $arFile);
    $res = CIBlockElement::GetList(array(), array('=ID' => $arData['ID']), false, false, array('ID',
                                                                                               'IBLOCK_ID',
                                                                                               'PROPERTY_DOCUMENT'));
    $ar = $res->Fetch();
    $file_id = $ar['PROPERTY_DOCUMENT_VALUE'];
//        CIBlockElement::SetPropertyValueCode($arData['ID'], "DOCUMENT", $file_id);
//        \CIBlockElement::SetPropertyValuesEx($arData['ID'],$arData['IBLOCK_ID'],array('DOCUMENT'=>array(
//            'VALUE'=>\CFile::MakeFileArray($file_id),'DESCRIPTION'=>$arFile['name']
//            )));
    unlink($temp_file);
    return $file_id;
  }

  /**
   * Получение списка счетов
   *
   * @param array $arParam
   *
   * @return array
   */

  public function getList($arParams)
  {
    Loader::includeModule('iblock');
    $arNav = intval($arParams['limit']) > 0 ? array('nTopCount' => intval($arParams['limit'])) : false;
    $ob = new CIBlockElement;
    $arParams['filter']['IBLOCK_ID'] = $this->getIblockId();
    $arSelectDefault = array('*', 'PROPERTY_*');
    $arSelect = !empty($arParams['select']) ? $arParams['select'] : $arSelectDefault;
    $arOrder = !empty($arParams['order']) ? $arParams['order'] : array();
    $res = $ob->GetList(
      $arOrder, $arParams['filter'], false, $arNav, $arSelect
    );
    while ($ob = $res->GetNextElement())
    {
      $arFields = $ob->GetFields();
      $arProperties = $ob->GetProperties();
      $arValues[] = array_merge($arFields, $arProperties);
    }
    return $arValues;
  }

  /**
   * Получение счета по ID
   *
   * @param int $id ID элемента инфоблока Счет
   *
   * @return array
   */

  public function getById($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->id = $id;
    $ar = $this->getList(array('filter' => array('ID' => $id)));
    return current($ar);
  }

  /**
   * Получаем инфу о последнем счете клиента
   *
   * @param $client_element_id ID элемента инфоблока клиенты
   *
   * @return mixed
   */
  public static function getLastInvoice($client_element_id)
  {
    $arParams = array('limit' => 1,
                      'filter' => array('=PROPERTY_CLIENT' => $client_element_id),
                      'order' => array('ID' => 'DESC'));
    $invoice = new Invoices;
    return array_pop($invoice->getList($arParams));
  }

  /**
   * Получение полей и свойств счета в форматированном виде
   *
   * @param array $arValues
   *
   * @return array
   * @internal param int $id ID элемента инфоблока Счет
   *
   */

  public function getFormat(array $arValues)
  {
    $arValues = CUsefull::cleanTilda($arValues);

    $hash_data = md5(serialize($arValues));
    if (!isset($this->buffIblockId[$hash_data]))
    {
      // Если точно такого же массива данных еще не было
      $this->buffIblockId[$hash_data] = true;
      foreach ($arValues
               as
               $code
      =>
               $arValue)
      {
        if (is_array($arValue))
        {
          if (($arValue['PROPERTY_TYPE'] == 'N' || ($arValue['PROPERTY_TYPE'] == 'S' && $arValue['USER_TYPE'] != 'directory')))
          {
            $result[$code] = $arValue['VALUE'];
          }
          elseif ($arValue['PROPERTY_TYPE'] == 'F')
          {
            $getPath = function ($id) {
              return CFile::GetPath($id);
            };
            $result[$code] = $arValue['MULTIPLE'] == 'N' ? CFile::GetPath($arValue['VALUE']) : array_map($getPath, $arValue['VALUE']);
          }
          elseif ($arValue['PROPERTY_TYPE'] == 'E')
          {
            if (!empty($arValue['VALUE']))
            {
              $arOrder = array('SORT' => 'ASC',
                               'NAME' => 'ASC');
              $arFilter = array(
                'ID' => $arValue['VALUE'],
                'IBLOCK_ID' => $arValue['LINK_IBLOCK_ID']
              );
              $arSelect = array('*');
              $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
              if ($arValue['MULTIPLE'] == 'N')
              {
                if ($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $arProperties = $ob->GetProperties();
                  $ar_values = array_merge($arFields, $arProperties);
                  $result[$code] = $this->getFormat($ar_values);
                }
              }
              else
              {
                while ($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $arProperties = $ob->GetProperties();
                  $ar_values = array_merge($arFields, $arProperties);
                  $result[$code][$arFields['ID']] = $this->getFormat($ar_values);
                }
              }
            }
            else
            {
              $result[$code] = null;
            }
          }
          elseif ($arValue['USER_TYPE'] == 'directory')
          {
            if (!empty($arValue['VALUE']))
            {
              $ob = Info::getHlByTableName($arValue['USER_TYPE_SETTINGS']['TABLE_NAME']);
              $result[$code] = $ob->getList(array(
                'select' => array('*'),
                'filter' => array('ID' => array_values($arValue['VALUE']))
              ))->fetchAll();
            }
            else
            {
              $result[$code] = null;
            }
          }
          elseif ($arValue['PROPERTY_TYPE'] == 'L')
          {
            if ($arValue['MULTIPLE'] == 'N')
            {
              $result[$code] = array($arValue['VALUE_ENUM_ID'] => array('XML_ID' => $arValue['VALUE_XML_ID'],
                                                                        'VALUE' => $arValue['VALUE_ENUM']));
            }
            else
            {
              //@todo for multiply property
            }
          }
          elseif (isset($arValue['VALUE']))
          {
            $result[$code] = $arValue['VALUE'];
          }
        }
        else
        {
          $result[$code] = $arValue;
        }
      }
      // Сохраняем отформатированные данные
      $this->buffIblockId[$hash_data] = gzcompress(serialize($result));
    }
    elseif (isset($this->buffIblockId[$hash_data]))
    {
      //Если мы уже получали такие данные, просто вставляем их.
      $result = unserialize(gzuncompress($this->buffIblockId[$hash_data]));
    }
    return $result;
  }

  public function setClientCardId($id)
  {
    $this->client_id = $id;
  }

  public function getClientCardId()
  {
    if (!$this->client_id > 0)
    {
      $ar = $this->getById();
      $this->setClientCardId($ar['CLIENT']['VALUE']);
    }
    return $this->client_id;
  }

  /**
   * Получаем список услуг в виде строк таблицы для вставки в таблицу
   */

  protected function getTableListServices($arServices, $templateServiceName)
  {
    $i = 0;
    $tr = '';
    foreach ($arServices
             as
             $arService)
    {
      $i++;
      $tr .= "<tr>"
        . "<td align=\"right\">" . $i . "</td>"
        . "<td align=\"left\">" . str_replace('#SERVICE_NAME#', $arService['UF_NAME'], $templateServiceName) . "</td>"
        . "<td align=\"right\">" . $arService['UF_QUANTITY'] . "</td>"
        . "<td>шт.</td>"
        . "<td align=\"right\">" . number_format($arService['UF_PRICE'], 2, ',', ' ') . "</td>"
        . "<td align=\"right\">" . number_format($arService['UF_SUMM'], 2, ',', ' ') . "</td>"
        . "</tr>";
    }
    return $tr;
  }

  public static function GetAdminElementEditLink($IBLOCK_ID, $ELEMENT_ID, $arParams = array(), $strAdd = "")
  {
    if (
      (defined("CATALOG_PRODUCT") || $arParams["force_catalog"]) && !array_key_exists("menu", $arParams)
    )
    {
      $url = "clients_invoice_edit.php";
    }
    else
    {
      $url = "clients_invoice_edit.php";
    }
    $client = new Clients;

    $USER_ID = $client->getUserId($ELEMENT_ID);
    $PROFILE_ID = $client->getProfileId(array('user_id' => $USER_ID));

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    if ($ELEMENT_ID !== null)
    {
      $url .= "&ID=" . intval($ELEMENT_ID);
    }
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    $url .= "&PROFILE_ID=" . intval($PROFILE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
    {
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }
    }

    return $url . $strAdd;
  }

  /**
   * Модифицированный метод получения ссылки списка элементов
   *
   * @rerurn string
   */

  public static function GetAdminSectionListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
  {

    $url = "clients_invoice_list.php";

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
    {
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }
    }

    return $url . $strAdd;
  }

  /**
   * Модифицированный метод получения ссылки списка элементов
   *
   * @return string
   */

  public static function GetAdminElementListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
  {
    $url = "clients_invoice_list.php";

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
    {
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }
    }

    return $url . $strAdd;
  }

  public function updateProperties(array $arProperties)
  {
    Loader::includeModule('iblock');
    CIBlockElement::SetPropertyValuesEx($this->id, $this->iblock_id, $arProperties);
  }

  /**
   * Обновление массива свойств типа список по XML_ID значению варианта
   *
   * @param array $arProperties - Массив свойств array('PROPERTY_CODE|PROPERTY_ID'=>'XML_ID_VALUE');
   */
  public function updateEnumProperties(array $arProperties)
  {
    $iblock_id = $this->iblock_id;
    Loader::includeModule('iblock');
    foreach ($arProperties
             as
             $property_code
    =>
             $property_value)
    {
      $ar = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $iblock_id,
                                                        'PROPERTY_ID' => $property_code,
                                                        'EXTERNAL_ID' => $property_value))->Fetch();
      $PROPERTY_VALUES[$ar['PROPERTY_ID']] = $ar['ID'];
    }
    CIBlockElement::SetPropertyValuesEx($this->id, $iblock_id, $PROPERTY_VALUES);
  }

  /**
   * Генерирует pdf файл счета и не прикрепляет его к карточке клиента
   *
   * @param int $id ID элемента инфоблока Счета
   *
   * @return string   Возвращает путь к файлу
   */

  public function generatePdfEx($id = false,$elid = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->id = $id;
    // Получаем все данные для счета из инфоблоков
    $ar = $this->getById($id);
    $arData = $this->getFormat($ar);

	// Актуализируем цены SSL
	foreach($arData['SERVICES'] as $serviceKey=>$serviceArr) {
		if(!empty($serviceArr['UF_ELID']) || $elid) {
			if(!empty($serviceArr['UF_ELID'])) {
				$elid = $serviceArr['UF_ELID'];
			}
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "catalog_PRICE_5");
			$arFilter = Array("IBLOCK_ID"=>30, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_ELID"=>$elid);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				if(!empty($arFields['CATALOG_PRICE_5'])) {
					$arData['SERVICES'][$serviceKey]['UF_PRICE'] = $arFields['CATALOG_PRICE_5'];
					$arData['SERVICES'][$serviceKey]['UF_SUMM'] = $serviceArr['UF_QUANTITY'] * $arFields['CATALOG_PRICE_5'];
				}
			}
		}
	}
	
    $client = new Clients($arData['CLIENT']['CC_CLIENT']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();
    // Инфа о клиенте
    $arClient = $client->getById($client->getElementId());
    $type_payment = $arClient['TYPE_PAYMENT']['VALUE_XML_ID'];
    switch ($type_payment)
    {
      case 'sb_bill':
        return $this->generateSbPdf();
        break;
      case 'emoney':
        return false;
        break;
      default:
        if (!empty($arProfile['ADDRESS_FULL']))
        {
          $address = $arProfile['ADDRESS_FULL'];
        }
        else
        {
          // формирую адрес из индекса, города, и самого адреса
          $arAddress = array();
          $arAddress[] = $arProfile['POST_INDEX'];
          $arAddress[] = $arProfile['LOCATION'];
          $arAddress[] = $arProfile['ADDRESS'];
          $address = implode(', ', $arAddress);
        }
        // добавляю точку в конец Назначения платежа, если ее нет.
        $arData['DESCRIPTION'] .= strpos('.', $arData['DESCRIPTION']) == strlen($arData['DESCRIPTION']) - 1 || empty($arData['DESCRIPTION']) ? '' : '.';
        $arProfile['KPP'] = !empty($arProfile['KPP']) ? '/ ' . $arProfile['KPP'] : '';
        $contract_date = $arData['CLIENT']['DATE_CREATE'];

        Loader::includeModule('sale');
        // Формируем маркеры для pdf
        $table_services = $this->getTableListServices($arData['SERVICES'], Loc::getMessage('TEMPLATE_SERVICE_NAME', array(
              '#CONTRACT_ID#' => $arData['CLIENT']['NAME'],
              '#CONTRACT_DATE#' => FormatDateEx($contract_date, false, 'dd.mm.YYYY')
            )
          ) . " " . FormatDateEx($arData['DATE'], false, 'f_YYYY')
        );
        $arValues = array(
          'INVOICE_ID' => $arData['NUMBER'],
          'INVOICE_DATE' => FormatDateEx($arData['DATE'], false, 'd F YYYY'),
          'INVOICE_DATE_SAMPLE' => FormatDateEx($arData['DATE'], false, 'dd.mm.YYYY'),
          'CONTRACT_ID' => $arData['CLIENT']['NAME'],
          'CONTRACT_DATE' => FormatDateEx($contract_date, false, 'dd.mm.YYYY'),
          'NAME' => $arProfile['NAME'],
          'INN' => $arProfile['INN'],
          'KPP' => $arProfile['KPP'],
          'ADDRESS' => $address,
          'DESCRIPTION' => $arData['DESCRIPTION'],
          'TABLE_LIST_SERVICES' => $table_services,
          'TOTAL_SUM' => number_format($arData['SUMM'], 2, ',', ' '),
          'TOTAL_SUM_STRING' => Number2Word_Rus($arData['SUMM']),
        );
        // формируем pdf
        $pdf = new Pdf;
        $pdf->setMarkers($arValues);
        $path_module = Info::getPath();
        $pdf->setTemplateFromFolder($path_module . '/templates/pdf/invoice/');

        $date_name = FormatDateEx($arData['DATE'], false, 'f_YYYY');
        $arFilename = array($arProfile['NAME'],
                            $arData['CLIENT']['NAME'],
                            $date_name);
        //        $filename = implode('_', $arFilename).'.pdf';
        $filename = 'Счет по договору №' . $arData['CLIENT']['NAME']
          . ' от ' . FormatDateEx($contract_date, false, 'dd.mm.YYYY')
          . ', период ' . FormatDateEx($arData['DATE'], false, 'f YYYY')
          . ' ' . $arProfile['NAME'] . '.pdf';
        $filename = str_replace('"', '', $filename);
        $arParams = array(
          'path' => '/upload/clients/pdf/invoices/tmp/',
          'filename' => $filename
        );
        $temp_file = $_SERVER['DOCUMENT_ROOT'] . $arParams['path'] . $arParams['filename'];
        $file = $pdf->createPdf($arParams);

        $save_path = '/clients/pdf/invoices/';
        $arFile = CFile::MakeFileArray($file['PATH']);
        $arFile['name'] = $filename;
        $arFile['MODULE_ID'] = 'clients';
        $file_id = CFile::SaveFile($arFile, $save_path);
        unlink($temp_file);
        return $file_id;
        break;
    }
  }

  /**
   * Генерирует pdf файл квитанции и не прикрепляет его к карточке клиента
   *
   * @param int $id ID элемента инфоблока Счета
   *
   * @return string   Возвращает ID файла
   */

  public function generateSbPdfEx($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->id = $id;
    // Получаем все данные для счета из инфоблоков
    $ar = $this->getById($id);
    $arData = $this->getFormat($ar);
    $client = new Clients($arData['CLIENT']['CC_CLIENT']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();
    if (!empty($arProfile['ADDRESS_FULL']))
    {
      $address = $arProfile['ADDRESS_FULL'];
    }
    else
    {
      // формирую адрес из индекса, города, и самого адреса
      $arAddress = array();
      $arAddress[] = $arProfile['POST_INDEX'];
      $arAddress[] = $arProfile['LOCATION'];
      $arAddress[] = $arProfile['ADDRESS'];
      $address = implode(', ', $arAddress);
    }
    // добавляю точку в конец Назначения платежа, если ее нет.
    $arData['DESCRIPTION'] .= strpos('.', $arData['DESCRIPTION']) == strlen($arData['DESCRIPTION']) - 1 || empty($arData['DESCRIPTION']) ? '' : '.';
    $arProfile['KPP'] = !empty($arProfile['KPP']) ? '/ ' . $arProfile['KPP'] : '';
    $contract_date = $arData['CLIENT']['DATE_CREATE'];

    $ar_temp = explode(',', number_format($arData['SUMM'], 2, ',', ' '));
    $kopeiki = $ar_temp[1];


    Loader::includeModule('sale');
    // Формируем маркеры для pdf
    $arValues = array(
      'INVOICE_ID' => $arData['NUMBER'],
      'INVOICE_DATE' => FormatDateEx($arData['DATE'], false, 'd F YYYY'),
      'CONTRACT_ID' => $arData['CLIENT']['NAME'],
      'CONTRACT_DATE' => FormatDateEx($contract_date, false, 'dd.mm.YYYY'),
      'NAME' => $arProfile['NAME'],
      'INN' => $arProfile['INN'],
      'KPP' => $arProfile['KPP'],
      'ADDRESS' => $address,
      'DESCRIPTION' => $arData['DESCRIPTION'],
      'TOTAL_SUM_RUB' => str_replace(' ', '&nbsp;', number_format($arData['SUMM'], 0, ',', ' ')),
      'TOTAL_SUM_KOP' => $kopeiki,
    );
    // формируем pdf
    $pdf = new Pdf;
    $pdf->setMarkers($arValues);
    $path_module = Info::getPath();
    $pdf->setTemplateFromFolder($path_module . '/templates/pdf/invoice-sb/');

    $date_name = FormatDateEx($arData['DATE'], false, 'f_YYYY');
    $arFilename = array($arProfile['NAME'],
                        $arData['CLIENT']['NAME'],
                        $date_name);
//        $filename = implode('_', $arFilename).'.pdf';
    $filename = 'Квитанция к договору №' . $arData['CLIENT']['NAME']
      . ' от ' . FormatDateEx($contract_date, false, 'dd.mm.YYYY')
      . ', период ' . FormatDateEx($arData['DATE'], false, 'f YYYY')
      . ' ' . $arProfile['NAME'] . '.pdf';
    $filename = str_replace('"', '', $filename);
    $arParams = array(
      'path' => '/upload/clients/pdf/invoices/',
      'filename' => $filename
    );
    $temp_file = $_SERVER['DOCUMENT_ROOT'] . $arParams['path'] . $arParams['filename'];
    $file = $pdf->createPdf($arParams);
    $save_path = '/clients/pdf/invoices/';
    $arFile = CFile::MakeFileArray($file['PATH']);
    $arFile['name'] = $filename;
    $arFile['MODULE_ID'] = 'clients';
    $file_id = CFile::SaveFile($arFile, $save_path);
    unlink($temp_file);
    return $file_id;
  }

  /**
   * @deprecated
   * @uses setStatus()
   * Устанавливает статус счета Оплачен
   */

  public function paid($id = false)
  {

    $id = intval($id) > 0 ? $id : $this->getId();
	$this->setStatus(array('id' => $id,
                           'status' => 'paid'));
    if (Loader::includeModule('ncsupport.exchange'))
    {

      $cCardId = $this->getClientCardId();
	  
	  //ВЫБИРАЕМ ДАННЫЕ ИЗ СЧЕТА, КОТОРЫЙ ОПЛАЧЕН
	  
		$arParams = array(
			'filter' => array(
				'ACTIVE' => 'Y',
				'ID' => $cCardId
			)
		);

		$client = \Itin\Depohost\Clients::getList($arParams);

		if(!empty($client)) {
			
			$clientData = current($client);
			
			//Получаем информацию о счетах клиента
			$arParams = array('filter' => array('=PROPERTY_CLIENT' => $clientData['ID'], 'ID' => $id),'order' => array('ID' => 'DESC'));
			$invoice = new \Itin\Depohost\Invoices();
			$userInvoices = current($invoice->getList($arParams));
			$invoiceServices = $userInvoices['SERVICES']['VALUE'];
		}

		//ПОЛУЧАЕМ ВСЕ УСЛУГИ, КОТОРЫЕ ЗАКАЗАНЫ У КЛИЕНТА
		$arServicePaying = Clients::getCSList($cCardId);
		
		foreach ($arServicePaying as $servKey=>$serv) {
			if(in_array($servKey,$invoiceServices)) {
				if(empty($serv['UF_ELID']) && !empty($serv["UF_PROPERTIES"][1])) {
					$domain = str_replace("Домен: ", "", $serv["UF_PROPERTIES"][1]);
				
					$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(4)->fetch(); 

					$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); 
					$entity_data_class = $entity->getDataClass(); 

					$rsData = $entity_data_class::getList(array(
					   "select" => array("*"),
					   "order" => array("ID" => "ASC"),
					   "filter" => array("UF_INVOICE"=>$serv['ID'],"UF_USER"=>$serv['UF_CLIENT'],"UF_DOMAIN"=>$domain)
					));

					while($arData = $rsData->Fetch()) {
						if(!empty($arData['UF_ELID'])) {
							$serv['UF_ELID'] = $arData['UF_ELID'];
							$serv['UF_ISP'] = 1;
						}
					}
				}
			}
			if(!empty($serv['UF_ELID']) && in_array($servKey,$invoiceServices)) {
				if ($serv['UF_CATALOG_XML_ID'] === 'u9' && $serv['UF_ISP'] != 1)
				{
				  $client = new Clients;
				  $ar_client = $client->getById($cCardId);
				  $id_client = $client->getProfileId(array('user_id' => $ar_client['CC_CLIENT']['VALUE']));
				  $pr_client = $client->getProfile($id_client);

				  $serv['UF_SSL_TECH_MAIL'] = $pr_client['EMAIL']['VALUE'];
				  $serv['UF_SSL_TECH_PHONE'] = $pr_client['PHONE']['VALUE'];
				  $serv['UF_SSL_TECH_FTTL'] = 'Manager';
				  $serv['UF_SSL_TECH_LNAME'] = $this->str2url($pr_client['L_NAME']['VALUE']);
				  $serv['UF_SSL_TECH_FNAME'] = $this->str2url($pr_client['F_NAME']['VALUE']);
				  $serv['UF_SSL_OP_ORGNAME'] = $this->str2url($pr_client['NAME']['VALUE']);
				  $serv['UF_SSL_OP_ORGUTNAME'] = $this->str2url('Администрация');
				  $serv['UF_SSL_MAIL_ADDR'] = $pr_client['EMAIL']['VALUE'];
				  //UF_SSL_PRICELIST

				  $serv['UF_SSL_ADM_FNAME'] = $this->str2url($pr_client['F_NAME']['VALUE']);
				  $serv['UF_SSL_ADM_LNAME'] = $this->str2url($pr_client['L_NAME']['VALUE']);
				  $serv['UF_SSL_ADM_JTTL'] = 'Manager';
				  $serv['UF_SSL_APPRV_MAIL'] = $pr_client['EMAIL']['VALUE'];
				  $serv['UF_SSL_ADM_PHONE'] = $pr_client['PHONE']['VALUE'];

				  $arLocs = \CSaleLocation::GetByID($pr_client['LOCATION']['VALUE'], LANGUAGE_ID);
				  
				  $serv['UF_SSL_FIELD_COUNTRY'] = $this->str2url($arLocs['COUNTRY_NAME']);
				  $serv['UF_SSL_FIELD_CITY'] = $this->str2url($arLocs['CITY_NAME']);
				  $serv['UF_SSL_FIELD_REGION'] = $this->str2url((!empty($arLocs['REGION_NAME_ORIG'])?$arLocs['REGION_NAME_ORIG']:$arLocs['CITY_NAME']));
				  $serv['UF_SSL_FIELD_INDEX'] = $pr_client['POST_INDEX']['VALUE'];
				  $serv['UF_SSL_FIELD_ADDRESS'] = $this->str2url($pr_client['ADDRESS_FULL']['VALUE']);

				  $SSL = (new \NcSupport\Exchange\Import\SSL())->certificateOrder($serv, true);
				 
				  //записываем факт передачи заказа в ISP
				  if (!empty($SSL) && $SSL)
				  {
					Clients::getCSTransferServices($serv['ID'], 1);
				  }

				}
				elseif ($serv['UF_CATALOG_XML_ID'] === 'u9' && $serv['UF_ISP'] == 1)
				{
				  $res = (new \NcSupport\Exchange\Import\SSL())->certificateProlong($serv, true);
				  print_r($res);
				}
			}
      }

    }
  }

  /**
   *
   * Транслитерация для услуг SSL
   */
  public function rus2translit($string)
  {
    $converter = array(
      'а' => 'a', 'б' => 'b', 'в' => 'v',
      'г' => 'g', 'д' => 'd', 'е' => 'e',
      'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
      'и' => 'i', 'й' => 'y', 'к' => 'k',
      'л' => 'l', 'м' => 'm', 'н' => 'n',
      'о' => 'o', 'п' => 'p', 'р' => 'r',
      'с' => 's', 'т' => 't', 'у' => 'u',
      'ф' => 'f', 'х' => 'h', 'ц' => 'c',
      'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
      'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
      'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

      'А' => 'A', 'Б' => 'B', 'В' => 'V',
      'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
      'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
      'И' => 'I', 'Й' => 'Y', 'К' => 'K',
      'Л' => 'L', 'М' => 'M', 'Н' => 'N',
      'О' => 'O', 'П' => 'P', 'Р' => 'R',
      'С' => 'S', 'Т' => 'T', 'У' => 'U',
      'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
      'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
      'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
      'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
    );
    return strtr($string, $converter);
  }

  /**
   *
   * Транслитерация для услуг SSL
   */
  public function str2url($str)
  {
	$arParams = array("replace_space"=>"-","replace_other"=>"-","change_case"=>false);
	$trans = \Cutil::translit($str,"ru",$arParams);
    return $trans;
	
    /*$str = $this->rus2translit($str);
    $str = strtolower($str);
    $str = preg_replace('~[^-a-z0-9_]+~u', ' ', $str);
    $str = trim($str, "-");
    return $str;*/
  }

  /**
   * @deprecated
   * @uses setStatus()
   * Устанавливает статус счета Не оплачен
   */

  public function unPaid($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $this->setStatus(array('id' => $id,
                           'status' => 'no-paid'));
  }

  /**
   * Устанавливает статус счета
   *
   * @params array $param     Массив параметров. Ключи массива:
   *                          'id' - ID элемента инфоблока Счета
   *                          'status' - XML_ID статуса <paid|no-paid>
   */

  public function setStatus($param)
  {

    $id = intval($param['id']) > 0 ? $param['id'] : $this->getId();
    Loader::includeModule('iblock');
    $ob = new CIBlockPropertyEnum;
    $res = $ob->GetList(array('SORT' => 'ASC'), array('PROPERTY_ID' => 'STATUS',
                                                      'EXTERNAL_ID' => $param['status'],
                                                      'IBLOCK_ID' => Info::getIblockId('invoices')));
    if ($ar = $res->Fetch())
    {
      $status_id = $ar['ID'];
      $this->updateProperties(array('STATUS' => $status_id));

    }
    else
    {
      return false;
    }
  }

  /**
   * Получение информации о статусе счета
   *
   * @return array
   */

  public function getStatus($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $iblock_id = Info::getIblockId('invoices');
    Loader::includeModule('iblock');
    $ob = new CIBlockElement;
    $res = $ob->GetProperty($iblock_id, $id, array(), array('CODE' => 'STATUS',
                                                            'EMPTY' => 'N'));
    if ($ar = $res->Fetch())
    {
      return $ar;
    }
    else
    {
      return false;
    }
  }

  /**
   * Получение массива соотношений XML_ID -> ID значения свойства статуса
   *
   * @return array
   */

  public static function getRatioStatus()
  {
    $iblock_id = Info::getIblockId('invoices');
    Loader::includeModule('iblock');
    $ob = new CIBlockPropertyEnum;
    $res = $ob->GetList(array(), array('IBLOCK_ID' => $iblock_id,
                                       'PROPERTY_ID' => 'STATUS'));
    while ($ar = $res->Fetch())
    {
      $arVariants[$ar['XML_ID']] = $ar;
    }
    return $arVariants;
  }

  /**
   * Отправка маила об зачислении оплаты
   *
   * @param int $id ID элемента инфоблока Счета
   *
   * @return null
   */

  public function sendToMailOnPaid($id = false)
  {
    Loader::includeModule('iblock');
    $client = new Clients;
    $id = intval($id) > 0 ? intval($id) : $this->getId();
    $ob = new CIBlockElement;
    $res = $ob->GetList(array(), array('ID' => $id), false, false, array('ID',
                                                                         'IBLOCK_ID',
                                                                         'PROPERTY_CLIENT',
                                                                         'PROPERTY_NUMBER',
                                                                         'PROPERTY_DATE',
                                                                         'PROPERTY_SUMM'));
    if ($ar = $res->Fetch())
    {


      $client_card_id = $ar['PROPERTY_CLIENT_VALUE'];
      $profile_id = $client->getProfileId(array('element_id' => $client_card_id));
      $arProfile = $client->getProfile($profile_id);
      $arClient = $client->getList(array(
        'select' => array('CC_ABONPLATA',
                          'DATE_CREATE',
                          'NAME',),
        'filter' => array('=ID' => $client_card_id),
      ));
      $arClient = current($arClient);
    }
    $arFieldsEvent = array(
      'NAME' => $arProfile['NAME']['VALUE'],
      'NUMBER' => $ar['PROPERTY_NUMBER_VALUE'],
      'DATE' => $ar['PROPERTY_DATE_VALUE'],
      'ABONPLATA' => number_format($arClient['CC_ABONPLATA']['VALUE'], 0, ',', ' '),
      'SUMM' => number_format($ar['PROPERTY_SUMM_VALUE'], 0, ',', ' '),
      'NUMBER_CONTRACT' => $arClient['NAME'],
      'DATE_CONTRACT' => FormatDateEx($arClient['DATE_CREATE'], false, 'dd.mm.YYYY'),
    );
    $client->sendMail('CLIENT_INVOICE_PAID', $arFieldsEvent);
//        return \CEvent::Send('CLIENT_SEND_INVOICE',$arSites,$arFieldsEvent);
  }

  /**
   * Указывает был ли раньше установлен статус Оплачен
   *
   * @param int $id ID элемента инфоблока Счета
   *
   * @return boolean
   */

  public function isChangeStatus($id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $iblock_id = Info::getIblockId('invoices');
    Loader::includeModule('iblock');
    $ob = new CIBlockElement;
    $res = $ob->GetProperty($iblock_id, $id, array(), array('CODE' => 'CHANGE_STATUS',
                                                            'EMPTY' => 'N'));
    $ar = $res->Fetch();
    // Каждый раз меняем флаг Есть ли есть неоплаченные счета
    $client_id = $this->getClientCardId();
    Clients::setNoPaid($client_id);

    return $ar['VALUE'] == 'changed' ? true : false;
  }

  /**
   * Устанавливает флаг смены статуса
   */

  public function setChanged($value, $id = false)
  {
    $id = intval($id) > 0 ? $id : $this->getId();
    $iblock_id = Info::getIblockId('invoices');
    Loader::includeModule('iblock');
    $ob = new CIBlockElement;
    $changeStatus = ((bool)$value === true) ? 'changed' : 'no-changed';
    $ob->SetPropertyValuesEx($id, $iblock_id, array('CHANGE_STATUS' => $changeStatus));
  }

  public function payPaymaster($invoice_id = false)
  {
    global $USER;
    $invoice_id = intval($invoice_id) > 0 ? $invoice_id : $this->getId();

    $merchant_id = Option::get('clients', 'paymaster_merchant_id');
    $bTest = Option::get('clients', 'paymaster_test', 'N');

    $arInvoice = $this->getById();
    $arData = $this->getFormat($arInvoice);
    if ($USER->GetID() != (int)$arData['CLIENT']['CC_CLIENT'])
    {
      die('Access denied');
    }
    $client = new Clients($arData['CLIENT']['CC_CLIENT']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();

    $contract_date = $arData['CLIENT']['DATE_CREATE'];


    $invoice_name = 'Счет по договору №' . $arData['CLIENT']['NAME']
      . ' от ' . FormatDateEx($contract_date, false, 'dd.mm.YYYY')
      . ', период ' . FormatDateEx($arData['DATE'], false, 'f YYYY')
      . ', ' . $arProfile['NAME'];

    $sum = $arData['SUMM'];

    $params = array(
      'LMI_MERCHANT_ID' => $merchant_id,
      'LMI_PAYMENT_AMOUNT' => $sum,
      'LMI_CURRENCY' => 'RUB',
      'LMI_PAYMENT_NO' => $invoice_id,
      'LMI_PAYMENT_DESC_BASE64' => base64_encode($invoice_name),
      'LMI_SIM_MODE' => '0',
    );

    if ($bTest == 'Y')
    {
      $params['LMI_SIM_MODE'] = 2;
    }
    $params = http_build_query($params);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://paymaster.ru/Payment/Init');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);

    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
    curl_exec($curl);
    $url = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_close($curl);
    LocalRedirect($url);
  }

  public function getDefaultValueProperties()
  {

    \Bitrix\Main\Loader::includeModule('iblock');

    $ob = new \Bitrix\Iblock\PropertyTable;

    $arParams = array(
      'select' => array('*'),
      'filter' => array('IBLOCK_ID' => $this->iblock_id, 'ACTIVE' => 'Y'),
      'order' => array('SORT' => 'ASC')
    );
    $res = $ob->getList($arParams);
    $values = array();

    while ($ar = $res->fetch())
    {
      $code = $ar['CODE'];
      $values[$code] = $ar['DEFAULT_VALUE'];
    }

    return $values;

  }
}
