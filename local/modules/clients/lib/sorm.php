<?php
/**
 * @package Itin\Depohost
 * @subpackage Sorm
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * @version 0.0.1
 * Date: 15.08.20
 * Time: 13:56
 */


namespace Itin\Depohost;


use Bitrix\Main\Config\Option;
use Bitrix\Main\IO\File;
use Bitrix\Main\Loader;

class Sorm
{
  protected $separate = '|';
  protected $endLine = "\n\r";
  protected $version = '1.2.0';
  public $arHeaders = [
    'msisdn' => 'номер телефона в международном формате',
    'imsi' => 'идентификатор мобильного абонента',
    'iccid' => 'идентификатор SIM-карты',
    'contract_date' => 'дата заключения контракта',
    'last_change_date' => 'дата изменения записи по абоненту',
    'end_contract_date' => 'дата закрытия контракта',
    'abonent_name' => 'имя абонента',
    'birthday' => 'дата рождения',
    'document' => 'документ (паспорт, (серия номер))',
    'registration_address' => 'адрес регистрации абонента',
    'invoice_address' => 'адрес доставки счетов',
    'installation_address' => 'адрес установки, где реально установлен телефон',
    'contact' => 'контактное лицо',
    'contact_info' => 'контактная информация (телефон, e-mail и т.д.)',
    'inn' => 'ИНН',
    'tariff' => 'название тарифа',
    'diler' => 'название дилера, который продал SIM-карту конечному абоненту',
  ];

  public $hiddenHeaders = [
    'msisdn', 'imsi', 'iccid', 'last_change_date', 'tariff', 'diler'
  ];

  function __construct()
  {
  }

  public function createAndSendUpload()
  {
    try {
      $this->createUpload();
      $this->sendToEmail();
      $this->deleteFile();
    }catch (\Exception $e) {
      // send error to email
    }
  }

  /**
   * @return void
   * @throws \Bitrix\Main\ArgumentNullException
   * @throws \Bitrix\Main\ArgumentOutOfRangeException
   */
  public function createUpload() {

    $this->createFile();
    $this->addHeaderToFile();
    $filter = [];
    if(Option::get('clients', 'sorm_only_active', 'N') === 'Y') {
      $filter['ACTIVE'] = 'Y';
    }
    $arData = $this->mapData($this->getItems($filter));

    foreach ($arData as $arRow)
    {
      $this->addRowDataToFile($arRow);
    }
  }


  /**
   * @return void
   * @throws \Bitrix\Main\ArgumentException
   * @throws \Bitrix\Main\ObjectPropertyException
   * @throws \Bitrix\Main\SystemException
   */
  public function sendToEmail()
  {
    $ob = new \Bitrix\Main\SiteTable;
    $res = $ob->getList();
    $arSites = array();
    while ($ar = $res->fetch())
    {
      $arSites[] = $ar['LID'];
    }
    $arFields = ['ATTACH' => $this->getFilePath()];
    \CEvent::Send('SORM_UPLOAD', $arSites, $arFields);
  }

  private function mapData($arItems)
  {
    return array_map(function ($arItem) {
      return $this->mapItem($arItem);
    }, $arItems);
  }

  public function mapItem($arItem)
  {
    $date = MakeTimeStamp($arItem['CREATED_DATE'], 'YYYY.MM.DD');
    $arItem['SORM_END_DATE'] = $arItem['SORM_END_DATE'] ? MakeTimeStamp($arItem['SORM_END_DATE'], 'DD.MM.YYYY') : '';
    $arItem['SORM_BIRTHDAY'] = $arItem['SORM_BIRTHDAY'] ? MakeTimeStamp($arItem['SORM_BIRTHDAY'], 'DD.MM.YYYY') : '';

    return [
      'msisdn' => '',
      'imsi' => '',
      'iccid' => '',
      'contract_date' => Sorm::formatDate($date),
      'last_change_date' => '',
      'end_contract_date' => Sorm::formatDate($arItem['SORM_END_DATE']),
      'abonent_name' => $arItem['USER']['NAME'],
      'birthday' => Sorm::formatDate($arItem['SORM_BIRTHDAY']),
      'document' => $arItem['SORM_DOCUMENT'],
      'registration_address' => $arItem['REGISTRATION_ADDRESS'],
      'invoice_address' => $arItem['INVOICE_ADDRESS'],
      'installation_address' => '',
      'contact' => $arItem['CONTACT_FIO'],
      'contact_info' => $arItem['USER']['EMAIL'],
      'inn' => $arItem['INN'],
      'tariff' => '',
      'diler' => '',
    ];
  }

  public function getItem($id)
  {
    $arItems = $this->getItems(['=ID' => $id]);

    if (count($arItems))
    {
      return $arItems[0];
    }

    return false;
  }

  private function getItems($filters = [])
  {
    Loader::includeModule('iblock');
    $filters['IBLOCK_ID'] = Info::getIblockId('ccards');
    $ob = new \CIBlockElement;
    $res = $ob->GetList(
      ['ID' => 'ASC'],
      $filters,
      false,
      false,
      ['ID', 'NAME', 'CREATED_DATE', 'PROPERTY_CC_CLIENT', 'PROPERTY_SORM_END_DATE', 'PROPERTY_SORM_BIRTHDAY',
       'PROPERTY_SORM_DOCUMENT']
    );
    $arClients = [];

    while ($ar = $res->Fetch())
    {
      $userId = $ar['PROPERTY_CC_CLIENT_VALUE'];
      $client = new Clients($userId);
      $arProfile = $client->getProfile();
      $arClient = [
        'ID' => $ar['ID'],
        'CREATED_DATE' => $ar['CREATED_DATE'],
        'USER_ID' => $userId,
        'SORM_END_DATE' => $ar['PROPERTY_SORM_END_DATE_VALUE'],
        'SORM_BIRTHDAY' => $ar['PROPERTY_SORM_BIRTHDAY_VALUE'],
        'SORM_DOCUMENT' => $ar['PROPERTY_SORM_DOCUMENT_VALUE'],
      ];
      $arClient['INVOICE_ADDRESS'] = $arProfile['ADDRESS']['VALUE'];

      $arClient['CONTACT_FIO'] = trim(implode(' ', [$arProfile['L_NAME']['VALUE'], $arProfile['F_NAME']['VALUE'],
                                                    $arProfile['P_NAME']['VALUE']]));

      switch ($client->getPersonTypeId())
      {
        case '1':   //Физ лицо
          $arClient['REGISTRATION_ADDRESS'] = $arProfile['ADDRESS']['VALUE'];
          $arClient['INN'] = '';
          break;
        case '2':   // Юр лицо
          $arClient['REGISTRATION_ADDRESS'] = $arProfile['ADDRESS_FULL']['VALUE'];
          $arClient['INN'] = $arProfile['INN']['VALUE'];
          break;
        case '3':   // ИП
          $arClient['REGISTRATION_ADDRESS'] = $arProfile['ADDRESS']['VALUE'];
          $arClient['INN'] = $arProfile['INN']['VALUE'];
          break;
      }
      $arClients[] = $arClient;
    }

    $arUserIds = array_column($arClients, 'USER_ID');
    $by = 'id';
    $order = 'asc';
    $res = \CUser::GetList($by, $order, ['ID' => implode('|', $arUserIds)], ['FIELDS' => ['ID', 'NAME', 'EMAIL']]);
    while ($ar = $res->Fetch())
    {
      $index = array_search($ar['ID'], $arUserIds);
      $arClients[$index]['USER'] = $ar;
    }

    return $arClients;
  }

  public function getFileName()
  {
    $date = FormatDate('Y-m-d');
    $agentId = Option::get('clients', 'sorm_agent_id', '3390');
    $regionId = Option::get('clients', 'sorm_region_id', 'CFO');

    return "{$agentId}_{$regionId}_{$date}_v{$this->version}.csv";
  }

  public function getFilePath()
  {
    return $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $this->getFileName();
  }

  private function createFile()
  {
    File::putFileContents($this->getFilePath(), '');
  }

  private function deleteFile()
  {
    File::deleteFile($this->getFilePath());
  }

  private function addHeaderToFile()
  {
    $this->addRowDataToFile(array_keys($this->arHeaders));
  }

  private function addRowDataToFile($rowData)
  {
    $safeData = array_map(function ($val) {
      return str_replace([$this->separate, $this->endLine], '', $val);
    }, array_values($rowData));

    $formatData = implode($this->separate, $safeData) . $this->endLine;
    File::putFileContents($this->getFilePath(), $formatData, File::APPEND);
  }

  /**
   * @param $timestamp - Optional
   * @return string
   */
  public static function formatDate($timestamp)
  {
    return date('c', $timestamp);
  }

}