<?php
namespace Itin\Depohost\Util;
/**
 * Класс для создания всяких элементов интерфейса админки
 *
 * @author Aleksandr Terentev
 */
class AdminInterface
{
    /*
     * Возвращает html поля типа чекбокс
     * 
     * @params array $arParams  Массив параметров
     *      'id'        string          ID поля
     *      'content'   string          Контент label
     *      'required'  boolean         Обязательное поле
     *      'value'     array | string  Варианты значений
     *      'checked'   boolean         Стоит флажок
     *      'params'    array           Доп аттрибуты
     *      'disabled'  boolean         Неактивное
     *      'align'     string          Расположение чекбокса <left|right>
     */
    public static function getCheckBoxField($arParams)
    {
        extract($arParams);
        if (is_array($value))
        {
            $html = '<input type="hidden" name="'.$id.'" value="'.htmlspecialcharsbx($value[1]).'">
                <input type="checkbox" name="'.$id.'" value="'.htmlspecialcharsbx($value[0]).'"'.($checked? ' checked': '').($disabled? ' disabled': '');
            $hidden = '<input type="hidden" name="'.$id.'" value="'.htmlspecialcharsbx($checked? $value[0]: $value[1]).'">';
        }
        else
        {
            $html = '<input type="checkbox" name="'.$id.'" value="'.htmlspecialcharsbx($value).'"'.($checked? ' checked': '').($disabled? ' disabled': '');
            $hidden = '<input type="hidden" name="'.$id.'" value="'.htmlspecialcharsbx($value).'">';
        }

        foreach($params as $param)
            $html .= ' '.$param;
        $html .= '>';
        $label = $content;
        if ($required)
        {
            $label = '<span class="adm-required-field">'.$label.'</span>';
        }
        if ($align=='left')
        {
            $html = '<tr><td width="10">'.$html.'</td><td>'.$label.'</td></tr>';
        }
        else
        {
            $html = '<tr><td>'.$label.'</td><td width="10">'.$html.'</td></tr>';
        }
            
        
        return self::wrapTable($html);
    }
    /*
     * Обрачивает в теги таблицы
     */
    private static function wrapTable($html)
    {
        return "<table>"
            .$html
            . "</table>";
    }
    /*
     * Возврашает 
     */
}
