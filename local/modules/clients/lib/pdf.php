<?php
namespace Itin\Depohost;
use \Bitrix\Main\Loader as Loader;
use Bitrix\Main\IO;

/**
 * Работа с pdf
 *
 * @author  Aleksandr Terentev
 * 
 * @uses    class WkHtmlToPdf   Для генерации pdf
 * @uses    class CBXVirtualIo  Для работы с файлами и папками
 */
class Pdf
{
    /* @var object  Объект класса генерации pdf из html*/
    private $ob_pdf;
    /* @var object  Объект для работы с файлами и папками*/
    private $io;
    /* @var array   Массив путей с именем файлов к html шаблонам страниц */
    private $arPage;
    /* @var array   Массив маркеров */
    private $arValues;

    function __construct()
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/clients/include/WkHtmlToPdf.php");
        if (defined('ADMIN_SECTION') && ADMIN_SECTION===true)
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
        }
        else
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        }
        $this->ob_pdf = new \WkHtmlToPdf;
        
        $this->io = \CBXVirtualIo::GetInstance();
    }
    /*
     * Сканирует папку и достает из нее шаблоны файлов
     * файлы должны иметь расширение .tpl.html
     * 
     * @param string $path  -   Относительный путь папки
     * 
     * @return bool|array
     */
    public function setTemplateFromFolder($path)
    {
        $io = $this->io;
        $abs_path = IO\Path::isAbsolute($path) ? $path : $io->RelativeToAbsolutePath($path);
        if($io->DirectoryExists($abs_path))
        {
            $dir = $io->GetDirectory($abs_path);
            $arChildren = $dir->GetChildren();
            $arPage = array();
            foreach ($arChildren as $child)
            {
                if (!$child->IsDirectory() && $child->IsReadable() && strpos($child->GetName(), '.tpl.html')!==false)
                {
                    $arPage[] = $child->GetPathWithName();
                }
            }
        }
        else
        {
            $GLOBALS['APPLICATION']->ThrowException('Отсутствует папка','pdf');
        }
        if (empty($arPage))
        {
            return false;
        }
        else
        {
            $this->arPage = $arPage;
            return $arPage;
        }
            
    }
    /*
     * Создание pdf файла из html шаблонов страниц
     * 
     * @param array $arParams                           Массив параметров метода. Необязательный параметр
     * @example $arParams  = array(
     *          'pages' => array(
     *              '/path1/filename_page_1.html',
     *              '/path1/filename_page_2.html')      Массив файлов html шаблонов страниц
     *          'path' => '/upload/pdf/',               Папка в которой будет сохраняться файл
     *          'filename' => 'file.pdf'                Имя файла в который будет сохранен файл
     *          'arValues' => array('MARKER'=>'VALUE')  Полный массив значений маркеров
     *      )
     * 
     * @return array                                    'SRC'   Относительный путь с именем созданного файла
     *                                                  'PATH'  Абсолютный путь
     */
    public function createPdf($arParams)
    {
        $io = $this->io;
        $pdf = $this->ob_pdf;
        
        if (empty($arParams['pages']))
        {
            $arPage = $this->arPage;
        }
        else
        {
            $arPage = array();
            if (is_array($arParams['pages']))
            {
                foreach ($arParams['pages'] as $page)
                {
                    $arPage[] = $io->RelativeToAbsolutePath($page);
                }
            }
            else
            {
                $arPage[] = $io->RelativeToAbsolutePath($arParams['pages']);
            }
        }
        if (empty($arPage))
        {
            $GLOBALS['APPLICATION']->ThrowException('Отсутствуют шаблоны страниц для генерации','pdf');
        }
        else
        {
            $this->arPage = $arPage;            
            $path_relative = empty($arParams['path']) ? '/upload/pdf/' : $arParams['path'];
            $path = $io->RelativeToAbsolutePath($path_relative);
            $filename = empty($arParams['filename']) ? 'file_'.time().rand(1, 100).'.pdf' : $arParams['filename'];
            $arValues = !empty($arParams['arValues']) ? $arParams['arValues'] : $this->arValues;
            
            $this->arValues = $arValues;            
            sort($arPage);
            foreach ($arPage as $page)
            {
                $file = $io->GetFile($page);
                $file_content = $file->GetContents();
                if ($file_content!==null)
                {
                    if (is_array($arValues))
                    {
                        $file_content = $this->replaceMarkers(array('content'=>$file_content,'arValues' => $arValues));
                    }
                        
                    $pdf->addPage($file_content);
                }                
            }
            if (!$io->DirectoryExists)
            {
                $io->CreateDirectory($path);
            }
            

            $pathWithFilename = $path.DIRECTORY_SEPARATOR.$filename;

            if($pdf->saveAs($pathWithFilename))
            {

                return array(
                    'SRC' => $path_relative.$filename,
                    'PATH' => $pathWithFilename
                );
            }
            else
            {
                return false;
            }
            
        }
    }
    /*
     * Устанавливает массив маркеров
     * 
     * @param array $arValues       Полный массив значений маркеров
     *                              'arValues' => array('MARKER'=>'VALUE')
     * 
     * @return bool
     */
    public function setMarkers($arValues)
    {
        if (is_array($arValues))
        {
            $this->arValues = $arValues;
            return true;
        }
        else
        {
            return false;
        }
    }
    /*
     * Заменяет маркеры вида #MARKER# на элементы с одноименными ключами
     * 
     * @param array $arParams
     * @example $arParams = array(
     *          'content' => '<content with marker #KEY#>',
     *          'arValues' => array('KEY' => 'VALUE'); 
     *      )
     * 
     * @return string
     */    

    public function replaceMarkers($arParams)
    {
        $content = $arParams['content'];
        $arValues = !empty($arParams['arValues']) ? $arParams['arValues'] : $this->arValues;
        
        foreach ($arValues as $marker => $value)
        {
            $value = empty($value) ? '' : $value;   // Чтобы не остались в pdf не замененные маркеры
            $value = is_array($value) ? implode(' / ', $value) : $value;    // и массивы в pdf нам не нужны
            
            $content = str_replace('#'.$marker.'#', $value, $content);
        }
        return $content;
    }
    /*
     * Генерирует двухколоночную таблицу
     * 
     * @param array $arFields   Массив полей вида array('NAME'=>'Имя поля','VALUE'=>'Значение поля',=>'STRONG'=>true)
     */
    public static function getHtmlSimpleTable($arFields)
    {
        $html = "<table>";
        
        foreach ($arFields as $key => $arField)
        {
            $html_value = $arField['STRONG'] === true 
                    ? '<th class="second">'.$arField['VALUE'].'</th>'
                    : '<td class="second">'.$arField['VALUE'].'</td>';
            $html .='<tr>'
                        . '<td class="first">'
                        . $arField['NAME']
                        . '</td>'
                        . $html_value
                    . '</tr>';
        }
        $html .= "</table>";
        return $html;
    }
}
