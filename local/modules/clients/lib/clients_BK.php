<?php
/** work with Clients
 * @package itin
 * @subpackage depohost
 * @author Alexandr Terentev
 */
namespace Itin\Depohost;

use Bitrix\Main;
use Bitrix\Iblock;
use CIBlockSectionPropertyLink;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class Clients
{
  /*** @var int */
  protected $user_id;
  /*** @var int */
  protected $profile_id;
  /*** @var array массив ID групп которые относятся к клиентам */
  protected $arGroup;
  /*** @var array массив ошибок */
  protected $errors; // @var
  /*** @var int ID типа плательщик */
  protected $person_type_id;
  /*** @var int $iblock_id ID инфоблока карточек клиента */
  protected $iblock_id;
  /*** @var array  Массив полей профиля покупателя */
  protected $arPropfile;
  /*** @var int    ID элемента Карточки клиента */
  protected $element_id;
  /*** @var string Номер договора */
  protected $contract_id;
  protected $invoices;

  /**
   * Класс для работы с клиентами
   *
   * @param integer $user_id ID пользователя в системе
   */
  function __construct($user_id = false)
  {
    $this->user_id = $user_id ? $user_id : false;
    $this->profile_id = $this->user_id ? $this->getProfileId(array('user_id' => $this->user_id)) : false;
    $this->arGroup = array('11');// @todo Доставать ID групп из настроек модуля
    $this->errors = array();
    $this->iblock_id = Info::getIblockId('ccards');

    $loader = new Main\Loader;
    $loader->includeModule('iblock');
    $loader->includeModule('sale');
  }

  /**
   * Получение списка клиентов
   *
   * @param array $arParams
   *
   * @return array
   */
  public static function getList($arParams = array()) //@todo
  {
    $arParams['select'][] = 'ID';
    $arParams['select'][] = 'IBLOCK_ID';
    $arParams['select'][] = 'NAME';
    $arParams['select'][] = 'CODE';
    $arParams['select'][] = 'XML_ID';

    $arParams['filter']['IBLOCK_ID'] = !isset($arParams['filter']['IBLOCK_ID']) ? Info::getIblockId('ccards') : $arParams['filter']['IBLOCK_ID'];
    $arParams['order'] = is_array($arParams['order']) ? $arParams['order'] : array();
    $limit = intval($arParams['limit'])>0 ? array('nTopCount' => $arParams['limit']) : false;

    $ob = new \CIBlockElement;
    $res = $ob->GetList(
        $arParams['order'],
        $arParams['filter'],
        false,
        $limit,
        $arParams['select']
    );
    $result = [];

    while ($ob = $res->GetNextElement())
    {
      $arFields = $ob->GetFields();
      $arProperties = $ob->GetProperties();
      $result[] = array_merge($arFields, $arProperties);
    }
    return $result;
  }

  /**
   * Получение клиента по ID
   *
   * @param int $id ID элемента инфоблока Карточки клиентов
   *
   * @return array
   */
  public static function getById($element_id)
  {
    $ar = self::getList(array('filter' => array('ID' => $element_id)));
    return current($ar);
  }

  /**
   * Получение информации о группах пользователей для клиентов
   *
   * @return array
   */
  public function getClientsGroup()
  {
    $groups_id = $this->arGroup;
    if (!empty($groups_id))
    {
      $group = new Main\GroupTable;

      $arParams = array(
          'select' => array('*'),
          'filter' => array('ACTIVE' => 'Y',
              'ID' => $groups_id)
      );
      $resGroup = $group->getList($arParams);

      while ($arGroup = $resGroup->fetch())
      {
        $result[$arGroup['ID']] = $arGroup;
      }
    }
    else
    {
      $this->setError(__METHOD__, 'empty arGroup');
    }
    return $result;
  }

  /**
   * Получение ID пользователя по ID элемента карточки клиенты
   *
   * @param int $element_id
   *
   * @return int|bool
   */
  public function getUserId($element_id = false)
  {
    if ($this->user_id > 0)
    {
      return $this->user_id;
    }
    else
    {
      $element_id = intval($element_id);

      if ($element_id > 0)
      {
        $this->element_id = $element_id;
        $res = \CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array('ID' => $element_id),
            false,
            array('nTopCount' => 1),
            array('ID',
                'IBLOCK_ID',
                'PROPERTY_CC_CLIENT')
        );
        if ($ar = $res->Fetch())
        {
          $result = $ar['PROPERTY_CC_CLIENT_VALUE'];
        }
        else
        {
          $this->setError(__METHOD__, 'not available user');
          $result = false;
        }
      }
      else
      {
        $this->setError(__METHOD__, 'empty contract ID');
        $result = false;
      }
      $this->user_id = $result;
      return $result;
    }


  }

  /**
   * Получение ID элемента Карточки клиента по пользователю
   *
   * @param int $user_id
   *
   * @return int|bool    Возвращает ID элемента или false если элемент не найден
   */
  public function getElementId($user_id = false)
  {
    if ($this->element_id > 0)
    {
      return $this->element_id;
    }
    else
    {
      $user_id = intval($user_id) > 0 ? $user_id : $this->user_id;
      $this->user_id = $user_id;
      $res = \CIBlockElement::GetList(
          array("SORT" => "ASC"),
          array('PROPERTY_CC_CLIENT' => $user_id),
          false,
          array('nTopCount' => 1),
          array('ID',
              'IBLOCK_ID')
      );
      if ($ar = $res->Fetch())
      {
        return $ar['ID'];
      }
      else
      {
        return false;
      }
    }
  }

  /** Получение списка профилей пользователя
   *
   * @param array $params
   *
   * @return array|bool
   */

  public function getProfileId($params = array())
  {
    $result = false;
    if (intval($this->profile_id) > 0)
    {
      return $this->profile_id;
    }
    else
    {
      $user_id = empty($params['user_id']) ? $this->user_id : $params['user_id'];

      if (intval($user_id) > 0)
      {
        Main\Loader::includeModule('sale');
        $dbUserProfile = \CSaleOrderUserProps::GetList(
            array("DATE_UPDATE" => "DESC"),
            array("USER_ID" => IntVal($user_id)),
            false,
            array('nTopCount' => 1),
            array()
        );

        if ($arUserProfile = $dbUserProfile->Fetch())
        {
          $this->personTypeId = $arUserProfile['PERSON_TYPE_ID'];
          $result = $arUserProfile['ID'];
        }
      }
      elseif (intval($params['element_id']) > 0)
      {
        $user_id = $this->getUserId(intval($params['element_id']));
        $result = $this->getProfileId(array('user_id' => $user_id));
        $this->element_id = intval($params['element_id']);
      }
      $this->profile_id = $result;

      return intval($result);
    }
  }

  /**
   * Получаем информацию о профиле покупателя
   */
  public function getProfile($profile_id = false)
  {
    if (!empty($this->arPropfile))
    {

      return $this->arPropfile;
    }
    else
    {
      $profile_id = $profile_id > 0 ? $profile_id : $this->profile_id;

      if (!$profile_id > 0 && $this->user_id > 0)
      {
        $profile_id = $this->getProfileId(array('user_id' => $this->user_id));
      }
      elseif (!$profile_id > 0 && $this->element_id > 0)
      {
        $profile_id = $this->getProfileId(array('element_id' => $this->element_id));
      }

      $this->profile_id = $profile_id;
      $res = \CSaleOrderUserPropsValue::GetList(array('PROP_SORT' => 'ASC'), array('USER_PROPS_ID' => $profile_id));
      while ($ar = $res->Fetch())
      {
        $arProfile[$ar['PROP_CODE']] = $ar;
      }
      $this->arPropfile = $arProfile;
      return $arProfile;
    }
  }

  /**
   * Получаем инфу о профиле покупателя в форматированом виде
   *
   * @uses $this->getProfile
   *
   * @param array $arProfile Не форматированный профиль. Необязательный параметр
   *
   * @return array
   */
  public function getFormatProfile($arProfile = false)
  {
    $arProfile = $arProfile ? $arProfile : $this->getProfile();

    foreach ($arProfile
             as
             $code
    =>
             $arFields)
    {
      $value = $arFields['VALUE'];
      if ($arFields['PROP_TYPE'] == 'LOCATION')
      {
        $location = \CSaleLocation::GetByID($arFields['VALUE']);

        if (!empty($location['REGION_NAME']))
        {
          $arName[] = $location['REGION_NAME'];
        }
        $arName[] = $location['CITY_NAME'];
        $value = implode(', ', $arName);
      }
      if ($arFields['PROP_TYPE'] == 'SELECT')
      {
        $value = $arFields['VARIANT_NAME'];
      }
      $result[$code] = $value;
    }
    return $result;
  }

  /**
   * Получение Номера договора
   *
   * @return string|bool  Возвращает номер договора или false, если карточки клиента нет
   */
  public function getContractId()
  {
    if (!empty($this->contract_id))
    {
      return $this->contract_id;
    }
    else
    {
      $element_id = $this->getElementId();
      $res = \Bitrix\Iblock\ElementTable::getList(
          array(
              'select' => array('NAME'),
              'filter' => array('=ID' => $element_id),
              'limit' => 1
          )
      );
      $ar = $res->fetch();
      return $ar['NAME'];
    }
  }

  /**
   * Проверка: является ли пользователь клиентом
   *
   * @param int $user_id
   *
   * @return bool
   */
  protected function isClient($user_id = false)
  {
    $user_id = !$user_id ? $this->$user_id : $user_id;

    if (intval($user_id) > 0)
    {
      $result = false;

      $user = new Main\UserGroupTable;

      $arParams = array(
          'select' => array('GROUP_ID'),
          'filter' => array('USER_ID' => $user_id)
      );

      $rsUser = $user->getList($arParams);
      while ($arUser = $rsUser->fetch())
      {
        $isClient = in_array($arUser['GROUP_ID'], $this->arGroup);
        if ($isClient === true)
        {
          $result = true;
          $this->$user_id = $user_id;
        }
      }
    }
    else
    {
      $this->setError(__METHOD__, 'empty user ID');
    }
    return $result;
  }

  /**
   * Выводит ошибки имеющиеся в экземпляре объекта
   *
   * @param string $method Название метода ошибки, которого нужно вывести
   */
  public function getErrors($method = '')
  {
    $errors = $this->errors;
    return $method == '' ? $errors : $errors[$method];
  }

  /**
   * Инициализация ошибки
   *
   * @param string $method
   * $params string $text;
   *
   * @return bool
   */
  private function setError($method, $text)
  {
    $arErrors = array($method => $text);
    $this->errors += $arErrors;
    return true;
  }

  /**
   * Модифицированный метод получения ссылки редактирования элемента
   *
   * @rerurn string
   */
  public static function getAdminElementEditLink($IBLOCK_ID, $ELEMENT_ID, $arParams = array(), $strAdd = "")
  {
    if (
        (defined("CATALOG_PRODUCT") || $arParams["force_catalog"])
        && !array_key_exists("menu", $arParams)
    )
    {
      $url = "client_edit.php";
    }
    else
    {
      $url = "client_edit.php";
    }
    $client = new \Itin\Depohost\Clients;

    $USER_ID = $client->getUserId($ELEMENT_ID);
    $PROFILE_ID = $client->getProfileId(array('user_id' => $USER_ID));

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    if ($ELEMENT_ID !== null)
    {
      $url .= "&ID=" . intval($ELEMENT_ID);
    }
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    $url .= "&PROFILE_ID=" . intval($PROFILE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }

    return $url . $strAdd;
  }

  /**
   * Модифицированный метод получения ссылки списка элементов
   *
   * @rerurn string
   */
  public static function getAdminSectionListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
  {

    $url = "clients_list.php";

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }

    return $url . $strAdd;
  }

  /**
   * Модифицированный метод получения ссылки списка элементов
   *
   * @return string
   */
  public static function GetAdminElementListLink($IBLOCK_ID, $arParams = array(), $strAdd = "")
  {
    $url = "clients_list.php";

    $url .= "?IBLOCK_ID=" . intval($IBLOCK_ID);
    $url .= "&type=personal";
    $url .= "&lang=" . urlencode(LANGUAGE_ID);
    foreach ($arParams
             as
             $name
    =>
             $value)
      if (isset($value))
      {
        $url .= "&" . urlencode($name) . "=" . urlencode($value);
      }

    return $url . $strAdd;
  }

  /**
   * Кодируем пароль, так чтобы можно было его
   * раскодировать и получить его в чистом виде
   *
   * @param string $pass - пароль в чистом виде
   * @param string $salt - соль
   *
   * @return string - закодированный пароль
   */
  public static function encodePassword($pass)
  {
    $salt = rand(1000, 9999);
    return str_replace('=', '', base64_encode($pass . md5($salt)));
  }

  /**
   * Расскодировка пароля
   *
   * @param string $code - закодированный пароль
   * @param string $salt - соль
   *
   * @return string - раскодированный пароль
   */
  public static function decodePassword($code)
  {
    return substr(base64_decode($code . '='), 0, -32);
  }

  /**
   * Перезагрузка сервера по ID услуги
   *
   * @param int $service_id
   *
   * @return boolean
   */
  public function rebootServer($service_id)
  {
    $service_id = intval($service_id);
    if ($service_id > 0)
    {
      $ClientsService = self::getHL('service');
      $res = $ClientsService->getById($service_id);
      if ($ar = $res->fetch())
      {
        if (intval($ar['UF_REBOOT']) == 0)
        {
          $server_id = $ar['UF_SERVER_ID'];
          $arIP = $ar['UF_IP_ADDR'];
          $strIP = implode('/', $arIP);
          $client_id = $this->getUserId($ar['UF_CLIENT']);
          $ob = new \Bitrix\Main\SiteTable;
          $res = $ob->getList();
          $arSites = array();
          while ($ar = $res->fetch())
          {
            $arSites[] = $ar['LID'];
          }
          $arFields = array('SERVER_ID' => $server_id,
              'IP_ADDRESSES' => $strIP);
          \CEvent::SendImmediate('CLIENT_SERVER_REBOOT', $arSites, $arFields);


          // Создаем обращение в техподдержку
          Main\Loader::includeModule('support');
          $ticket = new \CTicket;
          $arFields = array(
              'CREATED_MODULE_NAME' => 'clients',
              'MODIFIED_MODULE_NAME' => 'clients',
              'TITLE' => Loc::getMessage('TICKET_REBOOT_TITLE', array('#SERVER_ID#' => $server_id,
                  '#IP_ADDR#' => $strIP)),
              'MESSAGE' => Loc::getMessage('TICKET_REBOOT_MESSAGE', array('#SERVER_ID#' => $server_id,
                  '#IP_ADDR#' => $strIP)),
              'STATUS_SID' => 'ACCEPTED',
              'OWNER_USER_ID' => $client_id,
              'CATEGORY_SID' => 'reboot',
          );
          $ticket_id = $ticket->Set($arFields, $MESSAGE_ID);
          $ClientsService->update($service_id, array('UF_REBOOT' => $ticket_id));// меняем флаг перезагрузки
          return intval($ticket_id) > 0 ? true : false;

        }
        else
        {
          return false;
        }

      }
      else
      {
        return false;
      }
    }

  }

  /**
   * Запрашивает можно ли перезагрузить сервер
   *
   * @param integer $service_id
   * @return boolean or null Доступна ли перезагрузка
   */
  public function accessReboot($service_id)
  {
    $service_id = intval($service_id);
    if ($service_id > 0)
    {
      $ClientsService = self::getHL('service');
      $res = $ClientsService->getById($service_id);
      if ($ar = $res->fetch())
      {
        return intval($ar['UF_REBOOT']) == 0;
      }
      else
      {
        return null;
      }
    }
  }

  public function ipkvm($service_id)
  {
    $service_id = intval($service_id);
    if ($service_id > 0)
    {
      $ClientsService = self::getHL('service');
      $res = $ClientsService->getById($service_id);
      if ($ar = $res->fetch())
      {
        if (intval($ar['UF_IPKVM']) == 0)
        {
          $server_id = $ar['UF_SERVER_ID'];
          $arIP = $ar['UF_IP_ADDR'];
          $strIP = implode('/', $arIP);
          $client_id = $this->getUserId($ar['UF_CLIENT']);
          $ob = new \Bitrix\Main\SiteTable;
          $res = $ob->getList();
          $arSites = array();
          while ($ar = $res->fetch())
          {
            $arSites[] = $ar['LID'];
          }
          $arFields = array('SERVER_ID' => $server_id,
              'IP_ADDRESSES' => $strIP);
          \CEvent::SendImmediate('CLIENT_SERVER_IP_KVM', $arSites, $arFields);


          // Создаем обращение в техподдержку
          Main\Loader::includeModule('support');
          $ticket = new \CTicket;
          $arFields = array(
              'CREATED_MODULE_NAME' => 'clients',
              'MODIFIED_MODULE_NAME' => 'clients',
              'TITLE' => Loc::getMessage('TICKET_IPKVM_TITLE', array('#SERVER_ID#' => $server_id,
                  '#IP_ADDR#' => $strIP)),
              'MESSAGE' => Loc::getMessage('TICKET_IPKVM_MESSAGE', array('#SERVER_ID#' => $server_id,
                  '#IP_ADDR#' => $strIP)),
              'STATUS_SID' => 'ACCEPTED',
              'OWNER_USER_ID' => $client_id,
              'CATEGORY_SID' => 'ipkvm',
          );
          $ticket_id = $ticket->Set($arFields, $MESSAGE_ID);
          $ClientsService->update($service_id, array('UF_IPKVM' => $ticket_id));// меняем флаг перезагрузки
          return intval($ticket_id) > 0 ? true : false;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }

  }

  /**
   * Запрашивает можно ли заказать ipkvm
   *
   * @param integer $service_id
   * @return boolean or null Доступно ли ipkvm
   */
  public function accessIpkvm($service_id)
  {
    $service_id = intval($service_id);
    if ($service_id > 0)
    {
      $ClientsService = self::getHL('service');
      $res = $ClientsService->getById($service_id);
      if ($ar = $res->fetch())
      {
        return intval($ar['UF_IPKVM']) == 0;
      }
      else
      {
        return null;
      }
    }
  }

  /**
   * получения объектов сущностей используемых в модуле Клиенты
   * метод был вынесен в отдельный класс Info, рекомендуется использовать его
   *
   * @param string $code - код
   *
   * @return object
   */
  public static function getHL($code)
  {
    switch ($code)
    {
      case 'service':
        $HL_ID = 1; // @var ID HL-блока хранящий все оформленные услуги
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $hldata = \Bitrix\Highloadblock\HighloadBlockTable::getById($HL_ID)->fetch();
        $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $hlDataClass = $hldata['NAME'] . 'Table';

        $object = new $hlDataClass;

        break;

      default:
        break;
    }
    return $object;
  }

  /**
   * Получение незашифрованного пароля
   *
   * @param $user_id - ID пользователя
   *
   * @return string|boolean
   */
  public static function getPassword($user_id)
  {
    $user = new \CUser;
    $res = $user->GetByID($user_id);
    if ($ar_user = $res->Fetch())
    {
      return self::decodePassword($ar_user['UF_PASSWORD']);
    }
    else
    {
      return false;
    }
  }

  /**
   * Возвращает ID инфоблока по символьному коду сущности
   *
   * @param string $entity_code
   *
   * @return int
   */
  public static function getIblockId($entity_code)
  {
    \Bitrix\Main\Loader::includeModule('iblock');
    $ob = new \Bitrix\Iblock\IblockTable;
    $arParams = array(
        'select' => array('ID'),
        'filter' => array('CODE' => $entity_code),
        'limit' => 1,
    );
    $res = $ob->getList($arParams);
    $ar = $res->fetch();

    return $ar['ID'] > 0 ? $ar['ID'] : false;
  }

  /**
   * Создает таблицу реквизитов для pdf контракта
   *
   * return string
   */
  public function getTableClientDetail()
  {
    $arProfile = $this->getProfile();
    $arFieldsTable = array(
        array('NAME' => '',
            'VALUE' => 'Заказчик:',
            'STRONG' => true),
        array('NAME' => 'Наименование',
            'VALUE' => $arProfile['NAME']['VALUE'],
            'STRONG' => true)
    );
    unset($arProfile['NAME']);
    foreach ($arProfile as $code => $arProp)
    {
      $value = $arProp['VALUE'];
      if ($arProp['PROP_TYPE'] == 'LOCATION')
      {
        $location = \CSaleLocation::GetByID($arProp['VALUE']);

        if (!empty($location['REGION_NAME']))
        {
          $arName[] = $location['REGION_NAME'];
        }
        $arName[] = $location['CITY_NAME'];
        $value = implode(', ', $arName);
      }
      elseif ($arProp['PROP_TYPE'] == 'SELECT')
      {
        $value = $arProp['VARIANT_NAME'];
      }
      elseif ($arProp['PROP_TYPE'] == 'ENUM')
      {
        $value = $arProp['VARIANT_DESCRIPTION'];
      }
      $name = $arProp['NAME'];
      $arFieldsTable[] = array('NAME' => $name,
          'VALUE' => $value);

    }
    $pdf = new Pdf;
    return $pdf->getHtmlSimpleTable($arFieldsTable);
  }

  /**
   * Генерирует pdf договор, сохраняет и привязывает к карточке клиента
   * Карточка клиента уже должна быть создана к моменту генерации
   *
   * @params int $user_id
   *
   * @return int|bool      Возвращет ID файла из таблицы b_file или false в случае неудачи
   */
  public function generateContractPdf($user_id = false)
  {
    if (!$user_id)
    {
      $user_id = $this->getUserId();
    }
    $element_id = $this->getElementId($user_id);
    $arProfile = $this->getProfile();
    $contract_id = $this->getContractId();
    $ob = new \CIBlockElement;

    $res = $ob->GetList(array(), array('ID' => $element_id), false, false, array('ID',
        'IBLOCK_ID',
        'DATE_CREATE'));

    if ($ar = $res->Fetch())
    {
      $date = ConvertDateTime($ar['DATE_CREATE'], 'd.m.Y');
    }

    $pdf = new Pdf;
    $html_table = $this->getTableClientDetail();

    $person_type_id = $this->getPersonTypeId();
    switch ($person_type_id)
    {
      case '1':   //Физ лицо
        $basis_of = '';
        $ceo = $arProfile['NAME']['VALUE'];
        break;
      case '2':   // Юр лицо
        $basis_of = ' в лице генерального директора ' . $arProfile['FIO_GEN_DIRECTOR']['VALUE'] . ', действующего на основании <b class="underlined">' . $arProfile['OSNOVANIE']['VARIANT_DESCRIPTION'] . '</b>,';
        $ceo = $arProfile['FIO_GEN_DIRECTOR']['VALUE'];
        break;
      case '3':   // ИП
        $basis_of = '';
        $ceo = $arProfile['NAME']['VALUE'];
        break;
      default:
        $basis_of = '';
        break;
    }
    $arTemp = explode(' ', trim($ceo));
    $arTemp[1] = substr($arTemp[1], 0, 1) . '.';
    $arTemp[2] = substr($arTemp[2], 0, 1) . '.';
    $ceo_short = implode(' ', $arTemp);

    // Формируем статические маркеры
    $arValues = array(
        'TABLE_CLIENT_INFO' => $html_table,
        'CONTRACT_ID' => $contract_id,
        'DATE' => $date,
        'NAME' => $arProfile['NAME']['VALUE'],
        'BASIS_OF' => $basis_of,
        'CEO_SHORT' => $ceo_short,

    );
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log.log',var_export($arValues,true),FILE_APPEND);

    foreach ($arProfile as $code => $arProp)
    {
      $value = $arProp['VALUE'];
      if ($arProp['PROP_TYPE'] == 'LOCATION')
      {
        $location = \CSaleLocation::GetByID($arProp['VALUE']);
        $arName[] = $location['REGION_NAME'];
        $arName[] = $location['CITY_NAME'];
        $value = implode(', ', $arName);
      }
      if ($arProp['PROP_TYPE'] == 'SELECT')
      {
        $value = $arProp['VARIANT_NAME'];
      }

      $arValues[$code] = $value;
    }


    $path = Info::getPath() . '/templates/pdf/contract/';
    $arPage = $pdf->setTemplateFromFolder($path);

    $pdf->setMarkers($arValues);
    // Шаблон имени файла
    $filename = 'Договор №' . $contract_id . ' от ' . $date . ' ' . $arProfile['NAME']['VALUE'] . '.pdf';
    $filename = str_replace('"', '', $filename);
    $arParams = array(
        'path' => '/upload/clients/pdf/contracts/tmp/',
        'filename' => $filename
    );
    $temp_file = $_SERVER['DOCUMENT_ROOT'] . $arParams['path'] . $arParams['filename'];
    $file = $pdf->createPdf($arParams);
    $arFile = \CFile::MakeFileArray($file['PATH']);

    $arFile['name'] = $filename;
    $arFile['MODULE_ID'] = 'clients';
//        $save_path = '/clients/pdf/contracts/';
//        $file_id = \CFile::SaveFile($arFile,$save_path);
    \CIBlockElement::SetPropertyValueCode($element_id, "CC_CONTRACT", $arFile);
    $res = \CIBlockElement::GetList(array(), array('=ID' => $element_id), false, false, array('ID',
        'IBLOCK_ID',
        'PROPERTY_CC_CONTRACT'));
    $ar = $res->Fetch();
    $file_id = $ar['PROPERTY_CC_CONTRACT_VALUE'];
    unlink($temp_file);
    $this->sendContractToMail($user_id);
    return $file_id;
  }

  public function sendContractToMail($user_id = false)
  {
    if (!$user_id)
    {
      $user_id = $this->getUserId();
    }
    $element_id = $this->getElementId($user_id);
    $contract_id = $this->getContractId();
    $ob = new \CIBlockElement;

    $res = $ob->GetList(array(), array('ID' => $element_id), false, false, array('ID',
        'IBLOCK_ID',
        'DATE_CREATE',
        'NAME',
        'PROPERTY_CC_CONTRACT'));

    $ar_files = array();
    if ($arClient = $res->Fetch())
    {
      $date = ConvertDateTime($arClient['DATE_CREATE'], 'd.m.Y');
      if (is_array($arClient['PROPERTY_CC_CONTRACT_VALUE']))
      {
        foreach ($arClient['PROPERTY_CC_CONTRACT_VALUE']
                 as
                 $file_id)
        {
          $file_src = \CFile::GetPath($file_id);
          if (strlen($file_src) > 0)
          {
            $ar_files[] = $file_src;
          }
        }
      }
      else
      {
        $file_src = \CFile::GetPath($arClient['PROPERTY_CC_CONTRACT_VALUE']);
        if (strlen($file_src) > 0)
        {
          $ar_files[] = $file_src;
        }

      }
    }
    $ob = new \Bitrix\Main\SiteTable;
    $res = $ob->getList();
    $arSites = array();
    while ($ar_site = $res->fetch())
    {
      $arSites[] = $ar_site['LID'];
    }
    $client = new Clients($user_id);
    $profile_id = $client->getProfileId(array('element_id' => $element_id));
    $arProfile = $client->getProfile($profile_id);
    $email = array();
    foreach ($arProfile
             as
             $prop)
    {
      if (strpos($prop['PROP_CODE'], 'EMAIL') !== false)
      {
        $email[] = $prop['VALUE'];
      }
    }
    $email = array_unique($email);
    $email = implode(', ', $email);

    $arUser = \CUser::GetByID($user_id)->Fetch();


    $arFieldsEvent = array(
        'EMAIL' => $email,
        'NUMBER_CONTRACT' => $arClient['NAME'],
        'DATE_CONTRACT' => FormatDateEx($arClient['DATE_CREATE'], false, 'dd.mm.YYYY'),
        'LOGIN' => $arUser['LOGIN'],
        'PASSWORD' => $client->decodePassword($arUser['UF_PASSWORD']),
    );

    // Достанем комментарий к заказу
    $ORDER_ID = (int)$contract_id;
    Loader::includeModule('sale');
    $res = \CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $ORDER_ID,
        'CODE' => 'COMMENT'));

    if ($ar = $res->Fetch())
    {
      $arFieldsEvent['COMMENT'] = $ar['NAME'] . ': ' . $ar['VALUE'];
    }
    else
    {
      $arFieldsEvent['COMMENT'] = '';
    }
    if (!empty($ar_files))
    {
      $arFieldsEvent['ATTACH'] = $ar_files;
    }
//        return \CEvent::SendImmediate('CLIENT_SEND_CONTRACT',$arSites,$arFieldsEvent);
    return \CEvent::Send('CLIENT_SEND_CONTRACT', $arSites, $arFieldsEvent);

  }

  /**
   * Отправляет письмо клиенту
   *
   * @params $eventId     Код почтового события
   * @params $arFields    значения маркеров для шаблона письма
   */
  public function sendMail($eventId, $arFields)
  {
    $arProfile = $this->getProfile();
    $ar_mails = array();
    foreach ($arProfile
             as
             $propProfile)
    {
      if (strpos(strtoupper($propProfile['PROP_CODE']), 'EMAIL') !== false)
      {
        $ar_mails[] = $propProfile['VALUE'];
      }
      elseif (strtoupper($propProfile['PROP_CODE'] == 'NAME'))
      {
        $name = $propProfile['VALUE'];
      }
    }

    if (is_array($arFields['ATTACH']))
    {
      foreach ($arFields['ATTACH']
               as
               $key
      =>
               $src)
      {
        if (strlen($src) == 0 || empty($src))
        {
          unset($arFields['ATTACH'][$key]);
        }
      }
    }
    if (empty($arFields['ATTACH']) && isset($arFields['ATTACH']))
    {
      unset($arFields['ATTACH']);
    }
    $arContract = $this->getById($this->element_id);
    $ar_mails = array_unique($ar_mails);
    $email = implode(',', $ar_mails);
    $arFields['EMAIL'] = $email;
    $arFields['NAME'] = $name;
    $ob = new \Bitrix\Main\SiteTable;
    $res = $ob->getList();
    $arSites = array();
    while ($ar = $res->fetch())
    {
      $arSites[] = $ar['LID'];
    }
    \CEvent::Send($eventId, $arSites, $arFields);
  }

  /**
   * Возвращает ID типа плательщика
   *
   * @return int
   */
  public function getPersonTypeId()
  {
    if (intval($this->person_type_id) > 0)
    {
      return $this->person_type_id;
    }
    else
    {
      $arProfile = $this->getProfile();
      $prop = current($arProfile);
      $this->person_type_id = $prop['PROP_PERSON_TYPE_ID'];
      return $prop['PROP_PERSON_TYPE_ID'];
    }
  }

  /**
   * Получаем список услуг клиента
   *
   * @params int $id          ID элемента инфоблока Карточки клиента
   * @params string $filter   дополнительный фильтр
   *
   * @return array
   */
  public static function getClientServicesList($id, $filter = false)
  {
    $ob_services = Info::getHL('service');

    $arParams = array(
        'select' => array('*'),
        'filter' => array('UF_CLIENT' => $id)
    );
    if (is_array($filter))
    {
      $arParams['filter'] = array_merge($arParams['filter'], $filter);
    }

    $res = $ob_services->getList($arParams);
    while ($ar = $res->fetch())
    {
      $result[$ar['ID']] = $ar;
    }
    return $result;
  }

  /**
   * Получаем список услуг клиента требующие оплаты
   *
   * @param int $id ID элемента инфоблока Карточки клиента
   *
   * @return array
   */
  public static function getClientServicesListRequirePay($id)
  {
    $ob_services = Info::getHL('service');

    $arParams = array(
        'select' => array('*'),
        'filter' => array('UF_CLIENT' => $id,
            'UF_BILL' => 1)
    );

    $res = $ob_services->getList($arParams);
    while ($ar = $res->fetch())
    {
      $result[$ar['ID']] = $ar;
    }
    return $result;
  }

  /**
   * Краткая запись метода getClientServicesListRequirePay($id)
   *
   * @uses self::getClientServiceRequirePay($id)
   */
  public static function getCSListReqPay($id)
  {
    return self::getClientServicesListRequirePay($id);
  }

  /**
   * Краткая запись метода getClientServicesList($id)
   *
   * @uses self::getClientServiceRequirePay($id)
   */
  public static function getCSList($id, $filter = false)
  {
    return self::getClientServicesList($id, $filter);
  }

  /**
   * Сохраняем услуги облагающиеся абонентской платой
   *
   * @param array $arParams
   *      'ar_ids'        массив ID записей ClientsService
   *      'element_id'    ID элемента Карточки клиента
   *
   * @return boolean
   */
  public static function setClientServicesListRequirePay($arParams)
  {
    extract($arParams);
    $arServices = self::getCSList($element_id);
    $ar_ids_old = array_keys($arServices);
    $ob_services = self::getHL('service');

    foreach ($ar_ids_old
             as
             $id)
    {
      $arFields = array();

      if (in_array($id, $ar_ids))
      {

        $arFields['UF_BILL'] = 1;
      }
      else
      {
        $arFields['UF_BILL'] = 0;
      }
      $ob_services->Update($id, $arFields);
    }
    return true;
  }

  /**
   * Сохраняем услуги облагающиеся абонентской платой
   *
   * @param array $arParams
   *      'ar_ids'        массив ID записей ClientsService
   *      'element_id'    ID элемента Карточки клиента
   *
   * @return boolean
   */
  public static function setCSListRequirePay($arParams)
  {
    return self::setClientServicesListRequirePay($arParams);
  }

  /**
   * Получаем список услуг являющимися выделенными серверами и имеющие номер сервера и ip-адрес
   */
  public static function getDedicatedServers($element_id)
  {
    if (intval($element_id) > 0)
    {
      $ob_services = Info::getHL('service');
      $arParams = array(
          'select' => array('*'),
          'filter' => array('UF_CLIENT' => $element_id,
              '!UF_SERVER_ID' => false,
              '!UF_IP_ADDR' => false),
      );
      $res = $ob_services->getList($arParams);
      while ($ar = $res->fetch())
      {
        $result[$ar['ID']] = $ar;
      }
      return $result;
    }
    else
    {
      return false;
    }
  }

  /**
   * Получение абонентской платы клиента
   *
   * @params $id - ID элемента карточки клиента
   */
  public static function getAbonplata($id)
  {
    $summ = 0;
    $ar = self::getCSListReqPay($id);
    foreach ($ar as $ar_values)
    {
      $summ += intval($ar_values['UF_SUMM']);
    }
    return $summ;
  }

  /**
   * Установка абонентской платы клиенту
   *
   * @params $param - массив параметров
   *          'id'    - ID элемента карточки клиента
   *          'summ'  - Размер абонентской платы
   */
  public static function setAbonplata($param)
  {
    extract($param);
    $client = new self;
    $client->element_id = $id;
    $arProperties = array(
        'CC_ABONPLATA' => $summ,
    );
    $client->updateProperties($arProperties);
  }

  /**
   * Пересчет абонентской платы
   * @params $id - ID элемента карточки клиента
   */
  public static function recalcAbonplata($id)
  {
    $summ = self::getAbonplata($id);
    $param = array('id' => $id,
        'summ' => $summ);
    self::setAbonplata($param);
  }

  /**
   * Обновление массива свойств клиента
   */
  public function updateProperties(array $arProperties)
  {
    Loader::includeModule('iblock');
    \CIBlockElement::SetPropertyValuesEx($this->getElementId(), $this->iblock_id, $arProperties);
  }

  /**
   * Обновление массива свойств типа список по XML_ID значению варианта
   *
   * @param array $arProperties - Массив свойств array('PROPERTY_CODE|PROPERTY_ID'=>'XML_ID_VALUE');
   */
  public function updateEnumProperties(array $arProperties)
  {
    $iblock_id = $this->iblock_id;
    Loader::includeModule('iblock');
    foreach ($arProperties
             as
             $property_code
    =>
             $property_value)
    {
      $ar = \CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $iblock_id,
          'PROPERTY_ID' => $property_code,
          'EXTERNAL_ID' => $property_value))->Fetch();
      $PROPERTY_VALUES[$ar['PROPERTY_ID']] = $ar['ID'];
    }
    \CIBlockElement::SetPropertyValuesEx($this->element_id, $iblock_id, $PROPERTY_VALUES);
  }

  /**
   * Получаем массив ID счетов
   *
   * @params array $params    Массив параметров
   *                          array(
   *                              'element_id' - ID элемента инфоблока Карточки клиента
   *                              'status' - XML_ID статуса счета
   *                              'filter' - филтрация счетов
   *                              'order' - сортировка
   *                              'limit' - количество записей
   *                          )
   */
  public static function getInvoices(array $params)
  {
    extract($params);
    $element_id = (int)$element_id;
    if ($element_id > 0)
    {
      $arParams = array('filter' => array('=PROPERTY_CLIENT' => $element_id));
      if (isset($status) && !empty($status))
      {
        $arStatuses = Invoices::getRatioStatus();
        $status_id = $arStatuses[$status]['ID'];
        $arParams['filter']['PROPERTY_STATUS'] = $status_id;
      }
      if (isset($filter) && is_array($filter))
      {
        $arParams['filter'] = array_merge($arParams['filter'],$filter);
      }
      if (isset($order) && is_array($order))
      {
        $arParams['order'] = $order;
      }
      if (isset($limit) && intval($limit)>0)
      {
        $arParams['limit'] = intval($limit)>0;
      }
      $invoice = new Invoices;
      $ar = $invoice->getList($arParams);

      $result = array();
      foreach ($ar
               as
               $ar_invoice)
      {
        $result[] = $ar_invoice['ID'];

      }
      return $result;
    }
    else
    {
      return false;
    }
  }

  /**
   * Есть ли счета со статусом
   *
   * @params $xml_id      - XML_ID статуса счета
   */
  public function availableInvoice($xml_id = 'no-paid', $exclude_id = false)
  {
    $element_id = $this->getElementId();

    $arParams = array('element_id' => $element_id,
        'status' => $xml_id);
    if (intval($exclude_id) > 0)
    {
      $arParams['filter'] = array('!ID'=>$exclude_id);
    }
    $arIds = $this->getInvoices($arParams);
//        $arStatuses = Invoices::getRatioStatus();
//        $status_id = $arStatuses[$xml_id]['ID'];
//        $invoice = new Invoices;
//
//        $arParams['filter'] = array('ID'=>$arIds,'PROPERTY_STATUS'=>$status_id);
//        if ($ar = $invoice->getList($arParams))

    if (!empty($arIds))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Инициализирует ID элемента инфоблока карточки клиента
   *
   * @params int $element_id      ID элемента инфоблока карточки клиента
   */
  public function setElementId($element_id)
  {
    $this->element_id = (int)$element_id;
  }

  /**
   * Устанавливает флаг Есть ли неоплаченные счета
   *
   * @param int $element_id ID элемента инфоблока Карточки клиента
   * @return string <Y|N>
   */
  public static function setNoPaid($element_id)
  {
    $client = new Clients;
    $client->setElementId((int)$element_id);
    $client->getInvoices(array('element_id' => $element_id));
    $value = $client->availableInvoice('no-paid') ? 'Y' : 'N';

    $arProperties = array('AVAILABLE_NO_PAID' => $value);
    $client->updateEnumProperties($arProperties);
    return $value;
  }

  /**
   * Устанавливает флаг Есть ли неоплаченные счета, проверяет все кроме указано счета
   * Используется в событии удаления счета
   *
   * @param int $element_id ID элемента инфоблока Карточки клиента
   * @return string <Y|N>
   */
  public static function setNoPaidEx($element_id, $invoice_id)
  {
    $client = new Clients;
    $client->setElementId((int)$element_id);
    $value = $client->availableInvoice('no-paid',$invoice_id) ? 'Y' : 'N';

    $arProperties = array('AVAILABLE_NO_PAID' => $value);
    $client->updateEnumProperties($arProperties);
    return $value;
  }

  /**
   * Возвращает массив email из профиля покупателя
   *
   * @param integer $user_id
   * @return array
   */
  public function getEmails($user_id = false)
  {
    if (!$user_id)
    {
      $user_id = $this->getUserId();
    }
    $arProfile = $this->getProfile();
    $arEmail = array();
    foreach ($arProfile
             as
             $prop)
    {
      if (strpos($prop['PROP_CODE'], 'EMAIL') !== false)
      {
        $arEmail[] = $prop['VALUE'];
      }
    }
    $arEmail = array_unique($arEmail);
    return $arEmail;
  }

  /**
   * Обновляет в карточке клиента дату последнего счета
   *
   * @param date(d.m.Y)|false $date
   *
   * @return false|date(d.m.Y)
   */
  public function updateLastInvoiceDate($date = false)
  {
    $element_id = $this->getElementId();
    if ($date === false)
    {
      $arLastInvoice = Invoices::getLastInvoice($element_id);
      $date = $arLastInvoice['DATE']['VALUE'];
    }
    if (empty($date))
    {
      $date = '';
    }
    \CIBlockElement::SetPropertyValuesEx($element_id,$this->iblock_id,array('LAST_INVOICE_DATE'=>$date));

    return $date;
  }
}
