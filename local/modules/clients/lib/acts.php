<?php
namespace Itin\Depohost;
use \Bitrix\Main\Loader as Loader;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Работа с актами клиентов
 *
 * @author Aleksandr Terentev
 */
class Acts extends Invoices
{
    /* @var int $iblock_id - ID инфоблока актов */
    protected $iblock_id;
    /** @var int    ID элемента инфоблока Актов */
    private $id;
    /** @var int    ID элемента Карточки клиента относящейся к акту */
    private $client_id;

    /*
     * @param int $id   ID элемента инфлока Актов
     */
    function __construct($id = false)
    {
        $this->id = $id;

        $this->iblock_id = Info::getIblockId('acts');
        if (defined('ADMIN_SECTION') && ADMIN_SECTION===true)
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
        }
        else
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        }
    }
    /*
     * Получение ID инфоблока Акты
     *
     * @return int
     */
    public function getIblockId()
    {
        if (intval($this->iblock_id)>0)
        {
            return $this->iblock_id;
        }
        else
        {
            $iblock_id = Info::getIblockId('acts');
            $this->iblock_id = $iblock_id;
            return $iblock_id;
        }
    }
    /*
     * Получение ID элемента инфоблока счета
     */
    public function getId()
    {
        return $this->id;
    }
    /*
     * Отправка счета на E-mail
     *
     * @param int $id   ID элемента инфоблока Акты
     *
     * @return null
     */
    public function sendToMail($id = false)
    {
        Loader::includeModule('iblock');
        $client = new Clients;
        $id = intval($id)>0 ? intval($id) : $this->getId();
        $ob = new \CIBlockElement;
        $res = $ob->GetList(array(),array('ID'=>$id),false,false,array('ID','IBLOCK_ID','PROPERTY_CLIENT','PROPERTY_DOCUMENT','PROPERTY_DATE','PROPERTY_NUMBER'));
        if($ar = $res->Fetch())
        {

            $doc_url = !empty($ar['PROPERTY_DOCUMENT_VALUE']) ? \CFile::GetPath($ar['PROPERTY_DOCUMENT_VALUE']) : false;

            $client_card_id = $ar['PROPERTY_CLIENT_VALUE'];
            $profile_id = $client->getProfileId(array('element_id'=>$client_card_id));
            $arProfile = $client->getProfile($profile_id);

            $arClient = $client->getList(array(
                'select' => array('CC_ABONPLATA','DATE_CREATE','NAME',),
                'filter' => array('=ID'=>$client_card_id),
            ));
            $arClient = current($arClient);

            $email = array();
            foreach ($arProfile as $prop)
            {
                if (strpos($prop['PROP_CODE'], 'EMAIL')!==false)
                {
                    $email[] = $prop['VALUE'];
                }
            }
            $email = array_unique($email);
            $email = implode(', ', $email);

        }
        $arFieldsEvent = array(
            'EMAIL' => $email,
            'NUMBER' => $ar['PROPERTY_NUMBER_VALUE'],
            'DATE' => $ar['PROPERTY_DATE_VALUE'],
            'NAME' => $arProfile['NAME']['VALUE'],
            'NUMBER_CONTRACT' => $arClient['NAME'],
            'DATE_CONTRACT' => FormatDateEx($arClient['DATE_CREATE'],false, 'dd.mm.YYYY'),

        );
        if (!empty($doc_url))
        {
            $arFieldsEvent['ATTACH'] = $doc_url;
        }
        $ob  = new \Bitrix\Main\SiteTable;
        $res = $ob->getList();
        $arSites = array();
        while ($ar = $res->fetch())
        {
            $arSites[] = $ar['LID'];
        }

        return \CEvent::Send('CLIENT_SEND_ACT',$arSites,$arFieldsEvent);
//        return \CEvent::SendImmediate('CLIENT_SEND_ACT',$arSites,$arFieldsEvent);
    }
    /*
     * Генерирует pdf файл акта
     *
     * @param int $id   ID элемента инфоблока Актов
     */
    public function generatePdf($id = false){
        $id = intval($id)>0 ? $id : $this->getId();
        $this->id = $id;
        // Получаем все данные для счета из инфоблоков
        $ar = $this->getById($id);
        $arData = $this->getFormat($ar);

        $client = new Clients($arData['CLIENT']['CC_CLIENT']);

        // Получаем все данные для счета из профиля покупателя
        $arProfile = $client->getFormatProfile();
        // формирую адрес из индекса, города, и самого адреса
        $arAddress = array();
        $arAddress[] = $arProfile['POST_INDEX'];
        $arAddress[] = $arProfile['LOCATION'];
        $arAddress[] = $arProfile['ADDRESS'];
        $address = implode(', ', $arAddress);
        // добавляю точку в конец Назначения платежа, если ее нет.
        $arData['DESCRIPTION'] .= strpos('.',$arData['DESCRIPTION'])==strlen($arData['DESCRIPTION'])-1 || empty($arData['DESCRIPTION']) ? '' : '.';
        $arProfile['KPP'] = !empty($arProfile['KPP']) ? '/ '.$arProfile['KPP'] : '';
        if (!empty($arData['CLIENT']))
        {
            $arClient = $arData['CLIENT'];
        }
        elseif ($arData['LINK_INVOICE']['CLIENT'])
        {
            $arClient = $arData['LINK_INVOICE']['CLIENT'];
        }
        $contract_date = $arData['CLIENT']['DATE_CREATE'];
        Loader::includeModule('sale');
        // Формируем маркеры для pdf
        $table_services = $this->getTableListServices($arData['SERVICES'],
            Loc::getMessage('TEMPLATE_SERVICE_NAME',
                array(
                    '#CONTRACT_ID#'=>$arData['CLIENT']['NAME'],
                    '#CONTRACT_DATE#'=>FormatDateEx($contract_date,false,'dd.mm.YYYY')
                )
            )
        );
        $arValues = array(
            'ACT_ID' => $arData['NUMBER'],
            'ACT_DATE' => FormatDateEx($arData['DATE'],false,'d F YYYY'),
            'CONTRACT_ID' => $arData['CLIENT']['NAME'],
            'CONTRACT_DATE' => FormatDateEx($contract_date,false,'dd.mm.YYYY'),
            'NAME' => $arProfile['NAME'],
            'INN' => $arProfile['INN'],
            'KPP' => $arProfile['KPP'],
            'ADDRESS' => $address,
            'DESCRIPTION' => $arData['DESCRIPTION'],
            'TABLE_LIST_SERVICES' => $table_services,
            'TOTAL_SUM' => str_replace(' ','&nbsp;',number_format($arData['SUMM'],2,',',' ')),
            'TOTAL_SUM_STRING' => Number2Word_Rus($arData['SUMM']),
            'COUNT_SERVICES' => count($arData['SERVICES']),
        );
        // формируем pdf
        $pdf = new Pdf;
        $pdf->setMarkers($arValues);
        $path_module = Info::getPath();
        $pdf->setTemplateFromFolder($path_module.'/templates/pdf/acts/');
        // Период берем от даты счета, если он есть
        $date_period = !empty($arData['LINK_INVOICE']['DATE']) ? $arData['LINK_INVOICE']['DATE'] : $arData['DATE'];
        $date_name = FormatDateEx($date_period,false,'f_YYYY');
        $arFilename = array($arProfile['NAME'],$arData['CLIENT']['NAME'],$date_name);
//        $filename = implode('_', $arFilename).'.pdf';
        $filename = 'Акт по договору №'.$arData['CLIENT']['NAME']
            .' от '.FormatDateEx($contract_date,false,'dd.mm.YYYY')
            .', период '.FormatDateEx($date_period,false,'f YYYY')
            .' '.$arProfile['NAME'];
        $filename = str_replace('"', '', $filename);
        $filename = trim($filename," \t.");
        $filename .= '.pdf';
        $arParams = array(
            'path' => '/upload/clients/pdf/acts/tmp/',
            'filename' => $filename
        );
        $temp_file = $_SERVER['DOCUMENT_ROOT'].$arParams['path'].$arParams['filename'];
        $file = $pdf->createPdf($arParams);
        $arFile = \CFile::MakeFileArray($file['PATH']);
        $arFile['name'] = $filename;
        $arFile['MODULE_ID'] = 'clients';
        Loader::includeModule('iblock');
        \CIBlockElement::SetPropertyValueCode($arData['ID'], "DOCUMENT", $arFile);
        unlink($temp_file);
    }
    public function add($arFields)
    {
        $arFields['IBLOCK_ID'] = $this->iblock_id;
        $ob = new \CIBlockElement;
        return $ob->Add($arFields);
    }
}
