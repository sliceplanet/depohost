<?php
namespace Itin\Depohost;

use Bitrix\Main;
use Bitrix\Main\Loader;
/**
 * Класс для получения информации связанной с модулем клиенты
 *
 * @author Aleksander Terentev
 */
class Info
{
    protected $path;
    
    function __construct()
    {
        Loader::includeModule('clients');
        
        if (defined('ADMIN_SECTION') && ADMIN_SECTION===true)
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
        }
        else
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        }
        $this->path = Main\IO\Path::combine(__DIR__,'../');
    }
    /*
     * Получение абсолютного пути к модулю
     * 
     * @return string
     */
    public static function getPath()
    {
        return Main\IO\Path::combine(__DIR__,'../');
    }
    /*
     * Возвращает ID инфоблока по символьному коду сущности
     * 
     * @param string $entity_code
     * 
     * @return int
     */
    public static function getIblockId($entity_code)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $ob = new \Bitrix\Iblock\IblockTable;
        $arParams = array(
            'select' => array('ID'),
            'filter' => array('CODE'=>$entity_code),
            'limit' => 1,
        );
        $res = $ob->getList($arParams);
        $ar = $res->fetch();
        
        return $ar['ID']>0 ? $ar['ID'] : false;
    }
    /*
     * получения объектов сущностей используемых в модуе Клиенты
     * 
     * @param string $code - код 
     * 
     * @return object
     */
    public static function getHL($code)
    {
        switch ($code)
        {
            case 'service':
                $HL_ID = 1; // @var ID HL-блока хранящий все оформленные услуги
                \Bitrix\Main\Loader::includeModule('highloadblock');
                $hldata = \Bitrix\Highloadblock\HighloadBlockTable::getById($HL_ID)->fetch();
                $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
                $hlDataClass = $hldata['NAME'].'Table';

                $object = new $hlDataClass; 

                break;

            default:
                break;
        }
        return $object;
    }
    /*
     * получения объектов сущностей используемых в модуле Клиенты по имени таблицы
     * 
     * @param string $table_name - имя таблицы 
     * 
     * @return object
     */
    public static function getHlByTableName($table_name)
    {
       \Bitrix\Main\Loader::includeModule('highloadblock');
        $arParams = array(
            'select'=>array('*'),
            'filter'=>array('TABLE_NAME'=>$table_name),
        );
        $hldata = \Bitrix\Highloadblock\HighloadBlockTable::getList($arParams)->fetch();
        $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $hlDataClass = $hldata['NAME'].'Table';

        $object = new $hlDataClass; 

        return $object;
    }
}
