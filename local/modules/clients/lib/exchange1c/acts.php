<?php
namespace Itin\Depohost\Exchange1C;
use Itin\Depohost;
use Itin\Depohost\Exchange1C\Acts;
use Itin\Depohost\Exchange1C\UtilXML;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use CDataXML;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/../exchange1c.php');
/**
 * Класс обмена с 1Ской счетами
 *
 * @author Aleksandr Terentev
 */
class Acts
{
    /** @var array      Параметры обмена с 1Ской счетами*/
    var $arParams = array();
    /** @var boolean    Содержит ли обмен только изменения*/
    var $bModify = true;
    
    /*
     * Содержит ли обмен только изменения
     * 
     * @params string $path     xpath узла
     * @params array @attr      Массив атрубутов узла
     * 
     * @return boolean
     */
    public function isModify($path, $attr)
    {
        $bModify = $attr[Loc::getMessage('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED')] == 'false' ? false : true;
        $this->bModify = $bModify;
        return $bModify;
    }

    /*
     * Обработка актов
     * 
     * @params CDataXML $value  Объект узла XML
     */
    public function importActs(CDataXML $value)
    {   
        Loader::includeModule('iblock');
        Loader::includeModule('clients');
        $bModify = $this->bModify;
        
        $value = $value->GetArray();
        $values = UtilXML::getValues($value); 
        $arFields = array();
        $arElement = $values[Loc::getMessage('NODE_ELEMENT')];        
        $arFields['XML_ID'] = $arElement[Loc::getMessage('NODE_ID')];
        $date = $arElement[Loc::getMessage('NODE_DATE')];
        $arProperties = $arElement[Loc::getMessage('NODE_PROPERTIES_VALUES')][Loc::getMessage('NODE_PROPERTY_VALUES')];        
        $arPropertiesValues = array();
        foreach ($arProperties as $key => $arProperty)
        {
            $property_xml_id = $arProperty[Loc::getMessage('NODE_ID')];
            $ar_prop = $this->handlerProperty($property_xml_id, $arProperty[Loc::getMessage('NODE_VALUE')]);
            
            $arPropertiesValues += $ar_prop; 
            
            if ($property_xml_id == 'NUMBER')
            {
                $number = $arProperty[Loc::getMessage('NODE_VALUE')];
            }
            
        }
        $arFields['PROPERTY_VALUES'] = $arPropertiesValues;
        $arFields['PROPERTY_VALUES']['DATE'] = ConvertTimeStamp(MakeTimeStamp($date,'YYYY-MM-DD HH:MI:SS'),'SHORT');
      
        $arFields['NAME'] = '№'.$number.' от '.$arFields['PROPERTY_VALUES']['DATE'];
        $act_ob = new Depohost\Acts;
        $ob = new \CIBlockElement;
        $res = $ob->GetList(array(),
            array('XML_ID'=>$arFields['XML_ID'],'IBLOCK_ID'=> intval(Depohost\Info::getIblockId('acts'))),
            false,
            array('nTopCount' => 1),
            array('ID')
        );
        if ($ar = $res->Fetch())
        {
            $act_id = $ar['ID'];
            $ob->Update($act_id,$arFields);
        }
        else
        {
            $act_id = $act_ob->add($arFields);
            
        }         
    }
    /*
     * Обработка классификатор свойств актов
     * 
     * @params CDataXML $value  Объект узла XML
     * 
     * @return array    Возвращает инфо по свойствам
     */
    public function handlerProperties(CDataXML $value)
    {
//        Loader::includeModule('iblock');
//        $value = $value->GetArray();
//        $values = UtilXML::getValues($value);
//        $arProperty = $values[Loc::getMessage('NODE_PROPERTY')];
    }
    /*
     * Обработчик значения свойства
     * 
     * @params array $arProperty    Часть массива, касающего значений свойства полученного методом UtilXML::getValues()
     * 
     * @return array
     */
    private function handlerProperty($property_xml_id,$arProperty)
    {
        $IBLOCK_ID = Depohost\Info::getIblockId('acts'); 
        
        $res = \CIBlockProperty::GetList(array(),array('IBLOCK_ID'=>$IBLOCK_ID));
        while ($ar = $res->Fetch())
        {
            $arPropertiesInfo[$ar['XML_ID']] = $ar;
        }
        $arPropertyInfo = $arPropertiesInfo[$property_xml_id];
        $property_value =  $arProperty;
        
        if ($arPropertyInfo['PROPERTY_TYPE'] == 'E')
        {
            $arSubElements = $property_value[Loc::getMessage('NODE_ELEMENTS')][Loc::getMessage('NODE_ELEMENT')];
            $ids = array();
            // Если не множественно свойство или одно значение в множественном
            if ($arPropertyInfo['MULTIPLE'] == 'N' || !isset($arSubElements[0]))
            {
                $arSubElements = array($arSubElements);
            }
            foreach ($arSubElements as $arSubElement)
            {
                $element_xml_id = trim($arSubElement[Loc::getMessage('NODE_ID')]); 
                if (!empty($element_xml_id))
                {
                    $ar_element = \CIBlockElement::GetList(
                        array(),
                        array('XML_ID'=>$element_xml_id,'IBLOCK_ID'=>$arPropertyInfo['LINK_IBLOCK_ID']),
                        false,
                        array('nTopCount' => 1),
                        array('ID')
                    )->Fetch();
                }
                $ids[]= !empty($ar_element['ID']) ? intval($ar_element['ID']) : false;                
            }
            if ($arPropertyInfo['MULTIPLE'] == 'N')
            {
                $ids = current($ids);
            }
            $result = $ids;
        }
        elseif ($arPropertyInfo['USER_TYPE'] == 'directory')
        {
            $arSubElements = $property_value[Loc::getMessage('NODE_ELEMENTS')][Loc::getMessage('NODE_ELEMENT')];
            $ids = array();
            if ($arPropertyInfo['MULTIPLE'] == 'N' || !isset($arSubElements[0]))
            {
                $arSubElements = array($arSubElements);
            }
            
            foreach ($arSubElements as $arSubElement)
            {
                $element_xml_id = trim($arSubElement[Loc::getMessage('NODE_ID')]);
                
                $ids[]= !empty($element_xml_id) ? intval($element_xml_id) : false;
            }
            if ($arPropertyInfo['MULTIPLE'] == 'N')
            {
                $ids = current($ids);
            }
            $result = $ids;
        }
        elseif($arPropertyInfo['PROPERTY_TYPE'] == 'L')
        {
            
        }
        elseif (!is_array($property_value))
        {
            $result = $property_value;
        }
        return array($arPropertyInfo['ID'] => $result);
    }
}
