<?php
namespace Itin\Depohost\Exchange1C;
use Itin\Depohost\Exchange1C\XmlStream as XmlStream;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/../exchange1c.php');
/**
 * Вспомогательный класс для разбора XML файлов обмена с 1С
 *  *
 * @author Aleksander Terentev
 * @uses XMLReader
 */
class XmlStream
{
    /** @var array Массив ошибок*/
    private $arErrors;
    /** @var object Объект SimpleXML */
    private $xml;

    /*
     * @params string $URI
     */
    function __construct()
    {

    }
    /*
     * Получения Классификатора свойств
     *
     * @return array
     */
    public function getPropertiesInfo(\CDataXML $value)
    {
          $value = $value->GetArray();
//        return $information;
//        $xml = $reader->expandSimpleXml();
//        $children = $xml->children();
//        $attributes = $xml->attributes();
//        echo (string) $attributes->{self::getMessageUtf8('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED')};
//        $children = self::encodeCharset(json_encode($children));
//        $children = json_decode($children, TRUE);
//
//        return true;
    }
    public function elementHandler($path, $attr)
    {
    }
    public function isModify()
    {
        $reader = $this;
        while ($reader->read())
        {
            $ar[] = $reader->localName;
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == self::getMessageUtf8('NODE_INFORMATION'))
            {
                $arResult[] = $reader->moveToAttribute(Loc::getMessage('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED')->value);
//                $arResult[] = self::encodeCharset($reader->moveToAttribute(self::getMessageUtf8('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED'))->value);
//                $arResult[] = self::encodeCharset($reader->moveToNextAttribute()->value);
            }
            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->localName == self::getMessageUtf8('NODE_COMMERCIAL_INFORMATION'))
            {
                echo self::encodeCharset($reader->localName);
                $arResult[] = self::encodeCharset($reader->moveToNextAttribute()->value);
                echo self::encodeCharset($reader->name);

            }
        }
        return $arResult;
    }
    /*
     * Регистрирует ошибку в объект
     *
     * @params array $arError array(<тип ошибки> => <текст ошибки>)
     */
    private function setError($arError)
    {
        $this->arErrors[] = $arError;
    }
    /*
     * Вывод ошибок зарегисрированных объектом
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->arErrors;
    }

    public static function getMessageUtf8($code)
    {
        return iconv(LANG_CHARSET,'UTF-8',Loc::getMessage($code));
    }

    public static function encodeCharset($data)
    {
        if (is_array($data))
        {
            $ar_ser = iconv('UTF-8', LANG_CHARSET, serialize($data));
            return unserialize($ar_ser);
        }
        elseif ($data === true || $data === false || $data === null)
        {
            return $data;
        }
        else
        {
            return iconv('UTF-8', LANG_CHARSET, (string) $data);
        }
    }
    /*
     * Преобразует массив узлов в xpath путь
     *
     * @params array $array
     *
     * @return string
     */
    private static function arrayToXpath(array $array)
    {
        return '/'.implode('/', $array);
    }
}
