<?php
namespace Itin\Depohost\Exchange1C;
use Itin\Depohost;
use Itin\Depohost\Exchange1C\Invoices;
use Itin\Depohost\Exchange1C\UtilXML;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use CDataXML;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/../exchange1c.php');
/**
 * Класс обмена с 1Ской счетами
 *
 * @author Aleksandr Terentev
 */
class Invoices
{
    /** @var array      Параметры обмена с 1Ской счетами*/
    var $arParams = array();
    /** @var boolean    Содержит ли обмен только изменения*/
    var $bModify = true;
    
    /** @var array      Инфо по вариантам Статусов */
    private $arStatusVariants;

    /*
     * Содержит ли обмен только изменения
     * 
     * @params string $path     xpath узла
     * @params array @attr      Массив атрубутов узла
     * 
     * @return boolean
     */
    public function isModify($path, $attr)
    {
        $bModify = $attr[Loc::getMessage('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED')] == 'false' ? false : true;
        $this->bModify = $bModify;
        return $bModify;
    }

    /*
     * Обработка счетов
     * 
     * @params CDataXML $value  Объект узла XML
     */
    public function importStatuses(CDataXML $value)
    {   
        Loader::includeModule('iblock');
        Loader::includeModule('clients');
        $bModify = $this->bModify;
        
        $value = $value->GetArray();
        $values = UtilXML::getValues($value);
        $arElement = $values[Loc::getMessage('NODE_ELEMENT')];        
        $invoice_id = intval($arElement[Loc::getMessage('NODE_ID')]);
        if ($invoice_id>0)
        {
            $arProperties = $arElement[Loc::getMessage('NODE_PROPERTIES_VALUES')][Loc::getMessage('NODE_PROPERTY_VALUES')];
    //        Если больше одного свойства
            if (isset($arProperties[0])){
                foreach ($arProperties as $key => $arProperty)
                {
                    if (trim($arProperty[Loc::getMessage('NODE_ID')]) == 'STATUS')
                    {
                        $status_value = trim($arProperty[Loc::getMessage('NODE_VALUE')]);
                        $status_id = $this->arStatusVariants[$status_value];
                    }
                } 
            } // Если есть только свойство Статус
            elseif(trim($arProperties[Loc::getMessage('NODE_ID')]) == 'STATUS'){
                $status_value = trim($arProperties[Loc::getMessage('NODE_VALUE')]);
                $status_id = $this->arStatusVariants[$status_value];
            }

            if ((int) $status_id >0)
            {
                $invoice_ob = new Depohost\Invoices($invoice_id);
                $arProperties = array(
                    'STATUS' => $status_id
                );      
                $invoice_ob->Update($arProperties);
            }
            else {
                // @todo Отсутствует статус счета
            }
        }   
        
    }
    /*
     * Обработка классификатор свойства Статус счета
     * 
     * @params CDataXML $value  Объект узла XML
     * 
     * @return array    Возвращает инфо по свойствам
     */
    public function handlerPropertyStatus(CDataXML $value)
    {
        Loader::includeModule('iblock');
        $value = $value->GetArray();
        $values = UtilXML::getValues($value);
        $arProperty = $values[Loc::getMessage('NODE_PROPERTY')];
       
        if ($arProperty[Loc::getMessage('NODE_ID')] == 'STATUS')
        {            
            foreach ($arProperty[Loc::getMessage('NODE_VARIANTS_VALUES')][Loc::getMessage('NODE_VARIANT')] as $arVariant)
            {   
                $variant_id = trim($arVariant[Loc::getMessage('NODE_ID')]);
                $ob = new \CIBlockPropertyEnum;
                $res = $ob->GetList(array('SORT'=>'ASC'), array('PROPERTY_ID'=>'STATUS','EXTERNAL_ID' => $variant_id));
                if ($ar = $res->Fetch())
                {
                    $resVariants[$variant_id] = $ar['ID'];
                }
                else
                {
                    $variant_value = trim($arVariant[Loc::getMessage('NODE_VALUE')]);
                    $status_id = $ob->Add(array('PROPERTY_ID'=>'STATUS','EXTERNAL_ID' => $variant_id,'VALUE' =>$variant_value));
                    $resVariants[$variant_id] = $status_id;
                }
            }
            $this->arStatusVariants = $resVariants;
        }
    }
}
