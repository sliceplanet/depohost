<?php
namespace Itin\Depohost\Exchange1C;
use Itin\Depohost\Exchange1C\UtilXML as UtilXML;
use Bitrix\Main\Localization\Loc;
use CDataXML;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/../exchange1c.php');
/**
 * Вспомогательный класс для работы с XML
 *
 * @author Aleksandr Terentev
 */
class UtilXML
{
    /*
     * Возвращает отформатированный массив значение из массива полученного методом CDataXML::GetArray()
     * 
     * @params array $array
     * 
     * @return array
     */
    public static function getValues(array $value)
    {  
        if (isset($value['#']))
        {
            $array = $value['#'];
        }
        else
        {
             $array = $value;
        }
        if (count($array)<2 && isset($array[0]['#']))
        {
            $array = $array[0]['#'];
        }
        $arValues = array();
        if (is_array($array))
        {
            foreach ($array as $key => $value)
            {
                if (is_array($value))
                {
                    $arValues[$key] = self::getValues($value);                                    
                }
                else
                {
                    $arValues[$key] = $value;
                }
            }
        }
        else
        {
            $arValues = $array;
        }
        return $arValues;
    }
}
