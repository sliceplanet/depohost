<?php

namespace Itin\Depohost;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use CEventMessage;

/**
 * Работа с смсками
 *
 * @author Aleksandr Terentev
 */
class Sms
{
  const HOST = 'https://api.devino.online';
  /**
   * @var array $arErrors Array of errors
   */
  private $arErrors;

  /**
   * @var string Last error
   */
  public $LAST_ERROR;

  private $arSms = [];
  private $apiKey;
  private $from;
  private $templateMessage;

  function __construct()
  {
    $this->apiKey = Option::get('clients', 'sms_api_key');
    $this->from = Option::get('clients', 'sms_source_address', 'DepoTelecom');

    if (!$this->apiKey)
    {
      throw new \Exception('Не указан api key для devinoonline https://docs.devino.online/ru/#authorization');
    }

    // if sms
    $arFilter = array('ACTIVE' => 'Y', 'TYPE_ID' => 'MONITORING_SMS');
    $by = 'id';
    $order = 'asc';
    $res = CEventMessage::GetList($by, $order, $arFilter);

    if ($ar = $res->Fetch())
    {
      $this->templateMessage = $ar['MESSAGE'];
    }
  }

  public function getMessage($arFields) {
    $message = $this->templateMessage;
    foreach ($arFields as $key => $value)
    {
      $message = str_replace('#' . $key . '#', $value, $message);
    }

    return $message;
}

  public function collectSmsToQueue(array $sms) {
    static::formatPhoneToE164($sms['to']);
    static::checkMessage($sms['text']);
    $sms['from'] = $this->from;

    $this->arSms[] = $sms;
  }

  public function sendQueue() {
    if(empty($this->arSms)) {
      return false;
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, static::HOST . '/sms/messages');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POST, 1);

    $arHeaders = array(
      'Authorization: Key ' . $this->apiKey,
      'Content-Type: application/json'
    );
    curl_setopt(
      $curl, CURLOPT_HTTPHEADER, $arHeaders);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['messages' => $this->arSms]));
    $response = curl_exec($curl);
    curl_close($curl);

    return $response;
  }

  /**
   * Send sms with help webservice REST API devinotele.com
   * Documentation for webservice https://docs.devino.online/ru/http/sms/
   *
   * @param string $to
   * @param string $message
   * @return string or false Return response from webservice or false if failed request
   * @throws \Exception
   */
  public static function send($to, $message)
  {
    static::checkMessage($message);
    static::formatPhoneToE164($to);
    $apiKey = Option::get('clients', 'sms_api_key');
    $from = Option::get('clients', 'sms_source_address', 'DepoTelecom');

    if (!$apiKey)
    {
      throw new \Exception('Не указан api key для devinoonline https://docs.devino.online/ru/#authorization');
    }

    $arMessages = [
      [
        'from' => $from,
        'to' => $to,
        'text' => $message,
        'priority' => 3
      ]
    ];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, static::HOST . '/sms/messages');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POST, 1);

    $arHeaders = array(
      'Authorization: Key ' . $apiKey,
      'Content-Type: application/json'
    );
    curl_setopt(
      $curl, CURLOPT_HTTPHEADER, $arHeaders);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['messages' => $arMessages]));
    $response = curl_exec($curl);
    curl_close($curl);

    return $response;
  }

  private static function checkMessage(string &$message)
  {
    $message = htmlspecialcharsbx($message);
    if (defined('LANG_CHARSET') && strtoupper(LANG_CHARSET) != 'UTF-8')
    {
      $message = mb_convert_encoding($message, 'utf-8', LANG_CHARSET);
    }
  }

  /**
   * Format phone to international standard E.164
   * @param string $to
   */
  private static function formatPhoneToE164(string &$to)
  {
    if (strpos($to, '8') === 0 || strpos($to, '7') === 0)
    {
      $to = '+7' . substr($to, 1);
    }
    elseif (strpos($to, '+7') === false)
    {
      $to = '+7' . $to;
    }
  }

  /**
   *
   * @param string $message Write message on error in array of errors
   * @return boolean
   */
  private function setError($message)
  {
    $this->arErrors[] = $message;
    $this->LAST_ERROR = $message;
    return true;
  }

  /**
   *
   * @return array Return array of errors
   */
  public function getErrors()
  {
    return $this->arErrors;
  }


}
