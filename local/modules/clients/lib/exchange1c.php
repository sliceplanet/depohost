<?php
namespace Itin\Depohost;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
/**
 * Класс обмена с 1С
 *
 * @author Aleksandr Terentev
 */
class Exchange1C
{
    /** @var string тип обмена invoices | acts | clients */
    private $type;

    function __construct($type = false)
    {
        $this->type = $type;
    }
    /*
     * Эскпорт в формате XML
     *
     * @params array $params
     *          'type'=><invoices | acts | clients>, Тип обмена
     *          'last'=>true                        Только изменения
     */
    public static function exportXml($params)
    {
        $arParams = array();
        \Bitrix\Main\Loader::includeModule('iblock');
        if ($params['last'])
        {
            $last_date = ConvertTimeStamp(self::getSuccessLastExchange($params['type']),'FULL');
            self::setStartLastExchange($params['type']);
            $arParams['filter']['DATE_MODIFY_FROM'] = $last_date;
        }

        //----- Собираем инфу для XML ---------
        $XML_ID = self::getIblockXmlId($params['type']);
        $ar = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID','NAME','CODE'),
            'filter' => array('XML_ID'=>$XML_ID),
            'limit' => 1,
        ))->fetch();
        $arFields = array(
            'last' =>$params['last'],
            'last_date' => $last_date,
            'IBLOCK_XML_ID'=>$XML_ID,
            'IBLOCK_NAME'=>$ar['NAME'],
            'IBLOCK_CODE'=>$ar['CODE'],
        );
        logi($_REQUEST, 'request','log/log');
        logi($arFields, 'fields','log/log');
        $IBLOCK_ID = $ar['ID'];

        $res = \CIBlockProperty::GetList(array('SORT'=>'ASC'),array('IBLOCK_ID'=>$IBLOCK_ID));
        while ($ar = $res->Fetch())
        {
            $arProperties[$ar['CODE']] = $ar;
        }
        $arFields['PROPERTIES'] = $arProperties;
        switch ($params['type'])
        {
            case 'invoices':
                $ob_el = new \Itin\Depohost\Invoices;
                $arParams['filter']['!PROPERTY_CLIENT'] = false;
              break;

            case 'acts':
                $ob_el = new \Itin\Depohost\Acts;
                break;

             case 'ccards':
                $ob_el = new \Itin\Depohost\Clients;
                break;
        }
        if (is_object($ob_el))
        {

            $ar = $ob_el->getList($arParams);
            foreach ($ar as $arItem)
            {
                $arFields['ITEMS'][$arItem['ID']] = $ob_el->getFormat($arItem);
            }
        }
        //--- Инфа по контрагентам
        if ($params['type']=='invoices')
        {
            $arFields['CONTRACTS'] = array();
            $arUserIDs = array();
            foreach ($arFields['ITEMS'] as $arItem)
            {
                $arUserIDs[] = intval($arItem['CLIENT']['CC_CLIENT']);
            }
            $arUserIDs = array_unique($arUserIDs);
            foreach ($arUserIDs as $user_id)
            {
                $client = new Clients($user_id);
                $arProfile = $client->getProfile();

                foreach ($arProfile as $code => $arProfileFields)
                {
                    $value = $arProfileFields['VALUE'];
                    if ($arProfileFields['PROP_TYPE']=='LOCATION')
                    {
                        $location = \CSaleLocation::GetByID($arProfileFields['VALUE']);
                        $arName = array();
                        if (!empty($location['REGION_NAME']))
                        {
                            $arName[] = $location['REGION_NAME'];
                        }
                        $arName[] = $location['CITY_NAME'];
                        $value = implode(', ', $arName);
                    }
                    $resultProfile[$code] = array(
                        'NAME'=>$arProfileFields['NAME'],
                        'XML_ID'=>$arProfileFields['ID'],
                        'VALUE'=>$value,
                    );
                }   // END foreach ($arProfile as $code => $arProfileFields)
                $ar = \CIBlockElement::GetByID($client->getElementId())->Fetch();
                $arFields['CONTRACTS'][] = array(
                    'XML_ID' => $ar['XML_ID'],
                    'NAME' => $resultProfile['NAME']['VALUE'],
                    'DETAILS' => $resultProfile
                );
            }   // END foreach ($arUserIDs as $user_id)
        }
        //--- END Инфа по контрагентам
        //----- END Собираем инфу для XML ---------
        // Выводим XML
        self::showXml($arFields, $params['type']);
    }
    /**
     * Выводит XML по входящим данным, метод для разделения шаблона XML и данных
     *
     * @params array $arFields      Входящие данные
     * @params string $type         Тип импорта
     */
    private static function showXml($arFields,$type)
    {
        echo '<?xml version="1.0" encoding="windows-1251"?>';
        ?>
        <<?=Loc::getMessage('NODE_COMMERCIAL_INFORMATION')?> date="<?php echo $arFields['last_date']?>" колво="<?php echo count($arFields['ITEMS'])?>" <?=Loc::getMessage('ATTRIBUTE_COMMERCIAL_INFORMATION_VERSION_SCHEMA')?>="2.021" <?=Loc::getMessage('ATTRIBUTE_COMMERCIAL_INFORMATION_DATE_CREATE')?>="<?=date("Y-m-dТH:i:s")?>">
            <<?=Loc::getMessage('NODE_QUALIFIER')?>>
                <<?=Loc::getMessage('NODE_ID')?>><?=$arFields['IBLOCK_XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                <<?=Loc::getMessage('NODE_NAME')?>><?=$arFields['IBLOCK_NAME']?></<?=Loc::getMessage('NODE_NAME')?>><?
                // ---- Описание всех свойств
                ?><<?=Loc::getMessage('NODE_PROPERTIES')?>>
                <?
                $arPropertiesLinkBlock = array();
                foreach ($arFields['PROPERTIES'] as $arProperty)
                {
                    if (!empty($arProperty['USER_TYPE']))
                    {
                        $PROPERTY_TYPE = $arProperty['USER_TYPE'];
                    }
                    else
                    {
                        $PROPERTY_TYPE = $arProperty['PROPERTY_TYPE'];
                    }
                    if (intval($arProperty['LINK_IBLOCK_ID'])>0)
                    {
                        $ar = \Bitrix\Iblock\IblockTable::getById($arProperty['LINK_IBLOCK_ID'])->fetch();
                        $LINK_IBLOCK = $ar['XML_ID'];
                        if ($arProperty['LINK_IBLOCK_ID'] == Info::getIblockId('ccards'))
                        {
                            $arPropertiesLinkBlock[$arProperty['CODE']]['CONTRACT_ID'] = array(
                                'XML_ID' => 'CONTRACT_ID',
                                'NAME' => 'Номер договора',
                                'MULTIPLY' => 'N',
                                'SORT' => 500,
                                'CODE' => 'NAME',
                                'PROPERTY_TYPE' => 'S',
                                'LINK_IBLOCK' => '',
                            );
                            $arPropertiesLinkBlock[$arProperty['CODE']]['CONTRACT_DATE'] = array(
                                'XML_ID' => 'CONTRACT_DATE',
                                'NAME' => 'Дата договора',
                                'MULTIPLY' => 'N',
                                'SORT' => 500,
                                'CODE' => 'DATE',
                                'PROPERTY_TYPE' => 'S',
                                'LINK_IBLOCK' => '',
                            );
                        }
                    }
                    elseif ($PROPERTY_TYPE == 'directory')
                    {
                        $table_name = $arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'];
                        \Bitrix\Main\Loader::includeModule('highloadblock');
                        $arParams = array(
                            'select'=>array('*'),
                            'filter'=>array('TABLE_NAME'=>$table_name),
                            'limit'=>1,
                        );
                        $hldata = \Bitrix\Highloadblock\HighloadBlockTable::getList($arParams)->fetch();
                        $LINK_IBLOCK = $hldata['NAME'];
                        if ($arProperty['XML_ID'] == 'SERVICES')
                        {
//                            $className = $hldata['NAME'].'Table';
//                            $ob = new $className;
                            // uf info
                            $fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hldata['ID'], 0, LANGUAGE_ID);
                            $arUF = $fields['UF_NAME'];
                            $arPropertiesLinkBlock[$arProperty['CODE']]['NAME'] = array(
                                'XML_ID' => 'NAME',
                                'NAME' => $arUF['EDIT_FORM_LABEL'],
                                'MULTIPLY' => $arUF['MULTIPLE'],
                                'SORT' => $arUF['SORT'],
                                'CODE' => $arUF['FIELD_NAME'],
                                'PROPERTY_TYPE' => 'S',
                                'LINK_IBLOCK' => '',
                            );
                            $arUF = $fields['UF_QUANTITY'];
                            $arPropertiesLinkBlock[$arProperty['CODE']]['QUANTITY'] = array(
                                'XML_ID' => 'QUANTITY',
                                'NAME' => $arUF['EDIT_FORM_LABEL'],
                                'MULTIPLY' => $arUF['MULTIPLE'],
                                'SORT' => $arUF['SORT'],
                                'CODE' => $arUF['FIELD_NAME'],
                                'PROPERTY_TYPE' => 'N',
                                'LINK_IBLOCK' => '',
                            );
                            $arUF = $fields['UF_PROPERTIES'];
                            $arPropertiesLinkBlock[$arProperty['CODE']]['PROPERTIES'] = array(
                                'XML_ID' => 'PROPERTIES',
                                'NAME' => $arUF['EDIT_FORM_LABEL'],
                                'MULTIPLY' => $arUF['MULTIPLE'],
                                'SORT' => $arUF['SORT'],
                                'CODE' => $arUF['FIELD_NAME'],
                                'PROPERTY_TYPE' => 'S',
                                'LINK_IBLOCK' => '',
                            );
                            $arUF = $fields['UF_CATALOG_XML_ID'];
                            $arPropertiesLinkBlock[$arProperty['CODE']]['PROPERTIES'] = array(
                                'XML_ID' => 'XML_ID',
                                'NAME' => $arUF['EDIT_FORM_LABEL'],
                                'MULTIPLY' => $arUF['MULTIPLE'],
                                'SORT' => $arUF['SORT'],
                                'CODE' => $arUF['FIELD_NAME'],
                                'PROPERTY_TYPE' => 'S',
                                'LINK_IBLOCK' => '',
                            );
                        }
                    }
                    else
                    {
                        $LINK_IBLOCK = '';
                    }
                ?>
                    <<?=Loc::getMessage('NODE_PROPERTY')?>>
                        <<?=Loc::getMessage('NODE_ID')?>><?=$arProperty['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                        <<?=Loc::getMessage('NODE_NAME')?>><?=$arProperty['NAME']?></<?=Loc::getMessage('NODE_NAME')?>>
                        <<?=Loc::getMessage('NODE_MULTIPLY')?>><?=$arProperty['MULTIPLY'] == 'Y' ? 'true' : 'false'?></<?=Loc::getMessage('NODE_MULTIPLY')?>>
                        <<?=Loc::getMessage('NODE_SORT')?>><?=$arProperty['SORT']?></<?=Loc::getMessage('NODE_SORT')?>>
                        <<?=Loc::getMessage('NODE_CODE')?>><?=$arProperty['CODE']?></<?=Loc::getMessage('NODE_CODE')?>>
                        <<?=Loc::getMessage('NODE_PROPERTY_TYPE')?>><?=$PROPERTY_TYPE?></<?=Loc::getMessage('NODE_PROPERTY_TYPE')?>>
                        <<?=Loc::getMessage('NODE_LINK_IBLOCK')?>><?=$LINK_IBLOCK?></<?=Loc::getMessage('NODE_LINK_IBLOCK')?>>
                        <?php
                        if (!empty($arPropertiesLinkBlock[$arProperty['CODE']]))
                        {
                        ?>
                        <<?=Loc::getMessage('NODE_PROPERTIES_ELEMENTS')?>>
                        <?
                            // Свойства элементов связанного инфоблока
                            foreach ($arPropertiesLinkBlock[$arProperty['CODE']] as $arPropertyLinkBlock)
                            {
                            ?>
                            <<?=Loc::getMessage('NODE_PROPERTY_ELEMENTS')?>>
                                <<?=Loc::getMessage('NODE_ID')?>><?=$arPropertyLinkBlock['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                <<?=Loc::getMessage('NODE_NAME')?>><?=$arPropertyLinkBlock['NAME']?></<?=Loc::getMessage('NODE_NAME')?>>
                                <<?=Loc::getMessage('NODE_MULTIPLY')?>><?=$arPropertyLinkBlock['MULTIPLY'] == 'Y' ? 'true' : 'false'?></<?=Loc::getMessage('NODE_MULTIPLY')?>>
                                <<?=Loc::getMessage('NODE_SORT')?>><?=$arPropertyLinkBlock['SORT']?></<?=Loc::getMessage('NODE_SORT')?>>
                                <<?=Loc::getMessage('NODE_CODE')?>><?=$arPropertyLinkBlock['CODE']?></<?=Loc::getMessage('NODE_CODE')?>>
                                <<?=Loc::getMessage('NODE_PROPERTY_TYPE')?>><?=$arPropertyLinkBlock['PROPERTY_TYPE']?></<?=Loc::getMessage('NODE_PROPERTY_TYPE')?>>
                                <<?=Loc::getMessage('NODE_LINK_IBLOCK')?>><?=$arPropertyLinkBlock['LINK_IBLOCK']?></<?=Loc::getMessage('NODE_LINK_IBLOCK')?>>
                            </<?=Loc::getMessage('NODE_PROPERTY_ELEMENTS')?>>
                            <?
                            }   //END foreach ($arPropertiesLinkBlock as $arPropertyLinkBlock)
                        ?>
                        </<?=Loc::getMessage('NODE_PROPERTIES_ELEMENTS')?>>
                        <?
                        }   //END if (!empty($arPropertiesLinkBlock))
                        if ($PROPERTY_TYPE == 'L')
                        {
                        ?>
                        <<?=Loc::getMessage('NODE_VARIANTS_VALUES')?>>
                        <?
                            $res = \CIBlockPropertyEnum::GetList(array('SORT'=>'ASC'),array('PROPERTY_ID'=>$arProperty['ID']));
                            while ($ar = $res->Fetch())
                            {
                            ?>
                        <<?=Loc::getMessage('NODE_VARIANT')?>>
                                <<?=Loc::getMessage('NODE_ID')?>><?=$ar['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                <<?=Loc::getMessage('NODE_VALUE')?>><?=$ar['VALUE']?></<?=Loc::getMessage('NODE_VALUE')?>>
                            </<?=Loc::getMessage('NODE_VARIANT')?>>
                            <?
                            }
                        ?>
                        </<?=Loc::getMessage('NODE_VARIANTS_VALUES')?>>
                        <?
                        }
                        ?>

                    </<?=Loc::getMessage('NODE_PROPERTY')?>>
                <?
                }   // END foreach ($arFields['PROPERTIES'] as $arProperty)
                ?>
                </<?=Loc::getMessage('NODE_PROPERTIES')?>><?
                // ----- END Описание всех свойств

                ?>
            </<?=Loc::getMessage('NODE_QUALIFIER')?>>
            <?
            // ---- END Классификатор
            // ---- Контрагенты
            ?>
            <<?=Loc::getMessage('NODE_CONTRACTS')?>>
                <?
                foreach ($arFields['CONTRACTS'] as $arClient)
                {
                ?>
                <<?=Loc::getMessage('NODE_CONTRACT')?>>
                    <<?=Loc::getMessage('NODE_ID')?>><?=$arClient['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                    <<?=Loc::getMessage('NODE_NAME')?>><?=$arClient['NAME']?></<?=Loc::getMessage('NODE_NAME')?>>
                    <<?=Loc::getMessage('NODE_DETAILS')?>>
                    <?
                    foreach ($arClient['DETAILS'] as $profileValues)
                    {
                    ?>
                        <<?=Loc::getMessage('NODE_DETAIL')?>>
                            <<?=Loc::getMessage('NODE_ID')?>><?=$profileValues['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                            <<?=Loc::getMessage('NODE_NAME')?>><?=$profileValues['NAME']?></<?=Loc::getMessage('NODE_NAME')?>>
                            <<?=Loc::getMessage('NODE_VALUE')?>><?=$profileValues['VALUE']?></<?=Loc::getMessage('NODE_VALUE')?>>
                        </<?=Loc::getMessage('NODE_DETAIL')?>>
                    <?
                    }   // END foreach ($arProfile as $profileValues)
                    ?>
                    </<?=Loc::getMessage('NODE_DETAILS')?>>
                </<?=Loc::getMessage('NODE_CONTRACT')?>>
                <?
                }   // END foreach ($arClients as $arClient)
                ?>
            </<?=Loc::getMessage('NODE_CONTRACTS')?>>
            <?// ---- END Контрагенты
            // ---- Информация
            ?>
            <<?=Loc::getMessage('NODE_INFORMATION')?> <?=Loc::getMessage('ATTRIBUTE_INFORMATION_IS_AVAILABLE_MODIFIED')?>="<?=$params['last'] ? 'true' : 'false'?>">
                <<?=Loc::getMessage('NODE_ID')?>><?=$arFields['IBLOCK_XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                <<?=Loc::getMessage('NODE_QUALIFIER_ID')?>><?=$arFields['IBLOCK_XML_ID']?></<?=Loc::getMessage('NODE_QUALIFIER_ID')?>>
                <<?=Loc::getMessage('NODE_CODE')?>><?=$arFields['IBLOCK_CODE']?></<?=Loc::getMessage('NODE_CODE')?>>
                <<?=Loc::getMessage('NODE_ELEMENTS')?>><?
                // ----- Список элементов
                unset($arFields['PROPERTIES']['DATE']);

                foreach ($arFields['ITEMS'] as $arItem)
                {
                ?>
                    <<?=Loc::getMessage('NODE_ELEMENT')?>>
                        <<?=Loc::getMessage('NODE_ID')?>><?=$arItem['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                        <<?=Loc::getMessage('NODE_DATE')?>><?=FormatDateEx($arItem['DATE'],false,'d.m.YYYY H:i:s')?></<?=Loc::getMessage('NODE_DATE')?>><?
                        // ---- Значения свойств элементов
                        ?><<?=Loc::getMessage('NODE_PROPERTIES_VALUES')?>>
                        <?php

                        foreach ($arFields['PROPERTIES'] as $code => $arProperty)
                        {

                            $arElementProperty = $arItem[$code];
                            // Проходимся по всему списку имеющихся свойств
                        ?>
                            <<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                                <<?=Loc::getMessage('NODE_ID')?>><?=$arProperty['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                <<?=Loc::getMessage('NODE_VALUE')?>>
                                <?php
                                    // Если свойство: привязка к Карточкам клиентов
                                    if ($arProperty['LINK_IBLOCK_ID'] == Info::getIblockId('ccards'))
                                    {
                                        $arProfile = array();
                                        $client_ob = new \Itin\Depohost\Clients($arElementProperty['CC_CLIENT']);
                                        $arProfile = $client_ob->getFormatProfile();
                                        $CLIENT_NAME = $arProfile['NAME'];
                                    ?>
                                        <<?=Loc::getMessage('NODE_ELEMENTS')?>>
                                            <<?=Loc::getMessage('NODE_ELEMENT')?>>
                                                <<?=Loc::getMessage('NODE_ID')?>><?=$arElementProperty['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                                <<?=Loc::getMessage('NODE_NAME')?>><?=$CLIENT_NAME?></<?=Loc::getMessage('NODE_NAME')?>>
                                                <<?=Loc::getMessage('NODE_PROPERTIES_VALUES')?>>
                                                    <<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                                                        <<?=Loc::getMessage('NODE_ID')?>><?=$arPropertiesLinkBlock[$arProperty['CODE']]['CONTRACT_ID']['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                                        <<?=Loc::getMessage('NODE_VALUE')?>><?=$arElementProperty['NAME']?></<?=Loc::getMessage('NODE_VALUE')?>>
                                                    </<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                                                    <<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                                                        <<?=Loc::getMessage('NODE_ID')?>><?=$arPropertiesLinkBlock[$arProperty['CODE']]['CONTRACT_DATE']['XML_ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                                        <<?=Loc::getMessage('NODE_VALUE')?>><?=FormatDateEx($arElementProperty['DATE_CREATE'],false, 'dd.mm.YYYY')?></<?=Loc::getMessage('NODE_VALUE')?>>
                                                    </<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                                                </<?=Loc::getMessage('NODE_PROPERTIES_VALUES')?>>
                                            </<?=Loc::getMessage('NODE_ELEMENT')?>>
                                        </<?=Loc::getMessage('NODE_ELEMENTS')?>>
                                    <?
                                    }// Если свойство: привязка к HL блоку ClientsService
                                    elseif ($arProperty['USER_TYPE'] == 'directory')
                                    {
                                    ?>
                                        <<?=Loc::getMessage('NODE_ELEMENTS')?>>
                                    <?
                                        if ($code == 'SERVICES')
                                        {
                                            if ($arProperty['MULTIPLY'] == 'N')
                                            {
                                                $arElementProperty = array($arElementProperty);
                                            }
                                            foreach ($arElementProperty as $field)
                                            {
                                                $arServiceProperties = array(
                                                    'PRICE' => $field['UF_PRICE'],
                                                    'QUANTITY' => $field['UF_QUANTITY'],
                                                    'PROPERTIES' => $field['UF_PROPERTIES'],
                                                    'XML_ID' => $field['UF_CATALOG_XML_ID'],
                                                );
                                                ?>
                                            <<?=Loc::getMessage('NODE_ELEMENT')?>>
                                                <<?=Loc::getMessage('NODE_ID')?>><?=$field['ID']?></<?=Loc::getMessage('NODE_ID')?>>
                                                <<?=Loc::getMessage('NODE_NAME')?>><?=$field['UF_NAME']?></<?=Loc::getMessage('NODE_NAME')?>>
                                                <?
                                                foreach ($arServiceProperties as $id => $value)
                                                {
                                                ?>
                                                <<?=Loc::getMessage('NODE_PROPERTY')?>>
                                                    <<?=Loc::getMessage('NODE_ID')?>><?=$id?></<?=Loc::getMessage('NODE_ID')?>>
                                                    <?
                                                        if (is_array($value) && !empty($value))
                                                        {?>
                                                            <<?=Loc::getMessage('NODE_VALUES')?>>
                                                            <?
                                                            foreach ($value as $val)
                                                            {?>
                                                                <<?=Loc::getMessage('NODE_VALUE')?>><?=$val?></<?=Loc::getMessage('NODE_VALUE')?>>
                                                            <?}     //END foreach ($value as $val)
                                                            ?>
                                                            </<?=Loc::getMessage('NODE_VALUES')?>>
                                                        <?
                                                        }   //END if (is_array($value))
                                                        else
                                                        {
                                                            if (is_array($value))
                                                            {
                                                                $value = implode(', ', $value);
                                                            }
                                                        ?>
                                                        <<?=Loc::getMessage('NODE_VALUE')?>><?=$value?></<?=Loc::getMessage('NODE_VALUE')?>>
                                                        <?
                                                        }
                                                    ?>
                                                </<?=Loc::getMessage('NODE_PROPERTY')?>>
                                                <?}     // END foreach ($arServiceProperties as $id => $value)
                                                ?>
                                            </<?=Loc::getMessage('NODE_ELEMENT')?>>
                                            <?
                                            }   // END foreach ($arElementProperty as $field)
                                        }   // END if ($arProperty['CODE'] == 'SERVICES')
                                            ?>
                                        </<?=Loc::getMessage('NODE_ELEMENTS')?>>
                                        <?php
                                    }   // END elseif ($arProperty['USER_TYPE'] == 'directory')
                                    elseif($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['MULTIPLE'] == 'N')
                                    {
                                        $arTemp = current($arElementProperty);
                                        echo $arTemp['XML_ID'];
                                    }   //  END elseif($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['MULTIPLY'] == 'N')
                                    elseif(!is_array($arElementProperty))
                                    {
                                        echo $arElementProperty;
                                    }
                                ?>
                                </<?=Loc::getMessage('NODE_VALUE')?>>
                            </<?=Loc::getMessage('NODE_PROPERTY_VALUES')?>>
                        <?
                        }   // END foreach ($arFields['PROPERTIES'] as $code => $arProperty)
                        ?>
                        </<?=Loc::getMessage('NODE_PROPERTIES_VALUES')?>><?
                        // ---- END Значения свойств
                    ?></<?=Loc::getMessage('NODE_ELEMENT')?>>
                <?
                }       // END foreach ($arFields['ITEMS'] as $arItem)
                // ----- END Список элементов

                ?></<?=Loc::getMessage('NODE_ELEMENTS')?>>
            </<?=Loc::getMessage('NODE_INFORMATION')?>>
        </<?=Loc::getMessage('NODE_COMMERCIAL_INFORMATION')?>>
        <?
    }
    /*
     * Время запуска последнего обмена
     *
     * @params string $type Тип обмена invoices | acts | clients
     *
     * @return int      Время в формате UNIX
     */
    public static function getStartLastExchange($type)
    {
        Option::get('clients','1c_exchange_last_time_'.$type, time());
    }
    /*
     * Устанавливает время последнего запуска обмена
     *
     * @params string $type Тип обмена invoices | acts | clients
     *
     * @return bool
     */
    public static function setStartLastExchange($type)
    {
        Option::set('clients','1c_exchange_last_time_'.$type, time());
    }
    /*
     * Время последнего успешного обмена
     *
     * @params string $type Тип обмена invoices | acts | clients
     *
     * @return int      Время в формате UNIX
     */
    public static function getSuccessLastExchange($type)
    {
        return Option::get('clients','last_export_time_committed_'.$type, time());
    }
    /*
     * Устанавливает время последнего успешного обмена
     *
     * @params string $type Тип обмена invoices | acts | clients
     *
     * @return bool
     */
    public static function setSuccessLastExchange($type)
    {
        Option::set('clients','last_export_time_committed_'.$type, time());
    }
    /*
     * Соответсвие типа обмена XML_ID инфоблока
     *
     * @params string $type Тип обмена invoices | acts | clients
     *
     * @return string       Возвращает XML_ID инфоблока участвующего в обмене
     */
    private static function getIblockXmlId($type)
    {
        $arTypeToIblock = array(
            'invoices' => 'invoices',
            'acts' => 'acts',
            'ccards' => 'client_cards',
        );
        return $arTypeToIblock[$type];
    }
    /*
     * Разбирает XML и преобразует в форматированный массив
     *
     * @params string $xmlString
     *
     * @return array
     */
    public static function parseInvoiceXml($xmlString)
    {
        $xmlString = (string)$xmlString;

    }
}
