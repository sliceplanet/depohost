<?php

namespace Itin\Depohost;

use Bitrix\Main\Localization\Loc;

/**
 * Класс работы с услугами клиента
 *
 * @author Aleksandr Terentev
 */
class Services
{

    /**
     * Отправляем список услуг на маил
     *
     * @params $arServices      Массив услуг
     * @params $params          Массив параметров с ключами
     *                          client_id       ID элемента инфоблока Карточки клиентов
     */
    public static function sendToMailList(array $arServices, array $params)
    {
        $html = "<ul>";
        $text_list = "";
        foreach ($arServices as $arService)
        {
            $html .= "<li>" . $arService['UF_NAME'] . "</li>";
            $text_list .= $arService['UF_NAME'] . "\r\n";
        }
        $html .= "</ul>";
        $client = new Clients;
        $user_id = $client->getUserId($params['client_id']);

        $arFields = array(
            'SERVICES_LIST_HTML' => $html,
            'SERVICES_LIST' => $text_list,
        );
        $client->sendMail('CLIENT_SERVICES_ADD', $arFields);
    }

    /**
     * Обрабатывает входящий массив маркеров шаблона названия
     * добавляя маркеры необходимые конкретным услугам
     *
     * @param string $service_code
     * @param array $params
     *
     * @return array Отформатированный массив маркеров
     */
    public static function handleReplacement($service_code, array $params)
    {
        $result = $params;

        foreach ($params['UF_PROPERTIES'] as $prop)
        {
            // Удаляем наименование характеристики
            if (stripos($prop, 'период') === false) // Период нам в характеристиках не нуженs
            {
                $arPropsValues[] = substr($prop, (strpos($prop, ': ') + 2));
            }

        }
        $result['STRING_CHARACTERISTICS'] = implode(', ', $arPropsValues);

        foreach ($params['UF_PROPERTIES'] as $prop)
        {
            if (stripos($prop, 'CPU') === 0)
            {
                $result['CPU'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'RAM') === 0)
            {
                $result['RAM'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'IP') === 0)
            {
                $result['IP'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'HDD') === 0)
            {
                $result['HDD'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'FTP') === 0)
            {
                $result['FTP'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'SSD') === 0)
            {
                $result['SSD'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'домен') === 0)
            {
                $result['DOMAIN'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'срок') === 0 && stripos($prop, 'год') !== false)
            {
                $result['STRING_YEARS'] = substr($prop, (strpos($prop, ': ') + 2));
            }
            elseif (stripos($prop, 'период') === 0)
            {
                $result['PERIOD'] = substr($prop, (strpos($prop, ': ') + 2));
            }
        }

        return $result;
    }

    /**
     * Возвращает полное наименование услуги, используется для счетов и актов
     *
     * @param type $service_code
     * @param array $params
     *
     * @return type
     */
    public static function getNameString($service_code = 'empty', array $params)
    {
        Loc::loadMessages(__FILE__);
        $params = self::handleReplacement($service_code, $params);

        if (!empty($params))
        {
            foreach ($params as $key => $value)
            {
                if (!(strpos($key, '#') === 0 && strpos($key, '#') === (strlen($key) - 1)))
                {
                    $key = '#' . $key . '#';
                    $temp_params[$key] = $value;
                }
            }
            $params = $temp_params;
        }
        $service_code = stripos($service_code, 'u') === 0 ? $service_code : 'empty';

        return Loc::getMessage('SERVICE_STRING_NAME_' . $service_code, $params);
    }

    /**
     * Возвращает краткое наименование услуги, используется в корзине и КП
     *
     * @param type $service_code
     * @param array $params
     *
     * @return type
     */
    public static function getName($service_code = 'empty', array $params)
    {
        Loc::loadMessages(__FILE__);
        if (!empty($params))
        {
            foreach ($params as $key => $value)
            {
                if (!(strpos($key, '#') === 0 && strpos($key, '#') === (strlen($key) - 1)))
                {
                    $key = '#' . $key . '#';
                    $temp_params[$key] = $value;
                }
            }
            $params = $temp_params;
        }
        $service_code = stripos($service_code, 'u') === 0 ? $service_code : 'empty';

        return Loc::getMessage('SERVICE_SHORT_NAME_' . $service_code, $params);
    }

    /**
     * Возвращает инфу об услуге
     *
     * @param integer $id ID услуги
     *
     * @return array or false
     */
    public static function getById($id)
    {
        $services_entity = Clients::getHL('service');
        $res = $services_entity->getById($id);
        if ($ar = $res->fetch())
        {
            return $ar;
        }
        else
        {
            return false;
        }

    }

    public static function isServer($iblockXmlId)
    {
        return in_array($iblockXmlId,array('u13','u1','u2'));
    }
}
