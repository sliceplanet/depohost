<?php
/**
 * Created by PhpStorm.
 * User: uk1
 * Date: 15.03.2018
 * Time: 9:29
 */

namespace Itin\Depohost;

use Bitrix\Main\Application;
use Bitrix\Main\IO\File;

/**
 * Class BillTxt Класс для работы с txt чеками
 *
 * @author Sanitskyy Vasyl
 */

class BillTxt
{
    /**
     * @var string Папка где хранятся чеки
     */
    protected $cat = '/kassa/';
    /**
     * @var int ИД заказа.
     */
    protected $id;

    /**
     * @var File Обект на файл чека.
     */
    protected $file;
    protected $file_path;

    protected $options = array(
        //Head
        'TYPE_BILL' => 0, //ТипЧека
        'TYPE_SHOP' => 0, //РежимИнтернетМагазина
        'PASSWORD' => 1, //Пароль оператора
        'CODE' => 3,  //Код налогооблажения
        //Str
        'SECTION' => 1,  //Секция
        'NDS' => 4,  //Соответствие налога в ККМ
        'PAY' => 4,  //Признак способа расчета
        'TYPE_PRODUCT' => 4,  //Признак предмета расчета
        //End
        'CUT_BILL' => 1,  //признак отрезать чек
        'NUMBER_LINE' => 1,  //на сколько строк протянуть ленту перед отрезкой
    );


    function __construct($ID){

        $this->id = $ID;
        $this->file_path = Application::getDocumentRoot(). $this->cat. 'check'. $this->id. '.txt';
        $this->file = new File($this->file_path, SITE_ID);

    }


    /**
     * Генерация txt счета и сохранение в нужную папку
     *
     * @param bool $no_rewrite Признак перезаписи существующих txt счетов
     * @return bool  Возврат успешности выполнение
     */

    public function generateBillTxt($no_rewrite = true){

        if($this->file->isExists() && $no_rewrite){

            return false;

        }else{

            $invoice = new Invoices($this->id);

            $ar = $invoice->getById();
            $services = $invoice->getFormat($ar);

            $client_id = $invoice->getClientCardId();

            $client = new Clients();
            $client->setElementId($client_id);

            $profile = $client->getProfile();

            $text = $this->getHeadBillTxt($profile);
            $text .= $this->getStrBillTxt($services);
            $text .= $this->getEndBillTxt($services);

            $this->file->putContents($text);

            copy($this->file_path, $this->file_path . '_tmp' );
            exec($c = "iconv -f UTF-8 -t Windows-1251 '{$this->file_path}_tmp' > '{$this->file_path}'");
            unlink($this->file_path . '_tmp');

            return true;

        }


    }

    protected function deletedSumbol($text){

        return str_replace(array("\"","\'",'&quot;'),array('','',''),$text);

    }

    protected function getHeadBillTxt($profile){

        $data = 'head;'.
            $this->id.';'. //НомерЧека
            $this->options['TYPE_BILL'].';'. //ТипЧека
            $profile['EMAIL']['VALUE'].';'. //ДанныеКлиента
            $this->options['TYPE_SHOP'].';'. //РежимИнтернетМагазина
            $profile['NAME']['VALUE'].';'. //Наименование клиента
            '0'.';'. //ИНН клиента
            '0'.';'. //КПП клиента
            $this->options['PASSWORD'].';'. //Пароль оператора
            $this->options['CODE'].';'  //Код налогооблажения
        ;

        return $data;

    }


    protected function getStrBillTxt($services){

        $data = '';

        if($services['SUMM'] > 0 && !empty($services['SERVICES'])){

            foreach ($services['SERVICES'] as $service){

                $data .= PHP_EOL.'str;'.
                    $this->deletedSumbol($service['UF_NAME']).';'. //Наименование товара
                    $service['UF_QUANTITY'].';'. //Количество
                    $service['UF_PRICE'].';'. //Цена
                    $this->options['SECTION'].';'. //Секция
                    $this->options['NDS'].';'. //Соответствие налога в ККМ
                    '0'.';'. //Суммовая скидка
                    $service['UF_SUMM'].';'. //СуммаПоПозиции
                    '0'.';'. //Штрихкод
                    $this->options['PAY'].';'.  //Признак способа расчета
                    $this->options['TYPE_PRODUCT'].';'  //Признак предмета расчета
                ;


            }



        }

        return $data;

    }

    protected function getEndBillTxt($services){

        $data = PHP_EOL.'end;'.
            $services['DISCOUNT'].';'. //Дополнительная скидка суммой  на весь итог чека
            $services['SUMM'].';'. //оплата видом 1(обычно это наличные),
            '0'.';'. //оплата видом 2(обычно это кредит(Безнал))(Без расчета сдачи),
            '0'.';'. //оплата видом 3(Без расчета сдачи),
            '0'.';'. //оплата видом 4(Без расчета сдачи),
            $this->options['CUT_BILL'].';'. //признак отрезать чек
            $this->options['NUMBER_LINE'].';'. //на сколько строк протянуть ленту перед отрезкой
            $services['SUMM'].';'. //Авансовая сумма предоплаты
            '0'.';'  //Постоплатная(кредитная) сумма
        ;

        return $data;

    }
}