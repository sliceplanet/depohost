<?php
/**
 * @package Itin\Depohost
 * @subpackage Sorm
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * @version 0.0.1
 * Date: 15.08.20
 * Time: 13:56
 */


namespace Itin\Depohost;


use Bitrix\Main\Config\Option;
use Bitrix\Main\IO\File;
use Bitrix\Main\Loader;

class SormV3
{
  protected $separate = ';';
  protected $endLine = "\n";
  protected $version = '3.0.0';

}