<?php

namespace Itin\Depohost;

use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CSaleBasket;
use CSaleOrder;
use CTicket;
use CUser;
use Itin\Depohost;
use Itin\Depohost\Acts;
use Itin\Depohost\Clients;
use Itin\Depohost\Invoices;
use Itin\Depohost\BillTxt;

/**
 * Класс обработчиков событий
 *
 * @author Aleksandr Terentev
 */
class EventHandlers
{
  /**
   * Обработчик события перед отправкой майла о новом заказе
   * 1 - создаем Карточку клиента и генерим договор и счет
   * 2 - это событие иницируется после сохранения профиля покупателя
   *      в этом событии мы можем получить реквизиты клиента и сгенерить договор и счет
   * 3 - сохраняем все данные об услугах в HL ClientsService
   * 4 - выставляю счет на услуги
   * 5 - отправка списка услуг
   * 6 - Если добавленна одна годовая услуга то сразу выставляю счет
   */

  function OnSaleComponentOrderOneStepComplete($ORDER_ID, $arOrder, $arParams)
  {
    if ($arOrder = CSaleOrder::GetByID($ORDER_ID))
    {
      if (intval($arOrder['USER_ID']) > 0)
      {

        $user_id = intval($arOrder['USER_ID']);
        $user = new CUser;
        $ar_user = $user->GetByID($user_id)->Fetch();
        Loader::includeModule('clients');
        $client = new Clients($user_id);
        $arProfile = $client->getFormatProfile();
        $arUserFields = array('NAME' => $arProfile['NAME'],
                              'LAST_NAME' => '',
                              'SECOND_NAME' => '');
        // Если еще нет договора

        if (empty($ar_user["UF_CONTRACT"]))
        {
          $arUserFields['LOGIN'] = $ORDER_ID;
          $arUserFields['UF_CONTRACT'] = $ORDER_ID;

          $user->Update($arOrder["USER_ID"], $arUserFields);
          $contract_id = $ORDER_ID;
          $bFirst = true; // первый раз заказывает
        }
        elseif ($ar_user['LOGIN'] != $ar_user["UF_CONTRACT"])
        {
          $arUserFields['LOGIN'] = $ar_user["UF_CONTRACT"];
          $user->Update($arOrder["USER_ID"], $arUserFields);
          $contract_id = $ar_user["UF_CONTRACT"];
          $bFirst = false;
        }


        $IBLOCK_ID = Clients::getIblockId('ccards');

        Loader::includeModule('iblock');
        $ob = new \CIBlockElement;
        $arFilter = array(
          'PROPERTY_CC_CLIENT' => $user_id
        );
        $res = $ob->GetList(array(), $arFilter);

        if ($ar = $res->fetch())
        {
          $element_id = $ar['ID'];
        }
        else
        {
          //Получаем значение типа оплаты по умолчанию
          $ar_type_payment_default = CIBlockPropertyEnum::GetList(
            array("SORT" => "ASC",
                  "VALUE" => "ASC"), array('PROPERTY_ID' => 'TYPE_PAYMENT',
                                           'DEF' => 'Y',
                                           'IBLOCK_ID' => $IBLOCK_ID)
          )->Fetch();

          // Добавляем Карточку клиента
          $element_id = $ob->Add(
            array(
              'ACTIVE' => 'Y',
              'IBLOCK_ID' => $IBLOCK_ID,
              'NAME' => $contract_id,
              'CODE' => $contract_id,
              'PROPERTY_VALUES' => array(
                'CC_CLIENT' => $user_id,
                'TYPE_PAYMENT' => $ar_type_payment_default['ID'],
              )
            )
          );

          // Генерим договор и прикрепляем к Карточке клиента
          $client->generateContractPdf($user_id);
        }
        $arID = array();
        $arBasketItems = array();
        // Получение состава заказа
        $dbBasketItems = CSaleBasket::GetList(
          array(
            "NAME" => "ASC",
            "ID" => "ASC"
          ), array(
          "ORDER_ID" => $ORDER_ID
        ), false, false, array("ID",
                               "CALLBACK_FUNC",
                               "MODULE",
                               "PRODUCT_ID",
                               "QUANTITY",
                               "PRODUCT_PROVIDER_CLASS")
        );
        while ($arItems = $dbBasketItems->Fetch())
        {
          if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
          {
            CSaleBasket::UpdatePrice($arItems["ID"], $arItems["CALLBACK_FUNC"], $arItems["MODULE"], $arItems["PRODUCT_ID"], $arItems["QUANTITY"], "N", $arItems["PRODUCT_PROVIDER_CLASS"]
            );
          }
        }

        $dbBasketItems = CSaleBasket::GetList(
          array(
            "NAME" => "ASC",
            "ID" => "ASC"
          ), array(
          "ORDER_ID" => $ORDER_ID
        ), false, false, array("ID",
                               "CALLBACK_FUNC",
                               "MODULE",
                               "PRODUCT_ID",
                               "QUANTITY",
                               "DELAY",
                               "CAN_BUY",
                               "PRICE",
                               "WEIGHT",
                               "PRODUCT_PROVIDER_CLASS",
                               "NAME",
                               'CATALOG_XML_ID',
          )
        );
        while ($arItems = $dbBasketItems->Fetch())
        {
          $arBasketItems[] = $arItems;
        }

        $month_service = false;

        foreach ($arBasketItems as $ar)
        {
          //получим elid сертификата из БД
          $arSelect = array("ID", "NAME", "PROPERTY_ELID");
          $arFilter = array("IBLOCK_ID" => 30, "ID" => $ar["PRODUCT_ID"]);
          $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
          $ob = $res->GetNextElement();
          $elid = 0;
          if ($ob)
          {
            $sslFields = $ob->GetFields();
          }


          // 3*
          $arBasket[] = $ar;
          $fields = array();
          $fields['UF_CLIENT'] = $element_id;
          $fields['UF_ORDER_ID'] = $ORDER_ID;
          $fields['UF_NAME_SHORT'] = $ar['NAME'];
          $fields['UF_CATALOG_XML_ID'] = $ar['CATALOG_XML_ID'];
          $fields['UF_QUANTITY'] = $ar['QUANTITY'];
          $fields['UF_PRICE'] = $ar['PRICE'];
          $fields['UF_SUMM'] = $ar['PRICE'] * $ar['QUANTITY'];
          $fields['UF_BASKET_ID'] = $ar['ID'];
          $fields['UF_BILL'] = 1;

          if ($sslFields["PROPERTY_ELID_VALUE"])
          {
            $fields['UF_ELID'] = $sslFields["PROPERTY_ELID_VALUE"];
          }

          $ar_el = CIBlockElement::GetByID($ar['PRODUCT_ID'])->Fetch();
          $fields['UF_PERIOD'] = $ar_el['IBLOCK_ID'] == 30 || $ar_el['IBLOCK_ID'] == 25 ? 'year' : 'month'; // ежегодная - если SSL-сертификат или домен

          if ($fields['UF_PERIOD'] == 'month')
          {
            $month_service = true;
          }

          // Свойства товара в корзине
          $db_res = CSaleBasket::GetPropsList(
            array(
              "SORT" => "ASC",
              "NAME" => "ASC"
            ), array("BASKET_ID" => $ar['ID'])
          );
          $arItemProp = array();
          $excludeProps = [
            'unique_id',
            'price_1month',
            'is-ms',
            'is-solution',
            'is-domain',
            'is-1c',
            'is-isp',
            'is-configurator',
            'is-ssl',
            'is-vds',
            'is_service',
          ];
          while ($ar_res = $db_res->Fetch())
          {
            if($ar_res['CODE'] === 'years') {
              $yearString = (int) $ar_res["VALUE"] === 1 ? 'год' : 'года';
              $arItemProp[] = $ar_res["NAME"] . ": " . $ar_res["VALUE"]. ' '.$yearString;
            }
            else if (strpos($ar_res['CODE'], 'XML_ID') === false && !in_array($ar_res['CODE'], $excludeProps))
            {
              $arItemProp[] = $ar_res["NAME"] . ": " . $ar_res["VALUE"];
            }
          }

          $fields['UF_PROPERTIES'] = !empty($arItemProp) ? $arItemProp : array();
          $fields['UF_NAME'] = Services::getNameString($fields['UF_CATALOG_XML_ID'], $fields);

          $ClientService = Clients::getHL('service');
          $arParams = array(
            'filter' => array('=UF_BASKET_ID' => $fields['UF_BASKET_ID'])
          );
          $res_service = $ClientService->getList($arParams);

          if ($ar_service = $res_service->fetch())
          {
            $ClientService->update($ar_service['ID'], $fields);
            $fields['ID'] = $ar_service['ID'];
          }
          else
          {
            $result = $ClientService->add($fields);
            if ($result->isSuccess())
            {

              $fields['ID'] = $result->getId();

            }

          }

          $arServices[] = $fields;

        }
        if (!$bFirst && !empty($arServices))
        {
          //Отправка списка услуг
          Services::sendToMailList($arServices, array('client_id' => $element_id));
        }
        // END Получения состава заказа
        // 4*
        // Определяем тип плательщика, если не физ. лицо, выставляем счет
//                $person_type_id = intval($client->getPersonTypeId());
//                if ($person_type_id != 1)
//                {
//                    $invoice_ob = new Invoices;
//                    $invoice_ob->setClientCardId($element_id);
//                    $invoice_ob->add();
//                }


        /**
         * 6 - Если добавленна одна годовая услуга услуга то сразу выставляю счет
         */


        if (!empty($arServices) && !$month_service)
        {

          $invoice = new Invoices;
          $default_services = $invoice->getDefaultValueProperties();

          $invoice->setClientCardId($element_id);

          $values = $default_services;
          $values['CLIENT'] = $element_id;
          $values['DATE'] = FormatDate('SHORT', time());

          $summ = 0;

          foreach ($arServices as $s)
          {
            var_dump($values['SERVICES']);
            if (isset($s['ID']))
            {
              if (!is_array($values['SERVICES']))
              {
                $values['SERVICES'] = [];
              }
              $values['SERVICES'][] = $s['ID'];

              $summ += $s['UF_SUMM'];

            }

          }
          if ($values['DISCOUNT'] > 0)
          {

            $summ = $summ - intval($summ * $values['DISCOUNT'] / 100);

          }

          $values['SUMM'] = $summ;

          $invoice->add(
            array(
              'ACTIVE' => 'Y',
              'PROPERTY_VALUES' => $values
            )
          );

        }


      }
    }
  }

  /**
   * Сохраняем псевдозашифрованный пароль, для последующей отправки
   */

  function PasswordSave(&$arFields)
  {
    if (!empty($arFields['PASSWORD']))
    {
      Loader::includeModule('clients');
      $arFields['UF_PASSWORD'] = Clients::encodePassword($arFields['PASSWORD']);
    }
  }

  /**
   * Обработчик элементов инфболока Акта
   * 1 - генерим имя элемента из номера и даты акта
   */

  function InvoiceHandler(&$arFields)
  {
    Loader::includeModule('clients');
    $IBLOCK_ID = Depohost\Info::getIblockId('acts');
    // 1*
    if (!empty($arFields['IBLOCK_ID']) && $arFields['IBLOCK_ID'] == $IBLOCK_ID && !$arFields['ACTIVATED'])
    {
      $ob = new PropertyTable;
      $arParams = array(
        'select' => array('ID',
                          'CODE'),
        'filter' => array('IBLOCK_ID' => $IBLOCK_ID,
                          'CODE' => array('NUMBER',
                                          'DATE')),
      );
      $res = $ob->getList($arParams);
      while ($ar = $res->fetch())
      {
        switch ($ar['CODE'])
        {
          case 'DATE':
            $date_id = $ar['ID'];
            $ar_temp = current($arFields['PROPERTY_VALUES'][$date_id]);
            $date = $ar_temp['VALUE'];
            break;
//                    case 'NUMBER':
//                        $number_id = $ar['ID'];
//                        break;
          default:
            break;
        }
      }
      if (!empty($arFields['PROPERTY_VALUES']['DATE']))
      {
        $date = $arFields['PROPERTY_VALUES']['DATE'];
      }
      $name = !empty($number) ? '№' . $number : 'б/н';
      $name .= ' от ' . $date;
      $arFields['NAME'] = $name;
    }
  }

  /**
   * Обработчик элементов инфболока Счета
   * после добавления
   *
   * 1 - генерю pdf счет и прицепляю счет
   * 2 - При смене статуса счета на Оплачен
   * 3 - Выставляю количество актов к счету по умолчанию
   * 4 - В карточке клиента обновляю дату последнего счета
   */

  function addInvoiceToClientHandler(&$arFields)
  {
    Loader::includeModule('clients');
    $IBLOCK_ID = Depohost\Info::getIblockId('invoices');

    if (!empty($arFields['IBLOCK_ID']) && $arFields['IBLOCK_ID'] == $IBLOCK_ID && $arFields['ID'] > 0 && !$arFields['ACTIVATED'])
    {
      $ob = new PropertyTable;
      $arParams = array(
        'select' => array('ID',
                          'CODE'),
        'filter' => array('IBLOCK_ID' => $arFields['IBLOCK_ID'],
                          'CODE' => array('NUMBER',
                                          'DATE')),
      );
      $res = $ob->getList($arParams);
      while ($ar = $res->fetch())
      {
        switch ($ar['CODE'])
        {
          case 'DATE':
            $date_id = $ar['ID'];
            $ar_temp = current($arFields['PROPERTY_VALUES'][$date_id]);
            $date = $ar_temp['VALUE'];
            break;
          case 'NUMBER':
            $number_id = $ar['ID'];
            break;
          default:
            break;
        }
      }

      if (empty($arFields['PROPERTY_VALUES']['NUMBER']) && empty($arFields['PROPERTY_VALUES'][$number_id]) || (!empty($arFields['PROPERTY_VALUES'][$number_id]) && $arFields['PROPERTY_VALUES'][$number_id] != $arFields['ID']) || (!empty($arFields['PROPERTY_VALUES']['NUMBER']) && $arFields['PROPERTY_VALUES']['NUMBER'] != $arFields['ID'])
      )
      {
        $ob = \CIBlockElement::GetList(array(), array('ID' => $arFields['ID'],
                                                      false,
                                                      false,
                                                      array('ID',
                                                            'PROPERTY_*')))->GetNextElement();
        $temp_ar_property = $ob->GetProperties();
        foreach ($temp_ar_property
                 as
                 $arProperty)
        {
          if ($arProperty['CODE'] == 'NUMBER')
          {
            $arProperty['VALUE'] = $arFields['ID'];
          }
          elseif ($arProperty['CODE'] == 'DATE')
          {
            $date = $arProperty['VALUE'];
          }
          elseif ($arProperty['PROPERTY_TYPE'] == 'L')
          {
            $arProperty['VALUE'] = $arProperty['VALUE_ENUM_ID'];
          }
          $arProperties[$arProperty['CODE']] = $arProperty['VALUE'];
        }
        $ob_el = new \CIBlockElement;
        $name = "№" . $arFields['ID'] . " от " . $date;
        $ob_el->Update($arFields['ID'], array('NAME' => $name,
                                              'PROPERTY_VALUES' => $arProperties));
      }

      $invoice = new Invoices($arFields['ID']);

      //4* update last invoice date in client card
      $client = new Clients();
      $client_id = $invoice->getClientCardId();
      $client->setElementId($client_id);
      $client->updateLastInvoiceDate($date);

      // 1*
      if (!empty($arFields['PROPERTY_VALUES']['ELID']))
      {
        $invoice->generatePdf(false, $arFields['PROPERTY_VALUES']['ELID']);
      }
      else
      {
        $invoice->generatePdf();
      }
      // 2*
      $bChanged = $invoice->isChangeStatus();
      $status = $invoice->getStatus();

      if ($bChanged && $status['VALUE_XML_ID'] == 'no-paid')
      {
        $invoice->setChanged(false);
      }
      elseif (!$bChanged && $status['VALUE_XML_ID'] == 'paid')
      {
        $invoice->sendToMailOnPaid();
        $invoice->setChanged(true);
      }

      // 3*
      $ar = $invoice->getById();
      if (!(int)$ar['COUNT_ACTS']['VALUE'] > 0)
      {
        $invoice->updateProperties(array('COUNT_ACTS' => 1));
      }
    }
  }

  /**
   * Обработчик элементов инфоблока Акты
   * после добавления
   *
   * 1 - генерю pdf акт и прицепляю акт
   * 2 - отправляю письмо с аттачем акта
   * 3 - добавляю инфу об акте в карточку клиента
   */

  function addActToClientHandler(&$arFields)
  {
    Loader::includeModule('clients');
    $IBLOCK_ID = Depohost\Info::getIblockId('acts');

    if (!empty($arFields['IBLOCK_ID']) && $arFields['IBLOCK_ID'] == $IBLOCK_ID && $arFields['ID'] > 0 && !$arFields['ACTIVATED'])
    {
      $act = new Acts($arFields['ID']);
      // 1*
      $act->generatePdf();
      // 2*
      $act->sendToMail();
      // 3*
      $ob = new CIBlockProperty;
      $CLIENT_IBLOCK_ID = Depohost\Info::getIblockId('ccards');
      if (!isset($arFields['PROPERTY_VALUES']['CLIENT']))
      {

        $res = $ob->GetList(array(), array('CODE' => 'CLIENT',
                                           'IBLOCK_ID' => $IBLOCK_ID));
        if ($ar = $res->Fetch())
        {
          $prop_id = $ar['ID'];
          if (isset($arFields['PROPERTY_VALUES'][$prop_id]))
          {
            $client_elem_id = is_array($arFields['PROPERTY_VALUES'][$prop_id]) ? current($arFields['PROPERTY_VALUES'][$prop_id]) : $arFields['PROPERTY_VALUES'][$prop_id];
          }
        }
      }
      else
      {
        $client_elem_id = is_array($arFields['PROPERTY_VALUES']['CLIENT']) ? current($arFields['PROPERTY_VALUES']['CLIENT']) : $arFields['PROPERTY_VALUES']['CLIENT'];
      }
      if (!empty($client_elem_id))
      {
        $res = CIBlockElement::GetProperty($CLIENT_IBLOCK_ID, $client_elem_id, array(), array('CODE' => 'CC_ACTS',
                                                                                              'EMPTY' => 'N'));
        $arValues = array($arFields['ID']);
        while ($ar = $res->Fetch())
        {
          $arValues[] = $ar['VALUE'];
        }
        \CIBlockElement::SetPropertyValuesEx($client_elem_id, $CLIENT_IBLOCK_ID, array('CC_ACTS' => $arValues));
      }
    }
  }

  /**
   * Добавляем к письму аттач
   */

  function MailAttach($event, $lid, $arFields)
  {
    if (!empty($arFields['ATTACH']))
    {
      $ar_attach = $arFields['ATTACH'];
      require_once $_SERVER["DOCUMENT_ROOT"] . '/local/modules/clients/include/mail_attach.php';
      if (is_array($lid))
      {
        $lid = $lid[0];
      }
      SendAttache($event, $lid, $arFields, $ar_attach);
      return false;
    }
  }

  /*
   * Обработчик записей Клиентских услуг
   */

  function ClientsServiceHandler(Entity\Event $event)
  {
    $errors = array();

    $primary = $event->getParameter('id');
    $fields = $event->getParameter('fields');

    $changedFields = array();
    $unsetFields = array();

    $result = new Entity\EventResult();

    if (array_key_exists('UF_CLIENT', $fields))
    {
      $client_id = $fields['UF_CLIENT'];
    }
    elseif (isset($primary['ID']) && (int)$primary['ID'] > 0)
    {
      $arService = Services::getById($primary['ID']);
      $client_id = $arService['UF_CLIENT'];
    }

    if ($event->getEventType() == 'ClientsServiceOnBeforeUpdate')
    {
      if (array_key_exists('UF_PRICE', $fields) && array_key_exists('UF_QUANTITY', $fields))
      {
        $changedFields = array('UF_SUMM' => intval((int)$fields['UF_PRICE'] * (int)$fields['UF_QUANTITY']));
      }
      // Проверяем есть ли мониторинг сервера
      if (array_key_exists('UF_IP_ADDR', $fields))
      {
        if (!is_array($fields['UF_IP_ADDR']) && is_array(unserialize($fields['UF_IP_ADDR'])))
        {
          $fields['UF_IP_ADDR'] = unserialize($fields['UF_IP_ADDR']);
        }
        Loader::includeModule('monitoring');
        $client = new Clients;
        $user_id = $client->getUserId($client_id);
        $newValues = array_filter(array_values($fields['UF_IP_ADDR']));
        if (empty($newValues))
        {
          Monitoring\Servers::cleanAllSettingsNotify($user_id);
        }
        else
        {
          if(empty($arService)) {
            $arService = Services::getById($primary['ID']);
          }
          $oldValues = array_values($arService['UF_IP_ADDR']);
          // было ли обновление ip-адресов
          $diffAdded = array_diff($newValues,$oldValues);
          $diffDeleted = array_diff($oldValues,$newValues);
          if(count($diffAdded) || count($diffDeleted)) {
            foreach ($diffAdded as $key => $ip)
            {
              if(array_key_exists($key, $diffDeleted)) {
                // значит произошло обновление
                $oldIp = $diffDeleted[$key];
                Monitoring\Servers::updateSettingsNotify($oldIp, $user_id, $ip);
              }
            }
            // при добавлении ip-адреса - ничего не предпринимаем
            foreach ($diffDeleted as $key => $ip)
            {
              if(!array_key_exists($key, $diffAdded)) {
                // значит произошло удаление
                Monitoring\Servers::cleanSettingsNotify($ip, $user_id);
              }
            }
          }
        }
      }
    }
    if ($event->getEventType() == 'ClientsServiceOnAfterAdd'
      || $event->getEventType() == 'ClientsServiceOnAfterUpdate'
    )
    {
      Clients::recalcAbonplata($client_id);
    }

    if (!empty($errors))
    {
      $result->setErrors($errors);
    }
    else
    {
      $result->modifyFields($changedFields);
      $result->unsetFields($unsetFields);
    }
    return $result;
  }

  /**
   * Обработчик события перед обновлением тикета
   *
   * 1 - Снимает флаги с услуг перезагрузки или ipkvm
   */

  function handlerBeforeUpdateTicket($arFields)
  {
    // 1*
    if ($arFields['CLOSE'] == 'Y')
    {
      $ID = $arFields['ID'];
      $service = Info::getHL('service');
      $arParams = array(
        'select' => array('ID',
                          'UF_REBOOT',
                          'UF_IPKVM'),
        'filter' => array(
          'LOGIC' => 'OR',
          'UF_REBOOT' => $ID,
          'UF_IPKVM' => $ID
        ),
        'limit' => 1
      );
      $res = $service->getList($arParams);
      if ($ar = $res->fetch())
      {
        if ($ar['UF_REBOOT'] == $ID)
        {
          $fields = array('UF_REBOOT' => false);
        }
        else
        {
          $fields = array('UF_IPKVM' => false);
        }
        $service->update($ar['ID'], $fields);
      }
    }
    return $arFields;
  }

  /**
   * Обработчик события обновления элементов в инфоблоки
   *
   * 1 -  при деактивации Карточки клиента деактивируем пользователя, счета, акты, учетку в ISPmanager
   *      при активации Карточки клиента активируем пользователя, счета, акты, учетку в ISPmanager
   */

  function HandlerAfterIBlockElementUpdate($arFields)
  {
    Loader::includeModule('clients');
    $IBLOCK_ID = Depohost\Info::getIblockId('ccards');
    // 1*
    if (!empty($arFields['IBLOCK_ID']) && $arFields['IBLOCK_ID'] == $IBLOCK_ID && $arFields['ID'] > 0 && !empty($arFields['ACTIVE']))
    {
      $client = new Clients;
      $USER_ID = $client->getUserId($arFields['ID']);
      $fields = $client->getById($arFields['ID']);
      $username = $fields['NAME'];
      $password = $client->getPassword($USER_ID);
      $arInvoice = $fields['CC_BILLING']['VALUE'];
      $arActs = $fields['CC_ACTS']['VALUE'];
      $ar_fields = array('ACTIVE' => $arFields['ACTIVE'],
                         'ACTIVATED' => true);
      $user = new CUser;
      $user->Update($USER_ID, $ar_fields);

      $el = new \CIBlockElement;
      foreach ($arInvoice
               as
               $value)
      {
        $el->Update($value, $ar_fields);
      }
      foreach ($arActs
               as
               $value)
      {
        $el->Update($value, $ar_fields);
      }
      $ispFtp = new Isp('ftp');
      $ispDns = new Isp('dns');

      $paramFtp = array('user' => 'ftp' . $username);
      $paramDns = array('user' => 'dns' . $username);
      if ($arFields['ACTIVE'] == 'N')
      {
        $ispFtp->deactivate($paramFtp);
        $ispDns->deactivate($paramDns);

        Loader::includeModule('monitoring');
        $arIPs = Monitoring\Servers::getUserIPs($USER_ID);
        if (!empty($arIPs))
        {
          Monitoring\Servers::cleanAllSettingsNotify($USER_ID);
        }
      }
      else
      {
        $ispFtp->activate($paramFtp);
        $ispDns->activate($paramDns);
      }
    }

    if (Depohost\Info::getIblockId('invoices') == $arFields['IBLOCK_ID'])
    {
      $invoice = new Invoices($arFields['ID']);
      $bChanged = $invoice->isChangeStatus();
      $status = $invoice->getStatus();
      if ($bChanged && $status['VALUE_XML_ID'] == 'no-paid')
      {
        $invoice->setChanged(false);
      }
      elseif (!$bChanged && $status['VALUE_XML_ID'] == 'paid')
      {
        $invoice->sendToMailOnPaid();
        $invoice->setChanged(true);
      }
    }
  }

  /**
   * Обработчик события OnBeforeIBlockElementDeleteHandler
   * 1* Обновляет дату последнего счета
   * 2* Устанавливает флаг Есть ли неоплаченные счета если удаляется счет
   *
   * @param $ID
   */
  function OnBeforeIBlockElementDeleteHandler($ID)
  {
    $res = CIBlockElement::GetByID($ID);
    if ($ob = $res->GetNextElement(true, false))
    {
      $arFields = $ob->GetFields();

      if (Depohost\Info::getIblockId('invoices') == $arFields['IBLOCK_ID'])
      {
        //1* update last invoice date in client card
        $invoice = new Invoices($ID);
        $client = new Clients();
        $client_id = $invoice->getClientCardId();
        $client->setElementId($client_id);
        $client->updateLastInvoiceDate();

        //2*
        $arFields['PROPERTIES'] = $ob->GetProperties(array(), array('CODE' => 'CLIENT'));
        $client_id = $arFields['PROPERTIES']['CLIENT']['VALUE'];
        Clients::setNoPaidEx($client_id, $ID);

      }
    }
  }

  /**
   * Обработчик события OnAfterIBlockElementAdd
   *
   * 1 - отправляю письмо с аттачем счета
   */

  function HandlerAfterIBlockElementAdd($arFields)
  {
    Loader::includeModule('clients');
    $IBLOCK_ID = Depohost\Info::getIblockId('invoices');

    if (!empty($arFields['IBLOCK_ID']) && $arFields['IBLOCK_ID'] == $IBLOCK_ID && $arFields['ID'] > 0 && !$arFields['ACTIVATED'])
    {
      $invoice = new Invoices($arFields['ID']);
      // 2*
      $b = $invoice->sendToMail();
    }
//        if (array_key_exists('IBLOCK_ID', $arFields) && array_key_exists('ID', $arFields) && Depohost\Info::getIblockId('ccards') == $arFields['IBLOCK_ID'])
//        {
//            Clients::recalcAbonplata($arFields['ID']);
//        }
  }

  /**
   * Обработчик события OnAfterIBlockElementSetPropertyValuesEx
   *
   * 1 - При смене статуса счета на Оплачен
   */

  function HandlerAfterIBlockElementSetPropertyValuesEx($ID, $IBLOCK_ID, $PROPERTY_VALUES, $FLAG)
  {
    Loader::includeModule('clients');
    if (Depohost\Info::getIblockId('invoices') == $IBLOCK_ID)
    {
      $invoice = new Invoices($ID);
      $bChanged = $invoice->isChangeStatus();
      $status = $invoice->getStatus();
      if (!array_key_exists('LAST_INVOICE_DATE', $PROPERTY_VALUES))
      {
        //update last invoice date in client card
        $client = new Clients();
        $client_id = $invoice->getClientCardId();
        $client->setElementId($client_id);
        $client->updateLastInvoiceDate();
      }


      if ($bChanged && $status['VALUE_XML_ID'] == 'no-paid')
      {
        $invoice->setChanged(false);
      }
      elseif (!$bChanged && $status['VALUE_XML_ID'] == 'paid')
      {
        $invoice->sendToMailOnPaid();
        $invoice->setChanged(true);


      }
    }
//        if (Depohost\Info::getIblockId('ccards') == $IBLOCK_ID)
//        {
//            Clients::recalcAbonplata($ID);
//        }
  }

  /**
   * Обработчик события OnAfterIBlockElementSetPropertyValuesEx
   *
   * 1 - При смене статуса счета на Оплачен
   * 2 - Для физ лиц
   * 3 - Генерация текстового Счета в каталог kassa
   */
  function HandlerAfterIBlockElementGenerateBillTxt($ID, $IBLOCK_ID, $PROPERTY_VALUES, $FLAG)
  {

    Loader::includeModule('clients');
    if (Depohost\Info::getIblockId('invoices') == $IBLOCK_ID)
    {

      $invoice = new Invoices($ID);
      $bChanged = $invoice->isChangeStatus();
      $status = $invoice->getStatus();
      $client = new Clients();
      $client_id = $invoice->getClientCardId();
      $client->setElementId($client_id);
      $profail_id = $client->getPersonTypeId();

      if (!$bChanged && $status['VALUE_XML_ID'] == 'paid' && $profail_id == 1)
      {

        $billTxt = new BillTxt($ID);
        $billTxt->generateBillTxt();

      }

    }


  }


  /**
   * Обработчик события перед добавление записи в корзину
   *
   * 1 - Изменяет название услуги по шаблону
   *
   * @param array $arFields
   */
  function OnAfterBasketAddHandler($ID, $arFields)
  {
    // 1*
    Loc::loadMessages(__FILE__);
    if (strpos($arFields['CATALOG_XML_ID'], 'u') === 0)
    {

      $name = Services::getName($arFields['CATALOG_XML_ID'], $arFields);
      \CSaleBasket::Update($ID, array('NAME' => $name,
                                      'PRODUCT_PROVIDER_CLASS' => ''));
    }
  }

  /**
   *
   * @param array $arFields Массив маркеров
   * @param array $arTemplate Инфо шаблона
   */
  function HandlerOnBeforeEventSend(&$arFields, $arTemplate)
  {
    if (array_key_exists('EVENT_NAME', $arTemplate) && stripos($arTemplate['EVENT_NAME'], 'TICKET') !== false)
    {
      $arFields['AUTO_CLOSE_MESSAGE'] = $arFields['WHAT_CHANGE'];
    }

    if (array_key_exists('EVENT_NAME', $arTemplate)
      && stripos($arTemplate['EVENT_NAME'], 'TICKET') !== false
      && array_key_exists('OWNER_USER_ID', $arFields)
      && (int)$arFields['OWNER_USER_ID'] > 0
    )
    {
      /**
       * Добавляем в письмо маркеры
       * #CLIENT_NAME# - Наименование клиента
       * #CONTRACT_ID# - Номер договора
       * #FIRST_MESSAGE_BODY# - Текст первого сообщения в обращении
       *
       * Добавляем второй e-mail из профиля покупателя в маркер #OWNER_EMAIL#
       */

      $client_id = (int)$arFields['OWNER_USER_ID'];
      $client = new Clients($client_id);
      $arProfile = $client->getFormatProfile();
      $arFields['CLIENT_NAME'] = $arProfile['NAME']; // Наименование клиента
      $arFields['CONTRACT_ID'] = $arFields['OWNER_USER_LOGIN'];   //Номер договора
      if (array_key_exists('SECOND_EMAIL', $arProfile) && filter_var($arProfile['SECOND_EMAIL'], FILTER_VALIDATE_EMAIL))
      {
        $arFields['OWNER_EMAIL'] .= ',' . $arProfile['SECOND_EMAIL'];
      }
      if (stripos($arFields['MODIFIED_MODULE_NAME'], 'auto closing') !== false)
      {
        $arFields['AUTO_CLOSE_MESSAGE'] = Loc::getMessage('AUTO_CLOSE_MESSAGE_TEXT');
      }

      if (strpos($arTemplate['EVENT_NAME'], 'TICKET_NEW') !== false)
      {
        $arFields['FIRST_MESSAGE_BODY'] = $arFields['MESSAGE_BODY']; //Текст первого сообщения в обращении
      }
      else
      {
        Loader::includeModule('support');
        $by = 's_number';
        $order = 'asc';
        $isFiltered = true;
        $arFilter = array(
          'TICKET_ID' => (int)$arFields['ID'],
          'TICKET_ID_EXACT_MATCH' => 'Y',
          'IS_HIDDEN' => 'N',
          'IS_MESSAGE' => 'Y',
        );
        $res = CTicket::GetMessageList($by, $order, $arFilter, $isFiltered);
        if ($ar = $res->Fetch())
        {
          $arFields['FIRST_MESSAGE_BODY'] = $ar['MESSAGE']; //Текст первого сообщения в обращении
        }
      }

    }
    if (array_key_exists('EVENT_NAME', $arTemplate)
      && $arTemplate['EVENT_NAME'] == 'USER_PASS_REQUEST'
      && array_key_exists('USER_ID', $arFields)
      && (int)$arFields['USER_ID'] > 0
    )
    {
      $client = new Clients((int)$arFields['USER_ID']);

      $arFields['PASSWORD'] = Clients::getPassword((int)$arFields['USER_ID']);
      $arFields['EMAILS'] = implode(',', $client->getEmails());
    }


  }

  function OnSaleComponentOrderOneStepOrderPropsHandler(&$arResult, $arUserResult, $arParams)
  {
    if (!empty($arUserResult['ORDER_PROP']))
    {
      foreach ($arResult['ORDER_PROP']['USER_PROPS_Y']
               as
               $arProp)
      {
        if ($arProp['CODE'] == 'OSNOVANIE')
        {
          $osnovaniePropId = $arProp['ID'];
        }
        elseif ($arProp['CODE'] == 'DOVERENNOST')
        {
          $doverennostPropId = $arProp['ID'];
        }
      }

      if ($arUserResult['ORDER_PROP'][$osnovaniePropId] == 20 && $arUserResult['ORDER_PROP'][$doverennostPropId] == '')
      {
        $arResult["ERROR"][] = GetMessage("SOA_ERROR_REQUIRE") . " \"" . $arResult['ORDER_PROP']['USER_PROPS_Y'][$doverennostPropId]['NAME'] . "\"";;
      }
    }

  }
}

//END class EventHandler
