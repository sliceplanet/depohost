<?php
/**
 * Класс для взаимодействия с IspManager
 *
 * @author Aleksandr Terentev
 */
namespace Itin\Depohost;
use Bitrix\Main\Localization\Loc;
use SimpleXMLElement;
use Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

class Isp
{
    /**@var array   Массив ошибок в процессе работы объекта */
    var $arErrors = array();
    /**@var boolean Авторизован ли*/
    private $isAuth;
    /**@var string  url веб-сервиса */
    private $url;
    /**@var string  индефикатор сессии */
    private $session_id;
    /**@var array   Параметры взаимодействия */
    private $arParams;

    /*
     * @params string $service  Тип службы <ftp|dns>
     */
    function __construct($service)
    {
        $moduleId = 'clients';
        $param['url'] = Option::get($moduleId, 'url_'.$service);
        $param['admin_login'] = Option::get($moduleId, 'login_'.$service);
        $param['admin_password'] = Option::get($moduleId, 'password_'.$service);
        $param['service'] = $service;
        $param['isp_version'] = 5;

        $this->setParams($param);
    }
    /*
     * Авторизация в панели
     *
     * @params  array $param    Массив параметров вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *      )
     */
    public function auth($param)
    {
        extract($param);
        $params = array(
            'out' => 'xml',
            'func' => 'auth',
            'username' => (string)$user,
            'password' => (string)$password,
        );
        $data = $this->query($params);

        if(!empty($data->auth)) {

            $this->session_id = (string)$data->auth;
            $this->isAuth = true;

            return true;
        }
        elseif($data->error->param == 'badpassword')
        {
            $bUpdate = $this->updateUser($params);
            if ($bUpdate)
            {
                $this->isAuth = true;
                return $bUpdate;
            }
            else
            {
                $this->isAuth = false;
                $msg = Loc::getMessage('ERROR_AUTH');
                $this->setError($msg);
                return false;
            }
        }
        elseif($data===false)
        {
            $this->isAuth = false;
            $msg = Loc::getMessage('ERROR_AUTH_SERVICE_NOT_AVAILABLE');
            $this->setError($msg);
        }
        else
        {
            $this->isAuth = false;
            $msg = $data->error->msg;
            $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_AUTH');
            $this->setError($msg);
            return false;
        }
    }
    /*
     * Создание нового пользователя
     */
    public function createUser($param)
    {
        extract($param);
        $session_id = $this->authAdmin();
        if ($session_id)
        {
            $arAdmin = $this->getAdmin();
            $params = array(
                'out' => 'xml',
                'auth' => $session_id,
                'func' => 'user.edit',
                'sok' => 'yes',
                'name' => (string)$user,
                'passwd' => (string)$password,
                'confirm' => (string)$password,
//                'preset' => 'FTP',
                'limit_quota' => 10000,
                'limit_db' => 0,
                'limit_db_users' => 0,
                'limit_ftp_users' => 4,
                'limit_domains' => 20,
                'limit_emaildomains' => 0,
                'limit_emails' => 0,
            );
            $data = $this->query($params);
            if (!isset($data->ok))
            {
                $msg = $data->error->msg;
                $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_CREATE_USER');
                $this->setError($msg);

                if ((string)$data->error['type'] == 'exists')
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            $this->setError(Loc::getMessage('ERROR_AUTH_ADMIN'));
            return false;
        }

    }
     /*
     * Обновление пользователя
     */
    public function updateUser($param)
    {
        extract($param);
        $session_id = $this->authAdmin();

        if ($session_id)
        {
            $arAdmin = $this->getAdmin();
            $session_id = $this->session_id;

            $params = array(
                'out' => 'xml',
                'auth' => $session_id,
                'func' => 'user.edit',
                'sok' => 'yes',
                'lang' => 'ru',
                'name' => (string)$username,
                'elid' => (string)$username,
                'passwd' => (string)$password,
                'confirm' => (string)$password,
                'limit_quota' => 10000,
                'limit_db' => 0,
                'limit_db_users' => 0,
                'limit_ftp_users' => 4,
                'limit_domains' => 20,
                'limit_emaildomains' => 0,
                'limit_emails' => 0,
            );
			if($this->arParams['service']==='dns'){
				$params['startpage'] = 'domain';
			}
            $data = $this->query($params);

            if (isset($data->error) && $data->error->param=='elid')
                {
                    unset($params['elid']);
                    $data = $this->query($params);
                }

            if (!isset($data->ok))
            {
                $msg = $data->error->msg;
                $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_CREATE_USER');
                $this->setError($msg);
                if ((string)$data->error['type'] == 'value' && (string)$data->error['object'] == 'passwd')
                {
                    // низкая сложность пароля пользователя
                    $new_password = (string)randString(10);
                    $params = array(
                        'out' => 'xml',
                        'func' => 'auth',
                        'username' => (string)$username,
                        'password' => $new_password,
                    );
                    return $this->updateUser($params);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return $password;
            }
        }
        else
        {
            $this->setError(Loc::getMessage('ERROR_AUTH_ADMIN'));
            return false;
        }

    }
    /*
     * Возвращает логин и пароль админа ispmanager
     */
    private function getAdmin()
    {
        $params = array('user'=>$this->arParams['admin_login'], 'password'=>$this->arParams['admin_password']);
        return $params;
    }
    /*
     * авторизация под админом панели IspManager
     *
    */
    private function authAdmin()
    {
        $params = $this->getAdmin();
        $session_id = $this->auth($params);
        if (!$session_id)
        {
            $msg = Loc::getMessage('ERROR_AUTH_ADMIN');
            $this->setError($msg);
            return false;
        }
        return $session_id;
    }
    /*
     * Установка параметров для запросов к веб-сервису
     *
     * @params array $params    Массив параметор вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *          'url' => <url веб-сервиса на котором требуется авторизоваться> *Необязательный параметр, может быть установлен с помощью $this->setParams
     *      )
     */
    public function setParams($param)
    {
        extract($param);
        if (!empty($url))
        {
            $this->url = $url;
        }
        $this->arParams = $param;
    }
    /*
     * Запрос к веб-сервису
     *
     * @params array $param     Массив параметров запроса
     *
     *
     */
    public function query($param)
    {
        $param = http_build_query($param);
        $responce = file_get_contents($this->url . '?' . $param);
        if ($responce!==false)
        {
            $data = new SimpleXMLElement($responce);
        }
        else
        {
            $data = false;
            $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_AUTH_SERVICE_NOT_AVAILABLE');
            $this->setError($msg);
        }
        return $data;
    }
    /*
     * Создать пользователя ftp
     *
     * @params array $param Массив параметор вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *      )
     */
    public function createFtpUser($param)
    {
        extract($param);
        $url = !empty($url) ? $url : $this->url;

        $bCreate = $this->createUser($param);
        if ($bCreate)
        {
            $session_id = $this->session_id;
            $params = array(
                'out' => 'xml',
                'auth' => $session_id,
                'func' => 'ftp.user.edit',
                'sok' => 'yes',
                'name' => (string)$user,
                'owner' => (string)$user,
                'passwd' => (string)$password,
                'confirm' => (string)$password,
                'home' => '/'
            );
            $data = $this->query($params);
            if (!isset($data->ok))
            {
                $msg = $data->error->msg;
                $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_CREATE_FTP_USER');
                $this->setError($msg);
                if ((string)$data->error['type'] == 'exists')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

     /*
     * Создать пользователя ftp
     *
     * @params array $param Массив параметор вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *      )
     */
    public function createDnsUser($param)
    {
        extract($param);
        $url = !empty($url) ? $url : $this->url;
        $this->authAdmin();
        $session_id = $this->session_id;
        $params = array(
            'out' => 'xml',
            'auth' => $session_id,
            'func' => 'user.edit',
            'sok' => 'yes',
            'name' => (string)$user,
            'owner' => 'root',
            'passwd' => (string)$password,
            'confirm' => (string)$password,
            'preset' => 'DNS',
			      'startpage' => 'domain',
        );
        $data = $this->query($params);
        if (!isset($data->ok))
        {
            $msg = $data->error->msg;
            $msg = !empty($msg) ? $msg : Loc::getMessage('ERROR_CREATE_DNS_USER');
            $this->setError($msg);
            $params = array(
                'out' => 'xml',
                'auth' => $session_id,
                'func' => 'user.edit',
                'sok' => 'yes',
                'name' => (string)$user,
                'elid' => (string)$user,
                'passwd' => (string)$password,
                'confirm' => (string)$password,
                'domainlimit' => '20',
            );
            $data = $this->query($params);
            if (!isset($data->ok))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    /*
     * Добавляет ошибку в массив ошибок
     *
     * @params string $message      Описание ошибки
     */
    private function setError($message)
    {
        $this->arErrors[] = (string) $message;
    }
    /*
     * Открывает личный кабинет в ispmager после авторизации и т.д.
     *
     * @params array $param     массив параметров с которыми была авторизация
     */
    public function open($param)
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $param = http_build_query($param);
        LocalRedirect(str_replace('out=xml','out=html',str_replace('http:','https:', $this->url)  . '?' .  $param));
    }
    /*
     * Деаквитивирует учетку в ISPmanager
     *
     * @params array $param Массив параметор вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *      )
     */
    public function deactivate($param)
    {
        extract($param);
        $url = $this->url;
        $this->authAdmin();
        $session_id = $this->session_id;
        $params = array(
            'out' => 'xml',
            'auth' => $session_id,
            'elid' => (string) $user,
            'name' => (string) $user,
        );
        $params['func'] = $this->arParams['isp_version'] == 4 ? 'user.disable' : 'user.suspend';
        return $this->query($params);
    }
    /*
     * Удаляет учетку в ISPmanager
     *
     * @params array $param Массив параметор вида:
     *      array(
     *          'user' => <username>,
     *          'password' => <password>,
     *      )
     */
    public function delete($param)
    {
        extract($param);
        $url = $this->url;
        $this->authAdmin();
        $session_id = $this->session_id;
        $params = array(
            'out' => 'xml',
            'auth' => $session_id,
            'elid' => (string) $user,
            'name' => (string) $user,
        );
        $params['func'] = 'user.delete';
        $this->query($params);
    }
    public function activate($param)
    {
        extract($param);
        $url = $this->url;
        $this->authAdmin();
        $session_id = $this->session_id;
        $params = array(
            'out' => 'xml',
            'auth' => $session_id,
            'elid' => (string) $user,
            'name' => (string) $user,
        );
        $params['func'] = $this->arParams['isp_version'] == 4 ? 'user.enable' : 'user.resume';
        $this->query($params);
    }
}
