<?php
//include_once 'lib/clients.php';
//include_once 'lib/clients.php';
\Bitrix\Main\Loader::registerAutoLoadClasses('clients',
    array(
        'Itin\\Depohost\\Clients' => 'lib/clients.php',
        'Itin\\Depohost\\Invoices' => 'lib/invoices.php',
        'Itin\\Depohost\\Acts' => 'lib/acts.php',
        'Itin\\Depohost\\Pdf' => 'lib/pdf.php',
        'Itin\\Depohost\\BillTxt' => 'lib/BillTxt.php',
        'Itin\\Depohost\\Info' => 'lib/info.php',
        'Itin\\Depohost\\EventHandlers' => 'lib/eventhandlers.php',
        'Itin\\Depohost\\СlientsAgents' => 'lib/agents.php',
        'Itin\\Depohost\\Rights' => 'lib/rights.php',
        'Itin\\Depohost\\Exchange1C' => 'lib/exchange1c.php',
        'Itin\\Depohost\\Isp' => 'lib/isp.php',
        'Itin\\Depohost\\Services' => 'lib/services.php',
        'Itin\\Depohost\\Sms' => 'lib/sms.php',
        'Itin\\Depohost\\Sorm' => 'lib/sorm.php',
        'Itin\\Depohost\\Exchange1C\\XmlStream' => 'lib/exchange1c/xmlstream.php',
        'Itin\\Depohost\\Exchange1C\\Invoices' => 'lib/exchange1c/invoices.php',
        'Itin\\Depohost\\Exchange1C\\Acts' => 'lib/exchange1c/acts.php',
        'Itin\\Depohost\\Exchange1C\\UtilXML' => 'lib/exchange1c/utilxml.php',
        'Itin\\Depohost\\Util\\AdminInterface' => 'lib/util/admininterface.php',
    )
);