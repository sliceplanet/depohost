<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CUtil::InitJSCore(array('window','ajax'));
?>
<script>
    var popupAdd_<?=$arParams['table_id']?> = function(){
    return new BX.CAdminDialog({
            'title': 'Добавление акта',
            'content_url': '/local/modules/clients/admin/ajax/add_act.php',
            'content_post': {'LINK_IBLOCK_ID':<?=$LINK_IBLOCK_ID?>,
                'IBLOCK_ID':<?=(int)$_REQUEST['IBLOCK_ID']?>,
                'PROPERTY_ID':<?=$PROPERTY_ID?>,
                'ID':<?=$ID?>,
                'return_url':'<?=$GLOBALS['APPLICATION']->GetCurPageParam('tabControl_active_tab=edit_acts',array('tabControl_active_tab'))?>',
                'isAjax':'Y'
                <?=!empty($invoice_id) ? ",'invoice_id':".$invoice_id : ""?>},
            'draggable': true,
            'resizable': true
        });
    }
</script>



