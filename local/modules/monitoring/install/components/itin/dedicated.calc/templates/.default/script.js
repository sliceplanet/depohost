function array_count_values(array) {
    // Counts all the values of an array
    //
    // +   original by: Ates Goral (http://magnetiq.com)
    // + namespaced by: Michael White (http://crestidg.com)

    var tmp_ar = new Object(), key;

    var countValue = function (value) {
        switch (typeof (value)) {
            case "number":
                if (Math.floor(value) != value) {
                    return;
                }
            case "string":
                if (value in this) {
                    ++this[value];
                } else {
                    this[value] = 1;
                }
        }
    }

    if (array instanceof Array) {
        array.forEach(countValue, tmp_ar);
    } else if (array instanceof Object) {
        for (var key in array) {
            countValue.call(tmp_ar, array[key]);
        }
    }
    return tmp_ar;
}
function each(arr) {
    //  discuss at: http://phpjs.org/functions/each/
    // original by: Ates Goral (http://magnetiq.com)
    //  revised by: Brett Zamir (http://brett-zamir.me)
    //        note: Uses global: php_js to store the array pointer
    //   example 1: each({a: "apple", b: "balloon"});
    //   returns 1: {0: "a", 1: "apple", key: "a", value: "apple"}

    this.php_js = this.php_js || {};
    this.php_js.pointers = this.php_js.pointers || [];
    var indexOf = function (value) {
        for (var i = 0, length = this.length; i < length; i++) {
            if (this[i] === value) {
                return i;
            }
        }
        return -1;
    };
    // END REDUNDANT
    var pointers = this.php_js.pointers;
    if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
    }
    if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
    }
    var arrpos = pointers.indexOf(arr);
    var cursor = pointers[arrpos + 1];
    var pos = 0;

    if (Object.prototype.toString.call(arr) !== '[object Array]') {
        var ct = 0;
        for (var k in arr) {
            if (ct === cursor) {
                pointers[arrpos + 1] += 1;
                if (each.returnArrayOnly) {
                    return [k, arr[k]];
                } else {
                    return {
                        1: arr[k],
                        value: arr[k],
                        0: k,
                        key: k
                    };
                }
            }
            ct++;
        }
        return false; // Empty
    }
    if (arr.length === 0 || cursor === arr.length) {
        return false;
    }
    pos = cursor;
    pointers[arrpos + 1] += 1;
    if (each.returnArrayOnly) {
        return [pos, arr[pos]];
    } else {
        return {
            1: arr[pos],
            value: arr[pos],
            0: pos,
            key: pos
        };
    }
}
function current(arr) {
    //  discuss at: http://phpjs.org/functions/current/
    // original by: Brett Zamir (http://brett-zamir.me)
    //        note: Uses global: php_js to store the array pointer
    //   example 1: transport = ['foot', 'bike', 'car', 'plane'];
    //   example 1: current(transport);
    //   returns 1: 'foot'

    this.php_js = this.php_js || {};
    this.php_js.pointers = this.php_js.pointers || [];
    var indexOf = function (value) {
        for (var i = 0, length = this.length; i < length; i++) {
            if (this[i] === value) {
                return i;
            }
        }
        return -1;
    };
    // END REDUNDANT
    var pointers = this.php_js.pointers;
    if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
    }
    if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
    }
    var arrpos = pointers.indexOf(arr);
    var cursor = pointers[arrpos + 1];
    if (Object.prototype.toString.call(arr) === '[object Array]') {
        return arr[cursor] || false;
    }
    var ct = 0;
    for (var k in arr) {
        if (ct === cursor) {
            return arr[k];
        }
        ct++;
    }
    return false; // Empty
}
function key(arr) {
    //  discuss at: http://phpjs.org/functions/key/
    // original by: Brett Zamir (http://brett-zamir.me)
    //    input by: Riddler (http://www.frontierwebdev.com/)
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //        note: Uses global: php_js to store the array pointer
    //   example 1: array = {fruit1: 'apple', 'fruit2': 'orange'}
    //   example 1: key(array);
    //   returns 1: 'fruit1'

    this.php_js = this.php_js || {};
    this.php_js.pointers = this.php_js.pointers || [];
    var indexOf = function (value) {
        for (var i = 0, length = this.length; i < length; i++) {
            if (this[i] === value) {
                return i;
            }
        }
        return -1;
    };
    // END REDUNDANT
    var pointers = this.php_js.pointers;
    if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
    }

    if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
    }
    var cursor = pointers[pointers.indexOf(arr) + 1];
    if (Object.prototype.toString.call(arr) !== '[object Array]') {
        var ct = 0;
        for (var k in arr) {
            if (ct === cursor) {
                return k;
            }
            ct++;
        }
        return false; // Empty
    }
    if (arr.length === 0) {
        return false;
    }
    return cursor;
}
function formatPrice(_number)
{
    var decimal = 0;
    var separator = '&nbsp;';
    var decpoint = '.';
    var format_string = '#';

    var r = parseFloat(_number)

    var exp10 = Math.pow(10, decimal);// приводим к правильному множителю
    r = Math.round(r * exp10) / exp10;// округляем до необходимого числа знаков после запятой

    rr = Number(r).toFixed(decimal).toString().split('.');

    b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);

    r = (rr[1] ? b + decpoint + rr[1] : b);
    return format_string.replace('#', r) + "&nbsp;Р";
}

jQuery(document).ready(function ($) {
    //  Calc logic
    function Calc() {
        data = {
            ram: arViewRAM,
            cpu: arCPU,
            raid: arRAID,
            hdd: arHDD,
            count_hdd: arCountHDD
        }
        this.data = data;
        this.values = {};
        this.dataChecked = {};

        //init on load
        this.createSliderRAM(firstArRam);
        this.changeGeneration($('#generation .option.checked'));
        this.handlerAllParametrs();
    }

    Calc.prototype.changeGeneration = function (generation) {
        //arViewRAM, arCPU, arRAID, arHDD
        generationData = {
            id: generation.attr('data-id'),
            formFactor: generation.attr('data-form-factor'),
            name: generation.attr('data-name'),
        };
        this.generation = generationData;
    }

    Calc.prototype.getDataParametr = function (type) {
        // type = ['hdd','cpu','ram','raid']
        var generation = this.generation;
        var data = this.data[type][generation.id];
        return data;
    }

    Calc.prototype.getCheckedParametr = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']
        return this.dataChecked[type];
    }

    Calc.prototype.setCheckedParametr = function (type, obj) {
        // type = ['hdd','cpu','ram','raid','generation']
        this.dataChecked[type] = obj;
    }
    Calc.prototype.getAllCheckedParametr = function () {
        var arData = {};

        arData.hdd = this.getCheckedParametr('hdd');
        arData.cpu = this.getCheckedParametr('cpu');
        arData.ram = this.getCheckedParametr('ram');
        arData.raid = this.getCheckedParametr('raid');
        arData.generation = this.getCheckedParametr('generation');

        return arData;
    }

    Calc.prototype.getValue = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']

        return this.values[type];
    }

    Calc.prototype.setValue = function (type, value) {
        // type = ['hdd','cpu','ram','raid','generation']
        this.values[type] = value;
    }

    Calc.prototype.handlerParametr = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']
        var value = this.getValue(type);
        var checked = this.getCheckedParametr(type);
        var paramsBlock = $('#parameters');

        switch (type) {
            case 'generation':
                var container = $('#generation tr');
                checked = container.find('.option.checked');
                var formFactor = checked.attr('data-form-factor');

                var img = $('#server-img');
                var map = $('#map-hdd');

                value = checked.attr('data-id');

                $('name="prop[generation]"').val(value);
                $('.type-server-header span').text('Вид сервера конфигурация ' + formFactor);
                if (img.hasClass('type' + formFactor.toLowerCase()) === false)
                {
                    img.removeClass();
                    img.addClass('server-img type' + formFactor.toLowerCase());
                    map.empty();
                    for (i = 0; i < this.getDataParametr('count_hdd'); i++)
                    {
                        map.append('<div class="deactive"></div>');
                    }
                    map.find('div:first').attr('class', 'active');
                }
                break;
            case 'cpu':
                checked = $('#freq-cpu .option.checked');
                value = checked.attr('data-id');
                var name = checked.attr('data-name');
                var cpuBlock = paramsBlock.find('#param-cpu');
                if (cpuBlock.length === 0)
                {
                    cpuBlock = paramsBlock.append('\
                                <div class="parameter-block" id="param-cpu">\
                                    <div class="caption">Процессоры</div>\
                                    <div class="parameter-value">\
                                        <div>' + name + '</div>\
                                    </div>\
                                </div>');
                }
                else
                {
                    cpuBlock.find('.parameter-value div').text(name);
                }

                break;
            case 'raid':
                checked = $('#calc-raid .option.checked');
                value = checked.attr('data-id');
                var name = checked.attr('data-name');
                var description = checked.attr('data-description');
                var raidBlock = paramsBlock.find('#param-raid');
                if (raidBlock.length === 0)
                {
                    raidBlock = paramsBlock.append('\
                                <div class="parameter-block" id="param-raid">\
                                    <div class="caption">Raid Контроллер</div>\
                                    <div class="parameter-value">\
                                        <div>' + description + '</div>\
                                    </div>\
                                </div>');
                }
                else
                {
                    raidBlock.find('.parameter-value div').text(description);
                }

                break;
            case 'ram':
                checked = $('#ram');
                value = checked.attr('data-id');

                $('#ram-view').text(checked.attr('data-size') + ' ' + measureGB);
                $('#param-ram .parameter-value').text(checked.attr('data-name'));
                break;
            case 'hdd':
                checked = $('.select-hdd select').not(':disabled');
                value = {};
                var arChecked = {};
                $.each(checked, function (index) {
                    value[index] = $(this).val();
                    arChecked[index] = $(this).find('option:selected');
                });
                var countArray = array_count_values(value);
                var differentValues = Object.keys(countArray).length;
                var countHDDBlocks = Math.round(differentValues / 2);
                $('[id^=param-hdd]').remove();
                for (i = 1; i <= countHDDBlocks; i++)
                {
                    var htmlValue = "";
                    for (k = 0; k < 2; k++)
                    {
                        var curElement = each(countArray);
                        if (curElement === false)
                        {
                            break;
                        }
                        var id = curElement['key'];
                        var quantity = curElement['value'];
                        var name = checked.find('option[data-id="'+id+'"]').attr('data-name');
                        htmlValue += '<div>'+quantity+' x '+name+'</div>';
                    }
                    paramsBlock.append('\
                        <div class="parameter-block" id="param-hdd-'+i+'">\
                            <div class="caption">HDD</div>\
                            <div class="parameter-value">'+htmlValue+'</div>\
                        </div>\
                    ');
                }
                checked = arChecked;
                break;
        }
        this.setValue(type, value);
        this.setCheckedParametr(type, checked);
    }

    Calc.prototype.handlerAllParametrs = function () {
        this.handlerParametr('generation');
        this.handlerParametr('cpu');
        this.handlerParametr('ram');
        this.handlerParametr('raid');
        this.handlerParametr('hdd');
        var sum = this.recalc();
        var stringCharacteristics = this.getStringCharacteristic();

        this.changeSum(sum);
    }

    Calc.prototype.recalc = function () {
        var arData = this.getAllCheckedParametr();
        var price;
        var sum = 0;

        $.each(arData,function(index,val){
            if (index == 'hdd')
            {
                $.each(val,function(i){
                    price = $(this).attr('data-price');
                    if (typeof price !== 'undefined')
                    {
                        sum += parseInt(price);
                    }
                });
            }
            else
            {
                price = $(val).attr('data-price');
                if (typeof price !== 'undefined')
                {
                    sum += parseInt(price);
                }
            }
        });
        return sum;
    }
    Calc.prototype.getStringCharacteristic = function () {
        var arData = this.getAllCheckedParametr();

        var string = "";
        var arNames = [];
        $.each(arData,function(index,val){

            switch (index){
                case 'hdd':
                    var arCount = array_count_values($(val));
                    $.each(arCount,function(id,count){
                        var name = count+'x'+$(val).find('[data-id='+id+']').attr('data-name');
                        arNames.push(name);
                    });
                    break;
                case 'generation':
                    break;
                default:
                    var name = typeof $(val).attr('data-description') !== 'undefined' ? $(val).attr('data-name')+' '+ $(val).attr('data-description') : $(val).attr('data-name');
                    arNames.push(name);
                    break;
            }
        });
        return arNames.join(',');
    }
    Calc.prototype.changeTerms = function (price,value) {
        var terms = $('#configuration .terms-block');
        var term = terms.find('.term-block[data-value='+value+']');
        var discount = 0;
        var sum = Math.round(price*value);
        var sum_discount = 0;

        if (value != 1)
        {
            discount = Math.round(price*arDiscount['CALC_'+value+'M']/100);
            sum_discount = Math.round(discount*value);
        }
        var sum_with_discount =  Math.round(sum-sum_discount);
        var price_with_discount = Math.round(price-discount);

        term.find('.top .price').html(formatPrice(sum_with_discount));
        term.find('.top .discount').html(formatPrice(sum_discount));
        term.find('.bottom .price').html(formatPrice(price_with_discount));
    }
    Calc.prototype.changeSum = function (sum) {
        var priceFormat = formatPrice(sum);
        var self = this;
        $('#summary .summ .price').html(priceFormat);
        var terms = $('#configuration .terms-block .term-block');
        $.each(terms,function(index,term){
            term = $(term);
            var value = parseInt(term.attr('data-value'));
            self.changeTerms(sum,value);
        });
    }

    Calc.prototype.showInput = function (type) {
//        // type = ['hdd','cpu','ram','raid']
        var data = this.getDataParametr(type);
        var html = "";
        var self = this;


        switch (type) {
            case 'hdd':
                self.templ = 'hdd_checkbox';

                for (i = 1; i <= self.getDataParametr('count_hdd'); i++)
                {
                    var val = {};
                    val['values'] = data;
                    val['i'] = i;
                    val['firstValue'] = data[0]['ID'];
                    html += self.createInputFromTemplate(val);
                }
                var container = $('#calc-hdd .select-checkbox-group');

                container.html(html);
                container.find('.select select').selectBox().change(function () {
                    var value = $(this).val();
                    $(this).parents('.input-block').find('input[type=checkbox]').val(value);
                });
                container.find('input[type=checkbox]').prettyCheckboxes();

                container.find('.checkbox:first').trigger('click');

                break;
            case 'raid':
                self.templ = 'radio';
                $.each(data, function (i, val) {
                    html += self.createInputFromTemplate(val);
                });
                var container = $('#calc-raid .radio-group table tr');
                container.html(html);
                self.checkedRadio(container.find('.option:first'));
                break;
            case 'ram':
                this.createSliderRAM(data);
                break;
            case 'cpu':
                self.templ = 'radio';
                $.each(data, function (i, val) {
                    html += self.createInputFromTemplate(val);
                });
                var container = $('#calc-freq-cpu .radio-group table tr');
                container.html(html);
                self.checkedRadio(container.find('.option:first'));
                break;
        }
    }

    Calc.prototype.createInputFromTemplate = function (arParams) {
        var html = "";
        switch (this.templ)
        {
            case 'radio':
                if (typeof arParams['PRICE']==='undefined' || arParams['PRICE']=='NaN')
                {
                    arParams['PRICE'] = 0;
                }
                if (typeof arParams['NAME']==='undefined' || arParams['NAME']=='NaN' || arParams['NAME']==null || arParams['NAME'].length<1)
                {
                    arParams['NAME']="";
                }
                html = '<td class="circle"></td>\
                    <td class="option" \
                        data-id="' + arParams['ID'] + '" \
                        data-name="' + arParams['NAME'] + '" \
                        data-description="' + arParams['DESCRIPTION'] + '"\
                        data-price="' + arParams['PRICE'] + '"\
                    >\
                        ' + arParams['NAME'] + '\
                        <div class="descr">' + arParams['DESCRIPTION'] + '</div>\
                    </td>';
                break;
            case 'hdd_checkbox':
                html = '<div class="input-block">\
                        <input id="hdd_' + arParams['i'] + '" name="prop[hdd[' + arParams['i'] + ']]" type="checkbox" value="' + arParams['firstValue'] + '">\
                        <label for="hdd_' + arParams['i'] + '">Диск ' + arParams['i'] + '</label>\
                        <div class="clearfix"></div>\
                        <div class="select select-hdd">\
                            <select name="hdd-type" class="small" disabled>';
                $.each(arParams['values'], function (id, arVal) {
                    if (typeof arVal['PRICE']==='undefined' || arVal['PRICE']=='NaN')
                    {
                        arVal['PRICE'] = 0;
                    }
                    html += '\
                        <option value="' + arVal['ID'] + '"\
                        data-id="' + arVal['ID'] + '"\
                        data-name="' + arVal['NAME'] + '"\
                        data-price="' + arVal['PRICE'] + '"\
                        >' + arVal['NAME'] + '</option>';
                });
                html += '</select>\
                        </div>\
                        <!--END .select-hdd-->\
                    </div>';
                break;
        }
        return html;
    }

    Calc.prototype.showAllInputs = function () {
        // show inputs current generation processor
        this.showInput('raid');
        this.showInput('cpu');
        this.showInput('hdd');
        this.showInput('ram');
        return this;
    }

    Calc.prototype.checkedRadio = function (obj) {
        // checked radio button
        var self = this;
        if (typeof obj !== 'object')
        {
            obj = $(obj);
        }

        var count_hdd_active = Object.keys(this.getCheckedParametr('hdd')).length;
        var secondObj = obj.hasClass('circle') ? obj.next('.option') : obj.prev('.circle'),
                val = obj.hasClass('circle') ? secondObj.attr('data-id') : obj.attr('data-id'),
                parentGroup = obj.parents('.radio-group'),
                id = parentGroup.attr('id'),
                input = $('[name="' + id + '"]');
        var price = parseInt($('.option[data-id='+val+']').attr('data-price'));
        if (count_hdd_active<2 && id=='raid' && price>0)
        {

        }
        else
        {
            parentGroup.find('.checked').removeClass('checked');

            obj.addClass('checked');
            secondObj.addClass('checked');

            var price = parseInt(parentGroup.find('[data-id='+val+']').attr('data-price'));

            input.val(val);
            obj.trigger('change-calc');
        }



    }

    Calc.prototype.createSliderRAM = function (arViewRAM) {
        if (typeof slider === 'object')
        {
            slider.destroy();
        }
        var slider = $("#sliderRAM").slider({
            range: "min",
            value: 0,
            min: 0,
            max: arViewRAM.length - 1,
            step: 1,
            slide: function (event, ui) {
                var arValue = arViewRAM[ui.value];
                var input = $('#ram');
                input.val(arValue['NAME']);
                input.attr({
                    'data-id': arValue['ID'],
                    'data-size': arValue['SIZE'],
                    'data-name': arValue['NAME'],
                    'data-price': arValue['PRICE']
                });
                $(this).trigger('change-calc');
            }
        });
        var width = 100 / arViewRAM.length;
        $('.slide-grath').empty();
        var margin = width / 2;
        var styles = {
            marginLeft: function () {
                return margin + "%";
            },
            width: function () {
                var tempWidth = 100 - (2 * margin);
                return tempWidth + "%";
            }

        }
        $('.slide').css(styles);
        for (i = 0; i < arViewRAM.length; i++)
        {
            $('.slide-grath').append('<div style="width: ' + width + '%;">' + arViewRAM[i]['SIZE'] + '</div>');
        }
    }

    Calc.prototype.mapHDD = function (count_active) {
        var map = $('#map-hdd');
        map.empty();
        for (i = 0; i < count_active; i++)
        {
            map.append('<div class="active"></div>');
        }
        for (; i < this.getDataParametr('count_hdd'); i++)
        {
            map.append('<div class="deactive"></div>');
        }
    }
    var calc = new Calc();


    $('#generation').on('click', 'td', function (event) {
        if ($(this).hasClass('option') === true)
        {
            var generation = $(this);
        }
        else
        {
            var generation = $(this).next('td.option');
        }
        calc.changeGeneration(generation);
        calc.showAllInputs();
    });

    //radio group
    $('.radio-group').on('click', '.circle, .option', function (event) {
        calc.checkedRadio($(this));
    });
    //checkbox hdd
    $('.select-checkbox-group').on('change', '.input-block [type=checkbox]', function (event) {
        var block = $(this).parents('.input-block'),
                bChecked = $(this).attr('checked');
        var active_checkbox = block.parent().find('[type=checkbox]:checked'),
            count_active = active_checkbox.length;

        if (bChecked === 'checked')
        {
            block.find('.select select').selectBox('enable');
        }
        else
        {
            block.find('.select select').selectBox('disable');
        }

        calc.mapHDD(count_active);
        $(this).trigger('change-calc');
    });
    $('.select-checkbox-group').on('change', '.input-block select', function (event) {
        $(this).trigger('change-calc');
    });
    $('#configuration').on('change-calc', '', function () {
        calc.handlerAllParametrs();
    });
});