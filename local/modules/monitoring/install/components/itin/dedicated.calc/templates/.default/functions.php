<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!function_exists('showHint'))
{
    function showHint($text, $icon = "")
    {
        if (strlen($text) > 0):
            ?>
            <div class="hint-block <?= $icon ?>">
                <div class="hint-text">
                    <div class="hint-close"></div>
                    <?= $text ?>
                </div>
                <div class="hint-corner"></div>
            </div>
            <?
        endif;
    }

}
if (!function_exists('showRadioButton'))
{
    function showRadioButton($arItems)
    {
        $bFirst = true;
        foreach ($arItems as $key => $arItem):
            if ($bFirst)
            {
                $checked = " checked";
                $arFistValue = $arItem['ID'];
                $bFirst = false;
            } else
            {
                $checked = "";
            }
            ?>
            <td width="30" class="circle<?= $checked ?>"></td>
            <td class="option<?= $checked ?>"
                data-id="<?= $arItem['ID'] ?>"
                data-name="<?= $arItem['NAME'] ?>"
                data-description="<?= $arItem['DESCRIPTION'] ?>"
                data-price="<?=(int) $arItem['PRICE'] ?>"
                >
                    <?= $arItem['NAME'] ?>
                <div class="descr"><?= $arItem['DESCRIPTION'] ?></div>
            </td>
            <?
        endforeach;
    }

}
if (!function_exists('first_value'))
{
    // Возвращает первое значение массива
    function first_value($array)
    {
        if (is_array($array))
        {
            reset($array);
            return current($array);
        } else
        {
            return $array;
        }
    }

}
if (!function_exists('price_format'))
{
    // Форматирует ценy
    function price_format(&$array)
    {
        $array[$key] = number_format($value, 0, "", " ");
    }

}
if (!function_exists('array_price_format'))
{
    // Форматирует массив цен
    function array_price_format(&$array)
    {
        if (!is_array($array))
        {
            $array = array($array);
        }
        foreach ($array as $key => $value)
        {
            $array[$key] = number_format($value, 0, "", " ");
        }
    }

}
?>