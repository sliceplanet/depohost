<?php
/*
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if ($APPLICATION->GetGroupRight(MONITORING_ADMIN_MODULE_NAME) != "D")
{
    Loc::loadMessages(__FILE__);


    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => 'dedicated',
        "sort" => 2,
        "text" => Loc::getMessage('main_menu_text'),
        "title" => Loc::getMessage('main_menu_title'),
        "icon" => "update_marketplace",
        "page_icon" => "blog_page_icon",
        "items_id" => "servers_menu",
        "items" =>
        array(
            array(
                "text" => Loc::getMessage('configurator_menu_text'),
                "url" => "/bitrix/admin/dedicated_configurator.php?lang=ru&IBLOCK_ID=21&type=xmlcatalog&find_section_section=0",
                "more_url" => array('/bitrix/admin/dedicated_configurator.php'),
                "title" => Loc::getMessage('configurator_menu_title'),
                "sort" => 100,
                "items_id" => "servers_menu_configurator",
                "icon" => "util_menu_icon",
            ),
            array(
                "text" => Loc::getMessage('hardware_calc_menu_text'),
                "url" => "",
                "more_url" => array(),
                "title" => Loc::getMessage('hardware_calc_menu_title'),
                "sort" => 200,
                "icon" => "iblock_menu_icon_sections",
                "items_id" => "hardwares_calc",
                "items" => $arHardware['dedicated'],
            ),
            array(
                "text" => Loc::getMessage('hardware_solution_menu_text'),
                "url" => "",
                "more_url" => array(),
                "title" => Loc::getMessage('hardware_solution_menu_title'),
                "sort" => 300,
                "icon" => "iblock_menu_icon_sections",
                "items_id" => "hardwares_solutions",
                "items" => $arHardware['hardwares_solutions'],
            ),
            array(
                "text" => Loc::getMessage('advance_options_menu_text'),
                "url" => "/bitrix/admin/dedicated_advance_options.php?lang=ru&IBLOCK_ID=50&type=catalog&find_section_section=0&act=sect_list",
                "more_url" => array(
                    '/bitrix/admin/iblock_section_edit.php?IBLOCK_ID=50&type=catalog&ID=0&lang=ru&IBLOCK_SECTION_ID=0&find_section_section=0',
                    '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=50&type=catalog&lang=ru&find_section_section=0',
                    '/bitrix/admin/dedicated_advance_options.php',
                    ),
                "title" => Loc::getMessage('advance_options_menu_title'),
                "sort" => 400,
                "icon" => "iblock_menu_icon_sections",
            ),
            array(
                "text" => Loc::getMessage('config_menu_text'),
                "url" => "/bitrix/admin/dedicated_config.php?lang=ru",
                "more_url" => array('/bitrix/admin/dedicated_config.php'),
                "title" => Loc::getMessage('config_menu_title'),
                "sort" => 500,
                "icon" => "sale_menu_icon",
            ),
        )
    );


    foreach ($aMenu['items'] as $key => $value)
    {
        $arSort[$key] = $value['sort'];
    }
    array_multisort($arSort, SORT_ASC, SORT_NUMERIC, $aMenu['items']);

    return $aMenu;
}
*/
