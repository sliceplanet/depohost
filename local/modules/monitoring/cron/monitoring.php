<?
define("NO_KEEP_STATISTIC", true);
use Bitrix\Main\Loader;
use Itin\Depohost\Monitoring\Servers;

if (empty($_SERVER['DOCUMENT_ROOT']))
{
    $_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/../../../../';
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);
ignore_user_abort();
Loader::includeModule('monitoring');
// Получаем список ip которые нужно промониторить
$limit = 0;
$arItems = Servers::getWaitingList($limit);
if (is_array($arItems) && !empty($arItems))
{
    Itin\Depohost\Monitoring::handleWaitingList($arItems);
}
?>