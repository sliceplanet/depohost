<?

use Bitrix\Main\Config\Option;

define("NO_KEEP_STATISTIC", true);

if (empty($_SERVER['DOCUMENT_ROOT']))
{
  $_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/../../../../';
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);
ignore_user_abort();
$sms_api_key_valid_date = Option::get('clients', 'sms_api_key_valid_date');
$defaultEmail = Option::get('main', 'email_from');

if($sms_api_key_valid_date) {
  $dateValid = \Bitrix\Main\Type\Date::createFromText($sms_api_key_valid_date);
  $time = $dateValid->getTimestamp();
  if($time - 30*24*60 > time()) {
    $resUser = \CUser::GetByID(1);
    $arUser = $resUser->Fetch();
    $mail = $arUser['EMAIL'];
    $subject = 'Истекает срок действия ключа API Devino Online';
    $headers = [
      'Content-type: text/html; charset=utf-8',
      'From: '. $defaultEmail
    ];
    $message = '
<html>
<body>
  <p><b>'.$sms_api_key_valid_date.'</b> истекает срок действия ключа <a href="https://docs.devino.online/ru/#authorization">API Devino Online</a></p>
  <p>Необходимо обновить ключ. Для этого обратитесь <a href="mailto://support@devinotele.com">в поддержку</a></p>
</body>
</html>';

    mail($mail, 'Истекает срок действия ключа API Devino Online', $message, implode("\r\n", $headers));
  }
}

?>