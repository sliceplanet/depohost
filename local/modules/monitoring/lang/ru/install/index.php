<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$MESS['MONITORING_INSTALL_NAME'] = "Мониторинг оборудования";
$MESS['MONITORING_INSTALL_DESCRIPTION'] = "Модуль мониторинга оборудования";
$MESS['MODULE_DENIED'] = "Доступ запрещен";
$MESS['MODULE_READ'] = "Чтение";
$MESS['MODULE_WRITE'] = "Запись";
?>
