<?php

/**
 * API for work with monitoring servers
 *
 * @package itin
 * @subpackage depohost
 * @author Alexandr Terentev <alvteren@gmail.com>
 */

namespace Itin\Depohost\Monitoring;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;
use CEvent;
use CEventMessage;
use CUser;
use Itin\Depohost\Clients;
use Itin\Depohost\Info;
use Itin\Depohost\Monitoring;
use Itin\Depohost\Sms;

Loc::loadMessages(__FILE__);

class Servers
{

  /**
   * @var array $arErrors Array of errors
   */
  private $arErrors;

  function __construct()
  {

  }

  /**
   * Возвращает объект класса HL блока мониторинга серверов
   *
   * @return object \MonitoringServersTable
   */
  public static function getHL()
  {
    $HL_ID = 2;
    Loader::includeModule('highloadblock');
    $hldata = HighloadBlockTable::getById($HL_ID)->fetch();
    $hlentity = HighloadBlockTable::compileEntity($hldata);
    $hlDataClass = $hldata['NAME'] . 'Table';

    $object = new $hlDataClass;
    return $object;
  }

  /**
   * Возвращает массив IP адрессов имеющихся в услугах у пользователя
   *
   * @param int $user_id
   * @return array
   */
  public static function getUserIPs($user_id = NULL)
  {
    if ($user_id == NULL || (int)$user_id <= 0)
    {
      $user_id = CUser::GetID();
    }
    $user_id = (int)$user_id;
    $result = array();

    Loader::includeModule('clients');

    $client = new Clients($user_id);

    $client_id = $client->getElementId();

    $arFilter = array('!UF_IP_ADDR' => false);
    $arServices = $client->getClientServicesList($client_id, $arFilter);

    foreach ($arServices as $arService)
    {
      foreach ($arService['UF_IP_ADDR'] as $ip_address)
      {
        $result[] = $ip_address;
      }
    }
    return $result;
  }

  /**
   *
   * @param string $ip
   * @param array $params Array parameters, example for:
   *      array(
   *          'type' => <http,ping>,
   *          'contact' => string,
   *          'time' => integer,
   *          'user_id' => integer,
   *      )
   */
  public static function saveSettingsNotify($ip, array $params)
  {
    if (empty($ip))
    {
      return false;
    }

    $default_time = 15;

    $setting = self::getHL();
    $temp = current($params);
    $arParams = array(
      'order' => array('ID' => 'ASC'),
      'filter' => array('UF_IP_ADDRESS' => $ip, 'UF_USER' => $temp['user_id']),
      'select' => array('ID')
    );
    $res = $setting->getList($arParams);
    while ($ar = $res->fetch())
    {
      $ar_ids[] = $ar['ID'];
    }
    $type = '';
    $contact = '';
    $user_id = '';
    $time = $default_time;

    $arFields = array(
      'UF_IP_ADDRESS' => $ip,
      'UF_TYPE' => &$type,
      'UF_CONTACT' => &$contact,
      'UF_TIME' => &$time,
      'UF_LAST' => time(),
      'UF_USER' => &$user_id,
    );
    reset($params);
    // clean empty contact $params
    foreach ($params as $key => $arParam)
    {
      if (empty($arParam['contact']))
      {
        unset($params[$key]);
      }
    }
    if ((count($params) - count($ar_ids)) < 0)
    {
      for ($i = 0; $i < (count($ar_ids) - count($params)); $i++)
      {
        foreach ($ar_ids as $key => $id)
        {
          $setting->delete($id);
          unset($ar_ids[$key]);
        }
      }
    }
    reset($params);
    foreach ($ar_ids as $id)
    {
      $temp = current($params);
      extract($temp);
      next($params);
      if (empty($temp))
      {
        break;
      }
      $arFields['UF_NEXT'] = ($time * 60) + time();
      $setting->update($id, $arFields);
    }
    for ($i = 0; $i < (count($params) - count($ar_ids)); $i++)
    {
      $temp = current($params);
      extract($temp);
      next($params);
      $arFields['UF_NEXT'] = ($time * 60) + time();
      $arNotify = array(
        'TYPE' => $arFields['UF_TYPE'],
        'IP_ADDRESS' => $arFields['UF_IP_ADDRESS'],
        'TIME' => $arFields['UF_TIME'],
        'CONTACT' => $arFields['UF_CONTACT'],
        'USER' => $arFields['UF_USER'],
      );

      $status = self::pingServer($arNotify);
      $arFields['UF_STATUS'] = $status ? 1 : 0;
      $result = $setting->add($arFields);
      if ($result->isSuccess())
      {
        $arNotify['ID'] = $result->getId();

        $arNotify['STATUS'] = $status;
        self::sendEmail($arNotify);
        self::saveStatus($arNotify, $arFields['UF_STATUS']);

        $arForSupport[$type][$ip] = $arNotify;
      }
    }
    if (isset($arForSupport) && is_array($arForSupport) && count($arForSupport) > 0)
    {
      foreach ($arForSupport as $arNotifies)
      {
        foreach ($arNotifies as $arNotify)
        {
          self::sendForSupport($arNotify);
        }
      }
    }
  }

  /**
   * Возвращает настройки уведомления
   *
   * @param string $ip
   * @return boolean or array
   */
  public static function getSettingsNotify($ip, $user_id)
  {
    if (empty($ip))
    {
      return false;
    }

    $result = array();

    $setting = self::getHL();

    $arParams = array(
      'order' => array('ID' => 'ASC'),
      'filter' => array('UF_IP_ADDRESS' => $ip, 'UF_USER' => $user_id),
      'select' => array('ID', 'UF_TYPE', 'UF_CONTACT', 'UF_TIME')
    );
    $res = $setting->getList($arParams);
    while ($ar = $res->fetch())
    {
      $result[$ar['ID']] = array(
        'type' => $ar['UF_TYPE'],
        'contact' => $ar['UF_CONTACT'],
        'time' => $ar['UF_TIME'],
      );
    }

    return $result;
  }

  /**
   *
   * @param integer $user_id
   * @return array
   */
  public static function getAllSettingsNotify($user_id)
  {
    $result = array();

    $setting = self::getHL();

    $arParams = array(
      'order' => array('ID' => 'ASC'),
      'filter' => array('UF_USER' => $user_id),
      'select' => array('ID', 'UF_IP_ADDRESS')
    );
    $res = $setting->getList($arParams);
    while ($ar = $res->fetch())
    {
      $result[$ar['ID']] = $ar;
    }

    return $result;
  }

  /**
   * Удаляет настройки пользователя определенного IP-адреса
   *
   * @param string $ip
   * @param integer $user_id
   */
  public static function cleanSettingsNotify($ip, $user_id)
  {
    $arSettings = self::getSettingsNotify($ip, $user_id);
    $setting = self::getHL();

    foreach (array_keys($arSettings) as $id)
    {
      $setting->delete($id);
    }
  }

  /**
   * Обновляет IP-адрес настройки пользователя определенного IP-адреса
   *
   * @param string $ip
   * @param integer $user_id
   */
  public static function updateSettingsNotify($ip, $user_id, $new_ip)
  {
    $arSettings = self::getSettingsNotify($ip, $user_id);
    $setting = self::getHL();

    foreach (array_keys($arSettings) as $id)
    {
      $setting->update($id, ['UF_IP_ADDRESS' => $new_ip]);
    }
  }

  /**
   * Удаляет все настройки пользователя
   *
   * @param string $ip
   * @param integer $user_id
   */
  public static function cleanAllSettingsNotify($user_id)
  {
    $arSettings = self::getAllSettingsNotify($user_id);
    $setting = self::getHL();

    foreach (array_keys($arSettings) as $id)
    {
      $setting->delete($id);
    }
  }

  /**
   * Возвращает список уведомления требующих отправки
   * Элементы массива можно передать в метод пинга \Itin\Depohost\Monitoring\Servers::pingServer()
   *
   * @param integer $limit - количество ip адресов для опроса
   *
   * @return array
   */
  public static function getWaitingList($limit = 0)
  {
    $result = array();

    $setting = self::getHL();

    $arParams = array(
      'order' => array('ID' => 'ASC'),
      'filter' => array('<=UF_NEXT' => time()),
      'select' => array('ID', 'UF_TYPE', 'UF_CONTACT', 'UF_TIME', 'UF_IP_ADDRESS', 'UF_STATUS', 'UF_USER')
    );
    if ((int)$limit > 0)
    {
      $arParams['limit'] = $limit;
    }
    $res = $setting->getList($arParams);
    while ($ar = $res->fetch())
    {
      $result[] = array(
        'ID' => $ar['ID'],
        'CONTACT' => $ar['UF_CONTACT'],
        'TIME' => $ar['UF_TIME'],
        'IP_ADDRESS' => $ar['UF_IP_ADDRESS'],
        'TYPE' => $ar['UF_TYPE'],
        'STATUS' => $ar['UF_STATUS'],
        'USER' => $ar['UF_USER'],
      );

    }

    return $result;
  }

  /**
   * Отвечает ли сервер
   *
   * @param array $notify array('IP_ADDRESS','TYPE')
   * @return boolean
   */
  public static function pingServer(array $notify)
  {
    if ($notify['TYPE'] == 'HTTP')
    {
      $bAvailable = Monitoring::http($notify['IP_ADDRESS']);
    }
    elseif ($notify['TYPE'] == 'PING')
    {
      $bAvailable = Monitoring::ping($notify['IP_ADDRESS']);
    }
    return $bAvailable !== false;
  }

  /**
   * Обновляет статус сервера
   *
   * @param array $notify
   * @param string $status 1 or 0
   * @return boolean
   */
  public static function saveStatus(array $notify, $status)
  {
    $setting = self::getHL();
    $next_time = $notify['TIME'] * 60 + time();
//        $next_time = time();

    return $setting->update($notify['ID'], array('UF_STATUS' => $status, 'UF_LAST' => time(), 'UF_NEXT' => $next_time));
  }

  /**
   * @param array $params array('STATUS','IP_ADDRESS','TYPE','USER')
   *
   * @return boolean
   */
  public static function isPhone($params) {
    if(strpos($params['CONTACT'], '@') !== false) {
      return false;
    }
    return strpos($params['CONTACT'], '+7') === 0 || strpos($params['CONTACT'], '7') === 0 || strpos($params['CONTACT'], '8') === 0;
  }

  /**
   * @param array $params array('STATUS','IP_ADDRESS','TYPE','USER')
   *
   * @return array
   */
  public static function mapToMessageFields($params) {
    $arFields = $params;
    $arFields['STATUS'] = $params['STATUS'] === false ? 'недоступен' : 'доступен';
    $arFields['STATUS_UPPER'] = strtoupper($arFields['STATUS']);
    $arFields['TYPE_LOWER'] = strtolower($arFields['TYPE']);
    $arFields['DATE'] = FormatDate('FULL', time());
    $arFields['CONTACT'] = $params['CONTACT'];

    Loader::includeModule('clients');
    $client = new Clients((int)$params['USER']);
    $arProfile = $client->getFormatProfile();

    $arFields['CLIENT_NAME'] = $arProfile['NAME'];

    return $arFields;
  }

  /**
   * Отправляет оповещение
   *
   * @param array $params array('STATUS','IP_ADDRESS','TYPE','USER')
   *
   * @return integer or string Возвращает ID e-mail или ответ от веб-сервиса sms
   */
  public static function sendEmail(array $params)
  {
    $arFields = static::mapToMessageFields($params);
    if (strpos($params['CONTACT'], '@') !== false)
    {
      // if email
      $ob = new SiteTable;
      $res = $ob->getList();
      $arSites = array();
      while ($ar = $res->fetch())
      {
        $arSites[] = $ar['LID'];
      }
      $arFields['EMAIL'] = $params['CONTACT'];
      return CEvent::SendImmediate('MONITORING_EMAIL', $arSites, $arFields);
    }
  }

  /**
   * Отправляет оповещение техподдержке
   *
   * @param array $params array('STATUS','IP_ADDRESS','TYPE','USER')
   * @return integer Возвращает ID e-mail
   */
  public static function sendForSupport(array $params)
  {
    $arFields = $params;
    $arFields['STATUS'] = $params['STATUS'] === false ? 'недоступен' : 'доступен';
    $arFields['STATUS_UPPER'] = strtoupper($arFields['STATUS']);
    $arFields['TYPE_LOWER'] = strtolower($arFields['TYPE']);
    $arFields['DATE'] = FormatDate('FULL', time());
    $arFields['CONTACT'] = $params['CONTACT'];
    Loader::includeModule('clients');
    $client = new Clients($arFields['USER']);
    // Получаем все данные для счета из профиля покупателя
    $arProfile = $client->getFormatProfile();

    $arFields['CLIENT_NAME'] = $arProfile['NAME'];

    $ob_services = Info::getHL('service');

    $arParams = array(
      'select' => array('UF_SERVER_ID'),
      'filter' => array('UF_CLIENT' => $client->getElementId(), 'UF_IP_ADDR' => $arFields['IP_ADDRESS']),
      'limit' => 1,
    );

    $res = $ob_services->getList($arParams);
    if ($ar = $res->fetch())
    {
      $arFields['SERVER_ID'] = $ar['UF_SERVER_ID'];
    }
    else
    {
      $arFields['SERVER_ID'] = Loc::getMessage('EMPTY_SERVER_ID');
    }

    $ob = new SiteTable;
    $res = $ob->getList();
    $arSites = array();
    while ($ar = $res->fetch())
    {
      $arSites[] = $ar['LID'];
    }
    $arFields['EMAIL'] = $params['CONTACT'];

    return CEvent::SendImmediate('MONITORING_EMAIL_FOR_SUPPORT', $arSites, $arFields);
  }

  /**
   * Возвращает ID клиента по IP адресу сервера
   *
   * @param string $ip
   * @return integer | false ID элемента инфоблока Клиентов
   * @throws \Bitrix\Main\LoaderException
   */
  public static function getClientIdByIp($ip)
  {
    Loader::includeModule('clients');
    $ob_services = Info::getHL('service');

    $arParams = array(
      'select' => array('UF_CLIENT'),
      'filter' => array('=UF_IP_ADDR' => $ip),
      'limit' => 1,
    );

    $res = $ob_services->getList($arParams);
    if ($ar = $res->fetch())
    {
      return (int)$ar['UF_CLIENT'];
    }
    else
    {
      return false;
    }
  }

  /**
   *
   * @param string $message Write message on error in array of errors
   * @return boolean
   */
  private function setError($message)
  {
    $this->arErrors[] = $message;
    return true;
  }

  /**
   *
   * @return array Return array of errors
   */
  public function getErrors()
  {
    return $this->arErrors;
  }

  /**
   * Var dump array of errors
   *
   * @return boolean
   */
  public function dumpErrors()
  {
    echo '<pre>';
    var_dump($this->getErrors());
    echo '</pre>';
    return true;
  }

}
