<?php

/**
 * Tools and properties dedicated module's
 *
 * @package itin
 * @subpackage depohost
 * @author Alexandr Terentev
 */

namespace Itin\Depohost;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Monitoring\Servers;

Loc::loadMessages(__FILE__);

class Monitoring
{

  /** @var string directory module's path */
  private $moduleDir;
  public const PING_ATTEMPT = 2;

  /**
   * Return full path to module
   *
   * @return string
   */
  static function getModuleDir()
  {
    $DOCUMENT_ROOT = self::getDocumentRoot();

    return $DOCUMENT_ROOT . '/local/modules/' . MONITORING_ADMIN_MODULE_NAME;
  }

  /**
   * Return DOCUMENT_ROOT
   */
  static function getDocumentRoot()
  {
    $DOCUMENT_ROOT = !empty($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : __DIR__ . "/../../../../";
    return $DOCUMENT_ROOT;
  }

  /**
   *
   * @param string $type
   * @return object
   */
  public static function getObjectHL($type)
  {
    switch ($type)
    {
      case 'monitoring-data':
        $HL_ID = 2;

        break;

      default:
        break;
    }

    Loader::includeModule('highloadblock');
    $hldata = HighloadBlockTable::getById($HL_ID)->fetch();
    $hlentity = HighloadBlockTable::compileEntity($hldata);
    $hlDataClass = $hldata['NAME'] . 'Table';

    $object = new $hlDataClass;
    return $object;
  }

  /**
   * Пингует по ip-адресу
   *
   * @param string $ip IP address without port
   * @return boolean
   */
  public static function ping($ip)
  {
    $attempt = static::PING_ATTEMPT;
    $result = shell_exec("ping -c $attempt $ip");

    return !empty($result) && stripos($result, '0 received') === false;
  }

  /**
   * Проверяет доступность сайта по http
   *
   * @param string $ip
   * @return boolean
   */
  public static function http($ip = NULL)
  {
    if ($ip == NULL)
    {
      return false;
    }
    $ch = curl_init($ip);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($httpcode >= 200 && $httpcode < 400)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public static function handleWaitingList(array $arItems)
  {
    Loader::includeModule('clients');
    $sms = new Sms();
    $i = 0;
    foreach ($arItems as $value)
    {
      $status = Servers::pingServer($value) ? 1 : 0;
      if ((int)$value['STATUS'] !== (int)$status)
      {
        $value['STATUS'] = $status === 1;
        $arSupport[$value['TYPE']][$value['IP_ADDRESS']] = $value;
        Servers::sendEmail($value);
        if(Servers::isPhone($value)) {
          $arFieldsMessage = Servers::mapToMessageFields($value);
          $arSmsFields = ['to' => $value['CONTACT'], 'text' => $sms->getMessage($arFieldsMessage)];
          $sms->collectSmsToQueue($arSmsFields);
        }
      }
      Servers::saveStatus($value, $status);
    }
    echo "Ответы по смс: \n\r";
    $sms->sendQueue();
    foreach ($arSupport as $type => $arIPaddresses)
    {
      foreach ($arIPaddresses as $ip => $value)
      {
        $params = $value;
        Servers::sendForSupport($params);
      }
    }
  }
}
