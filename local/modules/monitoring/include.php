<?php
require_once 'prolog.php';
\Bitrix\Main\Loader::registerAutoLoadClasses(MONITORING_ADMIN_MODULE_NAME,
    array(
        'Itin\\Depohost\\Monitoring' => 'lib/monitoring.php',
        'Itin\\Depohost\\Monitoring\\Servers' => 'lib/monitoring/servers.php',
    )
);