<?
//  Выгрузка в csv СОРМ
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);

if(empty($_SERVER["DOCUMENT_ROOT"])) {
  $_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/../../';
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);


Bitrix\Main\Loader::includeModule('clients');
$sorm = Itin\Depohost\СlientsAgents::sormCSV();
echo "Файл {$sorm->getFileName()} успешно создан и отправлен на e-mail указанный в настройках почтового события https://www.depohost.ru/bitrix/admin/message_edit.php?lang=ru&ID=112&type=&tabControl_active_tab=edit1";