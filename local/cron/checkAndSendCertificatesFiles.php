<?
//$_SERVER["DOCUMENT_ROOT"] = "/var/www/deporoot/data/www/depohost.ru";
$_SERVER["DOCUMENT_ROOT"] = "/var/www/design/data/www/dt-server.ru";

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true); 
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

while(ob_get_level()) {
   ob_get_clean();
}

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Iblock\ElementTable;
use Bitrix\Sale;
use Bitrix\Sale\Order;

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('ncsupport.exchange');

$orderElements = array();
$uniqueOrder = array();

$obItems = \Bitrix\Iblock\ElementTable::getList(array('select' => array('ID'),'filter' => array("IBLOCK_ID" => 30, "ACTIVE" => "Y")));
$sslElements = $obItems->fetchAll();
foreach($sslElements as $sslElement):
	$orderElements[] = $sslElement['ID'];
endforeach;

$dbRes = \Bitrix\Sale\Order::getList(array('filter' => array('BASKET.PRODUCT_ID' => $orderElements),'order' => array('ID' => 'DESC')));

while($arOrder = $dbRes->fetch()):
	if(!in_array($arOrder['ID'],$uniqueOrder)) {
		$uniqueOrder[] = $arOrder['ID'];
		$order = Sale\Order::load($arOrder['ID']);
		$basket = $order->getBasket();
		foreach ($basket as $basketItem) {
			$send = 0;
			$domain = 0;
			$isp = 0;
			$basketPropertyCollection = $basketItem->getPropertyCollection(); 
			foreach($basketPropertyCollection as $propertyItem) {
				if($propertyItem->getField('CODE') == 'domain') {
					$domain = $propertyItem->getField('VALUE');
				}
				if($propertyItem->getField('CODE') == 'isp_order') {
					$isp = $propertyItem->getField('VALUE');
				}
				if($propertyItem->getField('CODE') == 'crt_send') {
					$send = $propertyItem->getField('VALUE');
				}
			}
			if(($send == 0 || empty($send) || $send == 'N') && (!empty($isp) && $isp > 0)) {
				$certificate = (new \NcSupport\Exchange\Import\SSL())->certificateGet($isp);
				if($certificate) {
					$path = $_SERVER['DOCUMENT_ROOT'] . '/upload/certificates/'.$isp.'/';
					$zip = $path.'certificate.zip';
					$private = $path.$domain.'_private.key';

					$file = file_get_contents($certificate);
					if(!empty($file)) {
						@file_put_contents($zip,$file);
						
						if(file_exists($zip) && file_exists($private)) {
							$arUnpackOptions = Array(
							   "REMOVE_PATH"      => $_SERVER["DOCUMENT_ROOT"],
							   "UNPACK_REPLACE"   => false
							);

							$resArchiver = CBXArchive::GetArchive($zip);
							$resArchiver->SetOptions($arUnpackOptions);
							$resArchiver->Unpack($path);
							
							unlink($zip);
							$files = array_diff(scandir($path), array('..', '.', '.section.php', '.section.php'));
							
							$arPackFiles = array();
							foreach($files as $file) {
								$arPackFiles[] = $path.$file;
							}

							$packarc = CBXArchive::GetArchive($zip);
							$packarc->SetOptions(Array(
								"REMOVE_PATH" => $path,
							));
							$packarc->Pack($arPackFiles);
						}

						$propertyCollection = $order->getPropertyCollection();
						$emailPropValue = $propertyCollection->getUserEmail()->getValue();
						$namePropValue  = $propertyCollection->getPayerName()->getValue();
						
						$arEventFields['ORDER_ID'] = $arOrder['ID'];
						$arEventFields['DOMAIN'] = $domain;
						$arEventFields['EMAIL'] = $emailPropValue;
						$arEventFields['NAME'] = $namePropValue;

						CEvent::Send("CERTIFICATE", "s1", $arEventFields, "N", 114, array($zip));
						
						(new \NcSupport\Exchange\Import\SSL())->addOrderSendData($arOrder['ID'],$domain,1);
					}
				}
			}
		}
	}
endwhile;

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>