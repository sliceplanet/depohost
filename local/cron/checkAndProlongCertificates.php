<?
//$_SERVER["DOCUMENT_ROOT"] = "/var/www/deporoot/data/www/depohost.ru";
$_SERVER["DOCUMENT_ROOT"] = "/var/www/design/data/www/dt-server.ru";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true); 
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

while(ob_get_level()) {
   ob_get_clean();
}

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Iblock\ElementTable;
use Bitrix\Sale;
use Bitrix\Sale\Order;

\Bitrix\Main\Loader::includeModule('highloadblock');
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('clients');
\Bitrix\Main\Loader::includeModule('ncsupport.exchange');


function numberByText($n, $titles) {
	$cases = array(2, 0, 1, 1, 1, 2);
	return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}


$dateNow = strtotime('now');
$dateFrom = strtotime('today -12 months');
$dateTo = strtotime('today -11 months');

use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

$hlblock = HL\HighloadBlockTable::getById(4)->fetch(); 

$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass(); 

$rsData = $entity_data_class::getList(array(
   "select" => array("*"),
   "order" => array("ID" => "ASC"),
   "filter" => array(">UF_EXPIRE"=>$dateFrom,"<UF_EXPIRE"=>$dateTo)
));

while($arData = $rsData->Fetch()) {
	$arParams = array(
		'filter' => array(
			'ACTIVE' => 'Y',
			'ID' => $arData['UF_USER']
		)
	);

	$client = \Itin\Depohost\Clients::getList($arParams);

	if(!empty($client)) {
		
		$clientData = current($client);
		
		//Получаем информацию о счетах клиента
		$arParams = array('filter' => array('=PROPERTY_CLIENT' => $clientData['ID']),'order' => array('ID' => 'DESC'));
		$invoice = new \Itin\Depohost\Invoices();
		$userInvoices = $invoice->getList($arParams);

		$readyInvoice = false;
		foreach($userInvoices as $userInvoice) {
			//Нас интересуют только неоплаченные счета
			if($userInvoice['STATUS']['VALUE_XML_ID'] != 'paid') {
				//Получаем информацию об услуге
				foreach($userInvoice['SERVICES']['VALUE'] as $userServicesID) {
					$service = new \Itin\Depohost\Services();
					$userService = $service->getById($userServicesID);
					if($userService['UF_CATALOG_XML_ID'] === 'u9') {
						$domain = str_replace("Домен: ", "", $userService["UF_PROPERTIES"][1]);
						if($domain == $arData['UF_DOMAIN']/* || strpos($userService["UF_PROPERTIES"][0], $arData['UF_DOMAIN'])*/) {
							//Если есть неоплаченный счет именно по сертификату для этого домена, то просто пытаемся отправить повторное уведомление
							$readyInvoice = true;
						}
					}
				}
			}
		}
		
		$daysInYear = date('L')?366:365;
		
		if($readyInvoice) {
			$lastInvoice = strtotime($clientData['LAST_INVOICE_DATE']['VALUE']);
			$today = time();
			$diff = (($today - $lastInvoice) % 604800);

			if($diff == 0 && $diff < 2592000) {
				$type = 'SSL-сертификат';
				$days = $daysInYear - floor((($dateNow - $arData['UF_EXPIRE']) / 86400));
				$daysEnd = numberByText($days, array('день', 'дня', 'дней'));

				$fields = array(
					'SERVICE' => $arData['UF_NAME'],
					'TYPE' => $type,
					'DAYS' => $days,
					'DAYS_END' => $daysEnd
				);
					
				$clients = new \Itin\Depohost\Clients($clientData['CC_CLIENT']['VALUE']);
				$clients->sendMail('NEW_YEAR_SERVICE_BILL', $fields, 115);
			}
		} else {
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "catalog_PRICE_5");
			$arFilter = Array("IBLOCK_ID"=>30, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_ELID"=>$arData['UF_ELID']);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
			}
				
			$invoice = new \Itin\Depohost\Invoices();
			$default_services = $invoice->getDefaultValueProperties();
			
			$invoice->setClientCardId($arData['UF_USER']);
			
			$values = $default_services;
			$values['SUMM'] = $arFields['CATALOG_PRICE_5'];
			$values['CLIENT'] = $arData['UF_USER'];
			$values['SERVICES'] = array();
			$values['SERVICES'][] = $arData['UF_INVOICE'];
			$values['ELID'] = $arData['UF_ELID'];
			$values['DATE'] = FormatDate('SHORT',time());

			$values['BILL_ID'] = $invoice->add(
				array(
					'ACTIVE'=>'Y',
					'PROPERTY_VALUES'=>$values
				)
			);

			if($values['BILL_ID'] > 0){
				$type = 'SSL-сертификат';
				$days = $daysInYear - floor((($dateNow - $arData['UF_EXPIRE']) / 86400));
				$daysEnd = numberByText($days, array('день', 'дня', 'дней'));

				$fields = array(
					'SERVICE' => $arData['UF_NAME'],
					'TYPE' => $type,
					'DAYS' => $days,
					'DAYS_END' => $daysEnd
				);
				
				$clients = new \Itin\Depohost\Clients($clientData['CC_CLIENT']['VALUE']);
				$clients->sendMail('NEW_YEAR_SERVICE_BILL', $fields, 115);
			}
		}
	}
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>