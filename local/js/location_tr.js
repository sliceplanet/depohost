/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 14.11.15.
 */
jQuery(document).ready(function($) {
  $("table [data-detail] > td").on("click", function(event) {
    var $this = $(this);
    if (!$this.hasClass("order") && !$this.hasClass("period")) {
      var path = $this.parents("[data-detail]").data("detail");
      location.href = path;
    }
  });
});
