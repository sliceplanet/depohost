<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
if (\CHTTP::GetLastStatus()==='404 Not Found')
{
  require_once $_SERVER['DOCUMENT_ROOT'].'/404.php';
}
IncludeTemplateLangFile(__FILE__);
?>
<? if (\CHTTP::GetLastStatus() !== '404 Not Found' && $APPLICATION->GetCurPage(true) != SITE_DIR . 'index.php')
{ ?>
  <?
  $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    array(
      "AREA_FILE_SHOW" => "page",
      "AREA_FILE_SUFFIX" => "inc",
      "EDIT_TEMPLATE" => "page_inc.php",
      "COMPONENT_TEMPLATE" => ".default",
      "COMPOSITE_FRAME_MODE" => "A",
      "COMPOSITE_FRAME_TYPE" => "AUTO",
      "AREA_FILE_RECURSIVE" => "Y"
    ),
    false,
    array(
      "ACTIVE_COMPONENT" => "Y"
    )
  );
  ?>
<? } ?>
  </div> <!--/.relative-wrapper-->
  <footer class="footer">
    <div class="container">
      <div class="footer__links">
        <?
        $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
          "ROOT_MENU_TYPE" => "footer",
          "MENU_CACHE_TYPE" => "A",
          "MENU_CACHE_TIME" => "360000",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(),
          "MAX_LEVEL" => "1",
          "CHILD_MENU_TYPE" => "footer",
          "USE_EXT" => "N",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
        ), false
        );
        ?>
      </div>
    </div>
    <div class="footer__pay">
      <div class="container">
        <div class="footer__pay_wrapper">
          <span class="footer__pay_label">Мы принимаем к оплате:</span>
          <div class="footer__pay-images">
            <div class="master-card"></div>
            <div class="visa"></div>
            <div class="web-money"></div>
            <div class="qiwi"></div>
            <div class="alpha-bank"></div>
            <div class="sberbank"></div>
            <div class="yandex-money"></div>
          </div>
        </div>
      </div>
      <div class="footer__social">
        <?
        //        $APPLICATION->IncludeFile(
        //          $APPLICATION->GetTemplatePath("include_areas/footer_icons.php"), Array(), Array("MODE" => "html")
        //        );
        ?>
      </div>
    </div>
  </footer>
<?
$APPLICATION->IncludeFile('/include/counters.php',
  array(),
  array('MODE' => 'php', 'NAME' => 'счетчики')
);
?>
  <a href="#" class="up"></a>
  </body>
  </html>
