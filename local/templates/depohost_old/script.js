function array_count_values(array) {
  // Counts all the values of an array
  //
  // +   original by: Ates Goral (http://magnetiq.com)
  // + namespaced by: Michael White (http://crestidg.com)

  var tmp_ar = new Object(),
    key;

  var countValue = function(value) {
    switch (typeof value) {
      case "number":
        if (Math.floor(value) != value) {
          return;
        }
      case "string":
        if (value in this) {
          ++this[value];
        } else {
          this[value] = 1;
        }
    }
  };

  if (array instanceof Array) {
    array.forEach(countValue, tmp_ar);
  } else if (array instanceof Object) {
    for (var key in array) {
      countValue.call(tmp_ar, array[key]);
    }
  }
  return tmp_ar;
}

function each(arr) {
  //  discuss at: http://phpjs.org/functions/each/
  // original by: Ates Goral (http://magnetiq.com)
  //  revised by: Brett Zamir (http://brett-zamir.me)
  //        note: Uses global: php_js to store the array pointer
  //   example 1: each({a: "apple", b: "balloon"});
  //   returns 1: {0: "a", 1: "apple", key: "a", value: "apple"}

  this.php_js = this.php_js || {};
  this.php_js.pointers = this.php_js.pointers || [];
  var indexOf = function(value) {
    for (var i = 0, length = this.length; i < length; i++) {
      if (this[i] === value) {
        return i;
      }
    }
    return -1;
  };
  // END REDUNDANT
  var pointers = this.php_js.pointers;
  if (!pointers.indexOf) {
    pointers.indexOf = indexOf;
  }
  if (pointers.indexOf(arr) === -1) {
    pointers.push(arr, 0);
  }
  var arrpos = pointers.indexOf(arr);
  var cursor = pointers[arrpos + 1];
  var pos = 0;

  if (Object.prototype.toString.call(arr) !== "[object Array]") {
    var ct = 0;
    for (var k in arr) {
      if (ct === cursor) {
        pointers[arrpos + 1] += 1;
        if (each.returnArrayOnly) {
          return [k, arr[k]];
        } else {
          return {
            1: arr[k],
            value: arr[k],
            0: k,
            key: k
          };
        }
      }
      ct++;
    }
    return false; // Empty
  }
  if (arr.length === 0 || cursor === arr.length) {
    return false;
  }
  pos = cursor;
  pointers[arrpos + 1] += 1;
  if (each.returnArrayOnly) {
    return [pos, arr[pos]];
  } else {
    return {
      1: arr[pos],
      value: arr[pos],
      0: pos,
      key: pos
    };
  }
}

function current(arr) {
  //  discuss at: http://phpjs.org/functions/current/
  // original by: Brett Zamir (http://brett-zamir.me)
  //        note: Uses global: php_js to store the array pointer
  //   example 1: transport = ['foot', 'bike', 'car', 'plane'];
  //   example 1: current(transport);
  //   returns 1: 'foot'

  this.php_js = this.php_js || {};
  this.php_js.pointers = this.php_js.pointers || [];
  var indexOf = function(value) {
    for (var i = 0, length = this.length; i < length; i++) {
      if (this[i] === value) {
        return i;
      }
    }
    return -1;
  };
  // END REDUNDANT
  var pointers = this.php_js.pointers;
  if (!pointers.indexOf) {
    pointers.indexOf = indexOf;
  }
  if (pointers.indexOf(arr) === -1) {
    pointers.push(arr, 0);
  }
  var arrpos = pointers.indexOf(arr);
  var cursor = pointers[arrpos + 1];
  if (Object.prototype.toString.call(arr) === "[object Array]") {
    return arr[cursor] || false;
  }
  var ct = 0;
  for (var k in arr) {
    if (ct === cursor) {
      return arr[k];
    }
    ct++;
  }
  return false; // Empty
}

function key(arr) {
  //  discuss at: http://phpjs.org/functions/key/
  // original by: Brett Zamir (http://brett-zamir.me)
  //    input by: Riddler (http://www.frontierwebdev.com/)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //        note: Uses global: php_js to store the array pointer
  //   example 1: array = {fruit1: 'apple', 'fruit2': 'orange'}
  //   example 1: key(array);
  //   returns 1: 'fruit1'

  this.php_js = this.php_js || {};
  this.php_js.pointers = this.php_js.pointers || [];
  var indexOf = function(value) {
    for (var i = 0, length = this.length; i < length; i++) {
      if (this[i] === value) {
        return i;
      }
    }
    return -1;
  };
  // END REDUNDANT
  var pointers = this.php_js.pointers;
  if (!pointers.indexOf) {
    pointers.indexOf = indexOf;
  }

  if (pointers.indexOf(arr) === -1) {
    pointers.push(arr, 0);
  }
  var cursor = pointers[pointers.indexOf(arr) + 1];
  if (Object.prototype.toString.call(arr) !== "[object Array]") {
    var ct = 0;
    for (var k in arr) {
      if (ct === cursor) {
        return k;
      }
      ct++;
    }
    return false; // Empty
  }
  if (arr.length === 0) {
    return false;
  }
  return cursor;
}

function formatPrice(_number) {
  var decimal = 0;
  var separator = "&nbsp;";
  var decpoint = ".";
  var format_string = "#";

  var r = parseFloat(_number);

  var exp10 = Math.pow(10, decimal); // приводим к правильному множителю
  r = Math.round(r * exp10) / exp10; // округляем до необходимого числа знаков после запятой

  rr = Number(r)
    .toFixed(decimal)
    .toString()
    .split(".");

  b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "$1" + separator);

  r = rr[1] ? b + decpoint + rr[1] : b;
  return format_string.replace("#", r) + "&nbsp;Р";
}

function implode(glue, pieces) {
  // discuss at: http://phpjs.org/functions/implode/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Waldo Malqui Silva (http://waldo.malqui.info)
  // improved by: Itsacon (http://www.itsacon.net/)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  // returns 1: 'Kevin van Zonneveld'
  // example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  // returns 2: 'Kevin van Zonneveld'
  var i = "",
    retVal = "",
    tGlue = "";
  if (arguments.length === 1) {
    pieces = glue;
    glue = "";
  }
  if (typeof pieces === "object") {
    if (Object.prototype.toString.call(pieces) === "[object Array]") {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

jQuery(document).ready(function($) {
  var menu = document.querySelector(".header__menu");
  var menuOpenBtn = document.querySelector(".header__menu_btn");
  var menuCloseBtn = document.querySelector(".header__menu_close_btn");

  $(document).on("click", ".opened + .menu-mobile__fade", function(evt) {
    evt.preventDefault();
    menuOpenBtn.classList.remove("hidden");
    menuCloseBtn.classList.add("hidden");
    menu.classList.remove("opened");
  });
  menuOpenBtn.addEventListener("click", function(evt) {
    evt.preventDefault();
    menuOpenBtn.classList.add("hidden");
    menuCloseBtn.classList.remove("hidden");
    menu.classList.add("opened");
  });
  menuCloseBtn.addEventListener("click", function(evt) {
    evt.preventDefault();
    menuOpenBtn.classList.remove("hidden");
    menuCloseBtn.classList.add("hidden");
    menu.classList.remove("opened");
  });
  $(window).scroll(function() {
    if ($(this).scrollTop() > 900) {
      $(".up").addClass("active");
    } else {
      $(".up").removeClass("active");
    }
  });
  $(".up").click(function(event) {
    event.preventDefault();
    var body = $("html, body");
    body.animate({ scrollTop: 0 }, "500");
  });

  $("[type=tel]").mask("+7(999)999-9999");
});
