$(document).ready(function() {
  /*placeholder*/
  $("input[placeholder], textarea[placeholder]").placeholder();
  /*selectbox*/
  // $("select").selectbox();
  /*prettyCheckboxes*/
  $("input[type=radio], input[type=checkbox]").prettyCheckboxes();
  /*main slider
	$('#slider').cycle({
		fx:     'fade',
		speed:   500,
		timeout: 7000
	});*/
  /*server*/
  $(".server tr:even").addClass("second");
  /*select-os*/
  $(".select-os .select-os-btn").click(function() {
    $(".select-os div").slideToggle(400);

    return false;
  });
  $(window).click(function() {
    $(".select-os div").slideUp(400);
  });
  /*logged*/
  $(".login .login-set").click(function() {
    $(".login").toggleClass("active");
  });
  $(".login").mouseleave(function() {
    $(".login").removeClass("active");
  });
  /*server-opis*/
  $(".server-tabs").delegate("div:not(.active)", "click", function() {
    $(this)
      .addClass("active")
      .siblings()
      .removeClass("active")
      .parents("div.server-container")
      .find("div.server-content")
      .hide()
      .eq($(this).index())
      .show();
  });
  /*gallery*/
  $("#gallery-trumb")
    .after('<div class="gallery-nav-pos"><div class="gallery-nav">')
    .cycle({
      fx: "scrollHorz",
      speed: 500,
      timeout: 0,
      pager: ".gallery-nav"
    });
  $(".gallery-trumb a").click(function() {
    $(".gallery-trumb a").removeClass("act");
    $(this).addClass("act");
  });
});
function change_img(id) {
  $(".img_inline").hide();
  $(".img_hidden").hide();
  $("#product" + id).fadeIn(300);
}
