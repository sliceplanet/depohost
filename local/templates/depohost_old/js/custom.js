$(document).ready(function() {
  /*selects*/
  $("#mounth_quant")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#os")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#mounth")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#method")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product1")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product2")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product3")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product4")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product5")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product6")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product7")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product8")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $("#product9")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});
  $(".hosting-select")
    .selectBox({
      mobile: true,
      menuSpeed: "fast"
    })
    .change(function() {});

  /* prevents going to links*/
  $(".selectBox-label").click(function(event) {
    event.preventDefault();
  });

  /*1c quantity*/
  $("#minusm").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plusm").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus1m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity1");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus1m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity1");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus2m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity2");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus2m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity2");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus3m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity3");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus3m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity3");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus4m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity4");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus4m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity4");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus5m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity5");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus5m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity5");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus6m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity6");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus6m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity6");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus7m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity7");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus7m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity7");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus8m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity8");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus8m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity8");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minus9m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity9");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plus9m").click(function() {
    var $input = $(this)
      .parent()
      .find("input#quantity9");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
  $("#minusoffice").click(function() {
    var $input = $(this)
      .parent()
      .find("input#office");
    var count = parseInt($input.val()) - 1;
    count = count < 0 ? 0 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $("#plusoffice").click(function() {
    var $input = $(this)
      .parent()
      .find("input#office");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });

  /* products line 1c show and hide*/
  $("#product").change(function() {
    $("#box-quantity").show();
    $("#box-product1").show();
    //		$("#box-quantity1").show();
    $("#revome1").show();
    $("#quantity").val(1);
    $("#name").val($("#product option:selected").text());
  });

  $("#product1").change(function() {
    $("#box-product2").show();
    $("#box-quantity1").show();
    $("#revome2").show();
    $("#quantity1").val(1);
    $("#name1").val($("#product1 option:selected").text());
  });

  $("#product2").change(function() {
    $("#box-product3").show();
    $("#box-quantity2").show();
    $("#revome3").show();
    $("#quantity2").val(1);
    $("#name2").val($("#product2 option:selected").text());
  });

  $("#product3").change(function() {
    $("#box-product4").show();
    $("#box-quantity3").show();
    $("#revome4").show();
    $("#quantity3").val(1);
    $("#name3").val($("#product3 option:selected").text());
  });
  $("#product4").change(function() {
    $("#box-product5").show();
    $("#box-quantity4").show();
    $("#revome5").show();
    $("#quantity4").val(1);
    $("#name4").val($("#product4 option:selected").text());
  });
  $("#product5").change(function() {
    $("#box-product6").show();
    $("#box-quantity5").show();
    $("#revome6").show();
    $("#quantity5").val(1);
    $("#name5").val($("#product5 option:selected").text());
  });
  $("#product6").change(function() {
    $("#box-product7").show();
    $("#box-quantity6").show();
    $("#revome7").show();
    $("#quantity6").val(1);
    $("#name6").val($("#product6 option:selected").text());
  });
  $("#product7").change(function() {
    $("#box-product8").show();
    $("#box-quantity7").show();
    $("#revome8").show();
    $("#quantity7").val(1);
    $("#name7").val($("#product7 option:selected").text());
  });
  $("#product8").change(function() {
    $("#box-product9").show();
    $("#box-quantity8").show();
    $("#revome9").show();
    $("#quantity8").val(1);
    $("#name8").val($("#product8 option:selected").text());
  });
  $("#product9").change(function() {
    $("#box-quantity9").show();
    $("#quantity9").val(1);
    $("#name9").val($("#product9 option:selected").text());
  });

  $("#VAR_WORK").val($("#method option:selected").text());
  $("#method").change(function() {
    $("#VAR_WORK").val($("#method option:selected").text());
  });

  /* loging */
  $(".login-set").click(function() {
    $(".login").toggleClass("active");
  });
  $(".login").mouseleave(function() {
    $(".login").removeClass("active");
  });
});
