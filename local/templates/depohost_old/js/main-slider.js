/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 13.09.18.
 */
jQuery(document).ready(function($) {
  $(".bxslider").slick({
    fade: true,
    infinite: false,
    slidesToShow: 1,
    arrows: true,
    dots: true,
    swipeToSlide: false,
    prevArrow: '<div class="slick-prev"></div>',
    nextArrow: '<div class="slick-next"></div>',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          autoplay: true,

          dots: false,
          arrows: false,
          fade: true,
          autoplaySpeed: 5000
        }
      },
      {
        breakpoint: 768,
        settings: {
          adaptiveHeight: true,
          dots: false,
          arrows: false,
          fade: true
        }
      }
    ]
  });
  $(window).scroll(function() {
    if ($(this).scrollTop() > 500) {
      $(".bxslider").slick("slickPause");
    } else {
      $(".bxslider").slick("slickPlay");
    }
  });
});
