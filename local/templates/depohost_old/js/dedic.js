jQuery(document).ready(function($) {
  // button up
  $(window).on("scroll", "", function(event) {
    var up = $(".up");
    if ($(window).scrollTop() > 1000) {
      up.fadeIn(200);
    } else {
      up.fadeOut(200);
    }
  });
  $(".up").on("click", "", function(event) {
    var body = $("html, body");
    body.animate({ scrollTop: 0 }, "500");
  });

  $(".type-server ul li a").click(function(e) {
    e.preventDefault();
    $(this).tab("show");
    document.getElementById("page-dedic").scrollIntoView({
      behavior: "smooth",
      block: "start"
    });
  });
  /******* Calculate cost rent server*/

  /*******END Calculate*/

  // fixed menu on top when scrolling
  var offsetMenu = $(".type-server").offset();
  $(".type-server").affix({
    offset: {
      top: function() {
        return offsetMenu.top;
      }
    }
  });

  var close = false;
  //open hint
  $(document).on("click", ".hint-block", function(event) {
    var parent = $(this),
      hint = parent.find(".hint-text");
    if (!close) {
      $(".hint-corner:visible").css("visibility", "hidden");
      $(".hint-text:visible").css("visibility", "hidden");
      $(".hint-close:visible").css("visibility", "hidden");

      parent.find(".hint-corner").css("visibility", "visible");
      hint.css("visibility", "visible");
      hint.find(".hint-close").css("visibility", "visible");
    } else {
      close = false;
    }
  });
  //close hint
  $(document).on("click", ".hint-close:visible", function(event) {
    var closeButton = $(this),
      parent = closeButton.parents(".hint-block");
    close = true;
    parent.find(".hint-corner").css("visibility", "hidden");
    parent.find(".hint-text").css("visibility", "hidden");
    closeButton.css("visibility", "hidden");
  });

  // configuration change term
  $(document).on("click", ".term-block", function(event) {
    var obj = $(this);
    var buttonBuy = $(this)
      .parents(".order-block")
      .find(".buy-block a");
    obj
      .addClass("active")
      .siblings()
      .removeClass("active");
    buttonBuy.data({
      months: obj.data("value"),
      sum: obj.data("price")
    });
  });
  // select
  $(".select select")
    .selectBox({ mobile: true })
    .change(function() {
      var value = $(this).val();
      $(this)
        .parents(".input-block")
        .find("input[type=checkbox]")
        .val(value);
    });
  /* prevents going to links*/
  $(".selectBox-label").click(function(event) {
    event.preventDefault();
  });
});
