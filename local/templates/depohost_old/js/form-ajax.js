/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 21.05.17.
 */
$(document).on("submit", ".ajax-form,.form-ajax", function(event) {
  var form = $(this),
    type =
      typeof form.attr("method") !== "undefined" && form.attr("method") != ""
        ? form.attr("method")
        : "post",
    bAjax = true;

  var files = form.find("[type=file]");

  if (files.length > 0) {
    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      if (file.files.length > 0 && file.files[0].size <= 5000000) {
      }
    }
  }
  var required = form.find("[required]");
  if (required.length > 0) {
    for (var i = 0; i < required.length; i++) {
      var input = required[i];
      var bNoValid = false;
      if (input.value == "") {
        bNoValid = true;
      } else if (input.getAttribute("type") === "email") {
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!pattern.test(input.value)) {
          bNoValid = true;
        }
      }
      if (bNoValid) {
        input.classList.add("is-invalid");
      } else {
        input.classList.remove("is-invalid");
      }
    }
  }
  var isSuccess = true;

  if (!isSuccess) {
    event.preventDefault();
  } else if (bAjax) {
    event.preventDefault();
    form.append('<input type="hidden" name="ajax" value="Y">');

    var formData = new FormData(this);
    $.ajax({
      url: form.attr("action"),
      type: "POST",
      method: "POST",
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      beforeSend: function() {
        form.find(".btn,button").attr("disabled", true);
      },
      success: function(res) {
        form.find(".btn,button").attr("disabled", false);
        if (form.data("result")) {
          var container = $(form.data("result"));
          container.html(res);
          return;
        }
        if (typeof res === "string" && res != "") {
          try {
            res = JSON.parse(res);
          } catch (e) {
            return;
          }
        }
        form.trigger("onFormAjaxAfterSubmit", [res]);
        if (typeof res === "object") {
          if ("MESS" in res) {
            if (form.find(".message-container").length == 0) {
              form.prepend('<div class="message-container"></div>');
            }
            form
              .find(".message-container")
              .html('<div class="alert alert-success">' + res.MESS + "</div>");
          }

          if ("ERROR" in res) {
            if (form.find(".message-container").length == 0) {
              form.prepend('<div class="message-container"></div>');
            }
            form
              .find(".message-container")
              .html('<div class="alert alert-danger">' + res.ERROR + "</div>");
          } else if ("URL" in res) {
            location.href = res.URL;
          } else if ("RELOAD" in res) {
            location.reload();
          }
        }
      }
    });
  }
});
