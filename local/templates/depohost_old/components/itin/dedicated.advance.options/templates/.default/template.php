<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']) || empty($arResult['SECTIONS']))
{
    return; // no items, good bye
}
require_once 'functions.php';
?>
<div class="head-block">
    <?= Loc::getMessage('CALC_SERVER_NAME', array('#NAME#' => $arResult['NAME'])) ?>
</div>
<div class="services-wrapper clearfix">
    <div class="services-list">
        <? foreach ($arResult['SECTIONS'] as $arSection): ?>
            <div class="item-block">
                <div class="item-name"><?= $arSection['NAME'] ?></div>
                <div class="bottom-container clearfix">
                    <div class="select xl pull-left">
                        <select style="display: none;" class="selectBox" name="section[<?= $arSection['ID'] ?>]">
                            <option value="" selected data-price="0"><?= Loc::getMessage('ADVANCE_OPTION_EMPTY_VALUE') ?></option>
                            <?
                            foreach ($arResult['ITEMS'] as $arItem):
                                if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
                                    continue;
                                ?>
                                <option value="<?= $arItem['ID'] ?>" data-price="<?= $arItem['PROPERTY_PRICE_VALUE'] ?>"><?= $arItem['NAME'] ?></option>
    <? endforeach ?>
                        </select>
                    </div>
                    <div class="price">0&nbsp;р</div>
                    <div class="hint-wrapper">
                        <div class="hint-block">
                            <div class="hint-text">
                                <div class="hint-close"></div>
    <?= $arSection['DESCRIPTION'] ?>
                            </div>
                            <div class="hint-corner"></div>
                        </div>
                    </div>
                    <!--.hint-wrapper-->
                </div>
                <!--.bottom-container-->
            </div>
            <!--.item-block-->
<? endforeach ?>
    </div>
    <!--.services-list-->
    <aside class="right-side">
        <div class="item-name"><?= Loc::getMessage('CALC_SERVER_NAME', array('#NAME#' => $arResult['NAME'])) ?></div>
        <div class="item-params">
            <div class="caption underline-title">
<? Loc::getMessage('CHARACTERISTICS_CAPTION') ?>
            </div>
            <div class="descr"><?= Loc::getMessage('CALC_CHARACTERISTICS_STRING', array('#CHARACTERISTICS_STRING#' => $arResult['CHARACTERISTICS_STRING'])) ?></div>
        </div>
        <div class="item-services-list">
            <div class="caption">
<?= Loc::getMessage('CAPTION_ADVANCE_SERVICES') ?>
            </div>
            <ul class="items-list">
                <li><div><?=Loc::getMessage('SERVICES_LIST_EMPTY')?></div></li>
                <!--                <li>
                                    <div class="item-service-name">Администрирование</div>
                                    <div class="item-service-value">Администрирование сервера нашими специалистами</div>
                                </li>-->
            </ul>
            <div class="price-block">
                <div class="price-value"><?= getPriceFormat($arResult['PRICES']['SUM']) ?> р</div>
                <div class="descr"><?= Loc::getMessage('PRICE_BLOCK_DESCRIPTION', array('#MONTHS#' => $arResult['MONTHS'], '#STRING_MONTHS#' => CUsefull::endOfWord($arResult['MONTHS'], array('месяцев', 'месяц', 'месяца')))) ?></div>
            </div>
            <button class="btn-green sm"><?= Loc::getMessage('BUTTON_ADD') ?></button>
        </div>
    </aside>
</div>
<div class="center"></div>
<form id="add-basket" action="/personal/cart/ajax-add.php" method="get">
    <input type="hidden" id="months" name="months" value="<?= $arResult['MONTHS'] ?>">
    <input type="hidden" id="string-months" value="<?= CUsefull::endOfWord($arResult['MONTHS'], array('месяцев', 'месяц', 'месяца')); ?>">
    <input type="hidden" id="price" name="FIELDS[PRICE]" value="<?= intval($arResult['PRICES']['SUM']/$arResult['MONTHS']) ?>">
    <input type="hidden" name="FIELDS[NAME]" value="<?= urlencode(Loc::getMessage('RENT_SERVER_NAME', array('#NAME#'=>$arResult['NAME']))) ?>">
    <input type="hidden" name="action" value="BUY">
    <?  foreach ($arResult['PROPERTY_ARRAY'] as $key => $arItem):?>
        <input type="hidden" name="PROPS[<?=$key?>][NAME]" value="<?= $arItem['NAME'] ?>">
        <input type="hidden" name="PROPS[<?=$key?>][VALUE]" value="<?= $arItem['VALUE'] ?>">
    <?endforeach;?>
    <input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
    <?if($_REQUEST['type']=='solution' && (int)$arResult['ID']>0):?>
        <input type="hidden" name="product_id" value="<?=(int)$arResult['ID']?>">
    <?endif?>

    <input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
    <div id="services-form-buffer"></div>
</form>
<?
$MESS['SERVICES_LIST_EMPTY'] = Loc::getMessage('SERVICES_LIST_EMPTY');
?>
<?
if ($_REQUEST['isAjax'] == 'Y')
{
    echo '<script>BX.ready(function(){';
    
    echo "BX.message(".CUtil::PhpToJSObject( $MESS, false ).");";
    include 'script.js';
    
    echo '});</script>';
    echo '<style>';
    include 'style.css';
    echo '</style>';
}
else
{
    $APPLICATION->AddHeadString('<script>BX.ready(function(){BX.message('.CUtil::PhpToJSObject( $MESS, false ).');});</script>', true);
}
?>
