jQuery(document).ready(function ($) {
	
	/* function add_desc_select(sel){
		setTimeout( function() {
			var select_kam2_description = $(sel+" :selected").attr("data-description");
			$(sel).parents(".select").find("a.select_kam.selectBox-dropdown").append('<span class="disk_numb">'+(select_kam2_description)+'</div>');
		} , 10)
	} */
	
	//add_desc_select("#select_kam2");
	//add_desc_select("#select_kam3");
	
	$(".server-container .nav.nav-tabs li a").click(function(e) {
		e.preventDefault();
		var qQ = $(this).attr("href").slice(1);
		$(".server-container .nav.nav-tabs li").removeClass("active");
		$(this).parent().addClass("active");
		$(".tab-content .tab-pane").removeClass("active");
		$("#" + qQ).addClass("active");
	});
	
	$('#select_kam1').on('change','',function(event){
        event.preventDefault();
        //alert($(this).val());
		$('#calc-cpu td.option[data-id = '+$(this).val()+']').trigger('click');
		//alert($('td.option[data-id = '+$(this).val()+']').text());
		var opt_select_kam2;
		var opt_select_kam3;
		setTimeout( function() {
			$("#select_kam2").empty();
			
			$('#calc-freq-cpu td.option').each(function(){
				//alert($(this).attr("data-id"));				
				opt_select_kam2 += '<option value="'+$(this).attr("data-id")+'"data-description="'+$(this).attr("data-description")+'">'+$(this).text()+'</option>'
			});
			
			$("#select_kam2").prepend( $(opt_select_kam2));
			
			/* $('select_kam2').selectBox().change(function () {
				//alert($(this).val());
			}); */
			$('#select_kam2').selectBox("refresh");
			
			/* $(".disk_numb").remove();
			$("#hdd a.select_kam").each(function( index ) {
			  $(this).prepend('<span class="disk_numb">Диск '+(index+1)+'</div>');
			}); */
			
			//add_desc_select("#select_kam2")
			//add_desc_select("#select_kam3")
			
			$("#select_kam3").empty();
			
			$('#calc-raid td.option').each(function(){
				//alert($(this).attr("data-id"));
				
				opt_select_kam3 += '<option value="'+$(this).attr("data-id")+'">'+$(this).text()+'</option>'
			});
			$("#select_kam3").prepend( $(opt_select_kam3));
			
			/* $('select_kam3').selectBox().change(function () {
				//alert($(this).val());
			}); */
			$('#select_kam3').selectBox("refresh");
		} , 10)	
    });
	
	$('#select_kam2').on('change','',function(event){
        event.preventDefault();
        //alert($(this).val());
		$('#calc-freq-cpu td.option[data-id = '+$(this).val()+']').trigger('click');
		//alert($('td.option[data-id = '+$(this).val()+']').text());
		var opt_select_kam3;
		setTimeout( function() {
			$("#select_kam3").empty();
			
			$('#calc-raid td.option').each(function(){
				//alert($(this).attr("data-id"));
				
				opt_select_kam3 += '<option value="'+$(this).attr("data-id")+'">'+$(this).text()+'</option>'
			});
			$("#select_kam3").prepend( $(opt_select_kam3));
			
			/* $('select_kam3').selectBox().change(function () {
				//alert($(this).val());
			}); */
			$('#select_kam3').selectBox("refresh");
			//add_desc_select("#select_kam2")
			//add_desc_select("#select_kam3")
		} , 10)
    });
	
	$('#select_kam3').on('change','',function(event){
		$('#calc-raid td.option[data-id = '+$(this).val()+']').trigger('click');
		//add_desc_select("#select_kam3")
	});
	
	$('.term-block').on('click','',function(event){
		//alert($(this).text())
		var term_t = $(this).find(".term").text();
		var price_t = $(this).find(".top .price").text();
		$(".price_month span").text(term_t);
		$(".summary-block .summ .price").text(price_t);
		
	});
	
	
    //  Calc logic
    function Calc() {
        data = {
            ram: arViewRAM,
            cpu: arCPU,
            raid: arRAID,
            hdd: arHDD,
            count_hdd: arCountHDD
        }
        this.data = data;
        this.values = {};
        this.dataChecked = {};

        //init on load
        this.createSliderRAM(firstArRam);
        this.changeGeneration($('#generation .option.checked'));
        this.handlerAllParametrs();
    }

    Calc.prototype.changeGeneration = function (generation) {
        //arViewRAM, arCPU, arRAID, arHDD
        var generationData = {
            id: generation.attr('data-id'),
            formFactor: generation.attr('data-form-factor'),
            name: generation.attr('data-name'),
        };
        this.generation = generationData;
    }

    Calc.prototype.getDataParametr = function (type) {
        // type = ['hdd','cpu','ram','raid']
        var generation = this.generation;
        var data = this.data[type][generation.id];
        return data;
    }

    Calc.prototype.getCheckedParametr = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']
        return this.dataChecked[type];
    }

    Calc.prototype.setCheckedParametr = function (type, obj) {
        // type = ['hdd','cpu','ram','raid','generation']
        this.dataChecked[type] = obj;
    }
    Calc.prototype.getAllCheckedParametr = function () {
        var arData = {};
        arData.cpu = this.getCheckedParametr('cpu');
        arData.ram = this.getCheckedParametr('ram');
        arData.raid = this.getCheckedParametr('raid');
        arData.hdd = this.getCheckedParametr('hdd');
        arData.generation = this.getCheckedParametr('generation');

        return arData;
    }

    Calc.prototype.getValue = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']

        return this.values[type];
    }

    Calc.prototype.setValue = function (type, value) {
        // type = ['hdd','cpu','ram','raid','generation']
        this.values[type] = value;
    }

     Calc.prototype.getAllValues = function () {
        // type = ['hdd','cpu','ram','raid','generation']

        return this.values;
    }

    Calc.prototype.handlerParametr = function (type) {
        // type = ['hdd','cpu','ram','raid','generation']
        var value = this.getValue(type);
        var checked = this.getCheckedParametr(type);
        var paramsBlock = $('#parameters');

        switch (type) {
            case 'generation':
                var container = $('#generation tr');
                checked = container.find('.option.checked');
                var formFactor = checked.attr('data-form-factor');

                var img = $('#server-img');
                var map = $('#map-hdd');

                value = checked.attr('data-id');

                $('name="prop[generation]"').val(value);
                $('.type-server-header span').text('Сервер, конфигурация ' + formFactor);
                //if (img.hasClass('type' + formFactor.toLowerCase()) === false)
                //{
                    img.removeClass();
                    img.addClass('server-img type' + formFactor.toLowerCase());
                    map.empty();
                    for (i = 0; i < this.getDataParametr('count_hdd'); i++)
                    {
                        map.append('<div class="deactive"></div>');
                    }
                    map.find('div:first').attr('class', 'active');
                //}
                break;
            case 'cpu':
                checked = $('#freq-cpu .option.checked');
                value = checked.attr('data-id');
                var name = checked.attr('data-name');
				var description = checked.attr('data-description');
                var cpuBlock = paramsBlock.find('#param-cpu');
                if (cpuBlock.length === 0)
                {
                    cpuBlock = paramsBlock.append('\
                                <div class="parameter-block" id="param-cpu">\
                                    <div class="caption">Процессоры</div>\
                                    <div class="parameter-value">\
                                        <div>' + name + " " + description + '</div>\
                                    </div>\
                                </div>');
                }
                else
                {
                    cpuBlock.find('.parameter-value div').text(name + " " + description);
                }

                break;
            case 'raid':
                checked = $('#calc-raid .option.checked');
                value = checked.attr('data-id');
                var name = checked.attr('data-name');
                var description = checked.attr('data-description');
                var raidBlock = paramsBlock.find('#param-raid');
                if (raidBlock.length === 0)
                {
                    raidBlock = paramsBlock.append('\
                                <div class="parameter-block" id="param-raid">\
                                    <div class="caption">Raid Контроллер</div>\
                                    <div class="parameter-value">\
                                        <div>' + description + '</div>\
                                    </div>\
                                </div>');
                }
                else
                {
                    raidBlock.find('.parameter-value div').text(name + " " +description);
                }

                break;
            case 'ram':
                checked = $('#ram');
                value = checked.attr('data-id');

                $('#ram-view').text(checked.attr('data-size') + ' ' + measureGB);
                $('#param-ram .parameter-value').text(checked.attr('data-name'));
                break;
            case 'hdd':
                checked = $('.select-hdd select').not(':disabled');
                value = {};
                var arChecked = {};
                $.each(checked, function (index) {
					if($(this).find('option:selected').text()!="Не выбрано"){
					//alert($(this).find('option:selected').text());
                    value[index] = $(this).val();
                    arChecked[index] = $(this).find('option:selected');
					}
                });
                var countArray = array_count_values(value);
                var differentValues = Object.keys(countArray).length;
                var countHDDBlocks = Math.round(differentValues / 2);
                $('[id^=param-hdd]').remove();
                $('.linetwo').remove();
                for (i = 1; i <= countHDDBlocks; i++)
                {
                    var htmlValue = "";
                    for (k = 0; k < 2; k++)
                    {
                        var curElement = each(countArray);
                        if (curElement === false)
                        {
                            break;
                        }
                        var id = curElement['key'];
                        var quantity = curElement['value'];
                        var name = checked.find('option[data-id="'+id+'"]').attr('data-name');
                        htmlValue += '<div>'+quantity+' x '+name+'</div>';
                    }
					
					if(i == 2 || i == 4){
					paramsBlock.append('\
							<div class="linetwo">\
                            <br style="clear:both;" />\
                            <div style="clear:both;background-color:#cad7dc;height:1px;margin: 15px 0;"></div>\
							</div>\
                    ');
					}
                    paramsBlock.append('\
                        <div class="parameter-block" id="param-hdd-'+i+'">\
                            <div class="caption">HDD</div>\
                            <div class="parameter-value">'+htmlValue+'</div>\
                        </div>\
                    ');
					
					
					
                }
                checked = arChecked;
                break;
        }
        this.setValue(type, value);
        this.setCheckedParametr(type, checked);
    }

    Calc.prototype.handlerAllParametrs = function () {
        this.handlerParametr('generation');
        this.handlerParametr('cpu');
        this.handlerParametr('ram');
        this.handlerParametr('raid');
        this.handlerParametr('hdd');
        var sumOneMonths = this.recalc();
        var stringCharacteristics = this.getStringCharacteristic();

        this.changeSum(sumOneMonths);

        var values = this.getNamesArray();
        $('#configuration .server-parameters .descr').text(stringCharacteristics);

        $('#configuration .buy-block a').data({
            stringCharac: stringCharacteristics,
            values:values
        });
    }

    Calc.prototype.recalc = function () {
        var arData = this.getAllCheckedParametr();
        var price;
        var sum = 0;

        $.each(arData,function(index,val){
            if (index == 'hdd')
            {
                $.each(val,function(i){
                    price = $(this).attr('data-price');
                    if (typeof price !== 'undefined')
                    {
                        sum += parseInt(price);
                    }
                });
            }
            else
            {
                price = $(val).attr('data-price');
                if (typeof price !== 'undefined')
                {
                    sum += parseInt(price);
                }
            }
        });
        return sum;
    }
    Calc.prototype.getNamesArray = function () {
        var arData = this.getAllCheckedParametr();

        var string = "";
        var arNames = {};

        $.each(arData,function(index,val){
            var name = "";
            switch (index){
                case 'cpu':
                    name = $(val).data('name');
                    break;
                case 'hdd':
                    var ar = {};
                    var names = {};
                    $.each(val,function(i){
                        var id = $(this).data('id');
                        ar[i] = id;
                        names[id] = $(this).data('name');
                    });
                    var arCount = array_count_values(ar);
                    var arName = {};
                    $.each(arCount,function(id,count){
                        arName[id] = count+'x'+names[id];
                    });
                    name = implode(', ', arName);
                    break;
                default:
                    name = typeof $(val).attr('data-description') !== 'undefined' ? $(val).attr('data-name')+' '+ $(val).attr('data-description') : $(val).attr('data-name');
                    break;
            }
            if (typeof name !== 'undefined' && name.length>0)
            {
                arNames[index] = name;
            }
        });

        return arNames;
    }
    Calc.prototype.getStringCharacteristic = function () {
        var arNames = this.getNamesArray();
        delete arNames.generation;
        return implode(', ',arNames);
    }
    Calc.prototype.changeTerms = function (price,value) {
        var terms = $('#configuration .terms-block');
        var term = terms.find('.term-block[data-value='+value+']');
        var discount = 0;
        var sum = Math.round(price*value);
        var sum_discount = 0;

        if (value != 1)
        {
            discount = Math.round(price*arDiscount['CALC_'+value+'M']/100);
            sum_discount = Math.round(discount*value);
        }
        var sum_with_discount =  Math.round(sum-sum_discount);
        var price_with_discount = Math.round(price-discount);

        term.data('price',sum_with_discount);
        if (term.hasClass('active'))
        {
            $('#configuration .buy-block a').data('sum',sum_with_discount);
        }

        term.find('.top .price').html(formatPrice(sum_with_discount));
        term.find('.top .discount').html("экономия "+formatPrice(sum_discount));
        term.find('.bottom .price').html(formatPrice(price_with_discount));
    }
    Calc.prototype.changeSum = function (sum) {
        var priceFormat = formatPrice(sum);
        var self = this;
        $('#summary .summ .price').html(priceFormat);
        var terms = $('#configuration .terms-block .term-block');

        $.each(terms,function(index,term){
            term = $(term);
            var value = parseInt(term.attr('data-value'));
            self.changeTerms(sum,value);
        });
    }

    Calc.prototype.showInput = function (type) {
//        // type = ['hdd','cpu','ram','raid']
        var data = this.getDataParametr(type);
        var html = "";
        var self = this;


        switch (type) {
            case 'hdd':
                self.templ = 'hdd_checkbox';

                for (i = 1; i <= self.getDataParametr('count_hdd'); i++)
                {
                    var val = {};
                    val['values'] = data;
                    val['i'] = i;
                    val['firstValue'] = data[0]['ID'];
                    html += self.createInputFromTemplate(val);
                }
                var container = $('#calc-hdd .select-checkbox-group');

                container.html(html);
                container.find('.select select').selectBox({mobile: true}).change(function () {
                    var value = $(this).val();
                    $(this).parents('.input-block').find('input[type=checkbox]').val(value);
                });
                container.find('input[type=checkbox]').prettyCheckboxes();

                container.find('.checkbox:first').trigger('click');

                break;
            case 'raid':
                self.templ = 'radio';
                $.each(data, function (i, val) {
                    html += self.createInputFromTemplate(val);
                });
                var container = $('#calc-raid .radio-group table tr');
                container.html(html);
                self.checkedRadio(container.find('.option:first'));
                break;
            case 'ram':
                this.createSliderRAM(data);
                break;
            case 'cpu':
                self.templ = 'radio';
                $.each(data, function (i, val) {
                    html += self.createInputFromTemplate(val);
                });
                var container = $('#calc-freq-cpu .radio-group table tr');
                container.html(html);
                self.checkedRadio(container.find('.option:first'));
                break;
        }
    }

    Calc.prototype.createInputFromTemplate = function (arParams) {
        var html = "";
        switch (this.templ)
        {
            case 'radio':
                if (typeof arParams['PRICE']==='undefined' || arParams['PRICE']=='NaN')
                {
                    arParams['PRICE'] = 0;
                }
                if (typeof arParams['NAME']==='undefined' || arParams['NAME']=='NaN' || arParams['NAME']==null || arParams['NAME'].length<1)
                {
                    arParams['NAME']="";
                }
                html = '<td class="circle"></td>\
                    <td class="option" \
                        data-id="' + arParams['ID'] + '" \
                        data-name="' + arParams['NAME'] + '" \
                        data-description="' + arParams['DESCRIPTION'] + '"\
                        data-price="' + arParams['PRICE'] + '"\
                    >\
                        ' + arParams['NAME'] + '\
                        <div class="descr">' + arParams['DESCRIPTION'] + '</div>\
                    </td>';
                break;
            case 'hdd_checkbox':
                html = '<div class="input-block">\
						<input id="hdd_' + arParams['i'] + '" name="prop[hdd[' + arParams['i'] + ']]" type="checkbox" value="' + arParams['firstValue'] + '">\
						<label style="display:none" for="hdd_' + arParams['i'] + '">Диск ' + arParams['i'] + '</label>\
						<div class="select select-hdd">\
                        <select name="hdd-type" class="select_kam small">\
						<option value="0" data-name="Не выбрано" data-id="0">Не выбрано</option>';
                $.each(arParams['values'], function (id, arVal) {
                    if (typeof arVal['PRICE']==='undefined' || arVal['PRICE']=='NaN')
                    {
                        arVal['PRICE'] = 0;
                    }
                    html += '\
                        <option value="' + arVal['ID'] + '"\
                        data-id="' + arVal['ID'] + '"\
                        data-name="' + arVal['NAME'] + '"\
                        data-price="' + arVal['PRICE'] + '"\
                        >' + arVal['NAME'] + '</option>';
                });
                html += '</select>\
                        </div>\
						</div>\
                        <!--END .select-hdd-->';
                break;
        }
        return html;
    }

    Calc.prototype.showAllInputs = function () {
        // show inputs current generation processor
        this.showInput('raid');
        this.showInput('cpu');
        this.showInput('hdd');
        this.showInput('ram');
        return this;
    }

    Calc.prototype.checkedRadio = function (obj) {
        // checked radio button
        var self = this;
        if (typeof obj !== 'object')
        {
            obj = $(obj);
        }

//        var count_hdd_active = Object.keys(this.getCheckedParametr('hdd')).length;
        var secondObj = obj.hasClass('circle') ? obj.next('.option') : obj.prev('.circle'),
                val = obj.hasClass('circle') ? secondObj.attr('data-id') : obj.attr('data-id'),
                parentGroup = obj.parents('.radio-group'),
                id = parentGroup.attr('id'),
                input = $('[name="' + id + '"]');
        var price = parseInt($('.option[data-id='+val+']').attr('data-price'));

        parentGroup.find('.checked').removeClass('checked');

        obj.addClass('checked');
        secondObj.addClass('checked');

        var price = parseInt(parentGroup.find('[data-id='+val+']').attr('data-price'));

        input.val(val);
        obj.trigger('change-calc');



    }

    Calc.prototype.createSliderRAM = function (arViewRAM) {
        if (typeof slider === 'object')
        {
            slider.destroy();
        }
        var input = $('#ram');
        var slider = $("#sliderRAM").slider({
            range: "min",
            value: 0,
            min: 0,
            max: arViewRAM.length - 1,
            step: 1,
            slide: function (event, ui) {
                var arValue = arViewRAM[ui.value];

                input.val(arValue['NAME']);
                input.attr({
                    'data-id': arValue['ID'],
                    'data-size': arValue['SIZE'],
                    'data-name': arValue['NAME'],
                    'data-price': arValue['PRICE']
                });
                $(this).trigger('change-calc');
            }
        });
        var arFirstValue = arViewRAM[0];
        input.val(arFirstValue['NAME']);
        input.attr({
            'data-id': arFirstValue['ID'],
            'data-size': arFirstValue['SIZE'],
            'data-name': arFirstValue['NAME'],
            'data-price': arFirstValue['PRICE']
        });

        var width = 100 / arViewRAM.length;
        $('.slide-grath').empty();
        var margin = width / 2;
        var styles = {
            marginLeft: function () {
                return margin + "%";
            },
            width: function () {
                var tempWidth = 100 - (2 * margin);
                return tempWidth + "%";
            }

        }
		//$('.sel_kam').css( "width", styles.marginLeft() );
        $('.slide').css(styles);
        for (i = 0; i < arViewRAM.length; i++)
        {
            $('.slide-grath').append('<div style="width: ' + width + '%;">' + arViewRAM[i]['SIZE'] + '</div>');
        }
    }
	
	

    Calc.prototype.mapHDD = function (count_active) {
        var map = $('#map-hdd');
        map.empty();
        for (i = 0; i < count_active; i++)
        {
            map.append('<div class="active"></div>');
        }
        for (; i < this.getDataParametr('count_hdd'); i++)
        {
            map.append('<div class="deactive"></div>');
        }
    }
    var calc = new Calc();


    $('#generation').on('click', 'td', function (event) {
        if ($(this).hasClass('option') === true)
        {
            var generation = $(this);
        }
        else
        {
            var generation = $(this).next('td.option');
        }
        calc.changeGeneration(generation);
        calc.showAllInputs();
    });

    //radio group
    $('.radio-group').on('click', '.circle, .option', function (event) {
        calc.checkedRadio($(this));
    });
    //checkbox hdd
    /* $('.select-checkbox-group').on('change', '.input-block [type=checkbox]', function (event) {
        var block = $(this).parents('.input-block'),
                bChecked = $(this).attr('checked');
        var active_checkbox = block.parent().find('[type=checkbox]:checked'),
            count_active = active_checkbox.length;

        if (bChecked === 'checked')
        {
            block.find('.select select').selectBox('enable');
        }
        else
        {
            block.find('.select select').selectBox('disable');
        }

        calc.mapHDD(count_active);
        $(this).trigger('change-calc');
    }); */
	
	
	//calc.mapHDD(1);
	
	$("#hdd a.select_kam").each(function( index ) {	  
	  $(this).prepend('<span class="disk_numb">Диск '+(index+1)+'</div>');
	});
	
	$('.select-checkbox-group').on('change', 'select', function (event) {
		$(".disk_numb").remove();
		$("#hdd a.select_kam").each(function( index ) {
		  $(this).prepend('<span class="disk_numb">Диск '+(index+1)+'</div>');
		});
	});
	
    $('.select-checkbox-group').on('change', '.input-block select', function (event) {
        $(this).trigger('change-calc');
		var count_hdd_selected=0;
		$(".select-checkbox-group .input-block select :selected").each(function( index ) {
		  //console.log( index + ": " + $( this ).text() );
		  if($(this).text() != "Не выбрано"){
			count_hdd_selected++; 
		  }
		});		
		calc.mapHDD(count_hdd_selected);
		
    });
    $('#configuration').on('change-calc', '', function () {
        calc.handlerAllParametrs();
    });
    $('.buy-block > a').on('click','',function(event){
        event.preventDefault();
        var data = $(this).data();
        data['step'] = 2;
        data['isAjax'] = 'Y';

        console.log(data);
        $.ajax({
            url:'',
            type: 'get',
            data: data,
            success: function(res){
                if (res.length>0)
                {
                    $('.selectBox-dropdown-menu').remove();
                    $('#page-dedic').html(res);
                    var body = $("html, body");
                    var top = $('#page-dedic').offset().top;
                    body.animate({scrollTop:top}, '500');
                }
                else
                {
                    alert('ошибка ответа')
                }
            }
        });
    });
	
	
	
});


$( "#sliderRAM" ).slider({
	  change: function( event, ui ) {
		  setTimeout( function() {
			  //alert($('.ui-slider-range').css("width").slice(0, -2));
			  var sRAM = +$('#sliderRAM').css("width").slice(0, -2);
			  var www = +$('.ui-slider-range').css("width").slice(0, -2);
			  var mmll = +$('#sliderRAM').css("margin-left").slice(0, -2);
			  if(sRAM == www){
				 $('.sel_kam').css( "width",  www + mmll + mmll +"px"); 
			  }
			  else{
				  $('.sel_kam').css( "width",  www + mmll +"px");
			  }
			  var size = $('#ram').attr( "data-size");
			  $('.slide-grath div').each(function(){				
				 if($(this).text()==size){
					$(this).css("font-weight","bold") 
				 }
				 else{
					$(this).css("font-weight","normal") 
				 }
			  });
			  
			$(".select-hdd:first select :nth-child(2)").attr("selected", "selected");
			$('.select_kam').selectBox("refresh");
			//$(".select-checkbox-group .input-block select").trigger('change');
			$(".select-checkbox-group .input-block select").trigger('change-calc');
		  } , 10)  
	  }
	});