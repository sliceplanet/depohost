<?
$MESS ['TH_NUMBER'] = "Номер";
$MESS ['TH_DATE'] = "Дата";
$MESS ['TH_DESCRIPTION'] = "Назначение&nbsp;платежа";
$MESS ['TH_STATUS'] = "Статус";
$MESS ['TH_SUMM'] = "Сумма";
$MESS ['TH_DOCUMENT'] = "Файл";
$MESS ['TH_TYPE_PAYMENT'] = "Оплата";
$MESS ['EMPTY_RESULT'] = "Документы отсутствуют";

$MESS ['SB_BILL'] = "Квитанция";
$MESS ['BILL'] = "Счет";
$MESS ['EMONEY'] = "Электронный платеж";

?>