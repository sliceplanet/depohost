<?
$MESS['TH_NAME'] = "Наименование";
$MESS['TH_PROPERTIES'] = "Состав";
$MESS['TH_ACTION'] = "&nbsp;";
$MESS['TH_IP_ADDR'] = "IP-адрес";
$MESS['TH_SERVER_ID'] = "Номер сервера";
$MESS['BUTTON_ACTION'] = "Отправить запрос";
$MESS['BUTTON_NO_ACCESS'] = "Запрос отправлен";
$MESS['EMPTY_RESULT'] = "--Нет данных--";
$MESS['DESCRIPTION'] = "Для отправки запроса на перезагрузку, выберите, пожалуйста, ваш сервер и нажмите кнопку, <b>отправить запрос</b>";
?>