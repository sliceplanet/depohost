<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$APPLICATION->AddHeadString('<meta http-equiv="X-UA-Compatible" content="IE=edge" />',true);
//$APPLICATION->AddHeadString('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">',true);
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']))
{
    return; // no items, good bye
}
require_once 'functions.php';
$item = $arResult['FIRST_ITEM'];
?>
<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form"
      xmlns="http://www.w3.org/1999/html">
    <?// проверка идентификатора сессии ?>
    <?=bitrix_sessid_post();?>
<section id="configuration" class="tab-pane active clearfix" style="padding-top:10px">
    <div class="left-block">
        <div class="presentation-block">
            <div id="server-formfactor" class="type-server-header">
                <span><?echo $item['NAME']?></span>
                <? showHint($arResult['HINTS']['HINT_CALC_TYPE_SERVER'], "icon-white") ?>
            </div>
            <div class="blue-block clearfix">
                <div id="server-img" class="server-img <?= $item['FORM_FACTOR'] == '2U' ? "type2u" : "type1u" ?>">
                    <div id="map-hdd">
                        <div class="active"></div>
                        <?
                        for ($i = 1; $i < $item['COUNT_HDD']; $i++)
                        {
                            echo "<div class=\"deactive\"></div>";
                        }
                        ?>
                    </div>
                </div>
                
                <div>
                    <div id="parameters" class="parameters-block">
                        <?
                        // Параметры
                        $hdd_i = 0;
                        $sum = 0;
                        $arNamesProperties = array();
                        $default_count_hdd = $arResult['DEFAULT']['COUNT_HDD'];
                        foreach ($item['PROPERTIES'] as $code => $arValues):
                            $arValue = current($arValues);
                            switch ($code)
                            {
                                case 'HDD':
                                    $hdd_i++;
                                    $id = 'param-hdd-' . $hdd_i;
                                    $name = $default_count_hdd . "x" . $arValue['NAME'];
                                    break;
                                case 'RAID':
                                    $id = 'param-' . strtolower($code);
                                    $name = $arValue['DESCRIPTION'];
                                    break;
                                default:
                                    $id = 'param-' . strtolower($code);
                                    $name = $arValue['NAME'];
                                    break;
                            }
                                $sum += (int) $arValue['PRICE'];
                                $arNamesProperties[] = $name;
                                ?>
                                <div id="<?= $id ?>" class="parameter-block">
                                    <div class="caption"><?= Loc::getMessage('CALC_PARAMETRS_CAPTION_' . $code) ?></div>
                                    <div class="parameter-value">
                                        <div><?= $name ?></div>
                                    </div>
                                </div>
								<?$m++?>
								<? if ($m % 3 == 0): ?>
									<div class="">
									<br style="clear:both;" />
									<div style="clear:both;background-color:#cad7dc;height:1px;margin: 15px 0;"></div>
									</div>
								<? endif; ?>
                        <? endforeach; ?>
                    </div>
                </div>

            </div>  <!-- END .blue-block-->

        </div><!-- . presentation-block-->
        
    </div>
	
	<div class="right-block" style="width: 570px;">
		<span class="group-title" style="margin-left: 15px;"><?= Loc::getMessage('CALC_TITLE_CONFIGURATION') ?></span>
        <div class="select_kam_wrap">
            <div class="h5">
                <?= Loc::getMessage('CALC_TITLE_GROUP') ?>
            </div>
            <div style="border: medium none;height: 50px;">
                <div class="select-checkbox-group">
                    <div class="select">
                        <select name="select_groups" id="select_groups" class="select_kam small"<?= $disabled ?>>
                            <option value="0"> <?= Loc::getMessage('WHITHOUT_GROUP') ?></option>
                            <?foreach ($arResult['GROUPS'] as $id => $group):?>
                                <option value="<?= $group['ID'] ?>"><?= $group['NAME'] ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
		<div class="select_kam_wrap">
			<div class="h5">
                <b><?= Loc::getMessage('CALC_TITLE_GENERATION') ?> *</b>
				<? showHint($arResult['HINTS']['HINT_CALC_GENERATION']) ?>
				<!--.hint-block-->
			</div>
			<div style="border: medium none;height: 50px;">
			<div class="select-checkbox-group">
			<div class="select">
			<select name="select_kam1" id="select_kam1" class="select_kam small"<?= $disabled ?>>
				<option value="<?= $item['ID'] ?>"
					data-form-factor="<?= $item['FORM_FACTOR'] ?>"
				><?= $item['NAME'] ?></option>
				<?foreach ($arResult['ITEMS'] as $id => $arItem):?>
					<option value="<?= $arItem['ID'] ?>"
						data-form-factor="<?= $arItem['FORM_FACTOR'] ?>"
					><?= $arItem['NAME'] ?></option> 
				<? endforeach; ?>
			 </select>
			 </div>
			 </div>
			 </div>
		 </div>
		 
		 <div class="select_kam_wrap">
			 <div class="h5">
				<b><?= Loc::getMessage('CALC_TITLE_CPU') ?> *</b>
				<? showHint($arResult['HINTS']['HINT_CALC_CPU']) ?>
				<!--.hint-block-->
			</div>
			 <div style="border: medium none;height: 50px;">
			 <div class="select-checkbox-group">
			 <div class="select">
			 <select name="select_kam2" id="select_kam2"  class="select_kam small"<?= $disabled ?>>
				<? showSelectButton($item['PROPERTIES']['CPU']); ?>
			 </select>
			 </div>
			 </div>
			 </div> 
		 </div> 
		 
		 <div class="select_kam_wrap">
			 <div class="h5">
				<?= Loc::getMessage('CALC_TITLE_RAID') ?>
                <? showHint($arResult['HINTS']['HINT_CALC_RAID']) ?>
				<!--.hint-block-->
			</div>
			 <div style="border: medium none;height: 50px;">
			 <div class="select-checkbox-group">
			 <div class="select">
			 <select name="select_kam3" id="select_kam3"  class="select_kam small"<?= $disabled ?>>
				<? showSelectButton($item['PROPERTIES']['RAID']); ?>
			 </select>
			 </div>
			 </div>
			 </div> 
		 </div>
		 <div class="select_kam_wrap" style="width:270px;">
			<div id="calc-ram" class="calc-block"  style="width:270px;">
				<div class="h5">
                    <b><?= Loc::getMessage('CALC_TITLE_RAM') ?> *</b>
					<? showHint($arResult['HINTS']['HINT_CALC_RAM']) ?>
					<!--.hint-block-->
				</div>
				<? $arFistValue = first_value($item['PROPERTIES']['RAM']) ?>
				<div id="ram-view" class="ram-view"><?=$arFistValue['SIZE']?> <?= Loc::getMessage('CALC_MEASURE_GB') ?></div>
                <input type="hidden" name="select_ram" value="<?=$arFistValue['ID']?>" id="select_ram"/>
				<input
					type="hidden"
					name="prop[ram]"
					id="ram"
					value="<?=$arFistValue['NAME']?>"
					data-size="<?=$arFistValue['SIZE']?>"
					data-id="<?=$arFistValue['ID']?>"
					data-name="<?=$arFistValue['NAME']?>"
					data-price="<?=$arFistValue['PRICE']?>">
				<div class="slider-wrapper clearfix">
					<div class="slide-grath">
					</div>
					<div class="slide" id="sliderRAM">
					</div>
					<script type="text/javascript">
						<? $arViewRAM = $arResult['PROPERTIES']['RAM']; ?>
						var arViewRAM = JSON.parse('<?= json_encode($arViewRAM) ?>');
						var slideWidth = $('.slide').innerWidth();
						var measureGB = '<?= Loc::getMessage('CALC_MEASURE_GB') ?>';
						var firstArRam = arViewRAM['<?= $item['ID'] ?>'];
					</script>

				</div>
			</div>
			<div class="sel_kam" style="width:230px;height:5px;background-color:#8dc95e;position:relative;top: -43px;">
			</div>
		 </div>
		 
		 <div class="select_kam_wrap" style="width:570px;">
             <input type="hidden" name="select_hdd" id="select_hdd" />
			<div id="calc-hdd" class="calc-block">
				<div class="h5">
					<b><?= Loc::getMessage('CALC_TITLE_HDD') ?> *</b>
					<? showHint($arResult['HINTS']['HINT_CALC_HDD']) ?>
					<!--.hint-block-->
				</div>
				<div class="select-checkbox-group" id="hdd">
					<?
					$bFirst = true;
					$ccc=1;
					for ($i = 1; $i <= $item['COUNT_HDD']; $i++):
						if ($i<=$default_count_hdd)
						{
							$checked = " checked";
							$disabled = "";
							$bFirst = false;
						}
						else
						{
							$checked = "";
							$disabled = " disabled";
						}
						$firstValue = first_value($item['PROPERTIES']['HDD']);
						?>
						
						<?$ccc++?>
						<div class="input-block"<? if ($ccc % 2 == 0): ?> style="float:left;"<? endif; ?>	>
							<input id="hdd_<?= $i ?>" name="prop[hdd[<?= $i ?>]]" type="checkbox" value="<?= $firstValue['ID'] ?>"<?= $checked ?>>
							 <label style="display:none" for="hdd_<?= $i ?>"><?= Loc::getMessage('CALC_LABEL_HDD', array('#NUMBER#' => $i)) ?></label>
							<!--<div style="position:relative;">Диск <?=$ccc-1?></div>-->
							<div class="select select-hdd">
								<select name="hdd-type" class="select_kam small"<?//= $disabled ?>>
									<option value="0" data-name="Не выбрано" data-id="0">Не выбрано</option>
									<? foreach ($item['PROPERTIES']['HDD'] as $key => $arValue): ?>
										<option value="<?= $arValue['ID'] ?>"
											data-id="<?= $arValue['ID'] ?>"
											data-name="<?= $arValue['NAME'] ?>"
											data-price="<?= $arValue['PRICE'] ?>"
										><?= $arValue['NAME'] ?></option>
									<? endforeach; ?>
								</select>
							</div>
							<!--END .select-hdd-->
						</div>
						<!--END .input-block-->
					<? endfor; ?>
				</div>
				<script>
					var arHDD = JSON.parse('<?=json_encode($arResult['PROPERTIES']['HDD'])?>');
					var arCountHDD = JSON.parse('<?=  json_encode($arResult['PROPERTIES']['COUNT_HDD'])?>');
				</script>
				<!--#hdd-->
			</div>
			<!--#calc-hdd-->
		</div> 
	</div> 
	
	
	
	
	
    <div id="calc" class="right-block">
        <div id="calc-cpu" class="calc-block" style="display:none;">
            <div class="h5">
                <?= Loc::getMessage('CALC_TITLE_GENERATION') ?>
                <? showHint($arResult['HINTS']['HINT_CALC_GENERATION']) ?>
                <!--.hint-block-->
            </div>
            <div class="radio-group" id="generation">
                <table>                    
                    <tr>
                        <td class="circle checked"></td>
                        <td class="option checked" data-id="<?= $item['ID'] ?>" data-name="<?= $item['NAME'] ?>" data-form-factor="<?= $item['FORM_FACTOR'] ?>">Сервер <?= $item['NAME'] ?></td>
                        <?
                        $i = 0;
                        foreach ($arResult['ITEMS'] as $id => $arItem):
                            $i++;
                            ?>
                            <? if ($i % 3 == 0): ?>
                            </tr>
                        </table>
                        <table class="half">
                            <tr>
                            <? endif ?>
                            <td class="circle"></td>
                            <td class="option" data-id="<?= $arItem['ID'] ?>" data-name="<?= $arItem['NAME'] ?>" data-form-factor="<?= $arItem['FORM_FACTOR'] ?>">Сервер <?= $arItem['NAME'] ?></td>

                        <? endforeach; ?>
                    </tr>
                </table>
            </div>
            <!--#cpu-->
            <input type="hidden" name="prop[generation]" value="<?= $item['ID'] ?>" />
        </div>
        <!--#calc-cpu-->
        <div id="calc-freq-cpu" class="calc-block" style="display:none;">
            <div class="h5">
                <?= Loc::getMessage('CALC_TITLE_CPU') ?>
                <? showHint($arResult['HINTS']['HINT_CALC_CPU']) ?>
                <!--.hint-block-->
            </div>
            <div class="radio-group" id="freq-cpu">
                <table>
                    <tr>
                        <? showRadioButton($item['PROPERTIES']['CPU']); ?>
                    </tr>
                </table>
            </div>
            <!--#cpu-->
            <? $arFistValue = first_value($item['PROPERTIES']['CPU']) ?>
            <input type="hidden" name="prop[cpu]" value="<?= $arFistValue['ID'] ?>" />
            <script>
                var arCPU = JSON.parse('<?= json_encode($arResult['PROPERTIES']['CPU']) ?>');
            </script>
        </div>
        <!--#calc-cpu-->
        
        <!--#calc-ram-->
        <div id="calc-raid" class="calc-block" style="display:none;">
            <div class="h5">
                <?= Loc::getMessage('CALC_TITLE_RAID') ?>
                <? showHint($arResult['HINTS']['HINT_CALC_RAID']) ?>
                <!--.hint-block-->
            </div>
            <div class="radio-group" id="raid">
                <table>
                    <tr>
                        <? showRadioButton($item['PROPERTIES']['RAID']); ?>
                    </tr>
                </table>
            </div>
            <!--#raid-->
            <? $arFistValue = first_value($item['PROPERTIES']['RAID']) ?>
            <input type="hidden" name="prop[raid]" value="<?= $arFistValue['ID'] ?>" />
            <script>
                var arRAID = JSON.parse('<?= json_encode($arResult['PROPERTIES']['RAID']) ?>');
            </script>
        </div>
        <!--#calc-raid-->
        
    </div>
    <!--#calc-->
    <?
    // Суммы и скидки по периодам
    $arSum = array(
        '1m' => $sum,
        '3m' => round($sum * 3 - $sum * 3 * $arResult['DISCOUNTS']['CALC_3M'] / 100),
        '6m' => round($sum * 6 - $sum * 6 * $arResult['DISCOUNTS']['CALC_6M'] / 100),
        '12m' => round($sum * 12 - $sum * 12 * $arResult['DISCOUNTS']['CALC_12M'] / 100),
    );
    $arDiscount = array(
        '1m' => $sum - $arSum['1m'],
        '3m' => 3 * $sum - $arSum['3m'],
        '6m' => 6 * $sum - $arSum['6m'],
        '12m' => 12 * $sum - $arSum['12m'],
    );
    $arSumInMonth = array(
        '1m' => $arSum['1m'],
        '3m' => round($arSum['3m'] / 3),
        '6m' => round($arSum['6m'] / 6),
        '12m' => round($arSum['12m'] / 12),
    );

    array_price_format($arSum);
    array_price_format($arDiscount);
    array_price_format($arSumInMonth);
    ?>
    <script>
        var arDiscount = JSON.parse('<?=  json_encode($arResult['DISCOUNTS'])?>');
    </script>
    <div class="clearfix"></div>
    <div class="more_data">
        <div class="h5">
            ALT
        </div>
        <p><input type="text" name="ALT" value="" placeholder="ALT"/></p>
        <div class="h5">
            Дополнительные возможности
        </div>
        <p><input type="text" name="ADVANCE" value="" placeholder="Дополнительные возможности" /></p>
        <div class="h5">
            <b>Доступное количество *</b>
        </div>
        <p><input type="text" name="QUANTITY" value="" placeholder="Доступное количество" /></p>

    </div>
    <div class="clearfix"></div>
	<br style="clear:both" />
    <div class="order-block">
        <div class="terms-block">
            <div class="term-block active" id="term_1" data-value="1">
                <div class="top">
                    <div class="term">1 месяц</div>
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSum['1m'], " " => "&nbsp;")) ?></div>
                    <div class="discount"><?= Loc::getMessage('CALC_DISCOUNT', array('#DISCOUNT_PRICE#' => $arDiscount['1m'], " " => "&nbsp;")) ?></div>
                </div>
                <div class="bottom">
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSumInMonth['1m'], " " => "&nbsp;")) ?></div>
                    <?= Loc::getMessage('CALC_MEASURE') ?>
                </div>
            </div>
            <div class="term-block" id="term_2" data-value="3">
                <div class="top">
                    <div class="term">3 месяца</div>
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSum['3m'], " " => "&nbsp;")) ?></div>
                    <div class="discount"><?= Loc::getMessage('CALC_DISCOUNT', array('#DISCOUNT_PRICE#' => $arDiscount['3m'], " " => "&nbsp;")) ?></div>
                </div>
                <div class="bottom">
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSumInMonth['3m'], " " => "&nbsp;")) ?></div>
                    за сервер
                </div>
            </div>
            <div class="term-block" id="term_3" data-value="6">
                <div class="top">
                    <div class="term">6 месяцев</div>
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSum['6m'], " " => "&nbsp;")) ?></div>
                    <div class="discount"><?= Loc::getMessage('CALC_DISCOUNT', array('#DISCOUNT_PRICE#' => $arDiscount['6m'], " " => "&nbsp;")) ?></div>
                </div>
                <div class="bottom">
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSumInMonth['6m'], " " => "&nbsp;")) ?></div>
                    за сервер
                </div>
            </div>
            <div class="term-block" id="term_4" data-value="12">
                <div class="top">
                    <div class="term">12 месяцев</div>
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSum['12m'], " " => "&nbsp;")) ?></div>
                    <div class="discount"><?= Loc::getMessage('CALC_DISCOUNT', array('#DISCOUNT_PRICE#' => $arDiscount['12m'], " " => "&nbsp;")) ?></div>
                </div>
                <div class="bottom">
                    <div class="price"><?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => $arSumInMonth['12m'], " " => "&nbsp;")) ?></div>
                    за сервер
                </div>
            </div>
        </div>
		<div style="border-bottom: 1px solid #dde4e7;overflow: hidden;">
		<div id="summary" class="summary-block">
            <div class="server-parameters">
                <div class="caption underline-title">
                    <?= Loc::getMessage('CALC_TITLE_CHARACTERISTICS') ?>
                </div>
                <div class="descr">
                    <? $stringParametrs = implode(', ', $arNamesProperties) ?>
                    <?= Loc::getMessage('CALC_CHARACTERISTICS_STRING', array('#CHARACTERISTICS_STRING#' => $stringParametrs)) ?>
                </div>
            </div>
            <div class="summ">
                <div class="caption">
                    <?= Loc::getMessage('CALC_CAPTION_SERVER_TOTAL') ?>
                </div>
                <div class="price">
                    <?= Loc::getMessage('CALC_PRICE', array('#PRICE#' => number_format($sum, 0, "", " "), " " => "&nbsp;")) ?>
                </div>
				<div class="price_month">
                    за <span>1 месяц</span>
                </div>
            </div>
        </div>  <!--.summary-block -->
        <div class="buy-block">
<!--            <button type="submit" class="btn-green">--><?//= Loc::getMessage('BUTTON_ADD') ?><!--</button>-->
          <a class="btn-green"
               data-months="1"
               data-type="configurator"
               href="#"><?= Loc::getMessage('BUTTON_ADD')?>
          </a>
        </div>
        </div>
    </div>
    <!--.order-block-->
</section>
<!-- END #configuration -->
   </form>