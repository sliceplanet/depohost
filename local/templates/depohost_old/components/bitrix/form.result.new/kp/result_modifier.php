<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule('sale');
$dbBasketItems = CSaleBasket::GetList(
    array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
    array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
    false,
    false,
    array("ID",  "QUANTITY", "PRICE",)
);
$sum = 0;
while ($arItems = $dbBasketItems->Fetch())
{
    $sum += $arItems["QUANTITY"] * $arItems["PRICE"];
}
$arResult["total_sum"] = $sum > 0 ? SaleFormatCurrency($sum, "RUB"):0;
?>