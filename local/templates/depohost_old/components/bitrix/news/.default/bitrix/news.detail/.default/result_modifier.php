<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
  die();
}
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 23.09.18
 * Time: 20:44
 */
if (empty($arResult["PREVIEW_PICTURE"]))
{
  $arResult["PREVIEW_PICTURE"] = array(
    'SRC' => '/images/news-default.jpg',
    'WIDTH' => '307',
    'HEIGHT' => '164',
  );
}