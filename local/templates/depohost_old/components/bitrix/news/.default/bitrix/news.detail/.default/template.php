<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "name": "<?=$arResult['NAME']?>",
  "headline": "<?=$arResult['NAME']?>",
  <?if(is_array($arResult["PREVIEW_PICTURE"])):?>
  "image": {
    "@type": "ImageObject",
    "url": "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult["PREVIEW_PICTURE"]['SRC']?>",
    "height": <?=$arResult["PREVIEW_PICTURE"]['HEIGHT']?>,
    "width": <?=$arResult["PREVIEW_PICTURE"]['WIDTH']?>
  },
  <?endif?>
  "datePublished": "<?=date('c',MakeTimeStamp($arResult["ACTIVE_FROM"]))?>",
  <?if(MakeTimeStamp($arResult["ACTIVE_FROM"])>MakeTimeStamp($arResult["TIMESTAMP_X"])):?>
  "dateModified": "<?=date('c',MakeTimeStamp($arResult["ACTIVE_FROM"]))?>",
  <?else:?>
  "dateModified": "<?=date('c',MakeTimeStamp($arResult["TIMESTAMP_X"]))?>",
  <?endif?>
  "publisher": {
    "@type": "Organization",
    "name": "Artextelecom",
    "logo": {
      "@type": "ImageObject",
      "url": "https://<?=$_SERVER['SERVER_NAME']?>/images/logo-data.png",
      "width": 292,
      "height": 60
    }
  },
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult['DETAIL_PAGE_URL']?>"
  },
  "author": {
    "@type": "Organization",
    "name": "Artextelecom",
    "logo": {
      "@type": "ImageObject",
      "url": "https://<?=$_SERVER['SERVER_NAME']?>/images/logo-data.png",
      "width": 292,
      "height": 60
    }
  },
  "url" : "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult['DETAIL_PAGE_URL']?>",
  "description": "<?=strip_tags($arResult['PREVIEW_TEXT'])?>",
  "articleBody": "<?=strip_tags($arResult['DETAIL_TEXT'])?>"
}
</script>
<div class="news-detail">
	<?if(is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h1 class="news-detail-title"><?=$arResult["NAME"]?></h1>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
</div>