<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}

$parseurlRU = parse_url($_SERVER["REQUEST_URI"]);
if (preg_match("%PAGEN_(\d+)?=(\d+)%", $parseurlRU["query"],$matchesPag)) {
	if ( !empty($arResult["NavPageNomer"]) && ($arResult["NavPageNomer"] === $matchesPag[2]) ) {
		$APPLICATION->SetPageProperty("title", $APPLICATION->GetTitle() . " - Страница " . $matchesPag[2] . " | ArtexTelecom");
		$APPLICATION->SetPageProperty("description", $APPLICATION->GetTitle() . ". Страница " . $matchesPag[2] . ". ArtexTelecom.");
	}
}
