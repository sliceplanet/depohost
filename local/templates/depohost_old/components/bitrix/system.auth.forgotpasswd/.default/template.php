<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<div class="n-page-wrapper">
  <div class="aut-l"></div>
  <div class="aut-r"></div>
  <div class="container n-auth">
    <div class="row">
      <div class="col-xs-12">
        <section class="block">
          <div class="block-txt">

            <h1 class="h1">Авторизация<span><? $APPLICATION->ShowTitle(false) ?></span></h1>
            <?
            if ($arParams["~AUTH_RESULT"]):
            ShowMessage($arParams["~AUTH_RESULT"]);
            else:?>
              <p><?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?></p>
            <?
            endif;
            ?>
            <form name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
              <input type="hidden" name="AUTH_FORM" value="Y">
              <input type="hidden" name="TYPE" value="SEND_PWD">
              <? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
              <? endif ?>
              <? foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
              <? endforeach ?>

              <table class="bx-auth-table">
                <tbody>
                <tr class="bx-auth__wrap">
                  <td class="strong" colspan="2"><?= GetMessage("AUTH_GET_CHECK_STRING") ?></td>
                </tr>
                <tr class="bx-auth__wrap">
                  <td class="bx-auth-label">Номер договора:</td>
                  <td><input class="bx-auth-input" type="text" name="USER_LOGIN" maxlength="255"
                             value="<?= $arResult["LAST_LOGIN"] ?>"></td>
                </tr>
                <tr class="bx-auth__wrap">
                  <td></td>
                  <td class="authorize-submit-cell">
                    <input type="submit" name="send_account_info" value="<?= GetMessage("AUTH_SEND") ?>">
                    <div class="forgot-password"><a href="<?= $arResult["AUTH_AUTH_URL"] ?>"
                                                    rel="nofollow"><?= GetMessage("AUTH_AUTH") ?></a></div>
                  </td>
                </tr>
                </tbody>
              </table>
            </form>
            <script type="text/javascript">
              document.bform.USER_LOGIN.focus();
            </script>
          </div>
          <div class="block-pm auth">
            <div class="block-pm-ttl">Вы находитесь на странице восстановления пароля входа в личный кабинет</div>
            <div class="block-pm-item">В Личном кабинете вы можете
              полноценно управлять своими услугами.
            </div>
            <div class="block-pm-item"> Обращаться в финансовую
              службу.
            </div>
            <div class="block-pm-item"> Получать доступ к
              закрывающим документам.
            </div>
            <div class="block-pm-item"> Получить доступ к договору и
              приложения к нему на оказываемые услуги.
            </div>

            <div class="block-pm-item"> Обращаться в техническую
              поддержку.
            </div>
            <div class="block-pm-item"> Получить доступ в панель
              управления DNS записями вашего домена.
            </div>
            <div class="block-pm-item"> Получить доступ к FTP
              серверу прикрепленного к вашему аккаунту.
            </div>
            <div class="block-pm-item"> Управлять своими данными и
              многое другое.
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
