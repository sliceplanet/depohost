<?
$MESS['CT_BCSL_ELEMENT_DELETE_CONFIRM'] = 'Будет удалена вся информация, связанная с этой записью. Продолжить?';
$MESS['CT_BCSL_THEAD_NAME'] = 'DT #';
$MESS['CT_BCSL_THEAD_CPU'] = 'CPU';
$MESS['CT_BCSL_THEAD_RAM'] = 'RAM';
$MESS['CT_BCSL_THEAD_HDD'] = 'HDD';
$MESS['CT_BCSL_THEAD_SSD'] = 'SSD';
$MESS['CT_BCSL_THEAD_OS'] = 'OS';
$MESS['CT_BCSL_THEAD_IP'] = 'IP';
$MESS['CT_BCSL_THEAD_PRICE'] = 'Цена';
$MESS['CT_BCSL_THEAD_ORDER'] = 'Заказать';
?>