<?php use Bitrix\Main\Page\Asset;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/**  
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 03.05.15
 * Time: 10:43
 */
Asset::getInstance()->addCss('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin,cyrillic-ext');
