<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
<ul class="row" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
    <?
    foreach($arResult as $arItem):?>
        <li class="col-sm-3 footer__link" itemprop="name"><a itemprop="url" href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a></li>
    <?endforeach?>
</ul>
<?endif?>


