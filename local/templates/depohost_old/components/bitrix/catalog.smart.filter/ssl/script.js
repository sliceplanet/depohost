$(document).ready(function() {
  $(".smartfilter").on("change", function() {
    $(".ssl__table_row").hide();
    var domain = $("[name=PLUS_DOMEN]:checked").val();
    var props = $("[type=checkbox]:checked");
    if (props.length) {
      $.each(props, function() {
        var value = $(this).val();
        if (domain === "clean_all") {
          $("[data-filter*=" + value + "]").show();
        } else {
          $("[data-filter*=" + value + "][data-domain=" + domain + "]").show();
        }
      });
    } else {
      if (domain === "clean_all") {
        $(".ssl__table_row").show();
      } else {
        $("[data-domain=" + domain + "]").show();
      }
    }
  });
});
