<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<div class="container">
  <div class="microsoft_exchange__wrapper">
    <div class="head"><?= $arResult['NAME'] ?></div>
    <? foreach ($arResult["ITEMS"] as $arElement): ?>
      <?
      $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
      ?>
      <div class="table__blue">
        <div class="table_row">
          <div class="table_cell">
            <div><?= $arElement["NAME"] ?></div>
            <div><?= $arElement["PREVIEW_TEXT"] ?></div>
          </div>
          <div class="table_cell">&nbsp;</div>
          <div class="table_cell">&nbsp;</div>
          <div class="table_cell">&nbsp;</div>
        </div>
        <div class="table_row" id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
          <div class="table_cell">
            <label>Имя домена <span class="star_required">*</span>
              <input name="DOM_NAME" id="DOM_NAME" type="text" value=""
                     placeholder="<?= GetMessage('CT_CS_PLACEHOLEDER_DOM_NAME') ?>">
            </label>
          </div>
          <div class="table_cell">
            <? $arPrice = reset($arElement["PRICES"]); ?>
            <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
              <s><?= $arPrice["PRINT_VALUE"] ?></s><br/><span
                  class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
            <? else: ?>
              <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
            <? endif ?>
          </div>
          <div class="table_cell">
            <div class="item_buttons_counter_block elements-row">
              <input type="text" class="microsoft_exchange__quntity" id="quantity<?= $arElement['ID'] ?>m"
                     name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="1">
              <a id="plus<?= $arElement['ID'] ?>m" href="javascript:void(0)" class="bx_bt_white bx_small plus"
                 rel="nofollow">+</a>
              <a id="minus<?= $arElement['ID'] ?>m" href="javascript:void(0)" class="bx_bt_white bx_small minus"
                 rel="nofollow">-</a>
            </div>
            <script type="text/javascript">
              $(document).ready(function () {
                $('#minus<?= $arElement['ID'] ?>m').click(function () {
                  var $input = $(this).parent().find('input#quantity<?= $arElement['ID'] ?>m');
                  var count = parseInt($input.val()) - 1;
                  count = count < 1 ? 1 : count;
                  $input.val(count);
                  $input.change();
                  return false;
                });
                $('#plus<?= $arElement['ID'] ?>m').click(function () {
                  var $input = $(this).parent().find('input#quantity<?= $arElement['ID'] ?>m');
                  $input.val(parseInt($input.val()) + 1);
                  $input.change();
                  return false;
                });

                $("#quantity<?= $arElement['ID'] ?>m").change(function () {
                  $("#btn<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + $("#quantity<?= $arElement['ID'] ?>m").val() + "&DOM_NAME=" + $("#DOM_NAME").val());
                });
                $("#DOM_NAME").on('keyup', '', function (event) {
                  $("#btn<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + $("#quantity<?= $arElement['ID'] ?>m").val() + "&DOM_NAME=" + $("#DOM_NAME").val());
                });

                $('#btn<?= $arElement['ID'] ?>').click(function () {
                  var val = $("#DOM_NAME").val(),
                    expr = new RegExp('^([а-яА-ЯёЁa-zA-Z0-9]([а-яА-ЯёЁa-zA-Z0-9\\-]{0,61}[а-яА-ЯёЁa-zA-Z0-9])?\\.)+[а-яА-ЯёЁa-zA-Z]{2,6}$', 'ig'),
                    validDomain = expr.test(val);

                  if (val == '') {
                    alert('<?= Loc::getMessage('DOMAIN_NAME_EMPTY') ?>');
                    return false;
                  }
                  else if (!validDomain) {
                    alert('<?= Loc::getMessage('DOMAIN_NAME_NOT_VALID') ?>');
                    return false;
                  }
                });
              });
            </script>
          </div>
          <div class="table_cell order">
            <a href="<?= '/personal/cart/ajax-add.php?' . urlencode('action=BUY&id[]=' . $arElement['ID'] . '&quantity[]=1'); ?>"
               rel="nofollow" class="btn" id="btn<?= $arElement['ID'] ?>"><? echo GetMessage("CATALOG_BUY") ?></a>
          </div>
        </div>
      </div>
    <? endforeach; ?>
  </div>
</div>