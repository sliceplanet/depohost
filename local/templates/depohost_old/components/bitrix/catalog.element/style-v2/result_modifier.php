<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!$arResult['MODULES']['catalog'])
{
    return;
}
$arResult['TYPE'] = $arResult['IBLOCK_CODE'];

$arResult['MIN_PRICE']['PRINT_VALUE'] = number_format($arResult['MIN_PRICE']['VALUE'], 0, '.',
        ' ') . ' <span class="rub"></span>';
$arResult['BASKET_PROPS'] = array();
foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty)
{
    $arResult['BASKET_PROPS'][$arProperty['CODE']] = array(
        'INPUT_HTML' => '<input type="hidden" name="PROPS['.$arProperty['CODE'].'][VALUE]" value="' . $arProperty['DISPLAY_VALUE'] . '"/><input type="hidden" name="PROPS['.$arProperty['CODE'].'][NAME]" value="' . $arProperty['NAME'] . '"/>',
        'HIDDEN' => true
    );
}

$quantityMonths = intval($_REQUEST['months']) > 0 ? intval($_REQUEST['months']) : 1;

$arMonths = array(1, 3, 6, 12);
$htmlMonthsSelect = '<select class="select_kam small period" name="months">';
foreach ($arMonths as $month)
{
    $htmlMonthsSelect .= '<option value="' . $month . '"' . ($quantityMonths == $month ? ' selected="selected"'
            : '') . '>' . $month . ' ' . CUsefull::endOfWord($month, array('месяцев', 'месяц', 'месяца')) . '</option>';
}
$htmlMonthsSelect .= '</select>';
$arResult['BASKET_PROPS'][] = array('INPUT_HTML' => $htmlMonthsSelect);

$arResult['SUM']['VALUE'] = $arResult['MIN_PRICE']['VALUE'] * $quantityMonths;
$arResult['SUM']['PRINT_VALUE'] = number_format($arResult['SUM']['VALUE'], 0, '.', ' ') . ' <span class="rub"></span>';

$arResult['DISPLAY_PROPERTIES']['CONFIGURATION']['DISPLAY_VALUE'] = str_replace('/', ', ',
    $arResult['DISPLAY_PROPERTIES']['CONFIGURATION']['DISPLAY_VALUE']);
$arResult['ACTION_URL'] = SITE_DIR . 'personal/cart/ajax-add.php';
$arResult['CATALOG_MEASURE_NAME'] = 'в месяц';
$arResult['CATALOG_MEASURE_SHORT_NAME'] = '1 мес';

?>