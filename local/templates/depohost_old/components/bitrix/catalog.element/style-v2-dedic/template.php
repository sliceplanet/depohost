<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
  'ID' => $strMainID,
  'PICT' => $strMainID . '_pict',
  'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
  'STICKER_ID' => $strMainID . '_stricker',
  'BIG_SLIDER_ID' => $strMainID . '_big_slider',
  'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
  'SLIDER_LIST' => $strMainID . '_slider_list',
  'SLIDER_LEFT' => $strMainID . '_slider_left',
  'SLIDER_RIGHT' => $strMainID . '_slider_right',
  'OLD_PRICE' => $strMainID . '_old_price',
  'PRICE' => $strMainID . '_price',
  'DISCOUNT_PRICE' => $strMainID . '_price_discount',
  'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
  'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
  'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
  'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
  'QUANTITY' => $strMainID . '_quantity',
  'QUANTITY_DOWN' => $strMainID . '_quant_down',
  'QUANTITY_UP' => $strMainID . '_quant_up',
  'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
  'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
  'BUY_LINK' => $strMainID . '_buy_link',
  'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
  'COMPARE_LINK' => $strMainID . '_compare_link',
  'PROP' => $strMainID . '_prop_',
  'PROP_DIV' => $strMainID . '_skudiv',
  'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
  'OFFER_GROUP' => $strMainID . '_set_group_',
  'ZOOM_DIV' => $strMainID . '_zoom_cont',
  'ZOOM_PICT' => $strMainID . '_zoom_pict'
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);

?>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "<?= $arResult['NAME'] ?>",
  <?if(is_array($arResult['PREVIEW_PICTURE'])):?>
  "image": [
    "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult['PREVIEW_PICTURE']['SRC']?>"
   ],
   <?endif?>
  "description": "<?=strip_tags($arResult['PREVIEW_TEXT'])?>",
  "mpn": "<?= $arResult['ID'] ?>",
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RUB",
    "price": "<?= $arResult['MIN_PRICE']['VALUE'] ?>",
    "itemCondition": "http://schema.org/UsedCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "Artextelecom"
    }
  }
}
</script>
<div id="<? echo $arItemIDs['ID']; ?>">
  <div class="name">
    <? echo $arResult['NAME']; ?>
  </div>

  <div class="top">
    <div class="configuration"
         title="<?= $arResult['DISPLAY_PROPERTIES']['CONFIGURATION']['DISPLAY_VALUE']; ?>"><?= $arResult['DISPLAY_PROPERTIES']['CONFIGURATION']['DISPLAY_VALUE']; ?></div>
    <div class="price-wrapper">
      <div class="price">
        <div class="value"><?= $arResult['MIN_PRICE']['PRINT_VALUE'] ?></div>
      </div>
      <div class="measure"><?= $arResult['CATALOG_MEASURE_NAME'] ?></div>
    </div>
  </div>
  <div class="bottom">
    <? if (strlen($arResult['PREVIEW_TEXT']) > 0): ?>
      <p class="preview-text">
        <?= $arResult['PREVIEW_TEXT'] ?>
      </p>
    <? endif ?>
    <form class="order-side clearFix" action="<?= $arResult['ACTION_URL'] ?>" method="get">
      <strong class="caption"><?= GetMessage('CMP_CATALOG_ELEMENT_CAPTION_PRICE') ?></strong>
      <div class="n-price-wrap">
        <? foreach ($arResult['BASKET_PROPS'] as $arProp): ?>
          <? if ($arProp['HIDDEN']):
            echo $arProp['INPUT_HTML'];
          else:?>
            <div class="field"><?= $arProp['INPUT_HTML'] ?></div>
          <? endif; ?>
        <? endforeach ?>
        <div>
          <div class="sum-wrapper">
            <div id="sum"><?= $arResult['SUM']['PRINT_VALUE'] ?></div>
            <div class="measure-price"><?= $arResult['CATALOG_MEASURE_SHORT_NAME'] ?>
              = <span id="price"><?= $arResult['MIN_PRICE']['PRINT_VALUE'] ?></span></div>
          </div>
        </div>
      </div>
      <input type="hidden" name="solutionId" value="<?= $arResult['ID'] ?>"/>
      <input type="hidden" name="step" value="2"/>
      <input type="hidden" name="type" value="<?= $arResult['TYPE'] ?>"/>

      <div class="btn-wrapper">
        <button type="submit"
                class="btn-green sm"><?= GetMessage('CMP_CATALOG_ELEMENT_BUY_BUTTON') ?></button>
      </div>

    </form>
  </div>
  <? if (strlen($arResult['DETAIL_TEXT']) > 0): ?>
    <div class="detail-text">
      <h2 class="caption-text"><?= GetMessage('CMP_CATALOG_ELEMENT_CAPTION_DETAIL_TEXT') ?></h2>
      <?= $arResult['DETAIL_TEXT'] ?>
    </div>
  <? endif ?>
</div>
<script>
  var arDiscounts = <?=CUtil::PhpToJSObject($arResult['DEDICATED']['PRICES'])?>;
</script>
