<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Itin\Depohost\Clients;


global $USER;
if ($USER->IsAuthorized())
{
    Loader::includeModule('clients');
    $client = new Clients($USER->GetID());

    $person_id = $client->getPersonTypeId();
    $arResult["USER_VALS"]["PERSON_TYPE_ID"] = $person_id;

    $arResult["USER_VALS"]['PROFILE_ID'] = $client->getProfileId(array('user_id'=>$USER->GetID()));
    $arResult["USER_VALS"]['PROFILE_CHANGE'] = 'Y';
    $arUserResult = $arResult["USER_VALS"];
    $arFilter = array("PERSON_TYPE_ID" => $person_id, "ACTIVE" => "Y", "UTIL" => "N", "RELATED" => false);
    if (!empty($arParams["PROP_" . $person_id]))
        $arFilter["!ID"] = $arParams["PROP_" . $person_id];

    $dbProperties = CSaleOrderProps::GetList(
                    array(
                "GROUP_SORT" => "ASC",
                "PROPS_GROUP_ID" => "ASC",
                "USER_PROPS" => "ASC",
                "SORT" => "ASC",
                "NAME" => "ASC"
                    ), $arFilter, false, false, array(
                "ID", "NAME", "TYPE", "REQUIED", "DEFAULT_VALUE", "IS_LOCATION", "PROPS_GROUP_ID", "SIZE1", "SIZE2", "DESCRIPTION",
                "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "DELIVERY_ID", "PAYSYSTEM_ID", "MULTIPLE",
                "CODE", "GROUP_NAME", "GROUP_SORT", "SORT", "USER_PROPS", "IS_ZIP", "INPUT_FIELD_LOCATION"
                    )
    );
    $arResult["ORDER_PROP"]["USER_PROPS_Y"] = array();
    $arResult["ORDER_PROP"]["USER_PROPS_N"] = array();

    while ($arProperties = $dbProperties->GetNext())
    {
        $arProperties = getOrderPropFormated($arProperties, $arResult, $arUserResult, $arDeleteFieldLocation);

        if ($arProperties["USER_PROPS"] == "Y")
            $arResult["ORDER_PROP"]["USER_PROPS_Y"][$arProperties["ID"]] = $arProperties;
        else
            $arResult["ORDER_PROP"]["USER_PROPS_N"][$arProperties["ID"]] = $arProperties;

        $arResult["ORDER_PROP"]["PRINT"][$arProperties["ID"]] = Array("ID" => $arProperties["ID"], "NAME" => $arProperties["NAME"], "VALUE" => $arProperties["VALUE_FORMATED"], "SHOW_GROUP_NAME" => $arProperties["SHOW_GROUP_NAME"]);
    }
}
?>