 <div class="menu-wrap">
  <div class="menu-column w23">
   <div class="menu-block pb">
    <div class="menu-title"><a href="/arenda-dedicated_server/">Выделенные серверы</a></div>

    <ul class="menu-list">
     <li class="menu-item">
      <a href="/arenda-dedicated_server/" class="menu-link">
       Собрать свой сервер
      </a>
     </li>
     <li class="menu-item">
      <a href="/arenda-dedicated_server/#ready-made" class="menu-link">
       Готовые решения
      </a>
     </li>
    </ul>
   </div>
   <div class="menu-block">
    <div class="menu-title"><a href="/1c-server/">Аренда серверов для 1С</a></div>

    <ul class="menu-list">
     <li class="menu-item">
      <a href="/1c-server/" class="menu-link">
       Виртуальный сервер<br/> для 1С


      </a>
     </li>
     <li class="menu-item">
      <a href="/1c-server/" class="menu-link">
       Выделенный сервер<br/> для 1С

      </a>
     </li>
    </ul>
   </div>
  </div>
  <div class="menu-column w23">
   <div class="menu-block pb">
    <div class="menu-title"><a href="/arenda-virtualnogo-vydelennogo-servera-vds/">Виртуальные серверы</a></div>

    <ul class="menu-list">
     <li class="menu-item">
      <a href="/arenda-virtualnogo-vydelennogo-servera-vds/" class="menu-link">
       Собрать свой сервер
      </a>
     </li>
     <li class="menu-item">
      <a href="/arenda-virtualnogo-vydelennogo-servera-vds-gotovie-resheniya/" class="menu-link">
       Готовые решения
      </a>
     </li>
    </ul>
   </div>
   <div class="menu-block">
    <div class="menu-title"><a href="/microsoft/">Аренда ПО Microsoft</a></div>

    <ul class="menu-list">
     <li class="menu-item">
      <a href="/microsoft/" class="menu-link">
       Аренда Сервера Windows
      </a>
     </li>
     <li class="menu-item">
      <a href="/microsoft/" class="menu-link">
       Аренда SQL
      </a>
     </li>

    </ul>
   </div>
  </div>
  <div class="menu-column w16">
   <div class="menu-block">
    <div class="menu-title"><a href="/ssl-sertifikat/">SSL сертификат</a></div>

    <ul class="menu-list">
     <li class="menu-item">
      <a href="/ssl-sertifikat/" class="menu-link">
       Сравнение SSL<br/> сертификатов
      </a>
     </li>
     <li class="menu-item">
      <a href="/ssl-sertifikat/comodo/" class="menu-link">
       Comodo SSL
      </a>
     </li>
     <li class="menu-item">
      <a href="/ssl-sertifikat/thawte/" class="menu-link">
       Thawte SSL
      </a>
     </li>
     <li class="menu-item">
      <a href="/ssl-sertifikat/symantec/" class="menu-link">
       Symantec SSL
      </a>
     </li>
     <li class="menu-item">
      <a href="/ssl-sertifikat/true-business-id/" class="menu-link">
       True BusinessID
      </a>
     </li>
     <li class="menu-item">
      <a href="/ssl-sertifikat/wildcard-ssl/" class="menu-link">
       Wildcard SSL
      </a>
     </li>
    </ul>
   </div>
  </div>
  <div class="menu-column w38">
   <div class="menu-block">
    <div class="menu-title">Дополнительные услуги</div>
    <div class="menu-block-inner-wrap">
     <div class="menu-block-inner">


      <ul class="menu-list">
       <li class="menu-item">
        <a href="/domain/" class="menu-link">
         Регистрация<br/> доменов
        </a>
       </li>
       <li class="menu-item">
        <a href="/ispmanager/" class="menu-link">
         Аренда панели<br/>
         ISP manager
        </a>
       </li>
	   <?/*
       <li class="menu-item">
        <a href="/microsoft-exchange/" class="menu-link">
         Аренда почтового<br/> сервера MS<br/> Exchange
        </a>
       </li>
	   */?>
       <li class="menu-item">
        <a href="/arenda-cisco/" class="menu-link">
         Аренда сетевого<br/> оборудования
        </a>
       </li>
       <li class="menu-item">
        <a href="/hosting-dns/" class="menu-link">
         Хостинг DNS
        </a>
       </li>
       <li class="menu-item">
        <a href="/unix-hosting/" class="menu-link">
         Unix хостинг
        </a>
       </li>
      </ul>
     </div>
     <div class="menu-block-inner">
      <ul class="menu-list">
       <li class="menu-item">
        <a href="/monitoring/" class="menu-link">
         Мониторинг<br/> серверов и сервисов
        </a>
       </li>
       <li class="menu-item">
        <a href="/administrirovanie/" class="menu-link">
         Администрирование<br/>
         серверов и сервисов
        </a>
       </li>
       <li class="menu-item">
        <a href="/trafik/" class="menu-link">
         Стоимость трафика
        </a>
       </li>
       <li class="menu-item">
        <a href="/free-hosting/" class="menu-link">
         Бесплатно вы<br/> получаете
        </a>
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
