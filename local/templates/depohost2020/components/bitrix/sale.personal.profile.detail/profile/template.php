<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->IncludeComponent("bitrix:menu", "personal", array(
	"ROOT_MENU_TYPE" => "personal",
	"MENU_THEME" => "site",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "2",
	"CHILD_MENU_TYPE" => "childpersonal",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
<div class="content">
<?if(strlen($arResult["ID"])>0):?>
	<table class="sale_personal_profile_detail data-table">
		<?
		$i = 0;
		foreach($arResult["ORDER_PROPS"] as $val)
		{
			if(!empty($val["PROPS"]))
			{
				?>
				<tr>
					<th colspan="2"><b><?=$val["NAME"];?></b></th>
				</tr>
				<?if($i == 0):?>
				    <tr>
					    <td width="40%" align="right"><?echo GetMessage("SALE_PNAME")?>:</td>
					    <td width="60%"><?echo $arResult["NAME"];?></td>
				    </tr>
				<?endif; $i=1;
				foreach($val["PROPS"] as $vval)
				{
					$currentValue = $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_".$vval["ID"]];
					$name = "ORDER_PROP_".$vval["ID"];
					?>
					<tr>
						<td width="50%" align="right"><?=$vval["NAME"] ?>:</td>
						<td width="50%">
							<?if ($vval["TYPE"]=="CHECKBOX"):?>
								<input type="hidden" name="<?=$name?>" value="">
								<input type="checkbox" name="<?=$name?>" value="Y"<?if ($currentValue=="Y" || !isset($currentValue) && $vval["DEFAULT_VALUE"]=="Y") echo " checked";?>>
							<?elseif ($vval["TYPE"]=="TEXT"):?>
								<?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?>
							<?/*?>
							<?elseif ($vval["TYPE"]=="SELECT"):?>
								<select name="<?=$name?>" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:1; ?>">
									<?foreach($vval["VALUES"] as $vvval):?>
										<option value="<?echo $vvval["VALUE"]?>"<?if ($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) echo " selected"?>><?echo $vvval["NAME"]?></option>
									<?endforeach;?>
								</select>
							<?*/?>
							<?elseif ($vval["TYPE"]=="SELECT"):?>
									<?foreach($vval["VALUES"] as $vvval):?>
										<?if ($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) echo $vvval["NAME"]?>
									<?endforeach;?>
							<?elseif ($vval["TYPE"]=="MULTISELECT"):?>
								<select multiple name="<?=$name?>[]" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:5; ?>">
									<?
									$arCurVal = array();
									$arCurVal = explode(",", $currentValue);
									for ($i = 0; $i<count($arCurVal); $i++)
										$arCurVal[$i] = Trim($arCurVal[$i]);
									$arDefVal = explode(",", $vval["DEFAULT_VALUE"]);
									for ($i = 0; $i<count($arDefVal); $i++)
										$arDefVal[$i] = Trim($arDefVal[$i]);
									foreach($vval["VALUES"] as $vvval):?>
										<option value="<?echo $vvval["VALUE"]?>"<?if (in_array($vvval["VALUE"], $arCurVal) || !isset($currentValue) && in_array($vvval["VALUE"], $arDefVal)) echo" selected"?>><?echo $vvval["NAME"]?></option>
									<?endforeach;?>
								</select>
							<?elseif ($vval["TYPE"]=="TEXTAREA"):?>
								<?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?>
							<?elseif ($vval["TYPE"]=="LOCATION"):?>
							    <?foreach($vval["VALUES"] as $vvval):?>
								<?if (IntVal($vvval["ID"])==IntVal($currentValue) || !isset($currentValue) && IntVal($vvval["ID"])==IntVal($vval["DEFAULT_VALUE"])) echo $vvval["COUNTRY_NAME"]." - ".$vvval["CITY_NAME"]?>
							    <?endforeach;?>
							    <?/*
								<?if ($arParams['USE_AJAX_LOCATIONS'] == 'Y'):
									$APPLICATION->IncludeComponent('bitrix:sale.ajax.locations', '', array(
											"AJAX_CALL" => "N", 
											'CITY_OUT_LOCATION' => 'Y',
											'COUNTRY_INPUT_NAME' => $name.'_COUNTRY',
											'CITY_INPUT_NAME' => $name,
											'LOCATION_VALUE' => isset($currentValue) ? $currentValue : $vval["DEFAULT_VALUE"],
										),
										null,
										array('HIDE_ICONS' => 'Y')
									);
								else:
								?>
								<select name="<?=$name?>" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:1; ?>">
									<?foreach($vval["VALUES"] as $vvval):?>
										<option value="<?echo $vvval["ID"]?>"<?if (IntVal($vvval["ID"])==IntVal($currentValue) || !isset($currentValue) && IntVal($vvval["ID"])==IntVal($vval["DEFAULT_VALUE"])) echo " selected"?>><?echo $vvval["COUNTRY_NAME"]." - ".$vvval["CITY_NAME"]?></option>
									<?endforeach;?>
								</select>
								<?
								endif;
								?>
							    */?>
							<?elseif ($vval["TYPE"]=="RADIO"):?>
								<?foreach($vval["VALUES"] as $vvval):?>
									<input type="radio" name="<?=$name?>" value="<?echo $vvval["VALUE"]?>"<?if ($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) echo " checked"?>><?echo $vvval["NAME"]?><br />
								<?endforeach;?>
							<?endif?>

							<?if (strlen($vval["DESCRIPTION"])>0):?>
								<br /><small><?echo $vval["DESCRIPTION"] ?></small>
							<?endif?>
						</td>
					</tr>
					<?
				}
			}
		}
		?>

	</table>
<?else:?>
	<?=ShowError($arResult["ERROR_MESSAGE"]);?>
<?endif;?>
</div>