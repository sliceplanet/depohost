<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите номер договора.<br />Пароль будет выслан на Ваш E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = "Выслать пароль";
$MESS ['AUTH_SEND'] = "Выслать";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS ['AUTH_LOGIN'] = "Номер договора:";
$MESS ['AUTH_OR'] = "или";
$MESS ['AUTH_EMAIL'] = "E-Mail:";
$MESS ['AUTH_TITLE'] = "Забыли свой пароль?";
$MESS ['AUTH_TITLE_BLUE'] = "Восстановление";
$MESS ['AUTH_RESULT_OK'] = "Письмо с Вашим паролем отправлено на Ваш контактный E-Mail адрес указанный при регистрации";
$MESS ['AUTH_RESULT_ERROR'] = "Договор не найден";

?>