<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="n-page-wrapper">
  <div class="aut-l"></div>
  <div class="aut-r"></div>
  <div class="container n-auth">
    <div class="row">
      <div class="col-xs-12">
        <section class="block">
          <div class="block-txt">

            <h1 class="h1">Авторизация<span><? $APPLICATION->ShowTitle(false) ?></span></h1>
            <p>Укажите Ваш номер договора и пароль</p>
            <?
            ShowMessage($arParams["~AUTH_RESULT"]);
            ShowMessage($arResult['ERROR_MESSAGE']);
            ?>
            <form name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
              <input type="hidden" name="AUTH_FORM" value="Y">
              <input type="hidden" name="TYPE" value="AUTH">
              <? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
              <? endif ?>
              <? foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
              <? endforeach ?>

              <table class="bx-auth-table">
                <tbody>
                <tr class="bx-auth__wrap">
                  <td class="bx-auth-label">Номер договора:</td>
                  <td><input class="bx-auth-input" type="text" name="USER_LOGIN" maxlength="255"
                             value="<?= $arResult["LAST_LOGIN"] ?>"></td>
                </tr>
                <tr class="bx-auth__wrap">
                  <td class="bx-auth-label">Пароль:</td>
                  <td><input class="bx-auth-input" type="password" name="USER_PASSWORD" maxlength="255">
                  </td>
                </tr>
                <tr class="bx-auth__wrap">
                  <td></td>
                  <td class="authorize-submit-cell"><input type="submit" name="Login" value="Войти">
                    <div class="forgot-password"><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow">Забыли
                        свой
                        пароль?</a></div>
                  </td>
                </tr>
                </tbody>
              </table>
            </form>
          </div>
          <div class="block-pm auth">
            <div class="block-pm-ttl">Вы находитесь на странице входа в Личный кабинет</div>
            <div class="block-pm-item">В Личном кабинете вы можете
              полноценно управлять своими услугами.
            </div>
            <div class="block-pm-item"> Обращаться в финансовую
              службу.
            </div>
            <div class="block-pm-item"> Получать доступ к
              закрывающим документам.
            </div>
            <div class="block-pm-item"> Получить доступ к договору и
              приложения к нему на оказываемые услуги.
            </div>

            <div class="block-pm-item"> Обращаться в техническую
              поддержку.
            </div>
            <div class="block-pm-item"> Получить доступ в панель
              управления DNS записями вашего домена.
            </div>
            <div class="block-pm-item"> Получить доступ к FTP
              серверу прикрепленного к вашему аккаунту.
            </div>
            <div class="block-pm-item"> Управлять своими данными и
              многое другое.
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  <?if (strlen($arResult["LAST_LOGIN"])>0):?>
  try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
  <?else:?>
  try{document.form_auth.USER_LOGIN.focus();}catch(e){}
  <?endif?>
</script>
