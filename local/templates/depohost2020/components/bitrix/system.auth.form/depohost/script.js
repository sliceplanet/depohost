/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 15.09.18.
 */
var logButton = document.querySelector(".header__lk_enter.logged");
if (logButton) {
  logButton.addEventListener("click", function(evt) {
    logButton.classList.toggle("active");
  });
  logButton.addEventListener("mouseleave", function(evt) {
    evt.preventDefault();
    logButton.classList.remove("active");
  });
}
