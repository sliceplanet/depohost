<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
    //check if user is client
    if (in_array(11, $USER->GetUserGroupArray()))
    {
        ?>
      <div class="header__lk_enter logged">
        <div class="private-cab__ttl">Договор №<?= $arResult["USER_LOGIN"] ?></div>
        <div class="private-cab__set"></div>
        <div class="private-cab__login-menu">
          <a class="private-cab_link" href="/personal/">Личный кабинет</a>
          <a href="<?=$APPLICATION->GetCurPageParam('logout=yes',array('logout'))?>"><?= GetMessage("AUTH_LOGOUT_BUTTON") ?></a><br>
        </div>
      </div>
        <?
    }else
    {
        ?>
      <a href="<?=$arParams['REGISTER_URL']?>" class="private-cab__link">Войти</a>

        <?
    }
?>
 