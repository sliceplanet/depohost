<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
if ($USER->IsAuthorized())
{
  ?>
  <input type="hidden" name="PERSON_TYPE_OLD" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
  <input type="hidden" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
  <?
}
else
{
  if (count($arResult["PERSON_TYPE"]) > 1)
  {
    ?>
    <div class="type_person">
      <div class="type_person__label"><?= GetMessage("SOA_TEMPL_PERSON_TYPE") ?></div>
      <div class="type_person__values">
        <? foreach ($arResult["PERSON_TYPE"] as $v): ?>
          <label class="type_person__values_label"
                                              >
            <input type="radio" name="PERSON_TYPE"
                                                      value="<?= $v["ID"] ?>"<? if ($v["CHECKED"] == "Y" && $_POST['PERSON_TYPE'] != "AUTH") {
              echo " checked=\"checked\"";
            } ?> onClick="submitForm()">
            <span class="type_person__values_label_text"><?= $v["NAME"] ?></span>
          </label>
        <? endforeach; ?>
        <label class="type_person__values_label">
          <input type="radio" name="PERSON_TYPE" value="AUTH"
                 onClick="submitForm()"<? if ($_POST['PERSON_TYPE'] == "AUTH") {
            echo " checked=\"checked\"";
          } ?> >
          <span class="type_person__values_label_text"><?= GetMessage('OLD_CLIENT') ?></span>
        </label>
        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
      </div>
    </div>
    <?
  }
  else
  {
    if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
    {
      //for IE 8, problems with input hidden after ajax
      ?>
      <span style="display:none;">
                <input type="text" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
                <input type="text" name="PERSON_TYPE_OLD"
                       value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
            </span>
      <?
    }
    else
    {
      foreach ($arResult["PERSON_TYPE"] as $v)
      {
        ?>
        <input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>"/>
        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>"/>
        <?
      }
    }
  }
}
?>