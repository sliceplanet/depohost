<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<?
if (!function_exists("showFilePropertyField"))
{

  function showFilePropertyField($name, $property_fields, $values, $max_file_size_show = 50000)
  {
    $res = "";

    if (!is_array($values) || empty($values))
    {
      $values = array(
        "n0" => 0,
      );
    }

    if ($property_fields["MULTIPLE"] == "N")
    {
      $res = "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
    }
    else
    {
      $res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

      $res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
      $res .= "<br/><br/>";
      $res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[1]\" id=\"" . $name . "[1]\" onChange=\"javascript:addControl(this);\"></label>";
    }

    return $res;
  }

}

if (!function_exists("PrintPropsForm"))
{

  function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $personType = 0)
  {
    if (!empty($arSource))
    {
      ?>
	  <input class="sb_PERSON_TYPE_ID" name="PERSON_TYPE_ID" value="<?=$personType?>" type="hidden" />
      <div>
        <?
        foreach ($arSource as $arProperties)
        {
		  if ($arProperties['CODE'] == 'NAME' && $personType == 1)
          {
			$arProperties["REQUIED"] = 'N';
		  ?>
			<div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?> hidden">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <input class="sb_<?=$arProperties['CODE']?>" type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>"
                       value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>">

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
		  <?
		  }
		  elseif ($arProperties['CODE'] == 'CONTACT_PERSON' && $personType == 3)
          {
			$arProperties["REQUIED"] = 'N';
		  ?>
			<div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?> hidden">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <input class="sb_<?=$arProperties['CODE']?>" type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>"
                       value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>">

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
		  <?
		  }
		  elseif ($arProperties['CODE'] == 'CONTACT_NAME' && $personType == 2)
          {
			$arProperties["REQUIED"] = 'N';
		  ?>
			<div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?> hidden">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <input class="sb_<?=$arProperties['CODE']?>" type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>"
                       value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>">

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
		  <?
		  } 
          elseif ($arProperties['CODE'] == 'DOVERENNOST')
          {
            ?>
            <div class="row prop__type_<?=strtolower($arProperties["TYPE"])?>" id="doverennost">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <span class="bx_sof_req">*</span>
              </div>

              <div class="col-sm-7 prop__field_input">
                <input type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>"
                       value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>">

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties['CODE'] == 'OSNOVANIE')
          {
          ?>
            <div id="osnovanie" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <select name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                        size="<?= $arProperties["SIZE1"] ?>" onchange="toggleDoverennost(this);">
                  <?
                  foreach ($arProperties["VARIANTS"] as $arVariants):
                    ?>
                    <option
                        value="<?= $arVariants["VALUE"] ?>"<? if ($arVariants["SELECTED"] == "Y")
                    {
                      echo " selected";
                    } ?>><?= $arVariants["NAME"] ?></option>
                  <?
                  endforeach;
                  ?>
                </select>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
            <script>
              function toggleDoverennost(select_osnovanie) {
                var val = select_osnovanie.value;
                var doverennost = document.getElementById('doverennost');
                if (val == 20) {//variant is doverennost
                  doverennost.style.display = 'block';
                }
                else {
                  doverennost.style.display = 'none';
                }

              }

              window.onload = function () {
                var select_osnovanie = document.getElementById('ORDER_PROP_20');
                toggleDoverennost(select_osnovanie);
              };
            </script>
          <?
          }
          elseif ($arProperties["TYPE"] == "CHECKBOX")
          {
          ?>
          <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="">
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>
              <div class="col-sm-7 prop__field_input">
                <input type="checkbox" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>"
                       value="Y"<? if ($arProperties["CHECKED"] == "Y")
                {
                  echo " checked";
                } ?>>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "TEXT")
          {
			if ($arProperties['CODE'] == 'L_NAME' || $arProperties['CODE'] == 'F_NAME' || $arProperties['CODE'] == 'P_NAME') {
				$sbName = true;
			} else {
				$sbName = false;
			}
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <input type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>"
                       value="<?= $arProperties["VALUE"] ?>" name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>" 
                       <?if($sbName) {?>
						class="sb_<?=$arProperties['CODE']?>" 
						onkeyup="sbName();"
                       <?}?>
					   
					   >

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "SELECT")
          {
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <select name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                        size="<?= $arProperties["SIZE1"] ?>">
                  <?
                  foreach ($arProperties["VARIANTS"] as $arVariants):
                    ?>
                    <option
                        value="<?= $arVariants["VALUE"] ?>"<? if ($arVariants["SELECTED"] == "Y")
                    {
                      echo " selected";
                    } ?>><?= $arVariants["NAME"] ?></option>
                  <?
                  endforeach;
                  ?>
                </select>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "MULTISELECT")
          {
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <select multiple name="<?= $arProperties["FIELD_NAME"] ?>"
                        id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>">
                  <?
                  foreach ($arProperties["VARIANTS"] as $arVariants):
                    ?>
                    <option
                        value="<?= $arVariants["VALUE"] ?>"<? if ($arVariants["SELECTED"] == "Y")
                    {
                      echo " selected";
                    } ?>><?= $arVariants["NAME"] ?></option>
                  <?
                  endforeach;
                  ?>
                </select>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "TEXTAREA")
          {
          $rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                            <textarea rows="<?= $rows ?>" cols="<?= $arProperties["SIZE1"] ?>"
                                      name="<?= $arProperties["FIELD_NAME"] ?>"
                                      id="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["VALUE"] ?></textarea>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "LOCATION")
          {
          $value = 0;
          if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
          {
            foreach ($arProperties["VARIANTS"] as $arVariant)
            {
              if ($arVariant["SELECTED"] == "Y")
              {
                $value = $arVariant["ID"];
                break;
              }
            }
          }
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <?
                $GLOBALS["APPLICATION"]->IncludeComponent(
                  "bitrix:sale.ajax.locations", $locationTemplate, array(
                  "AJAX_CALL" => "N",
                  "COUNTRY_INPUT_NAME" => "COUNTRY",
                  "REGION_INPUT_NAME" => "REGION",
                  "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                  "CITY_OUT_LOCATION" => "Y",
                  "LOCATION_VALUE" => $value,
                  "ORDER_PROPS_ID" => $arProperties["ID"],
                  "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y")
                    ? "submitForm()" : "",
                  "SIZE1" => $arProperties["SIZE1"],
                ), null, array('HIDE_ICONS' => 'Y')
                );
                ?>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "RADIO")
          {
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <?
                if (is_array($arProperties["VARIANTS"]))
                {
                  foreach ($arProperties["VARIANTS"] as $arVariants):
                    ?>
                    <input
                        type="radio"
                        name="<?= $arProperties["FIELD_NAME"] ?>"
                        id="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"
                        value="<?= $arVariants["VALUE"] ?>" <? if ($arVariants["CHECKED"] == "Y")
                    {
                      echo " checked";
                    } ?> />

                    <label
                        for="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"><?= $arVariants["NAME"] ?></label></br>
                  <?
                  endforeach;
                }
                ?>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
          <?
          }
          elseif ($arProperties["TYPE"] == "FILE")
          {
          ?>
            <div id="<?=strtolower($arProperties['CODE'])?>" class="row prop__type_<?=strtolower($arProperties["TYPE"])?>">
              <div class="col-sm-5 prop__field_name">
                <?= $arProperties["NAME"] ?>
                <? if ($arProperties["REQUIED_FORMATED"] == "Y"): ?>
                  <span class="bx_sof_req">*</span>
                <? endif; ?>
              </div>

              <div class="col-sm-7 prop__field_input">
                <?= showFilePropertyField("ORDER_PROP_" . $arProperties["ID"], $arProperties,
                  $arProperties["VALUE"], $arProperties["SIZE1"]) ?>

                <?
                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                  ?>
                  <div class="bx_description">
                    <?= $arProperties["DESCRIPTION"] ?>
                  </div>
                <?
                endif;
                ?>
              </div>
            </div>
            <?
          }
        }
        ?>
      </div>
      <?
    }
  }
}
?>