<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");


	$countries = [];
   $db_vars = CSaleLocation::GetList(
        array(
                "SORT" => "ASC",
                "COUNTRY_NAME_LANG" => "ASC",
                "CITY_NAME_LANG" => "ASC"
            ),
        array("LID" => LANGUAGE_ID,'CITY_NAME'=>false),
        false,
        false,
        array()
    );
   while ($vars = $db_vars->Fetch()):

		$countries[] = $vars;

   endwhile;	
   
   if($USER->IsAuthorized()):
		CModule::IncludeModule('sale');

		if($arResult['USER_VALS']['PERSON_TYPE_ID'] == 2):
			$loc_id = $arResult["ORDER_PROP"]["USER_PROPS_Y"][3]['VALUE'];
			$loc = CSaleLocation::GetByID($loc_id);
		elseif($arResult['USER_VALS']['PERSON_TYPE_ID'] == 1):
			$loc_id = $arResult["ORDER_PROP"]["USER_PROPS_Y"][2]['VALUE'];
			$loc = CSaleLocation::GetByID($loc_id);
		elseif($arResult['USER_VALS']['PERSON_TYPE_ID'] == 3):
			$loc_id = $arResult["ORDER_PROP"]["USER_PROPS_Y"][17]['VALUE'];
			$loc = CSaleLocation::GetByID($loc_id);
		endif;
   
		$loc_options = '';
	
		if($loc_id):
		
			$loc_options = '<option value="">Укажите город</option>';
			$db_vars = CSaleLocation::GetList(
				array(
						"SORT" => "ASC",
						"COUNTRY_NAME_LANG" => "ASC",
						"CITY_NAME_LANG" => "ASC"
					),
				array("LID" => LANGUAGE_ID,'!CITY_NAME'=>false,'COUNTRY_ID'=>$loc['COUNTRY_ID']),
				false,
				false,
				array()
			);
			while ($vars = $db_vars->Fetch()):
				$sel = '';
				if($vars['ID'] == $loc_id)
					$selected = ' selected';
				$loc_options .= '<option value="'.$vars['ID'].'"'.$selected.'>'.$vars['CITY_NAME'].'</option>';

			endwhile;
		endif;
   
   
   endif;
   
//echo var_dump($arResult["ORDER_PROP"]["USER_PROPS_Y"]);
?>

  <div>

    <!--h4><?= GetMessage("SOA_TEMPL_PROP_INFO") ?></h4-->
    <?
    $bHideProps = true;


    if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
      if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
        ?>
        <!--
                    <div class="bx_block r1x3">
            <?= GetMessage("SOA_TEMPL_PROP_CHOOSE") ?>
                    </div>
                    <div class="bx_block r3x1">
                            <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
                                    <option value="0"><?= GetMessage("SOA_TEMPL_PROP_NEW_PROFILE") ?></option>
            <?
        foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
        {
          ?>
                                                    <option value="<?= $arUserProfiles["ID"] ?>"<? if ($arUserProfiles["CHECKED"] == "Y")
        {
          echo " selected";
        } ?>><?= $arUserProfiles["NAME"] ?></option>
                <?
        }
        ?>
                            </select>
                            <div style="clear: both;"></div>
                    </div>
            -->
      <?
      else:
        ?>
        <div class="bx_block r1x3">
          <?= GetMessage("SOA_TEMPL_EXISTING_PROFILE") ?>
        </div>
        <div class="bx_block r3x1">
          <?
          if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1)
          {
            foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
            {
              echo "<strong>" . $arUserProfiles["NAME"] . "</strong>";
              ?>
              <input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?= $arUserProfiles["ID"] ?>"/>
              <?
            }
          }
          else
          {
            ?>
            <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
              <?
              foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
              {
                ?>
                <option
                    value="<?= $arUserProfiles["ID"] ?>"<? if ($arUserProfiles["CHECKED"] == "Y")
                {
                  echo " selected";
                } ?>><?= $arUserProfiles["NAME"] ?></option>
                <?
              }
              ?>
            </select>
            <?
          }
          ?>
          <div style="clear: both;"></div>
        </div>
      <?
      endif;
    else:
      $bHideProps = false;
    endif;
    ?>
  </div>
  <div class="props__list">
    <div class="props__label" style="display:none;">
      <?//= GetMessage("SOA_TEMPL_BUYER_INFO") ?>
      <?
      if (array_key_exists('ERROR', $arResult) && is_array($arResult['ERROR']) && !empty($arResult['ERROR']))
      {
        $bHideProps = false;
      }
      ?>
      <input type="hidden" name="showProps" id="showProps" value="N"/>
    </div>

    <div id="sale_order_props">
	
		<?if($arResult['USER_VALS']['PERSON_TYPE_ID'] == 2):?>

			<div class="page-props pt2">

				<div class="form-block">
					<div class="form-block__h">Информация о юридическом лице</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="label-control">Наименование организации <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_10" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][10]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group is-location-1">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"<?if($c['ID']==$loc['COUNTRY_ID']):?> selected<?endif;?>><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group is-location-2">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="ORDER_PROP_3" class="location_city">
										<?=$loc_options?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ФИО генерального директора <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_19" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][19]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Директор исполняет обязанности на основании</label>
									<select name="ORDER_PROP_20" id="ORDER_PROP_20"  onchange="toggleDoverennost(this);">
										<option value="10">Устава</option>
										<option value="20">Доверенности</option>
									</select>
								</div>
							</div>
              <div id="doverennost" class="col-md-offset-6 col-md-6" hidden>
								<div class="form-group">
									<label class="label-control">Информация о доверенности</label>
                  <input type="text" name="ORDER_PROP_44" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][44]['VALUE']?>">
                </div>
							</div>
							<div  class="col-md-6">
								<div class="form-group">
									<label class="label-control">Юридический адрес <span class="required">*</span></label>
									<textarea name="ORDER_PROP_8" class="form-control"><?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][8]['VALUE']?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="ORDER_PROP_14" class="form-control"><?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][14]['VALUE']?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_21" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][21]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="form-block">
					<div class="form-block__h">Коды организации</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ИНН <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_15" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][15]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">КПП <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_16" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][16]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Банковские реквизиты </div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Расчетный счет <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_24" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][24]['VALUE']?>">
								</div>
							</div>
							<?/*
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Корреспондентский счет <span class="required">*</span></label>
									<input type="text" name="" class="form-control">
								</div>
							</div>
							*/?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">БИК <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_26" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][26]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Наименование банка <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_23" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][23]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_11" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][11]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_9" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][9]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail</label>
									<input type="text" name="ORDER_PROP_27" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][27]['VALUE']?>">
								</div>
							</div>
							
							<div class="col-md-6 hidden">
								<div class="form-group">
									<label class="label-control">Контактная персона <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_13" class="form-control sb_CONTACT_NAME" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][13]['VALUE']?>">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Имя контактного лица <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_48" onkeyup="sbName();" class="form-control sb_F_NAME" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][48]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Фамилия контактного лица <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_49" onkeyup="sbName();" class="form-control sb_L_NAME" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][49]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Отчество контактного лица <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_50" onkeyup="sbName();" class="form-control sb_P_NAME" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][50]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="Y" id="agree"<? if (array_key_exists('agree', $_POST)) echo ' checked' ?>>
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на <a href="https://www.depohost.ru/doc/pl.pdf" target="_blank">обработку персональных данных</a>
									</label>
								</div>
							</div>
							<div class="col-md-12" style="text-align:center;">
								<a href="javascript:void();" onclick="submitForm('Y');
                                return false;" class="button-blue2"><?= GetMessage("SOA_TEMPL_BUTTON") ?>
          </a>
							</div>
						</div>
					</div>
				</div>
			</div>		
		<?elseif($arResult['USER_VALS']['PERSON_TYPE_ID'] == 1):?>
			
			<div class="page-props pt1">
			
				<div class="form-block">
					<div class="form-block__h">Информация о физическом лице</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"<?if($c['ID']==$loc['COUNTRY_ID']):?> selected<?endif;?>><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="ORDER_PROP_2" class="location_city">
										<?=$loc_options?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="ORDER_PROP_5" class="form-control"><?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][5]['VALUE']?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_4" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][4]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_22" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][22]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_6" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][6]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail</label>
									<input type="text" name="ORDER_PROP_1" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][1]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6 hidden">
								<div class="form-group">
									<label class="label-control">ФИО <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_7" class="form-control sb_NAME" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][7]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Имя <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_54" class="form-control sb_F_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][54]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Фамилия <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_46" class="form-control sb_L_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][46]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Отчество <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_47" class="form-control sb_P_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][47]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="Y" id="agree"<? if (array_key_exists('agree', $_POST)) echo ' checked' ?>>
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на <a href="https://www.depohost.ru/doc/pl.pdf" target="_blank">обработку персональных данных</a>
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="offerta" value="Y" id="offerta"<? if (array_key_exists('offerta', $_POST)) echo ' checked' ?>>
										<span class="checkbox-checker"><span></span></span> 
										Я согласен(а) с условиями <a href="/upload/docs/oferta_fiz.pdf" target="_blank">публичной оферты</a>
									</label>
								</div>
							</div>
	
							<div class="col-md-12" style="text-align:center;">
								<a href="javascript:void();" onclick="submitForm('Y');
                                return false;"  class="button-blue2"><?= GetMessage("SOA_TEMPL_BUTTON") ?>
								</a>
							</div>
						</div>
					</div>
				</div>
				
			
			</div>
		<?elseif($arResult['USER_VALS']['PERSON_TYPE_ID'] == 3):?>
			
			<div class="page-props pt3">

				<div class="form-block">
					<div class="form-block__h">Информация об индивидуальном предпринимателе</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="label-control">Ф.И.О предпринимателя <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_28" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][28]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"<?if($c['ID']==$loc['COUNTRY_ID']):?> selected<?endif;?>><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="ORDER_PROP_17" class="location_city">
										<?=$loc_options?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="ORDER_PROP_30" class="form-control"><?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][10]['VALUE']?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_29" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][29]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Коды организации</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ИНН <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_32" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][32]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ОГРНИП <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_31" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][31]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Банковские реквизиты </div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Банк <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_33" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][33]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Расчетный счет <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_34" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][34]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">БИК <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_36" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][36]['VALUE']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_37" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][37]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_38" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][38]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail</label>
									<input type="text" name="ORDER_PROP_39" class="form-control" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][39]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6 hidden">
								<div class="form-group">
									<label class="label-control">Контактная персона <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_40" class="form-control sb_CONTACT_PERSON" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][40]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Фамилия контактного лица <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_52" class="form-control sb_L_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][52]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Имя контактного лица <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_51" class="form-control sb_F_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][51]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Отчество контактного лица  <span class="required">*</span></label>
									<input type="text" name="ORDER_PROP_53" class="form-control sb_P_NAME" onkeyup="sbName();" value="<?=$arResult["ORDER_PROP"]["USER_PROPS_Y"][53]['VALUE']?>">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="Y" id="agree"<? if (array_key_exists('agree', $_POST)) echo ' checked' ?>>
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на <a href="https://www.depohost.ru/doc/pl.pdf" target="_blank">обработку персональных данных</a>
									</label>
								</div>
							</div>
							<div class="col-md-12" style="text-align:center;">
								<a href="javascript:void();" onclick="submitForm('Y');
									return false;" class="button-blue2"><?= GetMessage("SOA_TEMPL_BUTTON") ?>
								</a>
							</div>
						</div>
					</div>
				</div>
							
			</div>
		<?endif;?>
	
	
      <?
     // PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], $arResult['USER_VALS']['PERSON_TYPE_ID']);
     // PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"], $arResult['USER_VALS']['PERSON_TYPE_ID']);
      ?>
    </div>
  </div>

  <script type="text/javascript">
  function toggleDoverennost(select_osnovanie) {
	var val = select_osnovanie.value;
	var doverennost = document.getElementById('doverennost');
	if (val == 20) {//variant is doverennost
	  doverennost.style.display = 'block';
	}
	else {
	  doverennost.style.display = 'none';
	}

  }

  window.onload = function () {
	var select_osnovanie = document.getElementById('ORDER_PROP_20');
	  toggleDoverennost(select_osnovanie);
  };
    function fGetBuyerProps(el) {
      var show = '<?= GetMessageJS('SOA_TEMPL_BUYER_SHOW') ?>';
      var hide = '<?= GetMessageJS('SOA_TEMPL_BUYER_HIDE') ?>';
      var status = BX('sale_order_props').style.display;
      var startVal = 0;
      var startHeight = 0;
      var endVal = 0;
      var endHeight = 0;
      var pFormCont = BX('sale_order_props');
      pFormCont.style.display = "block";
      pFormCont.style.overflow = "hidden";
      pFormCont.style.height = 0;
      var display = "";

      if (status == 'none') {
        el.text = '<?= GetMessageJS('SOA_TEMPL_BUYER_HIDE'); ?>';

        startVal = 0;
        startHeight = 0;
        endVal = 100;
        endHeight = pFormCont.scrollHeight;
        display = 'block';
        BX('showProps').value = "Y";
        el.innerHTML = hide;
      }
      else {
        el.text = '<?= GetMessageJS('SOA_TEMPL_BUYER_SHOW'); ?>';

        startVal = 100;
        startHeight = pFormCont.scrollHeight;
        endVal = 0;
        endHeight = 0;
        display = 'none';
        BX('showProps').value = "N";
        pFormCont.style.height = startHeight + 'px';
        el.innerHTML = show;
      }

      (new BX.easing({
        duration: 700,
        start: {opacity: startVal, height: startHeight},
        finish: {opacity: endVal, height: endHeight},
        transition: BX.easing.makeEaseOut(BX.easing.transitions.quart),
        step: function (state) {
          pFormCont.style.height = state.height + "px";
          pFormCont.style.opacity = state.opacity / 100;
        },
        complete: function () {
          BX('sale_order_props').style.display = display;
          BX('sale_order_props').style.height = '';
        }
      })).animate();
    }
  </script>

  <div style="display:none;">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:sale.ajax.locations", $arParams["TEMPLATE_LOCATION"], array(
      "AJAX_CALL" => "N",
      "COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
      "REGION_INPUT_NAME" => "REGION_tmp",
      "CITY_INPUT_NAME" => "tmp",
      "CITY_OUT_LOCATION" => "Y",
      "LOCATION_VALUE" => "",
      "ONCITYCHANGE" => "submitForm()",
    ), null, array('HIDE_ICONS' => 'Y')
    );
    ?>
  </div>
<?
if (!$GLOBALS['USER']->IsAuthorized())
{

  if ($arResult['USER_VALS']['PERSON_TYPE_ID'] == 1)
  {
    //if физ лицо
    ?>
		<?/*
    <div class="field_checkbox">
      <input type="checkbox" name="offerta" value="Y"
             id="offerta"<? if (array_key_exists('offerta', $_POST)) echo ' checked' ?>>
      <label for="offerta"><?= GetMessage('OFFERTA') ?></label>
    </div>
	*/?>
    <?
  }
  ?>
	<?/*
  <div class="field_checkbox">
    <input type="checkbox" name="agree" value="Y" id="agree"<? if (array_key_exists('agree', $_POST)) echo ' checked' ?>
    >
    <label for="agree"><?= GetMessage('AGREE') ?></label>
  </div>
  */?>
<? } ?>