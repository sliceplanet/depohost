<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!empty($arResult["ORDER"]))
{
    global $USER;
    $login = $USER->GetLogin();

    if ($_REQUEST['BITRIX_SM_LOGIN'] == $login)
    {
        $is_register = 'Y';
    }
    Bitrix\Main\Loader::includeModule('sale');
    $count = CSaleOrder::GetList(
            array("ID"=>"DESC"),
            array('USER_ID'=>$USER->GetID()),
            array()
        );
    if ((int)$count>1)
    {
        $is_register = 'Y';
    }
    ?>
    <h2 class="h2">Заказ сформирован</h2>
    <? if ($is_register == 'Y'): ?>
        <p class="message-page">
          Заказ успешно оформлен<br><br>
          <a href="<?= SITE_DIR ?>personal/" class="btn">Преейти в личный кабинет</a></div>
        </p>
    <? else: ?>
        <p class="message-page">
          Регистрация завершена успешно, вам на контактный e-mail высланны координаты доступа входа в личный кабинет.
          <br><br><a href="<?= SITE_DIR ?>personal/" class="btn">Преейти в личный кабинет</a>
        </p>
    <? endif;
    ?>
	<script type="text/javascript">
    dataLayer.push({'event': 'Form_Submit_registration'});
  </script>
    <!--	-->

    <?
    /*
      ?>
      <b><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></b><br /><br />
      <table class="sale_order_full_table">
      <tr>
      <td>
      <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
      <br /><br />
      <!--<?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>-->
      </td>
      </tr>
      </table>
      <?
      if (!empty($arResult["PAY_SYSTEM"]))
      {
      ?>
      <br /><br />

      <table class="sale_order_full_table">
      <tr>
      <td class="ps_logo">
      <div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
      <?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
      <div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
      </td>
      </tr>
      <?
      if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
      {
      ?>
      <tr>
      <td>
      <?
      if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
      {
      ?>
      <script language="JavaScript">
      window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
      </script>
      <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
      <?
      if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
      {
      ?><br />
      <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
      <?
      }
      }
      else
      {
      if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
      {
      include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
      }
      }
      ?>
      </td>
      </tr>
      <?
      }
      ?>
      </table>
      <?
      }
     */
}
else
{
    ?>
    <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br /><br />

    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
            </td>
        </tr>
    </table>
    <?
}
?>
