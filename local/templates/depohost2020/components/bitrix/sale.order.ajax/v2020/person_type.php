<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
if ($USER->IsAuthorized())
{
  ?>
  <input type="hidden" name="PERSON_TYPE_OLD" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
  <input type="hidden" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
  <?
}
else
{
  if (count($arResult["PERSON_TYPE"]) > 1)
  {
    ?>
	<div class="form-block">
		<div class="form-block__h">Тип договора</div>
		<div class="form-block__content">
			<div class="row">
				<? foreach ($arResult["PERSON_TYPE"] as $v): ?>
					<div class="col-md-6">
						<label class="radio-label">
							<input 
								type="radio" 
								name="PERSON_TYPE" 
								value="<?= $v["ID"] ?>"
								<? if ($v["CHECKED"] == "Y" && $_POST['PERSON_TYPE'] != "AUTH"):?>
									checked="checked"
								<?endif;?>
								onClick="submitForm()"
								> <span class="checker"><span></span></span> <?= $v["NAME"] ?>
						</label>
					</div>
				<?endforeach;?>
					<div class="col-md-6">
						<label class="radio-label">
							<input 
								type="radio" 
								name="PERSON_TYPE" 
								value="AUTH"
								<? if ($v["CHECKED"] == "Y" && $_POST['PERSON_TYPE'] == "AUTH"):?>
									checked="checked"
								<?endif;?>
								onClick="submitForm()"
								> <span class="checker"><span></span></span> У меня есть договор
						</label>
					</div>
					<input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
			</div>
		</div>
	</div>
    <?
  }
  else
  {
    if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
    {
      //for IE 8, problems with input hidden after ajax
      ?>
      <span style="display:none;">
                <input type="text" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
                <input type="text" name="PERSON_TYPE_OLD"
                       value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
            </span>
      <?
    }
    else
    {
      foreach ($arResult["PERSON_TYPE"] as $v)
      {
        ?>
        <input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>"/>
        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>"/>
        <?
      }
    }
  }
}
?>