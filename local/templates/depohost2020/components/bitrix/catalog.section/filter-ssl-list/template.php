<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

function mapDomain($xmlId) {
  switch ($xmlId) {
    case 'domen':
      return 'D';
    case 'domen_organisation':
      return 'D+O';
    case 'plus_ev':
      return 'EV';
  }
}
$this->setFrameMode(true); ?>

<div class="ssl__table ssl__table_padding-height">
  <div class="ssl__table_head">
    <? if(empty($arParams["TITLE_BLOCK"])): ?>
      <?= $arResult['NAME'] ?>
    <? else: ?>
      <?= $arParams["TITLE_BLOCK"]; ?>
    <? endif; ?>
    
    
  </div>
  <div class="ssl__table_body">
    <? foreach ($arResult["ITEMS"] as $arElement): ?>
      <?
      $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
      $arData = array();
      foreach ($arElement['DISPLAY_PROPERTIES'] as $arProperty)
      {
        if ($arProperty['CODE'] === 'PLUS_DOMEN')
        {
          if (!empty($arProperty['VALUE_XML_ID']))
          {
            $domain = $arProperty['VALUE_XML_ID'];
            $arData[mapDomain($arProperty['VALUE_XML_ID'])] = $arProperty['VALUE_XML_ID'];
          }
        }
        else
        {
          if (!empty($arProperty['VALUE']))
          {
            $key = substr($arProperty['CODE'],5); //erase PLUS_
            $arData[$key] = strtolower($arProperty['CODE']);
          }
        }
      }
      $arPrice = reset($arElement['PRICES']);
      ?>
      <div id="<?= $this->GetEditAreaId($arElement ['ID']) ?>" class="ssl__table_row"
           data-domain="<?= $domain ?>" data-filter="<?= implode(';', $arData) ?>"
        data-id="<?=$arElement ['ID']?>" data-price="<?= $arPrice['DISCOUNT_VALUE'] ?>">
        <div class="ssl__table_cell">
          <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>" class="ssl__name"><?= $arElement['NAME'] ?></a>
          <div class="ssl__description"><?= $arElement["PREVIEW_TEXT"] ?></div>
        </div>
        <div class="ssl__table_cell">
          <? if (!empty($arData)): ?>
            <? foreach ($arData as $key => $data): ?>
              <span class="ssl__icon ssl__icon-<?= $data ?>"><?=$key?></span>
            <? endforeach; ?>
          <? endif ?>
        </div>
        <div class="ssl__table_cell">
          <div class="ssl__price"><?= $arPrice['PRINT_DISCOUNT_VALUE'] ?></div>
        </div>
        <div class="ssl__table_cell ssl__order">
          <div class="btn">Заказать</div>
        </div>
        <div class="ssl__order_form">
            <div class="ssl__order_form_field">
              <input data-id="<?= $arElement['ID'] ?>" type="text" name="prop[domain]" value="" placeholder="Введите имя вашего домена"/>
            </div>
            <div class="ssl__order_form_field">
              <select data-id="<?= $arElement['ID'] ?>" name="prop[email]" disabled>
                <option value="0"><? echo GetMessage("EMPTY_EMAIL") ?></option>
                <? foreach ($arElement['TYPE_EMAIL'] as $value => $name): ?>
                  <option value="<?= $value ?>" data-val="<?= $name ?>"><?= $name ?></option>
                <? endforeach; ?>
              </select>
            </div>
            <div class="ssl__order_form_field">
              <? for ($i = 1; $i <= $arElement['COUNT_YEARS']; $i++): ?>
                <label>
                  <input
                      data-id="<?= $arElement['ID'] ?>"
                      data-measure="<?= CUsefull::endOfWord($i, array('лет', 'год', 'года')) ?>"
                      type="radio"
                      name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>"
                      class="year"
                      value="<?= $i ?>"
                    <?= $i == 1 ? ' checked' : '' ?>
                  />&nbsp;
                  <?= $i ?>&nbsp;<?= CUsefull::endOfWord($i, array('лет', 'год', 'года')) ?>
                </label>
              <? endfor; ?>
            </div>
            <div class="ssl__order_form_field btn_wrapper">
              <a href="javascript:void(0)" rel="nofollow" class="btn btn-buy-ssl">Продолжить</a>
            </div>
        </div>
      </div>

    <? endforeach; ?>
  </div>
</div>

<?
$section_props = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arResult['IBLOCK_ID'],
                                                        'ID' => $arResult['ID']), true, array("UF_DESCRIPTION"));
$props_array = $section_props->GetNext();
$res = CIBlockElement::GetByID($props_array['UF_DESCRIPTION']);
if ($ar_res = $res->GetNext())
{
  echo $ar_res['DETAIL_TEXT'];
}
?>
