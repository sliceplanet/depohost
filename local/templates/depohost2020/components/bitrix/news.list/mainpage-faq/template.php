<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
$this->setFrameMode(true);
?>
<section class="mainpage-block faq-block">
  <div class="container">
    <div class="mpb_h">Вопросы и ответы</div>
    <div class="faq-items">
  <? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

    <div class="faq-item">
      <!--<div class="faq-item__img">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
      </div>-->
      <div class="faq-item__bottom">
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="faq-item__name"><?=$arItem['NAME']?></a>
        <p class="faq-item__desc"><?=$arItem['PREVIEW_TEXT']?></p>
      </div>
    </div>

  <? endforeach; ?>
    </div>
  </div>
</section>
