<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
$this->setFrameMode(true);
?>
<div class="top-slider">
  <? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

    <div class="top-slider__slide">
      <div class="container_">
        <div class="top-slider__slide_wrapper" style="background: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>) no-repeat left top;background-size: contain;">
          <div class="top-slider__left">
          </div>
          <div class="top_slider__right">
            <div class="top-slider__right_content">
              <div class="top-slider__h"><?=$arItem['NAME']?></div>
              <div class="top-slider__subh"><?=$arItem['PROPERTIES']['TEXT1']['VALUE']?></div>
              <div class="top-slider__text"><?=$arItem['PREVIEW_TEXT']?></div>

              <a 
              class="blue-button" 
              href="<?=$arItem['PROPERTIES']['BUTTON_LINK']['VALUE'] ?: '#'?>" 
              <?=$arItem['PROPERTIES']['BUTTON_ATTR']['VALUE']?>><?=$arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']?></a>

            </div>
          </div>
        </div>
      </div>  
    </div>

  <? endforeach; ?>
</div>
