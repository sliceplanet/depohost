$(document).ready(function(){
  $('.top-slider').slick({
  autoplay:true,
  autoplaySpeed:5000,
  	arrows:true,
  	dots:true,
  	prevArrow: '<a class="mps-prev"><img src="/images/mps-left.png" /></a>',
  	nextArrow: '<a class="mps-next"><img src="/images/mps-right.png" /></a>',
  });
}); 
