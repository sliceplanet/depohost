<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main-new__block">
  <? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="main-new__item">
      <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
        <img class="main-new__img"
             src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
             alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
             title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>">
      </a>
      <div class="main-new__item-desc__wrap">
        <div class="main-new__item-desc__block">
          <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="main-new__item-title"><? echo $arItem["NAME"] ?></a>
          <div class="main-new__item-desc"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
        </div>
      </div>
    </div>
  <? endforeach; ?>
</div>
