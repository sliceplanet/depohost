<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
{
    return;
}
else
{
    $arItems = $arResult;
    $arResult['ALL_ITEMS'] = $arItems;
    $arResult['COUNT'] = count($arItems);
}

//--------------------
global $USER;
\Bitrix\Main\Loader::includeModule('clients');
$client_ob = new Itin\Depohost\Clients($USER->GetID());

/**@var int     ID элемента инфоблока Карточки клиентов*/
$element_id = $client_ob->getElementId();
$arClient = $client_ob->getById($element_id);
$arServices = $client_ob->getDedicatedServers($element_id);
$arResult['CLIENT_INFO'] = array(
    'IS_DEDICATED' => count($arServices)>0 ? 'Y' : 'N',
);
if ($arResult['CLIENT_INFO']['IS_DEDICATED'] == 'N')
{
    foreach ($arResult['ALL_ITEMS'] as $key => $arItem)
    {
        if ($arItem['PARAMS']['IS_DEDICATED'] == 'Y')
        {            
            unset($arResult['ALL_ITEMS'][$key]);
        }
    }
    $arResult['ALL_ITEMS'] = array_values($arResult['ALL_ITEMS']);
}

?>

