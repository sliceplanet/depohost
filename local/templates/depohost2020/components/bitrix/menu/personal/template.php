<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (empty($arResult["ALL_ITEMS"]))
	return;
if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');
$menuBlockId = "catalog_menu_".$this->randString();
foreach ($arResult['ALL_ITEMS'] as $key => $arItem)
{
    $next = $key+1;
    if ($arItem['DEPTH_LEVEL']==1):?>
        <div class="menu-item<?=$arItem['SELECTED'] ? ' active' : '';?>">
            <div class="menu-ttl"><i></i><?=$arItem['TEXT']?></div>
    <?endif;
    if ($arItem['DEPTH_LEVEL']==2):?>
            <a href="<?=$arItem['LINK']?>"
               <?=$arItem['SELECTED'] ? ' class="active"' : '';?>
                <?=!empty($arItem['PARAMS']['ATTR']) ? $arItem['PARAMS']['ATTR'] : ''?>
            ><?=$arItem['TEXT']?></a>
            <div class="clearFix"></div>
    <?endif;
    if ($arResult['ALL_ITEMS'][$next]['DEPTH_LEVEL']==1 || $key == $arResult['COUNT']-1):
        ?>
        </div>
        <?
    endif;
}
?>

