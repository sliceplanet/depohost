<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"])
{
    ?>
    <table class="hidden-xs blue-v2">
        <thead>
            <tr class="head bg-light-blue">
                <? foreach ($arResult['COLS'] as $col)
                {
                    if ($col == 'name')
                    {
                        echo '<td class="',$col,'"><div class="bordered">', GetMessage('CT_BCSL_THEAD_'.strtoupper($col)),'</div></td>';
                    }
                    else
                    {
             echo '<td class="',$col,'"><div class="with-icon">', GetMessage('CT_BCSL_THEAD_'.strtoupper($col)),'</div></td>';
                    }

                }
                ?>
            </tr>
        </thead>
        <tbody>


    <?
    foreach ($arResult['SECTIONS'] as $arSection)
    {
        if ($arSection['ELEMENT_CNT'] > 0)
        {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
                $arSectionDeleteParams);
            ?>
            <tr>
                <td class="section-name bg-dark-blue" colspan="9">
                    <?=$arSection['NAME']?>
                </td>
            </tr>
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "",
                Array(
                    "TEMPLATE_THEME" => "blue",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "ADD_TO_BASKET_ACTION" => "BUY",
                    "SHOW_CLOSE_POPUP" => "N",
                    "MESS_BTN_BUY" => "Заказать",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_COMPARE" => "Сравнить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                    "SECTION_ID" => $arSection["ID"],
                    "SECTION_CODE" => "",
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER2" => "asc",
                    "FILTER_NAME" => "arrFilter",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "N",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "BROWSER_TITLE" => "-",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "-",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_STATUS_404" => "N",
                    "PAGE_ELEMENT_COUNT" => "30",
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array("CONFIGURATION"),
                    "OFFERS_LIMIT" => "5",
                    "PRICE_CODE" => array("Розничная"),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                    "PRODUCT_PROPERTIES" => array('CONFIGURATION'),
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    'COLS' => $arResult['COLS'],
                ),
                $component
            );?>
        <?

        }//if ($arSection['ELEMENT_CNT'] > 0)

    }//foreach ($arResult['SECTIONS'] as &$arSection)
    ?>
        </tbody>
    </table>
    <div class="hidden-sm hidden-md hidden-lg item_list">
      <?
      foreach ($arResult['SECTIONS'] as $arSection)
      {
        if ($arSection['ELEMENT_CNT'] > 0)
        {
          $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
          $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
            $arSectionDeleteParams);
          ?>
          <div class="item_list__section"><?=$arSection['NAME']?></div>

          <?$APPLICATION->IncludeComponent(
          "bitrix:catalog.section",
          "mobile",
          Array(
            "TEMPLATE_THEME" => "blue",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => "-",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "ADD_TO_BASKET_ACTION" => "BUY",
            "SHOW_CLOSE_POPUP" => "N",
            "MESS_BTN_BUY" => "Заказать",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_BTN_COMPARE" => "Сравнить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "SECTION_ID" => $arSection["ID"],
            "SECTION_CODE" => "",
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "asc",
            "FILTER_NAME" => "arrFilter",
            "INCLUDE_SUBSECTIONS" => "Y",
            "SHOW_ALL_WO_SECTION" => "N",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "BROWSER_TITLE" => "-",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "-",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "-",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_COMPARE" => "N",
            "SET_STATUS_404" => "N",
            "PAGE_ELEMENT_COUNT" => "30",
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array("CONFIGURATION"),
            "OFFERS_LIMIT" => "5",
            "PRICE_CODE" => array("Розничная"),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "USE_PRODUCT_QUANTITY" => "N",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "Y",
            "PRODUCT_PROPERTIES" => array('CONFIGURATION'),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "HIDE_NOT_AVAILABLE" => "N",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            'COLS' => $arResult['COLS'],
          ),
          $component
        );?>
          <?

        }//if ($arSection['ELEMENT_CNT'] > 0)

      }//foreach ($arResult['SECTIONS'] as &$arSection)
      ?>
    </div>
    <?
}//if (0 < $arResult["SECTIONS_COUNT"])
