<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        echo '<tr class="item" data-detail="',$arItem['DETAIL_PAGE_URL'],'">';
        echo '<td class="name"><div class="bordered">',$arItem['NAME'],'</div></td>';
        foreach ($arResult['CONFIGURATION_COLS'] as $col)
        {
            ?>
            <td class="<?=$col?>"><?=array_shift($arItem['CONFIGURATION'])?></td>
        <?
        }
        echo '<td class="price"><div class="value">',str_replace(' ','&nbsp;',number_format($arItem['MIN_PRICE']['VALUE'],0,'.',' ')),'&nbsp;Р</div>',GetMessage('CT_BCS_PERIOD_VDS'),'</td>';
        ?>
      <td class="order">   <a href="<?= $arItem['BUY_URL'] ?>" rel="nofollow" class="btn-green sm"
         id="btn<?= $arItem['ID'] ?>">
        <span class="visible-lg"><? echo $arParams['MESS_BTN_BUY'] ?></span>
        <span class="visible-sm visible-md"><span class="icon-cart"></span></span>
      </a>
      </td>
      <?
        echo '</tr>';
    }//foreach ($arResult['ITEMS'] as $key => $arItem)
}//if (!empty($arResult['ITEMS']))
?>