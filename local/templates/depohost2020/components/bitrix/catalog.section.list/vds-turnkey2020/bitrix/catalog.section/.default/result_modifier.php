<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */



$arResult['CONFIGURATION_COLS'] = array_slice($arParams['COLS'],1,-2); //minus cols: name, price, order
$countCols = count($arResult['CONFIGURATION_COLS']);

foreach ($arResult['ITEMS'] as $key => $arItem)
{
    $properties = explode('/',$arItem['PROPERTIES']['CONFIGURATION']['VALUE']);
    $properties = array_slice($properties,0,$countCols); //if properties more but cols
    $arResult['ITEMS'][$key]['CONFIGURATION'] = $properties;
    $arResult['ITEMS'][$key]['BUY_URL'] .= '&amp;prop'.urlencode('[CONFIGURATION]').'='.urlencode($arItem['PROPERTIES']['CONFIGURATION']['VALUE']);
}?>