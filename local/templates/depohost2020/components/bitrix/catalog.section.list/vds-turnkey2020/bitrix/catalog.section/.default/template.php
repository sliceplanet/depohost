﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);
		
		$conf = $arItem['PROPERTIES']['CONFIGURATION']['VALUE'];
		$t = explode('/',$conf);
		$arItem['PROPERTIES']['CPU']['DISPLAY_VALUE'] = str_replace('CPU','',$t[0]);
		$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE'] = str_replace('RAM','',$t[1]);
		
		$hdd_ssd = explode('RAID',$t[2]);
		
		$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE'] = trim(str_replace('HDD','',$hdd_ssd[0]));
		$arItem['PROPERTIES']['RAID']['DISPLAY_VALUE'] = trim($hdd_ssd[1]);
		$arItem['PROPERTIES']['SSD']['DISPLAY_VALUE'] = $t[3];
		$desc = $t[4];
		if($t[5])
			$desc .= '/'.$t[5];
 ?><div class="table-div__cell">
			<div class="row">
				<div class="col-md-10">
					<div class="flex-row">
						<div class="server-code"><?=$arItem['NAME']?></div>
						<div class="server-name"><?= $arParams['SECTION_NAME'] ?></div>
						<!--<div class="label-discount">Скидка</div>-->
						<div class="server-desc"><?=$desc?></div>
					</div>
					<div class="flex-row">
						<div class="server-prop-text server-cpu">
							<div class="spt-h">CPU</div>
							<?=$arItem['PROPERTIES']['CPU']['DISPLAY_VALUE']?>
						</div>
						<div class="server-prop-text server-ram">
							<div class="spt-h">RAM</div>
							<?=$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE']?>
						</div>
						<div class="server-prop-text server-hdd">
							<div class="spt-h">HDD</div>
							<?=$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE']?>
						</div>
						<div class="server-prop-text server-ssd">
							<div class="spt-h">SSD</div>
							<?=$arItem['PROPERTIES']['SSD']['DISPLAY_VALUE']?>
						</div>
						<div class="server-prop-text server-raid">
							<div class="spt-h">RAID</div>
							<?=$arItem['PROPERTIES']['RAID']['DISPLAY_VALUE']?>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="price">
						<span><?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][$arParams['PRICE_CODE'][0]]['DISCOUNT_VALUE'],0,'.',' '))?></span>
						Р/мес
					</div> 
					<a class="button-blue"
						href="<?= $arItem['BUY_URL'] ?>"
						data-type="solution"
						data-solution-id="<?= $arItem['ID'] ?>"
						data-months="1"
					>
						Выбрать
					</a>
				</div>
			</div>
		</div>
<?
    }//foreach ($arResult['ITEMS'] as $key => $arItem)
}//if (!empty($arResult['ITEMS']))
?>