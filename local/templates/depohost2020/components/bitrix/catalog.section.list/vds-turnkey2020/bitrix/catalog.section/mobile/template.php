<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);
        ?>
      <div class="item_list__info">
        <div class="item_list__info__row">
          <div class="item_list__info__row_label">DT #</div>
          <div class="item_list__info__row_value"><?= $arItem["NAME"] ?></div>
        </div>
        <?foreach ($arResult['CONFIGURATION_COLS'] as $col)
        {
            ?>
          <div class="item_list__info__row">
            <div class="item_list__info__row_label"><?=GetMessage('CMP_CATALOG_SECTION_MOBILE_COL_NAME_'.strtoupper($col))?></div>
            <div class="item_list__info__row_value"><?=array_shift($arItem['CONFIGURATION'])?></div>
          </div>
        <?
        }?>

        <div class="item_list__info__row">
          <div class="item_list__info__row_label">Цена</div>
          <div class="item_list__info__row_value item_list__info__row_value_price">
                   <?= str_replace(' ','&nbsp;',number_format($arItem['MIN_PRICE']['VALUE'],0,'.',' ')).' Р'?>
            <div class="item_list__info__row_value_price_period"><?=GetMessage('CT_BCS_PERIOD_VDS')?></div>
          </div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"></div>
          <div class="item_list__info__row_value"><a id="btn-m<?= $arElement['ID'] ?>" href="<?= $arItem['BUY_URL'] ?>" rel="nofollow"
                                                     class="btn-green sm order-btn">Заказать</a></div>
        </div>

      </div>

      <?
    }//foreach ($arResult['ITEMS'] as $key => $arItem)
}//if (!empty($arResult['ITEMS']))
?>