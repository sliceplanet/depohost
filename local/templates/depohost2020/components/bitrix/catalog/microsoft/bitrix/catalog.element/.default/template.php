<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
  'ID' => $strMainID,
  'PICT' => $strMainID . '_pict',
  'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
  'STICKER_ID' => $strMainID . '_stricker',
  'BIG_SLIDER_ID' => $strMainID . '_big_slider',
  'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
  'SLIDER_LIST' => $strMainID . '_slider_list',
  'SLIDER_LEFT' => $strMainID . '_slider_left',
  'SLIDER_RIGHT' => $strMainID . '_slider_right',
  'OLD_PRICE' => $strMainID . '_old_price',
  'PRICE' => $strMainID . '_price',
  'DISCOUNT_PRICE' => $strMainID . '_price_discount',
  'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
  'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
  'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
  'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
  'QUANTITY' => $strMainID . '_quantity',
  'QUANTITY_DOWN' => $strMainID . '_quant_down',
  'QUANTITY_UP' => $strMainID . '_quant_up',
  'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
  'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
  'BUY_LINK' => $strMainID . '_buy_link',
  'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
  'COMPARE_LINK' => $strMainID . '_compare_link',
  'PROP' => $strMainID . '_prop_',
  'PROP_DIV' => $strMainID . '_skudiv',
  'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
  'OFFER_GROUP' => $strMainID . '_set_group_',
  'ZOOM_DIV' => $strMainID . '_zoom_cont',
  'ZOOM_PICT' => $strMainID . '_zoom_pict'
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "<?= $arResult['NAME'] ?>",
  <?if(is_array($arResult['PREVIEW_PICTURE'])):?>
  "image": [
    "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult['PREVIEW_PICTURE']['SRC']?>"
   ],
   <?endif?>
  "description": "<?=strip_tags($arResult['PREVIEW_TEXT'])?>",
  "mpn": "<?= $arResult['ID'] ?>",
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RUB",
    "price": "<?= $arResult['MIN_PRICE']['VALUE'] ?>",
    "itemCondition": "http://schema.org/UsedCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "Artextelecom"
    }
  }
}
</script>
<div class="content" id="<? echo $arItemIDs['ID']; ?>">

  <div class="h1">
    <? echo $arResult['NAME']; ?>
    <span><?
      $res = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
      if ($ar_res = $res->GetNext())
      {
        echo $ar_res['NAME'];
      }
      ?></span>
  </div>
  <div class="wrapper element">
    <div class="server element">
      <div class="n-server-top__wrap">
        <div class="n-server-top__title">Стоимость тарифа <?= $arResult['NAME'] ?></div>
        <div class="item_price">
          <?
          $boolDiscountShow = (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF']);
          ?>
          <div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>"
               style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $arResult['MIN_PRICE']['PRINT_VALUE'] : ''); ?></div>
          <div class="item_current_price"
               id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['DISCOUNT_VALUE']; ?>
            &nbsp;<?= GetMessage('RUB_IN_MOUNTH') ?></div>
          <div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>"
               style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? GetMessage('ECONOMY_INFO', array('#ECONOMY#' => $arResult['MIN_PRICE']['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
        </div>
      </div>
      <div class="server-info">
        <?
        if ('' != $arResult['PREVIEW_TEXT'])
        {
          echo('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>');
        }
        ?>
      </div>
    </div>
    <div class="item_info_section">

      <div class="server-opis-price microsoft-element">
        <p class="server-opis-price-title">Цена:</p>
        <div class="server-opis-price-num">
          <?
          $boolDiscountShow = (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF']);
          ?>
          <div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>"
               style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $arResult['MIN_PRICE']['PRINT_VALUE'] : ''); ?></div>
          <div class="item_current_price"
               id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['DISCOUNT_VALUE']; ?></div>
          <div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>"
               style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? GetMessage('ECONOMY_INFO', array('#ECONOMY#' => $arResult['MIN_PRICE']['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
        </div>
        <div class="server-opis-price-txt"><span>/</span><?= GetMessage('RUB_IN_MOUNTH_UNDERTABLE') ?></div>
      </div>
      <?
      if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
      {
        ?>
        <div class="item_buttons vam">
            <span class="item_buttons_counter_block">

                <input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input"
                       value="<?
                       echo(isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) ? 1 : $arResult['CATALOG_MEASURE_RATIO']
                       );
                       ?>">

                <div>
                    <a href="javascript:void(0)" class="bx_bt_white bx_small bx_fwb"
                       id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
                    <a href="javascript:void(0)" class="bx_bt_white bx_small bx_fwb"
                       id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
                </div>

                <span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"></span>
            </span>
          <script type="text/javascript">
            $(document).ready(function () {
              $(".bx_bt_white").click(function () {
                var str = '<?=$strMainID?>';
                var id = $(this).attr('id');
                var quantity = $("#<? echo $arItemIDs['QUANTITY']; ?>").val();
                if (id == '<?=$arItemIDs['QUANTITY_UP']?>') {
                  quantity++;
                }
                else {
                  quantity--;
                  if (quantity <= 0) {
                    quantity = 1;
                  }
                }
                $("#<? echo $arItemIDs['QUANTITY']; ?>").val(quantity);
                $("#<? echo $arItemIDs['BUY_LINK']; ?>").attr("href", "/microsoft/?action=BUYIT&id[]=<?= $arResult['ID'] ?>&quantity[]=" + quantity);
              });
            });
          </script>
          <span class="item_buttons_counter_block">
            <a href="/microsoft/?action=BUYIT&amp;id[]=<?= $arResult['ID'] ?>&amp;quantity[]=1"
               class="bx_big bx_bt_blue bx_cart btn"
               id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><?
              echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD')
              );
              ?></a>

        </span>
        </div>
        <?
        if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
        {
          if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
          {
            ?>
            <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
            <?
          }
          else
          {
            if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
            {
              ?>
              <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>:
                <span><? $arResult['CATALOG_QUANTITY']; ?></span></p>
              <?
            }
          }
        }
      }
      else
      {
        ?>
        <div class="item_buttons vam">
                        <span class="item_buttons_counter_block">
                            <a href="javascript:void(0);" class="btn"
                               id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><?
                              echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD')
                              );
                              ?></a>
                          <?
                          if ('Y' == $arParams['DISPLAY_COMPARE'])
                          {
                            ?>
                            <a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" href="javascript:void(0)"
                               class="bx_big bx_bt_white bx_cart" style="margin-left: 10px"><?
                              echo('' != $arParams['MESS_BTN_COMPARE'] ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE')
                              );
                              ?></a>
                            <?
                          }
                          ?>
                        </span>
        </div>
        <?
      }
      ?>
    </div>
    <?
    if ('' != $arResult['DETAIL_TEXT'])
    {
      ?>
      <div class="description">
        <div class="h1 description-title"><? echo GetMessage('FULL_DESCRIPTION'); ?>
          <span><?= $arResult['NAME'] ?></span></div>
        <?
        if ('html' == $arResult['DETAIL_TEXT_TYPE'])
        {
          echo $arResult['DETAIL_TEXT'];
        }
        else
        {
          ?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
        }
        ?>
      </div>
      <?
    }
    ?>

  </div>

</div>

