<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arPrices = [];
?>
<div class="hidden-xs">
  <div class="bg-blue wrap-table-v3">
    <div class="bg-left visible-lg"></div>
    <div class="bg-right visible-lg"></div>
    <div class="band-blue wrapper wide visible-lg"></div>
    <div class="wrapper wide container clearFix bg-white wrap-table">
      <h2 class="h1"><?= $arResult['NAME'] ?></h2>

      <form  id="order-desktop" class="order" method="get" action="/personal/cart/ajax-add.php">
        <table class="table-v3" id="table-list">
          <thead class="thead">
          <tr>
            <td><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_NAME') ?></td>
            <td><span class="icon-period"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PERIOD') ?></td>
            <td><span class="icon-price"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PRICE') ?></td>
            <td><span class="icon-order"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_ORDER') ?></td>
          </tr>
          </thead>
          <?
          $rsParentSection = CIBlockSection::GetByID($arParams["SECTION_ID"]);
          if ($arParentSection = $rsParentSection->GetNext())
          {
          $arFilter = array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
            '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']);
          $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
          while ($arSect = $rsSect->GetNext())
          {
            ?>
            <tr class="section">
              <td colspan="4"><? echo $arSect['NAME']; ?></td>
            </tr>
            <?
            foreach ($arResult["ITEMS"] as $arElement):

              if ($arElement["~IBLOCK_SECTION_ID"] == $arSect['ID']):

                $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'],
                  CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'],
                  CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"),
                  array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
                ?>
                <tr id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
                  <td class="name">
                    <a
                        href="<? echo $arSect['SECTION_PAGE_URL'] . $arElement["ID"] . '/'; ?>"><?= $arElement["NAME"] ?></a>
                    <?
                    if ($arElement['PREVIEW_PICTURE']['ID'] > 0)
                    {
                      echo '<img title="' . $arElement['PREVIEW_PICTURE']['DESCRIPTION'] . '" alt="' . $arElement['PREVIEW_PICTURE']['DESCRIPTION'] . '" src="' . $arElement['PREVIEW_PICTURE']['SRC'] . '">';
                    }
                    ?>
                  </td>
                  <td>
                    <input name="id[]" type="hidden" value="<?= $arElement['ID'] ?>">
                    <select id="pr<?= $arElement['ID'] ?>quantitym" class="select_kam small period selectBox"
                            name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>[]">
                      <?
                      for ($i = 0; $i <= $arResult['MAX_VALUE']; $i++):?>
                        <option value="<?= $i ?>"><?= $i ?> <?= $arElement['CATALOG_MEASURE_NAME'] ?></option>
                      <? endfor; ?>
                    </select>
                    <script type="text/javascript">
                      $(document).ready(function () {
                        $("#pr<?= $arElement['ID'] ?>quantitym").change(function () {
                          <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
                          <? if ($arPrice = $arElement["PRICES"][$code]):
                          $arPrices[$arElement['ID']] = intval($arPrice["DISCOUNT_VALUE"]);
                          ?>
                          var price = parseInt(<?=intval($arPrice["DISCOUNT_VALUE"])?>);
                          <? endif; ?>
                          <? endforeach; ?>

                          var quantity = parseInt($("#pr<?= $arElement['ID'] ?>quantitym").val()),
                            sum = price * quantity;

                          if ($('.list-checked [data-id="<?= $arElement['ID'] ?>"]').length <= 0 && quantity > 0) {
                            $('.list-checked').append('<li data-id="<?= $arElement['ID'] ?>"><?= $arElement['NAME'] ?></li>');
                          }
                          else if ($('.list-checked [data-id="<?= $arElement['ID'] ?>"]').length > 0 && quantity <= 0) {
                            $('.list-checked').find('[data-id="<?= $arElement['ID'] ?>"]').remove();
                          }
                          $('.catalog-price[data-id="<?= $arElement['ID'] ?>"]').text(sum + ' P');
                          $("#btn<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + quantity);
                        });
                      });
                    </script>
                  </td>
                  <td class="server-price">
                    <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
                      <? if ($arPrice = $arElement["PRICES"][$code]): ?>
                        <span data-id="<?= $arElement['ID'] ?>"
                              class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                        <div class="price-one"><span class="measure">1&nbsp;<?= $arElement['CATALOG_MEASURE_NAME'] ?>
                            &nbsp;=&nbsp;</span><span class="value"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span></div>
                      <? else: ?>
                        &nbsp;
                      <? endif; ?>
                    <? endforeach; ?>
                  </td>
                  <td class="server-btn">
                    <? if (count($arResult["PRICES"]) > 0): ?>
                      <a href="<?= $arElement['BUY_URL'] ?>" rel="nofollow" class="btn-green sm"
                         id="btn<?= $arElement['ID'] ?>">
                        <span class="visible-lg"><? echo GetMessage("CATALOG_BUY") ?></span>
                        <span class="visible-sm visible-md"><span class="icon-cart"></span></span>
                      </a>
                    <? endif; ?>
                  </td>
                </tr>
              <?
              endif;
            endforeach;
            ?>
            <?
          }
          ?>
        </table>
        <div class="wrap-checked">
          <div class="head-block">Выбрано на сумму <span id="total-desktop">0 Р</span></div>
          <ul class="list-checked"></ul>
        </div>
        <input type="hidden" name="action" value="BUY">

        <input id="order-checked" value="Заказать выбранные" type="submit" class="btn-green sm">
        <div class="clearfix"></div>
      </form>
      <?
      }
      ?>
    </div>
  </div>
  <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <p><?= $arResult["NAV_STRING"] ?></p>
  <?

  endif ?>
</div>
<div class="visible-xs container">
  <h2 class="table__title"><?= $arResult['NAME'] ?></h2>
  <form id="order-mobile" method="get" action="/personal/cart/ajax-add.php" class="item_list">
    <?
    $rsParentSection = CIBlockSection::GetByID($arParams["SECTION_ID"]);
    if ($arParentSection = $rsParentSection->GetNext())
    {
      $arFilter = array(
        'IBLOCK_ID' => $arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
        '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']);
      $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
      while ($arSect = $rsSect->GetNext())
      {
        ?>
        <div class="item_list__section"><? echo $arSect['NAME']; ?></div>
        <?
        foreach ($arResult["ITEMS"] as $arElement):

          if ($arElement["~IBLOCK_SECTION_ID"] == $arSect['ID']):
            $arPrice = reset($arElement["PRICES"]);

            $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'],
              CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'],
              CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"),
              array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item_list__info">
              <div class="item_list__info__row">
                <div class="item_list__info__row_label">Название</div>
                <div class="item_list__info__row_value"><?= $arElement["NAME"] ?></div>
              </div>
              <div class="item_list__info__row item_list__info__row_quantity">
                <div class="item_list__info__row_label">Количество</div>
                <div class="item_list__info__row_value">
                  <input name="id[]" type="hidden" value="<?= $arElement['ID'] ?>">
                  <select id="pr-m<?= $arElement['ID'] ?>quantitym" class="select_kam small period selectBox"
                          name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>[]">
                    <?
                    for ($i = 0; $i <= $arResult['MAX_VALUE']; $i++):?>
                      <option value="<?= $i ?>"><?= $i ?> <?= $arElement['CATALOG_MEASURE_NAME'] ?></option>
                    <? endfor; ?>
                  </select>
                  <script type="text/javascript">
                    $(document).ready(function () {
                      $("#pr-m<?= $arElement['ID'] ?>quantitym").change(function () {
                        var price = parseInt(<?=intval($arPrice["DISCOUNT_VALUE"])?>);
                        var quantity = parseInt($("#pr-m<?= $arElement['ID'] ?>quantitym").val()),
                          sum = price * quantity;

                        if ($('.list-checked-m [data-id="<?= $arElement['ID'] ?>"]').length <= 0 && quantity > 0) {
                          $('.list-checked-m').append('<li data-id="<?= $arElement['ID'] ?>"><?= $arElement['NAME'] ?></li>');
                        }
                        else if ($('.list-checked-m [data-id="<?= $arElement['ID'] ?>"]').length > 0 && quantity <= 0) {
                          $('.list-checked-m').find('[data-id="<?= $arElement['ID'] ?>"]').remove();
                        }
                        $('#order-mobile .catalog-price[data-id="<?= $arElement['ID'] ?>"]').text(sum + ' P');
                        $("#btn-m<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + quantity);


                      });
                    });
                  </script>
                </div>
              </div>
              <div class="item_list__info__row">
                <div class="item_list__info__row_label">Цена</div>
                <div class="item_list__info__row_value">
                   <span data-id="<?= $arElement['ID'] ?>"
                         class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                  <div class="price-one"><span class="measure">1&nbsp;<?= $arElement['CATALOG_MEASURE_NAME'] ?>
                      &nbsp;=&nbsp;</span><span class="value"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span></div>
                </div>
              </div>
              <div class="item_list__info__row">
                <div class="item_list__info__row_label"></div>
                <div class="item_list__info__row_value"><a id="btn-m<?= $arElement['ID'] ?>" href="<?= $arElement['BUY_URL'] ?>" rel="nofollow"
                                                           class="btn-green sm order-btn">Заказать</a></div>
              </div>

            </div>
          <?
          endif;
        endforeach;
        ?>

        <?
      }
      ?>
      <div class="item_list__checked">
        <div class="item_list__checked_label">Выбрано на сумму: <span id="total-mobile">0 Р</span></div>
        <ul class="item_list__checked_list list-checked-m"></ul>
      </div>
      <input type="hidden" name="action" value="BUY">

      <input value="Заказать выбранные" type="submit" class="order_selected btn-green sm order-btn">
      <?
    }
    ?>

  </form>
</div>
<script>
  var arPrices = JSON.parse('<?=json_encode($arPrices)?>');
  $("#order-desktop select").change(function () {
    var sum = 0;
    $.each($('#order-desktop select'), function() {
      var select = $(this);
      var id = select.prev().val();
      var count = +select.val();
      var price = arPrices[id] ? +arPrices[id] :  0;
      sum += price * count;
    });

    $('#total-desktop').html(formatPrice(sum));
  });

  $("#order-mobile select").change(function () {
    var sum = 0;
    $.each($('#order-mobile select'), function() {
      var select = $(this);
      var id = select.prev().val();
      var count = +select.val();
      var price = arPrices[id] ? +arPrices[id] :  0;
      sum += price * count;
    });

    $('#total-mobile').html(formatPrice(sum));
  });
</script>