jQuery(document).ready(function($) {
  var validDefault = { domain: false, email: false, year: false };
  var valid = validDefault;

  // validate domain
  $(document).on("keyup", '[name="prop[domain]"]', function(event) {
    var input = $(this),
      val = input.val(),
      expr = new RegExp(
        "^([а-яА-ЯёЁa-zA-Z0-9]([а-яА-ЯёЁa-zA-Z0-9\\-]{0,61}[а-яА-ЯёЁa-zA-Z0-9])?\\.)+[а-яА-ЯёЁa-zA-Z]{2,6}$",
        "ig"
      ),
      bool = expr.test(val),
      id = input.attr("data-id"),
      select_email = $('[name="prop[email]"]');
    if (bool) {
      var chunk = val.split(".");
      var d1 = chunk.pop(),
        d2 = chunk.pop();

      var domain = d2 + "." + d1;

      select_email.attr("disabled", false);
      $.each(select_email.find("option[value!=0]"), function(i, v) {
        op = $(v);
        op.text(op.attr("data-val") + "@" + domain);
      });
      valid.domain = true;
    } else {
      select_email.attr("disabled", true);
      valid.domain = false;
    }
    //
  });
  $(document).on("change", '[name="prop[email]"]', function(event) {
    var input = $(this),
      val = input.val(),
      id = input.attr("data-id");

    if (val != 0) {
      var button = $('tr[data-id="' + id + '"] a.btn'),
        parent = $('tr[data-id="' + id + '"]');
      button.attr("disabled", false);
      var year_check = parent.find(".year:checked"),
        year_val = year_check.val() + " " + year_check.attr("data-measure");
      //            var href = button.attr('data-url')+'&prop[domain]='+parent.find('[name="prop[domain]"]').val()+'&prop[email_admin]='+parent.find('[name="prop[email]"] option:selected').text()+'&prop[years]='+year_val+'&measure_name='+year_check.attr('data-measure')+'&'+QUANTITY_VARIABLE+'='+year_check.val();
      //            button.attr('href',href);
      valid.email = true;
    } else {
      valid.email = false;
    }
  });
  $(document).on("change", ".year", function(event) {
    var input = $(this),
      val = parseInt(input.val()),
      id = input.attr("data-id"),
      priceObj = $('.catalog-price[data-id="' + id + '"]'),
      price = parseInt(priceObj.attr("data-price"));
    priceObj.html(price * val + "&nbsp;руб.");
    valid.year = true;
  });
  $(document).on("click", ".btn-buy-ssl", function(event) {
    event.preventDefault();
    var btn = $(this),
      row = btn.parents("#ssl__form"),
      id = row.data("id");

    if (valid.year === false) {
      if (typeof row.find(".year:checked") !== "undefined") {
        valid.year = true;
      } else {
        row.find(".year:first").trigger("click");
      }
    }

    var year_check = row.find(".year:checked"),
      year_val = year_check.val() + " " + year_check.attr("data-measure"),
      domain_val = row.find('[name="prop[domain]"]').val(),
      email_select = row.find('[name="prop[email]"]'),
      email_val = row.find('[name="prop[email]"] option:selected').text();

    if (valid.email === true && valid.domain === true && valid.year === true) {
      var price = row.data("price"),
        sum = parseInt(price * year_check.val());

      var href =
        "/personal/cart/ajax-add.php?action=BUY&id=" +
        id +
        "&prop[domain]=" +
        domain_val +
        "&prop[email_admin]=" +
        email_val +
        "&prop[years]=" +
        year_val +
        "&measure_name=" +
        year_check.attr("data-measure") +
        "&" +
        QUANTITY_VARIABLE +
        "=1&SUM=" +
        sum +
        "&type=ssl";
      window.location = href;
    }
  });
  Ssl = function() {};

  Ssl.prototype.show = function(id) {
    $("td[data-id=" + id + "]").show();
  };
  var ssl = new Ssl();

  $("body").append('<div id="ssl__form"></div>');
  var $sslForm = $("#ssl__form");
  $(".ssl__order .btn").on("click", function() {
    var $row = $(this).parents(".ssl__table_row");
    var offset = $row.offset();
    var top = offset.top + $row.outerHeight(true);
    var left = offset.left;
    valid = validDefault;

    $sslForm.css({ top: top, left: left, width: $row.outerWidth(true) });
    $sslForm.html($row.find(".ssl__order_form").html());
    $sslForm.append('<div class="ssl__form_close">&times;</div>');

    $sslForm.show();
    try {
      $sslForm.find("[name='prop[domain]']").focus();
    } catch (e) {
      console.error(e);
    }
    $sslForm.data("id", $row.data("id"));
    $sslForm.data("price", $row.data("price"));
  });
  $sslForm.on("click", ".ssl__form_close", function(e) {
    e.preventDefault();
    $sslForm.hide();
  });
});
