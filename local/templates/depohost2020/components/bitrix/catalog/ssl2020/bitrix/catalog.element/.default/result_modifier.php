<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
Loc::loadMessages(__DIR__.'/template.php');
if (!$arResult['MODULES']['catalog'])
{
  return;
}
$arResult['TYPE'] = $arResult['IBLOCK_CODE'];

$arResult['COUNT_YEARS'] = $arResult['PROPERTIES']['MAX_YEARS']['VALUE'];
$arResult['TYPE_EMAIL'] = array_combine($arResult['PROPERTIES']['EMAIL']['VALUE_ENUM_ID'],
  $arResult['PROPERTIES']['EMAIL']['VALUE_ENUM']);

$arResult['MIN_PRICE']['PRINT_VALUE'] = number_format($arResult['MIN_PRICE']['VALUE'], 0, '.',
    ' ') . ' <span class="rub"></span>';
$arResult['BASKET_PROPS'] = array();

$quantityYears = intval($_REQUEST['quantity']) > 0 ? intval($_REQUEST['quantity']) : 1;


$arResult['BASKET_PROPS'][] = array('INPUT_HTML' => '<input type="text" name="prop[domain]" value="" placeholder="Домен" />','ID'=>'DOMAIN');

$emailsHtml = '<select id="email" disabled>
<option value="0" selected="selected">'.GetMessage("CMP_CATALOG_ELEMENT_EMPTY_EMAIL").'</option>
';
foreach ($arResult['TYPE_EMAIL'] as $value => $name)
{
  $emailsHtml .= '<option value="' . $value . '" data-val="' . $name . '">' . $name . '</option>';
}
$emailsHtml .= '</select>';

$arResult['BASKET_PROPS'][] = array('INPUT_HTML' => $emailsHtml,'ID'=>'EMAILS');
$yearsHtml = '';
for ($i = 1; $i <= $arResult['COUNT_YEARS']; $i++)
{
  $yearsHtml .= '<input data-measure="' . CUsefull::endOfWord($i, array('лет', 'год', 'года')) . '"
    type="radio"
    class="year"
    name="years"
    id="year_' . $i . '"
    value="' . $i . '"
    ' . ($i == 1 ? ' checked="checked"' : '') . '
    />&nbsp;
  <label for="year_' . $i . '">
    ' . $i . '&nbsp;' . CUsefull::endOfWord($i, array('лет', 'год', 'года')) . '
  </label>';
}
$arResult['BASKET_PROPS'][] = array('INPUT_HTML' => $yearsHtml,'ID'=>'YEARS');
$arResult['SUM']['VALUE'] = $arResult['MIN_PRICE']['VALUE'] * $quantityYears;
$arResult['SUM']['PRINT_VALUE'] = number_format($arResult['SUM']['VALUE'], 0, '.', ' ') . ' <span class="rub"></span>';

$arResult['ACTION_URL'] = SITE_DIR . 'personal/cart/ajax-add.php';
$arResult['CATALOG_MEASURE_NAME'] = 'в год';
$arResult['CATALOG_MEASURE_SHORT_NAME'] = '1 год';

?>