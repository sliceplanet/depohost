<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

function mapDomain2($xmlId) {
  switch ($xmlId) {
    case 'domen':
      return 'D';
    case 'domen_organisation':
      return 'D+O';
    case 'plus_ev':
      return 'EV';
  }
}

$this->setFrameMode(true); ?>


<div class="table-block table-block-gray ssl-table-block">
	<div class="table-div table-div1 table-div-6col">
		<div class="table-div__head">
			<div class="table-block_head_h"><?=$arResult['NAME']?></div>
			<div class="table-div__header">
				<div class="row">
					<div class="col-md-10 flex-row">
						<div class="table-div__hcell">Доступны  физ. лицам</div>
						<div class="table-div__hcell">Wildcard</div>
						<div class="table-div__hcell">поддержка IDN</div>
						<div class="table-div__hcell">EV</div>
						<div class="table-div__hcell">SGC</div>
						<div class="table-div__hcell">Шифрование</div>
						<div class="table-div__hcell">Выпуск от</div>
					</div>
					<div class="col-md-2">
						<div class="table-div__hcell price">стоимость</div>
					</div>
				</div>
			</div>
		</div>
		<div class="table-div__body">
    <? foreach ($arResult["ITEMS"] as $arElement): ?>
      <?
      $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
      $arData = array();
      foreach ($arElement['DISPLAY_PROPERTIES'] as $arProperty)
      {
        if ($arProperty['CODE'] === 'PLUS_DOMEN')
        {
          if (!empty($arProperty['VALUE_XML_ID']))
          {
            $domain = $arProperty['VALUE_XML_ID'];
            $arData[mapDomain2($arProperty['VALUE_XML_ID'])] = $arProperty['VALUE_XML_ID'];
          }
        }
        else
        {
          if (!empty($arProperty['VALUE']))
          {
            $key = substr($arProperty['CODE'],5); //erase PLUS_
            $arData[$key] = strtolower($arProperty['CODE']);
          }
        }
      }
      $arPrice = reset($arElement['PRICES']);
      ?>
	<div class="table-div__cell ssl-item-row" id="<?= $this->GetEditAreaId($arElement ['ID']) ?>" data-domain="<?= $domain ?>" data-filter="<?= implode(';', $arData) ?>"
        data-id="<?=$arElement ['ID']?>" data-price="<?= $arPrice['DISCOUNT_VALUE'] ?>">
		<div class="row">
			<div class="col-md-10">
				<div class="flex-row">
					<div class="position-name"><?= $arElement['NAME'] ?></div>
					<div class="position-desc"><?= $arElement["PREVIEW_TEXT"] ?></div>
				</div>
				<div class="flex-row main-cells">
					<div class="server-prop-text">
						<div class="spt-h">Доступны  физ. лицам</div>
						<div class="icon-yes"></div>
					</div>
					<div class="server-prop-text">
						<div class="spt-h">Wildcard</div>
						<?if($arElement['PROPERTIES']['PLUS_WC']['VALUE']):?>
							<div class="icon-yes"></div>
						<?else:?>
							<div class="icon-no"></div>
						<?endif;?>
					</div>
					<div class="server-prop-text">
						<div class="spt-h">поддержка IDN</div>
						<?if($arElement['PROPERTIES']['PLUS_IDN']['VALUE']):?>
							<div class="icon-yes"></div>
						<?else:?>
							<div class="icon-no"></div>
						<?endif;?>
					</div>
					<div class="server-prop-text">
						<div class="spt-h">EV</div>
						<?if($arElement['PROPERTIES']['PLUS_EV']['VALUE']):?>
							<div class="icon-yes"></div>
						<?else:?>
							<div class="icon-no"></div>
						<?endif;?>
					</div>
					<div class="server-prop-text">
						<div class="spt-h">SGC</div>
						<?if($arElement['PROPERTIES']['PLUS_SGC']['VALUE']):?>
							<div class="icon-yes"></div>
						<?else:?>
							<div class="icon-no"></div>
						<?endif;?>
					</div>
					<div class="server-prop-text">
						<div class="spt-h">Шифрование</div>
						256 bit
					</div>
					<div class="server-prop-text">
						<div class="spt-h">Выпуск от</div>
						15 мин
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="price">
					<span><?= number_format($arPrice['DISCOUNT_VALUE'],0,'.',' ') ?></span>
					Р/год
				</div> 
				<div class="button-blue" data-toggle="modal" data-target="#ssl_<?=$arElement['ID']?>">Выбрать</div>
			</div>
		</div>
	</div>		
	

    <? endforeach; ?>
  </div>
</div>

</div>

<?
$section_props = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arResult['IBLOCK_ID'],
                                                        'ID' => $arResult['ID']), true, array("UF_DESCRIPTION"));
$props_array = $section_props->GetNext();
$res = CIBlockElement::GetByID($props_array['UF_DESCRIPTION']);
if ($ar_res = $res->GetNext())
{
  echo $ar_res['DETAIL_TEXT'];
}
?>


<? foreach ($arResult["ITEMS"] as $arElement): ?>
	<?
	
      $arPrice = reset($arElement['PRICES']);
	?>
	<div class="modal ssl-modal fade" id="ssl_<?=$arElement['ID']?>" tabindex="-1" role="dialog" aria-labelledby="ssl_<?=$arElement['ID']?>Label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title"><?=$arElement['NAME']?></h5>
					<p class="modal-header-desc"><?=$arElement['PREVIEW_TEXT']?></p>
				</div>
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12">
							<div class="flex-row main-cells">
								<div class="server-prop-text">
									<div class="spt-h">Доступны  физ. лицам</div>
									<div class="icon-yes"></div>
								</div>
								<div class="server-prop-text">
									<div class="spt-h">Wildcard</div>
									<?if($arElement['PROPERTIES']['PLUS_WC']['VALUE']):?>
										<div class="icon-yes"></div>
									<?else:?>
										<div class="icon-no"></div>
									<?endif;?>
								</div>
								<div class="server-prop-text">
									<div class="spt-h">поддержка IDN</div>
									<?if($arElement['PROPERTIES']['PLUS_IDN']['VALUE']):?>
										<div class="icon-yes"></div>
									<?else:?>
										<div class="icon-no"></div>
									<?endif;?>
								</div>
								<div class="server-prop-text">
									<div class="spt-h">EV</div>
									<?if($arElement['PROPERTIES']['PLUS_EV']['VALUE']):?>
										<div class="icon-yes"></div>
									<?else:?>
										<div class="icon-no"></div>
									<?endif;?>
								</div>
								<div class="server-prop-text">
									<div class="spt-h">SGC</div>
									<?if($arElement['PROPERTIES']['PLUS_SGC']['VALUE']):?>
										<div class="icon-yes"></div>
									<?else:?>
										<div class="icon-no"></div>
									<?endif;?>
								</div>
								<div class="server-prop-text">
									<div class="spt-h">Шифрование</div>
									256 bit
								</div>
								<div class="server-prop-text">
									<div class="spt-h">Выпуск от</div>
									15 мин
								</div>
							</div>
						</div>
					</div>
					
					<div class="modal-form ssl-order-form">
						<form data-id="<?= $arElement['ID'] ?>" data-price="<?=$arPrice['DISCOUNT_VALUE']?>">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="label-control">Введите домен</label>
										<input data-id="<?= $arElement['ID'] ?>" type="text" required class="form-control" name="prop[domain]" value="" placeholder=""/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="label-control">Выберите email</label>
										<select data-id="<?= $arElement['ID'] ?>" name="prop[email]" required>
											<option value=""><? echo GetMessage("EMPTY_EMAIL") ?></option>
											<? foreach ($arElement['TYPE_EMAIL'] as $value => $name): ?>
												<option value="<?= $value ?>" data-val="<?= $name ?>"><?= $name ?></option>
											<? endforeach; ?>
										</select>									
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="label-control">Период</label>
										<select data-id="<?= $arElement['ID'] ?>" name="quantity" required >
											<option value=""></option>
											<? for ($i = 1; $i <= $arElement['COUNT_YEARS']; $i++): ?>
												<option value="<?= $i ?>" data-measure="<?= CUsefull::endOfWord($i, array('лет', 'год', 'года')) ?>"><?= $i ?>&nbsp;<?= CUsefull::endOfWord($i, array('лет', 'год', 'года')) ?></option>
											<? endfor; ?>
										</select>
									</div>
								</div>
							</div>
							
							<div class="ssl-form-actions">
								<button class="blue-button" type_="submit">купить</button>
							</div>
							
							
						</form>						
					</div>
					
					
					
				</div>
			</div>
		</div>
	</div>


<?endforeach;?>
