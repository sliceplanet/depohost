/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 18.10.15.
 */
jQuery(document).ready(function($) {
  var selectBoxOptions = { mobile: true };
  var price = parseInt($(".order-side [name=SUM]").val());
  $(".field select").selectBox(selectBoxOptions);

  var valid = { domain: false, email: false, year: true };
  $('[name="prop[domain]"]').on("keyup", "", function(event) {
    var input = $(this),
      val = input.val(),
      expr = new RegExp(
        "^([а-яА-ЯёЁa-zA-Z0-9]([а-яА-ЯёЁa-zA-Z0-9\\-]{0,61}[а-яА-ЯёЁa-zA-Z0-9])?\\.)+[а-яА-ЯёЁa-zA-Z]{2,6}$",
        "ig"
      ),
      bool = expr.test(val),
      select_email = $("#email");
    if (bool) {
      select_email.attr("disabled", false);
      select_email.selectBox("enable", selectBoxOptions);

      var chunk = val.split(".");
      var d1 = chunk.pop(),
        d2 = chunk.pop();

      var domain = d2 + "." + d1;

      $.each(select_email.find("option[value!=0]"), function(i, v) {
        var op = $(v);
        op.text(op.attr("data-val") + "@" + domain);
      });
      valid.domain = true;
    } else {
      select_email.attr("disabled", true);
      select_email.selectBox("disable", selectBoxOptions);

      valid.domain = false;
    }
    select_email.selectBox("refresh", selectBoxOptions);
  });
  $("#email").on("change", "", function(event) {
    var input = $(this),
      val = input.val();

    if (val != 0) {
      var button = $(".order-side [type=submit]");
      button.attr("disabled", false);
      valid.email = true;
    } else {
      valid.email = false;
      button.attr("disabled", true);
    }
  });
  $(".year").on("change", "", function(event) {
    var input = $(this),
      val = parseInt(input.val()),
      sum = price * val,
      priceObj = $("#sum");
    priceObj.html(
      number_format(sum, 0, ",", " ") + ' <span class="rub"></span>'
    );
    $(".order-side [name=SUM]").val(sum);
    valid.year = true;
  });
  $(".order-side").on("submit", function(event) {
    var row = $(this),
      year_check = row.find(".year:checked"),
      year_val = year_check.val() + " " + year_check.attr("data-measure"),
      email_val = row.find("#email option:selected").text();
    $('[name="prop[email_admin]"]').val(email_val);
    $('[name="prop[years]"]').val(year_val);
    $('[name="measure_name"]').val(year_check.attr("data-measure"));
  });
});
