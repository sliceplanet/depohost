<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Loader;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
$this->setFrameMode(true);
?>
<div id="ssl-choose-page">
	<div class="container">
	
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/icon-ssl.png">
			</div>
			<h1>Подбор SSL-сертификата</h1>
		</div>	
		
		<form>
		
			<div class="ssl-filter checkboxes-parent">
			
				<ul>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" class="all-checkbox" value="ALL" />
							<span class="checkbox-checker"></span> 
							Все
						</label>
					</li>
					<?/*<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="" />
							<span class="checkbox-checker"></span> 
							Доступен физ. лицам <span class="hint">?</span>
						</label>
					</li>*/?>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_IDN" />
							<span class="checkbox-checker"></span> 
							IDN — Поддержка национальных доменов <span class="hint">?</span>
						</label>
					</li>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_DOMEN" />
							<span class="checkbox-checker"></span> 
							Домен <span class="hint">?</span>
						</label>
					</li>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_SGC" />
							<span class="checkbox-checker"></span> 
							SGC — Высокий уровень шифрования <span class="hint">?</span>
						</label>
					</li>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_GR" />
							<span class="checkbox-checker"></span> 
							SSL green - зеленая строка браузера <span class="hint">?</span>
						</label>
					</li>
					<?/*<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="" />
							<span class="checkbox-checker"></span> 
							Домен и организации <span class="hint">?</span>
						</label>
					</li>*/?>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_EV" />
							<span class="checkbox-checker"></span> 
							EV — Расширенная проверка <span class="hint">?</span>
						</label>
					</li>
					<li>
						<label>
							<input type="checkbox" name="ssl-type[]" value="PLUS_WC" />
							<span class="checkbox-checker"></span> 
							WildCard — Поддержка субдоменов <span class="hint">?</span>
						</label>
					</li>
				</ul>
			
			</div>
		
		</form>
		
<?
if (isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && $_REQUEST[$arParams["ACTION_VARIABLE"]]=='BUY')
{
    Loader::includeModule('sale');
    Loader::includeModule('catalog');

    $quantity = intval($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
    $props = $_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]];
    $product_id = $_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]];
    $measure = !empty($_REQUEST['measure_name']) ? (string)$_REQUEST['measure_name'] : 'год';

    $ar_res = \CCatalogProduct::GetByIDEx($product_id);
    $arProduct = $ar_res['PRODUCT'];
    $arPrice = current($ar_res['PRICES']);
    $arProperties = $ar_res['PROPERTIES'];

    $arFields = array(
            "PRODUCT_ID" =>$arProduct['ID'],
            "PRODUCT_XML_ID" => $ar_res['XML_ID'],
            "MEASURE_CODE" => $arProduct['MEASURE'],
            "TYPE" => $arProduct['TYPE'],
            "SET_PARENT_ID" => 0,
            "MEASURE_NAME" => $measure,
            "CATALOG_XML_ID" => 0,
            "PRODUCT_PRICE_ID" => 0,
            "PRICE" => intval($arPrice['PRICE']),
            "CURRENCY" => $arPrice['CURRENCY'],
            "QUANTITY" => $quantity,
            "LID" => LANG,
            "MODULE" => "catalog",
            "PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
      );
      $arProps = array();

      foreach ($props as $code => $value)
      {
        $arProps[] = array(
            "NAME" => $arProperties[$code]['NAME'],
            "CODE" => $code,
            "VALUE" => str_replace('&nbps;',' ',trim($value)),
        );
      }


      $arFields["PROPS"] = $arProps;
      $basket_id = CSaleBasket::Add($arFields);
      $APPLICATION->RestartBuffer();
      LocalRedirect($arParams['BASKET_URL']);
}
?>
<?if($arParams["USE_COMPARE"]=="Y")
{?>

<?/*
$res = CIBlockSection::GetByID(162);
if($ar_res = $res->GetNext())
  echo $ar_res['DESCRIPTION'];
  */
?>
<?
global ${$arParams['FILTER_NAME']};
if($_REQUEST['ssl-type']):

	if($_REQUEST['ssl-type'][0] == 'ALL'):
	
	else:
		foreach($_REQUEST['ssl-type'] as $v):
			${$arParams['FILTER_NAME']}['!PROPERTY_'.$v] = false;
		endforeach;
	endif;

endif;
?>
	


<?$intSectionID=$APPLICATION->IncludeComponent(
"bitrix:catalog.section", "list", array(
	"IBLOCK_TYPE" => "ssl",
	"IBLOCK_ID" => "30",
	"SECTION_ID" => "162",
	"SECTION_CODE" => "ssl-sertifikaty",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "sort",
	"ELEMENT_SORT_ORDER2" => "asc",
	"FILTER_NAME" => $arParams["FILTER_NAME"],
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "N",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "30",
	"LINE_ELEMENT_COUNT" => "1",
	"PROPERTY_CODE" =>$arParams["LIST_PROPERTY_CODE"],
	"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
	"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
	"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
	"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
	"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
	"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
	"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => $arParams["PRICE_CODE"],
	"COMPATIBLE_MODE" => $arParams["COMPATIBLE_MODE"],
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
	"PRICE_VAT_INCLUDE" => "N",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => $arParams["PAGER_TITLE"],
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	$component
);
?>




<?
}
?>
	</div>
	

	<? $APPLICATION->IncludeFile(
		"/include/ssl/why-ssl.php"
	); ?>	
	
	<? $APPLICATION->IncludeFile(
		"/include/ssl/how-ssl-works.php"
	); ?>	
	
	<? $APPLICATION->IncludeFile(
		"/include/ssl/services-for-you.php"
	); ?>	
	
	
	
</div>
