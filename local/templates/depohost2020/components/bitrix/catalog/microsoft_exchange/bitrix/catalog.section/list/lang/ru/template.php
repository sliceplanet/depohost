<?
$MESS["CATALOG_BUY"] = "Заказать";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_TITLE"] = "Наименование";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["DOMAIN_NAME_EMPTY"] = "Имя домена не может быть пустым";
$MESS["DOMAIN_NAME_NOT_VALID"] = "Имя домена содержит ошибку";
$MESS["CT_CS_PLACEHOLEDER_DOM_NAME"] = "Введите имя домена";
?>