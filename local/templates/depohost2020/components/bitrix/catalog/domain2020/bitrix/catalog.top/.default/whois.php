<?php

Bitrix\Main\Loader::includeModule('whois');

function whois_domen2($domain)
{
    $domain_obj = new Itin\Depohost\Whois($domain);

    $whois_answer = $domain_obj->info();

    return $domain_obj->isAvailable();
}

?>
