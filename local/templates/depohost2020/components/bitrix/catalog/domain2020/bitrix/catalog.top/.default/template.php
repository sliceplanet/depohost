<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

$this->createFrame()->begin();
require_once 'whois.php';
?>


<div class="domain-choose-block">
	<div class="container">
	
		<form>
		
			<div class="domain-search-group">
				<input type="text" name="search_domain" class="form-control" placeholder="Введите имя домена" value="<?=$_REQUEST['search_domain']?>" /> 
				<button type="submit" class="button-blue2">ПОДОБРАТЬ</button>
			</div>
			
			<div class="domain-groups">
				<div class="domain-group collapse-parent opened">
					<div class="domain-group-header collapse-buttton_block collapse-arrow_icon_block" id="headingOne">
						<i></i>
						<button class="btn-collapse-link " type="button">
							Популярные
						</button>
					</div>
					<div id="collapseOne" class="collapse-content">
						<div class="domain-group-body">
							<ul>
								<?
								$property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
																						"SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
																											 "CODE" => "DOMAIN_BUY"));
								$prices = [];
								while ($enum_fields = $property_enums->GetNext())
								{
									$prices[$enum_fields['VALUE']] = (int)$enum_fields["XML_ID"];
								?>
									<li>
										<label>
											<input type="checkbox" name="domenzone[]" value="<?=$enum_fields["VALUE"]?>"<?if(in_array($enum_fields['VALUE'],$_REQUEST['domenzone'])):?> checked<?endif;?> /> 
											<span class="checkbox-checker"></span> 
											.<?=$enum_fields["VALUE"]?> <span class="domain-price"><?=(int)$enum_fields["XML_ID"]?> Р/год</span>
										</label>
									</li>
								<?
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</form>
		
	</div>
</div>

<?
if (isset($_REQUEST['search_domain']) && isset($_REQUEST['domenzone'])):
	$search_domain = htmlspecialcharsEx($_REQUEST['search_domain']);
	$domenzonez = $_REQUEST['domenzone'];
?>
	<div class="domain-search-results">
		<div class="container">
			<div class="dsr__h">Результаты поиска доменных имен</div>
			<div class="dsr__items">
<?
	foreach($domenzonez as $domenzone):
		$domenzone = htmlspecialcharsEx($domenzone);
		$domain = $search_domain . '.' . $domenzone;
		$domen_result = whois_domen2($domain);
?>		<? if ($domen_result): ?>

			<div class="dsr__item">
				<div class="dsr__item_left">
					<div class="dsr__item_dzone">.<?=$domenzone?></div>
					<div class="dsr__item_dname"><?=$domain?></div>
				</div>
				<div class="dsr__item_right">
					<div class="dsr__item_dstatus dstatus-ok">Доступно для регистрации</div>
					<div class="dsr__item_dprice"><?=$prices[$domenzone]?> <span>Р/год</span></div>
					<a href="<?= $arResult['URL_BUY'] ?>&DOMAIN_NAME=<?= urlencode($domain) ?>&DOMAIN_ZONE=<?= htmlspecialcharsbx($domenzone) ?>" class="button-blue">Заказать</a>
				</div>
			</div>

		<?else:?>

			<div class="dsr__item disabled">
				<div class="dsr__item_left">
					<div class="dsr__item_dzone">.<?=$domenzone?></div>
					<div class="dsr__item_dname"><?=$domain?></div>
				</div>
				<div class="dsr__item_right">
					<div class="dsr__item_dstatus dstatus-error">Доменное имя занято</div>
					<div class="dsr__item_dprice"><?=$prices[$domenzone]?> <span>Р/год</span></div>
					<span class="nobutton"></span>
				</div>
			</div>		
		<?endif;?>
		
<?
	endforeach;
?>
			</div>
			
			<div class="dsr__h">Рекомендуем</div>
			<div class="dsr__items">
			  <?
			  $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
																   "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
																						   "CODE" => "DOMAIN_BUY"));
			  while ($enum_fields = $property_enums->GetNext())
			  {
				if (!in_array($enum_fields["VALUE"],$domenzonez))
				{
				  $domain = $search_domain . '.' . $enum_fields["VALUE"];
				  $domen_result = whois_domen2($domain);
				  ?>

				<div class="dsr__item<?if ($domen_result != 1):?> disabled<?endif;?>">
					<div class="dsr__item_left">
						<div class="dsr__item_dzone">.<?= $enum_fields["VALUE"] ?></div>
						<div class="dsr__item_dname"><?= $search_domain ?>.<?= $enum_fields["VALUE"] ?></div>
					</div>
					<div class="dsr__item_right">
						<?if ($domen_result != 1):?>
							<div class="dsr__item_dstatus dstatus-error">Доменное имя занято</div>
						<?else:?>
							<div class="dsr__item_dstatus dstatus-ok">Доступно для регистрации</div>		
						<?endif;?>
						<div class="dsr__item_dprice"><?= (int)$enum_fields["XML_ID"] ?> <span>Р/год</span></div>
						<?if ($domen_result != 1):?>
							<span class="nobutton"></span>
						<?else:?>
							<a href="<?= $arResult['URL_BUY'] ?>&DOMAIN_NAME=<?= urlencode($domain) ?>&DOMAIN_ZONE=<?= $enum_fields["VALUE"] ?>" class="button-blue">Заказать</a>
						<?endif;?>
					</div>
				</div>
				
				  <?
				}
			  }
			  ?>
			</div>
			
			
			
		</div>
	</div>
<?
endif;
?>

<? $APPLICATION->IncludeFile(
	"/include/domain/desc.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/domain/wug.php"
  ); ?>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/contact-us.php"
  ); ?>



