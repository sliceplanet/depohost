<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arPrices = [];
?>

<div class="page-section__h">Аренда программного обеспечения Microsoft</div>

<form  id="order-desktop" class="order" method="get" action="/personal/cart/ajax-add.php">
	<input type="hidden" name="action" value="BUY" />
	<input type="hidden" name="is-ms" value="Y" />
<?
$rsParentSection = CIBlockSection::GetByID($arParams["SECTION_ID"]);
if ($arParentSection = $rsParentSection->GetNext())
{
	$arFilter = array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'], 
		'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
		'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], 
		'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
	);
	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
	while ($arSect = $rsSect->GetNext())
	{
?>
		

		<div class="table-block table-block-gray"> 
			<div class="table-div table-div1 table-microsoft">
				<div class="table-div__head">
					<div class="table-block_head_h"><?=$arSect['NAME']?></div>
				</div>
				<div class="table-div__body">

					<?
					foreach ($arResult["ITEMS"] as $arElement):

						if ($arElement["~IBLOCK_SECTION_ID"] == $arSect['ID']):

							$this->AddEditAction(
								$arElement['ID'], 
								$arElement['EDIT_LINK'],
								CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT")
							);
							$this->AddDeleteAction(
								$arElement['ID'], 
								$arElement['DELETE_LINK'],
								CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"),
								array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM'))
							);
							
							$code = 'Розничная';
							$arPrice = $arElement["PRICES"][$code];
					?>

							<div class="table-div__cell ms-row" id="<?= $this->GetEditAreaId($arElement['ID']); ?>" href="<? echo $arSect['SECTION_PAGE_URL'] . $arElement["ID"] . '/'; ?>">
								<div class="flex-row jcsb aic">
									<div class="td-cell">
										<div class="ms-name"><?=$arElement['NAME']?></div>
										<div class="ms-desc">
											Стоимость 
											<div class="ms-price-small">
												<span><?=$arPrice['DISCOUNT_VALUE']?></span> Р/мес					
											</div>
										</div>
													<input name="id[]" class="item_id" type="hidden" value="<?= $arElement['ID'] ?>">
									</div>
									<div class="td-cell">
										<div class="ms-counter">
											<div class="counter_block__wrapper">
												<div class="counter_block__actions" data-price="<?=$arPrice['DISCOUNT_VALUE']?>">
													<a href="#" class="cb_minus">-</a>
													<input type="text" name="quantity[]" value="0">
													<a href="#" class="cb_plus">+</a>
												</div>
											</div>
										</div>
									</div>
									<div class="td-cell">
										<div class="price ms-price">
											<span>0</span> Р/мес					
										</div>
									</div>
									<div class="td-cell">
										<div class="ms-actions">
											<a href="#" class="button-blue ms-check">Выбрать</a>
										</div>
										<div class="ms-chosen">
											<span class="green-checked-text">Выбрано</span>
											<a href="#" class="ms-uncheck"></a>
										</div>
									</div>
								</div>
							</div>

<?
						endif;
						
					endforeach;
?>
				</div>
			</div>
		</div>
		


<?
	}
	
}
?>
		<div class="ms-total-block">
			<div class="ms-total-block__wrapper">
				<div class="ms-total-block-text">
					Итого:
				</div>
				<div class="ms-total-block-price">
					<span>0</span> Р/мес
				</div>
				<div class="ms-total-block-action">
					<a href="#" class="ms-submit button-blue2">Оформить</a>
				</div>
			</div>
		</div>


</form>
