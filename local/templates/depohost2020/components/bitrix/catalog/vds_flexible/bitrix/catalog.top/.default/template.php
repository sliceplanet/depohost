<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<? foreach ($arResult["ROWS"] as $arItems): ?>
  <? foreach ($arItems as $arElement): ?>
    <? if (is_array($arElement)): ?>
      <? //= $arElement['DETAIL_TEXT'];?>
      <? if ($arResult["bDisplayButtons"]): ?>
        <? foreach ($arItems as $arElement): ?>
          <? if (is_array($arElement) && (!is_array($arElement["OFFERS"]) || empty($arElement["OFFERS"]))): ?>

            <? //if($arElement["CAN_BUY"]):?>
            <? if ($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])): ?>

              <form action="/personal/cart/ajax-add.php" method="post" enctype="multipart/form-data"
                    class="order-slide-content" id="order-slide-content">
                <section class="order">
                  <div class="container">
                    <h2 class="h2 white">Рассчитайте подходящий вам сервер</h2>
                    <div class="order-select">
                      <div class="row">
                        <div class="col-sm-6">
                          <? if ($arParams["USE_PRODUCT_QUANTITY"]): ?>
                            <div
                                class="order-ttl"><? echo GetMessage("CT_BCT_QUANTITY") ?></div>
                            <select name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>"
                                    id="mounth_quant">
                              <option value="1">1 месяц</option>
                              <option value="3">3 месяца</option>
                              <option value="6">6 месяцев</option>
                              <option value="12">12 месяцев</option>
                            </select>
                          <? endif; ?>
                        </div>
                        <div class="col-sm-6 os-js">
                          <?
                          foreach ($arElement["PRODUCT_PROPERTIES"] as $pid => $product_property):
                            $arProperty = $arElement["PROPERTIES"][$pid];
                            if ($arElement["PROPERTIES"][$pid]["CODE"] == "OS")
                            {
                              ?>
                              <div
                                  class="order-ttl"><? echo $arElement["PROPERTIES"][$pid]["NAME"] ?></div>
                              <?
                              if (
                                $arElement["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L" && $arElement["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
                              ):
                                ?>
                                <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                <label><input type="radio"
                                              name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]"
                                              value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"checked"' ?>><? echo $v ?>
                                </label><br>
                              <? endforeach; ?>
                              <? else: ?>
                                <select
                                    name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]"
                                    class="selectOs"
                                    id="os">
                                  <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                    <option
                                        class="os-<? echo $k ?>"
                                        value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo 'selected="selected"' ?>><? echo $v ?></option>
                                  <? endforeach; ?>
                                </select>
                              <? endif; ?>
                              <?
                            }
                          endforeach;
                          ?>
                        </div>
                      </div>
                    </div>
                    <?
                    foreach ($arElement["PRODUCT_PROPERTIES"] as $pid => $product_property):
                      if ($arElement["PROPERTIES"][$pid]["CODE"] != "OS" && $arElement["PROPERTIES"][$pid]["CODE"] != "PANEL")
                      {
                        ?>

                        <div class="property hidden-xs">
                          <div
                              class="order-slide-ttl"><? echo $arElement["PROPERTIES"][$pid]["NAME"] ?></div>
                          <?
                          if (
                            $arElement["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L" && $arElement["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
                          ):
                            ?>
                            <? foreach ($product_property["VALUES"] as $k => $v): ?>
                            <label><input type="radio"
                                          name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]"
                                          value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"checked"' ?>><? echo $v ?>
                            </label><br>
                          <? endforeach; ?>
                          <? else: ?>
                            <div class="slider-and-grath">
                              <?
                              if ($arElement["PROPERTIES"][$pid]["CODE"] == 'FTP' || $arElement["PROPERTIES"][$pid]["CODE"] == 'IP_QTY')
                              {
                                $freeFIRST = $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_STEP']['VALUE'] * $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_PRICE']['VALUE'];
                              }
                              else
                              {
                                $freeFIRST = 0;
                              }
                              ?>
                              <script type="text/javascript">
                                var os_price = 0;
                                $(function () {
                                  $("#slider<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").slider({
                                    range: "min",
                                    value: <?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_MIN']['VALUE'] ?>,
                                    min: <?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_MIN']['VALUE'] ?>,
                                    max: <?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_MAX']['VALUE'] ?>,
                                    step: <?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_STEP']['VALUE'] ?>,
                                    slide: function (event, ui) {
                                      $("#amount<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(ui.value);
                                      $("#price<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(<?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_PRICE']['VALUE'] ?> * ui.value -
                                      <?= $freeFIRST ?>)
                                      ;
                                      <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                      if (ui.value == <? echo $v ?>) {
                                        var balin = <? echo $k ?>;
                                      }
                                      <? endforeach; ?>
                                      $("#<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(balin);

                                      var os = parseInt($("#os :selected").val());
                                      if (os == 103 || os == 13) {
                                        os_price = <?= $arElement["PROPERTIES"]['OS_PRICE']['VALUE'] ?>;
                                      } else {
                                        os_price = 0;
                                      }

                                      $("#TOTALSUM").val((parseInt($("#priceCPU_QTY").val()) + parseInt($("#priceRAM").val()) + parseInt($("#priceHDD").val()) + parseInt($("#priceFTP").val()) + parseInt($("#priceIP_QTY").val()) + os_price) * parseInt($("#mounth_quant :selected").val()));
                                    }
                                  });
                                  $("#amount<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val($("#slider<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").slider("value"));
                                  $("#price<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(<?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_PRICE']['VALUE'] ?> * $("#slider<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").slider("value") -
                                  <?= $freeFIRST ?>)
                                  ;
                                  <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                  if ($("#slider<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").slider("value") == <? echo $v ?>) {
                                    var balin = <? echo $k ?>;
                                  }
                                  <? endforeach; ?>
                                  $("#<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(balin);
                                  $("#TOTALSUM").val((parseInt($("#priceCPU_QTY").val()) + parseInt($("#priceRAM").val()) + parseInt($("#priceHDD").val()) + parseInt($("#priceFTP").val()) + parseInt($("#priceIP_QTY").val())) * parseInt($("#mounth_quant :selected").val()));
                                });
                              </script>
                              <input type="hidden"
                                     id="price<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>">
                              <input type="hidden"
                                     id="<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>"
                                     name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]">

                              <div id="slider<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>"
                                   class='order-slide'></div>
                              <div class="order-slide-grath">
                                <?
                                $graphOffset = 100 / (count($product_property["VALUES"]) - 1);
                                if ($arElement["PROPERTIES"][$pid]["CODE"] == "CPU_QTY")
                                {
                                  $graphTITLE = 'Core';
                                }
                                elseif ($arElement["PROPERTIES"][$pid]["CODE"] == "IP_QTY")
                                {
                                  $graphTITLE = 'шт.';
                                }
                                else
                                {
                                  $graphTITLE = 'Gb';
                                }
                                $i = 0;
                                ?>
                                <? foreach ($product_property["VALUES"] as $k => $v): ?>
                                  <div style="left:<?= $graphOffset * $i ?>%"><? echo $v ?> <?= $graphTITLE ?></div>
                                  <?
                                  $i++;
                                endforeach; ?>
                              </div>
                            </div>

                          <? endif; ?>
                        </div>
                        <?
                        $min = intval($arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_MIN']['VALUE']);
                        $max = intval($arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_MAX']['VALUE']);
                        $step = intval($arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_STEP']['VALUE']);
                        ?>
                        <div class="property-mobile visible-xs">
                          <label for="property_<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>"
                                 class="property-mobile__label">
                            <? echo $arElement["PROPERTIES"][$pid]["NAME"] ?>
                          </label>
                          <select id="property_<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>" class="selectProperty">
                            <?
                            for ($i = $min; $i <= $max; $i = $i + $step):?>
                              <option value="<?= $i ?>"><?= $i . ' ' . $graphTITLE ?></option>
                            <? endfor; ?>
                          </select>
                          <script>
                            $('#property_<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>').on('change', function (event) {
                              var ui = {value: $(this).val()}
                              $("#amount<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(ui.value);
                              $("#price<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(<?= $arElement["PROPERTIES"][$arElement["PROPERTIES"][$pid]["CODE"] . '_PRICE']['VALUE'] ?> * ui.value -
                              <?= $freeFIRST ?>)
                              ;
                              <? foreach ($product_property["VALUES"] as $k => $v): ?>
                              if (ui.value == <? echo $v ?>) {
                                var balin = <? echo $k ?>;
                              }
                              <? endforeach; ?>
                              $("#<?= $arElement["PROPERTIES"][$pid]["CODE"] ?>").val(balin);

                              var os = parseInt($("#os :selected").val());
                              if (os == 103 || os == 13) {
                                os_price = <?= $arElement["PROPERTIES"]['OS_PRICE']['VALUE'] ?>;
                              } else {
                                os_price = 0;
                              }

                              $("#TOTALSUM").val((parseInt($("#priceCPU_QTY").val()) + parseInt($("#priceRAM").val()) + parseInt($("#priceHDD").val()) + parseInt($("#priceFTP").val()) + parseInt($("#priceIP_QTY").val()) + os_price) * parseInt($("#mounth_quant :selected").val()));
                            })
                          </script>
                        </div>
                        <?
                      }
                    endforeach;
                    ?>
                    <?
                    foreach ($arElement["PRODUCT_PROPERTIES"] as $pid => $product_property):
                      if ($arElement["PROPERTIES"][$pid]["CODE"] == "PANEL")
                      {
                        ?>
                        <?
                        if (
                          $arElement["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L" && $arElement["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
                        ):
                          ?>
                          <? foreach ($product_property["VALUES"] as $k => $v): ?>
                          <div class="order-ttl panel<?= $k ?>"><input type="checkbox"
                                                                       name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]"
                                                                       value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo 'checked="checked"' ?>
                                                                       id="PANEL<?= $k ?>"><label
                                for="PANEL<?= $k ?>"></label><? echo $arElement["PROPERTIES"][$pid]["NAME"] ?>
                          </div>
                        <? endforeach; ?>
                        <? endif; ?>
                        <?
                      }
                    endforeach;
                    ?>
                    <div class="order-bottom">
                      <div class="characteristics">
                        <div class="row">
                          <div class="col-sm-3 characteristics-item characteristics-col1">
                            <div class="img vds-cpu"></div>

                            <span>CPU 2,4 GHz</span>
                            <div class="characteristics-item__delimetr"></div>
                            <input id="amountCPU_QTY"> Core
                          </div>
                          <div class="col-sm-3 characteristics-item characteristics-col2">
                            <div class="img vds-ram"></div>
                            <span>RAM</span>
                            <div class="characteristics-item__delimetr"></div>
                            <input id="amountRAM"> Gb
                          </div>
                          <div class="col-sm-3 characteristics-item characteristics-col3">
                            <div class="img vds-hdd"></div>
                            <span>HDD SAS</span>
                            <div class="characteristics-item__delimetr"></div>
                            <input id="amountHDD"> Gb
                          </div>
                          <div class="col-sm-3 characteristics-item characteristics-col4">
                            <div class="img vds-speed"></div>
                            <span>SSD</span>
                            <div class="characteristics-item__delimetr"></div>
                            <input id="amountFTP"> Gb
                          </div>
                        </div>
                      </div>
                      <div class="order_total">
                        <div class="row">
                          <div class="col-sm-6">
                            <h2 class="h2 white">Закажи VDS сервер бесплатно!</h2>
                            <div class="order-price clearfix">
                              <? if ($arResult["bDisplayPrices"]): ?>
                                <? foreach ($arItems as $arElement): ?>
                                  <? if (is_array($arElement)): ?>
                                    <? if (is_array($arElement["OFFERS"]) && !empty($arElement["OFFERS"])): ?>
                                      <td width="<?= $arResult["TD_WIDTH"] ?>"
                                          class="data-cell" rowspan="2">
                                        <? foreach ($arElement["OFFERS"] as $arOffer): ?>
                                          <? foreach ($arParams["OFFERS_FIELD_CODE"] as $field_code): ?>
                                            <small><? echo GetMessage("IBLOCK_FIELD_" . $field_code) ?>
                                              :&nbsp;<? echo $arOffer[$field_code]; ?></small>
                                            <br/>
                                          <? endforeach; ?>
                                          <? foreach ($arOffer["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                                            <small><?= $arProperty["NAME"] ?>:&nbsp;<?
                                              if (is_array($arProperty["DISPLAY_VALUE"]))
                                              {
                                                echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
                                              }
                                              else
                                              {
                                                echo $arProperty["DISPLAY_VALUE"];
                                              }
                                              ?></small><br/>
                                          <? endforeach ?>
                                          <? foreach ($arOffer["PRICES"] as $code => $arPrice): ?>
                                            <? if ($arPrice["CAN_ACCESS"]): ?>
                                              <p><?= $arResult["PRICES"][$code]["TITLE"]; ?>
                                                :&nbsp;&nbsp;
                                                <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                                  <s><?= $arPrice["PRINT_VALUE"] ?></s>
                                                  <span
                                                      class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                                                <? else: ?>
                                                  <span
                                                      class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
                                                <? endif ?>
                                              </p>
                                            <? endif; ?>
                                          <? endforeach; ?>
                                        <? endforeach; ?>
                                      </td>
                                    <? else: ?>
                                      <div class="order-price-num">
                                        <? foreach ($arElement["PRICES"] as $code => $arPrice): ?>
                                          <? if ($arPrice["CAN_ACCESS"]): ?>
                                            <script type="text/javascript">
                                              var os_price = 0;
                                              $(document).ready(function () {
                                                var os = parseInt($("#os :selected").val());
                                                if (os == 103 || os == 13) {
                                                  os_price = <?= $arElement["PROPERTIES"]['OS_PRICE']['VALUE'] ?>;
                                                } else {
                                                  os_price = 0;
                                                }
                                                $("#os").change(function () {
                                                  var os = parseInt($("#os :selected").val());
                                                  if (os == 103 || os == 13) {
                                                    os_price = <?= $arElement["PROPERTIES"]['OS_PRICE']['VALUE'] ?>;
                                                  } else {
                                                    os_price = 0;
                                                  }

                                                });

                                                var total = (parseInt($("#priceCPU_QTY").val()) + parseInt($("#priceRAM").val()) + parseInt($("#priceHDD").val()) + parseInt($("#priceFTP").val()) + parseInt($("#priceIP_QTY").val()) + os_price) * parseInt($("#mounth_quant :selected").val());
                                                $("#summ span").text(total);

                                                var formOftype = $("#order-slide-content");
                                                formOftype.live("change", function () {

                                                  var os = parseInt($("#os :selected").val());
                                                  if (os == 103 || os == 13) {
                                                    var os_price = <?= $arElement["PROPERTIES"]['OS_PRICE']['VALUE'] ?>;
                                                  } else {
                                                    var os_price = 0;
                                                  }

                                                  var total = (parseInt($("#priceCPU_QTY").val()) + parseInt($("#priceRAM").val()) + parseInt($("#priceHDD").val()) + parseInt($("#priceFTP").val()) + parseInt($("#priceIP_QTY").val()) + os_price) * parseInt($("#mounth_quant :selected").val());
                                                  $("#TOTALSUM").val(total);
                                                });
                                              });
                                            </script>
                                            Цена:&nbsp;<input id="TOTALSUM" type="text"
                                                              name="SUM"
                                                              class="catalog-price">

                                          <? endif; ?>
                                        <? endforeach; ?>
                                        <? if (is_array($arElement["PRICE_MATRIX"])): ?>
                                          <table cellpadding="0" cellspacing="0"
                                                 border="0" width="100%"
                                                 class="data-table">
                                            <thead>
                                            <tr>
                                              <? if (count($arElement["PRICE_MATRIX"]["ROWS"]) >= 1 && ($arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)): ?>
                                                <td><?= GetMessage("CATALOG_QUANTITY") ?></td>
                                              <? endif; ?>
                                              <? foreach ($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType): ?>
                                                <td><?= $arType["NAME_LANG"] ?></td>
                                              <? endforeach ?>
                                            </tr>
                                            </thead>
                                            <? foreach ($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arQuantity): ?>
                                              <tr>
                                                <? if (count($arElement["PRICE_MATRIX"]["ROWS"]) > 1 || count($arElement["PRICE_MATRIX"]["ROWS"]) == 1 && ($arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)): ?>
                                                  <th nowrap>
                                                    <?
                                                    if (IntVal($arQuantity["QUANTITY_FROM"]) > 0 && IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                                    {
                                                      echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_FROM_TO")));
                                                    }
                                                    elseif (IntVal($arQuantity["QUANTITY_FROM"]) > 0)
                                                    {
                                                      echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], GetMessage("CATALOG_QUANTITY_FROM"));
                                                    }
                                                    elseif (IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                                    {
                                                      echo str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_TO"));
                                                    }
                                                    ?>
                                                  </th>
                                                <? endif; ?>
                                                <? foreach ($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):
                                                  ?>
                                                  <td>
                                                    <?
                                                    if ($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
                                                    {
                                                      echo '<s>' . FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . '</s> <span class="catalog-price">' . FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . "</span>";
                                                    }
                                                    else
                                                    {
                                                      echo '<span class="catalog-price">' . FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) . "</span>";
                                                    }
                                                    ?>&nbsp;
                                                  </td>
                                                <? endforeach ?>
                                              </tr>
                                            <? endforeach ?>
                                          </table>
                                        <? endif ?>
                                      </div>
                                    <? endif; ?>
                                  <? endif; ?>
                                <? endforeach ?>
                              <? endif; ?>
                              <div class="order-price-val"><span>/</span>рублей<br>в месяц</div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="test_period">
                              <? //TEST_PERIOD_14
                              if (isset($arItems[0]['PROPERTIES']['TEST_PERIOD_14']))
                              {
                                ?>
                                <input type="checkbox" value="Нет"
                                       name="<?= 'prop[' . $arItems[0]['PROPERTIES']['TEST_PERIOD_14']['CODE'] . ']' ?>"
                                       id="<?= $arItems[0]['PROPERTIES']['TEST_PERIOD_14']['CODE'] ?>">
                                <label
                                    for="<?= $arItems[0]['PROPERTIES']['TEST_PERIOD_14']['CODE'] ?>">Получить
                                  дополнительно 15 дней бесплатно</label>
                                <script>
                                  $(document).ready(function () {
                                    function test_period() {
                                      if ($("#TEST_PERIOD_14").prop("checked")) {
                                        $("#TEST_PERIOD_14").val('да');
                                      } else {
                                        $("#TEST_PERIOD_14").val('нет');
                                      }
                                    }

                                    $('#TEST_PERIOD_14').click(function () {
                                      test_period();
                                    });
                                    test_period();
                                  });
                                </script>
                                <?
                              }
                              ?>
                            </div>
                            <input type="submit" class="btn-big"
                                   name="<? echo $arParams["ACTION_VARIABLE"] . "BUY" ?>"
                                   value="<? echo GetMessage("CATALOG_BUY") ?>">
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"] ?>"
                             value="BUY">
                      <input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"] ?>"
                             value="<? echo $arElement["ID"] ?>">

                      <input type="hidden" name="type" value="vds">
                    </div>
                  </div>
                </section>
              </form>
            <? else: ?>
              <noindex>
                <a href="<? echo $arElement["BUY_URL"] ?>"
                   rel="nofollow"><? echo GetMessage("CATALOG_BUY") ?></a>
              </noindex>
            <? endif; ?>
          <? elseif ((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])): ?>
            <?= GetMessage("CATALOG_NOT_AVAILABLE") ?>
            <? //endif ?>

          <? endif; ?>
        <? endforeach ?>
      <? endif; ?>
    <? endif; ?>
  <? endforeach ?>
<?
endforeach ?>