/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 06.12.15.
 */
jQuery(document).ready(function($) {
  $(".selectBox").selectBox({ mobile: true });
});
$(document).ready(function() {
  $(".addservices").hide();
  $("#isp-form-order").hide();

  $(".btn-order").click(function() {
    $("#choise-lis").hide();
    $(".order-checked").hide();
    $("#isp-form-order").show();

    var id = $(this).attr("href");
    $("#table-list").hide();
    $(id).show();
    $(window).scrollTop($("#for-scrolling").offset().top);
 //   debugger;
    var prodqant = parseInt($(id + "quantitym").val());
    if (prodqant == 0) {
      prodqant = 1;
    }
    $(id + "quant").val(prodqant);

    var propid = parseInt($(id + "-id").val());
    var i = 0;
    while (i < prodqant) {
      $(id + "area-for-input").append(
        '<tr><td>IP адрес <span class="required-star">*</span></td><td><input name="nprop' +
          propid +
          '[]" type="text" value="" class="ipfields" placeholder="Введите IP адрес сервера"></td></tr>'
      );
      i++;
    }
  });
  $(".btn-order-mobile").click(function() {
    $("#choise-lis").hide();
    $(".order-checked").hide();
    $("#isp-form-order").show();

    var id = $(this).attr("href");
    var itemId = $(this).data("id");
    $(".item_list").hide();
    $("#pr" + itemId).show();
    $(window).scrollTop($("#for-scrolling").offset().top);

    var prodqant = parseInt($("#pr" + itemId + "quantitymobile").val());
    if (prodqant == 0) {
      prodqant = 1;
    }
    $("#pr" + itemId + "quant").val(prodqant);

    var propid = parseInt($("#pr" + itemId + "-id").val());
    var i = 0;
    while (i < prodqant) {
      $("#pr" + itemId + "area-for-input").append(
        '<tr><td>IP адрес <span class="required-star">*</span></td><td><input name="nprop' +
          propid +
          '[]" type="text" value="" class="ipfields" placeholder="Введите IP адрес сервера"></td></tr>'
      );
      i++;
    }
  });

  $(".order-checked").click(function() {
    $("#table-list").hide();
    $("#isp-form-order").show();
    $(window).scrollTop($("#for-scrolling").offset().top);
    $(".order-checked").hide();

    var idarray = 0;
    $(".period").each(function() {
      idarray = parseInt($(this).val());

      if (idarray > 0) {
        var idarrayid = 0;
        idarrayid = parseInt(
          $(this)
            .prev()
            .val()
        );
        $("#pr" + idarrayid + "quant").val(idarray);
        $("#pr" + idarrayid).show();

        var i = 0;
        while (i < idarray) {
          $("#pr" + idarrayid + "area-for-input").append(
            "<tr><td>IP адрес #" +
              (i + 1) +
              ' <span>*</span></td><td><input name="nprop' +
              idarrayid +
              '[]" type="text" value="" class="ipfields" placeholder="Введите IP адрес сервера"></td></tr>'
          );
          i++;
        }
      }
    });
  });

  $("#isp-buying").click(function(event) {
    var ipfields = 0;
    var bValid = true;

    $(".ipfields").each(function() {
      ipfields = parseInt($(this).val());

      event.preventDefault();
      if ($(this).val().length == 0) {
        $(this).addClass("error");
      } else {
        var ipformat = /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;
        if (!ipformat.test($(this).val())) {
          $(this).addClass("error");
          bValid = false;
        } else {
          $(this).removeClass("error");
        }
      }
    });

    if ($(".error")[0]) {
      if (bValid == false) {
        alert("Введите корректный IP адрес");
      } else {
        alert("Введите IP адрес");
      }
    } else {
      $("#isp-form-order").submit();
    }
  });
});
jQuery(document).ready(function($) {});
