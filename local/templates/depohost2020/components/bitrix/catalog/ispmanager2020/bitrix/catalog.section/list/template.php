<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arSections = array();
//echo var_dump($arResult["PRICES"]);
//Розничная
?>



<div class="table-block table-block-gray"> 
	<div class="table-div table-div1 table-isp">
		<div class="table-div__head">
			<div class="table-block_head_h">ISPmanager V5</div>
		</div>
		<div class="table-div__body">
			<?/*
			<div class="table-div__cell">
				<div class="flex-row jcsb aic">
					<div class="td-cell">
						<div class="isp-name">ISPmanager Lite v5 - лицензия</div>
						<div class="isp-desc">Without support</div>
					</div>
					<div class="td-cell">
						<div class="isp-period">
							Период: <b>1</b>  месяц
						</div>
					</div>
					<div class="td-cell">
						<div class="price isp-price">
							<span>900</span> Р/мес					
						</div>
					</div>
					<div class="td-cell">
						<div class="isp-actions">
							<a href="#" class="button-blue" data-toggle="modal" data-target="#isp1">Выбрать</a>
						</div>
					</div>
				</div>
			</div>
			*/?>
			<?foreach ($arResult["ITEMS"] as $arElement):?>
				<?
				$code = 'Розничная';
				$arPrice = $arElement["PRICES"][$code];
				?>
				<div class="table-div__cell" id="<?= $this->GetEditAreaId($arElement['ID']); ?>" href="<? echo $arSect['SECTION_PAGE_URL'] . $arElement["ID"] . '/'; ?>">
					<div class="flex-row jcsb aic">
						<div class="td-cell">
							<div class="isp-name"><?=$arElement['NAME']?></div>
							<div class="isp-desc"></div>
						</div>
						<div class="td-cell">
							<div class="isp-period">
								Период: <b>1</b>  месяц
							</div>
						</div>
						<div class="td-cell">
							<div class="price isp-price">
								<span><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span> Р/мес					
							</div>
						</div>
						<div class="td-cell">
							<div class="isp-actions">
								<a href="#" class="button-blue" data-toggle="modal" data-target="#isp_<?=$arElement['ID']?>">Выбрать</a>
							</div>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>

<?foreach ($arResult["ITEMS"] as $arElement):?>
	<?
	$code = 'Розничная';
	$arPrice = $arElement["PRICES"][$code];
	?>
	<div class="modal isp-modal fade" id="isp_<?=$arElement['ID']?>" tabindex="-1" role="dialog" aria-labelledby="ssl_<?=$arElement['ID']?>Label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title"><?=$arElement['NAME']?></h5>
					<p class="modal-header-desc"><?=$arElement['PREVIEW_TEXT']?></p>
				</div>
				<div class="modal-body">
					
					<div class="modal-form isp-order-form">
						<form action="/personal/cart/ajax-add.php" data-id="<?= $arElement['ID'] ?>" data-price="<?=$arPrice['DISCOUNT_VALUE']?>">
							<input name="ispmanager" value="BUY-WITH-PROPS" type="hidden">
							<input name="type" value="ispmanager" type="hidden">
							<input name="action" value="BUY" type="hidden">
							<input name="nid[]" value="<?= $arElement['ID'] ?>" type="hidden" id="pr<?= $arElement['ID'] ?>-id">
							<input name="nquantity[]" value="1" type="hidden" id="pr<?= $arElement['ID'] ?>quant">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="label-control">IP Adress</label>
										<input data-id="<?= $arElement['ID'] ?>" type="text" required class="form-control" name="nprop<?= $arElement['ID'] ?>[]" value="" placeholder=""/>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="label-control">Период</label>
										<div class="isp-period"><b>1</b> год</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="label-control">Цена</label>
										<div class="price isp-price">
											<span><?= $arPrice["DISCOUNT_VALUE"] ?></span> Р/мес					
										</div>
									</div>
								</div>
							</div>
							
							<div class="ssl-form-actions">
								<button class="blue-button" type_="submit">купить</button>
							</div>
							
							
						</form>						
					</div>
					
					
					
				</div>
			</div>
		</div>
	</div>

<?endforeach;?>



<?/*


<span id="for-scrolling"><span id="choise-lis"></span></span>
<div class="bg-blue wrap-table-v3">
  <div class="band-blue hidden-xs"></div>
  <div class="container bg-white">
    <h2 class="h1"><?= $arResult['NAME'] ?></h2>

    <form class="hidden-xs" method="get" action="/personal/cart/ajax-add.php">
      <table class="table-v3" id="table-list">
        <thead class="thead">
        <tr>
          <td><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_NAME') ?></td>
          <td><span class="icon-period"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PERIOD') ?></td>
          <td><span class="icon-price"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PRICE') ?></td>
          <td><span class="icon-order"></span><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_ORDER') ?></td>
        </tr>
        </thead>
        <?
        $rsParentSection = CIBlockSection::GetByID($arParams["SECTION_ID"]);
        if ($arParentSection = $rsParentSection->GetNext())
        {
        $arFilter = array(
          'IBLOCK_ID' => $arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
          '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
          'ACTIVE' => 'Y'
        );
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
        while ($arSect = $rsSect->GetNext())
        {
          $arSections[] = $arSect;
          ?>
          <tr class="section">
            <td colspan="4"><? echo $arSect['NAME']; ?></td>
          </tr>
          <?
          foreach ($arResult["ITEMS"] as $arElement):

            if ($arElement["~IBLOCK_SECTION_ID"] == $arSect['ID']):

              $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'],
                CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
              $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'],
                CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
              ?>
              <tr id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
                <td class="name">
                  <a
                      href="<? echo $arSect['SECTION_PAGE_URL'] . $arElement["ID"] . '/'; ?>"><?= $arElement["NAME"] ?></a>
                  <?
                  if ($arElement['PREVIEW_PICTURE']['ID'] > 0)
                  {
                    echo '<img title="' . $arElement['PREVIEW_PICTURE']['DESCRIPTION'] . '" alt="' . $arElement['PREVIEW_PICTURE']['DESCRIPTION'] . '" src="' . $arElement['PREVIEW_PICTURE']['SRC'] . '">';
                  }
                  ?>
                </td>
                <td>
                  <input name="id[]" type="hidden" value="<?= $arElement['ID'] ?>">
                  <select id="pr<?= $arElement['ID'] ?>quantitym" class="select_kam small period selectBox"
                          name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>[]">
                    <?
                    for ($i = 0; $i <= $arResult['MAX_VALUE']; $i++):?>
                      <option value="<?= $i ?>"><?= $i ?> <?= $arElement['CATALOG_MEASURE_NAME'] ?></option>
                    <? endfor; ?>
                  </select>
                  <script type="text/javascript">
                    $(document).ready(function () {
                      $("#pr<?= $arElement['ID'] ?>quantitym").change(function () {
                        <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
                        <? if ($arPrice = $arElement["PRICES"][$code]): ?>
                        var price = parseInt(<?=intval($arPrice["DISCOUNT_VALUE"])?>);
                        <? endif; ?>
                        <? endforeach; ?>

                        var quantity = parseInt($("#pr<?= $arElement['ID'] ?>quantitym").val()),
                          sum = price * quantity;

                        if ($('.list-checked-js [data-id="<?= $arElement['ID'] ?>"]').length <= 0 && quantity > 0) {
                          $('.list-checked-js').append('<li data-id="<?= $arElement['ID'] ?>"><?= $arElement['NAME'] ?></li>');
                        }
                        else if ($('.list-checked-js [data-id="<?= $arElement['ID'] ?>"]').length > 0 && quantity <= 0) {
                          $('.list-checked-js').find('[data-id="<?= $arElement['ID'] ?>"]').remove();
                        }

                        $('.catalog-price[data-id="<?= $arElement['ID'] ?>"]').text(sum + ' P');
                        $("#btn<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&type=ispmanager&id[]=<?= $arElement['ID'] ?>&quantity=" + quantity);
                      });
                    });
                  </script>
                </td>
                <td class="server-price">
                  <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
                    <? if ($arPrice = $arElement["PRICES"][$code]): ?>
                      <span data-id="<?= $arElement['ID'] ?>"
                            class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                      <? //if(!$arElement['IS_ENDLESS']):?>
                      <div class="price-one"><span class="measure">1&nbsp;<?= $arElement['CATALOG_MEASURE_NAME'] ?>
                          &nbsp;=&nbsp;</span><span class="value"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span></div>
                      <? //endif?>
                    <? else: ?>
                      &nbsp;
                    <? endif; ?>
                  <? endforeach; ?>
                </td>
                <td class="server-btn">
                  <? if (count($arResult["PRICES"]) > 0): ?>
                    <a href="#pr<?= $arElement["ID"] ?>" rel="nofollow" onclick="return false;"
                       class="btn-order btn-green sm">
                      <span class="visible-lg"><? echo GetMessage("CATALOG_BUY") ?></span>
                      <span class="visible-sm visible-md"><span class="icon-cart"></span></span>
                    </a>
                  <? endif; ?>
                </td>
              </tr>
            <?
            endif;
          endforeach;
          ?>
          <?
        }
        ?>
      </table>
      <div class="wrap-checked">
        <div class="head-block">Выбрано</div>
        <ul class="list-checked list-checked-js"></ul>
      </div>
      <a href="#" rel="nofollow" class="btn-green sm order-checked" onclick="return false;">Заказать
        выбранные</a>
      <input type="hidden" name="action" value="BUY">
      <input type="hidden" name="type" value="ispmanager">
    </form>
    <?
    }
    ?>
    <form class="visible-xs" method="get" action="/personal/cart/ajax-add.php">
      <div class="item_list">
        <?
        foreach ($arSections as $arSection)
        {
          ?>
          <div class="item_list__section"><?= $arSection['NAME'] ?></div>

          <?
          if (!empty($arResult['ITEMS']))
          {
            $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
            $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
            $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

            foreach ($arResult['ITEMS'] as $key => $arItem)
            {
              if ($arItem["~IBLOCK_SECTION_ID"] != $arSection['ID'])
              {
                continue;
              }
              $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
              $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
              $strMainID = $this->GetEditAreaId($arItem['ID']);
              ?>
              <div class="item_list__info item-m" data-id="<?= $arItem['ID'] ?>">
                <div class="item_list__info__row">
                  <div
                      class="item_list__info__row_label item_list__info__row_label_upper"><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_NAME') ?></div>
                  <div class="item_list__info__row_value"><?= $arItem["NAME"] ?></div>
                </div>
                <div class="item_list__info__row">
                  <div
                      class="item_list__info__row_label item_list__info__row_label_upper item_list__info__row_label_middle"><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PERIOD') ?></div>
                  <div class="item_list__info__row_value">
                    <input name="id[]" type="hidden" value="<?= $arItem['ID'] ?>">
                    <select id="pr<?= $arItem['ID'] ?>quantitymobile" class="select_kam small period selectBox"
                            name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>[]">
                      <?
                      for ($i = 0; $i <= $arResult['MAX_VALUE']; $i++):?>
                        <option value="<?= $i ?>"><?= $i ?> <?= $arElement['CATALOG_MEASURE_NAME'] ?></option>
                      <? endfor; ?>
                    </select>

                  </div>
                </div>
                <div class="item_list__info__row">
                  <div
                      class="item_list__info__row_label item_list__info__row_label_upper"><?= GetMessage('CMP_TPL_CATSEC_TABLE_HEAD_PRICE') ?></div>
                  <div class="item_list__info__row_value item_list__info__row_value_price">
                    <div class="catalog-price-js">
                      <?= str_replace(' ', '&nbsp;', number_format($arItem['MIN_PRICE']['VALUE'], 0, '.', ' ')) . ' Р' ?>
                    </div>
                    <div class="price-one"><span class="measure">1&nbsp;<?= $arElement['CATALOG_MEASURE_NAME'] ?>
                        &nbsp;=&nbsp;</span><span
                          class="value"><?= str_replace(' ', '&nbsp;', number_format($arItem['MIN_PRICE']['VALUE'], 0, '.', ' ')) . ' Р' ?></span>
                    </div>
                  </div>
                </div>
                <div class="item_list__info__row">
                  <div class="item_list__info__row_label item_list__info__row_label_upper"></div>
                  <div class="item_list__info__row_value">
                    <a href="#pr<?= $arItem["ID"] ?>m" data-id="<?= $arItem["ID"] ?>" rel="nofollow"
                       onclick="return false;"
                       class="btn-order-mobile btn-green sm"><? echo GetMessage("CATALOG_BUY") ?></a>
                    <script type="text/javascript">
                      $(document).ready(function () {
                        $("#pr<?= $arItem['ID'] ?>quantitymobile").change(function () {
                          var price = parseInt(<?=intval($arItem['MIN_PRICE']['VALUE'])?>);

                          var quantity = parseInt($("#pr<?= $arItem['ID'] ?>quantitymobile").val()),
                            sum = price * quantity;

                          if ($('.list-checked-js-m [data-id="<?= $arItem['ID'] ?>"]').length <= 0 && quantity > 0) {
                            $('.list-checked-js-m').append('<li data-id="<?= $arItem['ID'] ?>"><?= $arItem['NAME'] ?></li>');
                          }
                          else if ($('.list-checked-js-m [data-id="<?= $arItem['ID'] ?>"]').length > 0 && quantity <= 0) {
                            $('.list-checked-js-m').find('[data-id="<?= $arItem['ID'] ?>"]').remove();
                          }

                          $('.item-m[data-id="<?= $arItem['ID'] ?>"] .catalog-price-js').text(sum + ' P');
                          $("#btn<?= $arItem['ID'] ?>m").attr("href", "/personal/cart/ajax-add.php?action=BUY&type=ispmanager&id[]=<?= $arElement['ID'] ?>&quantity=" + quantity);
                        });
                      });
                    </script>
                  </div>
                </div>

              </div>

              <?
            }//foreach ($arResult['ITEMS'] as $key => $arItem)
          }//if (!empty($arResult['ITEMS']))
        }
        ?>
      </div>
      <div class="row">
        <div class="col-sm-6 wrap-checked">
          <div class="head-block">Выбрано</div>
          <ul class="list-checked list-checked-js-m"></ul>
        </div>
        <div class="col-sm-6">
          <a href="#" rel="nofollow" class="btn-green sm order-checked" onclick="return false;" >Заказать
            выбранные</a>
        </div>
      </div>

      <input type="hidden" name="action" value="BUY">
      <input type="hidden" name="type" value="ispmanager">

      <input value="Заказать выбранные" type="submit" class="btn order-selected" style="display:none;">
    </form>
    <form method="get" action="/personal/cart/ajax-add.php" id="isp-form-order">
      <input name="ispmanager" value="BUY-WITH-PROPS" type="hidden">
      <input name="type" value="ispmanager" type="hidden">
      <input name="action" value="BUY" type="hidden">
      <?
      $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT");
      $arFilter = Array("IBLOCK_ID" => 29, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
      $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 5000), $arSelect);
      while ($ob = $res->GetNextElement())
      {
        $arFields = $ob->GetFields();
        ?>
        <div id="pr<?= $arFields['ID'] ?>" class="addservices">
          <table class="table-v3">
            <tr class="section">
              <td><?= $arFields['NAME'] ?></td>
              <td>Цена руб./шт.</td>
            </tr>

            <tr>
              <td><?= $arFields['PREVIEW_TEXT'] ?></td>
              <td class="server-price">
                <?
                // Вывод списка доступных для просмотра цен товара $PRODUCT_ID
                $arProduct = GetCatalogProduct($arFields['ID']);
                $arPrice = GetCatalogProductPriceList($arFields['ID'], "SORT", "ASC");
                $bCanBuy = false;
                for ($i = 0; $i < count($arPrice); $i++)
                {
                  if ($arPrice[$i]["CAN_ACCESS"] == "Y")
                  {
                    if ($arPrice[$i]["CAN_BUY"] == "Y" && (IntVal($arProduct["QUANTITY"]) > 0 || $arProduct["QUANTITY_TRACE"] != "Y"))
                    {
                      $bCanBuy = true;
                    }
                    $productPRICE = FormatCurrency($arPrice[$i]["PRICE"], $arPrice[$i]["CURRENCY"]);
                    echo $productPRICE;
                  }
                }
                ?>
                <input name="nid[]" value="<?= $arFields['ID'] ?>" type="hidden" id="pr<?= $arFields['ID'] ?>-id">
                <input name="nquantity[]" value="quantity" type="hidden" id="pr<?= $arFields['ID'] ?>quant">
              </td>
            </tr>
          </table>
          <table class="table-v3">
            <thead>
            <tr class="section">
              <td colspan="2">
                Обязательные поля для ввода IP адресов в зависимости, сколько лицензий заказано
              </td>
            </tr>
            </thead>
            <tbody id="pr<?= $arFields['ID'] ?>area-for-input">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            </tbody>
          </table>
        </div>
        <?
      }
      ?>
      <a href="#" class="btn-green sm" rel="nofollow" onclick="return false;" id="isp-buying">Заказать</a>
    </form>

  </div>
</div>

<script type="text/javascript">

</script>

<?
/**/?>