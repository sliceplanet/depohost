<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CJSCore::Init(array("fx"));

?>

<div class="filter">
  <div class="filter__head">
    <? echo GetMessage("CT_BCSF_FILTER_TITLE") ?>
  </div>
  <div class="filter__properties">
      <form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
            action="<? echo htmlspecialchars($arResult["FORM_ACTION"]) ?>" method="get" class="smartfilter">
        <? foreach ($arResult["HIDDEN"] as $arItem): ?>
          <input
              type="hidden"
              name="<? echo $arItem["CONTROL_NAME"] ?>"
              id="<? echo $arItem["CONTROL_ID"] ?>"
              value="<? echo $arItem["HTML_VALUE"] ?>"
          >
        <? endforeach; ?>
        <?
        $key = \CUsefull::getKeyByFieldValue($arResult["ITEMS"], 'CODE', 'PLUS_DOMEN');
        if ($key >= 0)
        {
          $arItem = $arResult["ITEMS"][$key];
          unset($arResult["ITEMS"][$key]);
          ?>
          <div class="filter__radio_wrapper">
            <label><input name="PLUS_DOMEN" type="radio" value="clean_all"
                          checked="checked"><span>Все</span></label>
            <?
            foreach ($arItem["VALUES"] as $val => $arValue):?>
              <label class="filter__property <?= $arValue["URL_ID"] ?>">
                <input type="radio"
                       name="PLUS_DOMEN"
                       value="<?= $arValue["URL_ID"] ?>">
                <span><?= $arValue["VALUE"] ?></span>
              </label>
            <?endforeach; ?>
          </div>
          <?
        }
        ?>
        <div class="filter__checkbox_wrapper">

          <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <? if ($arItem["PROPERTY_TYPE"] == "N"): ?>
              <?
              if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
              {
                continue;
              }
              ?>
            <? elseif (!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])): ?>
              <label class="filter__property <?= strtolower($arItem['CODE']) ?>"><input type="checkbox"
                                                                                        name="<?= $arItem['CODE'] ?>" value="<?= strtolower($arItem['CODE']) ?>"><span><?= $arItem['NAME'] ?></span></label>
            <? endif; ?>
          <? endforeach; ?>
        </div>
      </form>
  </div>
</div>