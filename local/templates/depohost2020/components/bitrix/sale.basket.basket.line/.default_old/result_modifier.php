<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

if (IntVal($arResult["NUM_PRODUCTS"]) > 0)
{
    if (CModule::IncludeModule("sale"))
    {
        $arBasketItems = array();
        $dbBasketItems = CSaleBasket::GetList(
                        array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                        ), array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                        ), false, false, array("ID", "QUANTITY", "PRICE")
        );
        $quantity = 0;
        while ($arItems = $dbBasketItems->Fetch())
        {
            $quantity += $arItems['QUANTITY'];
            if (strlen($arItems["CALLBACK_FUNC"]) > 0)
            {
                CSaleBasket::UpdatePrice($arItems["ID"], $arItems["QUANTITY"]);
                $arItems = CSaleBasket::GetByID($arItems["ID"]);
            }
            $arBasketItems[] = $arItems;
        }
        $arResult['PRODUCTS_QUANTITY'] = $quantity;
    }
}
?>