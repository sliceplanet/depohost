<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="sale-basket-basket-line-container">
    <?
    $frame = $this->createFrame('sale-basket-basket-line-container')->begin();
    if (IntVal($arResult["NUM_PRODUCTS"]) > 0)
    {
        ?>
        <a href="<?= $arParams["PATH_TO_BASKET"] ?>"><?= GetMessage("IN_SHOPPING_CART") ?>&nbsp;&nbsp;</a><?= $arResult["PRODUCTS_QUANTITY"]; ?>&nbsp;шт.&nbsp;на&nbsp;<?= $arResult["TOTAL_PRICE"] ?>
        <?
    } else
    {
        ?>
        <div class="empty-basket">
            <?= GetMessage("SALE_NO_ITEMS") ?>
        </div>
        <?
    }

    $frame->end();
    ?>
</div>
