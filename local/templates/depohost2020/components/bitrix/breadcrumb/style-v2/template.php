<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";
	
$strReturn = '<div class="bx_breadcrumbs hidden-xs"><div class="container"><ul itemscope itemtype="http://schema.org/BreadcrumbList">';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	
	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
		$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemtype="http://schema.org/Thing" itemprop="item" href="'.$arResult[$index]["LINK"].'" title="'.$title.'"><span itemprop="name">'.$title.'</span></a>';
	else
		$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemtype="http://schema.org/Thing" itemprop="item"  href="'.$GLOBALS['APPLICATION']->GetCurPageParam('',array('clear_cache','logout')).'" ><span itemprop="name">'.$title.'</span></a>';
	$strReturn.= '<meta itemprop="position" content="'.($index+1).'" />';
	$strReturn.= '</li>';
}

$strReturn .= '</ul></div></div>';

return $strReturn;
?>