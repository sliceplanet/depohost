<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
//printr($arResult['ITEMS']);
$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0):
  ?>
  <div id="basket_items_list">
    <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="basket_form" id="basket_form">

      <table class="server hidden-xs" id="basket_items">
        <thead>
        <tr>
          <?
          foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
            $arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
            if ($arHeader["name"] == '')
            {
              $arHeader["name"] = GetMessage("SALE_" . $arHeader["id"]);
            }
            $arHeaders[] = $arHeader["id"];

            // remember which values should be shown not in the separate columns, but inside other columns
            if (in_array($arHeader["id"], array("TYPE")))
            {
              $bPriceuType = true;
              continue;
            }
            elseif ($arHeader["id"] == "PROPS")
            {
              $bPropsColumn = true;
              continue;
            }
            elseif ($arHeader["id"] == "DELAY")
            {
              $bDelayColumn = true;
              continue;
            }
            elseif ($arHeader["id"] == "DELETE")
            {
              $bDeleteColumn = true;
              $arDeleteHeader = $arHeader;
              continue;

            }
            elseif ($arHeader["id"] == "WEIGHT")
            {
              $bWeightColumn = true;
            }

            if ($arHeader["id"] == "NAME"):
              ?>
              <th class="item" id="col_<?= $arHeader["id"]; ?>">
            <?
            elseif ($arHeader["id"] == "PRICE"):
              ?>
              <th class="price" id="col_<?= $arHeader["id"]; ?>">
            <?
            else:
              ?>
              <th class="custom" id="col_<?= $arHeader["id"]; ?>">
            <?
            endif;
            ?>
            <?= $arHeader["name"]; ?>
            </th>
          <?
          endforeach;

          if ($bDelayColumn):
            ?>
            <th class="custom"></th>
          <?
          endif;
          if ($bDeleteColumn):
            ?>
            <th width="100" class="custom" id="col_<?= $arDeleteHeader["id"]; ?>"> <?= $arDeleteHeader["name"]; ?></th>
          <?
          endif;
          ?>
        </tr>
        </thead>

        <tbody>
        <?
        foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

          if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
            ?>
            <tr id="<?= $arItem["ID"] ?>">
              <?
              foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE",
                                                    "TYPE"))) // some values are not shown in the columns in this template
                {
                  continue;
                }

                if ($arHeader["id"] == "NAME"):
                  ?>
                  <td class="item">
                    <h2 class="bx_ordercart_itemtitle">
                      <?= $arItem["NAME"] ?>
                    </h2>
                    <div class="bx_ordercart_itemart">
                      <?
                      if ($bPropsColumn):
                        foreach ($arItem["PROPS"] as $val):

                          if (is_array($arItem["SKU_DATA"]))
                          {
                            $bSkip = false;
                            foreach ($arItem["SKU_DATA"] as $propId => $arProp)
                            {
                              if ($arProp["CODE"] == $val["CODE"])
                              {
                                $bSkip = true;
                                break;
                              }
                            }
                            if ($bSkip)
                            {
                              continue;
                            }
                          }

                          echo $val["NAME"] . ":&nbsp;<span>" . $val["VALUE"] . "<span><br/>";
                        endforeach;
                      endif;
                      ?>
                    </div>
                  </td>
                <?
                elseif ($arHeader["id"] == "QUANTITY"):
                  ?>
                  <td class="custom">
                    <?= $arItem["QUANTITY"] . '&nbsp;' . $arItem['MEASURE_TEXT']; ?>
                    <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>" name="QUANTITY_<?= $arItem['ID'] ?>"
                           value="<?= $arItem["QUANTITY"] ?>"/>
                  </td>
                <? elseif ($arHeader["id"] == "PRICE"):
                  ?>
                  <td class="price">
                    <div class="current_price" id="current_price_<?= $arItem["ID"] ?>">
                      <?= str_replace(" ", "&nbsp;", $arItem["PRICE_FORMATED"]) ?>
                    </div>
                  </td>
                <?
                elseif ($arHeader["id"] == "DISCOUNT"):
                  ?>
                  <td class="custom">
                    <span><?= $arHeader["name"]; ?>:</span>
                    <div
                        id="discount_value_<?= $arItem["ID"] ?>"><?= $arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] ?></div>
                  </td>
                <?
                elseif ($arHeader["id"] == "WEIGHT"):
                  ?>
                  <td class="custom">
                    <span><?= $arHeader["name"]; ?>:</span>
                    <?= $arItem["WEIGHT_FORMATED"] ?>
                  </td>
                <?
                elseif ($arHeader["id"] == "SUM"):
                  ?>
                  <td class="price">
                    <div class="current_price" id="current_sum_<?= $arItem["ID"] ?>">
                      <?= str_replace(" ", "&nbsp;", $arItem["SUM"]) ?>
                    </div>
                  </td>
                <?
                else:
                  ?>
                  <td class="custom">
                    <span><?= $arHeader["name"]; ?>:</span>
                    <?
                    if ($arHeader["id"] == "SUM"):
                    ?>
                    <div id="sum_<?= $arItem["ID"] ?>">
                      <?
                      endif;

                      echo $arItem[$arHeader["id"]];

                      if ($arHeader["id"] == "SUM"):
                      ?>
                    </div>
                  <?
                  endif;
                  ?>
                  </td>
                <?
                endif;
              endforeach;

              if ($bDelayColumn || $bDeleteColumn):
                ?>
                <td class="control">
                  <?
                  if ($bDeleteColumn):
                    ?>
                    <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>"><?= GetMessage("SALE_DELETE") ?></a>
                    <br/>
                  <?
                  endif;
                  if ($bDelayColumn):
                    ?>
                    <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delay"]) ?>"><?= GetMessage("SALE_DELAY") ?></a>
                  <?
                  endif;
                  ?>
                </td>
              <?
              endif;
              ?>
            </tr>
          <?
          endif;
        endforeach;
        ?>
        </tbody>
      </table>
      <input type="hidden" name="BasketOrder" value="BasketOrder"/>
      <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
      <input type="hidden" id="offers_props" value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
      <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
      <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
      <input type="hidden" id="count_discount_4_all_quantity"
             value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
      <input type="hidden" id="price_vat_show_value"
             value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
      <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
      <input type="hidden" id="coupon_approved" value="N"/>
      <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>

    </form>
  </div>
  <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="basket_form" id="basket_form_mobile">
    <div class="cart-mob__wrap visible-xs">
      <?
      foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

        if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
          ?>
          <div class="cart-mob__item">
            <div class="cart-mob__title"><?=$arItem['NAME']?></div>
            <?
            foreach ($arItem["PROPS"] as $val):

              if (is_array($arItem["SKU_DATA"]))
              {
                $bSkip = false;
                foreach ($arItem["SKU_DATA"] as $propId => $arProp)
                {
                  if ($arProp["CODE"] == $val["CODE"])
                  {
                    $bSkip = true;
                    break;
                  }
                }
                if ($bSkip)
                {
                  continue;
                }
              }
              ?>
              <div class="cart-mob__block">
                <span class="cart-mob__block-title"><?=$val["NAME"]?>:</span>
                <span class="cart-mob__block-value"><?=$val["VALUE"]?></span>
              </div>
              <?
            endforeach;
            ?>

            <div class="cart-mob__block">
              <span class="cart-mob__block-title">Цена:</span>
              <span class="cart-mob__block-value"><?=$arItem["PRICE_FORMATED"]?></span>
            </div>
            <div class="cart-mob__block">
              <span class="cart-mob__block-title">Кол-во:</span>
              <span class="cart-mob__block-value">
                <?= $arItem["QUANTITY"] . ' ' . $arItem['MEASURE_TEXT']; ?>
                <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>" name="QUANTITY_<?= $arItem['ID'] ?>"
                       value="<?= $arItem["QUANTITY"] ?>"/>
              </span>
            </div>

            <div class="cart-mob__block">
              <span class="cart-mob__block-title">Сумма:</span>
              <span class="cart-mob__block-value"><?=$arItem["SUM"]?></span>
            </div>
            <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>" class="cart-mob-del"><?= GetMessage("SALE_DELETE") ?></a>
          </div>
        <?endif;
      endforeach; ?>

    </div>
    <input type="hidden" name="BasketOrder" value="BasketOrder"/>
    <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
    <input type="hidden" id="offers_props" value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
    <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
    <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
    <input type="hidden" id="count_discount_4_all_quantity"
           value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="price_vat_show_value"
           value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="coupon_approved" value="N"/>
    <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>
  </form>

  <div class="bx_ordercart_order_pay">


    <div class="bx_ordercart_order_pay_right">
      <table class="bx_ordercart_order_sum">
        <? if ($bWeightColumn): ?>
          <tr>
            <td class="custom_t1"><?= GetMessage("SALE_TOTAL_WEIGHT") ?></td>
            <td class="custom_t2" id="allWeight_FORMATED"><?= $arResult["allWeight_FORMATED"] ?></td>
          </tr>
        <? endif; ?>
        <? if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"): ?>
          <tr>
            <td><? echo GetMessage('SALE_VAT_EXCLUDED') ?></td>
            <td id="allSum_wVAT_FORMATED"><?= $arResult["allSum_wVAT_FORMATED"] ?></td>
          </tr>
          <tr>
            <td><? echo GetMessage('SALE_VAT_INCLUDED') ?></td>
            <td id="allVATSum_FORMATED"><?= $arResult["allVATSum_FORMATED"] ?></td>
          </tr>
        <? endif; ?>

        <tr>
          <td class="fwb"><?= GetMessage("SALE_TOTAL") ?></td>
          <td class="fwb" id="allSum_FORMATED">
                        <span id="total_counted_cost"><?
                          echo str_replace(" ", "&nbsp;", number_format($arResult["allSum"], 0, ',', ' '));
                          ?></span>&nbsp;<?= GetMessage('RUB') ?>
          </td>
        </tr>
        <tr>
          <td class="custom_t1"></td>
          <td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
            <? if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0): ?>
              <?= $arResult["PRICE_WITHOUT_DISCOUNT"] ?>
            <? endif; ?>
          </td>
        </tr>

      </table>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>

    <div class="bx_ordercart_order_pay_center">

      <? if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0): ?>
        <?= $arResult["PREPAY_BUTTON"] ?>
        <span><?= GetMessage("SALE_OR") ?></span>
      <? endif; ?>
      <a href="/personal/order/getoffer/" class="btn getoffer"><?= GetMessage("SALE_GET_OFFER") ?></a>

      <a href="javascript:void(0)" onclick="checkOut();" class="btn checkout"><?= GetMessage("SALE_ORDER") ?></a>
    </div>
  </div>
<?
else:
  ?>
  <div id="warning_message">
    <?
    echo ShowError(GetMessage("SALE_NO_ITEMS"));
    ?>
  </div>
<?
endif;
?>