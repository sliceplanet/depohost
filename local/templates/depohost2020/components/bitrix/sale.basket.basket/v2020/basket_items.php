<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
function declOfNum($num, $titles) {
    $cases = array(2, 0, 1, 1, 1, 2);

    return $num . " " . $titles[($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)]];
}
//printr($arResult['ITEMS']);
$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

$totaltotal = 0;

if ($normalCount > 0):
  ?>
  
  
  
<div class="cart-items">

	<div class="cart-items__wrapper">
	
        <?
		$items = $arResult["GRID"]["ROWS"];
		foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

			if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				
				$total = $arItem['PRICE']* $arItem['QUANTITY'];
				if(!$props['is-configurator'] && !$props['is_solution']):
					$total_row = $arItem['PRICE']*$arItem['QUANTITY'];
				else:
					$total_row = $arItem['PRICE'];
				endif;
				
				$props = [];
				foreach($arItem['PROPS'] as $v):
					$props[$v['CODE']] = $v['VALUE'];
				endforeach;
				
				if(!$props['MONTHS']) $props['MONTHS'] = 1;
				
			
				
		//		echo var_dump($props);
				
				foreach($items as $k1 => $item):
				
					$props2 = [];
					foreach($item['PROPS'] as $v):
						$props2[$v['CODE']] = $v['VALUE'];
					endforeach;
					
					if($props2['unique_id'] != $props['unique_id'] || !$props2['is_service']) continue;
					
				/*	if($props['is_service'])
						$total += $item['PRICE'] * $item['QUANTITY']/ $props['MONTHS'];
					else
						$total += $item['PRICE'] * $item['QUANTITY'] * $props['MONTHS'];
				*/
				endforeach;
		//		echo var_dump($props);
				
				$totaltotal += $total;
				
				if($props['is_service']) continue;
				
								
				if($props['is-configurator']):
					if($props['price_1month']):
						$price_1month = $props['price_1month'];
						$sum = $price_1month;
						$arResult['DISCOUNTS'] = unserialize(\Bitrix\Main\Config\Option::get('dedicated', 'discounts_values'));

						// Суммы и скидки по периодам
						$arSum = array(
						'1m' => $sum,
						'3m' => round($sum * 3 - $sum * 3 * $arResult['DISCOUNTS']['CALC_3M'] / 100),
						'6m' => round($sum * 6 - $sum * 6 * $arResult['DISCOUNTS']['CALC_6M'] / 100),
						'12m' => round($sum * 12 - $sum * 12 * $arResult['DISCOUNTS']['CALC_12M'] / 100),
						);
						$arDiscount = array(
						'1m' => $sum - $arSum['1m'],
						'3m' => 3 * $sum - $arSum['3m'],
						'6m' => 6 * $sum - $arSum['6m'],
						'12m' => 12 * $sum - $arSum['12m'],
						);

					endif;				
				endif;
			
		?>
				<div class="cart-item collapse-parent opened" data-id="<?=$arItem['ID']?>">
					<div class="flex-row jcsb">
						<div class="cart-item__col1 collapse-buttton_block">
							<div class="cart-item__info collapse-arrow_icon_block">
								<i></i>
								<div>
									<?if($props['is-configurator']):?>
										<div class="server-code"><?=$props['CPU']?></div>
									<?elseif($props['DOMAIN_NAME']):?>
										<div class="server-code"><?=$props['DOMAIN_NAME']?></div>
									<?endif;?>
									<div class="server-name"><?=$arItem['NAME']?></div>
									<div class="server-desc">
										<?if($props['CONFIGURATION']):?>
											<?=$props['CONFIGURATION']?> 
										<?endif;?>
										
										<?if($props['OS']):?>
											<?=$props['OS']?> 
										<?endif;?>
										<?if($props['CPU_QTY']):?>
											<?=$props['CPU_QTY']?> 
										<?endif;?>
										<?if($props['FTP']):?>
											FTP:<?=$props['RAM']?> 
										<?endif;?>
										<?if($props['IP_QTY']):?>
											IP:<?=$props['IP_QTY']?> 
										<?endif;?>
										
										<?if($props['RAM']):?>
											RAM:<?=$props['RAM']?> 
										<?endif;?>
										<?if($props['HDD']):?>
											HDD:<?=$props['HDD']?> 
										<?endif;?>
										<?if($props['RAID']):?>
											RAID:<?=$props['RAID']?> 
										<?endif;?>
										<?if($props['SSD']):?>
											SSD:<?=$props['SSD']?> 
										<?endif;?>
										<?if($props['domain']):?>
											<?=$props['domain']?>  
										<?endif;?>
										<?if($props['domain'] && $props['years']):?>
											на <?=declOfNum($props['years'],['год','года','лет'])?>
										<?endif;?>
										<?if(count($props) == 1 && $props['MONTHS']):?>
											<?=$props['MONTHS']?>
										<?endif;?>
										
									</div>
								</div>
							</div>
						</div>
						<div class="cart-item__col2">
							<div class="cart-item__colh">стоимость</div>
							<div class="cart-item__price" data-price="<?=$arItem['PRICE']?>">
								<?if($props['unique_id']):?>
									<?=number_format(round($arItem['PRICE']/$props['MONTHS']),0,'.',' ')?> <span><span class="rub"></span>/мес</span>
								<?elseif($props['DOMAIN_NAME']):?>
									<?=number_format($arItem['PRICE'],0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?elseif($props['years']):?>
									<?=number_format(round($arItem['PRICE']/$props['years']),0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?elseif($props['MONTHS']):?>
									<?
									$m = (int)$props['MONTHS'];
									?>
									<?=number_format(round($arItem['PRICE']/$m),0,'.',' ')?> <span><span class="rub"></span>/мес</span>
								<?else:?>
									<?=number_format($arItem['PRICE'],0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?endif;?>
							</div>
						</div>
						<div class="cart-item__col3">
							<?if($props['is-ms']):?>
								<div class="cart-item__colh">количество</div>
								<div class="counter_block__wrapper">
									<div class="counter_block__actions">
										<a href="#" class="cb_minus">-</a>
										<input type="text" value="<?=$arItem['QUANTITY']?>">
										<a href="#" class="cb_plus">+</a>
									</div>
								</div>
							<?endif;?>
						</div>
						<div class="cart-item__col4">
							<?if($props['is-configurator'] || $props['is_solution'] || $arItem['NAME'] == 'Аренда VDS сервера'):?>
							<div class="cart-item__colh">период</div>
							<select name="period[]">
								<option value="1"<?if($props['MONTHS'] == 1):?> selected<?endif;?>>1 месяц</option>
								<option value="3"<?if($props['MONTHS'] == 3):?> selected<?endif;?>>3 месяца</option>
								<option value="6"<?if($props['MONTHS'] == 6):?> selected<?endif;?>>6 месяцев</option>
								<option value="12"<?if($props['MONTHS'] == 12):?> selected<?endif;?>>12 месяцев</option>
							</select>
							<?endif;?>
						</div>
						<div class="cart-item__col5">
							<div class="cart-item__colh"></div>
							<div class="cart-item__total_price">
								<div class="item__total_price_price" data-price="<?=$total_row?>">
									<?=number_format($total_row,0,'.',' ')?>  <span><span class="rub"></span></span>
								</div>
								<?if($props['is-configurator'] || $props['is_solution']):?>
									<div class="item__total_price_economy">
										Экономия за сервер <?=$arDiscount[(int)$props['MONTHS'].'m']?> Р
									</div>
								<?endif;?>
							</div>
						</div>
						<div class="cart-item__col6">
							<a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>" class="cart-delete"></a>
						</div>
					</div>
					<div class="cart-detalization-items collapse-content">
						<table>
							
							<tr data-id="<?=$arItem['ID']?>" data-price="<?=($arItem['PRICE'])?>" data-months="<?=$props['MONTHS'] ?: 1?>"<?if($props['is-configurator']):?> style="display:none;"<?endif;?>>
								<td><?=$arItem['NAME']?></td>
								<?
								if(!$props['MONTHS']) $props['MONTHS'] = 1;
								?>
								<td class="td-price det-price-main" data-price="<?=round($arItem['PRICE'])?>"><?=number_format(round($arItem['PRICE']*$arItem['QUANTITY']/$props['MONTHS']),0,'.',' ')?> <span><span class="rub"></span>/мес</span></td>
							</tr>
						
							
							<?if($arItem['NAME'] == 'Аренда VDS сервера' && $props['TEST_PERIOD_14'] == 'да'):?>
								<tr>
									<td>Дополнительно 15 дней</td>
									<td class="null-price">
										0 <span><span class="rub"></span></span>
									</td>
								</tr>
							<?endif;?>
							<?foreach($items as $k1 => $item):?>
							<?
								$props2 = [];
								foreach($item['PROPS'] as $v):
									$props2[$v['CODE']] = $v['VALUE'];
								endforeach;
							//	echo var_dump($props2).'<br><br>';;
								
								if($props2['unique_id'] != $props['unique_id'] || !$props2['is_service']) continue;
								$counter_products = [3015,5313,5339,5370,5380];
								
								$props['MONTHS'] = $props['MONTHS'] ?: 1;
								
								$total += $item['PRICE']*$item['QUANTITY'];
								
							?>
								<tr data-id="<?=$item['ID']?>" data-price="<?=$item['PRICE']?>" data-months="<?=(int)$props['MONTHS']?>">
									<td <?if(in_array($item['PRODUCT_ID'],$counter_products)):?> class="counter-product"<?endif;?>>
										<div class="sub-flex-row flex-row">
											<div class="sub-cart-item-name">
												<?=$item['NAME']?>
											</div>
											<?if(in_array($item['PRODUCT_ID'],$counter_products)):?>
												<div class="price-for-1-div">
													<div>
														<div class="cart-item__colh">стоимость</div>
														<div class="price-for-1">
															<?=number_format($item['PRICE']/$props['MONTHS'],0,'.',' ')?>  <span><span class="rub"></span>/мес</span>
														</div>
													</div>
												</div>
												<div class="sub_counter">
													<div class="cart-item__colh">количество</div>
													<div class="counter_block__wrapper">
														<div class="counter_block__actions">
															<a href="#" class="cb_minus">-</a>
															<input type="text" value="<?=$item['QUANTITY']?>">
															<a href="#" class="cb_plus">+</a>
														</div>
													</div>
												</div>
											<?endif;?>
										</div>
									</td>
									<td data-price="<?=$item['PRICE']*$item['QUANTITY']?>" class="td-price<?if(!(int)$item['PRICE']):?> null-price<?endif;?>">
										<?=number_format($item['PRICE']*$item['QUANTITY'],0,'.',' ')?> <span><span class="rub"></span>/<?=(int)$props['MONTHS']?> мес</span>
									</td>
									<td>
										<a href="<?= str_replace("#ID#", $item["ID"], $arUrls["delete"]) ?>" class="cart-delete"></a>
									</td>
								</tr>
							<?endforeach;?>
						</table>
						<div class="cart-detalization-bottom">
							<?if($props['is-configurator']):?>
								<a href="/arenda-dedicated_server/?bid=<?=$arItem['ID']?>" class="cart-change-conf">Изменить конфигурацию</a>
							<?endif;?>
							
							<div class="cdb-total">
								Итого: <span class="cdb-total-price" data-price="<?=$total?>"><?=number_format($total,0,'.',' ')?>  <span><span class="rub"></span></span></span>
							</div>
							
						</div>
					</div>
				</div>		
		<?
			endif;
		endforeach;
		?>
		
	
	</div>
	
</div>
<div id="cart-total-line">
	Итого: 
	<span id="cart-total-price"><?=number_format($totaltotal,0,'.',' ')?></span>
	<span id="cart-total-currency">Р</span>
</div>
  
<div class="cart-actions">
	<a href="/personal/order/make/" onclick_="checkOut();" class="button-blue2">Оформить заказ</a>
	<a href="/personal/order/getoffer/" class="getoffer">Получить коммерческое предложение</a>
</div>
  
  
<?
else:
  ?>
  <div id="warning_message">
    <?
    echo ShowError(GetMessage("SALE_NO_ITEMS"));
    ?>
  </div>
<?
endif;
?>