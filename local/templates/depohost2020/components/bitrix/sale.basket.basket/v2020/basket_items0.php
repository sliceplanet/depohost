<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
//printr($arResult['ITEMS']);
$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

$totaltotal = 0;

if ($normalCount > 0):
  ?>
  
  
  
<div class="cart-items">

	<div class="cart-items__wrapper">
	
        <?
		$items = $arResult["GRID"]["ROWS"];
		foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

			if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				
				$total = $arItem['PRICE']* $item['QUANTITY'];
				$total_row = $arItem['PRICE'];
				
				$props = [];
				foreach($arItem['PROPS'] as $v):
					$props[$v['CODE']] = $v['VALUE'];
				endforeach;
				
				if(!$props['is_configurator'] && !$props['is_solution']):
					$total_row *= $arItem['QUANTITY'];
				endif;
				
		//		echo var_dump($props);
				
				foreach($items as $k1 => $item):
				
					$props2 = [];
					foreach($item['PROPS'] as $v):
						$props2[$v['CODE']] = $v['VALUE'];
					endforeach;
					
					if($props2['unique_id'] != $props['unique_id'] || !$props2['is_service']) continue;
					
					$total += $item['PRICE'] * $item['QUANTITY'] * $props['MONTHS'];
				
				endforeach;
			//	echo var_dump($props);
				
				if($props['is_service']) continue;
				
				$totaltotal += $total;
			
		?>
				<div class="cart-item collapse-parent opened" data-id="<?=$arItem['ID']?>">
					<div class="flex-row jcsb">
						<div class="cart-item__col1 collapse-buttton_block">
							<div class="cart-item__info collapse-arrow_icon_block">
								<i></i>
								<div>
									<?if($props['is_configurator']):?>
										<div class="server-code"><?=$props['CPU']?></div>
									<?elseif($props['DOMAIN_NAME']):?>
										<div class="server-code"><?=$props['DOMAIN_NAME']?></div>
									<?endif;?>
									<div class="server-name"><?=$arItem['NAME']?></div>
									<div class="server-desc">
										<?if($props['CONFIGURATION']):?>
											<?=$props['CONFIGURATION']?> 
										<?endif;?>
										
										<?if($props['OS']):?>
											<?=$props['OS']?> 
										<?endif;?>
										<?if($props['CPU_QTY']):?>
											<?=$props['CPU_QTY']?> 
										<?endif;?>
										<?if($props['FTP']):?>
											FTP:<?=$props['RAM']?> 
										<?endif;?>
										<?if($props['IP_QTY']):?>
											IP:<?=$props['IP_QTY']?> 
										<?endif;?>
										
										<?if($props['RAM']):?>
											RAM:<?=$props['RAM']?> 
										<?endif;?>
										<?if($props['HDD']):?>
											HDD:<?=$props['HDD']?> 
										<?endif;?>
										<?if($props['RAID']):?>
											RAID:<?=$props['RAID']?> 
										<?endif;?>
										<?if($props['SSD']):?>
											SSD:<?=$props['SSD']?> 
										<?endif;?>
										
									</div>
								</div>
							</div>
						</div>
						<div class="cart-item__col2">
							<div class="cart-item__colh">стоимость</div>
							<div class="cart-item__price">
								<?if($props['unique_id']):?>
									<?=number_format(round($arItem['PRICE']/$props['MONTHS']),0,'.',' ')?> <span><span class="rub"></span>/мес</span>
								<?elseif($props['DOMAIN_NAME']):?>
									<?=number_format($arItem['PRICE'],0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?elseif($props['years']):?>
									<?=number_format(round($arItem['PRICE']/$props['years']),0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?else:?>
									<?=number_format($arItem['PRICE'],0,'.',' ')?> <span><span class="rub"></span>/год</span>
								<?endif;?>
							</div>
						</div>
						<div class="cart-item__col3">
							<?/*
							<div class="cart-item__colh">количество</div>
							<div class="counter_block__wrapper">
								<div class="counter_block__actions">
									<a href="#" class="cb_minus">-</a>
									<input type="text" value="<?=$arItem['QUANTITY']?>">
									<a href="#" class="cb_plus">+</a>
								</div>
							</div>
							*/?>
						</div>
						<div class="cart-item__col4">
							<?if($props['is_configurator'] || $props['is_solution'] || $arItem['NAME'] == 'Аренда VDS сервера'):?>
							<div class="cart-item__colh">период</div>
							<select name="period[]">
								<option value="1"<?if($props['MONTHS'] == 1):?> selected<?endif;?>>1 месяц</option>
								<option value="3"<?if($props['MONTHS'] == 3):?> selected<?endif;?>>3 месяца</option>
								<option value="6"<?if($props['MONTHS'] == 6):?> selected<?endif;?>>6 месяцев</option>
								<option value="12"<?if($props['MONTHS'] == 12):?> selected<?endif;?>>12 месяцев</option>
							</select>
							<?endif;?>
						</div>
						<div class="cart-item__col5">
							<div class="cart-item__colh"></div>
							<div class="cart-item__total_price">
								<div class="item__total_price_price">
									<?=number_format($total_row,0,'.',' ')?>  <span><span class="rub"></span></span>
								</div>
								<?if($props['is_configurator'] || $props['is_solution']):?>
									<div class="item__total_price_economy">
										Экономия за сервер 681 Р
									</div>
								<?endif;?>
							</div>
						</div>
						<div class="cart-item__col6">
							<a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>" class="cart-delete"></a>
						</div>
					</div>
					<div class="cart-detalization-items collapse-content">
						<table>
							<tr>
								<td><?=$arItem['NAME']?></td>
								<?
								if(!$props['MONTHS']) $props['MONTHS'] = 1;
								?>
								<td><?=number_format(round($arItem['PRICE']/$props['MONTHS']),0,'.',' ')?> <span><span class="rub"></span>/мес</span></td>
							</tr>
							<?if($arItem['NAME'] == 'Аренда VDS сервера' && $props['TEST_PERIOD_14'] == 'да'):?>
								<tr>
									<td>Дополнительно 15 дней</td>
									<td class="null-price">
										0 <span><span class="rub"></span></span>
									</td>
								</tr>
							<?endif;?>
							<?foreach($items as $k1 => $item):?>
							<?
								$props2 = [];
								foreach($item['PROPS'] as $v):
									$props2[$v['CODE']] = $v['VALUE'];
								endforeach;
								
								if($props2['unique_id'] != $props['unique_id'] || !$props2['is_service']) continue;
								
							?>
								<tr>
									<td><?=$item['NAME']?>(<?=$item['QUANTITY']?> шт.)</td>
									<td <?if(!(int)$item['PRICE']):?> class="null-price"<?endif;?>>
										<?=number_format($item['PRICE']*$item['QUANTITY'],0,'.',' ')?> <span><span class="rub"></span>/мес</span>
									</td>
								</tr>
							<?endforeach;?>
						</table>
						<div class="cart-detalization-bottom">
							<?if($props['is_configurator']):?>
								<a href="#" class="cart-change-conf">Изменить конфигурацию</a>
							<?endif;?>
							
							<div class="cdb-total">
								Итого: <span class="cdb-total-price"><?=number_format($total,0,'.',' ')?>  <span><span class="rub"></span></span></span>
							</div>
							
						</div>
					</div>
				</div>		
		<?
			endif;
		endforeach;
		?>
		
	
	</div>
	
</div>
<div id="cart-total-line">
	Итого: 
	<span id="cart-total-price"><?=number_format($totaltotal,0,'.',' ')?></span>
	<span id="cart-total-currency">Р</span>
</div>
  
<div class="cart-actions">
	<a href="javascript:void(0)" onclick="checkOut();" class="button-blue2">Оформить заказ</a>
	<a href="/personal/order/getoffer/" class="getoffer">Получить коммерческое предложение</a>
</div>
  
  
<?
else:
  ?>
  <div id="warning_message">
    <?
    echo ShowError(GetMessage("SALE_NO_ITEMS"));
    ?>
  </div>
<?
endif;
?>