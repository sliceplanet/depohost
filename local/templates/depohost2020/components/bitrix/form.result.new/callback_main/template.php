<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
?>

<?= str_replace('<form', '<form class="banner_callback--form" ', $arResult["FORM_HEADER"]);?>
	<?if ($arResult["isFormErrors"] == "Y"):?>
        <div class="alert alert-danger"><?=$arResult["FORM_ERRORS_TEXT"];?></div>
        <script>
			Recaptchafree.reset();
		</script>
    <?elseif($_REQUEST["formresult"] == "addok"):?>
    	<div class="alert alert-success">Сообщение успешно отправлено!</div>
		<script>
			Recaptchafree.reset();
		</script>
    <?endif;?>
	<div class="banner_callback--input">
		<label for="name">Имя</label>
		<?=$arResult["QUESTIONS"]["NAME"]["HTML_CODE"]?>
	</div>
	<div class="banner_callback--input">
		<label for="phone">Телефон</label>
		<?=$arResult["QUESTIONS"]["PHONE"]["HTML_CODE"]?>
	</div>
	<div class="banner_callback--input">
		<label for="phone">E-mail</label>
		<?=$arResult["QUESTIONS"]["EMAIL"]["HTML_CODE"]?>
	</div>
	<div class="banner_callback--button">
	 	<input class="btn-green" type="submit" name="web_form_submit" value="Попробовать бесплатно">
  		<div class="politic-form">
		  <input type="checkbox" name="politic" checked>
		  <label for="politic">Согласие с <a href="/rekvizit/private-policy/">политикой конфиденциальности</a></label>
	   	</div>	
	</div>
<script>
$(function(){
    $("[type=tel], [validate=phone]").mask("+7(999)999-9999");
});
</script>
<?=$arResult["FORM_FOOTER"];?>
