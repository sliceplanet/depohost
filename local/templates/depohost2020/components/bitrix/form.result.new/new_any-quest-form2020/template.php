<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
?>

<?= str_replace('<form', '<form class="any-quest-form" ', $arResult["FORM_HEADER"]);?>

	<?if ($arResult["isFormErrors"] == "Y"):?>
		<div class="alert alert-danger"><?=$arResult["FORM_ERRORS_TEXT"];?></div>
		<script>
			Recaptchafree.reset();
		</script>
	<?elseif($_REQUEST["formresult"] == "addok"):?>
		<div class="alert alert-success">Сообщение успешно отправлено!</div>
		<?if(isset($_COOKIE['seodev']) || isset($_COOKIE['dev'])):?>
			<script>
				Recaptchafree.reset();
			</script>
		<?endif;?>
	<?endif;?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="label-control">Имя <span class="required">*</span></label>
				<?=str_replace('<input ','<input class="form-control" ',$arResult["QUESTIONS"]["NAME"]["HTML_CODE"])?>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="label-control">Телефон <span class="required">*</span></label>
				<?=str_replace('<input ','<input class="form-control" ',$arResult["QUESTIONS"]["PHONE"]["HTML_CODE"])?>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="label-control">Ваш вопрос</label>
				 <?=str_replace('<textarea ','<textarea class="form-control" ',$arResult["QUESTIONS"]["QUESTION"]["HTML_CODE"])?>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="checlbox-label agree-label">
					<input type="checkbox" name="politic" value="1" />
					<span class="checkbox-checker"></span> 
					Согласен с политикой конфиденциальности
				</label>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<button class="button-blue2">получить ответ</button>
				<input type="hidden" name="web_form_submit" value="Получить ответ">
			</div>
		</div>			
	</div>
<?=$arResult["FORM_FOOTER"];?>
<script>
  $(function(){
      $("[type=tel], [validate=phone]").mask("+7(999)999-9999");
      $("[type=tel], [data-validate=phone]").mask("+7(999)999-9999");
  });
</script>

<?/*
<?= str_replace('<form', '<form class="any-quest-form" ', $arResult["FORM_HEADER"]);?>
  <?if ($arResult["isFormErrors"] == "Y"):?>
        <div class="alert alert-danger"><?=$arResult["FORM_ERRORS_TEXT"];?></div>
        <script>
      Recaptchafree.reset();
    </script>
    <?elseif($_REQUEST["formresult"] == "addok"):?>
      <div class="alert alert-success">Сообщение успешно отправлено!</div>
      <?if(isset($_COOKIE['seodev']) || isset($_COOKIE['dev'])):?>
        <script>
          Recaptchafree.reset();
        </script>
      <?endif;?>
    <?endif;?>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-lg-6 dedic-form">
        <label for="name">Имя</label>
        <?=$arResult["QUESTIONS"]["NAME"]["HTML_CODE"]?>
        <p></p>
        <label for="phone">Телефон</label>
        <?=$arResult["QUESTIONS"]["PHONE"]["HTML_CODE"]?>
        <p></p>
        <div class="politic-form hidden-xs hidden-sm hidden-md">
          <input type="checkbox" name="politic" checked>
          <label for="politic">Согласие с <a href="/rekvizit/private-policy/">политикой конфиденциальности</a></label>
        </div>
      </div>
      <div class="col-xs-12 col-lg-6 dedic-form">
         <label for="message">Ваш вопрос</label>
          <?=$arResult["QUESTIONS"]["QUESTION"]["HTML_CODE"]?>
         <div class="politic-form hidden-lg">
          <input type="checkbox" name="politic" checked>
          <label for="politic">Согласие с <a href="/rekvizit/private-policy/">политикой конфиденциальности</a></label>
        </div>
         <input class="btn-green" type="submit" name="web_form_submit" value="Получить ответ">
      </div>
    </div>
  </div>
<script>
  $(function(){
      $("[type=tel], [validate=phone]").mask("+7(999)999-9999");
      $("[type=tel], [data-validate=phone]").mask("+7(999)999-9999");
  });
</script>
<?=$arResult["FORM_FOOTER"];?>
*/?>