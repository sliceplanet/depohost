<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
?>
<div class="form-custom-service">
  <h3 class="form-custom-service__title">Не нашли нужную конфигурацию?<br>Мы поможем подобрать подходящий вариант</h3>
  <form class="form-ajax" action="/api/form.php" method="POST" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="WEB_FORM_ID" value="<?= $arResult['arForm']['ID'] ?>">

    <div class="row">
      <?
      foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
      {
        $arStructure = $arQuestion['STRUCTURE'][0];
        $answer = reset($arResult['arAnswers'][$FIELD_SID]);
        $fname = "form_" . $answer["FIELD_TYPE"] . "_" . $answer["ID"];
        if ($arStructure['FIELD_TYPE'] == 'hidden')
        {
          echo $arQuestion["HTML_CODE"];
        }
        else
        {
          $arStructure['FIELD_TYPE'] = !empty($arResult['arQuestions'][$FIELD_SID]['COMMENTS']) ? $arResult['arQuestions'][$FIELD_SID]['COMMENTS'] : $arStructure['FIELD_TYPE'];
          if ($arStructure['FIELD_TYPE'] === 'hidden')
          {
            ?>
            <input type="hidden" name="<?= $fname ?>" value="<?= $arParams['SERVICE_NAME'] ?>">
            <?
          }
          elseif ($arStructure['FIELD_TYPE'] === 'textarea')
          {
            ?>
            <label class="col-12 field">
              <span class="field__label"><?= $arQuestion['CAPTION'] ?><?if ($arQuestion["REQUIRED"] == "Y"): ?> <span class="field__star">*</span><? endif; ?></span>
            <textarea name="<?= $fname ?>" cols="<?= $arStructure['FIELD_WIDTH'] ?>"
                      rows="<?= $arStructure['FIELD_HEIGHT'] ?>"
              <?= $arQuestion["REQUIRED"] == "Y" ? 'required' : '' ?>><?= $arQuestion['VALUE'] ?></textarea>
              <div class="field__error" role="alert">Пожалуйста, заполните обязательное поле.</div>
            </label>
            <?
          }
          else
          {
            ?>
            <label class="col-xs-12 col-sm-6 field">
              <span class="field__label"><?= $arQuestion['CAPTION'] ?><?if ($arQuestion["REQUIRED"] == "Y"): ?> <span class="field__star">*</span><? endif; ?></span>
              <input type="<?= $arStructure['FIELD_TYPE'] ?>"
                     name="<?= $fname ?>"
                     value="<?= $arQuestion['VALUE'] ?>"
                <?= $arQuestion["REQUIRED"] == "Y" ? 'required' : '' ?>>
              <?
              if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?>
                <div class="field__error" role="alert">Пожалуйста, заполните обязательное поле.</div>
              <? endif; ?>
            </label>
            <?
          }
          ?>
          <?
        }
      } //endwhile
      ?>
    </div>
    <?if($arResult['USE_CAPTCHA'] === 'Y'):?>
      <div class="mf-captcha-new">
        <?=$arResult["CAPTCHA"];?>
      </div>
    <?endif?>
    <div class="field_checkbox">
      <input type="checkbox" name="agree" value="Y" id="agree" required>
      <label for="agree">Я даю согласие на обработку <a href="/doc/pl.pdf" target="_blank">персональных данных </a></label>
    </div>
    <div class="btn-submit-wrapper">
      <input class="blue-button" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit"
             name="web_form_submit"
             value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
    </div>

  </form>
</div>