<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
?>
<?= str_replace('<form', '<form class="dedic-form header-callback-form form-ajax-new" ', $arResult["FORM_HEADER"]);?>

<?if ($arResult["isFormErrors"] == "Y"):?>
    <div class="alert alert-danger"><?=$arResult["FORM_ERRORS_TEXT"];?></div>
    <script>
      Recaptchafree.reset();
    </script>
<?elseif($_REQUEST["formresult"] == "addok"):?>
	<div class="alert alert-success">Сообщение успешно отправлено!</div>
  <script>
    Recaptchafree.reset();
  </script>
<?endif;?>




<label for="name">Имя</label>
<?=$arResult["QUESTIONS"]["NAME"]["HTML_CODE"]?>
<br>
<label for="phone">Телефон</label>
<?=$arResult["QUESTIONS"]["PHONE"]["HTML_CODE"]?>

<div class="mf-captcha-new n-mt">
    <?//=$arResult["CAPTCHA"];?>
</div>

<div class="politic-form">
  <input type="checkbox" name="politic" checked>
  <label for="politic">Согласие с <a href="/rekvizit/private-policy/">политикой конфиденциальности</a></label>
</div>
<br>

<input class="btn-green hidden-xs hidden-sm hidden-md" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit"
        name="web_form_submit"
        value="Перезвоните мне"/>

<script>
    $(function(){
        $("[type=tel], [validate=phone]").mask("+7(999)999-9999");
    });
</script>
<?=$arResult["FORM_FOOTER"];?>