<?
use Bitrix\Main\Loader;
use Itin\Depohost\Dedicated;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!$arResult['MODULES']['catalog'])
{
  return;
}
$arResult['TYPE'] = 'solution';

$arResult['MIN_PRICE']['PRINT_VALUE'] = number_format($arResult['MIN_PRICE']['VALUE'], 0, '.',
    ' ') . ' <span class="rub"></span>';
Loader::includeModule('dedicated');
$arResult['DEDICATED'] = $arDedicated = Dedicated::getSolution($arResult['ID']);
$arResult['DISPLAY_PROPERTIES']['CONFIGURATION']['DISPLAY_VALUE'] = $arDedicated['CHARACTERISTICS_STRING'];

$arResult['BASKET_PROPS'] = array();
$quantityMonths = intval($_REQUEST['months']) > 0 ? intval($_REQUEST['months']) : 1;

$arMonths = array(1, 3, 6, 12);
$htmlMonthsSelect = '<select class="select_kam small period" name="months">';
foreach ($arMonths as $key => $month)
{
  $htmlMonthsSelect .= '<option value="' . $month . '"' . ($quantityMonths == $month ? ' selected="selected"'
      : '') . '>' . $month . ' ' . CUsefull::endOfWord($month, array('месяцев', 'месяц', 'месяца')) . '</option>';
}
$htmlMonthsSelect .= '</select>';
$arResult['BASKET_PROPS'][] = array('INPUT_HTML' => $htmlMonthsSelect);

$arResult['SUM']['VALUE'] = $arResult['MIN_PRICE']['VALUE'] * $quantityMonths;
$arResult['SUM']['PRINT_VALUE'] = number_format($arResult['SUM']['VALUE'], 0, '.', ' ') . ' <span class="rub"></span>';

global $APPLICATION;
$arResult['ACTION_URL'] = $APPLICATION->GetCurPageParam();
$arResult['CATALOG_MEASURE_NAME'] = 'в месяц';
$arResult['CATALOG_MEASURE_SHORT_NAME'] = '1 мес';


$arResult['CPU'] = CIBlockElement::GetByID($arResult['PROPERTIES']['CPU']['VALUE'])->Fetch()['NAME'];
$arResult['RAM'] = CIBlockElement::GetByID($arResult['PROPERTIES']['RAM']['VALUE'])->Fetch()['NAME'];
$arResult['HDD'] = [];
	foreach($arResult['PROPERTIES']['HDD']['VALUE'] as $hdd)
		$arResult['HDD'][] = CIBlockElement::GetByID($hdd)->Fetch()['NAME'];
		
$arResult['SSD'] = CIBlockElement::GetByID($arResult['PROPERTIES']['SSD']['VALUE'])->Fetch()['NAME'];
$arResult['RAID'] = CIBlockElement::GetByID($arResult['PROPERTIES']['RAID']['VALUE'])->Fetch()['NAME'];

?>