<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
  'ID' => $strMainID,
  'PICT' => $strMainID . '_pict',
  'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
  'STICKER_ID' => $strMainID . '_stricker',
  'BIG_SLIDER_ID' => $strMainID . '_big_slider',
  'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
  'SLIDER_LIST' => $strMainID . '_slider_list',
  'SLIDER_LEFT' => $strMainID . '_slider_left',
  'SLIDER_RIGHT' => $strMainID . '_slider_right',
  'OLD_PRICE' => $strMainID . '_old_price',
  'PRICE' => $strMainID . '_price',
  'DISCOUNT_PRICE' => $strMainID . '_price_discount',
  'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
  'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
  'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
  'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
  'QUANTITY' => $strMainID . '_quantity',
  'QUANTITY_DOWN' => $strMainID . '_quant_down',
  'QUANTITY_UP' => $strMainID . '_quant_up',
  'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
  'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
  'BUY_LINK' => $strMainID . '_buy_link',
  'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
  'COMPARE_LINK' => $strMainID . '_compare_link',
  'PROP' => $strMainID . '_prop_',
  'PROP_DIV' => $strMainID . '_skudiv',
  'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
  'OFFER_GROUP' => $strMainID . '_set_group_',
  'ZOOM_DIV' => $strMainID . '_zoom_cont',
  'ZOOM_PICT' => $strMainID . '_zoom_pict'
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);

?>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "<?= $arResult['NAME'] ?>",
  <?if(is_array($arResult['PREVIEW_PICTURE'])):?>
  "image": [
    "https://<?=$_SERVER['SERVER_NAME']?><?=$arResult['PREVIEW_PICTURE']['SRC']?>"
   ],
   <?endif?>
  "description": "<?=strip_tags($arResult['PREVIEW_TEXT'])?>",
  "mpn": "<?= $arResult['ID'] ?>",
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RUB",
    "price": "<?= $arResult['MIN_PRICE']['VALUE'] ?>",
    "itemCondition": "http://schema.org/UsedCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "Artextelecom"
    }
  }
}
</script>
<div id="<? echo $arItemIDs['ID']; ?>" class="server-order-detail">
	<div class="container">

		<h1>Сервер <?=$arResult['NAME']?> - подбор дополнительных услуг</h1>
		<?//if($arResult['PREVIEW_TEXT']):?>
			<p class="sod-desc"><?=$arResult['PREVIEW_TEXT']?></p>
		<?//endif;?>
		
		
		<div class="row">
			<div class="col-md-8">
	
				
			
				<div class="sod-main_info">
					<div class="sod-block_h">Оснащение сервера</div>
					<div class="sod-main_block">
						<div class="sod-main_block__top">
							<div class="sod-main_block__price">
								<?=str_replace('<span class="rub"></span>','<span><span class="rub"></span>/мес.</span>',$arResult['MIN_PRICE']['PRINT_VALUE'])?>
							</div>
							<div class="sod-main_block__info">
								<div class="server-code">???</div>
								<div class="server-name"><?=$arResult['NAME']?></div>
								<div class="server-desc">???</div>
							</div>
						</div>
						<div class="sod-main_block__bottom">
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">CPU</div>
										<div class="prop_value"><?=$arResult['CPU']?></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">RAM</div>
										<div class="prop_value"><?=$arResult['RAM']?></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">HDD</div>
										<div class="prop_value"><?=implode('<br>',$arResult['HDD'])?></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">SSD</div>
										<div class="prop_value"><?=$arResult['SSD']?></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">RAID</div>
										<div class="prop_value"><?=$arResult['RAID']?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
				
				<div class="sod-addional_items">
				
					<div class="sod-block_h">Дополнительные услуги</div>
					<div class="sod-addional_items_wrapper">
					
						<div class="sod_ai checked collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Название услуги</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Описание услуги</span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<ul class="sod_ai__list">
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price null-price">
														0 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price">
														500 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price null-price">
														0 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
								</ul>
							</div>
						</div>
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Название услуги</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Описание услуги</span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<ul class="sod_ai__list">
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price null-price">
														0 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price">
														500 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="aa1" />
											<div class="sod_ai__list_item">
												<div class="sod_ai__list_item_left">
													<span class="checker"><span></span></span>
													<span class="sod_ai__list_item_name">Место на выделенном FTP сервере 20 Гб</span>
												</div>
												<div class="sod_ai__list_item_right">
													<div class="sod_ai__list_item_price null-price">
														0 <span><span class="rub"></span>/мес.</span>
													</div>
												</div>
											</div>
										</label>
									</li>
								</ul>
							</div>
						</div>
					
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Бесплатные 2 домена (на год)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__domains_items">
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 1</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
											</div>
											<a href="#" class="button-blue">Проверить</a>
										</div>
									</div>
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 2</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
											</div>
											<a href="#" class="button-blue">Проверить</a>
										</div>
									</div>
									<p>Вы можете подобрать себе домены и позже - после оплаты услуги</p>
								</div>
								
							</div>
						</div>
						
					
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Бесплатные 2 домена (на год)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__domains_items">
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 1</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
												<div class="form-message green">Домен доступен для регистрации</div>
											</div>
											<a href="#" class="button-blue2">Заказать</a>
										</div>
									</div>
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 2</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df" class="error"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
												<div class="form-message red">Домен занят</div>
											</div>
											<a href="#" class="button-blue">Проверить</a>
										</div>
									</div>
									<p>Вы можете подобрать себе домены и позже - после оплаты услуги</p>
								</div>
								
							</div>
						</div>
						
						
					
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Бесплатные 2 домена (на год)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__domains_items">
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 1</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
												<div class="form-message green">Домен заказан и будет куплен после оплаты</div>
											</div>
											<a href="#" class="button-green">Заказан</a>
										</div>
									</div>
									<div class="sod_ai__domains_item">
										<label>Бесплатный домен 2</label>
										<div class="input-group">
											<div>
												<input type="text" name="" value="df" class="error"/>
												<select name="">
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
													<option value="">.РФ</option>
												</select>
												<div class="form-message red">Домен занят</div>
											</div>
											<a href="#" class="button-blue">Проверить</a>
										</div>
									</div>
									<p>Вы можете подобрать себе домены и позже - после оплаты услуги</p>
								</div>
								
							</div>
						</div>
						
						
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Бесплатный SSL сертификат (на год)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__ssl_info">
									<div class="input-group">
										<div class="input-block">
											<label>Введите домен</label>
											<input type="text" name="" value="df"/>
										</div>
										<div class="input-block">
											<label>Выберите email</label>
											<select name="">
												<option value=""></option>
												<option value="">administrator@test777.com</option>
												<option value="">administrator@test777.com</option>
												<option value="">administrator@test777.com</option>
											</select>
										</div>
										<a href="#" class="button-blue">Заказать</a>
									</div>
								</div>
								
							</div>
						</div>
						
						
						
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">Бесплатный SSL сертификат (на год)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__ssl_info">
									<div class="input-group">
										<div class="input-block">
											<label>Введите домен</label>
											<input type="text" name="" value="test.com"/>
										</div>
										<div class="input-block">
											<label>Выберите email</label>
											<select name="">
												<option value="">administrator@test777.com</option>
												<option value="">administrator@test777.com</option>
												<option value="">administrator@test777.com</option>
											</select>
										</div>
										<a href="#" class="button-green">Заказан</a>
									</div>
									<div class="form-message green">SSL сертификат заказан и будет доступен после оплаты</div>
								</div>
								
							</div>
						</div>
						
						<div class="sod_ai collapse-parent">
							<div class="sod_ai__top collapse-buttton_block">
								<div class="sod_ai__left collapse-arrow_icon_block">
									<i></i>
									<div>
										<div><span class="sod_ai__name">SQL Server 2012-2016 (Лицензии на ядро)</span> <span class="hint">?</span></div>
										<span class="sod_ai__desc">Выбрать домен можно позже <span class="sod_ai__gift_label">Подарок</span></span>
									</div>
								</div>
								<div class="sod_ai__right">
									<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
								</div>
							</div>	
							<div class="sod_ai__detalization collapse-content">	
								<div class="sod_ai__lic">
									<div class="flex-row jcsb">
										<div class="sod_ai__lic_name_info">
											<div class="sod_ai__lic_name">Windows Remote Desktop Services UsrCAL</div>
											<div class="sod_ai__lic_info"><span>500 <span><span class="rub"></span>/мес</span></span> за лицензию</div>
										</div>
										<div class="sod_ai__lic_counter counter_block">
											<div class="counter_block__wrapper">
												<span>количество</span>
												<div class="counter_block__actions">
													<a href="#" class="cb_minus">-</a>
													<input type="text" value="0">
													<a href="#" class="cb_plus">+</a>
													
												</div>
											</div>
										</div>
										<div class="sod_ai__lic_price this-is-price">
											0 <span class="rub"></span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						
						
						
						
						
						
					</div>
					
				</div>
	
		
	
	
			</div>
			<div class="col-md-4">
				<div class="sod-result_block collapse-parent">
				
					<div class="sod_rb__data">
					
						<div class="sod_rb__top collapse-buttton_block">
							<div class="sod_rb__top_left collapse-arrow_icon_block">
								<i></i>
								<span>Итого</span>
							</div>
							<div class="sod_rb__top_right">
								<div id="total_price">
									<?=str_replace('<span class="rub"></span>','<span><span class="rub"></span>/мес.</span>',$arResult['MIN_PRICE']['PRINT_VALUE'])?>
								</div>
							</div>
							
						</div>
						<div class="sod_rb__bottom collapse-content">
							<div class="sod_rb__order_item">
								<div class="sod_rb__order_item_name">D-E36  Cервер Xeon Е3 v6</div>
								<div class="sod_rb__order_item_price">9 000 <span><span class="rub"></span>/мес.</span></div>
							</div>
							<div class="sod_rb__order_item null-price">
								<div class="sod_rb__order_item_name">Профессиональное администрирование тариф "Базовый"</div>
								<div class="sod_rb__order_item_price">0 <span><span class="rub"></span>/мес.</span></div>
							</div>
						</div>
					
					</div>
					
					<div class="order-form-block">
						<div class="ofb-h">Оформление заказа</div>
						<form>
							<div class="ofb-items">
								
								<div class="ofb-item">
									<input id="cnt1" type="radio" name="cnt" value="1" checked />
									<label for="cnt1">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">1 месяц</span>
										<span class="ofb-item_price">9 500 <span class="rub"></span></span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt3" type="radio" name="cnt" value="3" />
									<label for="cnt3">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">3 месяца</span>
										<span class="ofb-item_price">27 500 <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия 681 р</span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt6" type="radio" name="cnt" value="6" />
									<label for="cnt6">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">6 месяцев</span>
										<span class="ofb-item_price">49 100 <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия 681 р</span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt12" type="radio" name="cnt" value="12" />
									<label for="cnt12">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">12 месяцев</span>
										<span class="ofb-item_price">59 500 <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия 681 р</span>
									</label>
								</div>
								
							</div>
							<button class="blue-button">Заказать</button>
							
							<a href="#" class="add-to-cart">Отложить в корзину</a>
						
						</form>
					</div>
				
				</div>
			</div>
			
			
			
		</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	</div>
</div>
<script>
  var arDiscounts = <?=CUtil::PhpToJSObject($arResult['DEDICATED']['PRICES'])?>;
</script>
