<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!function_exists('first_value'))
{
    // Возвращает первое значение массива
    function first_value($array)
    {
        if (is_array($array))
        {
            reset($array);
            return current($array);
        } else
        {
            return $array;
        }
    }

}
if (!function_exists('getPriceFormat'))
{
    // Форматирует ценy
    function getPriceFormat($price)
    {
        $price = number_format($price, 0, "", " ");
        return $price;
    }

}
if (!function_exists('array_price_format'))
{
    // Форматирует массив цен
    function array_price_format($array)
    {
        if (!is_array($array))
        {
            $array = array($array);
        }
        foreach ($array as $key => $value)
        {
            $array[$key] = number_format($value, 0, "", " ");
        }
        return $array;
    }

}
if (!function_exists('endOfWord'))
{
 /* окончание
    0 изображений  variants[0]
    1 изображение  variants[1]
    2-3-4 изображения variants[2]
    5-6-7-8-9-10 изображений variants[0]
    11-12-13-14-15-16-17-18-19-20 - человек
    */
    function endOfWord($str,$variants){
        $last = substr($str,strlen($str)-1,1);
        $last2 = substr($str,strlen($str)-2,2);
        $end = $variants[2];
        if($last==0 || ($last>=5 && $last<=9)) $end = $variants[0];
        if($last==1) $end = $variants[1];
        if($last>=2 && $last<=4) $end = $variants[2];
        if($last2>=12 && $last2<=14 && count($variants)>3) $end = $variants[3];

        return $end;
    }
}
?>