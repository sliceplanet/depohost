<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']))
{
    return; // no items, good bye
}
require_once 'functions.php';
?>

<section id="ready-made" class="clearfix">
    <div class="head clearfix">
        <h2><?= Loc::getMessage('TITLE_SOLUTIONS') ?></h2>
        <div class="descr-wrapper clearfix">
            <i class="icon-info"></i>
            <div class="descr">
                <?= Loc::getMessage('DESCRIPTION_SOLUTIONS') ?>
            </div>
        </div>
    </div>
    <!--.head-->
    <div id="filter" class="blue-block clearfix">
    </div>
    <!--#filter-->
    <div id="servers-list">
        <? foreach ($arResult['SECTIONS'] as $arSection): ?>
            <div class="section" id="<?= $arSection['CODE'] ?>">
                <h2><?= $arSection['NAME'] ?></h2>
                <div class="section-wrapper clearfix">
                    <?
                    $i = 0;
                    foreach ($arResult['ITEMS'] as $arItem):
                        if ($arSection['ID'] != $arItem['SECTION_ID'])
                            continue;
                        else
                            $i++;
                        if ($i > $arParams['PAGE_COUNT'])
                            break;
                        ?>
                        <div class="server-info clearfix">
                            <div class="caption"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
                            <div class="main-parameters clearfix">
                                <div class="parameter-block cpu clearfix">
                                    <div class="caption"><?= Loc::getMessage('CAPTION_CPU') ?></div>
                                    <div class="descr">
                                        <?= $arItem['PROPERTIES']['CPU']['DISPLAY_VALUE'] ?>
                                    </div>
                                </div>
                                <div class="parameter-block ram clearfix">
                                    <div class="caption"><?= Loc::getMessage('CAPTION_RAM') ?></div>
                                    <div class="descr">
                                        <?=$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE'] ?>
                                    </div>
                                </div>
                                <div class="parameter-block hdd clearfix">
                                    <div class="caption"><?= Loc::getMessage('CAPTION_HDD') ?></div>
                                    <div class="descr">
                                        <?= $arItem['PROPERTIES']['HDD']['DISPLAY_VALUE'] ?>

                                    </div>
                                </div>
                            </div>
                            <div class="advance-param">
                                <div class="caption"><?= $arItem['PROPERTIES']['ADVANCE']['NAME'] ?></div>
                                <div class="descr">
                                    <?= $arItem['PROPERTIES']['ADVANCE']['DISPLAY_VALUE'] ?>
                                </div>
                            </div>
                            <div class="order-block small clearfix">
                                <div class="terms-block">
                                    <? foreach ($arItem['PRICES'] as $count_month => $arPrices): ?>
                                        <div class="term-block<? if ($arPrices['SELECTED']) echo " active" ?>" data-value="<?= $count_month ?>">
                                            <div class="top">
                                                <div class="term"><?= $count_month ?> <?= CUsefull::endOfWord($count_month, array('месяцев', 'месяц', 'месяца')); ?></div>
                                                <div class="price"><?= getPriceFormat($arPrices['SUM']); ?> р</div>
                                                <div class="discount"><?= Loc::getMessage('DISCOUNT_CAPTION', array('#DISCOUNT_PRICE#' => getPriceFormat($arPrices['DISCOUNT']))) ?></div>
                                            </div>
                                            <div class="bottom">
                                                <div class="price"><?= getPriceFormat($arPrices['PRICE']); ?> р</div>
                                                <?= Loc::getMessage('MEASURE_SERVER') ?>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <div class="buy-block clearfix">
                                    <a class="btn-green sm" href="#"
                                       data-type="solution"
                                       data-solution-id="<?= $arItem['ID'] ?>"
                                       data-months="1"
                                       ><?= Loc::getMessage('ORDER_ADD') ?></a>
                                    <div class="descr-wrapper">
                                        <div class="descr">
                                            <?= Loc::getMessage('SERVER_AVAILABLE', array('#QUANTITY#' => $arItem['PROPERTIES']['QUANTITY']['DISPLAY_VALUE'], '#MEASURE#' => CUsefull::endOfWord($arItem['PROPERTIES']['QUANTITY']['DISPLAY_VALUE'], array('серверов', 'сервер', 'сервера')))) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--.order-block.small-->
                        </div>
                        <!--.server-info-->
                    <? endforeach; ?>
                </div>
                <!--.section-wrapper-->
            </div>
            <!--#<?= $arSection['CODE'] ?>-->
        <? endforeach; ?>
    </div>
    <!--.server-list-->
</section>
<!-- END #ready-made -->