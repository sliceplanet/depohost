function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
	// 
	// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return km + kw + kd;
}


$(document).ready(function() {


	$('body').on('click','.collapse-buttton_block',function(){
		
		var p = $(this).parents('.collapse-parent');
		var c = p.find('.collapse-content');
		
		p.toggleClass('opened');
		
		
		return false;
	});


	$('body').on('click','.cb_minus',function() {
		
		var min = 1;
		var inp = $(this).parent().find('input');
		var v = inp.val();
		
		if(inp.attr('data-min'))
		{
			min = parseInt(inp.attr('data-min'));
		}
		
		if(v > min)
			v--;
		else
			v = min;
		
		inp.val(v);
		inp.trigger('change');
		
		return false;
		
	});
	$('body').on('click','.cb_plus',function() {
		
		var max = 9999999999;
		var inp = $(this).parent().find('input');
		var v = inp.val();
		
		if(inp.attr('data-max'))
		{
			max = parseInt(inp.attr('data-min'));
		}
		
		if(v < max)
			v++;
		else
			v = max;
		
		inp.val(v);
		inp.trigger('change');
		
		
		return false;
		
	});

	
	if($('.table-block .main-cells').length)
	{
		$('.table-block .main-cells').eq(0).find('> div').each(function(i) {
			var p = $(this).parents('.table-block');
			p.find('.table-div__hcell').eq(i).width($(this).outerWidth());
		});
	}

	if($('.simple-table-block').length)
	{
		/*$('.table-block .main-cells').eq(0).find('> div').each(function(i) {
			var p = $(this).parents('.table-block');
			p.find('.table-div__hcell').eq(i).width($(this).outerWidth());
		});*/
	}
	
	function add_total_item(sid,name,price,delete_prev=true)
	{
		if(delete_prev)
		{
			$('#sod_rb__order_item__'+sid).remove();
		}
		
		var cl = 'sod_rb__order_item';
		if(!price) cl += ' null-price';
		var total_item = ''+
		'<div class="'+cl+'" id="sod_rb__order_item__'+sid+'" data-price="'+price+'">'+
			'<div class="sod_rb__order_item_name">'+name+'</div>'+
			'<div class="sod_rb__order_item_price">'+price+' <span><span class="rub"></span>/мес</span></div>'+
		'</div>';
		
		$('.sod_rb__bottom').eq(0).append(total_item);
	}
	
	function set_additional_service_input()
	{
		
		$('#additional-services').html('');
		var html = '';
		
		$('.sod_ai [type=radio]:checked').each(function() {
			html += '<input type="hidden" name="services[]" value="'+$(this).val()+'">';
		});
		$('.sod_ai .counter_block__actions input').each(function() {
			var n = parseInt($(this).val());
			var p = $(this).parents('.counter_block__actions');
			if(n)
			{
				html += '<input type="hidden" name="services[]" value="'+p.attr('data-id')+'">';
				html += '<input type="hidden" name="services_q['+p.attr('data-id')+']" value="'+$(this).val()+'">';
			}
		});
		
		$('#additional-services').html(html);
	}
	
	function configurator_count_total()
	{
		var server_price = parseInt($('.ofb-item [name=cnt]:checked').attr('data-price'));
		var months = parseInt($('.ofb-item [name=cnt]:checked').val());
		
		var total = server_price;
		
		$('.sod_rb__order_item').each(function(i){
			
			if(i)
			{
				var price = parseInt($(this).attr('data-price'));
				total += price * months;
			}
			
		});
		
		$('#total_price').html(total + ' <span><span class="rub"></span></span>');
		
		$('#total_price_note').text('при оплате за '+months+' мес.');
		
		set_additional_service_input();
	}
	
	


	$('body').on('change','.sod_ai [type=radio]',function(){
		
		var p = $(this).parents('.sod_ai');
		var name = $(this).parent().find('.sod_ai__list_item_name').text();
		var price = parseInt($(this).attr('data-price'));
		var id = parseInt($(this).val());
		var sid = parseInt(p.attr('data-id'));
		$('#sod_rb__order_item__'+sid).remove();
		
		if(p.find('[type=radio]:checked').length)
		{
			p.addClass('checked');
			p.find('.sod_ai__desc').text(name);
			p.find('.sod_ai__price').html(price+' <span><span class="rub"></span>/мес</span>');
			
			
			add_total_item(sid,name,price)
			
		}
		else
		{
			p.removeClass('checked');
			p.find('.sod_ai__desc').text('Нет');
			p.find('.sod_ai__price').html('0 <span><span class="rub"></span>/мес</span>');
		}
		
		configurator_count_total();
		
		
		
		return false;
	});
	
	
	
	$('body').on('change','.ofb-item [name=cnt]',function(){
		
		var price = parseInt($(this).attr('data-price'));
		var v = parseInt($(this).val());
		
		price = price / v;
		
		$('.sod_rb__order_item').eq(0).find('.sod_rb__order_item_price').html(price+' <span><span class="rub"></span>/мес</span></div>');
		
		$('#months').val(v);
		$('#price').val(price);
		
		configurator_count_total();
		
	});
	
	
	$('body').on('change','.counter_block input',function(){
	
		if($(this).parents('.ms-row').length) return;
		
		var v = parseInt($(this).val());
		var price = parseInt($(this).attr('data-price'));
		
		var total = v * price;
		
		var p = $(this).parents('.sod_ai');
		var sid = parseInt(p.attr('data-id'));
		var name = p.find('.sod_ai__name').text()+' - '+v+' шт.';
		p.find('.sod_ai__price').html(total+' <span><span class="rub"></span>/мес</span>');
		
		
		if(v)
		{
			p.addClass('checked');
			add_total_item(sid,name,total);
		}
		else
		{
			p.removeClass('checked');
		}
		
		configurator_count_total();
		
	});
	
	$('body').on('change','.cart-item [name="period[]"]',function(){
		
		var c = $(this).parents('.cart-item');
		var cnt = $(this).val();
		var id = c.attr('data-id');
		
		$.get('/personal/cart/ajax-change_months_count.php?id='+id+'&cnt='+cnt,
		function(data){
			
			if(data == 1)
			{
				location.reload();
			}
			else
			{
				alert(data);
			}
				
		});
		
		
	});
	
	
  
  
  
	var sod_result_block_top;

	if($('.sod-result_block').length)
		sod_result_block_top = $('.sod-result_block').offset().top;

	$(window).scroll(function() {
		
		if($('.sod-result_block').length && !sod_result_block_top)
			sod_result_block_top = $('.sod-result_block').offset().top;
		
		if(sod_result_block_top)
		{
			var st = $(window).scrollTop();
			
			if(st >= sod_result_block_top)
			{
				$('.sod-result_block').addClass('sticky');
			}
			else 
			{
				$('.sod-result_block').removeClass('sticky');
			}
		}
		
		
	});
	
	
	
	$('.all-checkbox').change(function() {
	
		var p = $(this).parents('.checkboxes-parent');
		
		if($(this).is(':checked'))
		{
			p.find('[type=checkbox]:checked').not($(this)).prop('checked',false);
		}
		
	});
	
	$('.checkboxes-parent [type=checkbox]').change(function() {
	
		if($(this).hasClass('all-checkbox')) return;
		
		var p = $(this).parents('.checkboxes-parent');
		
		if($(this).is(':checked'))
		{
			p.find('.all-checkbox').prop('checked',false);
		}
		
	});
	
	$('.ssl-filter [type=checkbox]').change(function() {
		
		var a = $(this).parents('form').serialize();
		
		var url = location.pathname;
		
		$.post(url,a,
		function(data){
			
			var html = $('<div>'+data+'</div>').find('.ssl-table-block').html();
			
			$('.ssl-table-block').html(html);
			
			$('.table-block .main-cells').eq(0).find('> div').each(function(i) {
				var p = $(this).parents('.table-block');
				p.find('.table-div__hcell').eq(i).width($(this).outerWidth());
			});
			
		});
		
	});
	
	$('.ssl-order-form form').submit(function(e) {
	
		e.preventDefault();
		
		var id = $(this).attr('data-id');
		var domain_val = $(this).find('[name="prop[domain]"]').val();
		var email_val = $(this).find('[name="prop[email]"]').val();
		var year_val = parseInt($(this).find('[name="quantity"]').val());
		var year_check = $(this).find('[name="quantity"] option:selected');
		
		var price = parseInt($(this).attr('data-price'));
		
		var sum = price * year_val;
		
		var href =
			"/personal/cart/ajax-add.php?action=BUY&id=" +
			id +
			"&prop[domain]=" +
			domain_val +
			"&prop[email_admin]=" +
			email_val +
			"&prop[years]=" +
			year_val +
			"&measure_name=" +
			year_check.attr("data-measure") +
			"&quantity=1" +
			"&SUM=" +
			sum +
			"&type=ssl";
			
		console.log(href);	
			
		window.location = href;
		
		return false;
	});
	
	
	function count_ms_total()
	{
		var total = 0;
		
		$('.table-microsoft .ms-row.checked').each(function() {
			
			var price = parseInt($(this).find('.ms-price span').text());
			total += price;
			
		});
		
		if(total)
			$('.ms-total-block').addClass('can-be-fixed');
		else
			$('.ms-total-block').removeClass('can-be-fixed');
		
		$(window).scroll();
		
		$('.ms-total-block-price span').text(total);
	}
	
	
	$('.ms-check').click(function() {
		
		var p = $(this).parents('.ms-row');
		var n = parseInt(p.find('input[type=text]').val());
		
		if(!n)
			p.find('input[type=text]').val(1);
		
		p.find('input[type=text]').trigger('change');
		
		p.addClass('checked');
		
		count_ms_total();
		
		return false;
	});
	
	$('.ms-uncheck').click(function() {
		
		var p = $(this).parents('.ms-row');
		p.find('input[type=text]').val(0);
		
		p.find('input[type=text]').trigger('change');
		
		p.removeClass('checked');
		
		count_ms_total();
		
		return false;
	});
	
	
	$('.ms-row .counter_block__wrapper input').change(function() {
		
		
		var p = $(this).parents('.ms-row');
		var price = parseInt(p.find('.counter_block__actions').attr('data-price'));
		var n = parseInt($(this).val());
				
		p.find('.ms-price span').text(price*n);
		
		if(n)
		{
			p.addClass('checked');
		}
		else
		{
			p.removeClass('checked');
		}
		
		count_ms_total();
		
	});
	
	
	$('.ms-submit').click(function(){
		
		if(!$('.ms-row.checked').length) return false;
		
	//	console.log($('#order-desktop').serialize());
	//	return false;
		
		$('#order-desktop').submit();
		
		
		return false;
	});
	
	
		
	if($('#configuration').length)
	{
		setTimeout(function(){
			var d1 = $('#configuration > div > div').eq(0);
			var d2 = $('#configuration > div > div').eq(1);
			
			var h1 = d1.height();
			d2.height(h1-49);
			
		},2000);

	}
	
	
	setTimeout(function(){
		var lhash = location.hash;
		
		if(lhash == '#ready-made')
		{
			$('.ready-made [href="#ready-made"]').click();
			var st = $('.configuration').offset().top;
			$('html,body').animate({scrollTop:st+'px'},500);
		}
	},3000);
	
	
	$('.sfy-item').click(function() {
		
		var url = $(this).find('a').eq(0).attr('href');
		console.log(url);
		location.href = url;
		
		
	});
	
	if($('.domain-search-results').length)
	{
		setTimeout(function(){
			var t = $('.domain-search-results').offset().top;
			t -= 80;
			$('html,body').animate({scrollTop:t+'px'},500);
			
		},1000);
		
	}
	
	
	$('.modal .isp-order-form [type=text]').mask('999.999.999.999');
	
	function count_basket_row()
	{
		var total = 0;
		$('.cart-item').each(function() {
			
			var total2 = 0;
			$(this).find('.cart-detalization-items .td-price').each(function(){
				
				total2 += parseInt($(this).attr('data-price'));
				
			});
			
			$(this).find('.cdb-total-price').attr('data-price',total2);
			$(this).find('.cdb-total-price').html(total2+' <span><span class="rub"></span></span>');
			
			total += total2;
		});
		
		$('#cart-total-price').html(total);
	}
	
	
	$('.cart-item__col3 input').change(function() {
		
		var v = parseInt($(this).val());
		
		var p = $(this).parents('[data-id]');
		var bid = p.attr('data-id');
		
		var price = parseInt(p.find('.cart-item__price').attr('data-price'));
		
		var total = price * v;
		
		p.find('.item__total_price_price').attr('data-price',total);
		p.find('.item__total_price_price').html(number_format(total,0,'.',' ')+' <span><span class="rub"></span></span>');
		
		p.find('.det-price-main').attr('data-price',total);
		p.find('.det-price-main').html(number_format(total,0,'.',' ')+' <span><span class="rub"></span></span>');
		
		p.find('.cdb-total-price').attr('data-price',total);
		p.find('.cdb-total-price').html(number_format(total,0,'.',' ')+' <span><span class="rub"></span></span>');
		
		$.get('/personal/cart/ajax-update.php?id='+bid+'&cnt='+v,function(data){});
		
		count_basket_row();
	});
	
	$('.cart-detalization-items .sub_counter input').change(function(){
		
		var v = parseInt($(this).val());
		
		var p = $(this).parents('[data-id]').eq(0);
		var bid = p.attr('data-id');
		
		var price = parseInt(p.attr('data-price'));
		var m = parseInt(p.attr('data-months'));
		
		var new_price = v * price;
		
		p.find('.td-price').attr('data-price',new_price);
		p.find('.td-price').html(new_price+' <span><span class="rub"></span>/'+m+' мес</span>');
		
		$.get('/personal/cart/ajax-update.php?id='+bid+'&cnt='+v,function(data){});
		
		count_basket_row();
		
	});
	
	
	if($('#prop_SERVER').val())
	{
	
		$('#configuration').css('opacity',0.1);
		var server = $('#prop_SERVER').val();
		var select_kam1;
		var select_kam2;
		var select_kam3;
		
		var st = $('#configuration').offset().top;
		$('html,body').animate({scrollTop:st+'px'},500);
		
		setTimeout(function(){
			
			$('#select_kam1 option').each(function() {
				if(server == $(this).text())
				{
					select_kam1 = $(this).val();
				}
			});
			//alert(select_kam1);
			
			if(select_kam1)
			{
				$('#select_kam1').val(select_kam1);
				$('#select_kam1').change();
				$('#select_kam1').trigger('change');
				$('#select_kam1').selectBox('destroy');
				$('#select_kam1').selectBox();
				setTimeout(function() {
					
					var cpu = $('#prop_CPU').val();
					
					$('#select_kam2 option').each(function() {
					
						var opt = $(this).text().trim().split('   ');
						opt = opt[0];
					
						if(cpu == opt)
						{
							select_kam2 = $(this).val();
						}
					});
					
					$('#select_kam2').val(select_kam2);
					$('#select_kam2').change();
					$('#select_kam2').trigger('change');
					$('#select_kam2').selectBox('destroy');
					$('#select_kam2').selectBox();
					
					
					setTimeout(function(){
					
						var raid = $('#prop_RAID').val();
						console.log(raid);
						
						$('#select_kam3 option').each(function() {
						
							var opt = $(this).text().trim();
							opt = opt.replace(/\s\s+/g, ' ');
							console.log(opt);
							if(raid == opt)
							{
								select_kam3 = $(this).val();
							}
						});
						
						$('#select_kam3').val(select_kam3);
						$('#select_kam3').change();
						$('#select_kam3').trigger('change');
						$('#select_kam3').selectBox('destroy');
						$('#select_kam3').selectBox();
						
						
						setTimeout(function(){
							
							var ram = $('#prop_RAM').val().split(' ');
							ram = parseInt(ram[0]);
							var n = 0;
							$('.slide-grath > div').each(function(i){
								
								var t = $(this).text().trim();
								if(t == ram)
									n = i;
								
							});
						//	$('#sliderRAM').slider('value',n);
						
							hs=$('#sliderRAM').slider();
							hs.slider('option', 'value',n);
							hs.slider('option','slide')
								   .call(hs,null,{ handle: $('.ui-slider-handle', hs), value: n });
							
							
							
							$('.hdd_input').each(function(i){
								
								var val;
								var hdd = $(this).val();
								
								$('[name="hdd-type"]').eq(i).find('option').each(function(){
									
									if(hdd == $(this).text().trim())
									{
										val = $(this).val();
									}
									
								});
								$('[name="hdd-type"]').eq(i).val(val);
								$('[name="hdd-type"]').eq(i).change();
								$('[name="hdd-type"]').eq(i).trigger('change');
								$('[name="hdd-type"]').eq(i).selectBox('destroy');
								$('[name="hdd-type"]').eq(i).selectBox();
								
								
							});
							
							$('[name="hdd-type"]').change();
							
							var months = parseInt($('#prop_MONTHS').val());
							$('.term-block[data-value='+months+']').click();
							
							
								$('#configuration').css('opacity',1);
						},500);
						
						
					},500);
					
					
					
				},500);
			}
			
			
		},2000);
		
	}
	
	
	$('body').on('click','.cart-delete',function(){
		
		var id = $(this).parents('[data-id]').eq(0).attr('data-id');
		
		var p = $(this).parents('tr').eq(0);
		
		if($(this).parent().hasClass('cart-item__col6'))
		{
			p = $(this).parents('.cart-item');
		}
		
		$.get('/personal/cart/ajax-delete.php?id='+id,function(data){
			
			p.remove();
			count_basket_row();
		
			if(!$('.cart-item').length)
			{
				$.get('/personal/cart/',function(data){
					
					var html = $('<div>'+data+'</div>').find('#cart').html();
					$('#cart').html(html);
					
				});
			}
					
		});
		
		return false;
		
	});
	
	
	if($('#microsoft-page').length)
	{

		var form_top = $('#microsoft-page form.order').eq(0).offset().top - 200;

		$(window).scroll(function() {
			
			var st = $(window).scrollTop();
			var st2 = form_top + $('#microsoft-page form.order').eq(0).height() - $(window).height() + 100;
			
			if(st > form_top && st <= st2 && $('.ms-total-block').hasClass('can-be-fixed'))
			{
				$('.ms-total-block').addClass('fixed');
			}
			else
			{
				$('.ms-total-block').removeClass('fixed');
			}
			
			
			
			
		});
	}
	
	$('body').on('change','.location_country',function(){
		
		var c = $(this).val();
		var t = $(this);
		
		$.get('/local/components/vamba/order/templates/.default/ajax-get_cities.php?cid='+c,
		function(data){
			
			t.parent().parent().next().find('.location_city').html(data);
			
		});
		
		
	});
	
	
	$('body').on('keyup','.order-form input,.order-form textarea',function(){
		
		if($(this).val())
		{
			$(this).removeClass('error');
		}
		
	});
	$('body').on('keyup','.order-form select',function(){
		
		if($(this).val())
		{
			$(this).removeClass('error');
		}
		
	});

});


























