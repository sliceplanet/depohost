<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
if (\CHTTP::GetLastStatus()==='404 Not Found')
{
  require_once $_SERVER['DOCUMENT_ROOT'].'/404.php';
}
IncludeTemplateLangFile(__FILE__);
?>
<? if (\CHTTP::GetLastStatus() !== '404 Not Found' && $APPLICATION->GetCurPage(true) != SITE_DIR . 'index.php')
{ ?>
<?
   $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
"AREA_FILE_SHOW" => "page",
"AREA_FILE_SUFFIX" => "inc",
"EDIT_TEMPLATE" => "page_inc.php",
"COMPONENT_TEMPLATE" => ".default",
"COMPOSITE_FRAME_MODE" => "A",
"COMPOSITE_FRAME_TYPE" => "AUTO",
"AREA_FILE_RECURSIVE" => "Y"
),
false,
array(
"ACTIVE_COMPONENT" => "Y"
)
);
?>
<? } ?>
<!--</div> &lt;!&ndash;/.relative-wrapper&ndash;&gt;-->
<footer class="footer">
 <div class="footer-top hidden-xs visible-lg">
  <div class="container">

  <?
     include 'menu.php';
      ?>
  </div>
 </div>
 <div class="footer-bottom">
  <div class="container">
   <div class="row">
    <div class="col-xs-12">
     <?
     $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
      "ROOT_MENU_TYPE" => "footer",
      "MENU_CACHE_TYPE" => "A",
      "MENU_CACHE_TIME" => "360000",
      "MENU_CACHE_USE_GROUPS" => "Y",
      "MENU_CACHE_GET_VARS" => array(),
      "MAX_LEVEL" => "1",
      "CHILD_MENU_TYPE" => "footer",
      "USE_EXT" => "N",
      "DELAY" => "N",
      "ALLOW_MULTI_SELECT" => "N"
      ), false
      );
      ?>
    </div>


   </div>
   <div class="row footer-bottom__middle">
    <div class="col-xs-12 col-lg-4">
     <div class="header__logo text-hide">
      <a href="/" title="На главную">На главную</a>
     </div>
    </div>
    <div class="col-xs-12 col-lg-5">
     <div class="footer__email-wrap">
      <div class="footer__email">
       <p>Отдел по работе с клиентами</p>
       <a href="mailto:info@depohost.ru">info@depohost.ru </a>
      </div>

      <div class="footer__email">
       <p>Тех поддержка</p>
       <a href="mailto:support@depohost.ru">support@depohost.ru </a>
      </div>
     </div>
    </div>
    <div class="col-xs-12 col-lg-3">

     <div class="footer__tel-wrap">

      <div class="footer__tel-block">
       <div class="footer__tel">
        <a href="tel:84957978500">(495) 797-8-500</a>
       </div>
       <div class="footer__tel">
        <a href="tel:88007004036">8-800-700 40 36</a>
       </div>
      </div>
     </div>
    </div>
   </div>
   <div class="row">
    <div class="col-xs-12 col-md-6">
     <div class="footer__copyright">
      &#169; 2019 Depohost. Все права защищены
     </div>
    </div>
   </div>


  </div>
 </div>
</footer>
<div id="callback" class="modal fade" role="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-title">Оставьте данные и мы Вам перезвоним</div>
   </div>
   <div class="modal-body dedic-form">
    <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "callback-header",
    Array(
    "SEF_MODE" => "N",
    "WEB_FORM_ID" => "CALLBACK_HEADER",
    "LIST_URL" => "result_list.php",
    "EDIT_URL" => "result_edit.php",
    "SUCCESS_URL" => "",
    "CHAIN_ITEM_TEXT" => "",
    "CHAIN_ITEM_LINK" => "",
    "IGNORE_CUSTOM_TEMPLATE" => "Y",
    "USE_EXTENDED_ERRORS" => "Y",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "SEF_FOLDER" => "/",
    "VARIABLE_ALIASES" => Array(
    )
    )
    );?>
   </div>

  </div>
 </div>
</div>

<div id="support" class="modal fade" role="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-title">Оставьте данные и мы Вам перезвоним</div>
   </div>
   <div class="modal-body dedic-form">
    <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "support-form",
    Array(
    "SEF_MODE" => "N",
    "WEB_FORM_ID" => "SUPPORT_FORM",
    "LIST_URL" => "result_list.php",
    "EDIT_URL" => "result_edit.php",
    "SUCCESS_URL" => "",
    "CHAIN_ITEM_TEXT" => "",
    "CHAIN_ITEM_LINK" => "",
    "IGNORE_CUSTOM_TEMPLATE" => "Y",
    "USE_EXTENDED_ERRORS" => "Y",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "SEF_FOLDER" => "/",
    "VARIABLE_ALIASES" => Array(
    )
    )
    );?>
   </div>
  </div>

 </div>
</div>


  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=latin,cyrillic&font-display=swap"
        rel="stylesheet"
        type="text/css">

<?
$APPLICATION->IncludeFile('/include/counters.php',
array(),
array('MODE' => 'php', 'NAME' => 'счетчики')
);
include_once $_SERVER['DOCUMENT_ROOT'].'/include/jivo-client-contacts.php';
?>
<a href="#" class="up"></a>
</body>
</html>
