<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arUrls = Array(
  "delete" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=delete&id=#ID#",
  "delay" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=delay&id=#ID#",
  "add" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=add&id=#ID#",
  "delete_all" => $APPLICATION->GetCurPage() . "?delete=all",
);

$arBasketJSParams = array(
  'SALE_DELETE' => GetMessage("SALE_DELETE"),
  'SALE_DELAY' => GetMessage("SALE_DELAY"),
  'SALE_TYPE' => GetMessage("SALE_TYPE"),
  'TEMPLATE_FOLDER' => $templateFolder,
  'DELETE_URL' => $arUrls["delete"],
  'DELAY_URL' => $arUrls["delay"],
  'ADD_URL' => $arUrls["add"]
);
?>
  <script type="text/javascript">
    var basketJSParams = <?= CUtil::PhpToJSObject($arBasketJSParams); ?>
  </script>
  <h1 class="h1" id="pagetitle"><?= $APPLICATION->ShowTitle(false); ?></h1>
<?
$APPLICATION->AddHeadScript($templateFolder . "/script.js");


if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
  ?>
  <div id="warning_message">
    <?
    if (is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"]))
    {
      foreach ($arResult["WARNING_MESSAGE"] as $v)
      {
        echo ShowError($v);
      }
    }
    ?>
  </div>
  <?
  $normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
  $normalHidden = ($normalCount == 0) ? "style=\"display:none\"" : "";

  $delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
  $delayHidden = ($delayCount == 0) ? "style=\"display:none\"" : "";

  $subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
  $subscribeHidden = ($subscribeCount == 0) ? "style=\"display:none\"" : "";

  $naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
  $naHidden = ($naCount == 0) ? "style=\"display:none\"" : "";
  ?>
  <div id="basket_form_container">
    <div class="bx_ordercart">
      <?
      include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items.php");
      ?>
    </div>
    <!--.bx_ordercart-->
  </div>
  <!--#basket_form_container-->
  <?
}
elseif ($normalCount <= 0)
{
  //ShowError(GetMessage('SALE_EMPTY_BASKET'));

  ?>
  <p><span class="errortext"><?= GetMessage('SALE_EMPTY_BASKET') ?></span></p>
  <?

}
else
{
  ShowError($arResult["ERROR_MESSAGE"]);
}
?>
  <h2 class="cart-add-title"><?= GetMessage('ADD_SERVICE') ?></h2>
  <div class="category-menu">
  <? $APPLICATION->IncludeFile(
    $APPLICATION->GetTemplatePath("include_areas/catalog_icons.php"),
    Array(),
    Array("MODE" => "html")
  ); ?>
  </div>

