<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (array_key_exists('delete', $_REQUEST) && $_REQUEST['delete'] == 'all')
{
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
    LocalRedirect($APPLICATION->GetCurPage());
}
?>