<?
$MESS["CATALOG_BUY"] = "Заказать";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_TITLE"] = "Наименование";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["RUB"] = "руб.";
$MESS["PLACEHOLDER_IP_FIELDS"] = "Введите IP адрес сервера";
$MESS['CMP_TPL_CATSEC_TABLE_HEAD_NAME'] = 'Название';
$MESS['CMP_TPL_CATSEC_TABLE_HEAD_PERIOD'] = 'Срок аренды';
$MESS['CMP_TPL_CATSEC_TABLE_HEAD_PRICE'] = 'Цена';
$MESS['CMP_TPL_CATSEC_TABLE_HEAD_ORDER'] = 'Заказ';
$MESS['CMP_TPL_CATSEC_MEASURE_NAME'] = '1 шт';
?>