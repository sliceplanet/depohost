<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//Make all properties present in order
//to prevent html table corruption
foreach ($arResult["ITEMS"] as $key => $arElement)
{
  $arRes = array();
  foreach ($arParams["PROPERTY_CODE"] as $pid)
  {
    $arRes[$pid] = CIBlockFormatProperties::GetDisplayValue($arElement, $arElement["PROPERTIES"][$pid], "catalog_out");
  }
  foreach ($arElement["PRICES"] as $code => $arPrice)
  {
    $arPrice['PRINT_VALUE'] = $arPrice['VALUE'] . ' P';
    $arPrice['PRINT_DISCOUNT_VALUE'] = $arPrice['DISCOUNT_VALUE'] . ' P';
    $arResult["ITEMS"][$key]["PRICES"][$code] = $arPrice;
  }

  $arResult["ITEMS"][$key]['IS_ENDLESS'] = (stripos($arElement['NAME'], 'вечная')) !== false ? true : false;

	$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"] = $arRes;
}

$arResult['MAX_VALUE'] = 100;
?>