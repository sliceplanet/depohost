<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="container">
  <table class="hidden-xs server hosting">
    <tr>
      <th>Тарифные планы</th>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <?
        $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
        ?>
        <th class="tarif-title" id="<?= $this->GetEditAreaId($arElement['ID']); ?>"><?= $arElement["NAME"] ?></th>
      <? endforeach ?>
    </tr>
    <tr>
      <td><span class="group-title">Общие свойства</span></td>
      <td colspan="<?= count($arResult["ITEMS"]) ?>"></td>
    </tr>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] == 'CMN'):
        ?>
        <tr>
          <td>
            <div class="text-bold"><?= $arProperty['NAME'] ?></div>
          </td>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info" style="text-align:center;">
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] != 'CMN'):
        ?>
        <tr>
          <td>
            <div class="text-bold"><?= $arProperty['NAME'] ?></div>
          </td>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info<? if (@$uflag[0] == 'PLS')
            {
              echo ' plus';
            } ?>" style="text-align:center;">
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <tr>
      <td><span class="group-title">Цена за месяц</span></td>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td class="server-price">
          <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
            <? if ($arPrice = $arElement["PRICES"][$code]): ?>
              <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                <s><?= $arPrice["PRINT_VALUE"] ?></s><br/><span
                    class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
              <? else: ?>
                <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
              <? endif ?>
            <? else: ?>
              &nbsp;
            <? endif; ?>
          <? endforeach; ?>
        </td>
      <? endforeach ?>
    </tr>
    <tr class="server-btn">
      <td></td>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td>
          <? if (count($arResult["PRICES"]) > 0): ?>
            <a href="<? echo $arElement["BUY_URL"] ?>" rel="nofollow" class="btn"><? echo GetMessage("CATALOG_BUY") ?></a>
          <? endif; ?>
          &nbsp;
        </td>
      <? endforeach ?>
    </tr>
  </table>
  <div class="visible-xs">
    <div class="mobile_tariffs">
      <div class="mobile_tariffs__title">Тарифные планы</div>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
      <div class="mobile_tariffs__item item_list__info">
        <div class="mobile_tariffs__item_name"><?=$arElement['NAME']?></div>
        <?
        foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
            ?>
          <div class="item_list__info__row">
            <div class="item_list__info__row_label"><?= $arProperty['NAME'] ?></div>
            <div class="item_list__info__row_value"><?= $arProperty['VALUE'] ?></div>
          </div>
          <?
        endforeach
        ?>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label">Цена за месяц</div>
          <div class="item_list__info__row_value">
            <?= str_replace(' ','&nbsp;',number_format($arElement['MIN_PRICE']['VALUE'],0,'.',' ')).' Р'?>
          </div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"></div>
          <div class="item_list__info__row_value"><a id="btn-m<?= $arElement['ID'] ?>" href="<?= $arElement['BUY_URL'] ?>" rel="nofollow"
                                                     class="btn">Заказать</a></div>
        </div>
      </div>
      <? endforeach ?>
    </div>
  </div>
</div>
