<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
  <table class="hidden-xs server hosting">
    <tr>
      <th></th>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <?
        $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
        ?>
        <th class="tarif-title" id="<?= $this->GetEditAreaId($arElement['ID']); ?>"><?= $arElement["NAME"] ?></th>
      <? endforeach ?>
    </tr>
    <tr>
      <td colspan="<?= count($arResult["ITEMS"]) + 1 ?>"><span class="group-title">Общие свойства</span></td>
    </tr>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] == 'CMN'):
        ?>
        <tr>
          <td>
            <div class="text-bold"><?= $arProperty['NAME'] ?></div>
          </td>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info" style="text-align:center">
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <tr>
      <td colspan="<?= count($arResult["ITEMS"]) + 1 ?>"><span class="group-title">Дополнительные свойства</span></td>
    </tr>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] != 'CMN'):
        ?>
        <tr>
          <td>
            <div class="text-bold"><?= $arProperty['NAME'] ?></div>
          </td>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info<? if (@$uflag[0] == 'PLS')
            {
              echo ' plus';
            } ?>" style="text-align:center">
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <tr>
      <td><span class="group-title">Цена за месяц</span></td>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td class="server-price">
          <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
            <? if ($arPrice = $arElement["PRICES"][$code]): ?>
              <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                <s><?= $arPrice["PRINT_VALUE"] ?></s><br/><span
                    class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
              <? else: ?>
                <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
              <? endif ?>
            <? else: ?>
              &nbsp;
            <? endif; ?>
          <? endforeach; ?>
        </td>
      <? endforeach ?>
    </tr>
    <tr>
      <td></td>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td>
          <select name="quantity<?= $arElement['ID'] ?>" id="quantity<?= $arElement['ID'] ?>" class="hosting-select">
            <option value="1">1 месяц</option>
            <option value="3">3 месяца</option>
            <option value="6">6 месяцев</option>
            <option value="12">12 месяцев</option>
          </select>
        </td>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#quantity<?= $arElement['ID'] ?>").change(function () {
              $("#btn<?= $arElement['ID'] ?>").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + $("#quantity<?= $arElement['ID'] ?>").val() + "&months=" + $("#quantity<?= $arElement['ID'] ?>").val());
            });
          });
        </script>
      <? endforeach ?>
    </tr>
    <tr class="server-btn">
      <td></td>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td>
          <? if (count($arResult["PRICES"]) > 0): ?>
            <a href="<?= $arElement['BUY_URL'] ?>" rel="nofollow" class="btn"
               id="btn<?= $arElement['ID'] ?>"><? echo GetMessage("CATALOG_BUY") ?></a>
            &nbsp;
          <? endif; ?>
        </td>
      <? endforeach ?>
    </tr>
  </table>
  <div class="table_wrapper__mobile visible-xs">
  <table class="server hosting">
    <tr>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <?
        $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
        ?>
        <th class="tarif-title" id="<?= $this->GetEditAreaId($arElement['ID']); ?>"><?= $arElement["NAME"] ?></th>
      <? endforeach ?>
    </tr>
    <tr>
      <td colspan="<?= count($arResult["ITEMS"]) ?>"><span class="group-title">Общие свойства</span></td>
    </tr>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] == 'CMN'):
        ?>
        <tr>
          <td class="property_label" colspan="<?= count($arResult["ITEMS"]) ?>">
            <?= $arProperty['NAME'] ?>
          </td>
        </tr>
        <tr>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info text-center" >
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <tr>
      <td colspan="<?= count($arResult["ITEMS"]) ?>"><span class="group-title">Дополнительные свойства</span></td>
    </tr>
    <?
    foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty):
      $uflag = explode("_", $arProperty['CODE']);
      if (@$uflag[0] != 'CMN'):
        ?>
        <tr>
          <td class="property_label" colspan="<?= count($arResult["ITEMS"]) ?>">
            <?= $arProperty['NAME'] ?>
          </td>
        </tr>
        <tr>
          <? foreach ($arResult["ITEMS"] as $arElement): ?>
            <td class="server-info<? if (@$uflag[0] == 'PLS')
            {
              echo ' plus';
            } ?>" style="text-align:center">
              <?= $arElement["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE'] ?>
            </td>
          <? endforeach ?>
        </tr>
      <?
      endif;
    endforeach
    ?>
    <tr>
      <td class="property_label" colspan="<?= count($arResult["ITEMS"]) ?>"><span class="group-title">Цена за месяц</span></td>
    </tr>
    <tr>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td class="server-price">
          <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
            <? if ($arPrice = $arElement["PRICES"][$code]): ?>
              <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                <s><?= $arPrice["PRINT_VALUE"] ?></s><br/><span
                    class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
              <? else: ?>
                <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
              <? endif ?>
            <? else: ?>
              &nbsp;
            <? endif; ?>
          <? endforeach; ?>
        </td>
      <? endforeach ?>
    </tr>
    <tr>
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td>
          <select name="quantity<?= $arElement['ID'] ?>_m" id="quantity<?= $arElement['ID'] ?>_m" class="hosting-select">
            <option value="1">1 месяц</option>
            <option value="3">3 месяца</option>
            <option value="6">6 месяцев</option>
            <option value="12">12 месяцев</option>
          </select>
        </td>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#quantity<?= $arElement['ID'] ?>_m").change(function () {
              $("#btn<?= $arElement['ID'] ?>_m").attr("href", "/personal/cart/ajax-add.php?action=BUY&id[]=<?= $arElement['ID'] ?>&quantity[]=" + $("#quantity<?= $arElement['ID'] ?>_m").val() + "&months=" + $("#quantity<?= $arElement['ID'] ?>_m").val());
            });
          });
        </script>
      <? endforeach ?>
    </tr>
    <tr class="server-btn">
      <? foreach ($arResult["ITEMS"] as $arElement): ?>
        <td>
          <? if (count($arResult["PRICES"]) > 0): ?>
            <a href="<?= $arElement['BUY_URL'] ?>" rel="nofollow" class="btn"
               id="btn<?= $arElement['ID'] ?>_ms"><? echo GetMessage("CATALOG_BUY") ?></a>
            &nbsp;
          <? endif; ?>
        </td>
      <? endforeach ?>
    </tr>
  </table>
</div>