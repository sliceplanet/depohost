<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

$this->createFrame()->begin();
require_once 'whois.php';
?>
<div class="domain__wrapper">
  <script type="text/javascript">
    $(document).ready(function () {
      if (window.location.pathname =="/domain/com/") {
        $('.hosting-select').val('com');
        $('.selectBox-label').text('com')
      } else if (window.location.pathname =="/domain/rf/") {
        $('.hosting-select').val('rf');
        $('.selectBox-label').text('rf')
      }
      $('#check_domain').click(function () {
        var val = $("#DOMAIN_NAME").val(),
          expr = new RegExp('^([а-яА-ЯёЁa-zA-Z0-9]([а-яА-ЯёЁa-zA-Z0-9\\-]{0,61}[а-яА-ЯёЁa-zA-Z0-9]))$', 'ig'),
          validDomain = expr.test(val);


        if (val == '') {
          alert('<?=Loc::getMessage('DOMAIN_NAME_EMPTY')?>');
          return false;
        }
        else if (!validDomain) {
          alert('<?=Loc::getMessage('DOMAIN_NAME_NOT_VALID')?>');
          return false;
        }
        else {
          $("#check_domain_form").submit();
          return false;
        }

      });
    });
  </script>
  <form action="#check_domain_form_header" method="get" id="check_domain_form">
    <div class="head">Проверка&nbsp;домена <?= $ar_res['NAME'] ?></div>
    <div class="table__blue">
      <div class="table_row">
        <div class="table_cell"><input name="search_domain" type="text" id="DOMAIN_NAME"
                                       placeholder="<?= GetMessage('CP_CT_PLACEHOLDER_DOMAIN_NAME') ?>"></div>
        <div class="table_cell">
          <select name="domenzone" class="hosting-select domain">
            <?
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                                 "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                         "CODE" => "DOMAIN_BUY"));
            while ($enum_fields = $property_enums->GetNext())
            {
              echo '<option value="' . $enum_fields["VALUE"] . '">' . $enum_fields["VALUE"] . '</option>';
            }
            ?>
          </select>
        </div>
        <div class="table_cell order"><a href="#" class="btn" id="check_domain">Проверить</a></div>
      </div>
    </div>
  </form>
  <?
  if (isset($_REQUEST['search_domain']) && isset($_REQUEST['domenzone']))
  {
    $search_domain = htmlspecialcharsEx($_REQUEST['search_domain']);
    $domenzone = htmlspecialcharsEx($_REQUEST['domenzone']);
    $domain = $search_domain . '.' . $domenzone;
    $domen_result = whois_domen($domain);
    ?>

    <? if ($domen_result): ?>
    <div class="head">
      Домен <?= $domain ?> доступен для регистрации
    </div>
    <div class="table__blue">
      <div class="table_row">
        <div class="table_cell strong hidden-xs">Доступен для регистрации</div>
        <div class="table_cell"><?= $domain ?></div>
        <div class="table_cell">
          <?
          $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                               "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                       "CODE" => "DOMAIN_BUY",
                                                                                       "VALUE" => $_REQUEST['domenzone']));
          while ($enum_fields = $property_enums->GetNext())
          {
            echo (int)$enum_fields["XML_ID"];
          }
          ?>
          <? echo GetMessage("RUB") ?>
        </div>
        <div class="table_cell order"><a
              href="<?= $arResult['URL_BUY'] ?>&DOMAIN_NAME=<?= urlencode($domain) ?>&DOMAIN_ZONE=<?= htmlspecialcharsbx($domenzone) ?>"
              class="btn">Заказать</a></div>
      </div>
    </div>
  <? else: ?>
    <div class="head">
      Домен <?= $domain ?> не доступен для регистрации
    </div>
    <div class="table__blue table__mobile">
      <div class="table_row">
        <div class="table_cell hidden-xs">Не доступен для регистрации</div>
        <div class="table_cell strong"><?= $domain ?></div>
        <div class="table_cell text-center">
          <?
          $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                               "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                       "CODE" => "DOMAIN_BUY",
                                                                                       "VALUE" => $_REQUEST['domenzone']));
          while ($enum_fields = $property_enums->GetNext())
          {
            echo (int)$enum_fields["XML_ID"];
          }
          ?>
          <? echo GetMessage("RUB") ?>
        </div>
        <div class="table_cell text-center">Занят</div>
      </div>
    </div>

  <? endif; ?>
    <div class="head">Также вы можете зарегистрировать</div>
    <div class="table__blue">
      <?
      $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                           "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                   "CODE" => "DOMAIN_BUY"));
      while ($enum_fields = $property_enums->GetNext())
      {
        if ($enum_fields["VALUE"] != $_REQUEST['domenzone'])
        {
          $domain = $search_domain . '.' . $enum_fields["VALUE"];
          $domen_result = whois_domen($domain);
          ?>
          <div class="table_row">
            <div class="table_cell">
              <?
              if ($domen_result != 1)
              {
                echo 'Не доступен для регистрации';
              }
              else
              {
                ?>
                <b style="color:#008A3E">Доступен для регистрации</b>
              <? } ?>
            </div>
            <div class="table_cell domain-name strong"><?= $search_domain ?>.<?= $enum_fields["VALUE"] ?></div>
            <div class="table_cell"><?= (int)$enum_fields["XML_ID"] ?><? echo GetMessage("RUB") ?></div>
            <div class="table_cell order">
              <?
              if ($domen_result != 1)
              {
                echo 'Занят';
              }
              else
              {
                ?>
                <a href="<?= $arResult['URL_BUY'] ?>&DOMAIN_NAME=<?= urlencode($domain) ?>&DOMAIN_ZONE=<?= $enum_fields["VALUE"] ?>"
                   class="btn">Заказать</a>
              <? } ?>
            </div>
          </div>
          <?
        }
      }
      ?>
    </div>
    <?
  }
  ?>
  <? if (!isset($_REQUEST['search_domain']) && !isset($_REQUEST['domenzone'])): ?>
    <div class="table__blue table__mobile">
      <div class="table_head">
        <div class="table_cell">Домен</div>
        <div class="table_cell">Покупка</div>
        <div class="table_cell">Продление</div>
      </div>
      <div class="table_body">
        <?
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                             "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                     "CODE" => "DOMAIN_BUY"));
        $relinkDomains = ['ru' => '/domain/ru/', 'рф' => '/domain/rf/', 'com' => '/domain/com/'];
        while ($enum_fields = $property_enums->GetNext())
        {
          ?>
          <div class="table_row">
            <? if(empty($relinkDomains[$enum_fields["VALUE"]])): ?>
              <div class="table_cell"><?= $enum_fields["VALUE"] ?></div>
            <? else: ?>
              <div class="table_cell"><a href="<?=$relinkDomains[$enum_fields["VALUE"]];?>"><?= $enum_fields["VALUE"] ?></a></div>
            <? endif; ?>
            <div class="table_cell domain_price"><?= (int)$enum_fields["XML_ID"] ?><? echo GetMessage("RUB") ?></div>
            <div class="table_cell domain_name"><?
              $property_enums_LONG = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC",
                                                                        "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'],
                                                                                                "CODE" => "DOMAIN_LONG",
                                                                                                "SORT" => $enum_fields["SORT"]));
              while ($enum_fields_LONG = $property_enums_LONG->GetNext())
              {
                ?>
                <?= (int)$enum_fields_LONG["XML_ID"] ?><? echo GetMessage("RUB") ?>
              <? } ?></div>
          </div>
          <?
        }
        ?>
      </div>
    </div>
  <? endif ?>
</div>