<? use Bitrix\Main\Page\Asset;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$GLOBALS['APPLICATION']->SetPageProperty("NOT_SHOW_TABS", "Y");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
Asset::getInstance()->addJs('/local/js/phpjs/strings/number_format.js');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/pages/ssl-vds.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/pages/ssl-vds-media.css');
?>