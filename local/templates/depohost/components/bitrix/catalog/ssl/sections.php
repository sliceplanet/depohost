<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Loader;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
$this->setFrameMode(true);
?>
<div class="container">
<?
if (isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && $_REQUEST[$arParams["ACTION_VARIABLE"]]=='BUY')
{
    Loader::includeModule('sale');
    Loader::includeModule('catalog');

    $quantity = intval($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
    $props = $_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]];
    $product_id = $_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]];
    $measure = !empty($_REQUEST['measure_name']) ? (string)$_REQUEST['measure_name'] : 'год';

    $ar_res = \CCatalogProduct::GetByIDEx($product_id);
    $arProduct = $ar_res['PRODUCT'];
    $arPrice = current($ar_res['PRICES']);
    $arProperties = $ar_res['PROPERTIES'];

    $arFields = array(
            "PRODUCT_ID" =>$arProduct['ID'],
            "PRODUCT_XML_ID" => $ar_res['XML_ID'],
            "MEASURE_CODE" => $arProduct['MEASURE'],
            "TYPE" => $arProduct['TYPE'],
            "SET_PARENT_ID" => 0,
            "MEASURE_NAME" => $measure,
            "CATALOG_XML_ID" => 0,
            "PRODUCT_PRICE_ID" => 0,
            "PRICE" => intval($arPrice['PRICE']),
            "CURRENCY" => $arPrice['CURRENCY'],
            "QUANTITY" => $quantity,
            "LID" => LANG,
            "MODULE" => "catalog",
            "PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
      );
      $arProps = array();

      foreach ($props as $code => $value)
      {
        $arProps[] = array(
            "NAME" => $arProperties[$code]['NAME'],
            "CODE" => $code,
            "VALUE" => str_replace('&nbps;',' ',trim($value)),
        );
      }


      $arFields["PROPS"] = $arProps;
      $basket_id = CSaleBasket::Add($arFields);
      $APPLICATION->RestartBuffer();
      LocalRedirect($arParams['BASKET_URL']);
}
?>
<?if($arParams["USE_COMPARE"]=="Y")
{?>

<?
$res = CIBlockSection::GetByID(162);
if($ar_res = $res->GetNext())
  echo $ar_res['DESCRIPTION'];
?>

<?
if (!$arParams["FILTER_VIEW_MODE"])
	$arParams["FILTER_VIEW_MODE"] = "VERTICAL";
if ('Y' == $arParams['USE_FILTER'])
{
	if (CModule::IncludeModule("iblock"))
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		);
		if(0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		{
			$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
		}
		elseif('' != $arResult["VARIABLES"]["SECTION_CODE"])
		{
			$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
		}

		$obCache = new CPHPCache();
		if($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
		{
			$arCurSection = $obCache->GetVars();
		}
		else
		{
			$arCurSection = array();
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->GetNext())
				{
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->GetNext())
					$arCurSection = array();
			}

			$obCache->EndDataCache($arCurSection);
		}
	}
?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"ssl",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"SAVE_IN_SESSION" => "N",
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION"
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);?>
<?
}
?>

<?$intSectionID=$APPLICATION->IncludeComponent(
"bitrix:catalog.section", "list", array(
	"IBLOCK_TYPE" => "ssl",
	"IBLOCK_ID" => "30",
	"SECTION_ID" => "162",
	"SECTION_CODE" => "ssl-sertifikaty",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "sort",
	"ELEMENT_SORT_ORDER2" => "asc",
	"FILTER_NAME" => $arParams["FILTER_NAME"],
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "N",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "30",
	"LINE_ELEMENT_COUNT" => "1",
	"PROPERTY_CODE" =>$arParams["LIST_PROPERTY_CODE"],
	"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
	"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
	"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
	"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
	"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
	"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
	"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => $arParams["PRICE_CODE"],
	"COMPATIBLE_MODE" => $arParams["COMPATIBLE_MODE"],
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
	"PRICE_VAT_INCLUDE" => "N",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => $arParams["PAGER_TITLE"],
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	$component
);
?>

<?
}
?>
</div>
