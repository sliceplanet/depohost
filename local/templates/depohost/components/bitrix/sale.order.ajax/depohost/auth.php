<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"]) > 0)
    LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");

if (in_array(11, $USER->GetUserGroupArray()))
{
    ?>
    <p>Вы зарегистрированы и успешно авторизовались. <?= $arUser["ID"] ?></p>
    <p><a href="<?= SITE_DIR ?>">Вернуться на главную страницу</a></p>
    <?
} else
{
    if (isset($_GET['forgot_password']) && $_GET['forgot_password'] == 'yes')
    {
        $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "", array());
    } else
    {
        ?>

        <? $APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array()); ?>

        <?
    }
}
?>
