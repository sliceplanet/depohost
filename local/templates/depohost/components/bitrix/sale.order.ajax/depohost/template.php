<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
  if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
  {
    if (strlen($arResult["REDIRECT_URL"]) > 0)
    {
      $APPLICATION->RestartBuffer();
      if ($_REQUEST['IS_REGISTER'] == 'Y')
      {
        $arResult["REDIRECT_URL"] = $arResult["REDIRECT_URL"] . '&IS_REGISTER=Y';
      }
      ?>
      <script type="text/javascript">
        window.top.location.href = '<?= CUtil::JSEscape($arResult["REDIRECT_URL"]) ?>';
      </script>
      <?
      die();
    }
  }
}
if (!$USER->IsAuthorized() && $_POST['PERSON_TYPE'] == 'AUTH')
{
  $APPLICATION->RestartBuffer();
  ?>
  <script type="text/javascript">
    window.top.location.href = '<?= CUtil::JSEscape('/auth/?backurl=/personal/order/make/') ?>';
  </script>
  <?
  die();
}
\Bitrix\Main\Page\Asset::getInstance()->addJs($templateFolder . "/jquery.maskedinput.min.js");
$APPLICATION->SetAdditionalCSS($templateFolder . "/style_cart.css");

$APPLICATION->SetAdditionalCSS($templateFolder . "/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder . "/style.css");

CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>
<a name="order_form"></a>

<div id="order_form_div" class="order-checkout">
  <div class="container">
    <NOSCRIPT>
      <div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
    </NOSCRIPT>

    <?
    if (!function_exists("getColumnName"))
    {

      function getColumnName($arHeader)
      {
        return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_" . $arHeader["id"]);
      }

    }

    if (!function_exists("cmpBySort"))
    {

      function cmpBySort($array1, $array2)
      {
        if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
        {
          return -1;
        }

        if ($array1["SORT"] > $array2["SORT"])
        {
          return 1;
        }

        if ($array1["SORT"] < $array2["SORT"])
        {
          return -1;
        }

        if ($array1["SORT"] == $array2["SORT"])
        {
          return 0;
        }
      }

    }
    ?>

    <div class="bx_order_make">
      <?
      //no authorizated person
      if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
      {
        if (!empty($arResult["ERROR"]))
        {
          foreach ($arResult["ERROR"] as $v)
          {
            echo ShowError($v);
          }
        }
        elseif (!empty($arResult["OK_MESSAGE"]))
        {
          foreach ($arResult["OK_MESSAGE"] as $v)
          {
            echo ShowNote($v);
          }
        }

        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
      } //authorizated person
      else
      {
      if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y" || !empty($arResult["ORDER_ID"]))
      {
        if (strlen($arResult["REDIRECT_URL"]) == 0)
        {
          include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
        }
      }

      else
      {


      ?>
      <script type="text/javascript">
        function submitForm(val) {
          if (val != 'Y')
            BX('confirmorder').value = 'N';
          var orderForm = BX('ORDER_FORM');
          if ($('#offerta').length && typeof $('#offerta').attr('checked') === 'undefined'
            && $('[name="PERSON_TYPE"]:checked').val() == 1
            && val == 'Y') {
            alert('<?=GetMessage('ERROR_OFFERTA')?>');
            return false;
          }
          if ($('#agree').length && typeof $('#agree').attr('checked') === 'undefined'
            && val == 'Y') {
            alert('<?=GetMessage('ERROR_AGREE')?>');
            return false;
          }
          var formData = new FormData(orderForm);
          var xhr = new XMLHttpRequest();
          xhr.open("POST", orderForm.action, true);

          xhr.onreadystatechange = function() {//Call a function when the state changes.
            if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
              $('#order_form_content').html(this.response);
              BX('order_form_div').classList.remove('loading');
            }
          };
          xhr.send(formData);

          return true;
        }

        function SetContact(profileId) {
          BX("profile_change").value = "Y";
          submitForm();
        }
      </script>

      <?
      if ($_POST["is_ajax_post"] != "Y")
      {
      ?>
      <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="ORDER_FORM"
            enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <div id="order_form_content">
          <?
          }
          else
          {
            $APPLICATION->RestartBuffer();
          }
          if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
          {
            foreach ($arResult["ERROR"] as $v)
            {
              echo ShowError($v);
            }
            ?>
            <script type="text/javascript">
              top.BX.scrollToNode(top.BX('ORDER_FORM'));
            </script>

            <?
            echo '<div>';
          }
          elseif ($USER->IsAuthorized())
          {
            echo '<h2>Идет обработка заказа</h2>'
              . '<div style="display:none">';
          }
          include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php");
          include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php");
          if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
          {
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
          }
          else
          {
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
          }

          include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/related_props.php");

          //include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
          if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
          {
            echo $arResult["PREPAY_ADIT_FIELDS"];
          }

if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
	echo '</div>';
} elseif ($USER->IsAuthorized()) {
	echo '</div>';
}
          ?>

          <script>
            $("#phone input[type=text]").mask("+7 (999) 999-9999");
          </script>
          <?
          if ($_POST["is_ajax_post"] != "Y")
          {
          ?>
        </div>
        <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
        <input type="hidden" name="profile_change" id="profile_change" value="N">
        <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">

        <div class="bx_ordercart_order_pay_center">
          <a href="javascript:void();" onclick="submitForm('Y');
                                return false;" class="btn"><?= GetMessage("SOA_TEMPL_BUTTON") ?>
          </a>
        </div>

      </form>
      <?
      if ($arParams["DELIVERY_NO_AJAX"] == "N")
      {
        ?>
        <div
            style="display:none;"><? $APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
        <?
      } ?>
    </div>

  <?
  if ($USER->IsAuthorized()): ?>
    <script type="text/javascript">
      submitForm('Y');
    </script>
  <?
  endif;

  }
  else
  {
  ?>
    <script type="text/javascript">
      top.BX('confirmorder').value = 'Y';
      top.BX('profile_change').value = 'N';
    </script>
  <?
  die();
  }
  }
  }
  ?>
  </div>
</div>
</div>
