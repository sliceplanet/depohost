<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["CATEGORIES"])):?>
	<div class="title-search-result-wrapper">
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
				<?if($category_id === "all"):?>
          <div class="title-search-all"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a></div>
				<?elseif($arItem['ITEM_ID']):?>
					<div class="title-search-item"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a></div>
				<?endif;?>
			<?endforeach;?>
		<?endforeach;?>
	</div>
<?endif;
?>