<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$currentPage = parse_url($APPLICATION->GetCurPage());

$parentFirstLevel = '';

foreach($arResult as $index => &$item){
	if ($item['DEPTH_LEVEL'] == 1) {
		$parentFirstLevel = $index;
	} else {
		if ($item["LINK"] == $currentPage['path']) {
			$arResult[$parentFirstLevel]["SELECTED"] = true;
		}
	}

	if ( ($item["LINK"] != $currentPage['path']) && ($currentPage['path'] != '/') && ($item["LINK"] == '/') ) {
		$arResult[$index]["SELECTED"] = false;
	}
}
