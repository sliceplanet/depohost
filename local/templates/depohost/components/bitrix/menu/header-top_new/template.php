<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!empty($arResult['FIRST_LEVEL'])):?>
  <menu class="header__top_menu">
    <? foreach ($arResult['FIRST_LEVEL'] as $index => $arFirstLevelItem):?>
      <li class="header__top_menu-item<?= $arFirstLevelItem['SELECTED'] ? ' active' : '' ?>">
        <a class="header__top_menu-link" href="<?=$arFirstLevelItem['LINK']?>"><?=$arFirstLevelItem['TEXT']?></a>
        <? if ($arFirstLevelItem['LINK'] === '/uslugi/' || ($arFirstLevelItem['IS_PARENT'] && !empty($arResult['SECOND_LEVELS'][$index]))): ?>
          <div class="blue-line-menu-icon"></div>
          <div class="header__top_dropdown">
            <?
            if ($arFirstLevelItem['LINK'] === '/uslugi/')
            {
              include $_SERVER['DOCUMENT_ROOT'] . '/local/templates/depohost/menu.php';
            }
            else
            {
              ?>
              <ul class="menu-list menu-list_inner">
                <?
                foreach ($arResult['SECOND_LEVELS'][$index] as $arSecondLevelItem): ?>
                  <li class="menu-item">
                    <a href="<?= $arSecondLevelItem['LINK'] ?>" class="menu-link">
                      <?= $arSecondLevelItem['TEXT'] ?>
                    </a>
                  </li>
                <?endforeach; ?>
              </ul>
              <?
            }
            ?>
          </div>
        <? endif; ?>
      </li>
    <? endforeach; ?>
  </menu>
<? endif ?>