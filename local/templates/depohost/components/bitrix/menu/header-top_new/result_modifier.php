<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$currentPage = parse_url($APPLICATION->GetCurPage());

$parentFirstLevel = '';
$arFirstLevel = [];
$arSecondLevels = [];

foreach($arResult as $index => $item){
	if ($item['DEPTH_LEVEL'] == 1) {
		$parentFirstLevel = $index;
    $arFirstLevel[] = $item;
    end($arFirstLevel);
	} else {
		if ($item["LINK"] == $currentPage['path']) {
			$arResult[$parentFirstLevel]["SELECTED"] = true;
		}
	}

  if ($item['DEPTH_LEVEL'] == 2)
  {
    $arSecondLevels[key($arFirstLevel)][] = $item;
  }

	if ( ($item["LINK"] != $currentPage['path']) && ($currentPage['path'] != '/') && ($item["LINK"] == '/') ) {
		$arResult[$index]["SELECTED"] = false;
	}
}

$arResult['FIRST_LEVEL'] = $arFirstLevel;
$arResult['SECOND_LEVELS'] = $arSecondLevels;
//echo '<pre>';
//var_dump($arResult['FIRST_LEVEL']);
//var_dump($arResult['SECOND_LEVELS']);