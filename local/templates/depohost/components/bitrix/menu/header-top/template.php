<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>


<?if (!empty($arResult)):?>

<menu class="header__top_menu">

 <?foreach($arResult as $kItem => $arItem):?>

 <?php if ( !empty($arResult[$kItem-1]["DEPTH_LEVEL"]) && ($arItem["DEPTH_LEVEL"] > $arResult[$kItem-1]["DEPTH_LEVEL"])
 ): ?>
 <i>&#9660;</i>
 <ul class="header__top_dropdown">
  <?php endif ?>

  <?if ($arItem["SELECTED"]):?>
  <li class="<?php if ($arItem[" DEPTH_LEVEL
  "] == 1): ?>active<?php endif ?><?php if ($arItem["DEPTH_LEVEL"] == 2): ?>active-second<?php endif ?>"><a
  href="<?=$arItem[" LINK"]?>"><?=$arItem["TEXT"]?></a>
  <?php if ($arItem["IS_PARENT"] != 1): ?>
  </li>
  <?php endif ?>
  <?else:?>
  <li><a href="<?=$arItem[" LINK"]?>"><?=$arItem["TEXT"]?></a>
   <?php if ($arItem["IS_PARENT"] != 1): ?>
  </li>
  <?php endif ?>
  <?endif?>

  <?php if ( (!empty($arResult[$kItem+1]["DEPTH_LEVEL"]) && ($arItem["DEPTH_LEVEL"] >
  $arResult[$kItem+1]["DEPTH_LEVEL"])) || (!isset($arResult[$kItem+1]["DEPTH_LEVEL"]) && ($arItem["DEPTH_LEVEL"] > 1))
  ): ?>
 </ul>
 </li>
 <?php endif ?>

 <?endforeach?>

</menu>
<?endif?>