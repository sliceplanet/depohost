<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arParams['~AUTH_RESULT']))
{
    if ($arParams['~AUTH_RESULT']['TYPE'] == 'OK')
    {
        $arParams['~AUTH_RESULT']['MESSAGE'] = GetMessage('AUTH_RESULT_OK');
    }
    elseif($arParams['~AUTH_RESULT']['TYPE'] == 'ERROR')
    {
        $arParams['~AUTH_RESULT']['MESSAGE'] = GetMessage('AUTH_RESULT_ERROR');
    }
}

?>
