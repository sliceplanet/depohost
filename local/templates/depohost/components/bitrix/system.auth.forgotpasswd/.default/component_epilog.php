<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
  die();
}
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 10.09.18
 * Time: 11:09
 */
$APPLICATION->SetTitle("Восстановление");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/auth.css');
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/auth-media.css');
