<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
    //check if user is client
    if (in_array(11, $USER->GetUserGroupArray()))
    {
        ?>
      <a href="<?=$arParams['PROFILE_URL']?>" class="mobile_menu__lk_enter">Личный кабинет</a>
        <?
    }else
    {
        ?>
      <a href="<?=$arParams['REGISTER_URL']?>" class="mobile_menu__lk_enter">Личный кабинет</a>

        <?
    }
?>
