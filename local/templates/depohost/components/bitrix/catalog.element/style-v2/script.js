/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 18.10.15.
 */
jQuery(document).ready(function($) {
  $(".field select").selectBox();
  var price = $("[name=sum]").val();
  $("[name=months]").on("change", function(event) {
    var months = $(this).val();
    var sum = parseInt(price) * parseInt(months);
    var formatSum = number_format(sum, 0, ",", " ");
    $("#sum").html(formatSum + '<span class="rub"></span>');
    $("[name=sum]").val(sum);
  });
});
