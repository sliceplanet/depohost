<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="order_form_div">
  <? if ($arResult["total_sum"] > 0): ?>
    <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

    <?= $arResult["FORM_NOTE"] ?>

    <? if ($arResult["isFormNote"] != "Y")
    {
      ?>
      <?= $arResult["FORM_HEADER"] ?>

      <?
      if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
      {

      } // endif
      ?>
      <h1 class="kp-title">Получить коммерческое предложение</h1>
        <p>Введите, пожалуйста, ваш контактный e-mail на который вы желаете получить коммерческое предложение.</p>
      <p>
        <div class="kp-summ">Ваша сумма заказа: <?= $arResult["total_sum"] ?></div>
      </p>
      <?
      foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
      {
        if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
        {
          echo $arQuestion["HTML_CODE"];
        }
        else
        {
          ?>
          <div>

            <?= str_replace('class="inputtext"', 'placeholder="' . $arQuestion["CAPTION"] . '"', $arQuestion["HTML_CODE"]) ?>
          </div>
          <?
        }
      } //endwhile
      ?>
      <?
      if ($arResult["isUseCaptcha"] == "Y")
      {
        ?>
        <div>
          <b><?= GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?></b>


          <input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/><img
              src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
              width="180" height="40"/>
          <?= GetMessage("FORM_CAPTCHA_FIELD_TITLE") ?><?= $arResult["REQUIRED_SIGN"]; ?>
          <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext"/>
        </div>
        <?
      } // isUseCaptcha
      ?>
      <div class="agree">
        <input type="checkbox" name="agree" value="Y"
               id="agree"<? if (array_key_exists('agree', $_POST)) echo ' checked' ?> required>
        <label for="agree"><?= GetMessage('AGREE') ?></label>
      </div>
      <div class="bx_ordercart_order_pay_center">
        <button type="submit" name="web_form_submit" value="y" class="btn-big"> Получить коммерческое предложение</button>
      </div>

      <?= $arResult["FORM_FOOTER"] ?>
      <?
    } //endif (isFormNote)
    ?>
  <? else: ?>
    <p>Вы не выбрали услугу.</p>
  <? endif; ?>
</div>