/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 11.08.19.
 */
document.addEventListener("DOMContentLoaded", function() {
  var $formCustomService = $(".form-custom-service form");
  if ($formCustomService) {
    $formCustomService.on("onFormAjaxAfterSubmit", function() {
      dataLayer.push({ event: "Form_Submit_Feedback2" });
    });
  }
});
