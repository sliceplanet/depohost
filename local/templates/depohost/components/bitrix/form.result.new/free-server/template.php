<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
?>
<?= str_replace('<form', '<form class="free-trial__form dedic-form form-ajax-new" ', $arResult["FORM_HEADER"]);?>
	<?if ($arResult["isFormErrors"] == "Y"):?>
        <div class="alert alert-danger"><?=$arResult["FORM_ERRORS_TEXT"];?></div>
        <script>
            Recaptchafree.reset();
        </script>
    <?elseif($_REQUEST["formresult"] == "addok"):?>
    	<div class="alert alert-success">Сообщение успешно отправлено!</div>
        <script>
          dataLayer.push({'event': 'Form_Submit_Free_server'});
          Recaptchafree.reset();
        </script>
    <?endif;?>

    
	<label for="name">Имя</label> 
	<?=$arResult["QUESTIONS"]["NAME"]["HTML_CODE"]?>
	<p></p>
	<label for="phone">Телефон</label>
	<?=$arResult["QUESTIONS"]["PHONE"]["HTML_CODE"]?>
	<p></p>
  <?if($arResult['isUseCaptcha']):?>
    <div class="mf-captcha-new">
        <?=$arResult["CAPTCHA"];?>
    </div>
  <?endif?>
 	<input class="btn-green" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit"
             name="web_form_submit"
             value="Получить доступ к тестовому серверу"/>
<script>
    $(function(){
        $("[type=tel], [data-validate=phone]").mask("+7(999)999-9999");
    });
</script>
<?=$arResult["FORM_FOOTER"];?>