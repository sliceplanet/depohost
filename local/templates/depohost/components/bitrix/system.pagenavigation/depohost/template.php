<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
  if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
  {
    return;
  }
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<div class="news-paginator__list">
  <?
  $bFirst = true;

  if ($arResult["NavPageNomer"] > 1):

    if ($arResult["nStartPage"] > 1):
      $bFirst = false;
      if ($arResult["bSavePage"]):
        ?>
        <a class="news-paginator__item"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
      <?
      else:
        ?>
        <a class="news-paginator__item" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
      <?
      endif;
      if ($arResult["nStartPage"] > 2):

        ?>
        <a class="news-paginator__item news-paginator__item_dotts"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>">...</a>
      <?
      endif;
    endif;
  endif;

  do
  {
    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
      ?>
      <a
          class="news-paginator__item active"><?= $arResult["nStartPage"] ?></a>
    <?
    else:
      ?>
      <?if($arResult["nStartPage"] != 1):?>
      <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
      ?> class="news-paginator__item"><?= $arResult["nStartPage"] ?></a>
      <?else:?>
            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>"<?
      ?> class="news-paginator__item"><?= $arResult["nStartPage"] ?></a>
      <?endif;?>
    <?
    endif;
    $arResult["nStartPage"]++;
    $bFirst = false;
  } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

  if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
    if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
      if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
        ?>
        <a class="news-paginator__item news-paginator__item_dotts"
           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>">...</a>
      <?
      endif;
      ?>
      <a class="news-paginator__item" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
    <?
    endif;
  endif;
  ?>
</div>
