<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
  die();
}
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 10.09.18
 * Time: 22:53
 */
foreach ($arResult['ITEMS'] as $key => $arItem)
{
  if (empty($arItem['PREVIEW_PICTURE']))
  {
    $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = '/images/news-default.jpg';
    $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['ALT'] = 'Новости';
  }
  if (empty(trim($arItem["PREVIEW_TEXT"])) && !empty(trim($arItem["DETAIL_TEXT"])))
  {
    $arResult['ITEMS'][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]),150);
  }
  else{
    $arResult['ITEMS'][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]),150);
  }
}

$cp = $this->__component;
if (is_object($cp)) {
   $cp->arResult["NavPageNomer"] = $arResult["NAV_RESULT"]->NavPageNomer;
   $cp->SetResultCacheKeys(array("NavPageNomer"));
}

