<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1 class="h1"><?= $APPLICATION->ShowTitle(false) ?></h1>
<div class="news-wrap">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
  <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news-item">
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
      <img class="news-item__img"
           src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
           alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
           title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
      >
    </a>
    <div class="news-item__right">
      <div class="news-item__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
      <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-item__title"><?echo $arItem["NAME"]?></a>
      <div class="news-item__desc"><?echo $arItem["PREVIEW_TEXT"];?></div>
    </div>
  </div>
<?endforeach;?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  <?=$arResult["NAV_STRING"]?>
<?endif;?>
