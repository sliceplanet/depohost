<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 
//echo '<pre>';
//var_dump($arResult);
//return;
?>
<table width="100%" class="blue">
    <tbody>
        <tr>
            <th width="20" align="right"><?=Loc::getMessage('TH_NUMBER');?></th>
            <th align="left"><?=Loc::getMessage('TH_DOCUMENT');?></th>
        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="5"><?=Loc::getMessage('EMPTY_RESULT')?></td> 
        </tr> 
    <?
else:
    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        $fileName = 'Акт '.$arItem['NAME'].'.pdf';
        ?>
        <tr id="el_<?=$element_id?>">
            <td align="right"><?=($element_id+1)?></td> 
            <td><a href="<?=$arItem['SRC']?>" target="_blank" download="<?=$arItem['NAME']?>"><?=$arItem['NAME']?></a></td>   
        </tr>  
    <?endforeach;
endif;
?>
    </tbody>
</table>    
