<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
//echo '<pre>';
//var_dump($arResult['ITEMS']);
//return;
?>
<table width="600" class="blue">
    <tbody>
        <tr>
            <th><?=Loc::getMessage('TH_NAME');?></th>
<!--            <th>--><?//=Loc::getMessage('TH_PROPERTIES');?><!--</th>-->
            <th width="50"><?=Loc::getMessage('TH_QUANTITY');?></th>
        <?if ($arResult['IS_DEDICATED']):?>
            <th><?=Loc::getMessage('TH_IP_ADDR');?></th>
<!--            <th>--><?//=Loc::getMessage('TH_SERVER_ID');?><!--</th>-->
        <?endif;?>
            <th><?=Loc::getMessage('TH_PRICE');?></th>
            <th><?=Loc::getMessage('TH_SUMM');?></th>

        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="5"><?=Loc::getMessage('EMPTY_RESULT')?></td>
        </tr>
    <?
else:
    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        ?>
        <tr id="el_<?=$element_id?>">
            <td><?=$arItem['NAME']?></td>
<!--            <td>--><?//=implode('<br />',$arItem['PROPERTIES'])?><!--</td>-->
            <td width="50" align="center"><?=$arItem['QUANTITY']?></td>
        <?if ($arResult['IS_DEDICATED']):?>
            <td align="center"><?=implode('<br />',$arItem['IP_ADDR'])?></td>
<!--            <td align="center">--><?//=$arItem['SERVER_ID']?><!--</td>-->
        <?endif;?>
            <td align="right"><?=str_replace(' ','&nbsp;',CurrencyFormat($arItem['PRICE'],'RUB'))?></td>
            <td align="right"><?=str_replace(' ','&nbsp;',CurrencyFormat($arItem['SUMM'],'RUB'))?></td>

        </tr>
    <?endforeach;?>
        <tr>
            <td colspan="<?=$arResult['IS_DEDICATED'] ? 4 : 3;?>" align="right"><?=Loc::getMessage('FOOT_SUMM');?></td>
            <td align="right"><?=str_replace(' ','&nbsp;',CurrencyFormat($arResult['SUMM'],'RUB'))?></td>
        </tr>
    <?
endif;
?>
    </tbody>
</table>
