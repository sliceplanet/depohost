<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;
global $APPLICATION;
Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']))
{
    $message = is_array($arResult['ERRORS']) ? implode("\r\n", $arResult['ERRORS']) : $arResult['ERRORS'];
    ShowError($message);
}
?>
<table class="blue">
    <tbody>
        <tr>
            <th><?=Loc::getMessage('TH_NUMBER');?></th>
            <th><?=Loc::getMessage('TH_DATE');?></th>
            <th><?=Loc::getMessage('TH_DESCRIPTION');?></th>
            <th><?=Loc::getMessage('TH_STATUS');?></th>
            <th><?=Loc::getMessage('TH_SUMM');?></th>
            <?if($arResult['IS_FIZ']===true):?>
                <th><?=Loc::getMessage('TH_TYPE_PAYMENT');?></th>
            <?else:?>
                <th><?=Loc::getMessage('TH_DOCUMENT');?></th>
            <?endif?>
        </tr>
<?
if (empty($arResult['ITEMS'])):?>
        <tr>
            <td align="center" colspan="6"><?=Loc::getMessage('EMPTY_RESULT')?></td>
        </tr>
    <?
else:

    foreach ($arResult['ITEMS'] as $element_id => $arItem):
        $arStatus = current($arItem['STATUS']);
        $fileName = 'Счет '.$arItem['NAME'].'.pdf';
        ?>
        <tr id="el_<?=$element_id?>">
            <td align="right"><?=$arItem['NUMBER']?></td>
            <td align="center"><?=$arItem['DATE']?></td>
            <td><?=$arItem['DESCRIPTION']?></td>
            <td align="center" class="<?=$arStatus['XML_ID']?>"><?=$arStatus['VALUE']?></td>
            <td align="right"><?=str_replace(' ','&nbsp;',CurrencyFormat($arItem['SUMM'],'RUB'))?></td>
        <?if ($arStatus['XML_ID']!='paid'):?>
            <?if($arResult['IS_FIZ']===true):?>
            <td>
                <ul>
                    <?/*?>
                    <li><a href="<?=$APPLICATION->GetCurPage().'?invoice_id='.$element_id.'&invoice_type=bill'?>" target="_blank"><?=Loc::getMessage('BILL')?></a></li>
                    <li><a href="<?=$APPLICATION->GetCurPage().'?invoice_id='.$element_id.'&invoice_type=sb_bill'?>" target="_blank"><?=Loc::getMessage('SB_BILL')?></a></li>
                    <?*/?>
                    <li><a href="<?=$arItem['DOCUMENT']?>" target="_blank" download="<?=$fileName?>"><?=Loc::getMessage('BILL')?></a></li>
                    <li> <a href="<?=$arItem['PAY_URL']?>"><?=Loc::getMessage('EMONEY')?></a></li>
                </ul>
            </td>
            <?else:?>
            <td><a href="<?=$arItem['DOCUMENT']?>" target="_blank" download="<?=$fileName?>"><?=$fileName?></a></td>
            <?endif?>
        <?else:?>
            <td>&nbsp;</td>
        <?endif?>
        </tr>
    <?endforeach;
endif;

?>
    </tbody>
</table>
