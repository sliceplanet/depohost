<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (array_key_exists('FIELDS', $_REQUEST) && array_key_exists('accept', $_REQUEST))
{
    ShowNote(Loc::getMessage('ACCEPT_MESSAGE'));
}

if (empty($arResult['ITEMS'])):
    ?>
    <h5 align="center"><?= Loc::getMessage('EMPTY_RESULT') ?></h5>
<? else:?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/include/monitoring/text.php",
        "EDIT_TEMPLATE" => ""
            ), false
    );
    ?>

        <?
        foreach ($arResult['ITEMS'] as $id => $arItem):
            ?>
    <form action="" method="post">
            <input type="hidden" name="IP[<?= $id ?>]" value="<?= $arItem['IP_ADDRESS'] ?>">
            <table class="blue">
                <tbody>
                    <tr>
                        <th align="left" colspan="3"><?= $arItem['IP_ADDRESS'] ?></th>
                    </tr>
                    <?
                    unset($arItem['IP_ADDRESS']);
                    foreach ($arItem as $contact_type => $arTypes):
                        foreach ($arTypes as $type => $arValues):
                            foreach ($arValues as $i => $value):
                                ?>
                                <tr>
                                    <td>
                    <?= Loc::getMessage('TYPE_NAME_' . $type) ?>&nbsp;<?= Loc::getMessage('CONTACT_NAME_' . $contact_type) ?>&nbsp;<?= ($i + 1) ?>
                                    </td>
                                    <td>
                                        <input type="text" name="FIELDS[<?= $id ?>][<?= $contact_type ?>][<?= $type ?>][<?= $i ?>][contact]" value="<?= $value['contact'] ?>">
                                    </td>
                                    <td>
                                        <select class="select" name="FIELDS[<?= $id ?>][<?= $contact_type ?>][<?= $type ?>][<?= $i ?>][time]">
                                            <option value=""><?= Loc::getMessage('TIME_SELECT_EMPTY_VALUE_NAME') ?></option>
                                            <option value="15" <? if ($value['time'] == 15) echo 'selected' ?>>15 <?= Loc::getMessage('TIME_SELECT_MINUTE_NAME') ?></option>
                                            <option value="30" <? if ($value['time'] == 30) echo 'selected' ?>>30 <?= Loc::getMessage('TIME_SELECT_MINUTE_NAME') ?></option>
                                            <option value="60" <? if ($value['time'] == 60 && $value['contact'] != '') echo 'selected' ?>>1<?= Loc::getMessage('TIME_SELECT_HOUR_NAME') ?></option>
                                        </select>
                                    </td>
                                </tr>
                            <?
                            endforeach;
                        endforeach;
                    endforeach;
                    ?>
                    <?
//        foreach ($arItem['SMS'] as $arType):
//            for($i=0;$i<$arParams['COUNT_CONTACT'];$i++):
//
//            endfor;
//        endforeach;
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3">
                            <!--<input class="btn" type="submit" name="abort" value="<?= Loc::getMessage('ABORT') ?>">-->
                            <input class="btn" type="submit" name="accept" value="<?= Loc::getMessage('ACCEPT') ?>">
                        </th>
                    </tr>
                </tfoot>
            </table>
            <div class="delimiter-horizontal"></div>
            <div class="delimiter-horizontal"></div>
            <div class="delimiter-horizontal"></div>
        </form>
    <? endforeach; ?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/include/monitoring/text-foot.php",
        "EDIT_TEMPLATE" => ""
            ), false
    );
    ?>

<?
endif;
?>