<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

//iblock 21
function get_prod_21_cost($bPropsColumn, $PRICE_FORMATED, $arItemPROPS)
{
    $prop_price = 0;
    $total_prop_price = 0;
    $prod_cost = 0;

    if ($bPropsColumn):
        foreach ($arItemPROPS as $val):
            $prop_price = explode(':_:', $val["NAME"]);
            if (@$prop_price[2] == "free")
            {
                $prop_price = 0;
            } else
            {
                $prop_price = $val["VALUE"];
            }
            $total_prop_price += $prop_price;

        endforeach;
    endif;

    $prod_price = str_replace(' ', '', $PRICE_FORMATED);
    $prod_cost = $prod_price + $total_prop_price;

    return $prod_cost;
}

//iblock 22
function get_prod_22_cost($bPropsColumn, $PRICE_FORMATED, $arItemPROPS, $PRODUCT_ID)
{
    $prop_price = 0;
    $total_prop_price = 0;
    $prod_cost = 0;
    if ($bPropsColumn):
        foreach ($arItemPROPS as $val):
            if ($val["CODE"] == 'PANEL')
            {
                $prop_price = 0;
            } else
            {
                $db_props = CIBlockElement::GetProperty(22, $PRODUCT_ID, array("sort" => "asc"), Array("CODE" => $val["CODE"] . "_PRICE"));
                if ($ar_props = $db_props->Fetch())
                    if ($val["CODE"] == 'OS')
                    {
                        $prop_price = 0;
                        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 22, "CODE" => $val["CODE"]));
                        while ($enum_fields = $property_enums->GetNext())
                        {
                            if ($enum_fields["VALUE"] == $val["VALUE"] && ($enum_fields["ID"] == 13 or $enum_fields["ID"] == 103))
                            {
                                $prop_price = $ar_props["VALUE"];
                            }
                        }
                    } else
                    {
                        $prop_price = $val["VALUE"] * $ar_props["VALUE"];
                        if ($val["CODE"] == 'HDD')
                            $prop_price = $prop_price - ($ar_props["VALUE"] * 10);
                        if ($val["CODE"] == 'IP_QTY')
                            $prop_price = $prop_price - $ar_props["VALUE"];
                    }
            }
            $total_prop_price += $prop_price;
        endforeach;
    endif;
    $prod_cost = $total_prop_price;

    return $prod_cost;
}

//iblock 23
function get_prod_23_cost($bPropsColumn, $PRICE_FORMATED, $arItemPROPS)
{
    $prop_price = 0;
    $total_prop_price = 0;
    $prod_cost = 0;
    if ($bPropsColumn):
        foreach ($arItemPROPS as $val):
            $prop_price = 0;
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 23, "CODE" => 'VAR_WORK'));
            while ($enum_fields = $property_enums->GetNext())
            {
                if ($enum_fields["VALUE"] == $val['VALUE'])
                    $prop_price = (int) $enum_fields["XML_ID"];
            }
            if ($val['CODE'] == 'PRICE_MOS' && $val['VALUE'] == 'да')
            {

                $res = CIBlockProperty::GetByID("PRICE_MOS", 23, false);
                if ($ar_res = $res->GetNext())
                    $prop_price = (int) $ar_res['DEFAULT_VALUE'];
            }

            $total_prop_price += $prop_price;
        endforeach;
    endif;
    $prod_cost = str_replace(' ', '', $PRICE_FORMATED) + $total_prop_price;

    return $prod_cost;
}

//25
function get_prod_25_cost($bPropsColumn, $arItemPROPS)
{
    if ($bPropsColumn):
        foreach ($arItemPROPS as $val):
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => 25, "CODE" => 'DOMAIN_BUY'));
            while ($enum_fields = $property_enums->GetNext())
            {
                if ($enum_fields['VALUE'] == $val["VALUE"])
                    $prod_cost = (int) $enum_fields['XML_ID'];
            }
        endforeach;
    endif;

    return $prod_cost;
}

?>