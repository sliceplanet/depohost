<item turbo="false">
<pubDate><?=date('D, j M Y H:i:s');?> +0300</pubDate>
<link><?=$data["SERVER_NAME"];?><?=$data["DETAIL_PAGE_URL"];?></link>
<title><?=$data["SEO"]["ELEMENT_META_TITLE"];?></title>
<description><?=$data["SEO"]["ELEMENT_META_DESCRIPTION"];?></description>
<turbo:content>
<![CDATA[
  <header>
    <h1><?=$data["NAME"];?></h1>
    <menu>
      <a href="<?=$data["SERVER_NAME"];?>/arenda-dedicated_server/">Аренда выделенного сервера</a>
      <a href="<?=$data["SERVER_NAME"];?>/arenda-virtualnogo-vydelennogo-servera-vds/">Аренда VDS</a>
      <a href="<?=$data["SERVER_NAME"];?>/1c-server/">Аренда сервера для 1C</a>
      <a href="<?=$data["SERVER_NAME"];?>/microsoft/">Аренда Microsoft</a>
      <a href="<?=$data["SERVER_NAME"];?>/ssl-sertifikat/">SSL сертификат</a>
    </menu>
  </header>
  <figure>
    <img src="<?=$data["SERVER_NAME"];?>/local/templates/depohost/turbo/images/server.png">
  </figure>


    <table>
    <tr>
      <td>
        Цена
      </td>
      <td>
        <?=$data["MIN_PRICE"];?> руб.
      </td>
      </tr>
    <tr>
      <td>
        Комплектация
      </td>
      <td>
          <?=$data["PROPERTIES"]["GENERATION"]["ELEMENT_DESC"]["FIELDS"]["NAME"];?> <?=$data["PROPERTIES"]["CPU"]["ELEMENT_DESC"]["FIELDS"]["NAME"];?> <?=$data["PROPERTIES"]["RAM"]["ELEMENT_DESC"]["FIELDS"]["NAME"];?> <?=$data["PROPERTIES"]["RAID"]["ELEMENT_DESC"]["FIELDS"]["NAME"];?>
      </td>
      </tr>
    </table>
      <button formaction="<?=$data["SERVER_NAME"];?><?=$data["DETAIL_PAGE_URL"];?>"
              data-background-color="#00adf7"
              data-color="white"
              data-primary="true">Заказать</button>
    ]]>
    </turbo:content>
</item>