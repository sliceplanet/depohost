<item turbo="true">
<pubDate><?=date('D, j M Y H:i:s');?> +0300</pubDate>
<link><?=$data["SERVER_NAME"];?><?=$data["DETAIL_PAGE_URL"];?></link>
<title><?=$data["PROPERTIES"]["BROWSER_TITLE"]["VALUE"];?></title>
<description><?=$data["SEO"]["ELEMENT_META_DESCRIPTION"];?></description>
<turbo:content>
<![CDATA[
  <header>
    <h1><?=$data["NAME"];?></h1>
    <menu>
      <? if(!empty($data["DETAIL_PICTURE"])): ?>
        <img src="<?=$data["DETAIL_PICTURE"]["SRC"];?>" alt="<?=$data["NAME"];?>">
      <? endif; ?>
      <a href="<?=$data["SERVER_NAME"];?>/arenda-dedicated_server/">Аренда выделенного сервера</a>
      <a href="<?=$data["SERVER_NAME"];?>/arenda-virtualnogo-vydelennogo-servera-vds/">Аренда VDS</a>
      <a href="<?=$data["SERVER_NAME"];?>/1c-server/">Аренда сервера для 1C</a>
      <a href="<?=$data["SERVER_NAME"];?>/microsoft/">Аренда Microsoft</a>
      <a href="<?=$data["SERVER_NAME"];?>/ssl-sertifikat/">SSL сертификат</a>
    </menu>
  </header>
    <?=str_replace('src="','src="'.$data["SERVER_NAME"],$data["DETAIL_TEXT"]);?>
    ]]>
    </turbo:content>
</item>