<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>
<?
require_once('prices.php');
?>
<!DOCTYPE HTML>
<html lang="ru">
<head>
  <title><? $APPLICATION->ShowTitle() ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=latin,cyrillic"
        rel="stylesheet"
        type="text/css">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5K53WZ7');</script>
  <!-- End Google Tag Manager -->
  <? $APPLICATION->ShowHead() ?>
  <?
  // js
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-1.7.2.min.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/script.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/custom.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/form-ajax.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-ui-1.10.4.custom.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/vendors/jquery.maskedinput/jquery.maskedinput.js');
  $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');

  ?><?
  //style
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.selectbox-m.min.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery-ui.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/img-sprite.min.css');

  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/style.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/media.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/template.css');
  $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/custom.css');
  \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/sprites/advantages.css');
  ?>
  <meta name="google-site-verification" content="aDk94lNvFnT4p8MNEn1jvp8TfprTcA38KmDaT1M7FcY" />
</head>
<body class="<?= $APPLICATION->GetProperty('body_class') ?>">
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div class="header_fixed">
    <div class="container">
      <div class="header_fixed__wrapper">
        <div class="header-logo-wrap">
          <div class="header__logo text-hide">
            <? if ($APPLICATION->GetCurDir() !== '/'): ?>
              <a href="/" title="На главную">На главную</a>
            <? endif ?>
          </div>
        </div>
        <div class="header_fixed__menu">
          <?
          $APPLICATION->IncludeComponent("bitrix:menu", "blue-line", array(
          "ROOT_MENU_TYPE" => "fixed",
          "MENU_CACHE_TYPE" => "A",
          "MENU_CACHE_TIME" => "3600",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(),
          "MAX_LEVEL" => "2",
          "CHILD_MENU_TYPE" => "top2l",
          "USE_EXT" => "Y",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
        ), false
        );
        ?>
        </div>
        <div class="header__phone hidden-xs hidden-sm hidden-md">
          <a href="tel:+74957978500" class="">8 (495) 797-85-00</a>
          <a href="tel:88007004036" class="">8 (800) 700-40-36</a>
        </div>
        <div class="private-cab">
          <?
          $APPLICATION->IncludeComponent("bitrix:system.auth.form", "depohost", array(
            "REGISTER_URL" => "/auth/",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "/personal/profile/",
            "SHOW_ERRORS" => "N"
          ), false
          );
          ?>
        </div>
        <div class="header__basket">
            <?
              $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "", array(
              "ROOT_MENU_TYPE" => "top",
              "MENU_CACHE_TYPE" => "A",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(),
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "left",
              "USE_EXT" => "Y",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
              ), false
              );
            ?>
          </div>
        <div class="header__callback hidden-xs hidden-sm hidden-md">
          <button type="button" data-toggle="modal" data-target="#callback" class="btn-green">Заказать звонок</button>
        </div>

      </div>
    </div>
  </div>
<header class="header header-v2">
  <div class="header-blue-line">
    <div class="container">
      <div class="header-blue-line__row">
        <div class="header-blue-line__menu">
          <?
          $APPLICATION->IncludeComponent("bitrix:menu", "blue-line", array(
          "ROOT_MENU_TYPE" => "blue",
          "MENU_CACHE_TYPE" => "A",
          "MENU_CACHE_TIME" => "3600",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(),
          "MAX_LEVEL" => "2",
          "CHILD_MENU_TYPE" => "blue2l",
          "USE_EXT" => "Y",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
        ), false
        );
        ?>

        </div> 
        <div class="private-cab">
          <?
          $APPLICATION->IncludeComponent("bitrix:system.auth.form", "depohost", array(
            "REGISTER_URL" => "/auth/",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "/personal/profile/",
            "SHOW_ERRORS" => "N"
          ), false
          );
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="header__top_wrapper">
    <div class="container">
      <div class="header__top ">
        <div class="header-logo-wrap">
          <div class="header__logo text-hide">
            <? if ($APPLICATION->GetCurDir() !== '/'): ?>
              <a href="/" title="На главную">На главную</a>
            <? endif ?>
          </div>
          <div class="header__slogan hidden-xs visible-sm visible-md hidden-lg">
            <div class="header__slogan_text">
              Ваш надежный хостинг партнер
            </div>
          </div>
        </div>

        <div class="header__tools-desk hidden-xs hidden-sm hidden-md">
          <div class="header__basket">
            <?
              $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "", array(
              "ROOT_MENU_TYPE" => "top",
              "MENU_CACHE_TYPE" => "A",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(),
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "left",
              "USE_EXT" => "Y",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
              ), false
              );
            ?>
          </div>
        </div>
        <div class="header__phone hidden-xs hidden-sm hidden-md">
          <a href="tel:+74957978500" class="">(495) 797-8-500</a>
          <a href="tel:88007004036" class="">8-800-700 40 36</a>
        </div>
        <div class="header__callback hidden-xs hidden-sm hidden-md">
          <button type="button" data-toggle="modal" data-target="#callback" class="btn-green">Заказать звонок</button>
        </div>
        <div class="header__menu_btn hidden-lg"></div>
        <div class="header__menu_close_btn hidden-lg hidden"><span aria-hidden="true">&times;</span></div>
      </div>
    </div>
  </div>
  <div class="header__menu menu-mobile">
    <div class="mobile_menu__phone hidden-lg">
      <div class="mobile_menu__phone-block">
        <a href="tel:+74957978500" class="mobile_menu__phone_moscow"><span
              class="mobile_menu__phone_city_code">(495)</span>797-8-500</a>
        <a href="tel:88007004036" class="mobile_menu__phone_free_line">8-800-700-40-36</a>
      </div>
    </div>
    <div class="mobile_menu__tools hidden-lg">
      <?
      $APPLICATION->IncludeComponent("bitrix:system.auth.form", "mobile", array(
        "REGISTER_URL" => "/auth/",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "/personal/profile/",
        "SHOW_ERRORS" => "N"
      ), false
      );
      ?>
    </div>
    <div class="header__menu_list">
      <div class="container hidden-xs hidden-sm hidden-md ">
        <?
        $APPLICATION->IncludeComponent("bitrix:menu", "header-top_new", array(
          "ROOT_MENU_TYPE" => "top",
          "MENU_CACHE_TYPE" => "A",
          "MENU_CACHE_TIME" => "3600",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(),
          "MAX_LEVEL" => "2",
          "CHILD_MENU_TYPE" => "top2l",
          "USE_EXT" => "Y",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
        ), false
        );
        ?>
      </div>
      <div class="hidden-lg">
        <?
        $APPLICATION->IncludeComponent("bitrix:menu", "header-top", array(
          "ROOT_MENU_TYPE" => "top-mobile",
          "MENU_CACHE_TYPE" => "A",
          "MENU_CACHE_TIME" => "3600",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(),
          "MAX_LEVEL" => "2",
          "CHILD_MENU_TYPE" => "top2l",
          "USE_EXT" => "Y",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
        ), false
        );
        ?>
      </div>
    </div>
  </div>
  <div class="menu-mobile__fade"></div>
</header>
<? if ($APPLICATION->GetDirProperty('IS_SHOWED_TITLE', false, 'N') !== 'N'): ?>
  <div class="title">
    <div class="container">
      <div class="title-wrap">
        <div class="title__icon"></div>
        <h1 class="title__text"><?= $APPLICATION->ShowTitle(false) ?></h1>
      </div>
    </div>
  </div>
<? endif ?>
<?
$APPLICATION->IncludeComponent(
  "bitrix:breadcrumb",
  "style-v2",
  array(
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO",
    "PATH" => "",
    "SITE_ID" => "s1",
    "START_FROM" => "0",
    "COMPONENT_TEMPLATE" => "style-v2"
  ),
  false
);
?>
<div class="container">

  <?
  // Инфотабы
  $APPLICATION->AddBufferContent('ShowTabs'); //Выводим Инфотабы если свойство Страницы/Раздела NOT_SHOW_TABS != Y
  ?>
</div>
<div class="relative-wrapper">