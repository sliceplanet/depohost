<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Itin\Depohost\Exchange1C;
if(!is_array($arParams["GROUP_PERMISSIONS"]))
	$arParams["GROUP_PERMISSIONS"] = array(1);
if(empty($arParams["SITE_LIST"]))
	$arParams["SITE_LIST"] = "";

$arParams["USE_ZIP"] = $arParams["USE_ZIP"]!="N";
$arParams["REPLACE_CURRENCY"] = htmlspecialcharsEx($arParams["REPLACE_CURRENCY"]);
if ($arParams["USE_TEMP_DIR"] !== "Y" && $arParams["USE_TEMP_DIR"] !== "N")
	$arParams["USE_TEMP_DIR"] = (defined("BX24_HOST_NAME")? "Y": "N");
$arParams["INTERVAL"] = IntVal($arParams["INTERVAL"]);
$arParams["FILE_SIZE_LIMIT"] = intval($arParams["FILE_SIZE_LIMIT"]);
if($arParams["FILE_SIZE_LIMIT"] < 1)
	$arParams["FILE_SIZE_LIMIT"] = 200*1024; //200KB

if($arParams["INTERVAL"] <= 0)
	@set_time_limit(0);

$bUSER_HAVE_ACCESS = false;
if(isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
	$arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
	foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
	{
		if(in_array($PERM, $arUserGroupArray))
		{
			$bUSER_HAVE_ACCESS = true;
			break;
		}
	}
}

$type = htmlspecialcharsEx($_REQUEST['type']);
$gzCompressionSupported = (($_GET["mode"] == "query" || $_POST["mode"] == "query")
	&& isset($arParams["GZ_COMPRESSION_SUPPORTED"]) && $arParams["GZ_COMPRESSION_SUPPORTED"] && function_exists("gzcompress"));

ob_start();

$curPage = substr($APPLICATION -> GetCurPage(), 0, 22);
if(strlen($_GET["sessid"]) > 0 && COption::GetOptionString("clients", "secure_1c_exchange", "N") != "Y")
{
	COption::SetOptionString("client", "secure_1c_exchange", "Y");
}
if (!CModule::IncludeModule('clients'))
{
    echo "failure\n".GetMessage("CC_CLIENTS_ERROR_MODULE");
    return;
}
if(!CModule::IncludeModule('sale'))
{
    echo "failure\n".GetMessage("CC_BSC1_ERROR_MODULE");
    return;
}
if($_GET["mode"] == "checkauth" && $USER->IsAuthorized())
{
	if(
		(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
		&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
		&& !defined("BX_SESSION_ID_CHANGE")
	)
	{
		echo "failure\n",GetMessage("CC_BSC1_ERROR_SESSION_ID_CHANGE");
	}
	else
	{
		echo "success\n";
		echo session_name()."\n";
		echo session_id() ."\n";
		echo bitrix_sessid_get()."\n";

		COption::SetOptionString("clients", "export_session_name_".$curPage, session_name());
		COption::SetOptionString("clients", "export_session_id_".$curPage, session_id());
	}
}
elseif(!$USER->IsAuthorized())
{
	echo "failure\n".GetMessage("CC_BSC1_ERROR_AUTHORIZE");
}
elseif(COption::GetOptionString("clients", "secure_1c_exchange", "N") == "Y" && !check_bitrix_sessid())
{
	echo "failure\n".GetMessage("CC_BSC1_ERROR_SOURCE_CHECK");
}
elseif(!$bUSER_HAVE_ACCESS)
{
	echo "failure\n".GetMessage("CC_BSC1_PERMISSION_DENIED");
}
else
{
	if ($arParams["USE_TEMP_DIR"] === "Y")
		$DIR_NAME = $_SESSION["BX_CML2_EXPORT"]["TEMP_DIR"];
	else
		$DIR_NAME = $_SERVER["DOCUMENT_ROOT"]."/".COption::GetOptionString("main", "upload_dir", "upload")."/1c_invoices/";

	$ABS_FILE_NAME = false;
	$WORK_DIR_NAME = false;

	if(isset($_GET["filename"]) && strlen($_GET["filename"]) > 0 && strlen($DIR_NAME) > 0)
	{
		//This check for 1c server on linux
		$filename = preg_replace("#^(/tmp/|upload/1c/webdata)#", "", $_GET["filename"]);
		$filename = trim(str_replace("\\", "/", trim($filename)), "/");

		$io = CBXVirtualIo::GetInstance();
		$bBadFile = HasScriptExtension($filename)
			|| IsFileUnsafe($filename)
			|| !$io->ValidatePathString("/".$filename)
		;

		if(!$bBadFile)
		{
			$filename = trim(str_replace("\\", "/", trim($filename)), "/");

			$FILE_NAME = rel2abs($DIR_NAME, "/".$filename);
			if((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/".$filename))
			{
				$ABS_FILE_NAME = $DIR_NAME.$FILE_NAME;
				$WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/")+1);
			}
		}
	}

	if($_GET["mode"]=="init")
	{
		if ($arParams["USE_TEMP_DIR"] === "Y")
		{
			$DIR_NAME = CTempFile::GetDirectoryName(6, "1c_invoices");
		}
		else
		{
			$DIR_NAME = $_SERVER["DOCUMENT_ROOT"]."/".COption::GetOptionString("main", "upload_dir", "upload")."/1c_invoices/";
			DeleteDirFilesEx(substr($DIR_NAME, strlen($_SERVER["DOCUMENT_ROOT"])));
		}

		CheckDirPath($DIR_NAME);
		if(!is_dir($DIR_NAME))
		{
			echo "failure\n".GetMessage("CC_BSC1_ERROR_INIT");
		}
		else
		{
			$ht_name = $DIR_NAME.".htaccess";
			if(!file_exists($ht_name))
			{
				$fp = fopen($ht_name, "w");
				if($fp)
				{
					fwrite($fp, "Deny from All");
					fclose($fp);
					@chmod($ht_name, BX_FILE_PERMISSIONS);
				}
			}

			$_SESSION["BX_CML2_EXPORT"]["zip"] = $arParams["USE_ZIP"] && function_exists("zip_open");
			if($arParams["USE_TEMP_DIR"] === "Y")
				$_SESSION["BX_CML2_EXPORT"]["TEMP_DIR"] = $DIR_NAME;

			echo "zip=".($_SESSION["BX_CML2_EXPORT"]["zip"]? "yes": "no")."\n";
			echo "file_limit=".$arParams["FILE_SIZE_LIMIT"]."\n";

			if(strlen($_GET["version"]) > 0)
			{
				echo bitrix_sessid_get()."\n";
				echo "version=2.021";
				$_SESSION["BX_CML2_EXPORT"]["version"] = $_GET["version"];
			}
		}
	}
	elseif($_GET["mode"] == "query" || $_POST["mode"] == "query")
	{
		$arFilter = Array();
		$nTopCount = false;

                if(strlen(COption::GetOptionString("clients", "last_export_time_committed_".$type, ""))>0)
                        $arFilter["DATE_MODIFY_FROM"] = ConvertTimeStamp(COption::GetOptionString("clients", "last_export_time_committed_".$type, ""), "FULL");
                COption::SetOptionString("clients", "last_export_time_".$type, time());
                logi($arFilter,'arFilter','log.log');

//		if(strlen($_SESSION["BX_CML2_EXPORT"]["version"]) <= 0)
		$arParams["INTERVAL"] = 0;

		CTimeZone::Disable();
                $GLOBALS['APPLICATION']->RestartBuffer();
                $params = array('type'=>$type,'last'=>true);
                Exchange1C::exportXml($params);
		//$arResultStat = CSaleExport::ExportOrders2Xml($arFilter, $nTopCount, $arParams["REPLACE_CURRENCY"], $bCrmMode, $arParams["INTERVAL"], $_SESSION["BX_CML2_EXPORT"]["version"]);
		CTimeZone::Enable();

	}
	elseif($_GET["mode"]=="success")
	{
		if($_COOKIE[COption::GetOptionString("clients", "export_session_name_".$curPage, "")] == COption::GetOptionString("clients", "export_session_id_".$curPage, ""))
		{
			COption::SetOptionString("clients", "last_export_time_committed_".$type, COption::GetOptionString("clients", "last_export_time_".$type, ""));
			global $CACHE_MANAGER;
			$CACHE_MANAGER->Clean("clients_invoices"); // for real-time orders
			echo "success\n";
		}
		else
			echo "error\n";
	}
	elseif($_GET["mode"] == "file")// new version
	{
		if($ABS_FILE_NAME)
		{
			if(function_exists("file_get_contents"))
				$DATA = file_get_contents("php://input");
			elseif(isset($GLOBALS["HTTP_RAW_POST_DATA"]))
				$DATA = &$GLOBALS["HTTP_RAW_POST_DATA"];
			else
				$DATA = false;

			if(isset($DATA) && $DATA !== false)
			{
				CheckDirPath($ABS_FILE_NAME);
				if($fp = fopen($ABS_FILE_NAME, "ab"))
				{
					$result = fwrite($fp, $DATA);
					if($result === (function_exists("mb_strlen")? mb_strlen($DATA, 'latin1'): strlen($DATA)))
					{
						if($_SESSION["BX_CML2_EXPORT"]["zip"])
							$_SESSION["BX_CML2_EXPORT"]["zip"] = $ABS_FILE_NAME;
						echo "success\n";
					}
					else
					{
						echo "failure\n".GetMessage("CC_BSC1_ERROR_FILE_WRITE", array("#FILE_NAME#"=>$FILE_NAME));
					}
					fclose($fp);
				}
				else
				{
					echo "failure\n".GetMessage("CC_BSC1_ERROR_FILE_OPEN", array("#FILE_NAME#"=>$FILE_NAME));
				}
			}
			else
			{
				echo "failure\n".GetMessage("CC_BSC1_ERROR_HTTP_READ");
			}
		}
	}
	elseif($_GET["mode"] == "import" && $_SESSION["BX_CML2_EXPORT"]["zip"] && strlen($_SESSION["BX_CML2_EXPORT"]["zip"]) > 1)
	{
		if(!array_key_exists("last_zip_entry", $_SESSION["BX_CML2_EXPORT"]))
			$_SESSION["BX_CML2_EXPORT"]["last_zip_entry"] = "";

		$result = CSaleExport::UnZip($_SESSION["BX_CML2_EXPORT"]["zip"], $_SESSION["BX_CML2_EXPORT"]["last_zip_entry"]);
		if($result===false)
		{
			echo "failure\n".GetMessage("CC_BSC1_ZIP_ERROR");
		}
		elseif($result===true)
		{
			$_SESSION["BX_CML2_EXPORT"]["zip"] = false;
			echo "progress\n".GetMessage("CC_BSC1_ZIP_DONE");

		}
		else
		{
			$_SESSION["BX_CML2_EXPORT"]["last_zip_entry"] = $result;
			echo "progress\n".GetMessage("CC_BSC1_ZIP_PROGRESS");
		}
	}
	elseif($_GET["mode"] == "import" && $ABS_FILE_NAME)
	{

            if(file_exists($ABS_FILE_NAME) && filesize($ABS_FILE_NAME)>0)
            {
                if(!is_array($_SESSION["BX_CML2_EXPORT"]) || !array_key_exists("last_xml_entry", $_SESSION["BX_CML2_EXPORT"]))
                        $_SESSION["BX_CML2_EXPORT"]["last_xml_entry"] = "";

                $position = false;
                $loader = new Exchange1C\Invoices;
                $loader->arParams = $arParams;
                $startTime = time();

                $o = new CXMLFileStream;

                $root_xpath = "/".GetMessage("NODE_COMMERCIAL_INFORMATION");
                $o->registerElementHandler($root_xpath."/".GetMessage("NODE_INFORMATION"), array($loader, "isModify"));
                $xpath_property_info = $root_xpath."/".GetMessage("NODE_QUALIFIER")."/".GetMessage("NODE_PROPERTIES")."/".GetMessage("NODE_PROPERTY");
                $o->registerNodeHandler($xpath_property_info, array($loader, "handlerPropertyStatus"));
                $xpath_element = $root_xpath."/".GetMessage("NODE_INFORMATION")."/".GetMessage("NODE_ELEMENTS")."/".GetMessage("NODE_ELEMENT");
                $o->registerNodeHandler($xpath_element, array($loader, "importStatuses"));


                $o->setPosition($_SESSION["BX_CML2_EXPORT"]["last_xml_entry"]);
                if ($o->openFile($ABS_FILE_NAME))
                {
                    while($o->findNext())
                    {
                        if($arParams["INTERVAL"] > 0)
                        {
                            $_SESSION["BX_CML2_EXPORT"]["last_xml_entry"] = $o->getPosition();
                            if(time()-$startTime > $arParams["INTERVAL"])
                            {
                                break;
                            }
                        }
                    }
                }

                if(!$o->endOfFile())
                    echo "progress";
                else
                {
                    $_SESSION["BX_CML2_EXPORT"]["last_xml_entry"] = "";
                    echo "success";
                }
                if(strlen($loader->strError)>0)
                    echo $loader->strError;
                echo "\n";
            }
            else
            {
                echo "failure\n".GetMessage("CC_BSC1_EMPTY_CML");
            }
	}
	else
	{
		echo "failure\n".GetMessage("CC_BSC1_ERROR_UNKNOWN_COMMAND");
	}
}

$contents = ob_get_contents();
ob_end_clean();

    if(toUpper(LANG_CHARSET) != "WINDOWS-1251")
            $contents = $APPLICATION->ConvertCharset($contents, LANG_CHARSET, "windows-1251");

    if ($gzCompressionSupported)
    {
            $contents = gzcompress($contents);

            header("Content-Type: application/octet-stream");
            header("Content-Length: ".(function_exists("mb_strlen")? mb_strlen($contents, 'latin1') : strlen($contents)));
    }
    else
    {
            $str = (function_exists("mb_strlen")? mb_strlen($contents, 'latin1'): strlen($contents));
            if(in_array($_GET["mode"], array("query", "info")) || in_array($_POST["mode"], array("query", "info")))
            {
                    header("Content-Type: application/xml; charset=windows-1251");
                    header("Content-Length: ".$str);
            }
            else
            {
                    header("Content-Type: text/html; charset=windows-1251");
            }
    }

    echo $contents;
    die();
?>
