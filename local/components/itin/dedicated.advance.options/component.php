<?

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;

Loc::loadMessages(__FILE__);


Loader::includeModule('dedicated');
Loader::includeModule('iblock');
$base_price_id = 5; // ID base type price
$IBLOCK_ID = $arParams['IBLOCK_ID'];
// services
$arOrder = array('SORT' => 'ASC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);

if($arParams['TEST_ACCESS'] == 'Y'):
	$arFilter['ID'] = 279;
else:
	$arFilter['!ID'] = 279;
endif;

$db = CIBlockSection::GetList($arOrder, $arFilter, false, array('ID', 'NAME', 'CODE', 'DESCRIPTION'));
while ($ar = $db->Fetch())
{
    $arSections[] = $ar;
}
//printr($arSections);
$arOrder = array('SORT' => 'ASC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);

if($arParams['TEST_ACCESS'] == 'Y'):
	$arFilter['SECTION_ID'] = 279;
else:
	$arFilter['!SECTION_ID'] = 279;
endif;

$arSelect = array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'PROPERTY_PRICE');
$db = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

while ($ar = $db->Fetch())
{
    $arItems[] = $ar;
}
//printr($arItems);
$arResult['ITEMS'] = $arItems;
$arResult['SECTIONS'] = $arSections;

$arResult['MONTHS'] = (int) $_REQUEST['months'];
if ($arResult['MONTHS'] < 1)
    $arResult['MONTHS'] = 1;

$arResult['MONTH_PRICE'] = $_REQUEST['month_price'];

//handle server info
if (array_key_exists('solutionId', $_REQUEST) && (int) $_REQUEST['solutionId'] > 0 && $_REQUEST['type'] == 'solution')
{
    // if ready solutions
    $base_price_id = 5; // ID base type price

    $arResultTemp = Dedicated::getSolution((int) $_REQUEST['solutionId']);

    $arResult = array_merge($arResult,$arResultTemp);
    $arResult['PRICES'] = $arResult['PRICES'][$arResult['MONTHS']];
}
elseif ($_REQUEST['type'] == 'configurator')
{
	
//	echo var_dump($_REQUEST);
//	die;
	
    $arResult['CHARACTERISTICS_STRING'] = htmlspecialcharsbx($_REQUEST['stringCharac']);
    $arResult['PRICES']['SUM'] = (int) $_REQUEST['sum'];

    if (is_array($_REQUEST['values']))
    {
        $arResult['SERVER_ITEMS'] = $_REQUEST['values'];
//        printr($arResult['SERVER_ITEMS']);
        $arResult['NAME'] = $arResult['SERVER_ITEMS']['generation'];
    }
    else
    {
        $APPLICATION->ThrowException(Loc::getMessage('ERROR_INPUT_DATA'));
    }

}

//printr($arResult);



$this->IncludeComponentTemplate();
