<?php use Bitrix\Main\Page\Asset;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/**  
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 15.11.15
 * Time: 20:10
 */
 
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/dedic.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/dedic-media.min.css');
