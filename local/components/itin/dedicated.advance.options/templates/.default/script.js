jQuery(document).ready(function($) {
  function AdvanceOptions() {
    this.getSelected();
    this.months = parseInt($("#months").val());
    this.stringMonths = $("#string-months").val();
    this.priceServer = parseInt($("#price").val());
  }
  AdvanceOptions.prototype.getSelected = function() {
    return $(".services-list select option:selected");
  };

  AdvanceOptions.prototype.recalc = function() {
    var selected = this.getSelected();
    var arPrices = {};
    var values = {};
    var sum = 0;
    $.each(selected, function(i) {
      arPrices[i] = $(this).data("price");
      values[i] = $(this).attr("value");
      sum += arPrices[i];
    });

    this.sum = parseInt(sum * this.months);
    this.arPrices = arPrices;
    this.values = values;
    return sum;
  };

  AdvanceOptions.prototype.showChange = function() {
    var selected = this.getSelected();
    var stringMonths = this.stringMonths;
    var months = this.months;
    var html = "";
    var inputs = "";

    $.each(selected, function(i) {
      if ($(this).attr("value") != "") {
        var name_service = $(this)
          .parents(".item-block")
          .find(".item-name")
          .text();
        var name_value = $(this).text();
        var price = $(this).data("price");
        var sum = price * months;

        html +=
          '<li>\
                        <div class="item-service-name">' +
          name_service +
          '</div>\
                        <div class="item-service-value">' +
          name_value +
          "</div>\
                        <div>Цена в месяц: " +
          formatPrice(price) +
          "</div>";
        if (months > 1) {
          html +=
            "<div>За " +
            months +
            " " +
            stringMonths +
            ": " +
            formatPrice(sum) +
            "</div>";
        }
        html += "</li>";

        inputs +=
          '<input type="hidden" name="services[' +
          i +
          ']" value="' +
          $(this).attr("value") +
          '">';
      }
    });
    if (html.length == 0) {
      html = "<li><div>" + BX.message("SERVICES_LIST_EMPTY") + "</div></li>";
    }
    $(".items-list").html(html);
    $("#services-form-buffer").html(inputs);
  };

  var advanceOptions = new AdvanceOptions();

  $(".select.xl select")
    .selectBox({ mobile: true })
    .change(function() {
      var sumOptions = advanceOptions.recalc();
      var price = $(this)
        .find("option:selected")
        .data("price");
      var priceContainer = $(this)
        .parents(".bottom-container")
        .find(".price");
      priceContainer.html(formatPrice(price));
      advanceOptions.showChange();
      var totalSum =
        (sumOptions + advanceOptions.priceServer) * advanceOptions.months;
      $(".price-block .price-value").html(formatPrice(totalSum));
    });
  $(".item-services-list button").on("click", "", function(event) {
    event.preventDefault();
    $("#add-basket").trigger("submit");
  });
});
