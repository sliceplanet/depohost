<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . "/template.php");
Loader::includeModule('iblock');
if (is_array($arResult['SERVER_ITEMS']))
{
    $GENERATION_IBLOCK_ID = 44;
    foreach ($arResult['SERVER_ITEMS'] as $key => $value)
    {
	//	echo var_dump($value);
	//	echo '<br>';
		
        if (empty($value))
            continue;
        $res = CIBlockProperty::GetByID(strtoupper($key), $GENERATION_IBLOCK_ID);
        $ar = $res->Fetch();
        $arResult['PROPERTY_ARRAY'][strtoupper($key)] = array(
            'NAME' => $ar['NAME'],
            'VALUE' => $value,
        );
    }
}
?>