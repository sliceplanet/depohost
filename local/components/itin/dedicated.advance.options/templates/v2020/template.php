<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']) || empty($arResult['SECTIONS']))
{
    return; // no items, good bye
}
require_once 'functions.php';

$sum = $arResult['MONTH_PRICE'] ?: $arResult['PRICES']['SUM'];//$arResult['PRICES']['SUM'];
$arResult['DISCOUNTS'] = unserialize(\Bitrix\Main\Config\Option::get('dedicated', 'discounts_values'));

// Суммы и скидки по периодам
$arSum = array(
'1m' => $sum,
'3m' => round($sum * 3 - $sum * 3 * $arResult['DISCOUNTS']['CALC_3M'] / 100),
'6m' => round($sum * 6 - $sum * 6 * $arResult['DISCOUNTS']['CALC_6M'] / 100),
'12m' => round($sum * 12 - $sum * 12 * $arResult['DISCOUNTS']['CALC_12M'] / 100),
);
$arDiscount = array(
'1m' => $sum - $arSum['1m'],
'3m' => 3 * $sum - $arSum['3m'],
'6m' => 6 * $sum - $arSum['6m'],
'12m' => 12 * $sum - $arSum['12m'],
);
$arSumInMonth = array(
'1m' => $arSum['1m'],
'3m' => round($arSum['3m'] / 3),
'6m' => round($arSum['6m'] / 6),
'12m' => round($arSum['12m'] / 12),
);

array_price_format($arSum);
array_price_format($arDiscount);
array_price_format($arSumInMonth);


$counter_section_id = [230,240,241,242,243];

//unset($arResult['ITEMS']);

//echo var_dump($arResult['PROPERTIES']);

if(count($arResult['PROPERTIES']['HDD']['VALUE'])):

	$res = CIBlockElement::GetList(
		[],
		['ID'=>$arResult['PROPERTIES']['HDD']['VALUE']],
		false,
		false,
		['ID','NAME']
	);
	while($arr = $res -> Fetch()):
		$hdd[$arr['ID']] = $arr['NAME'];
	endwhile;
	
	if(count($hdd)):
		$arResult['PROPERTIES']['SSD']['DISPLAY_VALUE'] = [];
		$arResult['PROPERTIES']['HDD']['DISPLAY_VALUE'] = [];
		
		$vals = array_count_values($arResult['PROPERTIES']['HDD']['VALUE']);
		foreach($hdd as $k => $v):
			if(strstr($v,'SSD')):
				$arResult['PROPERTIES']['SSD']['DISPLAY_VALUE'][] = $vals[$k].' x '.$v;
			else:
				$arResult['PROPERTIES']['HDD']['DISPLAY_VALUE'][] = $vals[$k].' x '.$v;
			endif;
		endforeach;
		$arResult['PROPERTIES']['HDD']['DISPLAY_VALUE'] = implode(',<br>',$arResult['PROPERTIES']['HDD']['DISPLAY_VALUE']);
		$arResult['PROPERTIES']['SSD']['DISPLAY_VALUE'] = implode(',<br>',$arResult['PROPERTIES']['SSD']['DISPLAY_VALUE']);
	endif;
	
endif;

if($arResult['PROPERTY_ARRAY']['HDD']['VALUE']):
	$t = explode(';',$arResult['PROPERTY_ARRAY']['HDD']['VALUE']);
	$ssd = [];
	$hdd = [];
	foreach($t as $v):
		if(strstr($v,'SSD')):
			$ssd[] = $v;
		else:
			$hdd[] = $v;
		endif;
	endforeach;
	$arResult['PROPERTY_ARRAY']['HDD']['VALUE'] = implode(', ',$hdd);
	$arResult['PROPERTY_ARRAY']['SSD']['VALUE'] = implode(', ',$ssd);
endif;


?>

<div class="head-block">
	<div class="container">
		<?= Loc::getMessage('CALC_SERVER_NAME', array('#NAME#' => $arResult['NAME'])) ?>
	</div>
</div>
<div class="bx_breadcrumbs v2020 hidden-xs mb30">
	<div class="container">
		<ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
				<a itemtype="http://schema.org/Thing" itemprop="item" href="/" title="Главная">
					<span itemprop="name">Главная</span>
				</a>
				<meta itemprop="position" content="1">
			</li>
			<?if($arParams['TEST_ACCESS'] == 'Y'):?>
				<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a itemtype="http://schema.org/Thing" itemprop="item" href="/free-hosting/test-access/">
						<span itemprop="name">Тестовый доступ</span>
					</a>
				<meta itemprop="position" content="2">
				</li>
			<?else:?>
				<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a itemtype="http://schema.org/Thing" itemprop="item" href="/arenda-dedicated_server/">
						<span itemprop="name">Выделенный сервер</span>
					</a>
				<meta itemprop="position" content="2">
				</li>
			<?endif;?>
			<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
				<a itemtype="http://schema.org/Thing" itemprop="item" href="#" class="current">
					<span itemprop="name"><?=$arResult['NAME']?></span>
				</a>
			<meta itemprop="position" content="2">
			</li>
		</ul>
	</div>
</div>
<div id="<? echo $arItemIDs['ID']; ?>" class="server-order-detail">
	<div class="container">
		<h1>Подбор дополнительных услуг</h1>
		<p class="sod-desc"><?=$arResult['PREVIEW_TEXT']?></p>
		
		<?//=var_dump($arResult['PROPERTY_ARRAY']);?>

		<div class="row">
			<div class="col-md-8">
			
				<div class="sod-main_info">
					<div class="sod-block_h">Оснащение сервера</div>
					<div class="sod-main_block">
						<div class="sod-main_block__top">
							<div class="sod-main_block__price">
								<?=str_replace('<span class="rub"></span>','<span><span class="rub"></span>/мес.</span>',$arResult['MIN_PRICE']['PRINT_VALUE'])?>
							</div>
							<div class="sod-main_block__info">
								<div class="server-code"><?=$arResult['NAME']?></div>
								<div class="server-name"></div>
								<div class="server-desc"><?=$arResult['PREVIEW_TEXT']?></div>
							</div>
						</div>
						<div class="sod-main_block__bottom">
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">CPU</div>
										<div class="prop_value"><?=$arResult['PROPERTY_ARRAY']['CPU']['VALUE'] ?: $arResult['PROPERTIES']['CPU']['DISPLAY_VALUE']?></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">RAM</div>
										<div class="prop_value"><?=$arResult['PROPERTY_ARRAY']['RAM']['VALUE'] ?: $arResult['PROPERTIES']['RAM']['DISPLAY_VALUE']?></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">HDD</div>
										<div class="prop_value"><?=$arResult['PROPERTY_ARRAY']['HDD']['VALUE'] ?: $arResult['PROPERTIES']['HDD']['DISPLAY_VALUE']?></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">SSD</div>
										<div class="prop_value"><?=$arResult['PROPERTY_ARRAY']['SSD']['VALUE'] ?: $arResult['PROPERTIES']['SSD']['DISPLAY_VALUE']?></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="prop_block">
										<div class="prop_name">RAID</div>
										<div class="prop_value"><?=$arResult['PROPERTY_ARRAY']['RAID']['VALUE'] ?: $arResult['PROPERTIES']['RAID']['DISPLAY_VALUE']?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			
				<div class="sod-addional_items">
				
					<div class="sod-block_h">Дополнительные услуги</div>
					<div class="sod-addional_items_wrapper">
						<?foreach ($arResult['SECTIONS'] as $arSection):?>
					
							<div class="sod_ai checked_ collapse-parent" data-id="<?=$arSection['ID']?>">
								<div class="sod_ai__top collapse-buttton_block">
									<div class="sod_ai__left collapse-arrow_icon_block">
										<i></i>
										<div>
											<div><span class="sod_ai__name"><?= $arSection['NAME'] ?></span> <span class="hint">?<span class="hint-data"><?= $arSection['DESCRIPTION'] ?></span></span></div>
											<span class="sod_ai__desc">Нет</span>
										</div>
									</div>
									<div class="sod_ai__right">
										<div class="sod_ai__price">0 <span><span class="rub"></span>/мес.</span></div>
									</div>
								</div>	
								<div class="sod_ai__detalization collapse-content">	
									<?if(in_array($arSection['ID'],$counter_section_id)):?>
										<?
										$item = false;
										$sort = 100000;
										foreach ($arResult['ITEMS'] as $arItem):
											if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID']) continue;
											if($arItem['SORT'] < $sort):
												$sort = $arItem['SORT'];
												$item = $arItem;
											endif;
										endforeach;
										?>
										<div class="sod_ai__lic">
											<div class="flex-row jcsb">
												<div class="sod_ai__lic_name_info">
													<div class="sod_ai__lic_name"><?=$item['NAME']?></div>
													<div class="sod_ai__lic_info"><?=$item['PREVIEW_TEXT']?></div>
												</div>
												<div class="sod_ai__lic_counter counter_block" min="1" max="99">
													<div class="counter_block__wrapper">
														<span>количество</span>
														<div class="counter_block__actions" data-id="<?=$item['ID']?>">
															<a href="#" class="cb_minus">-</a>
															<input type="text" value="0" data-price="<?= $item['PROPERTY_PRICE_VALUE'] ?>">
															<a href="#" class="cb_plus">+</a>
															
														</div>
													</div>
												</div>
												<div class="sod_ai__lic_price this-is-price">
													<?= $item['PROPERTY_PRICE_VALUE'] ?> <span class="rub"></span>
												</div>
											</div>
										</div>
									<?else:?>
										<ul class="sod_ai__list">
										<?
										foreach ($arResult['ITEMS'] as $arItem):
											if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID']) continue;
										?> 
											<li>
												<label>
													<input type="radio" name="section[<?= $arSection['ID'] ?>]" value="<?= $arItem['ID'] ?>" data-price="<?= $arItem['PROPERTY_PRICE_VALUE'] ?>" />
													<div class="sod_ai__list_item">
														<div class="sod_ai__list_item_left">
															<span class="checker"><span></span></span>
															<span class="sod_ai__list_item_name"><?= $arItem['NAME'] ?></span>
														</div>
														<div class="sod_ai__list_item_right">
															<div class="sod_ai__list_item_price<?if((int)$arItem['PROPERTY_PRICE_VALUE'] == 0):?> null-price<?endif;?>">
																<?= $arItem['PROPERTY_PRICE_VALUE'] ?> <span><span class="rub"></span>/мес.</span>
															</div>
														</div>
													</div>
												</label>
											</li>
										<? endforeach ?>
										</ul>
									<?endif;?>
								</div>
							</div>
					
					
						<? endforeach ?>
					</div>
					
				</div>
			</div>
			<div class="col-md-4">
				<div class="sod-result_block collapse-parent opened">
				
					<div class="sod_rb__data">
					
						<div class="sod_rb__top collapse-buttton_block">
							<div class="sod_rb__top_left collapse-arrow_icon_block">
								<i></i>
								<span>Итого</span>
							</div>
							<div class="sod_rb__top_right">
								<div id="total_price">
									<?= getPriceFormat($arResult['PRICES']['SUM']) ?> <span><span class="rub"></span>/мес</span>
								</div>
								<div id="total_price_note">при оплате за <?=$arResult['MONTHS']?> мес.</div>
							</div>
							
						</div>
						<div class="sod_rb__bottom collapse-content">
							<div class="sod_rb__order_item">
								<div class="sod_rb__order_item_name"><?=$arResult['NAME']?></div>
								<div class="sod_rb__order_item_price">
									<?= getPriceFormat($arResult['PRICES']['SUM']/$arResult['MONTHS']) ?> <span><span class="rub"></span>/мес</span>
								</div>
							</div>
						</div>
					
					</div>
					
					<div class="order-form-block">
						<?/*<div class="ofb-h">Оформление заказа</div>*/?>
						<form action="/personal/cart/ajax-add.php" method="get">
							<input type="hidden" name="price_1month" value="<?=$arSum['1m']?>" />
							<input type="hidden" id="months" name="months" value="<?= $arResult['MONTHS'] ?>">
							<input type="hidden" id="string-months" value="<?= CUsefull::endOfWord($arResult['MONTHS'], array('месяцев', 'месяц', 'месяца')); ?>">
							<input type="hidden" id="price" name="FIELDS[PRICE]" value="<?= intval($arResult['PRICES']['SUM']/$arResult['MONTHS']) ?>">
							<input type="hidden" name="FIELDS[NAME]" value="<?= urlencode(Loc::getMessage('RENT_SERVER_NAME', array('#NAME#'=>$arResult['NAME']))) ?>">
							<input type="hidden" name="action" value="BUY">
							<?  foreach ($arResult['PROPERTY_ARRAY'] as $key => $arItem):?>
								<input type="hidden" name="PROPS[<?=$key?>][NAME]" value="<?= $arItem['NAME'] ?>">
								<input type="hidden" name="PROPS[<?=$key?>][VALUE]" value="<?= $arItem['VALUE'] ?>">
							<?endforeach;?>
							<input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
							<?if($_REQUEST['type']=='solution' && (int)$arResult['ID']>0):?>
								<input type="hidden" name="product_id" value="<?=(int)$arResult['ID']?>">
							<?endif?>
							<input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
							
							<div id="additional-services">
							
							</div>
							
							<div class="ofb-items" style="display:none;">
								
								<div class="ofb-item">
									<input id="cnt1" type="radio" name="cnt" value="1"<?if($arResult['MONTHS'] == 1):?> checked<?endif;?> data-price="<?=$arSum['1m']?>" />
									<label for="cnt1">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">1 месяц</span>
										<span class="ofb-item_price"><?=number_format($arSum['1m'],0,'.',' ')?> <span class="rub"></span></span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt3" type="radio" name="cnt" value="3"<?if($arResult['MONTHS'] == 3):?> checked<?endif;?> data-price="<?=$arSum['3m']?>" />
									<label for="cnt3">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">3 месяца</span>
										<span class="ofb-item_price"><?=number_format($arSum['3m'],0,'.',' ')?> <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия <?=$arDiscount['3m']?> р</span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt6" type="radio" name="cnt" value="6"<?if($arResult['MONTHS'] == 6):?> checked<?endif;?> data-price="<?=$arSum['6m']?>" />
									<label for="cnt6">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">6 месяцев</span>
										<span class="ofb-item_price"><?=number_format($arSum['6m'],0,'.',' ')?> <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия <?=$arDiscount['6m']?> р</span>
									</label>
								</div>
								<div class="ofb-item">
									<input id="cnt12" type="radio" name="cnt" value="12"<?if($arResult['MONTHS'] == 12):?> checked<?endif;?> data-price="<?=$arSum['12m']?>" />
									<label for="cnt12">
										<span class="ofb-item_checker"><span></span></span>
										<span class="ofb-item_name">12 месяцев</span>
										<span class="ofb-item_price"><?=number_format($arSum['12m'],0,'.',' ')?> <span class="rub"></span></span>
										<span class="ofb-item_economy">Экономия <?=$arDiscount['12m']?> р</span>
									</label>
								</div>
								
							</div>
							<button class="blue-button">Заказать</button>
							
							<a href="#" class="add-to-cart">Отложить в корзину</a>
						
						</form>
					</div>
				
				</div>
			
			</div>
			
		</div>
	</div>
</div>

<?/*

<div class="head-block">
    <?= Loc::getMessage('CALC_SERVER_NAME', array('#NAME#' => $arResult['NAME'])) ?>
</div>
<div class="services-wrapper clearfix">
    <div class="services-list">
        <? foreach ($arResult['SECTIONS'] as $arSection): ?>
            <div class="item-block">
                <div class="item-name"><?= $arSection['NAME'] ?></div>
                <div class="bottom-container clearfix">
                    <div class="select xl pull-left">
                        <select style="display: none;" class="selectBox" name="section[<?= $arSection['ID'] ?>]">
                            <option value="" selected data-price="0"><?= Loc::getMessage('ADVANCE_OPTION_EMPTY_VALUE') ?></option>
                            <?
                            foreach ($arResult['ITEMS'] as $arItem):
                                if ($arItem['IBLOCK_SECTION_ID'] != $arSection['ID'])
                                    continue;
                                ?>
                                <option value="<?= $arItem['ID'] ?>" data-price="<?= $arItem['PROPERTY_PRICE_VALUE'] ?>"><?= $arItem['NAME'] ?></option>
    <? endforeach ?>
                        </select>
                    </div>
                    <div class="price">0&nbsp;р</div>
                    <div class="hint-wrapper">
                        <div class="hint-block">
                            <div class="hint-text">
                                <div class="hint-close"></div>
    <?= $arSection['DESCRIPTION'] ?>
                            </div>
                            <div class="hint-corner"></div>
                        </div>
                    </div>
                    <!--.hint-wrapper-->
                </div>
                <!--.bottom-container-->
            </div>
            <!--.item-block-->
<? endforeach ?>
    </div>
    <!--.services-list-->
    <aside class="right-side">
        <div class="item-name"><?= Loc::getMessage('CALC_SERVER_NAME', array('#NAME#' => $arResult['NAME'])) ?></div>
        <div class="item-params">
            <div class="caption underline-title">
<? Loc::getMessage('CHARACTERISTICS_CAPTION') ?>
            </div>
            <div class="descr"><?= Loc::getMessage('CALC_CHARACTERISTICS_STRING', array('#CHARACTERISTICS_STRING#' => $arResult['CHARACTERISTICS_STRING'])) ?></div>
        </div>
        <div class="item-services-list">
            <div class="caption">
<?= Loc::getMessage('CAPTION_ADVANCE_SERVICES') ?>
            </div>
            <ul class="items-list">
                <li><div><?=Loc::getMessage('SERVICES_LIST_EMPTY')?></div></li>
                <!--                <li>
                                    <div class="item-service-name">Администрирование</div>
                                    <div class="item-service-value">Администрирование сервера нашими специалистами</div>
                                </li>-->
            </ul>
            <div class="price-block">
                <div class="price-value"><?= getPriceFormat($arResult['PRICES']['SUM']) ?> р</div>
                <div class="descr"><?= Loc::getMessage('PRICE_BLOCK_DESCRIPTION', array('#MONTHS#' => $arResult['MONTHS'], '#STRING_MONTHS#' => CUsefull::endOfWord($arResult['MONTHS'], array('месяцев', 'месяц', 'месяца')))) ?></div>
            </div>
            <button class="btn-green sm"><?= Loc::getMessage('BUTTON_ADD') ?></button>
        </div>
    </aside>
</div>
<div class="center"></div>
<form id="add-basket" action="/personal/cart/ajax-add.php" method="get">
    <input type="hidden" id="months" name="months" value="<?= $arResult['MONTHS'] ?>">
    <input type="hidden" id="string-months" value="<?= CUsefull::endOfWord($arResult['MONTHS'], array('месяцев', 'месяц', 'месяца')); ?>">
    <input type="hidden" id="price" name="FIELDS[PRICE]" value="<?= intval($arResult['PRICES']['SUM']/$arResult['MONTHS']) ?>">
    <input type="hidden" name="FIELDS[NAME]" value="<?= urlencode(Loc::getMessage('RENT_SERVER_NAME', array('#NAME#'=>$arResult['NAME']))) ?>">
    <input type="hidden" name="action" value="BUY">
    <?  foreach ($arResult['PROPERTY_ARRAY'] as $key => $arItem):?>
        <input type="hidden" name="PROPS[<?=$key?>][NAME]" value="<?= $arItem['NAME'] ?>">
        <input type="hidden" name="PROPS[<?=$key?>][VALUE]" value="<?= $arItem['VALUE'] ?>">
    <?endforeach;?>
    <input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
    <?if($_REQUEST['type']=='solution' && (int)$arResult['ID']>0):?>
        <input type="hidden" name="product_id" value="<?=(int)$arResult['ID']?>">
    <?endif?>

    <input type="hidden" name="type" value="<?= htmlspecialcharsbx($_REQUEST['type'])?>">
    <div id="services-form-buffer"></div>
</form>
<?
$MESS['SERVICES_LIST_EMPTY'] = Loc::getMessage('SERVICES_LIST_EMPTY');
?>
<?
if ($_REQUEST['isAjax'] == 'Y')
{
    echo '<script>BX.ready(function(){';
    
    echo "BX.message(".CUtil::PhpToJSObject( $MESS, false ).");";
    include 'script.js';
    
    echo '});</script>';
    echo '<style>';
    include 'style.css';
    echo '</style>';
}
else
{
    $APPLICATION->AddHeadString('<script>BX.ready(function(){BX.message('.CUtil::PhpToJSObject( $MESS, false ).');});</script>', true);
}
*/
?>
