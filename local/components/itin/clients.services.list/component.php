<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Clients;
use Itin\Depohost\Info;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('clients');
$IBLOCK_ID = Info::getIblockId('ccards');

$user_id = intval($USER->GetID());
$client_ob = new \Itin\Depohost\Clients($user_id);

$client_id = $client_ob->getElementId();

$filter['UF_PERIOD'] = $arParams['SERVICES_TYPE'];
$arServices = $client_ob->getCSList($client_id,$filter);
$summ = 0;

/** @var array $arServicesWithProps Массив xml_id услуг у которых выводим короткое наименование и список всех свойств*/
$arServicesWithProps = array('u7','u2');

foreach ($arServices as $id => $arService)
{
    if (in_array($arService['UF_CATALOG_XML_ID'], $arServicesWithProps))
    {
        $arService['UF_NAME'] = $arService['UF_NAME_SHORT'].'<div style="font-size:80%">'.implode('<br>',$arService['UF_PROPERTIES']).'</div>';
    }
    $arResult['ITEMS'][$id] = array(
        'NAME' => $arService['UF_NAME'],
        'PROPERTIES' => $arService['UF_PROPERTIES'],
        'QUANTITY' => $arService['UF_QUANTITY'],
        'PRICE' => $arService['UF_PRICE'],
        'SUMM' => $arService['UF_SUMM'],
        'IP_ADDR' => $arService['UF_IP_ADDR'],
        'SERVER_ID' => $arService['UF_SERVER_ID'],
    );
    if (!empty($arService['UF_IP_ADDR']) && !empty($arService['UF_SERVER_ID']))
    {
        $arResult['IS_DEDICATED'] = true;
    }
    $summ +=  intval($arService['UF_SUMM']);
}
$arResult['SUMM'] = $summ;

$this->IncludeComponentTemplate();
