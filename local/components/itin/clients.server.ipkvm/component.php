<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Clients;
use Itin\Depohost\Info;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('clients');
$IBLOCK_ID = Info::getIblockId('ccards');

$user_id = intval($USER->GetID());
$client_ob = new Clients($user_id);

$bReboot = false;
if (!empty($_REQUEST['server_id']))
{
    $server_id = intval($_REQUEST['server_id']);
    $bReboot = $client_ob->ipkvm($server_id);
    if ($bReboot)
    {
        $arResult['SUCCESS'][] = Loc::getMessage('REQUEST_SUCCESS');
    }
    else
    {
        $arResult['ERRORS'][] = Loc::getMessage('REQUEST_ABORT');
    }
}

$client_id = $client_ob->getElementId();

$arServices = $client_ob->getDedicatedServers($client_id);
$arResult['ITEMS'] = array();
foreach ($arServices as $id => $arService)
{
    $arResult['ITEMS'][$id] = array(
        'NAME' => $arService['UF_NAME'],
        'PROPERTIES' => $arService['UF_PROPERTIES'],
        'IP_ADDR' => $arService['UF_IP_ADDR'],
        'SERVER_ID' => $arService['UF_SERVER_ID'],
        'ACCESS' => $client_ob->accessIpkvm($id),
    );
}

$arResult['ERROR_ABORT'] = Loc::getMessage('REQUEST_ABORT');
$this->IncludeComponentTemplate();
