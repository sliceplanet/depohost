<?

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Monitoring\Servers;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('monitoring');

$ar_ips = Servers::getUserIPs($USER->GetID());
$arItems = array();
if (!isset($arParams['COUNT_CONTACT']) || (int) $arParams['COUNT_CONTACT'] <= 0)
{
    $arParams['COUNT_CONTACT'] = 2;
}
if (!isset($arParams['DEFAULT_TIME']) || (int) $arParams['DEFAULT_TIME'] <= 0)
{
    $arParams['DEFAULT_TIME'] = 60;
}

if (array_key_exists('abort', $_REQUEST))
{
    reset($_REQUEST['IP']);
    $ip = htmlspecialcharsbx(current($_REQUEST['IP']));
    Servers::cleanSettingsNotify($ip,$USER->GetID());
}
if (array_key_exists('FIELDS', $_REQUEST) && array_key_exists('accept', $_REQUEST))
{
    foreach ($_REQUEST['FIELDS'] as $id => $arContactType)
    {
        $params = array();
        $ip = htmlspecialcharsbx($_REQUEST['IP'][$id]);
        foreach ($arContactType as $contact_type => $arType)
        {
            foreach ($arType as $type => $arValues)
            {
                foreach ($arValues as $arValue)
                {
                    $params[] = array(
                        'type' => $type,
                        'contact' => $arValue['contact'],
                        'time' => (int) $arValue['time'] > 0 ? $arValue['time'] : $arParams['DEFAULT_TIME'],
                        'user_id' => $USER->GetID()
                    );
                }
            }
        }
        Servers::saveSettingsNotify($ip, $params);
    }
}

foreach ($ar_ips as $ip)
{
    $empty_array = array();
    // массив с пустыми значениями по количеству возможных контактов
    $ar_count_contact = array_pad($empty_array, $arParams['COUNT_CONTACT'], '');

    $arItem = array(
        'IP_ADDRESS' => $ip,
        'EMAIL' => array(
            'HTTP' => $ar_count_contact,
            'PING' => $ar_count_contact,
        ),
        'SMS' => array(
            'HTTP' => $ar_count_contact,
            'PING' => $ar_count_contact,
        ),
    );

    $arSettings = Servers::getSettingsNotify($ip, $USER->GetID());


    foreach ($arSettings as $arSetting)
    {
        $key = '';
        if (strpos($arSetting['contact'],'@') !== false)
        {
            //if emails
            $key = 'EMAIL';
        } elseif (strpos($arSetting['contact'],'+7') === 0
                || strpos($arSetting['contact'],'7') === 0
                || strpos($arSetting['contact'],'8') === 0
            )
        {
            //if phone
            $key = 'SMS';
        }
        if ($key != '')
        {
            $temp = each($arItem[$key][$arSetting['type']]);
            $arItem[$key][$arSetting['type']][$temp['key']] = $arSetting;
        }

    }
//    $arItem['SETTINGS'] = $arSettings;

    $arItems[] = $arItem;
}

//var_dump($debug);
$arResult['ITEMS'] = $arItems;
//echo '<pre>';
//var_dump($arResult);

$this->IncludeComponentTemplate();
