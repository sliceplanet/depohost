<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
@set_time_limit(0);
ini_set('memory_limit', '3048M');
@ignore_user_abort(true);
$moduleId = 'rkn';
$settings['dumpdate'] = Option::get($moduleId, 'dumpdate');
$settings['code'] = Option::get($moduleId, 'code');
$arParams = json_decode(Option::get($moduleId, 'PARAMS'), true);

$bDebug = $arParams['DEBUG'] == 'Y' ? true : false;
$logFilename = strlen($arParams['FILE_LOG']) > 0 ? $arParams['FILE_LOG'] : $_SERVER['DOCUMENT_ROOT'] . '/local/modules/rkn/logs/response.log';
$logTime = FormatDate('FULL', time());


$soap = new SoapClient('http://vigruzki.rkn.gov.ru/services/OperatorRequest/?wsdl');
$data = $soap->getLastDumpDateEx();

$isValid = false;
if (empty($settings['dumpdate']))
{
  $settings['dumpdate'] = intval($data->lastDumpDate);
  $isValid = true;
}
elseif (intval($settings['dumpdate']) < intval($data->lastDumpDate) || intval($settings['dumpdate']) < intval($data->lastDumpDateUrgently))
{
  $settings['dumpdate'] = $data->lastDumpDate;
  $isValid = true;
}
elseif (intval($settings['dumpdate']) < intval($data->lastDumpDateUrgently))
{
  $settings['dumpdate'] = $data->lastDumpDateUrgently;
  $isValid = true;
}

// Если пора делать новый запрос
if ($isValid)
{
  $requestFile = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/modules/rkn/files/Depo_Telecom.xml');
  $signatureFile = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/local/modules/rkn/files/Depo_Telecom.xml.sig');
  $params = new stdClass();
  $params->requestFile = new SoapVar($requestFile, XSD_BASE64BINARY, 'xsd:base64Binary');
  $params->signatureFile = new SoapVar($signatureFile, XSD_BASE64BINARY, 'xsd:base64Binary');
  $params->dumpFormatVersion = "2.0";
  $data = $soap->sendRequest($params);
  if ($data->result)
  {
    $settings['code'] = $data->code;
  }

  $arLog[] = Loc::getMessage('LOG_UPDATE_AVAIBLE');
}
elseif (!$isValid && empty($settings['code']))
{
  $arLog[] = Loc::getMessage('LOG_NO_UPDATE');
}

if (!empty($settings['code']))
{
  $bResponse = false;
  while (!$bResponse)
  {
    sleep(60);
    $params = new stdClass();
    $params->code = new SoapVar($settings['code'], XSD_STRING, 'xsd:string');
    $data = $soap->getResult($params);
    if ($data->result)
    {
      $arLog[] = Loc::getMessage('LOG_SUCCESS_RESPONSE');
      $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/rkn/';
      if (!(\Bitrix\Main\IO\Directory::isDirectoryExists($path)))
      {
        \Bitrix\Main\IO\Directory::createDirectory($path);
      }
      $fileName = $path . 'rkn-'.FormatDate('FULL',time()).'.zip';
      file_put_contents($fileName, $data->registerZipArchive);

      $arFieldsEvent['TABLE_LIST_SITES'] = "";
      $arFieldsEvent['ATTACH'] = $fileName;
      $ob = new \Bitrix\Main\SiteTable;
      $res = $ob->getList();
      $arSites = array();
      while ($ar = $res->fetch())
      {
        $arSites[] = $ar['LID'];
      }

      \CEvent::SendImmediate('RKN_SEND_LIST_SITES', $arSites, $arFieldsEvent);
      $settings['code'] = '';
      $bResponse = true;

      // clear old files
      try {
        $dir = new \Bitrix\Main\IO\Directory($path);
        $files = $dir->getChildren();
        foreach ($files as $file) {
          if($file instanceof Bitrix\Main\IO\File) {
            $time = $file->getCreationTime();
            // last 7 days
            if($time < time() - 7 * 24 * 60 * 60) {
              $file->delete();
            }
          }
        }
      }
      catch (\Exception $e) {
        //
      }
    }
    else
    {
      $arLog[] = FormatDate('FULL', time()) . ': ' . Loc::getMessage('LOG_FAIL_RESPONSE') . (iconv('utf-8', LANG_CHARSET, $data->resultComment));
      $bResponse = false;
    }
  }

}
if ($bDebug && strlen($logFilename) > 0 && !empty($arLog))
{
  $path = new \Bitrix\Main\IO\Path;
  if (strpos($logFilename, $_SERVER['DOCUMENT_ROOT']) !== 0)
  {
    $logFilename = $path->combine($_SERVER['DOCUMENT_ROOT'], $logFilename);

  }
  $messages = $logTime
    . "\r\n"
    . "--------------"
    . "\r\n";
  foreach ($arLog as $message)
  {
    $messages .= $message . "\r\n\r\n";
  }
  file_put_contents($logFilename, $messages, FILE_APPEND);
  echo $messages;
}
foreach ($settings as $k => $v)
{
  Option::set($moduleId, $k, $v);
}