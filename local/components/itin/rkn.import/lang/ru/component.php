<?
$MESS["HEADER_TABLE_DATE"] = "Дата";
$MESS["HEADER_TABLE_NUMBER"] = "Номер";
$MESS["HEADER_TABLE_ORGANIZATION"] = "Организация";
$MESS["HEADER_TABLE_DOMAIN"] = "Домен";
$MESS["HEADER_TABLE_URL"] = "URL";
$MESS["HEADER_TABLE_IP"] = "IP";
$MESS["HEADER_TABLE_URGENCY"] = "Тип срочности";
$MESS["HEADER_TABLE_INCLUDE_TIME"] = "Дата блокировки";
$MESS["HEADER_TABLE_IP_SUBNET"] = "IP-подсеть";
$MESS["HEADER_TABLE_ENTRY_TYPE"] = "Тип реестра";

$MESS["TABLE_COMMENT"] = "Ссылки предоставлены в ознакомительных целях, просьба не злоупотребять ими.";

$MESS["LOG_NO_UPDATE"] = "Обновлений базы еще не было";
$MESS["LOG_SUCCESS_RESPONSE"] = "Обновление получено";
$MESS["LOG_FAIL_RESPONSE"] = "Ответ сервера: ";
$MESS["LOG_UPDATE_AVAIBLE"] = "Обновления есть, отправлен запрос";
$MESS["ENTRY_TYPE_VALUE_1"] = "ЕАИС";
$MESS["ENTRY_TYPE_VALUE_2"] = "НАП";
$MESS["ENTRY_TYPE_VALUE_3"] = "398-ФЗ";
$MESS["ENTRY_TYPE_VALUE_4"] = "97-ФЗ(организаторы распространения информации)";
$MESS["ENTRY_TYPE_VALUE_DEFAULT"] = "Неопределен";

?>