<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . "/template.php");
// удаляем лишние значения свойств
//$arResult['ITEMS'] = array_slice($arResult['ITEMS'], 0, 5, true);
foreach ($arResult['ITEMS'] as $id => &$arItem)
{
    foreach ($arItem['PROPERTIES'] as $code => $arValues)
    {
        if ($code == 'CPU' || $code == 'RAID')
        {
            // Оставляем три значения
            $arItem['PROPERTIES'][$code] = $arValues;
        }

        if ($code == 'RAID')
        {
            foreach ($arValues as $key => $arValue)
            {
                $arItem['PROPERTIES'][$code][$key]['NAME'] = $arValue['BREND'];
                $arItem['PROPERTIES'][$code][$key]['DESCRIPTION'] = $arValue['NAME'];
            }
        }

    }

}
foreach ($arResult['PROPERTIES']['RAID'] as $generation_id => $arValues)
{
    foreach ($arValues as $key => $arValue)
    {
        $arResult['PROPERTIES']['RAID'][$generation_id][$key]['NAME'] = $arValue['BREND'];
        $arResult['PROPERTIES']['RAID'][$generation_id][$key]['DESCRIPTION'] = $arValue['NAME'];
    }
}

$arResult['DEFAULT'] = array(
    'COUNT_HDD' => 1,
);
$arFirst = array_shift($arResult['ITEMS']);

$arResult['FIRST_ITEM'] =&$arFirst;  // first element

?>