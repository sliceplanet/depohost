<?

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


Loader::includeModule('dedicated');
Loader::includeModule('iblock');

$IBLOCK_ID = $arParams['IBLOCK_ID'];
$arOrder = array('SORT' => 'ASC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);
$db = CIBlockElement::GetList($arOrder, $arFilter, false, false);

while ($ob = $db->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProperties = $ob->GetProperties(array('SORT' => 'ASC'));

    $form_factor = $arProperties['FORM_FACTOR'];
    $arIds = array();
    foreach ($arProperties as $property_code => $arProperty)
    {
        if ($arProperty['PROPERTY_TYPE'] == 'E')
        {
            // Собираем ID всех значений свойств, чтобы разом достать всю инфу о них
            if (is_array($arProperty['VALUE']))
            {
                $arIds = array_merge($arProperty['VALUE'], $arIds);
                $relationCodeProperty[$arProperty['LINK_IBLOCK_ID']] = $property_code;
            }
        }
    }

    $arFilter = array(
        'ID' => array_values($arIds),
        'ACTIVE' => 'Y',
    );
    $res_el = CIBlockElement::GetList($arOrder, $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_ID', 'SORT', 'PROPERTY_PRICE', 'PROPERTY_ALT', 'PROPERTY_BREND', 'PROPERTY_DESCRIPTION', 'PROPERTY_SIZE'));

    $arProperties = array();

    while ($ar_el = $res_el->Fetch())
    {
        if (isset($relationCodeProperty[$ar_el['IBLOCK_ID']]))
        {
            $arPropValues = array();
            $code = $relationCodeProperty[$ar_el['IBLOCK_ID']];
            foreach ($ar_el as $key => $value)
            {
                if (strpos($key, 'PROPERTY_') === 0 && strpos($key, '_VALUE') == strlen($key) - 6 && !empty($value))
                {
                    $key = str_replace('PROPERTY_', '', $key);
                    $key = str_replace('_VALUE', '', $key);

                    $arPropValues[$key] = $value;
                }
            }

            $arPropValues['NAME'] = $ar_el['NAME'];
            $arPropValues['SORT'] = $ar_el['SORT'];
            $arPropValues['ID'] = $ar_el['ID'];
            $arProperties[$code][] = $arPropValues;
            $arResult['PROPERTIES'][$code][$arFields['ID']][] = $arPropValues; //data in slice properties
        }
    }
    $arResult['PROPERTIES']['COUNT_HDD'][$arFields['ID']] = (int) $form_factor['VALUE_SORT'];

    $arItems[$arFields['ID']] = array(
        'ID' => $arFields['ID'],
        'NAME' => $arFields['NAME'],
        'PROPERTIES' => $arProperties,
        'FORM_FACTOR' => $form_factor['VALUE'],
        'COUNT_HDD' => (int) $form_factor['VALUE_SORT'],
    );
}
$arResult['ITEMS'] = $arItems;  //data in slice generations processors
//$arResult['RELATION_CODE'] = $arRelationCode;   // Соотношение символьного кода свойства с его ID

$default_values = array(
    'HINT_CALC_TYPE_SERVER' => "",
    'HINT_CALC_GENERATION' => "",
    'HINT_CALC_CPU' => "",
    'HINT_CALC_RAM' => "",
    'HINT_CALC_RAID' => "",
    'HINT_CALC_HDD' => "",
);
foreach ($default_values as $key => $value)
{
    $arHints[$key] = Option::get(ADMIN_MODULE_NAME, strtolower($key), $value);
}
$arResult['HINTS'] = $arHints;
$arDiscount = unserialize(Option::get('dedicated', 'discounts_values'));
$arResult['DISCOUNTS'] = $arDiscount;
$this->IncludeComponentTemplate();
