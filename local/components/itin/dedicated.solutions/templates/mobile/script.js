/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 03.05.15.
 */
function checkValueMobile(obj) {
  obj = $(obj);
  var tr = obj.parents(".item_list__info");
  var id = obj.data("id");
  var months = obj.val();
  tr.find(".order-btn").data("months", months);

  if (!!itemsSolutions && !!itemsSolutions[id]) {
    tr.find(".price-js .value").html(
      number_format(
        itemsSolutions[id]["PRICES"][months]["SUM"],
        0,
        ".",
        "&nbsp;"
      ) + "&nbsp;P"
    );
    tr.find(".price-month").html(
      number_format(
        itemsSolutions[id]["PRICES"][months]["PRICE"],
        0,
        ".",
        "&nbsp;"
      ) + "&nbsp;p"
    );
  }
}
jQuery(document).ready(function($) {
  $(".period").selectBox({ mobile: true });
});
