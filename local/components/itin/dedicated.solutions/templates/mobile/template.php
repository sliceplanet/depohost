<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']))
{
  return; // no items, good bye
}
require_once 'functions.php';

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="hidden-sm hidden-md hidden-lg item_list">

  <? foreach ($arResult['SECTIONS'] as $arSection):
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
      $arSectionDeleteParams);
    ?>
    <div class="item_list__section"><?= $arSection['NAME'] ?></div>
    <?
    $i = 0;
    foreach ($arResult['ITEMS'] as $arItem):
      if ($arSection['ID'] != $arItem['SECTION_ID'])
      {
        continue;
      }
      else
      {
        $i++;
      }
      if ($i > $arParams['PAGE_COUNT'])
      {
        break;
      }
      ?>
      <div class="item_list__info">
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"><?= GetMessage('HEADER_CPU') ?></div>
          <div class="item_list__info__row_value"><?= $arItem['PROPERTIES']['CPU']['DISPLAY_VALUE'] ?></div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"><?= GetMessage('HEADER_RAM') ?></div>
          <div class="item_list__info__row_value"><?= $arItem['PROPERTIES']['RAM']['DISPLAY_VALUE'] ?></div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"><?= GetMessage('HEADER_HDD') ?></div>
          <div class="item_list__info__row_value"><?= $arItem['PROPERTIES']['HDD']['DISPLAY_VALUE'] ?></div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"><?= GetMessage('HEADER_RAID') ?></div>
          <div class="item_list__info__row_value"><?= $arItem['PROPERTIES']['RAID']['DISPLAY_VALUE'] ?></div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"><?= GetMessage('HEADER_PERIOD') ?></div>
          <div class="item_list__info__row_value">
            <select class="select_kam2 period" data-id="<?= $arItem['ID'] ?>" onchange="checkValueMobile(this);">
              <option value="1"><?= GetMessage('PERIOD_VALUE_NAME_1') ?></option>
              <option value="3"><?= GetMessage('PERIOD_VALUE_NAME_3') ?></option>
              <option value="6"><?= GetMessage('PERIOD_VALUE_NAME_6') ?></option>
              <option value="12"><?= GetMessage('PERIOD_VALUE_NAME_12') ?></option>
            </select>
          </div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label">Цена</div>
          <div class="item_list__info__row_value item_list__info__row_value_price price-js">
            <div class="value">
              <?= str_replace(' ', '&nbsp;', number_format($arItem['PRICES'][1]['SUM'], 0, '.', ' ')) ?>&nbsp;P
            </div>
            1&nbsp;мес&nbsp;=&nbsp;<span
                class="price-month"><?= str_replace(' ', '&nbsp;', number_format($arItem['PRICES'][1]['PRICE'], 0, '.', ' ')) ?>
              &nbsp;p</span>
          </div>
        </div>
        <div class="item_list__info__row">
          <div class="item_list__info__row_label"></div>
          <div class="item_list__info__row_value">
            <a id="btn-m<?= $arElement['ID'] ?>"
               data-type="solution"
               data-solution-id="<?= $arItem['ID'] ?>"
               data-months="1"
               rel="nofollow"
               class="btn-green sm order-btn btn-solutions-order"
            >Заказать</a>
          </div>
        </div>
      </div>
    <? endforeach ?>
  <? endforeach ?>
</div>
