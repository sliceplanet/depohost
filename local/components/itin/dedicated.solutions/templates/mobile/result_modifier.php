<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . "/template.php");
$arItems = array();
foreach ($arResult['ITEMS'] as $arItem)
{
    $arItems[$arItem['ID']] = $arItem;
}
$arResult['ITEMS'] = $arItems;