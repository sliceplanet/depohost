<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']))
{
    return; // no items, good bye
}
require_once 'functions.php';

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<section id="ready-made" class="tab-pane clearfix">
    <div class="row">
        <div class="col-sm-12">
         <h2 style="font-size:24px;font-family:'MyriadProRegular';margin-bottom:30px;"><?= Loc::getMessage('TITLE_SOLUTIONS') ?></h2>

            <!--.head-->
            <table class="hidden-xs blue-v2">
                <thead>
                <tr class="head bg-light-blue">
                    <td class="cpu"><div class="with-icon"><?=GetMessage('HEADER_CPU')?></div></td>
                    <td class="ram"><div class="with-icon"><?=GetMessage('HEADER_RAM')?></div></td>
                    <td class="hdd"><div class="with-icon"><?=GetMessage('HEADER_HDD')?></div></td>
                    <td class="raid"><div class="with-icon"><?=GetMessage('HEADER_RAID')?></div></td>
                    <td class="period"><div class="with-icon"><?=GetMessage('HEADER_PERIOD')?></div></td>
                    <td class="price"><div class="with-icon"><?=GetMessage('HEADER_PRICE')?></div></td>
                    <td class="order"><div class="with-icon"><?=GetMessage('HEADER_ORDER')?></div></td>
                </tr>
                </thead>
                <tbody>
                    <? foreach ($arResult['SECTIONS'] as $arSection):
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
                            $arSectionDeleteParams);
                    ?>
                    <tr>
                        <td class="section-name bg-dark-blue" colspan="7">
                            <?= $arSection['NAME'] ?>
                        </td>
                    </tr>
                        <?
                        $i = 0;
                        foreach ($arResult['ITEMS'] as $arItem):
                            if ($arSection['ID'] != $arItem['SECTION_ID'])
                            {
                                continue;
                            }
                            else
                            {
                                $i++;
                            }
                            if ($i > $arParams['PAGE_COUNT'])
                            {
                                break;
                            }
                            ?>
                    <tr class="item" data-detail="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <td class="cpu">
                            <?=$arItem['PROPERTIES']['CPU']['DISPLAY_VALUE']?>
                        </td>
                        <td class="ram">
                            <?=$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE']?>
                        </td>
                        <td class="hdd">
                            <?=$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE']?>
                        </td>
                        <td class="raid">
                            <?=$arItem['PROPERTIES']['RAID']['DISPLAY_VALUE']?>
                        </td>
                        <td class="period">
                            <select class="select_kam2 period" data-id="<?=$arItem['ID']?>" onchange="checkValue(this);">
                                <option value="1"><?=GetMessage('PERIOD_VALUE_NAME_1')?></option>
                                <option value="3"><?=GetMessage('PERIOD_VALUE_NAME_3')?></option>
                                <option value="6"><?=GetMessage('PERIOD_VALUE_NAME_6')?></option>
                                <option value="12"><?=GetMessage('PERIOD_VALUE_NAME_12')?></option>
                            </select>
                        </td>
                        <td class="price">
                            <div class="value">
                                <?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][1]['SUM'],0,'.',' '))?>&nbsp;P
                            </div>
                            1&nbsp;мес&nbsp;=&nbsp;<span class="price-month"><?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][1]['PRICE'],0,'.',' '))?>&nbsp;p</span>
                        </td>
                        <td class="order buy-block">
                            <a class="btn-green sm btn-solutions-order" href="#"
                               data-type="solution"
                               data-solution-id="<?= $arItem['ID'] ?>"
                               data-months="1"
                                >
                              <span class="visible-lg"><?= Loc::getMessage('ORDER_ADD') ?></span>
                              <span class="visible-sm visible-md"><span class="icon-cart"></span></span>
                            </a>
                        </td>
                    </tr>
                        <? endforeach?>
                    <? endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    var itemsSolutions = <?=CUtil::PhpToJSObject($arResult['ITEMS']);?>;
</script>