/**
 * Created by Aleksandr Terentev <alvteren@gmail.com> on 03.05.15.
 */
function checkValue(obj) {
  obj = $(obj);
  var tr = obj.parents("tr");
  var id = obj.data("id");
  var months = obj.val();
  tr.find(".btn-green").data("months", months);
  if (!!itemsSolutions && !!itemsSolutions[id]) {
    tr.find(".price .value").html(
      number_format(
        itemsSolutions[id]["PRICES"][months]["SUM"],
        0,
        ".",
        "&nbsp;"
      ) + "&nbsp;P"
    );
    tr.find(".price-month").html(
      number_format(
        itemsSolutions[id]["PRICES"][months]["PRICE"],
        0,
        ".",
        "&nbsp;"
      ) + "&nbsp;p"
    );
  }
}
jQuery(document).ready(function($) {
  $(".period").selectBox({ mobile: true });
  
  $(".btn-solutions-order").on("click", "", function(event) {
    event.preventDefault();
    var data = $(this).data();
    data["step"] = 2;
    data["isAjax"] = "Y";

    $.ajax({
      url: "",
      type: "get",
      data: data,
      success: function(res) {
        if (res.length > 0) {
          $(".selectBox-dropdown-menu").remove();
           $("#page-dedic").html(res);
          var body = $("html, body");
          //var top = $("#page-dedic").offset().top;
          //body.animate({ scrollTop: top }, "500");
        } else {
          alert("ошибка ответа");
        }
      }
    });
  });
  $(".solution-order-button").on("click", "", function(event) {
    event.preventDefault();
    var data = $(this).data();
    data["step"] = 2;
    data["isAjax"] = "Y";

    $.ajax({
      url: "",
      type: "get",
      data: data,
      success: function(res) {
        if (res.length > 0) {
          $(".selectBox-dropdown-menu").remove();
		  
		  $('.bx_breadcrumbs').eq(0).remove();
		  
           $(".main-page").html(res);
          var body = $("html, body");
          var top = $(".main-page").offset().top;
          body.animate({ scrollTop: top }, "500");
        } else {
          alert("ошибка ответа");
        }
      }
    });
  });
  
  
  
  
  
  
});
