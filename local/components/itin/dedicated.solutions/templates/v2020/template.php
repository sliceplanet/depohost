<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if (empty($arResult['ITEMS']))
{
    return; // no items, good bye
}
require_once 'functions.php';

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<a name="ready-made"></a>
<section id="ready-made"<?if(!$arParams['visible']):?> class="tab-pane clearfix"<?endif;?>>
    <div class="row">
        <div class="col-sm-12">

<div class="table-block table-block-gray"> 
         		<div class="table-div table-div1 table-div-6col">
         			<div class="table-div__head">
	         			<div class="table-block_head_h"><?= Loc::getMessage('TITLE_SOLUTIONS') ?></div>
	         			<div class="table-div__header">
		                    <div class="table-div__hcell cpu"><?=GetMessage('HEADER_CPU')?></div>
		                    <div class="table-div__hcell ram"><?=GetMessage('HEADER_RAM')?></div>
		                    <div class="table-div__hcell hdd"><?=GetMessage('HEADER_HDD')?></div>
		                    <div class="table-div__hcell ssd"><?=GetMessage('HEADER_SSD')?></div>
		                    <div class="table-div__hcell raid"><?=GetMessage('HEADER_RAID')?></div>
		                    <div class="table-div__hcell price"><?=GetMessage('HEADER_PRICE')?></div>
	         			</div>
         			</div>
         			<div class="table-div__body">
						<?/*
         				<div class="table-div__cell">
         					<div class="row">
	         					<div class="col-md-10">
	         						<div class="flex-row">
	         							<div class="server-code">D2-56</div>
	         							<div class="server-name">Выделенный сервер</div>
	         							<div class="label-discount">Скидка</div>
	         							<div class="server-desc">Поддержка до 32Gb Ram DDR3</div>
	         						</div>
	         						<div class="flex-row">
	         							<div class="server-prop-text server-cpu">
	         								<div class="spt-h"><?=GetMessage('HEADER_CPU')?></div>
	         								2XE5620 2.40 ГГЦ
	         							</div>
	         							<div class="server-prop-text server-ram">
	         								<div class="spt-h"><?=GetMessage('HEADER_RAM')?></div>
	         								32GB PC12800 DDR3
	         							</div>
	         							<div class="server-prop-text server-hdd">
	         								<div class="spt-h"><?=GetMessage('HEADER_HDD')?></div>
	         								2 X SAS2.0 600GB,15K 
	         								<br>
	         								2 X SATA3: 2 TB
	         							</div>
	         							<div class="server-prop-text server-raid">
	         								<div class="spt-h"><?=GetMessage('HEADER_RAID')?></div>
	         								2405/6405/6GB.S/1<br>28MB RAID 1, 10
	         							</div>
	         							<div class="server-prop-text server-ssd">
	         								<div class="spt-h"><?=GetMessage('HEADER_SSD')?></div>
	         								2405/6405/6GB.S/1<br>28MB RAID 1, 10
	         							</div>
	         						</div>
	         					</div>
	         					<div class="col-md-2">
	         						<div class="price">
	         							<span>900</span>
	         							Р/мес
	         						</div> 
	         						<div class="button-blue">Выбрать</div>
	         					</div>
         					</div>
         				</div>
						*/?>
		                    <? foreach ($arResult['SECTIONS'] as $arSection):
		                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
		                            $arSectionDeleteParams);
		                    ?> 
			                    <?
		                        $i = 0;
		                        foreach ($arResult['ITEMS'] as $arItem):
		                            if ($arSection['ID'] != $arItem['SECTION_ID'])
		                            {
		                                continue;
		                            }
		                            else
		                            {
		                                $i++;
		                            }
		                            if ($i > $arParams['PAGE_COUNT'])
		                            {
		                                break;
		                            }
									
									$hdd = explode('<br>',$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE']);
									$ssd = [];
									foreach($hdd as $k => $v):
										if(strstr($v,'SSD')):
											unset($hdd[$k]);
											$ssd[] = $v;
										endif;	
									endforeach;
									$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE'] = implode('<br>',$hdd);
									$arItem['PROPERTIES']['SSD']['DISPLAY_VALUE'] = implode('<br>',$ssd);
									
		                        ?>
		         				<div class="table-div__cell">
		         					<div class="row">
			         					<div class="col-md-10">
			         						<div class="flex-row">
			         							<div class="server-code"><?=$arItem['NAME']?></div>
			         							<div class="server-name"><?= $arSection['NAME'] ?></div>
			         							<!--<div class="label-discount">Скидка</div>-->
			         							<div class="server-desc"></div>
			         						</div>
			         						<div class="flex-row">
			         							<div class="server-prop-text server-cpu">
			         								<div class="spt-h"><?=GetMessage('HEADER_CPU')?></div>
			         								<?=$arItem['PROPERTIES']['CPU']['DISPLAY_VALUE']?>
			         							</div>
			         							<div class="server-prop-text server-ram">
			         								<div class="spt-h"><?=GetMessage('HEADER_RAM')?></div>
			         								<?=$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE']?>
			         							</div>
			         							<div class="server-prop-text server-hdd">
			         								<div class="spt-h"><?=GetMessage('HEADER_HDD')?></div>
			         								<?=$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE']?>
			         							</div>
			         							<div class="server-prop-text server-ssd">
			         								<div class="spt-h"><?=GetMessage('HEADER_SSD')?></div>
			         								<?=$arItem['PROPERTIES']['SSD']['DISPLAY_VALUE']?>
			         							</div>
			         							<div class="server-prop-text server-raid">
			         								<div class="spt-h"><?=GetMessage('HEADER_RAID')?></div>
			         								<?=$arItem['PROPERTIES']['RAID']['DISPLAY_VALUE']?>
			         							</div>
			         						</div>
			         					</div>
			         					<div class="col-md-2">
			         						<div class="price">
			         							<span><?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][1]['SUM'],0,'.',' '))?></span>
			         							Р/мес
			         						</div> 
			         						<a class="button-blue solution-order-button"
												data-type="solution"
					                            data-solution-id="<?= $arItem['ID'] ?>"
					                            data-months="1"
			         						>
			         							Выбрать
			         						</a>
			         					</div>
			         				</div>
			         			</div>


		               			<? endforeach;?>
		              		<? endforeach;?>
         			</div>
         		</div>	


         	</div>


<?/*
     		<h2><?= Loc::getMessage('TITLE_SOLUTIONS') ?></h2>

            <!--.head-->
            <table class="hidden-xs blue-v2">
                <thead>
                <tr class="head bg-light-blue">
                    <td class="cpu"><div class="with-icon"><?=GetMessage('HEADER_CPU')?></div></td>
                    <td class="ram"><div class="with-icon"><?=GetMessage('HEADER_RAM')?></div></td>
                    <td class="hdd"><div class="with-icon"><?=GetMessage('HEADER_HDD')?></div></td>
                    <td class="raid"><div class="with-icon"><?=GetMessage('HEADER_RAID')?></div></td>
                    <td class="period"><div class="with-icon"><?=GetMessage('HEADER_PERIOD')?></div></td>
                    <td class="price"><div class="with-icon"><?=GetMessage('HEADER_PRICE')?></div></td>
                    <td class="order"><div class="with-icon"><?=GetMessage('HEADER_ORDER')?></div></td>
                </tr>
                </thead>
                <tbody>
                    <? foreach ($arResult['SECTIONS'] as $arSection):
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete,
                            $arSectionDeleteParams);
                    ?>
                    <tr>
                        <td class="section-name bg-dark-blue" colspan="7">
                            <?= $arSection['NAME'] ?>
                        </td>
                    </tr>
                        <?
                        $i = 0;
                        foreach ($arResult['ITEMS'] as $arItem):
                            if ($arSection['ID'] != $arItem['SECTION_ID'])
                            {
                                continue;
                            }
                            else
                            {
                                $i++;
                            }
                            if ($i > $arParams['PAGE_COUNT'])
                            {
                                break;
                            }
                            ?>
                    <tr class="item" data-detail="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <td class="cpu">
                            <?=$arItem['PROPERTIES']['CPU']['DISPLAY_VALUE']?>
                        </td>
                        <td class="ram">
                            <?=$arItem['PROPERTIES']['RAM']['DISPLAY_VALUE']?>
                        </td>
                        <td class="hdd">
                            <?=$arItem['PROPERTIES']['HDD']['DISPLAY_VALUE']?>
                        </td>
                        <td class="raid">
                            <?=$arItem['PROPERTIES']['RAID']['DISPLAY_VALUE']?>
                        </td>
                        <td class="period">
                            <select class="select_kam2 period" data-id="<?=$arItem['ID']?>" onchange="checkValue(this);">
                                <option value="1"><?=GetMessage('PERIOD_VALUE_NAME_1')?></option>
                                <option value="3"><?=GetMessage('PERIOD_VALUE_NAME_3')?></option>
                                <option value="6"><?=GetMessage('PERIOD_VALUE_NAME_6')?></option>
                                <option value="12"><?=GetMessage('PERIOD_VALUE_NAME_12')?></option>
                            </select>
                        </td>
                        <td class="price">
                            <div class="value">
                                <?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][1]['SUM'],0,'.',' '))?>&nbsp;P
                            </div>
                            1&nbsp;мес&nbsp;=&nbsp;<span class="price-month"><?=str_replace(' ','&nbsp;',number_format($arItem['PRICES'][1]['PRICE'],0,'.',' '))?>&nbsp;p</span>
                        </td>
                        <td class="order buy-block">
                            <a class="btn-green sm btn-solutions-order" href="#"
                               data-type="solution"
                               data-solution-id="<?= $arItem['ID'] ?>"
                               data-months="1"
                                >
                              <span class="visible-lg"><?= Loc::getMessage('ORDER_ADD') ?></span>
                              <span class="visible-sm visible-md"><span class="icon-cart"></span></span>
                            </a>
                        </td>
                    </tr>
                        <? endforeach?>
                    <? endforeach?>
                </tbody>
            </table>
*/?>

        </div>
    </div>
</section>
<script>
    var itemsSolutions = <?=CUtil::PhpToJSObject($arResult['ITEMS']);?>;
</script>