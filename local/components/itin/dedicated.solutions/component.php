<?

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


Loader::includeModule('dedicated');
Loader::includeModule('iblock');
$base_price_id = 5; // ID base type price

$arDiscount = unserialize(Option::get('dedicated', 'discounts_values'));
$arResult['DISCOUNTS'] = array(
    1 => 0,
    3 => $arDiscount['SOLUTIONS_3M'],
    6 => $arDiscount['SOLUTIONS_6M'],
    12 => $arDiscount['SOLUTIONS_12M'],
);

$IBLOCK_ID = $arParams['IBLOCK_ID'];
$arOrder = array('SORT' => 'ASC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);
$db = CIBlockSection::GetList($arOrder, $arFilter, false, array('ID', 'NAME','CODE'));
while ($ar = $db->Fetch())
{
    $arSections[] = $ar;
}
$arOrder = array('SORT' => 'ASC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);
$arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'CATALOG_GROUP_'.$base_price_id, 'PROPERTY_*','DETAIL_PAGE_URL');

if($arParams['SECTION_ID']):
	$arFilter['SECTION_ID'] = (int)$arParams['SECTION_ID'];
endif;
if($arParams['!SECTION_ID']):
	$arFilter['!SECTION_ID'] = $arParams['!SECTION_ID'];
endif;


$db = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

$new = array();

while ($ob = $db->GetNextElement())
{
    $arFields = $ob->GetFields();

    $arProperties = $ob->GetProperties(array('SORT' => 'ASC'));
    $arPropValues = array();
    $arIds = array();
    $arFields['PROPERTIES'] = $arProperties;
    $new[] = $arFields;

    foreach ($arProperties as $property_code => $arProperty)
    {
        if ($arProperty['PROPERTY_TYPE'] == 'E' || $property_code == 'HDD')
        {
            // Собираем ID всех значений свойств типа привязка к элементу, чтобы разом достать всю инфу о них
            if (!is_array($arProperty['VALUE']))
            {
                $arProperty['VALUE'] = array($arProperty['VALUE']);
            }
            $arIds = array_merge($arProperty['VALUE'], $arIds);
            if($property_code == 'HDD'){
                $arProperty['LINK_IBLOCK_ID'] = 43;
            }
            $relationCodeProperty[$arProperty['LINK_IBLOCK_ID']] = $property_code;
        }
        else
        {
            $arPropValues[$property_code]['DISPLAY_VALUE'] = $arProperty['VALUE'];
        }
        $arPropValues[$property_code]['NAME'] = $arProperty['NAME'];
        $arPropValues[$property_code]['VALUE'] = $arProperty['VALUE'];

    }

    $arFilter = array(
        'ID' => array_values($arIds),
        'ACTIVE' => 'Y',
    );
    $res_el = CIBlockElement::GetList($arOrder, $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_ID', 'SORT', 'PROPERTY_PRICE'));

    $arProps = array();

    while ($ar_el = $res_el->Fetch())
    {
//
        if (isset($relationCodeProperty[$ar_el['IBLOCK_ID']]))
        {

            $code = $relationCodeProperty[$ar_el['IBLOCK_ID']];

            $arElements[$ar_el['ID']] = $ar_el;
            $arPropValues[$code]['DISPLAY_VALUE'] = $ar_el['NAME'];
        }
    }
//    var_dump($arPropValues['HDD']['VALUE']);
    foreach ($arPropValues as $code => $arProp)
    {

        if (count($arProp['VALUE']) > 1 || $code == 'HDD')
        {
            $ar_counts = array_count_values($arProp['VALUE']);

            $arNames = array();
            foreach ($ar_counts as $element_id => $count)
            {
                $arNames[] = $count . " x " . $arElements[$element_id]['NAME'];
            }
            $arPropValues[$code]['DISPLAY_VALUE'] = implode('<br>', $arNames);
        }
    }
    // prices
    $arPrices = array();
    $price = (int) $arFields['CATALOG_PRICE_'.$base_price_id];
    foreach ($arResult['DISCOUNTS'] as $count_month => $discount_value)
    {
        $discount= intval($price*$discount_value/100);
        $arPrices[$count_month] = array(
            'DISCOUNT' => intval($discount*$count_month),
            'PRICE' => intval($price-$discount),
            'SUM' => intval(($price-$discount)*$count_month)
        );

        if ($count_month==1)
        {
            $arPrices[$count_month]['SELECTED'] = true;
        }
    }


    $arItems[] = array(
        'ID' => $arFields['ID'],
        'NAME' => $arFields['NAME'],
        'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
        'PROPERTIES' => $arPropValues,
        'PRICES' => $arPrices,
        'SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
    );
}
$arResult['ITEMS'] = $arItems;
$arResult['SECTIONS'] = $arSections;


//echo '<pre>';print_r($new);echo "</pre>";


$this->IncludeComponentTemplate();
