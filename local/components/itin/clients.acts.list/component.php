<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Acts;
use Itin\Depohost\Info;

Loc::loadMessages(__FILE__);

global $USER;

Loader::includeModule('clients');
$IBLOCK_ID = Info::getIblockId('acts');

$user_id = intval($USER->GetID());
$client_ob = new \Itin\Depohost\Clients($user_id);

$client_id = $client_ob->getElementId();
$ar_client = $client_ob->getById($client_id);
$arElements = $ar_client['CC_ACTS']['VALUE'];

foreach ($arElements as $element_id)
{
    $acts = new Acts(intval($element_id));
    $arTemp = $acts->getById($element_id);
    if (!empty($arTemp))
    {
        $arData = $acts->getFormat($arTemp);
        $contract_date = $arData['CLIENT']['DATE_CREATE'];
        $arResult['ITEMS'][$element_id] = $arData;
        $arResult['ITEMS'][$element_id]['DESCRIPTION'] = Loc::getMessage('TEMPLATE_DESCRIPTION',
            array(
                '#CONTRACT_ID#'=>$arData['CLIENT']['NAME'],
                '#CONTRACT_DATE#'=>FormatDateEx($contract_date,false,'dd.mm.YYYY')
            )
        )." ".FormatDateEx($arData['DATE'],false,'f YYYY');
    }
}

$this->IncludeComponentTemplate();
