(function (window)
{
	if (!!window.JCFlatVote)
	{
		return;
	}

	window.JCFlatVote = {
		trace_vote: function (div, flag)
		{
			var my_div;
			//Left from current
			my_div = div;
			while (my_div = my_div.previousSibling)
			{
				if (flag)
					BX.addClass(my_div, 'bx-star-active');
				else
					BX.removeClass(my_div, 'bx-star-active');
			}
			//current
			if (flag)
				BX.addClass(div, 'bx-star-active');
			else
				BX.removeClass(div, 'bx-star-active');
			//Right from the current
			my_div = div;
			while (my_div = my_div.nextSibling)
			{
				BX.removeClass(my_div, 'bx-star-active');
			}
		},

		do_vote: function (div, vote)
		{
			var url = location.pathname;
			$.ajax({
				type: "POST",
				url: "/local/components/skobeeff/page.vote/ajax.php",
				data: {
					VOTE: vote,
					URL: url
				},
				success: function (responce) {
					if (responce.length) {
						$('.page-rating').replaceWith(responce);
					}
				}
			});
		}
	}
}
)(window);
