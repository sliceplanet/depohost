<?php

define("NO_KEEP_STATISTIC", true);
define('PUBLIC_AJAX_MODE', true);
define("NOT_CHECK_PERMISSIONS", true);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$template = (!empty($_REQUEST["TEMPLATE"])) ? $_REQUEST["TEMPLATE"] : "";

$APPLICATION->IncludeComponent("skobeeff:page.vote", $template,[
	"HL_BLOCK_ID" => 3,
	"MAX_VOTE" => 5
]);

CMain::FinalActions();