<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock"); 

use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

class PageVote extends CBitrixComponent
{

	public function onPrepareComponentParams($params)
	{
		return $params;
	}

	public function executeComponent()
	{

		if (!empty($_REQUEST["VOTE"])) {
			$this->addVote();
			return;
		}

		$url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);


		$hlblock = HL\HighloadBlockTable::getById($this->arParams["HL_BLOCK_ID"])->fetch(); 

		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entityClass = $entity->getDataClass(); 


		$voteRowIterator = $entityClass::getList(array(
		   "select" => array("*"),
		   "filter" => array("UF_URL" => $url)  
		));


		if ($voteRow = $voteRowIterator->Fetch()) {
			$id = $voteRow["ID"];
			$count = $voteRow["UF_VOTE_COUNT"];
			$sum = $voteRow["UF_VOTE_SUM"];
			$rating = $voteRow["UF_VOTE_RATING"];
		} else {
			$count = 0;
			$sum = 0;
			$rating = 0;

			$voteInsert = $entityClass::add([
				"UF_URL" => $url,
				"UF_VOTE_COUNT" => $count,
				"UF_VOTE_SUM" => $sum,
				"UF_VOTE_RATING" => $rating
			]);

			$id = $voteInsert->getId();
		}

		$this->arResult["ID"] = $id;
		$this->arResult["COUNT"] = $count;
		$this->arResult["SUM"] = $sum;
		$this->arResult["RATING"] = $rating;

		$this->includeComponentTemplate();
	}

	public function addVote() 
	{
		$url = (!empty($_REQUEST["URL"])) ? $_REQUEST["URL"] : '';
		$vote = (!empty($_REQUEST["VOTE"])) ? $_REQUEST["VOTE"] : '';

		if (!empty($_SESSION["PAGE_VOTE"]) && in_array($url, $_SESSION["PAGE_VOTE"]))
			return;

		if ($vote <=0 && $vote > $this->arParams["MAX_VOTE"]) 
			return;

		$hlblock = HL\HighloadBlockTable::getById($this->arParams["HL_BLOCK_ID"])->fetch(); 

		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entityClass = $entity->getDataClass(); 


		$voteRowIterator = $entityClass::getList(array(
		   "select" => array("*"),
		   "filter" => array("UF_URL" => $url)  
		));


		if ($voteRow = $voteRowIterator->Fetch()) {
			$id = $voteRow["ID"];
			$count = $voteRow["UF_VOTE_COUNT"];
			$sum = $voteRow["UF_VOTE_SUM"];
			$rating = $voteRow["UF_VOTE_RATING"];

			$count++;
			$sum += $vote;
			$rating = round($sum / $count);

			$entityClass::update($id,[
				"UF_URL" => $url,
				"UF_VOTE_COUNT" => $count,
				"UF_VOTE_SUM" => $sum,
				"UF_VOTE_RATING" => $rating
			]);

			$_SESSION["PAGE_VOTE"][] = $url;

			$this->arResult["URL"] = $url;
			$this->arResult["COUNT"] = $count;
			$this->arResult["SUM"] = $sum;
			$this->arResult["RATING"] = $rating;

			$this->includeComponentTemplate();
		} 

	}
}