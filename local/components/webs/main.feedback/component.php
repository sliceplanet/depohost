<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
// $arParams["USE_CAPTCHA"] = true;
$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '')
	$arParams["EVENT_NAME"] = "FEEDBACK_FORM";
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '')
	$arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]) && empty($_POST['check']))
{
	// echo '<pre>';print_r($arParams);echo "</pre>";
	// echo '<pre>';print_r($_POST);echo "</pre>";
	$arResult["ERROR_MESSAGE"] = array();
	if(check_bitrix_sessid())
	{
		
		//�������� �����
		if(in_array("NAME",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_name"]) <= 1){
			
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_NAME");
			
		}else{
			
			if(!preg_match("/^(?:[^\d_\s\(\)><\*]{2,15}\s?){1,3}$/is",$_POST["user_name"])){
				
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_NAME");
				
			}else{
				
				$user_name = strip_tags($_POST['user_name']);
				$user_name = mysql_escape_string($user_name);
				
			}
			
			
		}
		
		//�������� ������
		if(in_array("EMAIL", $arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_email"]) <= 1){
			
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_EMAIL");
			
		}else{
			
			if(!check_email($_POST["user_email"])){
				
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");
				
			}else{
				
				$user_email = strip_tags($_POST['user_email']);
				$user_email = mysql_escape_string($user_email);
				
				
			}
		
		}
		
		//�������� ����
		if(in_array("SUBJECT", $arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_subject"]) <= 1){
			
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_SUBJECT");
			
		}else{
			
			if(preg_match("/(\<a[^>]+href\=)|(icq)|(skype)|((?:https?:\/\/)?(?:www\.)?(?:[�-�a-z�����������������0-9_\.\/]{1,}\.)(?:��|[a-z]{2,6}))|[a-zA-Z]{4,}/is",$_POST["user_subject"])){
				
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_SUBJECT_WRONG");
				
			}else{
				
				$user_subject = strip_tags($_POST['user_subject']);
				$user_subject = mysql_escape_string($user_subject);
				
			}
		
		}
		
		
		//�������� ���������
		if(in_array("MESSAGE", $arParams["REQUIRED_FIELDS"]) && strlen($_POST["MESSAGE"]) <= 1){
			
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_MESSAGE");
			
		}else{
			
			if(preg_match("/(\<a[^>]+href\=)|(icq)|(skype)|((?:https?:\/\/)?(?:www\.)?(?:[�-�a-z�����������������0-9_\.\/]{1,}\.)(?:��|[a-z]{2,6}))|[a-zA-Z]{4,}/is",$_POST["MESSAGE"])){
				
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_MESSAGE_WRONG");
				
			}else{
				
				$MESSAGE = strip_tags($_POST['MESSAGE']);
				$MESSAGE = mysql_escape_string($MESSAGE);
				
			}
		
		}
				
		
		if($arParams["USE_CAPTCHA"] == "Y")
		{
			include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
			$captcha_code = $_POST["captcha_sid"];
			$captcha_word = $_POST["captcha_word"];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString("main", "captcha_password", "");
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0)
			{
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
					$arResult["ERROR_MESSAGE"][] = GetMessage("MF_CAPTCHA_WRONG");
			}
			else
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_CAPTHCA_EMPTY");

		}			
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			$arFields = Array(
				"AUTHOR" => $user_name,
				"AUTHOR_EMAIL" => $user_email,
				"AUTHOR_SUBJECT" => $user_subject,
				"EMAIL_TO" => $arParams["EMAIL_TO"],
				"TEXT" => $MESSAGE,
			);
			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
				foreach($arParams["EVENT_MESSAGE_ID"] as $v)
					if(IntVal($v) > 0)
						CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
			}
			else
				CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
			$_SESSION["MF_NAME"] = htmlspecialcharsbx($_POST["user_name"]);
			$_SESSION["MF_EMAIL"] = htmlspecialcharsbx($_POST["user_email"]);
			$_SESSION["MF_SUBJECT"] = htmlspecialcharsbx($_POST["user_subject"]);
			LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));
		}
		
		$arResult["MESSAGE"] = htmlspecialcharsbx($_POST["MESSAGE"]);
		$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_POST["user_name"]);
		$arResult["AUTHOR_SUBJECT"] = htmlspecialcharsbx($_POST["user_subject"]);
		$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($_POST["user_email"]);
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
	$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

if(empty($arResult["ERROR_MESSAGE"]))
{
	if($USER->IsAuthorized())
	{
		$arResult["AUTHOR_NAME"] = $USER->GetFormattedName(false);
		$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($USER->GetEmail());
	}
	else
	{
		if(strlen($_SESSION["MF_NAME"]) > 0)
			$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_SESSION["MF_NAME"]);
		if(strlen($_SESSION["MF_EMAIL"]) > 0)
			$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($_SESSION["MF_EMAIL"]);
		if(strlen($_SESSION["MF_SUBJECT"]) > 0)
			$arResult["AUTHOR_SUBJECT"] = htmlspecialcharsbx($_SESSION["MF_SUBJECT"]);
	}
}

if($arParams["USE_CAPTCHA"] == "Y")
	$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();
