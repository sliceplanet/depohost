<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
}
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="contacts-form">
  <? if (!empty($arResult["ERROR_MESSAGE"]))
  {
    foreach ($arResult["ERROR_MESSAGE"] as $v)
    {
      ShowError($v);
    }
  }
  if (strlen($arResult["OK_MESSAGE"]) > 0)
  {
    ?>
    <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div>
	<script type="text/javascript">
		dataLayer.push({'event': 'Form_Submit_Feedback'});
	</script>
  <?
  }
  ?>

  <? if (strlen($arResult["OK_MESSAGE"]) > 0 || !empty($arResult["ERROR_MESSAGE"])) { ?>
    <script>
      Recaptchafree.reset();
    </script>
  <? } ?>

  <form action="<?= POST_FORM_ACTION_URI ?>" method="POST">
    <?= bitrix_sessid_post() ?>
    <div class="mf-name">
      <div class="field-name">
        <?= GetMessage("MFT_NAME") ?> <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?>
          <span class="mf-req">*</span><? endif ?>
      </div>
      <div class="field"><input class="input" type="text" name="user_name" value="<?= $arResult["AUTHOR_NAME"] ?>"/><i></i></div>
    </div>
    <div class="mf-email">
      <div class="field-name">
        <?= GetMessage("MFT_EMAIL") ?> <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])): ?>
          <span class="mf-req">*</span><? endif ?>
      </div>
      <div class="field"><input class="input" type="text" name="user_email" value="<?= $arResult["AUTHOR_EMAIL"] ?>"/><i></i></div>
    </div>
    <div class="mf-name">
      <div class="field-name">
        <?= GetMessage("MFT_SUBJECT") ?> <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("SUBJECT", $arParams["REQUIRED_FIELDS"])): ?>
          <span class="mf-req">*</span><? endif ?>
      </div>
      <div class="field"><input class="input" type="text" name="user_subject" value="<?= $arResult["AUTHOR_SUBJECT"] ?>"/><i></i>
      </div>
    </div>

    <div class="mf-message">
      <div class="field-name">
        <?= GetMessage("MFT_MESSAGE") ?> <? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])): ?>
          <span class="mf-req">*</span><? endif ?>
      </div>
      <textarea name="MESSAGE" rows="5" cols="40" class="textarea"><?= $arResult["MESSAGE"] ?></textarea>
    </div>

    <? if ($arParams["USE_CAPTCHA"] == "Y"): ?>
      <div class="mf-captcha">
        <div class="field-name"><?= GetMessage("MFT_CAPTCHA") ?></div>
        <input type="hidden" name="captcha_sid" value="<?= $arResult["capCode"] ?>">
        <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["capCode"] ?>" width="180" height="40"
             alt="CAPTCHA">
        <div class="field"><input class="input" type="text" name="captcha_word" size="30" maxlength="50" value=""></div>
      </div>
    <? endif; ?>
    <input type="text" name="check" value="" style="display:none;"/>
    <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>"/>
    <div class="form-btn left"><input type="submit" name="submit" value="<?= GetMessage("MFT_SUBMIT") ?>"
                                      class="btn-big"></div>
  </form>
</div>