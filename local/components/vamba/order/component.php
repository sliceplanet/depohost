<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Main\Application,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\Delivery,
	
	Bitrix\Highloadblock as HL,
	Bitrix\Sale\PriceMaths,
	Bitrix\Iblock,
	Bitrix\Catalog;
	
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');

//echo var_dump($_POST);

if($_POST['make_order'] == 'Y'):

	$APPLICATION->RestartBuffer();
	
	$pt = $_POST['PERSON_TYPE'];
	if($pt == 1):
		$email = $_POST['field'][1][6];
	elseif($pt == 2):
		$email = $_POST['field'][2][9];
	elseif($pt == 3):
		$email = $_POST['field'][3][38];
	endif;
	
	if(!$USER->IsAuthorized())
	{	
		$pass = randString(7, array(
		  "abcdefghijklnmopqrstuvwxyz",
		  "ABCDEFGHIJKLNMOPQRSTUVWX­YZ",
		  "0123456789",
		  "!@#\$%^&*()",
		));
		
		
		$ures = CUser::GetList(($by="personal_country"), ($order="desc"), array('EMAIL'=>$email));
		if($ures->SelectedRowsCount()):
			$arr = $ures->Fetch();
			$ID = $arr['ID'];
		else:
			$login = $email;
		
			
			$user = new CUser;
			$arFields = Array(
		//	  "NAME"              => $_POST['ORDER']['PROPS'][1] ?: $_POST['ORDER']['PROPS'][9],
			  "EMAIL"             => $login,
			  "LOGIN"             => $login,
			  "LID"               => "ru",
			  "ACTIVE"            => "Y",
			  "PASSWORD"          => $pass,
			  "CONFIRM_PASSWORD"  => $pass,
			);

			$ID = $user->Add($arFields);
			if (intval($ID) > 0)
			{
				$USER->Authorize($ID);
				$new_user = true;
			}
			else
				echo $user->LAST_ERROR;
			
		endif;
	}
	
	$json = [];
	
	$UID = $USER->GetID() ?: $ID;

	$req = Application::getInstance()->getContext()->getRequest();
    $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
    $currencyCode = Option::get('sale', 'default_currency', 'RUB');

    $order = Order::create($siteId, $UID);
    $order->setPersonTypeId($pt);
	$order->setField("DATE_INSERT", new \Bitrix\Main\Type\DateTime());
	
    $basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();
	
    $order->setBasket($basket);
	

	
    /*Shipment*/
	$shipmentCollection = $order->getShipmentCollection();
	$shipment = $shipmentCollection->createItem();
	$service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
	$shipment->setFields(array(
		'DELIVERY_ID' => $service['ID'],
		'DELIVERY_NAME' => $service['NAME'],
	));
    $shipmentItemCollection = $shipment->getShipmentItemCollection();
    foreach ($order->getBasket() as $item)
    {
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());
    }
	
	
	
    $paymentCollection = $order->getPaymentCollection();
	$payment = $paymentCollection->createItem();
	$paySystemService = PaySystem\Manager::getObjectById(7);
	$pname = $paySystemService->getField("NAME");
	$payment->setFields(array(
		'PAY_SYSTEM_ID' => $_POST['payment_method'],//$paySystemService->getField("PAY_SYSTEM_ID"),
		'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
		'SUM' => $order->getPrice(),
	));

    /**/
    $order->doFinalAction(true);
	
    $order->setField('CURRENCY', $currencyCode);
	
    $order->setField('USER_DESCRIPTION', $_POST['COMMENT']);
	
	$propertyCollection = $order->getPropertyCollection();
	foreach($_POST['field'][$pt] as $k => $v):
		$somePropValue[$k] = $propertyCollection->getItemByOrderPropertyId($k);
		$somePropValue[$k]->setValue($v);
	endforeach;
	
    $r = $order->save();
    $orderId = $order->GetId();

    if($orderId > 0){
		$json['order_id'] = $orderId;
		//LocalRedirect('success.php?ORDER_ID='.$orderId);
        //echo "Ваш заказ оформлен";
    }
    else{
	
		$arResult['ERRORS'] = [];
		global $APPLICATION;
		if ($ex = $APPLICATION->GetException())
			$arResult['ERRORS'][] = $ex->GetString();
		
		// либо объекты ошибок с доп данными
		//print_r($r->getErrors());
		// либо только сообщения
		//print_r($r->getErrorMessages());
		
		$arResult['ERRORS'] = array_merge($arResult['ERRORS'],$r->getErrorMessages());
		$json['errors'] = $arResult['ERRORS'];
		/*
		// так же в заказе могут быть предупреждения, которые не являются причиной остановки процесса сохранения заказа, но мы их сохраняем в маркировки 
		print_r($r->getWarnings());
		print_r($r->getWarningMessages());
*/
						
						
		//echo "<p style='color:red;'>Ошибка оформления</p>";
	}	
	
	if($orderId)
	{
	
		$ORDER_ID = $orderId;
		
	//	CSaleOrder::Update($ORDER_ID,['PRICE_DELIVERY'=>$_POST['delivery_price']]);

	}
	
	if($new_user):
		CUser::SendUserInfo($UID, SITE_ID, "Приветствуем Вас как нового пользователя нашего сайта!");
	endif;
	
	$email = COption::GetOptionString("sale", "order_email");
	
	$order_list = '';
	foreach($basket as $basketItem):
		$order_list .= $basketItem->getField('NAME') . ' - ' . $basketItem->getQuantity() . '<br />';
	endforeach;
	
	$order_list .= '<br/>';
		
	
	$arFields = [
		'SALE_EMAIL' => $email,
		'EMAIL' => $email,
		'ORDER_ID' => $ORDER_ID,
		'ORDER_ACCOUNT_NUMBER_ENCODE' => $ORDER_ID,
		'ORDER_REAL_ID' => $ORDER_ID,
		'ORDER_DATE' => date('d.m.Y H:i:s'),
		'ORDER_USER' => $USER->GetFullName(),
		'PRICE' => $order->getField('PRICE'),
		'EMAIL' => $USER->GetEmail(),
		'ORDER_LIST' => $order_list,
	];
	
	CEvent::Send('SALE_NEW_ORDER','s1',$arFields);
	
	//////////////////////////////
	
	echo json_encode($json);
	//CSaleBasket::DeleteAll();
	die;
	
endif;




$this->includeComponentTemplate();


	
?>