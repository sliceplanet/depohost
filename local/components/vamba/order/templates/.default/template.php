<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	//$this->setFrameMode(true);
	//echo var_dump($_COOKIE);
	$arUser = $USER->GetByID($USER->GetID())->Fetch();


	$countries = [];
   $db_vars = CSaleLocation::GetList(
        array(
                "SORT" => "ASC",
                "COUNTRY_NAME_LANG" => "ASC",
                "CITY_NAME_LANG" => "ASC"
            ),
        array("LID" => LANGUAGE_ID,'CITY_NAME'=>false),
        false,
        false,
        array()
    );
   while ($vars = $db_vars->Fetch()):

		$countries[] = $vars;

   endwhile;	
	
	
	
?>
<div id="order">
	<div class="container">
		<form class="order-form">
		
			<div class="form-block">
				<div class="form-block__h">Тип договора</div>
				<div class="form-block__content">
					<div class="row">
						<div class="col-md-6">
							<label class="radio-label">
								<input type="radio" name="PERSON_TYPE" value="2" checked=""> <span class="checker"><span></span></span> Юридическое лицо
							</label>
						</div>
						<div class="col-md-6">
							<label class="radio-label">
								<input type="radio" name="PERSON_TYPE" value="1"> <span class="checker"><span></span></span> Физическое лицо
							</label>
						</div>
						<div class="col-md-6">
							<label class="radio-label">
								<input type="radio" name="PERSON_TYPE" value="3"> <span class="checker"><span></span></span> Индивидуальный предприниматель
							</label>
						</div>
						<div class="col-md-6">
							<label class="radio-label">
								<input type="radio" name="PERSON_TYPE" value="4"> <span class="checker"><span></span></span> По договору
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="page-props pt2">


				<div class="form-block">
					<div class="form-block__h">Информация о юридическом лице</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="label-control">Наименование организации <span class="required">*</span></label>
									<input type="text" name="field[2][10]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group is-location-1">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group is-location-2">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="field[2][3]" class="location_city">
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ФИО генерального директора <span class="required">*</span></label>
									<input type="text" name="field[2][19]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Директор исполняет обязанности на основании</label>
									<select name="field[2][20]">
										<option value="1">Устава</option>
										<option value="2">Доверенности</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Юридический адрес <span class="required">*</span></label>
									<textarea name="field[2][8]" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="field[2][14]" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="field[2][21]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="form-block">
					<div class="form-block__h">Коды организации</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ИНН <span class="required">*</span></label>
									<input type="text" name="field[2][15]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">КПП <span class="required">*</span></label>
									<input type="text" name="field[2][16]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Банковские реквизиты </div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Расчетный счет <span class="required">*</span></label>
									<input type="text" name="field[2][24]" class="form-control">
								</div>
							</div>
							<?/*
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Корреспондентский счет <span class="required">*</span></label>
									<input type="text" name="" class="form-control">
								</div>
							</div>
							*/?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">БИК <span class="required">*</span></label>
									<input type="text" name="field[2][26]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="field[2][11]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="field[2][9]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail <span class="required">*</span></label>
									<input type="text" name="field[2][27]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактная персона <span class="required">*</span></label>
									<input type="text" name="field[2][48]" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="1">
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на обработку персональных данных
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<button class="button-blue2">ОФОРМИТЬ ЗАКАЗ</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="page-props pt1">
			
				<div class="form-block">
					<div class="form-block__h">Информация о физическом лице</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="field[1][2]" class="location_city">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="field[1][5]" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="field[1][4]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="field[1][22]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="field[1][6]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail <span class="required">*</span></label>
									<input type="text" name="field[1][1]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ФИО <span class="required">*</span></label>
									<input type="text" name="field[1][7]" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="1">
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на обработку персональных данных
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<button class="button-blue2">ОФОРМИТЬ ЗАКАЗ</button>
							</div>
						</div>
					</div>
				</div>
				
			
			</div>
			
			<div class="page-props pt3">

				<div class="form-block">
					<div class="form-block__h">Информация об индивидуальном предпринимателе</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="label-control">Ф.И.О предпринимателя <span class="required">*</span></label>
									<input type="text" name="field[3][28]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Местоположение<span class="required">*</span></label>
									<select class="location_country">
										<option value=""></option>
										<?foreach($countries as $c):?>
											<option value="<?=$c['ID']?>"><?=$c['COUNTRY_NAME']?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control"><span class="required" style="display:none;">*</span></label>
									<select name="field[3][17]" class="location_city">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Адрес для доставки писем <span class="required">*</span></label>
									<textarea name="field[3][30]" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Почтовый индекс <span class="required">*</span></label>
									<input type="text" name="field[3][29]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Коды организации</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ИНН <span class="required">*</span></label>
									<input type="text" name="field[3][32]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">ОГРНИП <span class="required">*</span></label>
									<input type="text" name="field[3][31]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-block">
					<div class="form-block__h">Банковские реквизиты </div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Банк <span class="required">*</span></label>
									<input type="text" name="field[3][33]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Расчетный счет <span class="required">*</span></label>
									<input type="text" name="field[3][34]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">БИК <span class="required">*</span></label>
									<input type="text" name="field[3][36]" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-block">
					<div class="form-block__h">Контактная информация</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактный телефон <span class="required">*</span></label>
									<input type="text" name="field[3][37]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">E-Mail <span class="required">*</span></label>
									<input type="text" name="field[3][38]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Дополнительный E-mail <span class="required">*</span></label>
									<input type="text" name="field[3][38]" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="label-control">Контактная персона <span class="required">*</span></label>
									<input type="text" name="field[3][40]" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="1">
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на обработку персональных данных
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<button class="button-blue2">ОФОРМИТЬ ЗАКАЗ</button>
							</div>
						</div>
					</div>
				</div>
							
			</div>
			
			<div class="page-props pt4">
				<div class="form-block">
					<div class="form-block__h">Информация о договоре</div>
					<div class="form-block__content">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="label-control">Номер договора <span class="required">*</span></label>
									<input type="text" name="field[4][18]" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-label agree-label">
										<input type="checkbox" name="agree" value="1">
										<span class="checkbox-checker"><span></span></span> 
										Я даю согласие на обработку персональных данных
									</label>
								</div>
							</div>
							<div class="col-md-12">
								<button class="button-blue2">ОФОРМИТЬ ЗАКАЗ</button>
							</div>
						</div>
					</div>
				</div>
			
			</div>
			
			<input type="hidden" name="make_order" value="Y" />
		</form>
	</div>
</div>

