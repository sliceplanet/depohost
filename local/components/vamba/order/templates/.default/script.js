$(document).ready(function(){
	
//	alert(1);
	
	var pt = $('[name="PERSON_TYPE"]:checked').val();
	
	$('.page-props.pt'+pt).show();
	
	$('[name="PERSON_TYPE"]').change(function(){
		
		pt = $('[name="PERSON_TYPE"]:checked').val();
		$('.page-props').hide();
		if(pt == 4)
			location.href = '/auth/?backurl=/personal/order/make/';
		else
			$('.page-props.pt'+pt).show();
		
	});
	
	
	$('.order-form').submit(function(){
		
		var a = $(this).serialize();
		var go = true;
		var to;
		$('.form-group .error').removeClass('error');
		
		$('.page-props.pt'+pt).find('.form-group').each(function() {
			
			var req = $(this).find('.required').length;
			
			if(req)
			{
				var field = $(this).find('input').eq(0);
				if(!field.length)
					field = $(this).find('select').eq(0);
				if(!field.length)
					field = $(this).find('textarea').eq(0);
					
				if(!field.val())
				{
					field.addClass('error');
					go = false;
				}
					
			}
			
			
		});
		
		if(!go)
		{
			var t = $('.page-props.pt'+pt).find('.form-group .error').eq(0).offset().top - 95;
			$('html,body').animate({scrollTop:t+'px'},500);
		}
		else
		{
	
			$.post(location.pathname,a,
			function(data){
				
				if(data.order_id)
				{
					location.href = '/personal/services/';
				}
				else
				{
					if(data.error)
						alert(data.error);
					else
						alert('Произошла ошибка! Повторите попытку позже или свяжитесь с администраицей сайта.');
				}
				
			},'json');
			
		}
		
		
		return false;
	});
	
	
	$('.location_country').change(function(){
		
		var c = $(this).val();
		var t = $(this);
		
		$.get('/local/components/vamba/order/templates/.default/ajax-get_cities.php?cid='+c,
		function(data){
			
			t.parent().parent().next().find('.location_city').html(data);
			
		});
		
		
	});
	
	
	$('input,textarea').keyup(function(){
		
		if($(this).val())
		{
			$(this).removeClass('error');
		}
		
	});
	$('select').change(function(){
		
		if($(this).val())
		{
			$(this).removeClass('error');
		}
		
	});
	
});