<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;

define("NO_KEEP_STATISTIC", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/cart/ajax-add.php');
global $APPLICATION;

$APPLICATION->RestartBuffer();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');


$basket_id = (int)$_GET['id'];
$new_cnt = (int)$_GET['cnt'];

$b = CSaleBasket::GetByID($basket_id);
$arr = CIBlockElement::GetByID($b['PRODUCT_ID'])->Fetch();

if($arr['IBLOCK_ID'] == 28):
	$name = 'Аренда программного обеспечения Microsoft '.$arr['NAME'];
endif;

CSaleBasket::Update($basket_id,['QUANTITY'=>$new_cnt]);
CSaleBasket::Update($basket_id,['NAME'=>$name]);


?>