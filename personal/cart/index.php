<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>

<? $APPLICATION->IncludeComponent(
  "bitrix:sale.basket.basket",
  "v2020",
  array(
	"COLUMNS_LIST" => array(
	  0 => "NAME",
	  1 => "PROPS",
	  2 => "DELETE",
	  3 => "PRICE",
	  4 => "QUANTITY",
	  5 => "SUM",
	),
	"PATH_TO_ORDER" => "/personal/order/make/",
	"HIDE_COUPON" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
	"USE_PREPAYMENT" => "N",
	"QUANTITY_FLOAT" => "N",
	"SET_TITLE" => "N",
	"ACTION_VARIABLE" => "action"
  ),
  false
); ?>
<section class="page-section section-graybg cart-bottom-section">
	<div class="page-section__h">Добавить другие услуги</div>
<? $APPLICATION->IncludeFile(
	"/include/mainpage/services.php"
  ); ?>
</section>

<?/*

  <div class="cart">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="wrapper">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <? $APPLICATION->IncludeComponent(
              "bitrix:sale.basket.basket",
              ".default",
              array(
                "COLUMNS_LIST" => array(
                  0 => "NAME",
                  1 => "PROPS",
                  2 => "DELETE",
                  3 => "PRICE",
                  4 => "QUANTITY",
                  5 => "SUM",
                ),
                "PATH_TO_ORDER" => "/personal/order/make/",
                "HIDE_COUPON" => "Y",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "USE_PREPAYMENT" => "N",
                "QUANTITY_FLOAT" => "N",
                "SET_TITLE" => "N",
                "ACTION_VARIABLE" => "action"
              ),
              false
            ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
*/?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>