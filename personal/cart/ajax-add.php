<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;

define("NO_KEEP_STATISTIC", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/cart/ajax-add.php');
global $APPLICATION;

$APPLICATION->RestartBuffer();

$random_string = md5(rand(10000,1000000000000000));

// add basket dedicated server
$arProps = array();

if (array_key_exists('action', $_REQUEST) && ($_REQUEST['action'] == 'ADD2BASKET' || $_REQUEST['action'] == 'BUY')) {
    Loader::includeModule('catalog');
    Loader::includeModule('sale');
    /**@var integer $base_price_id ID базовой цены */
    $base_price_id = 5;
    $quantity = (array_key_exists('quantity', $_REQUEST) && (int)$_REQUEST['quantity'] > 0) ? (int)$_REQUEST['quantity'] : 1;

    if (array_key_exists('product_id', $_REQUEST)) {
        $product_id = (int)$_REQUEST['product_id'];
    }
	
	
	if($_REQUEST['bid']):
		CSaleBasket::Delete($_REQUEST['bid']);
	endif;

//    $quantity = (array_key_exists('months', $_REQUEST) && (int) $_REQUEST['months'] > 0) ? (int) $_REQUEST['months'] : $quantity;


    $arFieldsDefault = array(
        'LID' => LANG,
        'CURRENCY' => 'RUB',
        'PRODUCT_PROVIDER_CLASS' => '',
        'MODULE' => 'catalog',
        'QUANTITY' => $quantity,
        'MEASURE_NAME' => Loc::getMessage('MEASURE_NAME_PC'),
    );

    $arFieldsDefault['PRODUCT_ID'] = 1; // hack for items no available to catalog

    $arFields = $arFieldsDefault;
    $arFields['PROPS'] = &$arProps;

    // handle properties from request
    if (array_key_exists('DOM_NAME', $_REQUEST) && !empty($_REQUEST['DOM_NAME'])) {
        $arProps[] = array(
            'NAME' => Loc::getMessage('PROPERTY_DOMAIN_NAME'),
            'CODE' => 'DOMAIN',
            'VALUE' => htmlspecialcharsbx($_REQUEST['DOM_NAME']),
            'SORT' => 100,
        );
    }
    if (array_key_exists('prop', $_REQUEST) && !empty($_REQUEST['prop']) && !empty($_REQUEST['id'])) {
        $res = CIBlockElement::GetByID((int)$_REQUEST['id']);
        $ar = $res->Fetch();
        $IBLOCK_ID = $ar['IBLOCK_ID'];
        $arFields['NAME'] = $ar['NAME'];
        $arFields['PRODUCT_ID'] = (int)$_REQUEST['id'];
        $arFields['PRODUCT_PROVIDER_CLASS'] = "CCatalogProductProvider";
        $arFields['CATALOG_XML_ID'] = $ar['IBLOCK_EXTERNAL_ID'];

        foreach ($_REQUEST['prop'] as $code => $value) {
            $bEnum = true;
            if ((int)$value > 0) {
                $res = CIBlockPropertyEnum::GetList(array('sort' => 'asc'), array('ACTIVE' => 'Y', 'PROPERTY_ID' => $code, 'ID' => $value, 'IBLOCK_ID' => $IBLOCK_ID));
                if ($ar = $res->Fetch()) {
                    $value = $ar['VALUE'];
                    $name = $ar['PROPERTY_NAME'];
                    switch ($code) {
                        case 'CPU_QTY':
                            $value = $value . " " . Loc::getMessage('MEASURE_NAME_CORE');
                            break;
                        case 'IP_QTY':
                            $value = $value . " " . Loc::getMessage('MEASURE_NAME_PC');
                            break;
                        case 'FTP':
                        case 'RAM':
                        case 'HDD':
                            $value = $value . " " . Loc::getMessage('MEASURE_NAME_GB');
                            break;
                    }
                } else {
                    $bEnum = false;
                }
            } else {
                $bEnum = false;
            }
            if (!$bEnum) {
                $res = CIBlockProperty::GetByID($code, $IBLOCK_ID);
                if ($ar = $res->Fetch()) {
                    $name = $ar['NAME'];
                }
            }
            $arProps[] = array(
                'NAME' => $name,
                'CODE' => $code,
                'VALUE' => $value,
                'SORT' => 100,
            );
        }
    }
    if (array_key_exists('SUM', $_REQUEST) && (int)$_REQUEST['SUM'] > 0 && (int)$_REQUEST['quantity'] > 0) {
//        $arFields['PRICE'] = intval($_REQUEST['SUM'] / $_REQUEST['quantity']);
        $arFields['PRICE'] = intval($_REQUEST['SUM']);
		
    }
    if (array_key_exists('price_1month', $_REQUEST) && (int)$_REQUEST['price_1month'] > 0) {
		$price_1m_prop = [
			'NAME' => 'Цена за 1 месяц',
			'CODE' => 'price_1month',
			'VALUE' => (int)$_REQUEST['price_1month'],
			'SORT' => 5,
		];
            $arProps[] = $price_1m_prop;
		
    }
    if (array_key_exists('months', $_REQUEST)) {

        $months = (int)$_REQUEST['months'] > 0 ? (int)$_REQUEST['months'] : 1;
        $arMonthsProps = array(
            'NAME' => Loc::getMessage('PROPERTY_MONTHS_NAME'),
            'VALUE' => $months . " " . CUsefull::endOfWord($months, array('месяцев', 'месяц', 'месяца')),
            'CODE' => 'MONTHS',
            'SORT' => 1000,
        );
        $arProps[] = $arMonthsProps;
    }
    if (array_key_exists('years', $_REQUEST)) {
        $years = (int)$_REQUEST['years'] > 0 ? (int)$_REQUEST['years'] : 1;

        $arProps[] = array(
            'NAME' => Loc::getMessage('PROPERTY_YEARS_NAME'),
            'VALUE' => $years . " " . CUsefull::endOfWord($years, array('лет', 'год', 'года')),
            'CODE' => 'YEARS',
            'SORT' => 1000,
        );
    }
    if (is_array($_REQUEST['FIELDS'])) {
        foreach ($_REQUEST['FIELDS'] as $key => $value) {
            $arFields[$key] = urldecode($value);
        }
    }

    if (is_array($_REQUEST['PROPS'])) {
        foreach ($_REQUEST['PROPS'] as $code => $arProp) {
            if (!empty($arProp['NAME'])) {
                $arProps[] = array(
                    'NAME' => $arProp['NAME'],
                    'VALUE' => $arProp['VALUE'],
                    'CODE' => $code,
                );
            }
        }
    }
	
	if($_REQUEST['is-ms'] == 'Y'):
		
            $arProps[] = array(
                'NAME' => 'is-ms',
                'VALUE' => true,
                'CODE' => 'is-ms',
                'SORT' => 1000,
            );
	endif;

    switch ($_REQUEST['type']) {
        case 'solution':
            Loader::includeModule('dedicated');
            $arResult = Dedicated::getSolution($product_id);
            foreach ($arResult['PROPERTIES'] as $code => $arProperty) {
                if (!empty($arProperty['DISPLAY_VALUE'])) {
                    switch ($code) {
                        case 'CPU':
                            $sort = 100;
                            break;
                        case 'RAM':
                            $sort = 200;
                            break;
                        case 'HDD':
                            $sort = 300;
                            break;
                        case 'RAID':
                            $sort = 400;
                            break;
                        case 'MONTHS':
                            $sort = 1000;
                            break;
                        default:
                            $sort = 500;
                            break;
                    }
                    $arProps[] = array(
                        'NAME' => $arProperty['NAME'],
                        'VALUE' => str_replace('<br>', ", ", $arProperty['DISPLAY_VALUE']),
                        'CODE' => $code,
                        'SORT' => $sort,
                    );
                }
            }
			
			$arProps[] = array(
				'NAME' => 'unique_id',
				'VALUE' => $random_string,
				'CODE' => 'unique_id',
				'SORT' => 1,
			);	
            $arProps[] = array(
                'NAME' => 'is-solution',
                'VALUE' => true,
                'CODE' => 'is-solution',
                'SORT' => 1000,
            );
			
			
            $arFields['NAME'] = $arResult['NAME'];
            $arFields['PRICE'] = $arResult['PRICES'][$months]['PRICE'] * $months;
            $arFields['MEASURE_CODE'] = 362;    // months
            break;
        case 'domain':
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "CODE" => "DOMAIN_BUY", "VALUE" => htmlspecialcharsbx($_REQUEST['DOMAIN_ZONE'])));
            if ($enum_fields = $property_enums->GetNext()) {
                $arFields['PRICE'] = (int)$enum_fields["XML_ID"];
            }
            $arFields['NAME'] = Loc::getMessage('SERVICE_DOMAIN_NAME');
            $arProps[] = array(
                "NAME" => Loc::getMessage('PROPERTY_DOMAIN_NAME'),
                "CODE" => "DOMAIN_NAME",
                "VALUE" => htmlspecialcharsbx(urldecode($_REQUEST['DOMAIN_NAME'])),
                'SORT' => 100,
            );
            $arProps[] = array(
                'NAME' => 'is-domain',
                'VALUE' => true,
                'CODE' => 'is-domain',
                'SORT' => 1000,
            );
            break;
        case '1c':

            $IBLOCK_ID = 23;
            Loader::includeModule('iblock');
            $res = CIBlock::GetByID($IBLOCK_ID);
            $ar = $res->Fetch();
            $arFields['CATALOG_XML_ID'] = $ar['XML_ID'];
            foreach ($_REQUEST['quantity'] as $i => $prodq) {
                $arProps = array();
                if ($_REQUEST['id'][$i] > 0) {

                    $arSelect = Array("ID", "NAME");
                    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "NAME" => $_REQUEST['prname'][$i]);
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, array('nTopCount' => 1), $arSelect);
                    if ($ob = $res->GetNextElement()) {
                        $arname = $ob->GetFields();
                    }
                    $arFields['PRODUCT_ID'] = $arname['ID'];
                    $arFields['NAME'] = $arname['NAME'];
                    $arFields['QUANTITY'] = $prodq;
                    $arFields['PRICE'] = (int)$_REQUEST['price'][$i] + (int)$_REQUEST['office-price'];


                    $arProps[] = array(
                        "NAME" => Loc::getMessage('PROPERTY_VARIANT_WORK_NAME'),
                        "CODE" => "VAR_WORK",
                        "VALUE" => $_REQUEST['VAR_WORK'],
                        'SORT' => 100,
                    );

                    if ($_REQUEST['PRICE_OFFICE'] != '') {
                        $arProps[] = array(
                            "NAME" => Loc::getMessage('PROPERTY_OFFICE_NAME'),
                            "CODE" => "PRICE_OFFICE",
                            "VALUE" => $_REQUEST['PRICE_OFFICE'],
                            'SORT' => 100,
                        );
                    }

                    $arProps[] = array(
                        "NAME" => Loc::getMessage('PROPERTY_1C_ACCESS_NAME'),
                        "CODE" => "1C_ACCESS",
                        "VALUE" => $_REQUEST['access'],
                        'SORT' => 100,
                    );
					//kamil
					if ($_REQUEST['TEST_PERIOD_1C'] != '') {
						$arProps[] = array(
							"NAME" => Loc::getMessage('PROPERTY_TEST_PERIOD_1C'),
							"CODE" => "TEST_PERIOD_1C",
							"VALUE" => isset($_REQUEST['TEST_PERIOD_1C']) && strlen($_REQUEST['TEST_PERIOD_1C']) > 0 ? $_REQUEST['TEST_PERIOD_1C'] : 'Нет',
							'SORT' => 100,
						);
					}
					
                    $arFields['PRODUCT_PROVIDER_CLASS'] = "";
                    $arFields["PROPS"] = $arProps;
                    $basket_id = CSaleBasket::Add($arFields);
                }//
            }
            $arFields = array();
            if ($_REQUEST['access'] == 'VDS') {
                LocalRedirect("/vds/");
            } elseif ($_REQUEST['access'] == 'Выделенный сервер') {
                LocalRedirect("/dedicated_server/");
            }
			
            $arProps[] = array(
                'NAME' => 'is-1c',
                'VALUE' => true,
                'CODE' => 'is-1c',
                'SORT' => 1000,
            );
			
            break;
        case 'ispmanager':
            $arPropsISP = $arProps;
            foreach ($_REQUEST['nquantity'] as $key => $nquant) {
                $arPropIpAddress = array();
                if ((int)$nquant > 0) {
                    $arFields['PRODUCT_ID'] = (int)$_REQUEST['nid'][$key];


                    $arFields['QUANTITY'] = (int)$nquant;
                    $arFields['IGNORE_CALLBACK_FUNC'] = "Y";
                    $res = CIBlockElement::GetByID($arFields['PRODUCT_ID']);
                    $ar = $res->Fetch();
                    $arPrice = \CPrice::GetBasePrice($arFields['PRODUCT_ID']);
                  $arFields['NAME'] = $ar['NAME'];
                  $arFields['PRICE'] = $arPrice['PRICE'];
                  $arFields['CURRENCY'] = $arPrice['CURRENCY'];
                  $arFields['CATALOG_XML_ID'] = $ar['IBLOCK_EXTERNAL_ID'];

                    foreach ($_REQUEST['nprop' . $_REQUEST['nid'][$key]] as $i => $value) {
                        $arPropIpAddress[] = array(
                            "NAME" => Loc::getMessage('PROPERTY_ISPMANAGER_IP_ADDRESS', array('#NUMBER#' => ($i + 1))),
                            "CODE" => 'ISP_IP_ADDRESS',
                            "VALUE" => $value,
                            'SORT' => 100,
                        );
                    }

                    $arFields["PROPS"] = array_merge($arPropsISP, $arPropIpAddress);
                    $res = CSaleBasket::Add($arFields);
				//	echo var_dump($arFields);die;
					
					$basket_id = $res;
                  $arDebug['ispmanager'][] = $arFields;
                }
            }
            $arFields = array();
            $arProps[] = array(
                'NAME' => 'is-isp',
                'VALUE' => true,
                'CODE' => 'is-isp',
                'SORT' => 1000,
            );
            break;
        case 'configurator':
            $IBLOCK_ID = 21;
            Loader::includeModule('iblock');
            $res = CIBlock::GetByID($IBLOCK_ID);
            $ar = $res->Fetch();
            $arFields['CATALOG_XML_ID'] = $ar['XML_ID'];
            $arFields['PRICE'] = $arFields['PRICE'] * $months;
            foreach ($arProps as $key => $arProp) {
                switch ($arProp['CODE']) {
                    case 'CPU':
                        $sort = 100;
                        break;
                    case 'RAM':
                        $sort = 200;
                        break;
                    case 'HDD':
                        $sort = 300;
                        break;
                    case 'RAID':
                        $sort = 400;
                        break;
                    case 'MONTHS':
                        $sort = 1000;
                        break;
                    default:
                        $sort = 500;
                        break;
                }
                $arProps[$key]['SORT'] = $sort;
            }
			$arProps[] = array(
				'NAME' => 'unique_id',
				'VALUE' => $random_string,
				'CODE' => 'unique_id',
				'SORT' => 1,
			);	
			$arProps[] = array(
				'NAME' => 'Сервер собраный на конфигураторе',
				'VALUE' => true,
				'CODE' => 'is-configurator',
				'SORT' => 1,
			);	

            break;
        case 'ssl':
            $arFields['PRODUCT_PROVIDER_CLASS'] = "";
            $arProps[] = array(
                'NAME' => 'is-ssl',
                'VALUE' => true,
                'CODE' => 'is-ssl',
                'SORT' => 1000,
            );
            break;
        case 'vds':
            $arFields['PRODUCT_PROVIDER_CLASS'] = "";
            $arFields['QUANTITY'] = 1;
            $months = (int)$quantity;
            $arProps[] = array(
                'NAME' => Loc::getMessage('PROPERTY_MONTHS_NAME'),
                'VALUE' => $months . " " . CUsefull::endOfWord($months, array('месяцев', 'месяц', 'месяца')),
                'CODE' => 'MONTHS',
                'SORT' => 1000,
            );
            break;
            $arProps[] = array(
                'NAME' => 'is-vds',
                'VALUE' => true,
                'CODE' => 'is-vds',
                'SORT' => 1000,
            );
            break;

        case 'multy':
        case 'multi':
        case 'multiple':
        default:
            if (!is_array($_REQUEST['quantity'])) {
                if ((int)$_REQUEST['quantity'] <= 0) {
                    $_REQUEST['quantity'] = 1;
                }
                $_REQUEST['quantity'] = array(0 => $_REQUEST['quantity']);
                $_REQUEST['id'] = array(0 => $_REQUEST['id']);
            }
            if (is_array($_REQUEST['quantity'])) {
                Loader::includeModule('iblock');
                foreach ($_REQUEST['quantity'] as $i => $prodq) {
                    if ((int)$_REQUEST['id'][$i] > 0 && (int)$prodq > 0) {
                        $arFields['PRODUCT_ID'] = (int)$_REQUEST['id'][$i];
                        $arFields['PRODUCT_PROVIDER_CLASS'] = "";
                        $arFields['QUANTITY'] = $prodq;


                        $res = CIBlockElement::GetList(array(), array('=ID' => $arFields['PRODUCT_ID']), false, false, array('IBLOCK_EXTERNAL_ID', 'ID', 'NAME', 'CATALOG_GROUP_' . $base_price_id));
                        $ar = $res->Fetch();
                        $arFields['CATALOG_XML_ID'] = $ar['IBLOCK_EXTERNAL_ID'];
                        $arFields['NAME'] = $ar['NAME'];
                        if ((int)$months > 0) {
                            $arFields['QUANTITY'] = 1;
                            $arFields['PRICE'] = $ar['CATALOG_PRICE_' . $base_price_id] * $months;
                        } else {
                            $arFields['PRICE'] = $ar['CATALOG_PRICE_' . $base_price_id];
                        }

				//		echo var_dump($arFields);
				//		die;

                        $arFields["PROPS"] = $arProps;
                        $basket_id = CSaleBasket::Add($arFields);
                        $arDebug['default'][] = $arFields;
                        if ($ex = $APPLICATION->GetException())
                            $arDebug['errors'][] = $ex->GetString();
                    }
                }
                $arFields = array();
            }
            break;
    }
	
	
	
    // add advance services
    if (($_REQUEST['type'] == 'solution' || $_REQUEST['type'] == 'configurator') && is_array($_REQUEST['services'])) {
        // add to basket advance services
        $arServices = $_REQUEST['services'];
        $IBLOCK_ID = $arParams['IBLOCK_ID'];

        // services sections
        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID);
        $db = CIBlockSection::GetList($arOrder, $arFilter, false, array('ID', 'NAME', 'CODE', 'DESCRIPTION'));
        while ($ar = $db->Fetch()) {
            $arSections[] = $ar;
        }

        Loader::includeModule('iblock');
        $res = CIBlockElement::GetList(array(), array('ID' => $arServices), false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_PRICE', 'IBLOCK_EXTERNAL_ID'));
        while ($arService = $res->Fetch()) {

			$prop_service = array(
				'NAME' => 'unique_id',
				'VALUE' => $random_string,
				'CODE' => 'unique_id',
				'SORT' => 1,
			);	
			$prop_service2 = array(
				'NAME' => 'is_service',
				'VALUE' => true,
				'CODE' => 'is_service',
				'SORT' => 2,
			);	

            $arFieldsService = array(
                'PRICE' => $arService['PROPERTY_PRICE_VALUE'] * $months,
                'NAME' => $arService['NAME'],
                'PRODUCT_ID' => $arService['ID'],
                'PROPS' => array($arMonthsProps,$prop_service,$prop_service2),
                'CATALOG_XML_ID' => $arService['IBLOCK_EXTERNAL_ID'],
				'QUANTITY' => $_REQUEST['services_q'][$arService['ID']] ?: 1,
            );
//            $arFieldsService['MEASURE_NAME'] = Loc::getMessage('MEASURE_NAME_MONTHS');
            $arFieldsService = array_merge($arFieldsDefault, $arFieldsService);
            $arDebug['services'][$arService['ID']] = $arFieldsService;
            CSaleBasket::Add($arFieldsService);
            if ($ex = $APPLICATION->GetException())
                $arDebug['service'][$arService['ID']] = $ex->GetString();
        }
    }
    if ((int)$product_id > 0) {
        $arFields['PRODUCT_ID'] = $product_id;
        Loader::includeModule('iblock');
        $res = CIBlockElement::GetByID($product_id);
        $ar = $res->Fetch();
        $arFields['CATALOG_XML_ID'] = $ar['IBLOCK_EXTERNAL_ID'];
    }


    if (!empty($arFields)) {
        CSaleBasket::Add($arFields);
    }
	
//	echo var_dump($_REQUEST).'<br><br>';
//	echo var_dump($arFields);
//	die;

    $arDebug['field_solution'] = $arFields;
    if ($ex = $APPLICATION->GetException())
        $arDebug['solution'] = $ex->GetString();
    $arDebug['request'] = $_REQUEST;
//    echo '<pre>';
//    var_dump($arDebug);
//    die();
}

if ($_REQUEST['isAjax'] != 'Y') {
    $back_url = isset($_REQUEST['back_url']) && strlen($_REQUEST['back_url']) > 0 ? $_REQUEST['back_url'] : '/personal/cart/';
    $APPLICATION->RestartBuffer();

    LocalRedirect($back_url);
}
?>
