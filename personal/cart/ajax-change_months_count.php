<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;

define("NO_KEEP_STATISTIC", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/cart/ajax-add.php');
global $APPLICATION;

$APPLICATION->RestartBuffer();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');


$basket_id = (int)$_GET['id'];
$new_cnt = (int)$_GET['cnt'];

$arItem = CSaleBasket::GetByID($basket_id);

$props = [];
$db_res = CSaleBasket::GetPropsList(
	array(
			"SORT" => "ASC",
			"NAME" => "ASC"
		),
	array("BASKET_ID" => $basket_id)
);
while($ar_res = $db_res->Fetch())
{		
	if($ar_res['CODE'] == 'MONTHS')
	{
		$old_cnt = (int)$ar_res['VALUE'];
		$ar_res['VALUE'] = $new_cnt;
	}
	if($ar_res['CODE'] == 'unique_id')
	{
		$uid = $ar_res['VALUE'];
	}
	if($ar_res['CODE'] == 'price_1month')
	{
		$price_1month = (int)$ar_res['VALUE'];
	}
	if($ar_res['CODE'] == 'OS')
	{
		$is_vds = true;
	}
	$props[$basket_id][] = $ar_res;
}


if($price_1month && !$is_vds):
	$sum = $price_1month;
	$arResult['DISCOUNTS'] = unserialize(\Bitrix\Main\Config\Option::get('dedicated', 'discounts_values'));

	// Суммы и скидки по периодам
	$arSum = array(
	'1m' => $sum,
	'3m' => round($sum * 3 - $sum * 3 * $arResult['DISCOUNTS']['CALC_3M'] / 100),
	'6m' => round($sum * 6 - $sum * 6 * $arResult['DISCOUNTS']['CALC_6M'] / 100),
	'12m' => round($sum * 12 - $sum * 12 * $arResult['DISCOUNTS']['CALC_12M'] / 100),
	);
	$arDiscount = array(
	'1m' => $sum - $arSum['1m'],
	'3m' => 3 * $sum - $arSum['3m'],
	'6m' => 6 * $sum - $arSum['6m'],
	'12m' => 12 * $sum - $arSum['12m'],
	);
	$arSumInMonth = array(
	'1m' => $arSum['1m'],
	'3m' => round($arSum['3m'] / 3),
	'6m' => round($arSum['6m'] / 6),
	'12m' => round($arSum['12m'] / 12),
	);

endif;

if($uid):

	$ids = [];
	$db_res = CSaleBasket::GetPropsList(
		array(
				"SORT" => "ASC",
				"NAME" => "ASC"
			),
		array("!BASKET_ID" => $basket_id,'CODE'=>'unique_id','VALUE'=>$uid)
	);
	while($ar_res = $db_res->Fetch())
	{		
		//echo var_dump($ar_res);
		if($ar_res['CODE'] == 'MONTHS')
		{
		//	$ar_res['VALUE'] = $new_cnt;
		}
		//$props[$ar_res['BASKET_ID']][] = $ar_res;
		$ids[] = $ar_res['BASKET_ID'];
	}

	$db_res = CSaleBasket::GetPropsList(
		array(
				"SORT" => "ASC",
				"NAME" => "ASC"
			),
		array("BASKET_ID" => $ids)
	);
	while($ar_res = $db_res->Fetch())
	{		
		if($ar_res['CODE'] == 'MONTHS')
		{
			$ar_res['VALUE'] = $new_cnt;
		}
		$props[$ar_res['BASKET_ID']][] = $ar_res;
	}
	
endif;

if($is_vds):

	foreach($props as $k => $v):

		$arFields = array(
			"PROPS" => $v,
		);
		
		$arItem = CSaleBasket::GetByID($k);
		$arFields['PRICE'] = $arItem['PRICE']/$old_cnt;
		$arFields['PRICE'] = $arFields['PRICE'] * $new_cnt;
		
		if (CSaleBasket::Update($k, $arFields)) {
		
		} else {
			echo $APPLICATION->GetException()->GetString();
		}
		
	endforeach;
	
else:

	foreach($props as $k => $v):


		$arFields = array(
			"PROPS" => $v,
			);
	//	echo var_dump($arFields);
		
		if($k == $basket_id)
		{
			$arFields['PRICE'] = $arSum[$new_cnt.'m'];
		}
		else
		{
			$arItem = CSaleBasket::GetByID($k);
			$arFields['PRICE'] = $arItem['PRICE']/$old_cnt;
			$arFields['PRICE'] = $arFields['PRICE'] * $new_cnt;
		}
		
		
		if (CSaleBasket::Update($k, $arFields)) {
		
		} else {
			echo $APPLICATION->GetException()->GetString();
		}
		
		
	endforeach;
	
endif;;
		echo 1;
?>