<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Itin\Depohost\Dedicated;

define("NO_KEEP_STATISTIC", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/cart/ajax-add.php');
global $APPLICATION;

$APPLICATION->RestartBuffer();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');


$basket_id = (int)$_GET['id'];
CSaleBasket::Delete($basket_id);


?>