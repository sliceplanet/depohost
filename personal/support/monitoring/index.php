<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мониторинг оборудования");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
  <div class="container clearfix">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "side",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_TEMPLATE" => ""
    ), false
    );
    ?>

    <div class="content">
      <h1><? $APPLICATION->ShowTitle(false); ?><span>Личный кабинет</span></h1>

      <? $APPLICATION->IncludeComponent(
        "itin:monitoring.settings",
        "",
        Array(
          "IBLOCK_ID" => "",
          'COUNT_CONTACT' => '2'
        ),
        false
      ); ?>
    </div>
  </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>