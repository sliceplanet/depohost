<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Itin\Depohost\Isp;

global $USER, $APPLICATION;
\Bitrix\Main\Data\StaticHtmlFileStorage::deleteRecursive($APPLICATION->GetCurPage());
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();

$username = 'dns' . $arUser['LOGIN'];
\Bitrix\Main\Loader::includeModule('clients');
$userpassword = \Itin\Depohost\Clients::getPassword($arUser['ID']);
if (empty($arUser['LOGIN']) || empty($userpassword))
{
  echo '<div class="content">';
  ShowMessage('Ошибка авторизации (отсутствует логин или пароль). Обратитесь в техподдержку');
  echo '</div> ';
}
else
{
  $isp = new Isp('dns');
  if ($new_password = $isp->auth(array('user' => $username, 'password' => $userpassword)))
  {
    $bAuth = true;
    if (count_chars($new_password) > 0 && !is_bool($new_password))
    {
      $userpassword = $new_password;
    }
  }
  else
  {
    $bAuth = $isp->createDnsUser(array('user' => $username, 'password' => $userpassword));
  }
  if ($bAuth)
  {
        $isp->open(array(
            'func' => 'auth',
            'username' => (string)$username,
            'password' => (string)$userpassword,
        ));
  }
  else
  {
    echo '<div class="content">';
    ShowMessage('Ошибка авторизации. Обратитесь в техподдержку');
    echo '</div> ';
  }
  /*
  $params = '?out=xml&func=auth&username='.$username.'&password='.urlencode($userpassword);
  $url = 'https://dns.depohost.ru/manager/';

  $data = new SimpleXMLElement(file_get_contents($url . $params));

          echo '<br/><br/>Пытаемся авторизоваться:<br/><br/>';
          echo '<pre>'; print_r($userpassword); echo '</pre>';
          echo '<pre>'; print_r($data); echo '</pre>';

  if(!empty($data->auth)) {
          LocalRedirect(str_replace('out=xml','out=html',$url . $params));
  }
  else {
          echo 'Добавляем пользователя потом редиректим';
          $paramsAdmin = '?out=xml&func=auth&username=depohostsys&password=ExjOYXbDvLMrU3Uh';
          $data = new SimpleXMLElement(file_get_contents($url . $paramsAdmin));

          $paramsCreate = array(
              'out' => 'xml',
              'auth' => (string)$data->auth,
              'func' => 'user.edit',
              'sok' => 'yes',
              'name' => (string)$username,
              'passwd' => (string)$userpassword,
              'confirm' => (string)$userpassword,
              'preset' => 'DNS'
          );
          $paramsCreate = http_build_query($paramsCreate);
          $data = new SimpleXMLElement(file_get_contents($url . '?' . $paramsCreate));


          echo '<br/><br/>создаем пользователя:<br/><br/>';
          echo '<pre>'; print_r($userpassword); echo '</pre>';
          echo '<pre>'; print_r($data); echo '</pre>';

          if (!isset($data->ok)) {
                  $paramsCreate = array(
                                  'out' => 'xml',
                                  'auth' => (string)$data->auth,
                                  'func' => 'user.edit',
                                  'sok' => 'yes',
                                  'elid' => (string)$username,
                                  'name' => (string)$username,
                                  'passwd' => (string)$userpassword,
                                  'confirm' => (string)$userpassword,
                                  'ftplimit' => (string)$data->ftplimit,
                                  'disklimit' => (string)$data->disklimit
                  );
                  $paramsCreate = http_build_query($paramsCreate);
                  $data = new SimpleXMLElement(file_get_contents($url . '?' . $paramsCreate));

                  if (!isset($data->ok)) {
                                  $out['fail'] = true;
                  }
          }

          if (isset($data->ok)) {
                  LocalRedirect($url . '?func=auth&username=' . urlencode($username) . '&password=' . urlencode($userpassword));
          }
  }


  echo '</div>'; */
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>