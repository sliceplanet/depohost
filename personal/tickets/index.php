<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Архив запросов");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
  <div class="container clearfix">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "side",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_TEMPLATE" => ""
    ), false
    );
    ?>

    <div class="content">
      <h1><? $APPLICATION->ShowTitle(false); ?><span>Личный кабинет</span></h1>
      <?
      $APPLICATION->IncludeComponent(
        "bitrix:support.ticket.list", "", Array(
        "TICKET_EDIT_TEMPLATE" => "/personal/tickets/new/?ID=#ID#&edit=1",
        "TICKETS_PER_PAGE" => "20",
        "SET_PAGE_TITLE" => "Y",
        "SET_SHOW_USER_FIELD" => array()
      ), false
      );
      ?>
    </div>
  </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>