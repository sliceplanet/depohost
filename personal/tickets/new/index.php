<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Открытые тикеты");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
  <div class="container clearfix">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "side",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_TEMPLATE" => ""
    ), false
    );
    ?>

    <div class="content">
      <h1><? $APPLICATION->ShowTitle(false); ?><span>Личный кабинет</span></h1>
      <? $APPLICATION->IncludeComponent("bitrix:support.ticket", ".default", Array(
        "ID" => $_REQUEST["ID"],
        "TICKET_LIST_URL" => "/personal/tickets/",
        "MESSAGES_PER_PAGE" => "20",  // Количество сообщений на одной странице
        "MESSAGE_MAX_LENGTH" => "70",  // Максимальная длина неразрывной строки
        "MESSAGE_SORT_ORDER" => "asc",  // Направление для сортировки сообщений в обращении
        "SET_PAGE_TITLE" => "Y",  // Устанавливать заголовок страницы
        "SHOW_COUPON_FIELD" => "N",  // Показывать поле ввода купона
      ),
        false
      ); ?>
    </div>
  </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>