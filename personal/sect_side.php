<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?> 
<?
global $USER;
if ((defined('NEED_AUTH') && NEED_AUTH) || isset($_REQUEST['logout_butt']) || !$USER->IsAuthorized()):
else:

\Bitrix\Main\Loader::includeModule('clients');
$client_ob = new Itin\Depohost\Clients($USER->GetID());

/**@var int     ID элемента инфоблока Карточки клиентов*/
$element_id = $client_ob->getElementId();
$arClient = $client_ob->getById($element_id);
$arServices = $client_ob->getDedicatedServers($element_id);
$arResult['CLIENT_INFO'] = array(
    'ABONPLATA' => number_format($arClient['CC_ABONPLATA']['VALUE'],0,',',' '),
);
?>
<aside class="sidebar">
    <div class="menu">
        <div class="menu-item">
            
            <div class="menu-ttl"><i></i>Стоимость ваших услуг</div>
            <a><b><?=$arResult['CLIENT_INFO']['ABONPLATA']?></b> руб. в месяц</a>			
        </div>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"personal", 
	array(
		"ROOT_MENU_TYPE" => "left",
		"MENU_THEME" => "site",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "360000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "sub",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
    </div> <!--END .menu-->
</aside>
<?endif?>
