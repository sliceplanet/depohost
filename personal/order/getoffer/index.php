<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Коммерческое предложение");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/getoffer.css');
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/pages/getoffer-media.css');
?>
  <div class="container news">
    <div class="row">
      <div class="col-xs-12">

        <? $APPLICATION->IncludeComponent("bitrix:form.result.new", "kp", Array(
          "WEB_FORM_ID" => "3", // ID веб-формы
          "IGNORE_CUSTOM_TEMPLATE" => "N", // Игнорировать свой шаблон
          "USE_EXTENDED_ERRORS" => "N", // Использовать расширенный вывод сообщений об ошибках
          "SEF_MODE" => "Y", // Включить поддержку ЧПУ
          "SEF_FOLDER" => "/personal/order/getoffer/", // Каталог ЧПУ (относительно корня сайта)
          "CACHE_TYPE" => "A", // Тип кеширования
          "CACHE_TIME" => "3600", // Время кеширования (сек.)
          "LIST_URL" => "", // Страница со списком результатов
          "EDIT_URL" => "", // Страница редактирования результата
          "SUCCESS_URL" => "done.php", // Страница с сообщением об успешной отправке
          "CHAIN_ITEM_TEXT" => "", // Название дополнительного пункта в навигационной цепочке
          "CHAIN_ITEM_LINK" => "", // Ссылка на дополнительном пункте в навигационной цепочке
        ),
          false
        ); ?>
      </div>
    </div>
  </div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>