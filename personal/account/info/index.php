<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль покупателя");
use Bitrix\Main\Loader;
use Itin\Depohost\Clients;
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
	<div class="container clearfix">
<?
$APPLICATION->IncludeComponent(
  "bitrix:main.include", "", Array(
  "AREA_FILE_SHOW" => "sect",
  "AREA_FILE_SUFFIX" => "side",
  "AREA_FILE_RECURSIVE" => "Y",
  "EDIT_TEMPLATE" => ""
), false
);
?>

<?
Loader::includeModule('clients');
$user_id = intval($USER->GetID());
$client_ob = new Clients($user_id);
$PROFILE_ID = $client_ob->getProfileId(array('user_id'=>$user_id));
//CModule::IncludeModule("sale");
//$dbUserProps = CSaleOrderUserProps::GetList(
//		array("DATE_UPDATE" => "DESC"),
//		array(
//				"USER_ID" => IntVal($GLOBALS["USER"]->GetID()),
//				"PERSON_TYPE_ID" => 2,
//			),
//		false,
//		false,
//		array("ID")
//	);
//$arUserProfile = $dbUserProps->Fetch();
?>
<?$APPLICATION->IncludeComponent("bitrix:sale.personal.profile.detail", "profile", Array(
	"PATH_TO_LIST" => "",
	"PATH_TO_DETAIL" => "",
	"ID" => $PROFILE_ID,
	"USE_AJAX_LOCATIONS" => "N",
	"SET_TITLE" => "N",
	),
	false,array("HIDE_ICONS" => "Y")
);?>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>