<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
  <div class="container clearfix">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "side",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_TEMPLATE" => ""
    ), false
    );
    ?>

    <? $APPLICATION->IncludeComponent(
      "itin:main.profile",
      "depohost",
      Array(
        "SET_TITLE" => "Y"
      )
      , false, array("HIDE_ICONS" => "Y")
    ); ?>
  </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>