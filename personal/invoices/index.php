<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Счета");
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/stl.css');
?>
  <div class="container clearfix">
    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "sect",
      "AREA_FILE_SUFFIX" => "side",
      "AREA_FILE_RECURSIVE" => "Y",
      "EDIT_TEMPLATE" => ""
    ), false
    );
    ?>

    <div class="content">
      <h1><? $APPLICATION->ShowTitle(false); ?><span>Личный кабинет</span></h1>
      <?
      if (array_key_exists('paid', $_REQUEST))
      {
        switch ($_REQUEST['paid'])
        {
          case 'success':
            ShowNote('Платеж успешно совершен');
            break;
          case 'fail':
          default:
            ShowError('Оплата не прошла. Возможно, Вы отказались от платежа или возникла другая ошибка');
            break;
        }
      }
      ?>
      <? $APPLICATION->IncludeComponent(
        "itin:clients.invoices.list",
        "",
        Array(
          "IBLOCK_ID" => "",
        ),
        false
      ); ?>
    </div>
  </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>