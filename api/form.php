<?php
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', "Y");
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
try {
  \Bitrix\Main\Loader::includeModule('form');
  $WEB_FORM_ID = $_REQUEST['WEB_FORM_ID'];
  $WEB_FORM_ID = CForm::GetDataByID($WEB_FORM_ID, $arForm, $arQuestions, $arAnswers, $arDropDown, $arMultiSelect);
  $arFields = $arEventFields = array();

  foreach ($arQuestions as $arQuestion) {

    $answer = reset($arAnswers[$arQuestion["SID"]]);
    $fname = "form_".$answer["FIELD_TYPE"]."_" . $answer["ID"];

    if (array_key_exists($fname, $_REQUEST)) {
      $value = htmlspecialcharsbx(trim($_REQUEST[$fname]));
      $arEventFields[$fname] = $value;
    }
    elseif (array_key_exists($arQuestion["VARNAME"], $_REQUEST)) {
      $value = htmlspecialcharsbx(trim($_REQUEST[$arQuestion["VARNAME"]]));
      $arEventFields[$fname] = $value;
    }
    elseif (array_key_exists($arQuestion["SID"], $_FILES)) {
      $value = $_FILES[$arQuestion["SID"]];
    }
    elseif (array_key_exists($arQuestion["VARNAME"], $_FILES)) {
      $value = $_FILES[$arQuestion["VARNAME"]];
      $arEventFields[$fname] = $value;
    }
    elseif (array_key_exists($fname, $_FILES)) {
      $value = $_FILES[$fname];
      $arEventFields[$fname] = $value;
    }
    $arFields[$fname] = $value;
  }
  $arEventFields = array_change_key_case($arEventFields,CASE_UPPER);

  $captchaError = false;

  if($arForm["USE_CAPTCHA"] == "Y"){
    include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
    $captcha_code = $_POST["captcha_sid"];
    $captcha_word = $_POST["captcha_word"];
    $cpt = new CCaptcha();
    $captchaPass = COption::GetOptionString("main", "captcha_password", "");
    if (
      strlen($captcha_word) > 0 && strlen($captcha_code) > 0
    ){
      if(!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass)){
        $captchaError = true;
        throw new \Exception("Ошибка captcha");
      }

    }else{
      $captchaError = true;
      throw new \Exception("Ошибка captcha");
    }
  }


  if (!empty($arFields) && !$captchaError) {
    $id = \CFormResult::Add($WEB_FORM_ID, $arFields, 'N', $GLOBALS['USER']->GetID());

    if ($id > 0) {
      if (\CFormResult::Mail($id)) {
        $arResult = array(
          'MESS' => 'Ваше сообщение было отправлено успешно. Спасибо.'
        );
      } else {
        global $strError;
        if (strlen($strError)) {
          throw new \Exception($strError);
        }
        throw new \Exception('Ваш запрос не отправлен. Попробуйте позже');
      }
    }
  }

} catch (\Exception $e) {
  $arResult = array(
    'ERROR' => $e->getMessage()
  );
}
echo json_encode($arResult);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');