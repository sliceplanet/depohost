<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Свидетельство о государственной регистрации нашей компании ООО \"Депо Телеком\"");
$APPLICATION->SetPageProperty("keywords", "Свидетельство о государственной регистрации ООО \"Депо Телеком\"");
$APPLICATION->SetPageProperty("title", "Свидетельство о государственной регистрации ООО \"Депо Телеком\" ");
$APPLICATION->SetTitle("Лицензии нашей компании");
?>

<section class="page-section how-ssl-works-section dns-hosting-text">
	
	<div class="container">
	
		<h1 class="page-section__h"><? $APPLICATION->ShowTitle(false) ?></h1>
		<p class="page-section__desc">На данной странице представлены лицензии, свидетельства, сертификаты, дающие представление об открытости нашей
        компании, о подлинности используемого лицензионного программного обеспечения. </p>
     
	
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__h">Свидетельство о государственной регистрации ООО "Депо Телеком"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
					<a href="/doc/svidetelstvo.jpg" title="Свидетельство о государственной регистрации" target="_blank"
            rel="nofollow"><img src="/doc/svidetelstvo.jpg" alt="Свидетельство о государственной регистрации"
                                width="125" height="172"></a>
				</div>
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h">Лицензия на предоставление телематических услуг связи "№159574"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
				<a href="/doc/telematika_nwe.jpg" title="Лицензия на Телематические услуги связи " target="_blank"
           rel="nofollow"><img src="/doc/telematika_nwe.jpg" alt="Лицензия на предоставление телематических услуг связи"
                               width="125" height="172"></a>
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Лицензия на оказания услуг связи по передачи данных, за исключением услуг связи
        по передачи данных для целей передачи голосовой информации "№152501"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
				<a href="/doc/licdata.jpg" title="Лицензия на оказания услуг связи по передачи данных" target="_blank"
            rel="nofollow"><img src="/doc/licdata.jpg" alt="Лицензия на оказания услуг связи по передачи данных"
                                width="125" height="172"></a>
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Сертификат "Сертификат официального партнёра ЗАО "ИСПсистем"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
				<a href="/doc/isp.jpg" title="Сертификат ISP" target="_blank" rel="nofollow"><img src="/doc/isp.jpg"
                                                                                           alt="Сертификат ISP"
                                                                                           width="125" height="91"></a>
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Сертификат "Сертификат официального партнёра "1С"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
				<a href="/doc/1C_sertifikat.JPG" title="Сертификат 1С партнера" target="_blank" rel="nofollow"><img
              src="/doc/1C_sertifikat.JPG" alt="Сертификат 1С партнера" width="125" height="177"></a>
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Свидетельство "Свидетельство торговой марки Artextelecom"</div>
				<div class="hsw-block__content">
				</div>
				<div class="hsw-block_img">
			<a href="/doc/artextelecom.jpg" title="Свидетельство регистрации товарного знака" target="_blank"
            rel="nofollow"><img src="/doc/artextelecom.jpg" alt="Свидетельство регистрации товарного знака" width="125"
                                height="172"></a>
				</div>
			</div>
		</div>
	</div>
</section>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>