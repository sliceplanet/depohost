<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "Аренда сервера на Linux. ArtexTelecom — предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("title", "Аренда виртуального (VDS) сервера Linux в Москве | Аренда VDS Linux сервера");
$APPLICATION->SetTitle("Аренда сервера Linux");
?>
<div class="container">
  
<p>Ищете надежный облачный сервер для Linux? Компания ArtexTelecom предлагает выгодные условия для размещения данных вашего сайта. У нас вы можете взять в аренду VDS-сервер на Linux оптимальной конфигурации с учетом типа интернет-ресурса и его загруженности. Мы гарантируем бесперебойную работу и круглосуточную техподдержку. </p>

</div>
<div class="vds relative-wrapper">
  <div class="container block_text">
    <h2 class="h2">Выберите готовое решение</h2>
    Готовые решения будут вам предоставлены в течении 15 минут после оплаты.
  </div>
  <div class="vds-solution-wrapper">
    <div class="bg-left vissible-lg"></div>
    <div class="bg-right vissible-lg"></div>
    <div class="container">
      <? $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "vds-turnkey",
        array(
          "ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
          "CACHE_GROUPS" => "Y",  // Учитывать права доступа
          "CACHE_TIME" => "36000000",  // Время кеширования (сек.)
          "CACHE_TYPE" => "A",  // Тип кеширования
          "COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
          "IBLOCK_ID" => "63",  // Инфоблок
          "IBLOCK_TYPE" => "xmlcatalog",  // Тип инфоблока
          "SECTION_CODE" => "",  // Код раздела
          "SECTION_FIELDS" => "",  // Поля разделов
          "SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
          "SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
          "SECTION_USER_FIELDS" => "",  // Свойства разделов
          "SHOW_PARENT_NAME" => "Y",
          "TOP_DEPTH" => "2",  // Максимальная отображаемая глубина разделов
          "VIEW_MODE" => "LINE"
        )
      ); ?>
    </div>
  </div>
</div>
<div class="container">
  
<p align="center"><img title="Аренда сервера на Linux" src="/images/pages/arenda-servera-na-linux/1.png" alt="Аренда сервера на Linux"></p>

<h2>Преимущества VDS-серверов для Linux </h2>

<ol class="normal_ol">
  <li>Расширенные ресурсы, соответствующие выбранной конфигурации.</li>
  <li>Собственный выделенный IP-адрес </li>
  <li>Ежедневное резервное копирование.</li>
  <li>Высокоскоростной бесперебойный доступ к Интернету.</li>
  <li>Совместимость с различными операционными системами и программным обеспечением. </li>
  <li>Полный root-доступ.</li>
  <li>Возможность переноса данных на другое оборудование.</li>
  <li>Высокая отказоустойчивость.</li>
  <li>Возможность быстрого увеличения и уменьшения производительности, подбора оптимальных параметров.</li>
  <li>Полная изоляция от других пользователей - выделенный сервер закрепляется только за вашим сайтом.</li>
</ol>

<h2>6 причин для сотрудничества с ArtexTelecom</h2>

<div class="advantage-img-wrap">
  <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item">
      <div class="advantage__item_icon calc"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Для постоянных клиентов предусмотрены скидки при внесении предоплаты за 3–6–12 месяцев сотрудничества.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">При аренде сервера Linux у нас вы получаете бесплатный перенос данных, настройку и подробные консультации по работе.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon mouse"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Мы дарим бесплатные 15 дней размещения вашего сайта на наших серверах после зачисления аванса за пользование услугой. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Вы вносите только абонентскую плату – мы не начисляем никаких дополнительных комиссий. В исключительных случаях возможна рассрочка платежа в течение одного месяца.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon day"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Если вы обнаружили ошибку или не знаете, как работать в панели администратора, обратитесь в техподдержку. Наш специалист сразу же приступит к настройке.
        </div>
      </div>
    </li>
    
    <li class="advantage__item">
      <div class="advantage__item_icon path"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> При обращении к нам вы получаете бесплатные интернет-трафик и 100 Гб для хранения резервных копий на внутреннем FTP-сервере.
        </div>
      </div>
    </li>
  </ul>
</div>

<p>Чтобы взять в аренду виртуальные серверы на Linux или купить дополнительные опции, свяжитесь с нашим сотрудником с помощью формы обратной связи на странице «Контакты». Или позвоните по круглосуточному номеру 8 (495) 797-8-500. Мы поможем подобрать идеальный сервер по мощности, производительности и цене.</p>
<section class="services dedic-section">
  <div class="subtitle subtitle-custom">
      УСЛУГИ ДЛЯ ВАС
  </div>
  <ul class="services-list-custom">
    <li><a href="/ssl-sertifikat/">SSL сертификаты</a></li>
    <li><a href="/hosting/">Хостинг</a></li>
    <li><a href="/1c-server/">Аренда сервера для 1С</a></li>
    <li><a href="/domain/">Регистрация домена</a></li>
  </ul>
</section>
</div>
<div class="page-blue-block">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда выделенного сервера'
      )
    ); ?>
  </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>