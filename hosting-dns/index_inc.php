<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
  die();
} ?>

<section class="page-section how-ssl-works-section dns-hosting-text">
	
	<div class="container">
	
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__content">
					    <p>
					  Хостинг DNS очень удобен в организации DNS записей домена, а его реализация происходит в <a
						  href="https://www.depohost.ru/ispmanager/" title="Ссылка: https://www.depohost.ru/ispmanager/">многофункциональной
						панели управления</a>. Панель позволяет редактировать и создавать записи в связанных с ней сайтами, приложениями
					  или доменами.<br>
					</p>
					    <p>
      DNS-поддержка – это возможность качественно управлять вашим доменом, а также редактировать master-запись с
      синхронизацией на других DNS серверах. Соответственно, можно быстро внести изменения в любой <a href="/domain/">домен</a>.
    </p>
				</div>
				<div class="hsw-block_img">
					<img src="/bitrix/images/news/123.jpg">
				</div>
			</div>
			
			<div class="hsw-block">
				<div class="hsw-block__h">Хостинг DNS от нашей компании – это:</div>
				<div class="hsw-block__content">
					<ul class="ul50p fw500">
      <li>максимальное удобство при самостоятельном управлении DNS записями собственных доменов и субдоменов;</li>
      <li>невысокая цена хостинга;</li>
      <li>возможность поддержки DNS master и slave зоны одного или нескольких доменов;</li>
      <li>неограниченное количество ресурсных записей доменов.</li>
					</ul>
    <p>
      <a href="https://www.depohost.ru/">Мы предлагаем</a>
      поддержку большинства DNS зон. Такая поддержка заинтересует клиентов с высокими требованиями к скорости сервиса.
      Это могут быть корпоративные заказчики, гос структуры, крупные веб-проекты и операторы связи.
    </p>
    <p>
      Используемая нами технология IP-Anycast позволяет передавать сообщения по самому короткому веб-маршруту,
      расположенному к ближним DNS узлам. Кроме того, данная технология обеспечивает взаимную замену DNS-узлов.
    </p>
    <p>
      Наши услуги, а именно, хостинг DNS – это устойчивость к отказу и надежность, это наличие DNS-узлов как в России,
      так и за пределами страны. Мы защищаем наших клиентов от перебоев в сети, от сетевых атак и других неприятностей в
      конкретной клиентской зоне DNS.
    </p>
    <p>
      Наши услуги – это отличная связная способность DNS с отечественными и зарубежными операторами, а также доступ к
      Internet Exchange для бесперебойного обмена трафиком для российских и зарубежных точек доступа. Кроме того, у нас
      присутствует открытая серверная поддержка «BIND», мониторинг сервиса, <a
          href="https://www.depohost.ru/administrirovanie/" title="Ссылка: https://www.depohost.ru/administrirovanie/">круглосуточная
        тех поддержка</a> и гарантия доступности сервера в любое время.
    </p>
				</div>
				
			</div>
			
		</div>
		
	</div>
		
</section> 


<section class="page-section anyq-section anyq-black">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	


<?global $relatedNews; 
$relatedNews = [ "ID" => [13092, 13090, 13296, 13294] ];?>
<div class="articles-block  main-new__wrap">
  <div class="container">
    <div class="main-new__title">
      Статьи по теме:
    </div>
    <? $APPLICATION->IncludeComponent(
      "bitrix:news.list",
      "related-articles",
      Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "DETAIL_URL" => "/news/#ELEMENT_CODE#/",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("", ""),
        "FILTER_NAME" => "relatedNews",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "4",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
      ),
      $component
    ); ?>
  </div>
</div>
