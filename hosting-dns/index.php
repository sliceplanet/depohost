<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "хостинг DNS, поддержка ДНС, поддержка доменных зон");
$APPLICATION->SetPageProperty("description", "Хостинг DNS предлагается компанией ArtexTelecom. Служба поддержки нужна если у вас под управлением есть один или несколько доменов различного уровня.");
$APPLICATION->SetPageProperty("keywords", "хостинг dns, поддержка dns, техническая служба dns, центр поддержки dns");
$APPLICATION->SetPageProperty("title", "DNS хостинг, техническая поддержка ДНС от ArtexTelecom");
$APPLICATION->SetTitle("Хостинг DNS, служба поддержки доменных имен");
?>
<section class="page-section whyssl-section why-vvds">
		
	<div class="page-section__h">ДЛЯ ЧЕГО НУЖЕН DNS ХОСТИНГ</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds1.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Низкая стоимость</div>
					<p>Наши цены ниже чем у наших конкурентов за те же услуги</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds2.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Поддержка ДНС</div>
					<p>Поддержка DNS master и slave зоны вашего домена или доменов</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds3.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Неограниченные ресурсные записи</div>
					<p>Во всех тарифах неограниченное количество ресурсных записей ваших доменов</p>
				</div>
			</div>			
		</div>
		
	</div>
	
</section>

<section class="page-section section-graybg">

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"hosting_dns2020",
	Array(
		"IBLOCK_TYPE" => "xmlcatalog",
		"IBLOCK_ID" => "26",
		"HIDE_NOT_AVAILABLE" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/hosting-dns/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "N",
		"FILTER_VIEW_MODE" => "HORIZONTAL",
		"USE_REVIEW" => "N",
		"USE_COMPARE" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(0=>"",1=>"",),
		"COMPARE_PROPERTY_CODE" => array(0=>"CMN_MAX",1=>"CMN_RES",2=>"",),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"PRICE_CODE" => array(0=>"Розничная",),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"SHOW_TOP_ELEMENTS" => "Y",
		"TOP_ELEMENT_COUNT" => "30",
		"TOP_LINE_ELEMENT_COUNT" => "1",
		"TOP_ELEMENT_SORT_FIELD" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "desc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(0=>"CMN_MAX",1=>"CMN_RES",2=>"",),
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "1",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(0=>"CMN_MAX",1=>"CMN_RES",2=>"",),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(0=>"CMN_MAX",1=>"CMN_RES",2=>"",),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "",
		"USE_ALSO_BUY" => "N",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Заказать",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"SEF_URL_TEMPLATES" => Array(
			"sections" => "",
			"section" => "",
			"element" => "",
			"compare" => "compare.php?action=#ACTION_CODE#"
		),
		"VARIABLE_ALIASES" => Array(
			"sections" => Array(),
			"section" => Array(),
			"element" => Array(),
			"compare" => Array(
				"ACTION_CODE" => "action"
			),
		)
	)
);?>

</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>