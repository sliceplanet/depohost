
	<section class="page-section connect-us-section">
	
		<div class="container">
		
			<div class="row">
			
				<div class="col-md-6">
					<p>
					Свяжитесь с нами любым удобным для вас способом: 
					</p>
				</div>
				<div class="col-md-6">
					<ol>
						<li>В режиме онлайн консультации;</li>
						<li>Оставив письмо с запросом в разделе <a href="#">Контакты</a>;</li>
						<li>Позвонив по телефону:  <b>+7 (495) 797-8-500</b>	</li>			
					</ol>
				</div>
			
			</div>
			
		</div>
	
	</section>