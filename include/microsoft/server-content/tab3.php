<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h2>Поддержка и консультация клиентов</h2>
<p><hr/>
<p>Мы целиком и полностью поддерживаем всех наших клиентов, арендовавших Microsoft ПО. Своевременно закачиваются и устанавливаются обновления, делаются резервные копии всех данных, есть возможность автоматического аварийного восстановления и многое другое. Каждый наш клиент может рассчитывать на круглосуточную техподдержку и бесплатные консультации по программному обеспечению Microsoft, которое он арендует у нашей компании.
    <br />
    И даже если вы никогда ранее на работали с программами в аренду, то мы сможем в течении краткого срока все рассказать и объяснить, научив эффективной и продуктивной работе в данной среде.</p>
