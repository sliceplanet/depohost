<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

if(!function_exists('formatPhone')) {
  function formatPhone($phone)
  {
    $phone = preg_replace('/\D/i', '', $phone);
    $phone = substr($phone,0,11);
    $phone = preg_replace('/^((8|7))?([\d]{3})([\d]{3})([\d]{2})([\d]{2})$/i',
      '+7(${3})${4}-${5}-${6}', $phone);

    return $phone;
  }
}
if($GLOBALS['USER']->IsAuthorized() && \Bitrix\Main\Loader::includeModule('clients')) {


  $client = new \Itin\Depohost\Clients($GLOBALS['USER']->GetID());
  $arProfile = $client->getProfile();
  $arUserProfile = [
    'email' => $arProfile['EMAIL']['VALUE'],
    'name' => $arProfile['NAME']['VALUE'],
    'phone' => formatPhone($arProfile['PHONE']['VALUE']),
  ];
  ?>
  <script>
    window.jivo_onLoadCallback = function() {
      var arProfile = <?=CUtil::PhpToJSObject($arUserProfile)?>;
      jivo_api.setContactInfo(arProfile);
    };
  </script>
  <?
}
?>