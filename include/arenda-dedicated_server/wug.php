
<section class="page-section wug-section">
	
	<h2 class="page-section__h">Что вы получаете</h2>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Возможность работы с любыми нагрузками</div>
					<p>Выделенные серверы используются там, где ресурсов VPS/VDS недостаточно. При этом ваши возможности по масштабированию еще выше: по вашему заказу мы можем расширить долговременную и оперативную память, увеличить ширину канала и так далее. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_4.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Доступ 24/7/365</div>
					<p>Вы можете управлять вашим сервером со стационарного компьютера, ноутбука или смартфона. Вы можете организовать доступ для любого числа сотрудников и предоставить им возможность работы с общими файлами и программами. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_7.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатный безлимитный трафик</div>
					<p>Мы не ограничиваем входящий и исходящий трафик на все время работы. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_2.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатное пространство для резервных копий</div>
					<p>Каждый клиент, арендующий dedicated-сервер получает 100 Gb для создания бэкапа данных. По согласованию мы также создаем копии информации по согласованному графику.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_5.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатную настройку и установку оборудования</div>
					<p>Мы начинаем готовить сервер к работе сразу же после получения оплаты. По заказу наши сотрудники также установят все необходимое программное обеспечение, включая ОС. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_8.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Хранение и защиту ваших данных согласно ФЗ 152</div>
					<p>Мы обеспечиваем защиту от несанкционированного доступа, шифрование данных и 4-й класс защиты оборудования. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_3.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущество SAS и SSD</div>
					<p>Вы можете выбирать устройства для хранения данных. Если вам необходимо увеличить быстродействие, мы подключим SAS и SSD диски и перенесем информацию на них.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_6.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Лицензионную версию операционной системы</div>
					<p>Если вы арендуете dedicated-сервер с ОС Windows, мы бесплатно предоставляем лицензию на Windows Server 2008/2012/2016. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_9.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Снижение затрат</div>
					<p>Вы платите только за аренду сервера и не несете расходов, связанных с его обслуживанием и ремонтом. </p>
				</div>
			</div>
		</div>

		<div class="bordered-text">Мы гарантируем каждому клиенту аптайм не менее 99,9%. Даже если нам необходимо провести замену и ремонт оборудования, мы используем подменные устройства для обеспечения бесперебойной работы. </div>
		
	</div>
	
</section>