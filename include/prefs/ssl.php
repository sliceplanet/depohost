<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<ul>
	<li> <img alt="Быстрая покупка за 15 минут" src="/images/blue/1_204.png" class="icos">
	<div class="tabs-name">
		 Быстрая покупка за 15 минут
		<div class="teaser">
			 Оформите заказ, произведите оплату и получите зарегистрированный сертификат за 15 минут
		</div>
	</div>
 </li>
	<li> <img alt="Область применений" src="/images/blue/1_214.png" class="icos">
	<div class="tabs-name">
		 Область применений
		<div class="teaser">
			 Веб сайт, интернет магазин, интернет торговля, бизнес приложения, почта, сервера приложений с использованием протокола RDP и многое другое
		</div>
	</div>
 </li>
	<li> <img src="/images/blue/1_222.png" class="icos" alt="Безопасность">
	<div class="tabs-name">
		 Безопасность
		<div class="teaser">
			 SSL сертификат гарантирует вам безопасность передачи данных и стойкость шифрования в общественных и открытых сетях, обеспечивает защиту обмена данными между клиент/сервером
		</div>
	</div>
 </li>
</ul>
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<li> <img alt="Преимущество использования" src="/images/blue/1_13.png" class="icos">
	<div class="tabs-name">
		 Преимущество использования
		<div class="teaser">
			 Защита персональных данных, стойкость шифрования, легкость в использовании и настройке
		</div>
	</div>
 </li>
	<li> <img alt="Задачи которые призван решать SSL" src="/images/blue/1_211.png" class="icos">
	<div class="tabs-name">
		 Задачи, которые призван решать SSL
		<div class="teaser">
			 В первую очередь сохранность персональных данных, конфиденциальность и гарантию целостности, а так же шифрование
		</div>
	</div>
 </li>
	<li> <img alt="Как выбрать SSL сертификат" src="/images/blue/1_219.png" class="icos">
	<div class="tabs-name">
		 Как выбрать SSL сертификат
		<div class="teaser">
			 Выберите тип сертификата из списка, используя удобный фильтр, введите имя домена и обязательный e-mail (он должен соответствовать предлагаемому шаблону), нажмите Заказать
		</div>
	</div>
 </li>
</ul>