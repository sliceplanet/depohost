<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_127.png" alt="Бесплатная поддержка и консультация">
    <div class="tabs-name"> Бесплатная поддержка и консультация 
      <div class="teaser">Проконсультируем Вас по любым вопросам связанным с выбором ПО, поможем сделать правильный выбор </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_03.png" alt="Бесплатная установка и настройка">
    <div class="tabs-name"> Бесплатная установка и настройка 
      <div class="teaser">Бесплатно установим и настроим арендованный Windows сервер или любое другое ПО Microsoft </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_147.png" alt="Преимущество использования">
    <div class="tabs-name"> Преимущество использования 
      <div class="teaser">Экономия, большой выбор, доступ к новым приложениям в течении часа при наличии заключенного договора </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> 
    <img class="icos" src="/images/blue/1_130.png" alt="Большой выбор">
    <div class="tabs-name"> Большой выбор 
      <div class="teaser"> Для заказа аренды приложений Microsoft доступны почти вся линейка продуктов компании </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_82.png" alt="Большие возможности">
    <div class="tabs-name"> Большие возможности 
      <div class="teaser">Вы можете попробовать арендовать любой продукт Microsoft прежде чем его приобрести</div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_144.png" alt="Выгодная экономия">
    <div class="tabs-name"> Выгодная экономия 
      <div class="teaser"> Аренда Windows сервера выгодно отличается от лицензии приобретенной в розницу, экономьте заказывайте аренду сервера Windows </div>
     </div>
   </li>
 </ul>
