<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_95.png" alt="Удобство">
    <div class="tabs-name"> Удобство 
      <div class="teaser">Самостоятельно управляйте DNS записями ваши доменов и субдоменов </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_100.png" alt="Низкая стоимость">
    <div class="tabs-name"> Низкая стоимость 
      <div class="teaser">Наши цены ниже чем у наших конкурентов за те же услуги</div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> 
    <img class="icos" src="/images/blue/1_93.png" alt="Поддержка ДНС">
    <div class="tabs-name"> Поддержка ДНС 
      <div class="teaser">Поддержка DNS master и slave зоны вашего домена или доменов </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_102.png" alt="Неограниченные ресурсные записи">
    <div class="tabs-name"> Неограниченные ресурсные записи 
      <div class="teaser">Во всех тарифах неограниченное количество ресурсных записей ваших доменов  </div>
     </div>
   </li>
 </ul>
