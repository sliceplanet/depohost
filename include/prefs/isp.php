<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_184.png" alt="Удобство использования">
    <div class="tabs-name"> Удобство использования 
      <div class="teaser"> Защищенный доступ по протоколу HTTPS, поддержка всех браузеров, дружелюбный интерфейс </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_13.png" alt="Преимущество">
    <div class="tabs-name"> Преимущество 
      <div class="teaser">Интуитивно понятный интерфейс, простота использования, облегчает администрирование при минимальных знаниях </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_82.png" alt="Возможности">
    <div class="tabs-name"> Возможности 
      <div class="teaser">Настройка Веб сервера и пользователей, резервное копирование, установка дополнительных компонентов мониторинг нагрузки и многое другое</div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  
  <li> 
    <img class="icos" src="/images/blue/1_127.png" alt="Бесплатная поддержка и консультация">
    <div class="tabs-name"> Бесплатная поддержка и консультация 
      <div class="teaser">Оказываем бесплатную поддержку и консультируем наших клиентов как пользоваться данным продуктом </div>
     </div>
   </li>



  <li> 
    <img class="icos" src="/images/blue/1_188.png" alt="Кросплатформенность">
    <div class="tabs-name"> Кросплатформенность 
      <div class="teaser"> Панель может быть установлена на любую OS Windows или Linux. Рекомендуем использовать CentOS или Debian </div>
     </div>
   </li>
 
 
 
  <li> 
    <img class="icos" src="/images/blue/1_03.png" alt="Бесплатная установка и настройка">
    <div class="tabs-name"> Бесплатная установка и настройка 
      <div class="teaser">Все панели управления устанавливаем и настраиваем бесплатно </div>
     </div>
   </li>
 </ul>
