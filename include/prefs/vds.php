<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
 
<ul> 
  <li> <img class="icos" src="/images/blue/1_03.png" alt="Бесплатный трафик"  />
    <div class="tabs-name"> Бесплатный трафик 
      <div class="teaser">Арендуя виртуальный сервер, вы получаете бесплатный безлимитный трафик </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_18.png" alt="Бесплатная настройка и консультации"  />
    <div class="tabs-name"> Бесплатная настройка и консультации 
      <div class="teaser">Каждый виртуальный сервер уже настроен, панель ISPmanager в подарок бесплатно! </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_63.png" alt="Высокопроизводительные СХД"  />
    <div class="tabs-name"> Высокопроизводительные СХД 
      <div class="teaser">Для виртуализации и облачных решений мы используем высокопроизводительные СХД, HP, Infortrend </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_68.png" alt="Бесплатный тестовый доступ на 14 дней"  />
    <div class="tabs-name"> Дополнительно 15 дней бесплатно 
      <div class="teaser">После оплаты вы получаете дополнительно 15 дней бесплатно к выбранному виртуальному серверу <a href="/rules-test-vds.php" >Подробнее</a></div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> <img class="icos" src="/images/blue/1_47.png" alt="Аппаратная виртуализация KVM и Huper-V"  />
    <div class="tabs-name"> Аппаратная виртуализация KVM и Huper-V 
      <div class="teaser">Аппаратная виртуализация VDS сервера гарантирует честное распределение ресурсов </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_55.png" alt="Большой выбор OS Windows и Linux"  />
    <div class="tabs-name"> Большой выбор OS Windows и Linux 
      <div class="teaser">Взяв в аренду виртуальный сервер мы можем установить Вам любую операционную систему </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_60.png" alt="Круглосуточная поддержка 24/7"  />
    <div class="tabs-name"> Круглосуточная поддержка 24/7 
      <div class="teaser">Пользуясь нашими услугами вы всегда можете рассчитывать на круглосуточную техническую поддержку </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_73.png" alt="Вы приобретаете услуги из первых рук"  />
    <div class="tabs-name"> Вы приобретаете услуги из первых рук 
      <div class="teaser">Важно знать! Мы как российский оператор предоставляем услуги из первых рук, мы отвечаем за свое качество!</div>
     </div>
   </li>
 </ul>