<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> <img class="icos" src="/images/blue/1_03.png" alt="Бесплатная настройка и установка"  />
    <div class="tabs-name"> Бесплатная настройка и установка 
      <div class="teaser">Выделенный сервер вы получаете с установленной ОС, настроенными приложениями и джентльменским набором для Веб </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_18.png" alt="Бесплатный безлимитный трафик"  />
    <div class="tabs-name"> Бесплатный безлимитный трафик 
      <div class="teaser"> Абсолютно бесплатный трафик, без географического разделения и ограничений, без скрытых расчетов и платежей</div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_24.png" alt="Бесплатный SMS и EMAIL мониторинг"  />
    <div class="tabs-name"> Бесплатный SMS и EMAIL мониторинг 
      <div class="teaser">Вы можете получать информацию о доступности вашего оборудования в виде коротких sms или e-mail сообщений </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_32.png" alt="Бесплатное администрирование"  />
    <div class="tabs-name"> Бесплатное администрирование 
      <div class="teaser">Базовые настройки, несложные изменения, переустановка ОС и другие задачи продолжительностью не более 15 минут - выполняем бесплатно! </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"> 
  <li> <img class="icos" src="/images/blue/1_07.png" alt="Работа с большой нагрузкой"  />
    <div class="tabs-name"> Работа с большой нагрузкой 
      <div class="teaser">Выделенный сервер лучше всего подходит для высоко нагруженных проектов, сложных вычислений, работа с большим объемом данных</div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_13.png" alt="Преимущество SAS и SSD"  />
    <div class="tabs-name"> Преимущество SAS и SSD 
      <div class="teaser">Для получения высокого быстродействия используйте диски SAS, для максимальной отдачи дисковой подсистемы выбирайте SSD </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_27.png" alt="Канал передачи данных 100 Mbit.s"  />
    <div class="tabs-name"> Канал передачи данных 100 Mbit.s 
      <div class="teaser"> Каждый сервер подключен к физическому порту 100 Мбит.с. Смотрите подробней <a href="/trafik/" >стоимость трафика</a></div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_37.png" alt="Быстрый старт"  />
    <div class="tabs-name"> Быстрый старт 
      <div class="teaser"> Доступ к выделенному серверу вы получаете за 30 минут после оплаты - раздел готовые решения. Конфигурация из калькулятора в течении 1-ого рабочего дня</div>
     </div>
   </li>
 </ul>
