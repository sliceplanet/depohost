<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<ul class="col-sm-6">
	<li> <img alt="СНИЖЕНИЕ ВАШИХ ЗАТРАТ" src="/images/blue/1_123.png" class="icos">
	<div class="tabs-name">

		<div class="teaser">
			 Аренда сервера для 1С это экономия ваших средств не только на покупке, обслуживании и администрировании сервера
		</div>
	</div>
 </li>
	<li> <img alt="Доступ 24/7" src="/images/blue/1_162.png" class="icos">
	<div class="tabs-name">
		 Доступ 24/7
		<div class="teaser">
			 Бесспорное преимущество круглосуточный доступ из любой точки мира, офиса, дома, филиала в поездке или командировки
		</div>
	</div>
 </li>
	<li> <img alt="Бесплатное место для резервное копирование 100 GB" src="/images/blue/1_167.png" class="icos">
	<div class="tabs-name">
		 100 GB места для резервное копирование
		<div class="teaser">
			 Каждому клиенту Бесплатно предоставляется 100GB места для резервного копирования на отдельном FTP сервере на весь срок аренды
		</div>
	</div>
 </li>
	<li> <img alt="Хранение и защита ваших данных ФЗ 152" src="/images/blue/1_151.png" class="icos">
	<div class="tabs-name">
		 Хранение и защита ваших данных ФЗ 152
		<div class="teaser">
			 Защита от несанкционированного доступа, передача данных по шифрованным каналам, 3-я и 4-я категория, 4-й класс защиты
		</div>
	</div>
 </li>
</ul>
<ul class="col-sm-6">
	<li> <img alt="Работа с большой нагрузкой" src="/images/blue/1_07.png" class="icos">
	<div class="tabs-name">
		 Работа с большой нагрузкой
		<div class="teaser">
			 Все предложенные конфигурации протестированы, мы знаем как правильно рассчитать нагрузку на сервер, отвечающую "цена/качество"
		</div>
	</div>
 </li>
	<li> <img alt="БЕСПЛАТНЫЙ БЕЗЛИМИТНЫЙ ТРАФИК" src="/images/blue/1_18.png" class="icos">
	<div class="tabs-name">
		 БЕСПЛАТНЫЙ БЕЗЛИМИТНЫЙ ТРАФИК
		<div class="teaser">
			 Беря в аренду готовый сервер для 1С, Вы получаете на весь срок аренды бесплатный, безлимитный трафик для Вашего проекта
		</div>
	</div>
 </li>
	<li> <img alt="БЕСПЛАТНАЯ НАСТРОЙКА И УСТАНОВКА" src="/images/blue/1_03.png" class="icos">
	<div class="tabs-name">
		 БЕСПЛАТНАЯ НАСТРОЙКА И УСТАНОВКА
		<div class="teaser">
			 Мы предлагаем больше конкурентов: Вы можете рассчитывать на бесплатную настройку сервера и установку программ
		</div>
	</div>
 </li>
	<li> <img alt="ПРЕИМУЩЕСТВО SAS И SSD" src="/images/blue/1_13.png" class="icos">
	<div class="tabs-name">
		 ПРЕИМУЩЕСТВО SAS И SSD
		<div class="teaser">
			 Для получения высокого быстродействия используйте диски SAS, для максимальной отдачи дисковой подсистемы выбирайте SSD
		</div>
	</div>
 </li>
</ul>