<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_79.png" alt="Регистрация в любой доменной зоне">
    <div class="tabs-name"> Регистрация в любой доменной зоне 
      <div class="teaser">Регистрация доменов в популярных доменных зонах RU, COM, NET, ORG и т.д. </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_93.png" alt="Бесплатная поддержка DNS">
    <div class="tabs-name"> Бесплатная поддержка DNS 
      <div class="teaser">При регистрации домена вы получаете поддержку NS бесплатно, ресурсные записи не ограничены </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> 
    <img class="icos" src="/images/blue/1_82.png" alt="Возможности">
    <div class="tabs-name"> Возможности 
      <div class="teaser">Подчеркните свою индивидуальность, выберите подходящее доменное имя </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_88.png" alt="За 1 клик проверка в 7 зонах">
    <div class="tabs-name"> За 1 клик проверка в 7 зонах 
      <div class="teaser"> При проверке доменного имени в одной зоне, автоматически проверяем его доступность в других </div>
     </div>
   </li>
 </ul>
