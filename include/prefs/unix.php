<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_37.png" alt="Быстрый старт">
    <div class="tabs-name"> Быстрый старт 
      <div class="teaser">Доступ к услуге в течении 15 минут после оплаты </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_82.png" alt="Широкие возможности">
    <div class="tabs-name"> Широкие возможности 
      <div class="teaser">Все возможности хостинга плюс бесплатно панель управления ISPmanager Lite </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> 
    <img class="icos" src="/images/blue/1_123.png" alt="Низкие затраты">
    <div class="tabs-name"> Низкие затраты 
      <div class="teaser"> Самый дешевый и доступный из всех видов хостинга </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_238.png" alt="Поддержка PHP, MySQL и т.д.">
    <div class="tabs-name"> Поддержка PHP, MySQL и т.д. 
      <div class="teaser">Все тарифы поддерживают Весь джентельменский набор PHP хостинга </div>
     </div>
   </li>
 </ul>
