<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_107.png" alt="Размер почтового ящика 10 GB">
    <div class="tabs-name"> Размер почтового ящика 10 GB 
      <div class="teaser">Каждый почтовый ящик имеет размер 10GB и это не предел он может быть увеличен по вашему запросу </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_114.png" alt="Возможности Microsoft Exchange">
    <div class="tabs-name"> Возможности Microsoft Exchange 
      <div class="teaser">Общие ресурсы: календарь, адресная книга, общие папки, возможность планировать, назначать встречи, совещания и многое другое. </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_121.png" alt="Бесплатная защита от спама">
    <div class="tabs-name"> Бесплатная защита от спама 
      <div class="teaser">Каждый почтовый ящик и домен надежно защищен от спама </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> 
    <img class="icos" src="/images/blue/1_222.png" alt="Бесплатная Антивирусная защита">
    <div class="tabs-name"> Бесплатная Антивирусная защита 
      <div class="teaser">Каждый почтовый ящик находится под надежной защитой антивирусного ПО</div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_95.png" alt="Удобство и доступность">
    <div class="tabs-name"> Удобство и доступность 
      <div class="teaser">Использование Microsoft Exchange дает огромные преимущества и большие возможности корпоративной почтовой системы. </div>
     </div>
   </li>
 
  <li> 
    <img class="icos" src="/images/blue/1_123.png" alt="Снижение затрат">
    <div class="tabs-name"> Снижение затрат 
      <div class="teaser">Арендуя почтовый сервер MS Exchange вы экономите и на обслуживании почтовой системы</div>
     </div>
   </li>
 </ul>
