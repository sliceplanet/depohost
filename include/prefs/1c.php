<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> <img class="icos" src="/images/blue/1_123.png" alt="Снижение ваших затрат"  />
    <div class="tabs-name"> Снижение ваших затрат 
      <div class="teaser">Аренда программных продуктов 1С позволяет экономить не только на покупке, но и на обновлениях ИТС и администрировании</div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_162.png" alt="Доступ 24/7"  />
    <div class="tabs-name"> Доступ 24/7 
      <div class="teaser">Безусловное преимущество круглосуточный доступ из любой точки мира, офиса и филиала. домашнего компьютера или коммандировки </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_167.png" alt="Бесплатное резервное копирование до 15 дней"  />
    <div class="tabs-name"> Бесплатное резервное копирование до 15 дней 
      <div class="teaser">Каждому клиенту предоставляется бесплатно 20 GB места на отдельном сервере для резервных копий с возможностью расширения </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_174.png" alt="Бесплатное обновление ИТС"  />
    <div class="tabs-name"> Бесплатное обновление ИТС 
      <div class="teaser">Беря в аренду 1С вы получаете на весь срок пользования программным продуктом подписку ИТС бесплатно </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix 

ui-widget-header ui-corner-all"> 
  <li> <img class="icos" src="/images/blue/1_151.png" alt="Хранение и защита ваших данных ФЗ 152"  />
    <div class="tabs-name"> Хранение и защита ваших данных ФЗ 152 
      <div class="teaser">Защита от несанкционированного доступа, передача данных по шифрованным каналам, 3-я и 4-я категория, 4-й класс защиты </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_07.png" alt="Работа с большой нагрузкой"  />
    <div class="tabs-name"> Работа с большой нагрузкой 
      <div class="teaser">Мы знаем как правильно использовать аппаратные комплексы, чтобы рассчитать максимальную производительность, отвечающую &quot;цена/качество&quot; </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_169.png" alt="Для пользователей Web App ограничение 5 ГБ"  />
    <div class="tabs-name"> Для пользователей Web App ограничение 5 ГБ 
      <div class="teaser">Мы предлагаем больше конкурентов: по 5 ГБ места для одно пользовательских режимов и доступа через Remoute App </div>
     </div>
   </li>
 
  <li> <img class="icos" src="/images/blue/1_177.png" alt="Для пользователей VDS нет ограничений"  />
    <div class="tabs-name"> Для пользователей VDS нет ограничений 
      <div class="teaser">Для пользователей использующих VDS и выделенный сервер нет ограничений на персональное место на диске</div>
     </div>
   </li>
 </ul>
