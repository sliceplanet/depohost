<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<h2> Хранение и защита данных на арендованом сервере 1С </h2>
 <hr/> 
<p style="text-align: justify;">Аренда сервера с 1С гарантирует безопасность данных, высокий уровень защиты на всём сроке. Клиенты могут не беспокоиться о своих рабочих и личных данных, занесённых в базу 1С, ведь они защищены от посягательства третьих лиц. </p>

<p> </p>

<p>  </p>

<p style="text-align: justify;">Ещё одна удобная опция &ndash; создание резервных копий данных. Если произошёл непредвиденный сбой, восстановить информацию не составит никакого труда, что значительно увеличивает уровень безопасности, защиты имеющихся данных. </p>
