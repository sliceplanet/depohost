
<section class="page-section whyssl-section">
	
	<div class="page-section__h">Для чего нужен SSL-сертификат</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-1.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Быстрая покупка за 15 минут</div>
					<p>Оформите заказ, произведите оплату и получите зарегистрированный сертификат за 15 минут</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-2.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Область применений</div>
					<p>Веб сайт, интернет магазин, интернет торговля, бизнес приложения, почта, сервера приложений с использованием протокола RDP и многое другое</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-3.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Возможности</div>
					<p>SSL сертификат гарантирует вам безопасность передачи данных и стойкость шифрования в общественных и открытых сетях, обеспечивает защиту обмена данными между клиент/сервером</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-4.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущество использования</div>
					<p>Защита персональных данных, стойкость шифрования, легкость в использовании и настройке</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-5.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Задачи которые призван решать SSL</div>
					<p>В первую очередь сохранность персональных данных, конфиденциальность и гарантию целостности, а так же шифрование</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/icon-whyssl-6.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Как выбрать SSL сертификат</div>
					<p>Выберите тип сертификата из списка, используя удобный фильтр, введите имя домена и обязательный e-mail (он должен соответствовать предлагаемому шаблону), нажмите Заказать</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>
