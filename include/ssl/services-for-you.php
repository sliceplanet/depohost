
<section class="page-section services-for-you-section">
	<div class="container">
		<div class="page-section__h">Услуги для вас</div>
		<div class="sfy-block">
			<div class="row">
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-1.png"/></div>
						<a href="#">SSL сертификаты<br>Wildcard</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-2.png"/></div>
						<a href="#">SSL сертификаты<br>Comodo</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-3.png"/></div>
						<a href="#">Хостинг</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sfy-item">
						<div class="sfy-item__icon"><img src="/images/2020/icon-sfy-4.png"/></div>
						<a href="#">Регистрация<br>доменов</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>