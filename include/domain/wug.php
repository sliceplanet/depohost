	
<section class="page-section wug-section">
	
	<div class="page-section__h">Что вы получаете</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Работа с большой нагрузкой</div>
					<p>Все конфигурации протестированы, мы знаем как правильно рассчитать нагрузку на сервер, отвечающую «цена/качество»</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_4.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Доступ 24/7</div>
					<p>Бесспорное преимущество круглосуточный доступ из любой точки мира, офиса, дома, филиала в поездке или командировки</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_7.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатный безлимитный трафик</div>
					<p>Вы получаете на весь срок аренды бесплатный, безлимитный трафик для Вашего проекта</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_2.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">100 GB для резервного копирования</div>
					<p>Каждому клиенту Бесплатно предоставляется 100GB места для резервного копирования на отдельном FTP сервере на весь срок аренды</p>
				</div>
			</div>
			<div class="wug__item wug__item_big">
				<div class="wug__item_big_item">
					<a href="/domain/com/">Домены зоны <span>.com</span></a>
				</div>
				<div class="wug__item_big_item">
					<a href="/domain/rf/">Домены зоны <span>.рф</span></a>
				</div>
				<div class="wug__item_big_item">
					<a href="/domain/ru/">Домены зоны <span>.ru</span></a>
				</div>
				<div class="wug__item_big_item">
					<a href="/domain/besplatnye/">Бесплатные домены</a>
				</div>
				
			</div>
		</div>
		
	</div>
	
</section>	
	
<section class="faq-block">
	<div class="container">
		<h3 class="page-section__h">Вопрос-ответ</h3>
		<div class="faq-items" itemscope itemtype="https://schema.org/FAQPage">
		    <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
		        <div class="faq-item__bottom">
		            <a href="javascript:;" class="faq-item__name"><span itemprop="name">Как купить домен дешево?</span></a>
		            <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Чтобы сэкономить, сравните предложения различных компаний. Вы также можете воспользоваться скидками и специальными акциями. Например, многие конструкторы сайтов предлагают функцию хостинга и покупки доменных имен по особым ценам для своих пользователей.</span></p>
		        </div>
		    </div>
		    <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
		        <div class="faq-item__bottom">
		            <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли купить несколько доменных имен для одного человека или компании?</span></a>
		            <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">У большинства регистраторов нет ограничений по этому поводу.</span></p>
		        </div>
		    </div>
		    <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
		        <div class="faq-item__bottom">
		            <a href="javascript:;" class="faq-item__name"><span itemprop="name">Сколько времени занимает регистрация домена?</span></a>
		            <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Обычно процесс регистрации можно пройти за несколько минут. После этого вам потребуется подтвердить e-mail. По указанному адресу сайт будет открываться в течение 24 часов после регистрации.</span></p>
		        </div>
		    </div>
		</div>
	</div>
</section>	