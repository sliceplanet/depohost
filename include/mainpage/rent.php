
  <section class="rent-block">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="rent-block__title">Как взять в аренду выделенный или виртуальный сервер?</h2>
          <div class="rent-block__desc">Мы предоставляем услуги физическим и юридическим лицам, а также индивидуальным предпринимателям по всей России. Если вы ищите недорогой хостинг для сайта или другого проекта, обращайтесь к нам. Наши сотрудники всегда готовы проконсультировать вас по всем вопросам, связанным с нашими услугами.</div>
          <div class="rent-block__stage">
            <div class="rent-block__stage__item-wrap">
              <div class="rent-block__stage__item">
                <div class="rent-block__stage__item-img">
                  <div class="rent-1"></div>
                </div>
                <div class="rent-block__stage__item-desc_wrap">
                  <div class="rent-block__stage__item-num"> 1 </div>
                  <div class="rent-block__stage__item-desc">Выберите необходимую конфигурацию оборудования</div>
                  <div class="rent-block__stage__item-txt">Если вам нужна помощь, обратитесь к нашим сотрудникам. Мы разработаем решение, подходящее для вашей компании</div>
                </div>
              </div>
            </div>
            <div class="rent-block__stage__item-wrap">
              <div class="rent-block__stage__item">
                <div class="rent-block__stage__item-img">
                  <div class="rent-2"></div>
                </div>
                <div class="rent-block__stage__item-desc_wrap">
                  <div class="rent-block__stage__item-num"> 2 </div>
                  <div class="rent-block__stage__item-desc">Выберите срок оказания услуг дата центра</div>
                  <div class="rent-block__stage__item-txt">При единовременной оплате длительного периода вы получаете пропорциональные скидки.</div>
                </div>
              </div>
            </div>
            <div class="rent-block__stage__item-wrap">
              <div class="rent-block__stage__item">
                <div class="rent-block__stage__item-img">
                  <div class="rent-3"></div>
                </div>
                <div class="rent-block__stage__item-desc_wrap">
                  <div class="rent-block__stage__item-num"> 3 </div>
                  <div class="rent-block__stage__item-desc">Заполните заявку на сайте и отправьте заказ</div>
                  <div class="rent-block__stage__item-txt">Также укажите необходимый формат оплаты. Если вы заказываете услугу от имени юридического лица, мы вышлем необходимые документы. </div>
                </div>
              </div>
            </div>
            <div class="rent-block__stage__item-wrap">
              <div class="rent-block__stage__item">
                <div class="rent-block__stage__item-img">
                  <div class="rent-4"></div>
                </div>
                <div class="rent-block__stage__item-desc_wrap">
                  <div class="rent-block__stage__item-num"> 4 </div>
                  <div class="rent-block__stage__item-desc">Произведите оплату</div>
                  <div class="rent-block__stage__item-txt">Используйте выбранный способ оплаты.</div>
                </div>
              </div>
            </div>
            <div class="rent-block__stage__item-wrap last">
              <div class="rent-block__stage__item">
                <div class="rent-block__stage__item-img">
                  <div class="rent-5"></div>
                </div>
                <div class="rent-block__stage__item-desc_wrap">
                  <div class="rent-block__stage__item-num"> 5 </div>
                  <div class="rent-block__stage__item-desc">Получите данные для доступа и начните пользоваться ресурсом</div>
                  <div class="rent-block__stage__item-txt">Если вам необходима помощь в настройке или установке программ, обратитесь в техподдержку.</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>