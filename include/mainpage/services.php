<section class="services-block">

	<div class="container">

	<div class="services-items">

		<a href="/arenda-dedicated_server/" class="service-item" id="arenda-servera">
			<div class="service-item__img">
				<img src="/images/services/1.svg" />
			</div>
			<div class="service-item__name">Аренда сервера</div>
		</a>

		<a href="/arenda-virtualnogo-vydelennogo-servera-vds/" class="service-item" id="arenda-vds">
			<div class="service-item__img">
				<img src="/images/services/2.svg" />
			</div>
			<div class="service-item__name">Аренда VDS</div>
		</a>

		<a href="/domain/" class="service-item" id="reg-domens">
			<div class="service-item__img">
				<img src="/images/services/3.svg" />
			</div>
			<div class="service-item__name">Регистрация доменов</div>
		</a>

		<a href="/unix-hosting/" class="service-item" id="unix-hosting">
			<div class="service-item__img">
				<img src="/images/services/4.svg" />
			</div>
			<div class="service-item__name">UNIX хостинг</div>
		</a>

		<a href="/microsoft/" class="service-item" id="arenda-microsoft">
			<div class="service-item__img">
				<img src="/images/services/5.svg" />
			</div>
			<div class="service-item__name">Аренда Microsoft</div>
		</a>

		<a href="/1c-server/" class="service-item" id="arenda-1c">
			<div class="service-item__img">
				<img src="/images/services/6.svg" />
			</div>
			<div class="service-item__name">Аренда 1С сервера</div>
		</a>

		<a href="/ispmanager/" class="service-item" id="pan-upr">
			<div class="service-item__img">
				<img src="/images/services/7.svg" />
			</div>
			<div class="service-item__name">Панели управления</div>
		</a>

		<a href="/ssl-sertifikat/" class="service-item" id="ssl-sert">
			<div class="service-item__img">
				<img src="/images/services/8.svg" />
			</div>
			<div class="service-item__name">SSL сертификаты</div>
		</a>
	</div>

	</div>

</section>