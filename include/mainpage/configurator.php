<section class="mainpage-block configurator-block">
    <div class="container">
        <div class="type-server clearfix hidden-xs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="configuration active">
                    <a href="#configuration" class="n-nav-tabs__link" role="tab">

						<i></i>
                        <div>Конфигуратор</div>
                    </a>
                </li>
                <li class="ready-made">
                    <a href="#ready-made" class="n-nav-tabs__link" role="tab">
						<i></i>
                        <div>
                            Готовые решения
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content hidden-xs">
            <?$APPLICATION->IncludeComponent( "itin:dedicated.calc", "v2020", Array( "IBLOCK_ID" => "44", "IBLOCK_TYPE" => "dedicated" ) );?>
            <?$APPLICATION->IncludeComponent( "itin:dedicated.solutions", "v2020", Array( "IBLOCK_ID" => "21", "IBLOCK_TYPE" => "xmlcatalog", "PAGE_COUNT" => "99",'!SECTION_ID'=>278 ) );?>
        </div>
        <!--/.tab-content-->
        <div class="n-accordion panel-group visible-xs" id="accordion">
            <!-- 1 панель -->
            <div class="panel panel-default configuration">
                <!-- Заголовок 1 панели -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="n-nav-tabs__link" role="tab">
                            <div class="panel-title-wrap">
                                <i class="icon icon-configuration"></i>
                                <div>Конфигурация сервера</div>
                            </div>
                            <div class="accodr-arrow"></div>
                        </a>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <!-- Содержимое 1 панели -->
                    <div class="panel-body"><?$APPLICATION->IncludeComponent( "itin:dedicated.calc", "mobile", Array( "IBLOCK_ID" => "44", "IBLOCK_TYPE" => "dedicated" ) );?></div>
                </div>
            </div>
            <!-- 2 панель -->
            <div class="panel panel-default ready-made">
                <!-- Заголовок 2 панели -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="n-nav-tabs__link" role="tab">
                            <div class="panel-title-wrap">
                                <i class="icon icon-ready-made"></i>
                                <div>Готовые решения</div>
                            </div>
                            <div class="accodr-arrow"></div>
                        </a>
                    </div>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <!-- Содержимое 2 панели -->
                    <div class="panel-body"><?$APPLICATION->IncludeComponent( "itin:dedicated.solutions", "mobile", Array( "IBLOCK_ID" => "21", "IBLOCK_TYPE" => "xmlcatalog", "PAGE_COUNT" => "99",'!SECTION_ID'=>278 ) );?></div>
                </div>
            </div>
        </div>
    </div>
</section>
