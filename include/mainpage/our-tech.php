<section class="mainpage-block our-tech-block">

	<div class="container">

		<div class="mpb_h">Наши технологии</div>

		<div class="our-tech__items">
			<div class="our-tech__item"><img src="/images/tech/1.png" alt=""></div>
			<div class="our-tech__item"><img src="/images/tech/2.png" alt=""></div>
			<div class="our-tech__item"><img src="/images/tech/3.png" alt=""></div>
			<div class="our-tech__item"><img src="/images/tech/4.png" alt=""></div>
			<div class="our-tech__item"><img src="/images/tech/5.png" alt=""></div>
		</div>


	</div>

</section>