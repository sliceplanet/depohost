<section class="mainpage-block contact-us-block">

	<div class="container">

		<div class="contact-us__wrapper">
			<div class="contact-us__left">
				<div class="cu_h">
					Свяжитесь с нами<br>любым удобным для вас<br>способом: 
				</div>
			</div>
			<div class="contact-us__right">
				<ul>
					<li><span>1</span>В режиме онлайн консультации;</li>
					<li><span>2</span>Оставив письмо с запросом в разделе <a href="#">Контакты</a>;</li>
					<li><span>3</span>Позвонив по телефону:  <b>+7 (495) 797-8-500</b></li>
				</ul>
			</div>
		</div>

	</div>

</section>