<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> 
<ul> 
  <li> 
    <img class="icos" src="/images/blue/1_03.png">
    <div class="tabs-name"> Бесплатная настройка и установка
      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
    </div>
  </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-administration"></i> Бесплатный безлимитный трафик
      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-traffic-hosting"></i> Бесплатный SMS и EMAIL мониторинг

      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-free"></i> Бесплатное администрирование

      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 </ul>
 
<ul role="tablist" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"> 
  <li> 
    <div class="tabs-name"><i class="icon ico-sphere-application"></i> Работа с большой нагрузкой
 
      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-os"></i> Преимущество SAS и SSD 

      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-sms-email"></i> Канал передачи данных 100 Mbps 
      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 
  <li> 
    <div class="tabs-name"><i class="icon ico-cost-cutting"></i> Быстрый старт 
      <div class="teaser">При аренде VDS сервера вы можете с легкостью разместить средний интернет магазин </div>
     </div>
   </li>
 </ul>
