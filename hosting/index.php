<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Хостинг. ArtexTelecom – предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "Хостинг");
$APPLICATION->SetPageProperty("title", "Хостинг для сайта по недорогой цене в Москве | Услуги хостинга сайта, стоимость в месяц");
$APPLICATION->SetTitle("Хостинг");
?>

<div class="container">
 <div class="hosting-top">
  <div class="row">
   <div class="col-xs-12">

    <img src="/images/hosting-icon.png" title="Хостинг" alt="Хостинг">

    <h1 class="new-h1">Хостинг</h1>
    <div class="hosting-top-desc">Мы найдем подходящее решение для любого масштаба бизнеса – от недорогих вариантов для
     ресурсов с невысокой посещаемостью до продвинутых продуктов для интернет-магазинов с большим потоком клиентов.
    </div>
   </div>
  </div>


  <div class="row our_hosting">
   <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="hosting-top-block">
     <img title="Unix хостинг" src="/images/hosting-unix-icon.png" alt="Хостинг" width="56" height="48">
     <div class="hosting-top-block-title">UNIX хостинг</div>

     <p class="hosting-top-block__desc">Самый доступный вариант из всех видов хостинга. Подходит для start-up проектов и
      размещения сайтов с небольшим потоком посетителей.</p>
     <div class="block-btn-more-detailed"><a href="/unix-hosting/" class="btn-more-detailed">Подробнее</a></div>
    </div>
   </div>
   <div class="col-lg-6 col-md-6 col-sm-6">
    <div class="hosting-top-block">

     <img src="/images/hosting-dns-icon.png" title="DNS хостинг" alt="Хостинг" width="56" height="56">
     <div class="hosting-top-block-title">Хостинг DNS</div>

     <p class="hosting-top-block__desc">Более продвинутое решение, используемое для крупных платформ с большим количеством
      пользователей.</p>
     <div class="block-btn-more-detailed"><a href="/hosting-dns/" class="btn-more-detailed">Подробнее</a></div>
    </div>
   </div>
  </div>
 </div>
</div>
<div class="hosting-middle">
 <div class="container">
  <h2>Преимущества веб-хостинга с Artextelecom</h2>

  <div class="advantage-img-wrap">
   <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item">
     <div class="advantage__item_icon rel_serv"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Hадежные процессоры</div>

      <div class="advantage__item_description"> Все серверы нашей компании надежны: они имеют мощные процессоры и
       работают бесперебойно независимо от системы управления сайтом.
      </div>
     </div>
    </li>

    <li class="advantage__item">

     <div class="advantage__item_icon papers"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Удобство использования</div>

      <div class="advantage__item_description">При заказе услуги хостинга вы можете разместить неограниченное количество
       ресурсных записей ваших доменов.
      </div>
     </div>
    </li>

    <li class="advantage__item">

     <div class="advantage__item_icon page"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Домены в различных зонах</div>

      <div class="advantage__item_description"> Вы можете разместить на нашем сервере сайты на WordPress, MODx, Drupal,
       1С-Битрикс, Joomla, Tilda Publishing и других CMS. Независимо от выбранной системы работать на ней будет
       максимально удобно.
      </div>
     </div>
    </li>

    <li class="advantage__item">

     <div class="advantage__item_icon smart"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Неограниченное количество записей</div>

      <div class="advantage__item_description"> Мощности наших серверов достаточны для оптимизации сайта под большое
       количество устройств. Ваш веб-ресурс будет доступен для работы со смартфонов, планшетов, ПК, ноутбуков независимо
       от размеров их экранов.
      </div>
     </div>
    </li>

    <li class="advantage__item">

     <div class="advantage__item_icon www"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Мощные серверы</div>

      <div class="advantage__item_description"> Если у вас еще нет готового сайта, мы предлагаем регистрацию доменов в
       различных зонах – от популярных ru, net, com, org, info до редких sx, cc, tv и проч.
      </div>
     </div>
    </li>

    <li class="advantage__item">

     <div class="advantage__item_icon star"></div>
     <div class="advantage__item_teaser pt-0">
      <div class="advantage__item-title">Модернизация сервера</div>

      <div class="advantage__item_description"> Если вы решите усовершенствовать сайт, и потребуется большее
       пространство
       и мощности для его размещения, возможна модернизация сервера по вашему запросу.
      </div>
     </div>
    </li>
   </ul>
  </div>

  <h2>Бонусы</h2>

  <div class="advantage-img-wrap">
   <ul class="advantage-bonus" style="padding-bottom: 20px;">
    <li class="advantage__item_bonus">
     <div class="advantage__item_teaser-bonus">
      <div class="advantage__item_description-bonus"> Скидки постоянным клиентам при предоплате за 3–6–12 месяцев
       пользования
       услугой.
      </div>
     </div>
    </li>

    <li class="advantage__item_bonus">
     <div class="advantage__item_teaser-bonus">
      <div class="advantage__item_description-bonus">Бесплатная настройка сервера и приложений, перенос данных на наш
       сервер.
      </div>
     </div>
    </li>

    <li class="advantage__item_bonus">
     <div class="advantage__item_teaser-bonus">
      <div class="advantage__item_description-bonus">Бесплатные 15 дней размещения вашего сайта на наших серверах после
       оплаты
       хостинга.
      </div>
     </div>
    </li>
    <li class="advantage__item_bonus">
     <div class="advantage__item_teaser-bonus">
      <div class="advantage__item_description-bonus"> Бесплатные интернет-трафик и 100 Гб для хранения резервных копий
       на
       внутреннем FTP-сервере.
      </div>
     </div>
    </li>




   </ul>
  </div>

 </div>
</div>

</div>
<div class="container">
 <div class="callback-n">
  <div class="callback-n__block">
   <div class="callback-n__title">
    Свяжитесь с нами<br/> любым удобным для вас<br/> способом:
   </div>
    
  </div>
  <div class="callback-n__block">
   <div class="callback-n__list">
    <div class="callback-n__item">
     <div class="callback-n__item-num">1</div>
     В режиме онлайн консультации;

    </div>
    <div class="callback-n__item">
     <div class="callback-n__item-num">2</div>

     Оставив письмо с запросом в разделе <a class="callback-n__item_contacts" href="/contacts/">Контакты</a>

    </div>
    <div class="callback-n__item">
     <div class="callback-n__item-num">3</div>

     Позвонив по телефону: <a class="callback-n__item_phone" href="tel:+7(495)7978500"> +7 (495) 797-8-500</a></div>


   </div>
  </div>
 </div>
 <div class="technologies">
  <div class="technologies-title">Наши технологии</div>
  <div class="technologies-list">

   <div class="technologies-item">
    <img class="technologies-item-img" src="/images/logo-windows.png" alt="">
   </div>
   <div class="technologies-item">
    <img class="technologies-item-img" src="/images/logo-isp.png" alt="">
   </div>
   <div class="technologies-item">
    <img class="technologies-item-img" src="/images/logo-freebsd.png" alt="">
   </div>
   <div class="technologies-item">
    <img class="technologies-item-img" src="/images/logo-centos.png" alt="">
   </div>
   <div class="technologies-item">
    <img class="technologies-item-img" src="/images/logo-debian.png" alt="">
   </div>
  </div>
 </div>
</div>




<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>