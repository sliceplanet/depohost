<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Хостинг. ArtexTelecom – предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("keywords", "хостинг php, хостинг с php и mysql, хостинг php сайтов, хостинг с поддержкой php, хостинг сайтов php mysql, linux хостинг, unix хостинг");
$APPLICATION->SetPageProperty("title", "Хостинг для сайта — купить по недорогой цене | Услуги хостинга");
$APPLICATION->SetTitle("Хостинг");
?>

<div class="container" style="background-color: white;">
    <img src="/images/hosting.jpg" alt="Хостинг" width="100%">

<p>Ищете надежное пространство для размещения вашего интернет-магазина, витрины, игровой площадки или корпоративного портала? В компании ArtexTelecom вы можете воспользоваться услугой хостинга для сайта на выгодных условиях. Мы найдем подходящее решение для любого масштаба бизнеса – от недорогих вариантов для ресурсов с невысокой посещаемостью до продвинутых продуктов для интернет-магазинов с большим потоком клиентов. </p>

<h2>Наше предложение</h2>

<p>В ArtexTelecom вы можете купить хостинги:</p>

<div class="row our_hosting">
    <div class="col-lg-6 col-md-6 col-sm-6"><img src="/images/unix.gif" alt="Хостинг" width="140" height="100"><p>Unix. Самый доступный вариант из всех видов хостинга. Подходит для start-up проектов и размещения сайтов с небольшим потоком посетителей.</p></div>
    <div class="col-lg-6 col-md-6 col-sm-6"><img src="/images/dns.png" alt="Хостинг" width="100"><p>DNS. Более продвинутое решение, используемое для крупных платформ с большим количеством пользователей.</p></div>
</div>

<h2>Почему стоит выбрать наш веб-хостинг?</h2>

<div class="advantage-img-wrap">
  <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item">
      <div class="advantage__item_icon rel_serv"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Все серверы нашей компании надежны: они имеют мощные процессоры и работают бесперебойно независимо от системы управления сайтом. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon papers"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">При заказе услуги хостинга вы можете разместить неограниченное количество ресурсных записей ваших доменов.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon page"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Вы можете разместить на нашем сервере сайты на WordPress, MODx, Drupal, 1С-Битрикс, Joomla, Tilda Publishing и других CMS. Независимо от выбранной системы работать на ней будет максимально удобно. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon smart"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Мощности наших серверов достаточны для оптимизации сайта под большое количество устройств. Ваш веб-ресурс будет доступен для работы со смартфонов, планшетов, ПК, ноутбуков независимо от размеров их экранов. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon www"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Если у вас еще нет готового сайта, мы предлагаем регистрацию доменов в различных зонах – от популярных ru, net, com, org, info до редких sx, cc, tv и проч.
        </div>
      </div>
    </li>
    
    <li class="advantage__item">
      <div class="advantage__item_icon star"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Если вы решите усовершенствовать сайт, и потребуется большее пространство и мощности для его размещения, возможна модернизация сервера по вашему запросу. 
        </div>
      </div>
    </li>
  </ul>
</div>

<h2>Преимущества сотрудничества с ArtexTelecom</h2>

<div class="advantage-img-wrap">
  <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item">
      <div class="advantage__item_icon calc"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Скидки постоянным клиентам при предоплате за 3–6–12 месяцев пользования услугой. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">Бесплатная настройка сервера и приложений, перенос данных на наш сервер.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon mouse"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">Бесплатные 15 дней размещения вашего сайта на наших серверах после оплаты хостинга. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Никаких скрытых платежей – только абонентская плата. В исключительных случаях возможна рассрочка платежа в течение одного месяца. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon day"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Приступаем к исправлению ошибок и оказываем профессиональную помощь с настройками сразу же после вашего обращения в техподдержку. 
        </div>
      </div>
    </li>
    
    <li class="advantage__item">
      <div class="advantage__item_icon path"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Бесплатные интернет-трафик и 100 Гб для хранения резервных копий на внутреннем FTP-сервере.
        </div>
      </div>
    </li>
  </ul>
</div>

<p>Чтобы воспользоваться услугой хостинга сайтов, обратитесь к нашему специалисту в режиме онлайн-консультации с помощью формы обратной связи на странице «Контакты» или позвоните по круглосуточному номеру 8 (495) 797-8-500. Наш сотрудник сориентирует вас по тарифам и возможностям каждого предложения и поможет подобрать идеальный по цене и характеристикам вариант с учетом специфики вашего бизнеса. </p>

</div>

<div class="bg-left"></div>
<div class="bg-right"></div>
  <!-- <div class="container"> -->
     <? //$APPLICATION->IncludeComponent(
    //   "bitrix:catalog",
    //   "hosting",
    //   array(
    //     "IBLOCK_TYPE" => "xmlcatalog",
    //     "IBLOCK_ID" => "32",
    //     "HIDE_NOT_AVAILABLE" => "N",
    //     "SECTION_ID_VARIABLE" => "SECTION_ID",
    //     "SEF_MODE" => "Y",
    //     "SEF_FOLDER" => "/hosting/",
    //     "AJAX_MODE" => "N",
    //     "AJAX_OPTION_JUMP" => "N",
    //     "AJAX_OPTION_STYLE" => "Y",
    //     "AJAX_OPTION_HISTORY" => "N",
    //     "CACHE_TYPE" => "A",
    //     "CACHE_TIME" => "3600",
    //     "CACHE_FILTER" => "N",
    //     "CACHE_GROUPS" => "Y",
    //     "SET_STATUS_404" => "Y",
    //     "SET_TITLE" => "Y",
    //     "ADD_SECTIONS_CHAIN" => "Y",
    //     "ADD_ELEMENT_CHAIN" => "N",
    //     "USE_ELEMENT_COUNTER" => "Y",
    //     "USE_FILTER" => "N",
    //     "FILTER_VIEW_MODE" => "HORIZONTAL",
    //     "USE_REVIEW" => "N",
    //     "USE_COMPARE" => "Y",
    //     "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
    //     "COMPARE_FIELD_CODE" => array(
    //       0 => "",
    //       1 => "",
    //     ),
    //     "COMPARE_PROPERTY_CODE" => array(
    //       0 => "",
    //       1 => "",
    //     ),
    //     "COMPARE_ELEMENT_SORT_FIELD" => "sort",
    //     "COMPARE_ELEMENT_SORT_ORDER" => "asc",
    //     "DISPLAY_ELEMENT_SELECT_BOX" => "N",
    //     "PRICE_CODE" => array(
    //       0 => "Розничная",
    //     ),
    //     "USE_PRICE_COUNT" => "N",
    //     "SHOW_PRICE_COUNT" => "1",
    //     "PRICE_VAT_INCLUDE" => "Y",
    //     "PRICE_VAT_SHOW_VALUE" => "N",
    //     "CONVERT_CURRENCY" => "N",
    //     "BASKET_URL" => "/personal/cart/",
    //     "ACTION_VARIABLE" => "action",
    //     "PRODUCT_ID_VARIABLE" => "id",
    //     "USE_PRODUCT_QUANTITY" => "Y",
    //     "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    //     "ADD_PROPERTIES_TO_BASKET" => "Y",
    //     "PRODUCT_PROPS_VARIABLE" => "prop",
    //     "PARTIAL_PRODUCT_PROPERTIES" => "N",
    //     "PRODUCT_PROPERTIES" => array(),
    //     "SHOW_TOP_ELEMENTS" => "Y",
    //     "TOP_ELEMENT_COUNT" => "30",
    //     "TOP_LINE_ELEMENT_COUNT" => "1",
    //     "TOP_ELEMENT_SORT_FIELD" => "sort",
    //     "TOP_ELEMENT_SORT_ORDER" => "asc",
    //     "TOP_ELEMENT_SORT_FIELD2" => "id",
    //     "TOP_ELEMENT_SORT_ORDER2" => "desc",
    //     "TOP_PROPERTY_CODE" => array(
    //       0 => "",
    //       1 => "",
    //     ),
    //     "SECTION_COUNT_ELEMENTS" => "Y",
    //     "SECTION_TOP_DEPTH" => "2",
    //     "SECTIONS_VIEW_MODE" => "TEXT",
    //     "SECTIONS_SHOW_PARENT_NAME" => "Y",
    //     "PAGE_ELEMENT_COUNT" => "30",
    //     "LINE_ELEMENT_COUNT" => "1",
    //     "ELEMENT_SORT_FIELD" => "sort",
    //     "ELEMENT_SORT_ORDER" => "asc",
    //     "ELEMENT_SORT_FIELD2" => "id",
    //     "ELEMENT_SORT_ORDER2" => "desc",
    //     "LIST_PROPERTY_CODE" => array(
    //       0 => "CMN_MB",
    //       1 => "CMN_TRF",
    //       2 => "CMN_EML",
    //       3 => "CMN_DMN",
    //       4 => "CMN_WBS",
    //       5 => "CMN_SDM",
    //       6 => "CMN_CPU",
    //       7 => "CMN_MMR",
    //       8 => "CMN_PRC",
    //       9 => "PLS_PHP5",
    //       10 => "PLS_PHP4",
    //       11 => "PLS_ZND",
    //       12 => "PLS_BIN",
    //       13 => "PLS_CGI",
    //       14 => "PLS_PRL",
    //       15 => "ADD_BDM",
    //       16 => "PLS_PMA",
    //       17 => "PLS_SSI",
    //       18 => "PLS_FTP",
    //       19 => "PLS_MFTP",
    //       20 => "PLS_PSW",
    //       21 => "PLS_HTC",
    //       22 => "PLS_ISP",
    //       23 => "PLS_WBE",
    //       24 => "PLS_ANTI",
    //       25 => "PLS_ERR",
    //       26 => "PLS_SSL",
    //       27 => "",
    //     ),
    //     "INCLUDE_SUBSECTIONS" => "Y",
    //     "LIST_META_KEYWORDS" => "-",
    //     "LIST_META_DESCRIPTION" => "-",
    //     "LIST_BROWSER_TITLE" => "-",
    //     "DETAIL_PROPERTY_CODE" => array(
    //       0 => "CMN_MB",
    //       1 => "CMN_TRF",
    //       2 => "CMN_EML",
    //       3 => "CMN_DMN",
    //       4 => "CMN_WBS",
    //       5 => "CMN_SDM",
    //       6 => "CMN_CPU",
    //       7 => "CMN_MMR",
    //       8 => "CMN_PRC",
    //       9 => "PLS_PHP5",
    //       10 => "PLS_PHP4",
    //       11 => "PLS_ZND",
    //       12 => "PLS_BIN",
    //       13 => "PLS_CGI",
    //       14 => "PLS_PRL",
    //       15 => "ADD_BDM",
    //       16 => "PLS_PMA",
    //       17 => "PLS_SSI",
    //       18 => "PLS_FTP",
    //       19 => "PLS_MFTP",
    //       20 => "PLS_PSW",
    //       21 => "PLS_HTC",
    //       22 => "PLS_ISP",
    //       23 => "PLS_WBE",
    //       24 => "PLS_ANTI",
    //       25 => "PLS_ERR",
    //       26 => "PLS_SSL",
    //       27 => "",
    //     ),
    //     "DETAIL_META_KEYWORDS" => "-",
    //     "DETAIL_META_DESCRIPTION" => "-",
    //     "DETAIL_BROWSER_TITLE" => "-",
    //     "LINK_IBLOCK_TYPE" => "",
    //     "LINK_IBLOCK_ID" => "",
    //     "LINK_PROPERTY_SID" => "",
    //     "LINK_ELEMENTS_URL" => "",
    //     "USE_ALSO_BUY" => "Y",
    //     "ALSO_BUY_ELEMENT_COUNT" => "5",
    //     "ALSO_BUY_MIN_BUYES" => "2",
    //     "USE_STORE" => "N",
    //     "PAGER_TEMPLATE" => "",
    //     "DISPLAY_TOP_PAGER" => "N",
    //     "DISPLAY_BOTTOM_PAGER" => "Y",
    //     "PAGER_TITLE" => "Товары",
    //     "PAGER_SHOW_ALWAYS" => "N",
    //     "PAGER_DESC_NUMBERING" => "N",
    //     "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    //     "PAGER_SHOW_ALL" => "N",
    //     "ADD_PICT_PROP" => "-",
    //     "LABEL_PROP" => "-",
    //     "SHOW_DISCOUNT_PERCENT" => "N",
    //     "SHOW_OLD_PRICE" => "N",
    //     "DETAIL_SHOW_MAX_QUANTITY" => "N",
    //     "MESS_BTN_BUY" => "Купить",
    //     "MESS_BTN_ADD_TO_BASKET" => "Заказать",
    //     "MESS_BTN_COMPARE" => "Сравнение",
    //     "MESS_BTN_DETAIL" => "Подробнее",
    //     "MESS_NOT_AVAILABLE" => "Нет в наличии",
    //     "DETAIL_USE_VOTE_RATING" => "N",
    //     "DETAIL_USE_COMMENTS" => "N",
    //     "DETAIL_BRAND_USE" => "N",
    //     "AJAX_OPTION_ADDITIONAL" => "",
    //     "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
    //     "SEF_URL_TEMPLATES" => array(
    //       "sections" => "",
    //       "section" => "",
    //       "element" => "",
    //       "compare" => "compare.php?action=#ACTION_CODE#",
    //     ),
    //     "VARIABLE_ALIASES" => array(
    //       "compare" => array(
    //         "ACTION_CODE" => "action",
    //       ),
    //     )
    //   ),
    //   false
    // ); ?>
  <!-- </div> -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>