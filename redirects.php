<?php

$seoRedirects = [
  '/dedicated_server/' => '/arenda-dedicated_server/',
  '/vds/' => '/arenda-virtualnogo-vydelennogo-servera-vds/',
  '/arenda-virtualnogo-servera-na-windows/' => '/microsoft/',
  '/ssl/' => '/ssl-sertifikat/',
  '/index.php' => '/',
];

$curPage = $APPLICATION->GetCurPage(false);
foreach ($seoRedirects as $oldUrl => $newUrl)
{
  $pos = stripos($_SERVER["REQUEST_URI"], $oldUrl);
  if ($pos !== false)
  {
    $redirectTo = substr_replace($_SERVER["REQUEST_URI"], $newUrl, $pos, strlen($oldUrl));
    //file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log.log', $redirectTo . "\n\r", FILE_APPEND);
    LocalRedirect($redirectTo, true, 301);

  }
}

$pointRedirects = [
  '/microsoft/arenda-servera-windows/' => '/microsoft/',
  '/ispmanager/paneli-upravleniya-serverom/' => '/ispmanager/',
  '/arenda-dedicated_server/index2.php' => '/arenda-dedicated_server/',
  '/news/?PAGEN_1=1' => '/news/',
  '/arenda-virtualnogo-vydelennogo-servera-vds/testvds/' => '/rules-test-vds.php',
  '/arenda-servera/' => '/'
];

if(!empty($pointRedirects[$_SERVER["REQUEST_URI"]])){
  LocalRedirect($pointRedirects[$_SERVER["REQUEST_URI"]], true, 301);
}

