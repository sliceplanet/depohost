<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
global $APPLICATION;
function ShowTabs()
{
  global $APPLICATION;
  if (\CHTTP::GetLastStatus() !== '404 Not Found' && $APPLICATION->GetProperty('NOT_SHOW_TABS') != 'Y')
  {
    ob_start();
    $APPLICATION->IncludeComponent(
      "bitrix:main.include", "", Array(
      "AREA_FILE_SHOW" => "page",
      "AREA_FILE_SUFFIX" => "tabs",
      "AREA_FILE_RECURSIVE" => "N",
      "EDIT_MODE" => "html",
      "EDIT_TEMPLATE" => ""
    ), false, array('HIDE_ICONS' => 'Y')
    );

    $html = ob_get_contents();
    ob_end_clean();
    return $html;
  }
}

if (array_key_exists('action', $_REQUEST)
  && ($_REQUEST['action'] == 'BUY'
    || $_REQUEST['action'] == 'ADD2BASKET'
    || $_REQUEST['action'] == 'BUYIT'
    || $_REQUEST['action'] == 'BUY-WITH-PROPS'
  )
)
{
  if (stripos($APPLICATION->GetCurPage(), '/personal/cart/ajax-add.php') === false)
  {
    $_GET['action'] = 'BUY';
    LocalRedirect('/personal/cart/ajax-add.php?' . http_build_query($_GET));
  }

}


?>