<?php


// Обработчики событий связанные с модулем Клиенты

$fileDev = $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/include/dev.php';
if (file_exists($fileDev)) {
    require_once $fileDev;
}

$fileUsefull = $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/include/usefull.php';
if (file_exists($fileUsefull)) {
    require_once $fileUsefull;
}

include ($_SERVER["DOCUMENT_ROOT"] . "/redirects.php");

AddEventHandler("main", "OnBeforeEventAdd", array("KPHandler", "OnBeforeEventAddHandler"));

class KPHandler
{
    function OnBeforeEventAddHandler(&$event, &$lid, $arFields)
    {
        if ($event == "FORM_FILLING_SIMPLE_FORM_3") {
            require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/pdfgen/WkHtmlToPdf.php");
            require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/pdfgen/mail_attach.php");

            CModule::IncludeModule('sale');
            $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ), array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ), false, false, array("ID", "QUANTITY", "PRICE", "NAME", "PRODUCT_ID", 'MEASURE_NAME', 'NOTES')
            );
            $sum = 0;
            $table_hrml_str = '';
            $i = 1;
            $sale = $arFields["SIMPLE_QUESTION_889"] != '' ? (int)$arFields["SIMPLE_QUESTION_889"] : 0;
            while ($arItems = $dbBasketItems->Fetch()) {
                $discount_price = $arItems["PRICE"] - $arItems["PRICE"] * $sale / 100;
                $sum += $arItems["QUANTITY"] * $discount_price;

                $bMeasureShow = false;
                if (strlen($arItems["NOTES"]) > 0) {
                    $arNotes = unserialize($arItems["~NOTES"]);
                    if (key_exists('MEASURE_NAME', $arNotes) && $arNotes['MEASURE_NAME'] != "") {
                        $measure_name = $arNotes['MEASURE_NAME'];
                        $bMeasureShow = true;
                    }
                }
                if ($bMeasureShow === false) {
                    $measure_name = $arItems['MEASURE_NAME'];
                }
                $quantity = (int)$arItems["QUANTITY"] . '&nbsp;' . $measure_name;
                //добавляем дополнительные свойства
                // Выведем все свойства элемента корзины с кодом $basketID
                $rsPropsList = CSaleBasket::GetPropsList(
                    array("SORT" => "ASC", 'ID' => 'ASC'), array('BASKET_ID' => $arItems["ID"])
                );
                $arPropsList = array();
                while ($arProp = $rsPropsList->Fetch()) {
                    if (stripos($arProp['NAME'], 'XML_ID') === false && $arProp['NAME'] != 'Покупка') {
                        $arPropsList[] = $arProp;
                    }
                }

				$bad_props = [	
					'unique_id',
					'price_1month',
					'is-ms',
					'is-solution',	
					'is-domain',
					'is-1c',
					'is-isp',
					'is-configurator',
					'is-ssl',
					'is-vds',
					'is_service',
				];
                $propsText = '';
                foreach ($arPropsList as $prop) {
                    //echo '<br>'.'<br>'.$prop['BASKET_ID'].' ? '.$arItems['ID'].'<br>';
                    if ($prop['BASKET_ID'] == $arItems['ID']) {
						if(in_array($prop['CODE'],$bad_props)) continue;
						
                        $propsText .= $prop['NAME'] . ': ' . $prop['VALUE'] . '  <br>';
                        // $propsText .= $prop['NAME'] . ': ' . $prop['VALUE'] . ' __test__ <br>';
                    }
                }


                $table_hrml_str .= '
                    <tr style="background:white;">
                        <td>' . $i . '</td>
                        <td>' . $arItems["NAME"] . '&nbsp;&nbsp;-&nbsp;&nbsp;' . $quantity . '<br>' . $addText . $propsText . '</td>
                        <td>' . SaleFormatCurrency(($arItems["PRICE"] * $quantity), "RUB") . '</td>
                        <td>' . $sale . '%</td>
                        <td>' . SaleFormatCurrency((round($discount_price * $quantity)), "RUB") . '</td>
                    </tr>' . "\n";
                $i++;
            }


            $KP_HTML = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/pdfgen/kp.tpl.html");
            $KP_HTML = str_replace(array('#DATE#', '#TABLE_STR#', '#TOTAL_SUM#'), array(date('d.m.Y'), $table_hrml_str, SaleFormatCurrency($sum, "RUB")), $KP_HTML);

            $pdf = new WkHtmlToPdf;
            $pdf->addPage($KP_HTML);
            $pdfFile = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/pdfgen/tmp/" . substr(md5(time()), rand(1, 10)) . ".pdf";
            $pdf->saveAs($pdfFile);
            $arFields["FILES"][] = $pdfFile;
            SendAttache($event, $lid, $arFields);
            $event = 'null';
            $lid = 'null';
            unlink($pdfFile);
        }
    }

}

AddEventHandler("sale", "OnBeforeBasketDelete", "OnBeforeBasketDeleteHandler");

function OnBeforeBasketDeleteHandler($ID)
{
	$props = [];
	$db_res = CSaleBasket::GetPropsList(
			array(
					"SORT" => "ASC",
					"NAME" => "ASC"
				),
			array("BASKET_ID" => $ID)
		);
	while($ar_res = $db_res->Fetch())
	{		
		$props[$ar_res['CODE']] = $ar_res['VALUE'];
	}	
	
	if(!$props['is_service'] && $props['unique_id'])
	{
		$ids = [];

		$db_res = CSaleBasket::GetPropsList(
				array(
						"SORT" => "ASC",
						"NAME" => "ASC"
					),
				array("!BASKET_ID" => $ID,'CODE'=>'unique_id','VALUE'=>$props['unique_id'])
			);
		while($ar_res = $db_res->Fetch())
		{		
			CSaleBasket::Delete($ar_res['BASKET_ID']);
		}	
		
	}
}
