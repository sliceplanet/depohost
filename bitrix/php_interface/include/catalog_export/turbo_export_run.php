<?
//<title>Turbo Pages</title>
/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @var int $IBLOCK_ID */
/** @var string $SETUP_SERVER_NAME */
/** @var string $SETUP_FILE_NAME */
/** @var array $V */
/** @var array|string $XML_DATA */
/** @var bool $firstStep */
/** @var int $CUR_ELEMENT_ID */
/** @var bool $finalExport */
/** @var bool $boolNeedRootSection */
/** @var int $intMaxSectionID */
use Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Iblock,
	Bitrix\Catalog;

IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/export_yandex.php');
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");


if (!function_exists("yandex_replace_special"))
{
	function yandex_replace_special($arg)
	{
		if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
			return $arg[0];
		else
			return " ";
	}
}

if (!function_exists("yandex_text2xml"))
{
	function yandex_text2xml($text, $bHSC = false, $bDblQuote = false)
	{
		global $APPLICATION;

		$bHSC = (true == $bHSC ? true : false);
		$bDblQuote = (true == $bDblQuote ? true: false);

		if ($bHSC)
		{
			$text = htmlspecialcharsbx($text);
			if ($bDblQuote)
				$text = str_replace('&quot;', '"', $text);
		}
		$text = preg_replace("/[\x1-\x8\xB-\xC\xE-\x1F]/", "", $text);
		$text = str_replace("'", "&apos;", $text);
		//$text = $APPLICATION->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
		return $text;
	}
}










class TurboExport{

	const TEMPLATE_FOLDER = "/local/templates/depohost/turbo";

	const STATIC_FOLDER = "/static";

	const EXPORT_FIELD = "EXPORT_TURBO";

	const PRODUCT_IBLOCK = 21;

	const NEWS_IBLOCK = 3;

	const OFFERS_IBLOCK = 0;

	public $serverName = "https://www.depohost.ru";


	public function export()
	{

		$result .= $this->exportElementFromIblock(TurboExport::NEWS_IBLOCK,"news");

		$result = '<?xml version="1.0" encoding="UTF-8"?>
<rss
    xmlns:yandex="http://news.yandex.ru"
    xmlns:media="http://search.yahoo.com/mrss/"
    xmlns:turbo="http://turbo.yandex.ru"
    version="2.0"
>
    <channel>
    	<title>Аренда сервера, услуги аренды сервера в дата-центре в Москве, недорогой качественный хостинг сервера в ЦОД ArtexTelecom</title>
        <link>https://www.depohost.ru/</link>
        <description>Предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.</description>
		'.$result.'
    </channel>
</rss>';


		$result =  yandex_text2xml($result);

		file_put_contents($_SERVER["DOCUMENT_ROOT"]."/turbo_pages.xml", $result);
	}

	public function exportStaticPages()
	{
		$files = scandir($_SERVER["DOCUMENT_ROOT"].TurboExport::TEMPLATE_FOLDER.TurboExport::STATIC_FOLDER);

		$files = array_slice($files, 2);

		$data["SERVER_NAME"] = $this->serverName;
		foreach ($files as $key => $file) {
			ob_start();
			include($_SERVER["DOCUMENT_ROOT"].TurboExport::TEMPLATE_FOLDER.TurboExport::STATIC_FOLDER."/".$file);
			$result .= ob_get_clean();
		}

		return $result;
	}

	public function exportElementFromIblock($iblockId, $template)
    {
        if(!CModule::IncludeModule("iblock"))
            return false;

        $elementIterator = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
            	"IBLOCK_ID" => $iblockId,
            	"ACTIVE" => "Y",
            	"PROPERTY_".TurboExport::EXPORT_FIELD."_VALUE" => "Y"
            ),
            false,
            false,
            Array()
        );

        while($elementHandler = $elementIterator->GetNextElement()){

        	$element = $elementHandler->GetFields();

        	if(!empty($element["DETAIL_PICTURE"])){
        		$element["DETAIL_PICTURE"] = CFile::GetFileArray($element["DETAIL_PICTURE"]);
        	}

        	if(!empty($element["PREVIEW_PICTURE"])){
        		$element["PREVIEW_PICTURE"] = CFile::GetFileArray($element["PREVIEW_PICTURE"]);
        	}
        	$element["PROPERTIES"] = $elementHandler->GetProperties();


        	foreach($element["PROPERTIES"] as $code => &$property){
        		if($property["MULTIPLE"] == "Y" && $property["PROPERTY_TYPE"] == "F" ){

        			if(!empty($property["VALUE"])){
	        			$fileArray = array();
	        			foreach($property["VALUE"] as $fileId){
	        				$fileArray[] = CFile::GetFileArray($fileId);
	        			}

	        			$property = $fileArray;
        			}else{
        				$property = false;
        			}


        		} elseif ($property["PROPERTY_TYPE"] == "E") {
					$propertyIterator = CIBlockElement::GetList(
			            Array("SORT"=>"ASC"),
			            Array(
			            	"ID" => $property["VALUE"]
			            ),
			            false,
			            false,
			            Array()
			        );
			        if ($propertyE = $propertyIterator->GetNextElement()) {
			        	$property["ELEMENT_DESC"]["FIELDS"] = $propertyE->GetFields();
			        	$property["ELEMENT_DESC"]["PROPERTIES"] = $propertyE->GetProperties();
			        }
        		}
        	}



        	$rsProperties = CIBlockElement::GetProperty($iblockId, $element["ID"], "value_id", "asc", array("ACTIVE"=>"Y"));
			$arProperties = array();
			while($arProperty = $rsProperties->Fetch())
			{
				if($arProperty["CODE"]=="vote_count")
					$arProperties["vote_count"] = $arProperty;
				elseif($arProperty["CODE"]=="vote_sum")
					$arProperties["vote_sum"] = $arProperty;
				elseif($arProperty["CODE"]=="rating")
					$arProperties["rating"] = $arProperty;
			}

			$obProperty = new CIBlockProperty;
			$res = true;
			if(!array_key_exists("vote_count", $arProperties))
			{
				$res = $obProperty->Add(array(
					"IBLOCK_ID" => $iblockId,
					"ACTIVE" => "Y",
					"PROPERTY_TYPE" => "N",
					"MULTIPLE" => "N",
					"NAME" => "Количество проголосовавших",
					"CODE" => "vote_count",
				));
				if($res)
					$arProperties["vote_count"] = array("VALUE"=>0);
			}
			if($res && !array_key_exists("vote_sum", $arProperties))
			{
				$res = $obProperty->Add(array(
					"IBLOCK_ID" => $iblockId,
					"ACTIVE" => "Y",
					"PROPERTY_TYPE" => "N",
					"MULTIPLE" => "N",
					"NAME" => "Сумма оценок",
					"CODE" => "vote_sum",
				));
				if($res)
					$arProperties["vote_sum"] = array("VALUE"=>0);
			}
			if($res && !array_key_exists("rating", $arProperties))
			{
				$res = $obProperty->Add(array(
					"IBLOCK_ID" => $iblockId,
					"ACTIVE" => "Y",
					"PROPERTY_TYPE" => "N",
					"MULTIPLE" => "N",
					"NAME" => "Рейтинг",
					"CODE" => "rating",
				));
				if($res)
					$arProperties["rating"] = array("VALUE"=>0);
			}
			if($res)
			{
				$element["RAITING"] = round($arProperties["vote_sum"]["VALUE"] / $arProperties["vote_count"]["VALUE"]);
			}else{
				$element["RAITING"] = 0;
			}


			$offersIds = CCatalogSKU::getOffersList($element["ID"]);

			if(!empty($offersIds)){
				$offersIds = array_keys($offersIds[$element["ID"]]);

				$offersIterator = CIBlockElement::GetList(
					Array("SORT"=>"ASC"),
					Array(
						"IBLOCK_ID" => TurboExport::OFFERS_IBLOCK,
						"ID" => $offersIds,
						"ACTIVE" => "Y"
					),
					false,
					false,
					Array()
				);

				$prices = array();

				while($offer = $offersIterator->GetNext()){
					$offer["PRICE"] = CPrice::GetBasePrice($offer["ID"]);
					$prices[] = $offer["PRICE"]["PRICE"];

					$element["OFFERS"][] = $offer;
				}
				$element["MIN_PRICE"] = min($prices);


			}else{
				$element["PRICE"] =  CPrice::GetBasePrice($element["ID"]);

				$element["MIN_PRICE"] = $element["PRICE"]["PRICE"];
			}


			$seo = new \Bitrix\Iblock\InheritedProperty\ElementValues($iblockId, $element["ID"]);

			$element["SEO"] = $seo->getValues();

        	$result .= $this->view("iblocks/".$template."/element",$element);
        }
        return $result;
    }


    public function exportSectionFromIblock($iblockId, $template)
    {
    	if(!CModule::IncludeModule("iblock"))
            return false;

        $sectionIterator = CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"IBLOCK_ID" => $iblockId,
				"ACTIVE" => "Y"
			),
		    false,
			Array(
				"UF_*"
			),
			false
		);


		while($section = $sectionIterator->GetNext()){

			if(!empty($section["DETAIL_PICTURE"])){
				$section["DETAIL_PICTURE"] = CFile::GetPath($section["DETAIL_PICTURE"]);
			}
			$section["ELEMENTS"] = $this->getSectionElements($iblockId, $section["ID"]);

			$result .= $this->view("iblocks/".$template."/section", $section);
		}

		return $result;
    }

    public function getSectionElements($iblockId,$sectionId)
    {
    	if(!CModule::IncludeModule("iblock"))
            return false;

        $elementIterator = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
            	"IBLOCK_ID" => $iblockId,
            	"IBLOCK_SECTION_ID" => $sectionId,
            	"ACTIVE" => "Y"
            ),
            false,
            false,
            Array()
        );

        while($elementHandler = $elementIterator->GetNextElement()){
        	$element = $elementHandler->GetFields();
        	$element["PROPERTIES"] = $elementHandler->GetProperties();
        	$elements[] = $element;
        }

        return $elements;
    }

    public function view($template, $data = false)
    {
    	$data["SERVER_NAME"] = $this->serverName;
    	$file = $_SERVER["DOCUMENT_ROOT"].TurboExport::TEMPLATE_FOLDER.'/'.$template.".php";


    	

    	if(file_exists($file)){
    		ob_start();
    		include($file);
    		return ob_get_clean();
    	}else{
    		return false;
    	}
    }
}


$export = new TurboExport();

$export->export();