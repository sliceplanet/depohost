<?

/**
 * Полезные функции для Битрикса
 */
class CUsefull
{
  function __construct()
  {
    CModule::IncludeModule('sale');
  }

  /**
   * Преобработчик массива пользователя, если вместо массива пользователя передается его ID, то получаем массив
   *
   * @param int|array $arUser
   */
  public static function prepareArUser(&$arUser)
  {
    if (!is_array($arUser) && intval($arUser) > 0)
    {
      $resUser = \CUser::GetByID($arUser);
      $arUser = $resUser->Fetch();
    }
  }


  /**
   * Clean double spaces and returns in strings
   *
   * @param string $string
   *
   * @return string
   */
  public static function cleanString($string)
  {
    $string = preg_replace("/[ ]+/i", ' ', $string);
    $string = preg_replace("/(\r\n)+[ ]*/i", "\r\n", $string);
    $string = preg_replace("/(\n+[ ]*)+/i", "\n", $string);

    return $string;
  }

  /**
   * Возвращает ключ элемента массива $array у которого элемент с ключем $field равен $value
   *
   * Example:
   * $array = array('PROPERTY_222'=>array('ID'=>12,'CODE'=>'PROP_CODE'));
   * $field = 'CODE'
   * $value = 'PROP_CODE'
   *
   * return 'PROPERTY_222'
   *
   * @param array $array
   * @param string|int $field
   * @param mixed $value
   *
   * @return false|mixed
   */
  public static function getKeyByFieldValue($array, $field, $value)
  {
    foreach ($array as $key => $arFields)
    {
      if (array_key_exists($field, $arFields) && $arFields[$field]==$value)
      {
        return $key;
      }
    }

    return false;
  }

  /**
   * Возвращает элемент массива $array у которого элемент с ключем $field равен $value
   *
   * Example:
   * $array = array('PROPERTY_222'=>array('ID'=>12,'CODE'=>'PROP_CODE'));
   * $field = 'CODE'
   * $value = 'PROP_CODE'
   *
   * return 'PROPERTY_222'
   *
   * @param array $array
   * @param string|int $field
   * @param mixed $value
   *
   * @return false|mixed
   */
  public static function getByFieldValue($array, $field, $value)
  {
    foreach ($array as $key => $arFields)
    {
      if (array_key_exists($field, $arFields) && $arFields[$field]==$value)
      {
        return $arFields;
      }
    }

    return false;
  }


  public static function arrayKeysUpper($arImage)
  {
    $newKeys = array_map('strtoupper',array_keys($arImage));

    return array_column($newKeys,array_values($arImage));
  }

  // Получаем текущую корзину пользователя
  public function getBasket()
  {
    $arID = array();
    $arBasketItems = array();

    $dbBasketItems = CSaleBasket::GetList(array(
        "NAME" => "ASC",
        "ID" => "ASC"
    ), array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ), false, false, array(
        "ID",
        "CALLBACK_FUNC",
        "MODULE",
        "PRODUCT_ID",
        "QUANTITY",
        "PRODUCT_PROVIDER_CLASS"
    ));
    while ($arItems = $dbBasketItems->Fetch())
    {
      if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
      {
        CSaleBasket::UpdatePrice($arItems["ID"], $arItems["CALLBACK_FUNC"], $arItems["MODULE"], $arItems["PRODUCT_ID"],
            $arItems["QUANTITY"], "N", $arItems["PRODUCT_PROVIDER_CLASS"]);
        $arID[] = $arItems["ID"];
      }
    }
    if (!empty($arID))
    {
      $dbBasketItems = CSaleBasket::GetList(array(
          "NAME" => "ASC",
          "ID" => "ASC"
      ), array(
          "ID" => $arID,
          "ORDER_ID" => "NULL"
      ), false, false, array(
          "ID",
          "CALLBACK_FUNC",
          "MODULE",
          "PRODUCT_ID",
          "QUANTITY",
          "DELAY",
          "CAN_BUY",
          "PRICE",
          "WEIGHT",
          "PRODUCT_PROVIDER_CLASS",
          "NAME"
      ));
      while ($arItems = $dbBasketItems->Fetch())
      {
        $arBasketItems[] = $arItems;
      }
    }

    return $arBasketItems;
  }  // END method GetBasket

  // Добавление сразу нескольких товаров в корзину
  public function addBasketProducts($arProducts)
  {
    foreach ($arProducts as $key => $arProduct)
    {
      $arProduct['LID'] = SITE_ID;
      unset($arProduct['ID']);
      $arBasketIds[] = CSaleBasket::Add($arProduct);
    }

    return $arBasketIds;
  }

  /* окончание
   0 изображений  variants[0]
   1 изображение  variants[1]
   2-3-4 изображения variants[2]
   5-6-7-8-9-10 изображений variants[0]
   11-12-13-14-15-16-17-18-19-20 - человек
   */
  public static function endOfWord($str, $variants)
  {
    $last = substr($str, strlen($str) - 1, 1);
    $last2 = substr($str, strlen($str) - 2, 2);
    $end = $variants[2];
    if ($last == 0 || ($last >= 5 && $last <= 9)) $end = $variants[0];
    if ($last == 1) $end = $variants[1];
    if ($last >= 2 && $last <= 4) $end = $variants[2];
    if ($last2 >= 12 && $last2 <= 14 && count($variants) > 3) $end = $variants[3];

    return $end;
  }

  /**
   * Косячный метод, CIBlockElement::SetPropertyValuesEx сам по себе добавляет еще одно значение свойству
   * Добавляет значение в множественное свойство
   *
   * @params int $element_id
   * @params int $iblock_id
   * @params array $ar_property_values
   *          - массив свойств и значений array(<PROPERTY_CODE>=>array(<PROPERTY_VALUES>))
   *
   * @return boolean
   */
  public static function addMultiPropertyValue($element_id, $iblock_id, $ar_property_values)
  {
    foreach ($ar_property_values as $code => $ar_property)
    {
      \Bitrix\Main\Loader::includeModule('iblock');
      // Получаем уже имеющиеся значения свойств
      $res = \CIBlockElement::GetProperty($iblock_id, $element_id, array('sort' => 'asc'),
          array('CODE' => $code, 'EMPTY' => 'N'));
      $ar_property_value = array();
      while ($ar = $res->Fetch())
      {
        $ar_property_value[] = $ar['VALUE'];
      }
      $ar_property_value = array_merge($ar_property_value, $ar_property);

      return \CIBlockElement::SetPropertyValuesEx($element_id, $iblock_id, array($code => $ar_property_value));
    }

  }

  /**
   * Удаляем из массива ключи с ~
   *
   * @param array $array
   *
   * @return array
   */
  public static function cleanTilda(&$array)
  {
    $ar = array();
    foreach ($array as $key => $value)
    {
      if (strpos($key, '~') === false)
      {
        $ar[$key] = $value;
      }
    }
    $array = $ar;

    return $ar;
  }

  public static function clearNull(&$array)
  {
    $array = array_diff($array, array(null));
  }

  /**
   * Create array keys are values by column
   *
   * @param array $array
   * @param string $column
   *
   * @return array|bool
   */
  public static function arrayColumnKeys($array, $column)
  {
    if (!is_array($array))
    {
      return false;
    }

    return array_combine(array_column($array, $column), $array);
  }

  public static function localRedirectJS($url, $timeOut = 1)
  {
    $url = urldecode($url);
    $timeOut *= 1000;
    echo "
<script>
  setTimeout(function(){
    window.location.href = '$url';
  },$timeOut);
</script>
    ";
  }

  public static function redirectJS($url, $timeOut = 1)
  {
    self::localRedirectJS($url, $timeOut);
  }

  /**
   * Insert element $insertValueArray in array $input before element with key $refKey
   *
   * @param array $input
   * @param string $refKey
   * @param array $insertValueArray
   *
   * @param bool $isAssociative Is array associative?
   * @return array|bool
   */
  public static function arrayInsertBefore(array $input, $refKey, $insertValueArray, $isAssociative = true)
  {

    if (!array_key_exists($refKey,$input))
    {
      return $input;
    }

    $keys = array_keys($input);
    $index = array_search($refKey, $keys);

    if ($isAssociative)
    {
      $result = array_slice($input, 0, $index - 1, true)+
          $insertValueArray+
          array_slice($input, $index - 1, null, true)
      ;
    }
    else
    {
      $result = array_merge(
          array_slice($input, 0, $index - 1, true),
          $insertValueArray,
          array_slice($input, $index - 1, null, true)
      );
    }

    return $result;
  }

  /**
   * Insert element $insertValueArray in array $input after element with key $refKey
   *
   * @param array $input
   * @param string $refKey
   * @param array $insertValueArray
   *
   * @param bool $isAssociative Is array associative?
   * @return array|bool
   */
  public static function arrayInsertAfter(array $input, $refKey, $insertValueArray, $isAssociative = true)
  {

    if (!array_key_exists($refKey,$input))
    {
      return $input;
    }

    $keys = array_keys($input);
    $index = array_search($refKey, $keys);
    if ($isAssociative)
    {
      $result = array_slice($input, 0, $index + 1, true)+
          $insertValueArray+
          array_slice($input, $index + 1, null, true)
      ;
    }
    else
    {
      $result = array_merge(
          array_slice($input, 0, $index + 1, true),
          $insertValueArray,
          array_slice($input, $index + 1, null, true)
      );
    }



    return $result;

  }


  /**
   * Move element $targetKey before element with key $refKey
   *
   * @param array $input
   * @param string $refKey
   * @param string $targetKey
   *
   * @param bool $isAssociative Is array associative?
   * @return array|bool
   */
  public static function arrayMoveBefore(array $input, $refKey, $targetKey, $isAssociative = true)
  {

    if (!array_key_exists($refKey,$input) || !array_key_exists($targetKey,$input))
    {
      return $input;
    }

    $targetElement = $input[$targetKey];
    unset($input[$targetKey]);
    $result =  static::arrayInsertBefore($input,$refKey,array($targetKey=>$targetElement),$isAssociative);

    return $result;
  }

  /**
   * Move element $targetKey  after element with key $refKey
   *
   * @param array $input
   * @param string $refKey
   * @param string $targetKey
   *
   * @param bool $isAssociative Is array associative?
   * @return array|bool
   */
  public static function arrayMoveAfter(array $input, $refKey, $targetKey, $isAssociative = true)
  {

    if (!array_key_exists($refKey,$input) || !array_key_exists($targetKey,$input))
    {
      return $input;
    }

    $targetElement = $input[$targetKey];
    unset($input[$targetKey]);
    $result =  static::arrayInsertAfter($input,$refKey,array($targetKey=>$targetElement),$isAssociative);

    return $result;

  }
  /**
   * Sort array $input by key $sortByKey
   *
   * @param array $input
   * @param $sortByKey
   * @param int $sortOrder
   * @param int $sortType
   *
   * @return array
   */
  public static function arraySortByKey(array $input, $sortByKey, $sortOrder = SORT_ASC, $sortType = SORT_STRING){
    $arSort = array();
    foreach ($input as $key => $value)
    {
      $arSort[$key] = $value[$sortByKey];
    }
    array_multisort($arSort, $sortOrder, $sortType, $input);

    return $input;
  }

  public static function stepperScript($count = 100,$timeout = 2)
  {
    $page = 1;
    if (intval($_GET['page']) > 0)
    {
      $page = intval($_GET['page']);
    }
    $limit = ['offset'=>($page-1)*$count,'limit'=>$count];
    $page++;
    global $APPLICATION;
    $newPage = $APPLICATION->GetCurPageParam("page=$page",array('page'));
    ?>
    <script>
      setTimeout(function(){
        window.location.href = '<?=$newPage?>';
      },<?=(intval($timeout)*1000)?>);
    </script>
    <?
    return $limit;
  }

  /**
   * @param array $array
   * @param array|string $omit
   * @return array
   */
  public static function omit($array, $omit)
  {
    if (!is_array($omit)) {
      $omit = array($omit);
    }

    return array_diff_key($array,array_flip($omit));
  }

}  // END class Useful


?>
