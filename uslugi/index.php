<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Наши услуги");
$APPLICATION->SetPageProperty("title", "Наши услуги | ArtexTelecom");
$APPLICATION->SetPageProperty("description", "Наши услуги. ArtexTelecom - предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
?>

<section class="cards category-menu-with-text">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
      	<h1 class="text-center mb-25">Наши услуги</h1>
        <div class="category-menu">
          <? $APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/catalog_icons_with_text.php"),
                Array(),
                Array("MODE" => "html")
              ); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>