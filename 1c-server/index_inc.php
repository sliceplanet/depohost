<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	{
  die(); 
} 
?>
 
	
<div class="page-top-desc">
	<div class="container">
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/server1c.png" />
			</div>
			<h1>Выделенные сервера для 1С</h1>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>
			<div class="ptd-content">
				<div class="ptd-content__left">
					<p>Продукты 1С — очевидный выбор для многих предприятий по всей России. Эти решения оптимизируют работу бухгалтерии и управление бизнесом. Однако для их развертывания необходимо производительное оборудование, а также настройка защищенных каналов доступа. Компания Artex Telecom экономит ваши средства и предлагает услуги аренды сервера для 1С. Мы даем клиентам возможность выбрать из списка оптимальных конфигураций, получивших высокие баллы в тесте Гилева, или же разработать собственную.</p>
				</div>
				<div class="ptd-content__right">
					<img src="/images/2020/logo1c.png" />
				</div>
			</div>
		</div>
	</div>
</div>


<div class="section-graybg">

  <div class="container">
<?
global $sFilter;
$sFilter['!ID'] = [249,250];
?>
    <div class="server-1c server-1c-table">
      <? $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "vds-turnkey2020",
        Array(
          "ADD_SECTIONS_CHAIN" => "N",
          "CACHE_GROUPS" => "Y",
          "CACHE_TIME" => "36000000",
          "CACHE_TYPE" => "A",
          "COUNT_ELEMENTS" => "Y",
          "IBLOCK_ID" => "52",
          "IBLOCK_TYPE" => "xmlcatalog",
          "SECTION_CODE" => "",
          "SECTION_FIELDS" => array(),
          "SECTION_ID" => $_REQUEST["SECTION_ID"],
          "SECTION_URL" => "",
          "SECTION_USER_FIELDS" => array(),
          "SHOW_PARENT_NAME" => "Y",
          "TOP_DEPTH" => "2",
          "VIEW_MODE" => "LINE",
		  'FILTER_NAME' => 'sFilter',
        )
      ); ?>
    </div>
  </div>
  
<section class="page-section wug-section">
	
	<h2 class="page-section__h">Что вы получаете</h2>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Подбор оборудования с учетом нагрузки</div>
					<p>Мы поможем подобрать оптимальный вариант с точки зрения цены и производительности, а при необходимости разработаем индивидуальное решение. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_4.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Доступ к системе 24/7/365</div>
					<p>Вы и ваши сотрудники сможете получить доступ к программам и данным в любое время и с любого устройства. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_7.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатный и безлимитный трафик</div>
					<p>Мы не ограничиваем входящий и исходящий трафик для ваших проектов. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_2.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатные резервные копии</div>
					<p>Мы предоставляем пользователям 100GB для создания бэкапа данных на весь срок аренды. Хранилище не находится на основном сервере, поэтому информация на нем сохранится даже в случае критического сбоя.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_5.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатная настройка и установка</div>
					<p>При аренде сервера под 1С наши сотрудники бесплатно настроят оборудование и установят необходимые программы. </p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_8.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Хранение и защита ваших данных в соответствии с ФЗ 152</div>
					<p>Наши дата-центры находятся в Москве и круглосуточно охраняются. Наше оборудование обеспечивает защиту от несанкционированного доступа, шифрование передаваемых данных, 4-й класс защиты.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_3.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущества SAS и SSD</div>
					<p>Диски этих типов обеспечивают высокое быстродействие системы и максимальную отдачу дисковой подсистемы.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_6.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">OS Windows Server 2008/2012/2016</div>
					<p>Благодаря возможности установки этих операционных систем вам не придется привыкать к новому интерфейсу. Одну лицензию на ОС вы получаете в подарок при аренде виртуального сервера для 1С. Дополнительные мы добавляем по вашему запросу день в день.</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_9.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Снижение ваших затрат</div>
					<p>Аренда сервера значительно дешевле покупки, установки, настройки и содержания нового оборудования. Кроме того, мы предлагаем клиентам возможность масштабирования вычислительных мощностей. </p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>
  
  
</div>

<section class="page-block services-consist-block services1c-consist-block">

	<div class="container">

		<div class="mpb_h">К аренде 1С-сервера предоставляем сервис</div>

		<ul class="services-consist__list">
			<li>Первоначальная настройка сервера, приложений и программ 1С</li>
			<li>Установка SSL сертификатов на опубликованные ресурсы RDP или HTTPS</li>
			<li>Настройка резервного копирования (локально или на внешний FTP )</li>
			<li>Публикация 1С через Web</li>
			<li>Настройка сервера печати</li>
			<li>Доступ к серверу по протоколу RDP</li>
			<li>Настройка VPN соединения</li>
			<li>Создание системных пользователей и раздача прав доступа</li>
		</ul>

	</div>

</section>


<section class="page-block advanatages-block-1">
	
	<div class="container">
		<h2 class="page-section__h">Почему стоит арендовать<br> удаленный сервер для 1С?</h2> 
		
		<div class="flex-row jcsp">
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/1.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Вы обеспечиваете удобный доступ для всех сотрудников</div>
					<p>С помощью хостинга 1С вы можете создать единую рабочую среду, в которой могут работать все ваши специалисты. Они смогут получить доступ к информации с любого устройства, в том числе мобильного. </p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/2.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Вы экономите средства</div>
					<p>Аренда 1С-сервера — наиболее доступный вариант усовершенствования с коробочной версии до версии «в облаке». При этом при смене версий, в том числе с популярной 1С: «Бухгалтерия 8.3», вы не теряете созданные ранее файлы.  </p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/3.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Вы получаете гарантию надежности и безопасности</div>
					<p>Все сервера под 1С устанавливаются в наших центрах обработки данных в Москве. В отличие от объектов, расположенных в Европе, они обеспечивают более низкий пинг и быстрый доступ. Все ЦОДы соответствуют стандарту безопасности Tier III и круглосуточно охраняются. </p>
				</div>
			</div>
		</div>
		
		<h3 class="page-section__h">Аренда дополнительных лицензий</h3> 
		<p>Решения 1С, в том числе наиболее популярные «1С: Бухгалтерия» и «1С: Предприятие», распространяются только в платных версиях. Использование «взломанных» программ является нарушением закона. При этом некоторые дополнительные продукты 1С требуют покупки дополнительных лицензий. К их числу относятся серверы баз данных и терминальные серверы. Вы можете купить эти лицензии, но компания Artex Telecom предлагает вам возможность арендовать их. В этом случае вы сможете оперативно начать пользоваться новым функционалом, а при необходимости отказаться от него в любой момент. </p>
		<p>Кроме того, в услугу аренды уже включена стоимость лицензии на выбранную вами операционную систему.</p>

	</div>
	
</section>

<section class="page-block advanatages-block-2">
	
	<div class="page-section__h">Преимущества аренды сервера у нас</div>
	
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item2">
				<img src="/images/2020/adv1.png" />
				<p>Безопасноe хранение и передача инфо-данных по защищенным каналам в рамках виртуальной сети (VPN)</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv2.png" />
				<p>Круглосуточный доступ к серверу из любого места с возможностью выхода в Интернет</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv3.png" />
				<p>Возможность самостоятельного выбора и установки требуемого ПО</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv4.png" />
				<p>Обеспечение надежной технической поддержки в круглосуточном режиме</p>
				
			</div>
		</div>
		
	</div>
	
</section>

<section style="padding: 50px 0;">
	<div class="container">
		<h3 class="page-section__h">Как мы обеспечиваем безопасность ваших данных?</h3>
		<div class="flex-row jcsp">
			<div class="adv_item">
				<div>
					<p class="bold checked-text" style="min-height: 66px;">Создаем резервные копии и предоставляем 100 GB для бэкапов данных</p>
					<p class="text-justify">График создания резервных копий вы можете согласовать с нашими сотрудниками. Если вам необходима аренда сервера 1С, но у вас уже есть база, с которой вы работаете, мы можем развернуть ее на новом оборудовании.</p>
				</div>
			</div>
			<div class="adv_item">
				<div>
					<p class="bold checked-text" style="min-height: 66px;">Настраиваем доступ через VPN</p>
					<p class="text-justify">Благодаря этому исключается несанкционированный доступ.</p>
				</div>
			</div>
			<div class="adv_item">
				<div>
					<p class="bold checked-text" style="min-height: 66px;">Создаем системных пользователей и раздаем права доступ</p>
					<p class="text-justify">Благодаря этому исключается несанкционированный  доступ сотрудников к данным.</p>
				</div>
			</div>
		</div>
	</div>
</section>
  
<section class="faq-block">
    <div class="container">
        <h3 class="hsw-block__h">Вопрос-ответ</h3>
        <div class="faq-items" itemscope itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Могут ли я протестировать работу удаленного сервера 1С до заказа услуг?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, мы предоставляем такую возможность всем новым клиентам.  Длительность тестового периода составляет 5 дней. После этого вы можете продолжить пользоваться нашими услугами или отказаться от них. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">В чем преимущества выделенного сервера для 1С перед виртуальным?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Выделенный сервер — это физическое оборудование, расположенное в нашем ЦОДе. Вы являетесь его единственным пользователем, а значит все его ресурсы находятся в вашем распоряжении. На одном физическом сервере может быть размещено несколько виртуальных. Это значит, что ресурсы оборудования делятся между несколькими пользователями.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Возможно ли изменение характеристик выделенного сервера?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. Если текущих мощностей недостаточно, мы можем добавить дополнительные хранилища данных и оперативную память, установить более производительные процессоры. Масштабирование выделенных серверов обычно происходит в течение 1 рабочего дня.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Какие решения можно развернуть на сервере?</span></a>
                    <p class="faq-item__desc" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Мы предоставляем лицензии на «1С: Бухгалтерия» и «1С: Предприятие», но по вашему заказу можем развернуть любое другое решение. </span></p>
                </div>
            </div>
        </div>
    </div>
</section>
  
<? $APPLICATION->IncludeFile(
	"/include/mainpage/our-tech.php"
  ); ?>
  
  
<div class="help-conf-block">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального выделенного сервера для 1С'
      )
    ); ?>
  </div>
  
</div>