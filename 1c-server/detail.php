<?php
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 18.10.15
 * Time: 12:12
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Сервер для 1С - все конфигурации протестированы");
$APPLICATION->SetTitle("");
?>
<div class="container n-vds-dedic">
  <div class="row">
    <div class="col-xs-12">
      <? $APPLICATION->IncludeComponent("bitrix:catalog.element", "style-v2", Array(
        "LABEL_PROP" => "-",  // Свойство меток товара
        "PRODUCT_SUBSCRIPTION" => "N",  // Разрешить оповещения для отсутствующих товаров
        "SHOW_DISCOUNT_PERCENT" => "N",  // Показывать процент скидки
        "SHOW_OLD_PRICE" => "N",  // Показывать старую цену
        "SHOW_MAX_QUANTITY" => "N",  // Показывать общее количество товара
        "DISPLAY_COMPARE" => "N",  // Разрешить сравнение товаров
        "MESS_BTN_BUY" => "Заказать",  // Текст кнопки "Купить"
        "MESS_BTN_ADD_TO_BASKET" => "Заказать",  // Текст кнопки "Добавить в корзину"
        "MESS_BTN_SUBSCRIBE" => "Подписаться",  // Текст кнопки "Уведомить о поступлении"
        "MESS_BTN_COMPARE" => "Сравнение",  // Текст кнопки "Сравнение"
        "MESS_NOT_AVAILABLE" => "Нет в наличии",  // Сообщение об отсутствии товара
        "USE_VOTE_RATING" => "N",  // Включить рейтинг товара
        "USE_COMMENTS" => "N",  // Включить отзывы о товаре
        "BRAND_USE" => "N",  // Использовать компонент "Бренды"
        "IBLOCK_TYPE" => "xmlcatalog",  // Тип инфоблока
        "IBLOCK_ID" => "52",  // Инфоблок
        "ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],  // ID элемента
        "ELEMENT_CODE" => "",  // Код элемента
        "SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
        "SECTION_CODE" => "",  // Код раздела
        "SECTION_URL" => "#SITE_DIR#1c-server/",  // URL, ведущий на страницу с содержимым раздела
        "DETAIL_URL" => "#SITE_DIR#1c-server/#ELEMENT_ID#/",  // URL, ведущий на страницу с содержимым элемента раздела
        "SECTION_ID_VARIABLE" => "SECTION_ID",  // Название переменной, в которой передается код группы
        "CHECK_SECTION_ID_VARIABLE" => "Y",  // Использовать код группы из переменной, если не задан раздел элемента
        "SET_TITLE" => "Y",  // Устанавливать заголовок страницы
        "SET_BROWSER_TITLE" => "Y",  // Устанавливать заголовок окна браузера
        "BROWSER_TITLE" => "META_TITLE",  // Установить заголовок окна браузера из свойства
        "SET_META_KEYWORDS" => "Y",  // Устанавливать ключевые слова страницы
        "META_KEYWORDS" => "CONFIGURATION",  // Установить ключевые слова страницы из свойства
        "SET_META_DESCRIPTION" => "Y",  // Устанавливать описание страницы
        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
        "SET_STATUS_404" => "Y",  // Устанавливать статус 404, если не найдены элемент или раздел
        "ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
        "ADD_ELEMENT_CHAIN" => "Y",  // Включать название элемента в цепочку навигации
        "PROPERTY_CODE" => array(  // Свойства
                                   0 => "CONFIGURATION",
        ),
        "OFFERS_LIMIT" => "0",  // Максимальное количество предложений для показа (0 - все)
        "PRICE_CODE" => array(  // Тип цены
                                0 => "Розничная",
        ),
        "USE_PRICE_COUNT" => "N",  // Использовать вывод цен с диапазонами
        "SHOW_PRICE_COUNT" => "1",  // Выводить цены для количества
        "PRICE_VAT_INCLUDE" => "Y",  // Включать НДС в цену
        "PRICE_VAT_SHOW_VALUE" => "N",  // Отображать значение НДС
        "BASKET_URL" => "/personal/basket.php",  // URL, ведущий на страницу с корзиной покупателя
        "ACTION_VARIABLE" => "action",  // Название переменной, в которой передается действие
        "PRODUCT_ID_VARIABLE" => "id",  // Название переменной, в которой передается код товара для покупки
        "USE_PRODUCT_QUANTITY" => "Y",  // Разрешить указание количества товара
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",  // Название переменной, в которой передается количество товара
        "ADD_PROPERTIES_TO_BASKET" => "Y",  // Добавлять в корзину свойства товаров и предложений
        "PRODUCT_PROPS_VARIABLE" => "prop",  // Название переменной, в которой передаются характеристики товара
        "PARTIAL_PRODUCT_PROPERTIES" => "Y",
        // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
        "PRODUCT_PROPERTIES" => "",  // Характеристики товара
        "LINK_IBLOCK_TYPE" => "",  // Тип инфоблока, элементы которого связаны с текущим элементом
        "LINK_IBLOCK_ID" => "",  // ID инфоблока, элементы которого связаны с текущим элементом
        "LINK_PROPERTY_SID" => "",  // Свойство, в котором хранится связь
        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
        // URL на страницу, где будет показан список связанных элементов
        "CACHE_TYPE" => "A",  // Тип кеширования
        "CACHE_TIME" => "36000000",  // Время кеширования (сек.)
        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
        "USE_ELEMENT_COUNTER" => "Y",  // Использовать счетчик просмотров
        "HIDE_NOT_AVAILABLE" => "N",  // Не отображать товары, которых нет на складах
        "CONVERT_CURRENCY" => "N",  // Показывать цены в одной валюте
      ),
        false
      ); ?>
    </div>
  </div>
</div>
<?php if(isset($_COOKIE['seodev']))
{
    echo "<pre>".print_r($arResult,true)."</pre>";
} ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>
