<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	{
  die(); 
} 
?>
 
	
<div class="page-top-desc">
	<div class="container">
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/server1c.png" />
			</div>
			<div class="like_h1">Виртуальный сервер для 1С</div>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>
			<div class="ptd-content">
				<div class="ptd-content__left">
<p>Мы гарантируем надежное и безопасное решение, для организации удобной и простой работы Вашей бухгалтерии. Приобретая 1C сервер в аренду, компания избавляет себя от необходимости покупать и устанавливать серверное оборудование, покупать лицензии Microsoft которые можно арендовать.</p>

<p>Вы получаете централизованное, функциональное решение, обеспечиваемое профессиональной технической поддержкой. С ее помощью можно эффективно организовывать работу сотрудников предприятия в едином инфо-пространстве.</p>
				</div>
				<div class="ptd-content__right">
					<img src="/images/2020/logo1c.png" />
				</div>
			</div>
		</div>
	</div>
</div>


<div class="section-graybg">

  <div class="container">

    <div class="server-1c server-1c-table">
      <? $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "vds-turnkey2020",
        Array(
          "ADD_SECTIONS_CHAIN" => "N",
          "CACHE_GROUPS" => "Y",
          "CACHE_TIME" => "36000000",
          "CACHE_TYPE" => "A",
          "COUNT_ELEMENTS" => "Y",
          "IBLOCK_ID" => "52",
          "IBLOCK_TYPE" => "xmlcatalog",
          "SECTION_CODE" => "",
          "SECTION_FIELDS" => array(),
          "SECTION_ID" => $_REQUEST["SECTION_ID"],
          "SECTION_URL" => "",
          "SECTION_USER_FIELDS" => array(),
			'SID' => [249,250],
          "SHOW_PARENT_NAME" => "Y",
          "TOP_DEPTH" => "2",
          "VIEW_MODE" => "LINE"
        )
      ); ?>
    </div>
  </div>
  
<section class="page-section wug-section">
	
	<div class="page-section__h">Что вы получаете</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_1.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Работа с большой нагрузкой</div>
					<p>Все конфигурации протестированы, мы знаем как правильно рассчитать нагрузку на сервер, отвечающую «цена/качество»</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_4.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Доступ 24/7</div>
					<p>Бесспорное преимущество круглосуточный доступ из любой точки мира, офиса, дома, филиала в поездке или командировки</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_7.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатный безлимитный трафик</div>
					<p>Вы получаете на весь срок аренды бесплатный, безлимитный трафик для Вашего проекта</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_2.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">100 GB для резервного копирования</div>
					<p>Каждому клиенту Бесплатно предоставляется 100GB места для резервного копирования на отдельном FTP сервере на весь срок аренды</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_5.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатная настройка и установка</div>
					<p>Мы предлагаем больше конкурентов: Вы можете рассчитывать на бесплатную настройку сервера и установку программ</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_8.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Хранение и защита ваших данных фз 152</div>
					<p>Защита от несанкционированного доступа, передача данных по шифрованным каналам, 3-я и 4-я категория, 4-й класс защиты</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_3.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущество SAS и SSD</div>
					<p>Для получения высокого быстродействия используйте диски SAS, для максимальной отдачи дисковой подсистемы выбирайте SSD</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_6.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Windows Server 2008/2012/2016</div>
					<p>Установленную и активированную лицензионную OS Windows Server (доп. лицензии можно добавлять по запросу день в день)</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/wug/icon_9.png" />
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Снижение ваших затрат</div>
					<p>Аренда сервера для 1С это экономия ваших средств не только на покупке, обслуживании и администрировании сервера</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>
  
  
</div>

<section class="page-block services-consist-block services1c-consist-block">

	<div class="container">

		<div class="mpb_h">К аренде 1С-сервера предоставляем сервис</div>

		<ul class="services-consist__list">
			<li>Первоначальная настройка сервера, приложений и программ 1С</li>
			<li>Установка SSL сертификатов на опубликованные ресурсы RDP или HTTPS</li>
			<li>Настройка резервного копирования (локально или на внешний FTP )</li>
			<li>Публикация 1С через Web</li>
			<li>Настройка сервера печати</li>
			<li>Доступ к серверу по протоколу RDP</li>
			<li>Настройка VPN соединения</li>
			<li>Создание системных пользователей и раздача прав доступа</li>
		</ul>

	</div>

</section>


<section class="page-block advanatages-block-1">
	
	<div class="page-section__h">Аренда удаленного 1С-сервера<br>для вашей компании</div>
	
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/1.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это удобно</div>
					<p>Аренда терминального сервера является актуальным IT-решением, для удобной работы в удобном программном окружении. Базы данных компании всегда доступны для сотрудников в любом месте и с любого мобильного, а также стационарного устройства.</p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/2.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это выгодно</div>
					<p>Мы предлагаем выгодные условия аренды. На сегодняшний день это один из наиболее простых способов перехода с коробочной версии 1С-продукта на облачную, с возможностью полного переноса собственных баз данных.</p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/3.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Это надежно</div>
					<p>Наш сервер расположен в собственном помещении с обеспечением круглосуточной охраны.</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>

<section class="page-block advanatages-block-2">
	
	<div class="page-section__h">Преимущества аренды сервера у нас</div>
	
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item2">
				<img src="/images/2020/adv1.png" />
				<p>Безопасноe хранение и передача инфо-данных по защищенным каналам в рамках виртуальной сети (VPN)</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv2.png" />
				<p>Круглосуточный доступ к серверу из любого места с возможностью выхода в Интернет</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv3.png" />
				<p>Возможность самостоятельного выбора и установки требуемого ПО</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv4.png" />
				<p>Обеспечение надежной технической поддержки в круглосуточном режиме</p>
				
			</div>
		</div>
		
	</div>
	
</section>
  
<? $APPLICATION->IncludeFile(
	"/include/mainpage/our-tech.php"
  ); ?>
  
  
<div class="help-conf-block">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального выделенного сервера для 1С'
      )
    ); ?>
  </div>
  
</div>