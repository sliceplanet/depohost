<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="server-opis">
<h2 class="subtitle subtitle-custom">Что вы получить при заказе выделенного сервера</h2>
<ul class="advantage advantage-custom">
  <li class="advantage__item advantage__item-custom">
    <div class="icon-wrapper">
      <div class="advantage__item_icon advantage__item_icon-new performance"></div>
    </div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Неограниченный трафик на скорости до 1000 МБит.c</div>
      <div class="advantage__item_description">Минимальная задержка пакета для высоконагруженных проектов и популярных сайтов </div>
    </div>
  </li>
  <li class="advantage__item advantage__item-custom">
    <div class="icon-wrapper">
      <div class="advantage__item_icon advantage__item_icon-new progress"></div>
    </div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Создание сервера на заказ </div>
      <div class="advantage__item_description">Подбор конфигурации сервера из оборудования, которое вам необходимо</div>
    </div>
  </li>
  <li class="advantage__item advantage__item-custom">
    <div class="icon-wrapper">
      <div class="advantage__item_icon advantage__item_icon-new settings"></div>
    </div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">ISPManager для выделенного сервера</div>
      <div class="advantage__item_description">Многофункциональная и удобная панель управления серверным ПО, приложениями и выделенным сервером. Доступ с мобильных устройств</div>
    </div>
  </li>
  <li class="advantage__item advantage__item-custom">
    <div class="icon-wrapper">
      <div class="advantage__item_icon advantage__item_icon-new calendar"></div>
    </div>
    <div class="advantage__item_teaser">
      <div class="advantage__item_name">Тестовый доступ на 7 дней</div>
      <div class="advantage__item_description">Большинство серверов имеются в наличии и доступны вам для тестирования</div>
    </div>
  </li>
</ul>
</div>