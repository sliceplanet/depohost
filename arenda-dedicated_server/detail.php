<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Выделенный сервер");
$APPLICATION->SetTitle("Выделенный сервер");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic-media.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');

$curPage = $APPLICATION->GetCurPage(false);
if (!preg_match('/^\/arenda-dedicated_server\/(\\d+)\/$/', $curPage))
{
  \CHTTP::SetStatus('404 Not Found');
}
?>
  <?
  $APPLICATION->IncludeComponent(
	"bitrix:catalog.element", "style-v2-dedic2020", Array(
	"TEMPLATE_THEME" => "blue",
	"ADD_PICT_PROP" => "-",
	"LABEL_PROP" => "-",
	"DISPLAY_NAME" => "Y",
	"DETAIL_PICTURE_MODE" => "IMG",
	"ADD_DETAIL_TO_SLIDER" => "N",
	"DISPLAY_PREVIEW_TEXT_MODE" => "E",
	"PRODUCT_SUBSCRIPTION" => "N",
	"SHOW_DISCOUNT_PERCENT" => "N",
	"SHOW_OLD_PRICE" => "N",
	"SHOW_MAX_QUANTITY" => "N",
	"ADD_TO_BASKET_ACTION" => array("BUY"),
	"SHOW_CLOSE_POPUP" => "N",
	"MESS_BTN_BUY" => "Купить",
	"MESS_BTN_ADD_TO_BASKET" => "В корзину",
	"MESS_BTN_SUBSCRIBE" => "Подписаться",
	"MESS_BTN_COMPARE" => "Сравнить",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",
	"USE_VOTE_RATING" => "N",
	"USE_COMMENTS" => "N",
	"BRAND_USE" => "N",
	"IBLOCK_TYPE" => "xmlcatalog",
	"IBLOCK_ID" => "21",
	"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
	"ELEMENT_CODE" => "",
	"SECTION_ID" => $_REQUEST["SECTION_ID"],
	"SECTION_CODE" => "",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"CHECK_SECTION_ID_VARIABLE" => "Y",
	"SET_TITLE" => "Y",
	"SET_BROWSER_TITLE" => "Y",
	"BROWSER_TITLE" => "META_TITLE",
	"SET_META_KEYWORDS" => "Y",
	"META_KEYWORDS" => "-",
	"SET_META_DESCRIPTION" => "Y",
	"META_DESCRIPTION" => "-",
	"SET_STATUS_404" => "Y",
	"ADD_SECTIONS_CHAIN" => "N",
	"ADD_ELEMENT_CHAIN" => "Y",
	"PROPERTY_CODE" => array("CPU", "RAM", "RAID", "HDD", "ADVANCE", "QUANTITY"),
	"OFFERS_LIMIT" => "0",
	"PRICE_CODE" => array("Розничная"),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"USE_PRODUCT_QUANTITY" => "N",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"ADD_PROPERTIES_TO_BASKET" => "Y",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"PARTIAL_PRODUCT_PROPERTIES" => "N",
	"PRODUCT_PROPERTIES" => array("CPU", "RAM", "RAID", "HDD"),
	"DISPLAY_COMPARE" => "N",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_PROPERTY_SID" => "",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"USE_ELEMENT_COUNTER" => "Y",
	"HIDE_NOT_AVAILABLE" => "N",
	"CONVERT_CURRENCY" => "N"
  ), false
  );
  ?>
  
  <?
  $APPLICATION->IncludeComponent("itin:dedicated.advance.options", "", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "50",
  ), false
  );
  ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>