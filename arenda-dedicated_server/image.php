<?
header('Content-type: image/png');

function win2uni($s)
  {
    $s = convert_cyr_string($s,'w','i'); // преобразование win1251 -> iso8859-5
    // преобразование iso8859-5 -> unicode:
    for ($result='', $i=0; $i<strlen($s); $i++) {
      $charcode = ord($s[$i]);
      $result .= ($charcode>175)?"&#".(1040+($charcode-176)).";":$s[$i];
    }
    return $result;
  }
//text
$text = win2uni($_GET['text']);
$text = strip_tags($text);
$totalsimbs = iconv_strlen($text);
$height = 45;

$text1 = substr($text, 0, 125);
$text1 = rtrim($text1, "!,.-");
$text1 = substr($text1, 0, strrpos($text1, ' '));
$totalsimbs1 = iconv_strlen($text1);

$text2 = substr($text, $totalsimbs1, 100);
$text2 = trim($text2);
if($totalsimbs > $totalsimbs1+110){
	$text2 = rtrim($text2, "!,.-");
	$text2 = substr($text2, 0, strrpos($text2, ' '));
	$height = 70;
	$totalsimbs2 = iconv_strlen($text2);
	
	$text3 = substr($text, $totalsimbs1+$totalsimbs2+1, 140);
	$text3 = trim($text3);
	if($totalsimbs > $totalsimbs1+$totalsimbs2+140){
		$text3 = rtrim($text3, "!,.-");
		$text3 = substr($text3, 0, strrpos($text3, ' '));
		$totalsimbs3 = iconv_strlen($text3);
		
		$text4 = substr($text, $totalsimbs1+$totalsimbs2+$totalsimbs3+2);
		$text4 = trim($text4);
		$height = 85;
		$totalsimbs4 = iconv_strlen($text4);
	}
}

//функция преобразование текста  в картинку
$image = imagecreate(600, $height);
    $fon = imagecolorallocate($image, 255, 255, 255);
    $text_color = imagecolorallocate($image, 86, 88, 98);
    imagettftext($image, 11, 0, 2, 20, $text_color, 'Roboto-Regular.ttf', $text1);
    imagettftext($image, 11, 0, 2, 40, $text_color, 'Roboto-Regular.ttf', $text2);
    imagettftext($image, 11, 0, 2, 60, $text_color, 'Roboto-Regular.ttf', $text3);
    imagettftext($image, 11, 0, 2, 80, $text_color, 'Roboto-Regular.ttf', $text4);
    header('Content-type: image/png');
    imagepng($image);
 ?>