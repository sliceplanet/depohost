<?
header('Content-type: image/png');
$text = strip_tags($_GET['text']);

//$height = (int) $_GET['height'];

switch ($_GET['style'])
{
    case 'typeServerHeader':
        $rgbText = array(255,255,255);
        $rgbBg = array(83,164,217);
        $fontSize = 14;
        $marginLeft = 0;
        $marginBottom = 15;
        $fontFile = 'idealist_sans.ttf';
        $width = 268;
        $height = 20;
        $text = strtoupper($text);
        break;
    case 'solutionCaption':
        $rgbText = array(0,0,0);
        $rgbBg = array(83,164,217);
        $fontSize = 12;
        $marginLeft = 0;
        $marginBottom = 12;
        $fontFile = 'idealist_sans.ttf';
        $width = 418;
        $height = 14;
        $text = strtoupper($text);
        break;
    case 'solutionPropertyName':
        $rgbText = array(0,174,247);
        $rgbBg = array(255,255,255);
        $fontSize = 9;
        $marginLeft = 0;
        $marginBottom = 10;
        $fontFile = 'idealist_sans.ttf';
        $width = 80;
        $height = 10;
        $text = strtoupper($text);
        break;
    case 'solutionPropertyNameAdvance':
        $rgbText = array(0,174,247);
        $rgbBg = array(255,255,255);
        $fontSize = 9;
        $marginLeft = 0;
        $marginBottom = 10;
        $fontFile = 'idealist_sans.ttf';
        $width = 418;
        $height = 10;
        $text = strtoupper($text);
        break;
    case 'solutionPropertyValue':
        $rgbText = array(0,0,0);
        $rgbBg = array(255,255,255);
        $fontSize = 9;
        $marginLeft = 0;
        $marginBottom = 10;
        $fontFile = 'idealist_sans.ttf';
        $width = 123;
        $height = 10;
        $text = strtoupper($text);
        break;
    case 'solutionPropertyValueAdvance':
        $rgbText = array(0,0,0);
        $rgbBg = array(255,255,255);
        $fontSize = 8;
        $marginLeft = 0;
        $marginBottom = 12;
        $fontFile = 'idealist_sans.ttf';
        $width = 418;
        $height = 32;
        $text = wordwrap($text,120);
        break;
}

//функция преобразование текста  в картинку
$image = imagecreate($width, $height);
    $fon = imagecolorallocate($image, $rgbBg[0], $rgbBg[1], $rgbBg[2]);
    $text_color = imagecolorallocate($image, $rgbText[0], $rgbText[1], $rgbText[2]);
    imagettftext($image, $fontSize, 0, $marginLeft, $marginBottom, $text_color, $fontFile, $text);
    header('Content-type: image/png');
    imagepng($image);
 ?>