<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Аренда выделенного сервера. ArtexTelecom - предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг сервера от ArtexTelecom.");
$APPLICATION->SetPageProperty("tags", "Аренда выделенного сервера - Dedicated");
$APPLICATION->SetPageProperty("NOT_SHOW_TABS", "Y");
$APPLICATION->SetPageProperty("title", "Аренда выделенного сервера в Москве | Арендовать выделенный сервер не дорого");
$APPLICATION->SetTitle("Аренда выделенного сервера");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.selectbox-m.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic-media.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/select-small.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick-theme.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.prettyCheckboxes.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scripts_bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jQueryUITouchPunch.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');
CJSCore::Init();
$step = (int)$_REQUEST['step'];
?>
   <?
   if ($_REQUEST['isAjax'] == 'Y') {
      $APPLICATION->RestartBuffer();
    }
   ?> 
  <?
  if ($step == 2 && $_REQUEST['isAjax'] == 'Y'):
    $APPLICATION->IncludeComponent(
      "itin:dedicated.advance.options",
      "v2020",
      Array(
        "IBLOCK_ID" => "50",
        "IBLOCK_TYPE" => "catalog"
      )
    );
  endif;
  ?>
  <?
  if ($_REQUEST['isAjax'] == 'Y') {
    die();
  }?>
  
<div class="main-page">
	
<div class="page-top-desc vs-top-desc">
	<div class="container">
		<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/icon_vs.png" />
			</div>
			<h1>Аренда выделенного сервера</h1>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>
			<div class="ptd-content">
<p>Аренда выделенного сервера — оптимальный вариант для крупных и развивающихся проектов. Благодаря этой услуге вы можете получить в свое распоряжение производительное оборудование без крупных денежных затрат. Вы также 
экономите на установке, настройке и содержании серверов. При необходимости вы также можете масштабировать решения в соответствии с потребностями вашего бизнеса. Artex Telecom предлагает клиентам услуги аренды выделенных серверов в нашем дата-центре в Москве. Мы гарантируем высокий аптайм и широкий выбор оборудования. </p>
				
			</div>
		</div>
	</div>
</div>


<div class="section-graybg">

	<? $APPLICATION->IncludeFile(
		"/include/mainpage/configurator.php"
	  ); 
	?>
	<? $APPLICATION->IncludeFile(
		"/include/arenda-dedicated_server/wug.php"
	  ); 
	?>

</div>


<section class="page-block services-consist-block services1c-consist-block">
	
	<div class="container">

		<div class="mpb_h">К аренде выделенного сервера предоставляем сервис</div>

		<ul class="services-consist__list">
			<li>Первоначальная настройка сервера, приложений и сервисов</li> 
			<li>Установка SSL сертификатов на опубликованные ресурсы RDP или HTTPS</li>
			<li>Настройка резервного копирования (локально или на внешний FTP )</li>
			<li>Публикация приложений через Web</li>
			<li>Настройка сервера печати</li> 
			<li>Доступ к серверу по протоколу RDP</li>
			<li>Настройка VPN соединения</li>
			<li>Создание системных пользователей и раздача прав доступа</li>
		</ul>

	</div>

</section>


<section class="page-block advanatages-block-1">
	 
	<div class="container">
		<h2 class="page-section__h mt-30">Преимущества аренды выделенных серверов</h2>
		
		<div class="flex-row jcsp">
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/1.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Удобство</div>
					<p>Вы можете получить доступ к настройке оборудования с большинства мобильных и стационарных устройств. Все вычислительные ресурсы сервера доступны только вам и не делятся с другими пользователями. </p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/2.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Выгода</div>
					<p>Вы не несете расходов, связанных с обслуживанием оборудования и содержанием в штате системных администраторов. По вашему заказу наши специалисты установят любое ПО, необходимое вам для работы. </p>
				</div>
			</div>
			<div class="adv_item">
				<div class="adv_item_left">
					<img src="/images/2020/3.png" />
				</div>
				<div class="adv_item_right">
					<div class="adv_item_h">Надежность</div>
					<p>Все наши серверы расположены в дата-центре в Москве и соответствуют классу Tier III. Оборудование подобного класса применяется также в Германии и других странах Европы, США. </p>
				</div>
			</div>
		</div>
		
		<h3 class="page-section__h mt-30">Как подобрать оптимальное оборудование?</h3>

		<div class="flex-row jcsp">
			<div class="adv_item">
				<div>
					<p class="bold text-center">Выбрать один из готовых вариантов</p>
					<p class="text-justify">Они уже протестированы нами и другими пользователями, поэтому мы можем дать полную гарантию их высокой мощности. </p>
				</div>
			</div>
			<div class="adv_item">
				<div>
					<p class="bold text-center">Собрать подходящий сервер в конструкторе</p>
					<p class="text-justify">В этом случае вы сами сможете решить, какие ресурсы для вас в приоритете. Например, вы можете выбрать более мощный процессор, уменьшив объем оперативной и долговременной памяти</p>
				</div>
			</div>
			<div class="adv_item">
				<div>
					<p class="bold text-center">Доверить выбор нашим сотрудникам</p>
					<p class="text-justify">Вам достаточно описать задачи, которые вы планируете решать с помощью выделенного сервера. Наши сотрудники подберут оптимальную конфигурацию с учетом ваших потребностей. </p>
				</div>
			</div>
		</div>

		<p>Помните, что мы предоставляем возможность масштабирования серверов. По вашему запросу мы можем увеличить и уменьшить мощность оборудования, чтобы подстроить его под ваши текущие запросы. </p>

	</div>
	
</section>

<section class="faq-block">
    <div class="container">
        <h4 class="hsw-block__h">Вопросы и ответы</h4>
        <div class="faq-items" itemscope="" itemtype="https://schema.org/FAQPage">
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Могут ли я протестировать работу сервера до заказа услуг?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, мы предоставляем пробный период каждому пользователю.</span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Какие операционные системы можно использовать на серверах?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Вы можете установить ОС на базе Linux (Debian, Ubuntu) или Windows Server (2008, 2012, 2016, 2019). В последнем случае лицензия входит в стоимость услуги аренды оборудования. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Делаете ли вы резервные копии данных?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да. По умолчанию мы создаем их раз в месяц, но по вашему запросу можем составить для вас индивидуальный график. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Для каких целей можно использовать выделенные серверы?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Единственным ограничением в этом случае является мощность оборудования. Бюджетное оборудование подойдет для хостинга лендинга или сайта-визитки, а более производительное можно применять для размещения ERP или CRM системы. </span></p>
                </div>
            </div>
            <div class="faq-item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div class="faq-item__bottom">
                    <a href="javascript:;" class="faq-item__name"><span itemprop="name">Можно ли арендовать физический сервер дешево?</span></a>
                    <p class="faq-item__desc" itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">Да, для этого достаточно выбрать вариант с подходящей производительностью. Для дополнительной экономии вы можете недорого арендовать виртуальный сервер.</span></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-block advanatages-block-2">
	
	<div class="page-section__h">Преимущества аренды сервера у нас</div>
	
	<div class="container">
		
		<div class="flex-row jcsp">
			<div class="adv_item2">
				<img src="/images/2020/adv1.png" />
				<p>Безопасноe хранение и передача инфо-данных по защищенным каналам в рамках виртуальной сети (VPN)</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv2.png" />
				<p>Круглосуточный доступ к серверу из любого места с возможностью выхода в Интернет</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv3.png" />
				<p>Возможность самостоятельного выбора и установки требуемого ПО</p>
				
			</div>
			<div class="adv_item2">
				<img src="/images/2020/adv4.png" />
				<p>Обеспечение надежной технической поддержки в круглосуточном режиме</p>
				
			</div>
		</div>
		
	</div>
	
</section>
  
<? $APPLICATION->IncludeFile(
	"/include/mainpage/our-tech.php"
  ); ?>
  
  
<div class="help-conf-block">
  <div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new",
      "custom-service",
      Array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "#custom-service",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => "4",
        'SERVICE_NAME' => 'Аренда виртуального выделенного сервера для 1С'
      )
    ); ?>
  </div>
  
</div>


</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>