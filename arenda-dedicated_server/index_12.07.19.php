<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("KEYWORDS", "выделенный сервер , аренда выделенного сервера , выделенный сервер в России , хостинг выделенный сервер , выделенные сервера в москве , dedicated , dedicated server , dedicated сервер , выделенный сервер хостинг");
$APPLICATION->SetPageProperty("DESCRIPTION", "Возьмите выделенный сервер в Москве. Аренда выделенного физического сервера в Москве дешево: хостинг от ArtexTelecom. Предоставляем услуги dedicated server с 2007 года.");
$APPLICATION->SetPageProperty("tags", "Аренда выделенного сервера - Dedicated");
$APPLICATION->SetPageProperty("NOT_SHOW_TABS", "Y");
$APPLICATION->SetPageProperty("title", "Аренда физического выделенного сервера в Москве | Взять в аренду dedicated server от ArtexTelecom");
$APPLICATION->SetTitle("Аренда выделенного сервера");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.selectbox-m.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/pages/dedic-media.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/select-small.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/sprites/dedic.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick-theme.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/slick.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.prettyCheckboxes.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.selectbox.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scripts_bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jQueryUITouchPunch.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');
CJSCore::Init();
$step = (int)$_REQUEST['step'];
?><style>
  .type-server .configuration, .type-server .ready-made {
    width: 50%;
  }

  .pay-opinion .type-server {
    height: initial;
  }


  #page-dedic {
    margin-top: 0;
  }

/*  .pay-opinion .tab-pane {
    padding-top: 80px;
  }*/
</style>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div id="page-dedic">
				 <? if ($_REQUEST['isAjax'] == 'Y')
        {
        $APPLICATION->RestartBuffer();
        } ?> <? if ($step != 2 || $step == 1): ?>
				<div class="type-server clearfix hidden-xs">
					<ul class="nav nav-tabs" role="tablist">
						<li class="configuration active"> <a href="#configuration" class="n-nav-tabs__link" role="tab"> <i class="icon icon-configuration"></i>
						<div>
							Конфигурация сервера
						</div>
 </a></li>
						<li class="ready-made"><a href="#ready-made" class="n-nav-tabs__link" role="tab"><i class="icon icon-ready-made"></i>
						<div>
							Готовые решения
						</div>
 </a> </li>
					</ul>
				</div>
				<div class="tab-content hidden-xs">
					 <?$APPLICATION->IncludeComponent(
	"itin:dedicated.calc",
	"",
	Array(
		"IBLOCK_ID" => "44",
		"IBLOCK_TYPE" => "dedicated"
	)
);?> <?$APPLICATION->IncludeComponent(
	"itin:dedicated.solutions",
	"blue-v2",
	Array(
		"IBLOCK_ID" => "21",
		"IBLOCK_TYPE" => "xmlcatalog",
		"PAGE_COUNT" => "99"
	)
);?>
				</div>
				 <!--/.tab-content-->
				<div class="n-accordion panel-group visible-xs" id="accordion">
					 <!-- 1 панель -->
					<div class="panel panel-default configuration">
						 <!-- Заголовок 1 панели -->
						<div class="panel-heading">
							<div class="panel-title">
 <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="n-nav-tabs__link" role="tab">
								<div class="panel-title-wrap">
									<i class="icon icon-configuration"></i>
									<div>
										Конфигурация сервера
									</div>
								</div>
								<div class="accodr-arrow">
								</div>
 </a>
							</div>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							 <!-- Содержимое 1 панели -->
							<div class="panel-body">
								 <?$APPLICATION->IncludeComponent(
									"itin:dedicated.calc",
									"mobile",
									Array(
										"IBLOCK_ID" => "44",
										"IBLOCK_TYPE" => "dedicated"
									)
								);?>
							</div>
						</div>
					</div>
					 <!-- 2 панель -->
					<div class="panel panel-default ready-made">
						 <!-- Заголовок 2 панели -->
						<div class="panel-heading">
							<div class="panel-title">
 <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="n-nav-tabs__link" role="tab">
								<div class="panel-title-wrap">
 <i class="icon icon-ready-made"></i>
									<div>
										Готовые решения
									</div>
								</div>
								<div class="accodr-arrow">
								</div>
 </a>
							</div>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							 <!-- Содержимое 2 панели -->
							<div class="panel-body">
								 <?$APPLICATION->IncludeComponent(
	"itin:dedicated.solutions",
	"mobile",
	Array(
		"IBLOCK_ID" => "21",
		"IBLOCK_TYPE" => "xmlcatalog",
		"PAGE_COUNT" => "99"
	)
);?>
							</div>
						</div>
					</div>
				</div>
				<div class="server-opis server-opis-custom ">
					<h2 class="subtitle subtitle-custom">Что вы получить при заказе выделенного сервера</h2>
					<ul class="advantage advantage-custom">
						<li class="advantage__item advantage__item-custom">
						<div class="icon-wrapper">
							<div class="advantage__item_icon advantage__item_icon-new performance">
							</div>
						</div>
						<div class="advantage__item_teaser">
							<div class="advantage__item_name">
								Неограниченный трафик на скорости до 100&nbsp;МБит.c
							</div>
							<div class="advantage__item_description">
								Минимальная задержка пакета для высоконагруженных проектов и популярных сайтов
							</div>
						</div>
 </li>
						<li class="advantage__item advantage__item-custom">
						<div class="icon-wrapper">
							<div class="advantage__item_icon advantage__item_icon-new progress">
							</div>
						</div>
						<div class="advantage__item_teaser">
							<div class="advantage__item_name">
								Создание сервера на заказ
							</div>
							<div class="advantage__item_description">
								Подбор конфигурации сервера из оборудования, которое вам необходимо
							</div>
						</div>
 </li>
						<li class="advantage__item advantage__item-custom">
						<div class="icon-wrapper">
							<div class="advantage__item_icon advantage__item_icon-new settings">
							</div>
						</div>
						<div class="advantage__item_teaser">
							<div class="advantage__item_name">
								ISPManager для выделенного сервера
							</div>
							<div class="advantage__item_description">
								Многофункциональная и удобная панель управления серверным ПО, приложениями и выделенным сервером. Доступ с мобильных устройств
							</div>
						</div>
 </li>
						<li class="advantage__item advantage__item-custom">
						<div class="icon-wrapper">
							<div class="advantage__item_icon advantage__item_icon-new calendar">
							</div>
						</div>
						<div class="advantage__item_teaser">
							<div class="advantage__item_name">
								Тестовый доступ на 7 дней
							</div>
							<div class="advantage__item_description">
								Большинство серверов имеются в наличии и доступны вам для тестирования
							</div>
						</div>
 </li>
					</ul>
				</div>
 <section class="free-trial dedic-section">
				<div class="subtitle subtitle-custom">
					 Получить бесплатный выделенный сервер для тестового доступа
				</div>
 <br>
				<div class="row no-gutters free-trial_wrapper">
					<div class="col-xs-12 col-lg-4">
						<form class="free-trial__form dedic-form">
 <label for="name">Имя</label> <input type="text" name="name" placeholder="Введите имя">
							<p>
							</p>
 <label for="phone">Телефон</label> <input type="tel" name="phone" placeholder="+7(___)___-__-__">
							<p>
							</p>
 <input class="btn-green" type="submit" name="web_form_submit" value="Получить доступ к тестовому серверу">
						</form>
					</div>
					<div class="col-xs-12 col-lg-4">
						<div class="free-trial__char">
							<div class="char__ttl">
								 Характеристики
							</div>
							<div class="char__descrp">
								<p>
									 Процессор Е3 1230 v6 3,5 ГГц
								</p>
								<p>
									 Оперативная память 8 ГБ
								</p>
								<p>
									 Память до 2 Тб
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-4">
						<div class="free-trial__img">
 <img alt="Получи сервер бесплатно" src="/local/templates/depohost/images/server-free.png" style="max-width: 100%;">
						</div>
					</div>
				</div>
 </section> <section class="pay-opinion" style="margin-top: 50px;">
				<div class="subtitle subtitle-custom" style="padding-left: 20px; padding-right: 20px;">
					 СПОСОБЫ ОПЛАТЫ
				</div>
				<div class="pay-opinion-wrapper hidden-xs ">
					<ul class="nav nav-tabs pay-opinion__tabs type-server" role="tablist">
						<li class="configuration active"> <a href="#fiz" class="n-nav-tabs__link" role="tab"> <i class="icon icon-configuration"></i>
						<div>
							Физические лица
						</div>
 </a> </li>
						<li class="ready-made"> <a href="#jur" class="n-nav-tabs__link" role="tab"> <i class="icon icon-ready-made"></i>
						<div>
							Юридические лица
						</div>
 </a> </li>
					</ul>
					<div class="tab-content hidden-xs">
						<div id="fiz" class="tab-pane active clearfix ">
							<div class="footer__pay-images">
								<div class="master-card">
								</div>
								<div class="visa">
								</div>
								<div class="web-money">
								</div>
								<div class="qiwi">
								</div>
								<div class="alpha-bank">
								</div>
								<div class="sberbank">
								</div>
								<div class="yandex-money">
								</div>
							</div>
						</div>
						<div id="jur" class="tab-pane clearfix">
 <i class="pay-step pay-step_check"></i>
							<p class="pay-descrp">
								 Заключение договора<br>
								 оферты
							</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_bill"></i>
							<p class="pay-descrp">
								 Выставление счетов в<br>
								 панели управления
							</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_calendar-blue"></i>
							<p class="pay-descrp">
								 Актирование в конце <br>
								месяца
							</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_email"></i>
							<p class="pay-descrp">
								 Отправка актов почтой <br>
								и электронно
							</p>
						</div>
					</div>
				</div>
				<div class="n-accordion panel-group visible-xs" id="pay-acc">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="panel-title">
 <a data-toggle="collapse" data-parent="#pay-acc" href="#fiz-pan" class="n-nav-tabs__link" role="tab">
								<div class="panel-title-wrap">
 <i class="icon icon-configuration"></i>
									<div>
										Физические лица
									</div>
								</div>
								<div class="accodr-arrow">
								</div>
 </a>
							</div>
						</div>
						<div id="fiz-pan" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="footer__pay-images">
									<div class="master-card">
									</div>
									<div class="visa">
									</div>
									<div class="web-money">
									</div>
									<div class="qiwi">
									</div>
									<div class="alpha-bank">
									</div>
									<div class="sberbank">
									</div>
									<div class="yandex-money">
									</div>
								</div>
							</div>
						</div>
					</div>
					 <!-- 2 панель -->
					<div class="panel panel-default">
						 <!-- Заголовок 2 панели -->
						<div class="panel-heading">
							<div class="panel-title">
 <a data-toggle="collapse" data-parent="#pay-acc" href="#jur-pan" class="n-nav-tabs__link" role="tab">
								<div class="panel-title-wrap">
 <i class="icon icon-ready-made"></i>
									<div>
										Юридические лица
									</div>
								</div>
								<div class="accodr-arrow">
								</div>
 </a>
							</div>
						</div>
						<div id="jur-pan" class="panel-collapse collapse">
							 <!-- Содержимое 2 панели -->
							<div class="panel-body">
 <i class="pay-step pay-step_check"></i>
								<p class="pay-descrp">
									 Заключение договора<br>
									 оферты
								</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_bill"></i>
								<p class="pay-descrp">
									 Выставление счетов в<br>
									 панели управления
								</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_calendar-blue"></i>
								<p class="pay-descrp">
									 Актирование в конце <br>
									месяца
								</p>
 <i class="pay-arrow"></i> <i class="pay-step pay-step_email"></i>
								<p class="pay-descrp">
									 Отправка актов почтой <br>
									и электронно
								</p>
							</div>
						</div>
					</div>
				</div>
 </section> <br>
 <br>
 <section class="advantage dedic-section">
				<div class="subtitle subtitle-custom">
					 ПРЕИМУЩЕСТВА СОТРУДНИЧЕСТВА С НАМИ
				</div>
				<div class="advantage-block">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_data"></i>
								<div class="advantage-item__descrp">
									 Собственный дата-центр последнего поколения в Москве
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_web"></i>
								<div class="advantage-item__descrp">
									 Поддержка 24/7, 365 дней
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_settings-blue"></i>
								<div class="advantage-item__descrp">
									 Бесплатная настройка и установка ОС и приложений
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_login"></i>
								<div class="advantage-item__descrp">
									 Бесплатное администрирование
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_bin"></i>
								<div class="advantage-item__descrp">
									 Настройка сервера через 30 минут после оплаты
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="advantage-item">
 <i class="advantage-item__icon icon_layers"></i>
								<div class="advantage-item__descrp">
									 Резервное копирование данных
								</div>
							</div>
						</div>
					</div>
				</div>
 </section> <section class="dedic-section">
				<div class="seo-txt">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/dedicated_server/seo-text.php"
	)
);?>
				</div>
 </section> <section class="review-section dedic-section">
				<div class="subtitle subtitle-custom">
					 ОТЗЫВЫ
				</div>
				<ul class="dedic-slider">
					<li class="dedic-slider__item">
					<div class="review">
						<div class="review__header">
							<div class="review__ttl">
								 Александр
							</div>
							<div class="review__date">
								 26 сентября 2018 в 11:50
							</div>
						</div>
						<div class="review__descrp">
							 Около года назад в связи с расширением интернет-сайтов нашей компании потребовался мощный выделенный сервер, чего не смог предоставить предыдущий хостер (мастерхост), хотя с ними мы работали 5 лет и арендовали 2 сервера. Долго выбирали новый хостинг среди различных крупных и известных компаний в этой сфере. Выяснилось следущее: как правило хостеры неохотно подстраиваются под клиента и предлагают что есть, а нам нужен был мощный, дорогой сервер в аренду. В итоге выбирать пришлось из нескольких компаний, а лучшие условия предложил Депохост
						</div>
					</div>
 </li>
					<li class="dedic-slider__item">
					<div class="review">
						<div class="review__header">
							<div class="review__ttl">
								 Борис
							</div>
							<div class="review__date">
								 05 февраля 2019 в 01:58
							</div>
						</div>
						<div class="review__descrp">
							 Год назад арендовали сервер для 1С, в целом все нормально, ПО Microsoft Office дороговато а где оно сейчас дешевое, но главное не это, радует постоянный онлайн и отзывчивая поддержка и обратная связь
						</div>
					</div>
 </li>
				</ul>
 </section> <section class="services dedic-section">
				<div class="subtitle subtitle-custom">
					 УСЛУГИ ДЛЯ ВАС
				</div>
				<ul class="services-list-custom">
					<li><a href="#">Аренда VDS сервера</a></li>
					<li><a href="#">Аренда сервера для 1С</a></li>
					<li><a href="#">Хостинг DNS</a></li>
					<li><a href="#">Аренда серваер Windows</a></li>
					<li><a href="#">SSL сертификаты</a></li>
				</ul>
 </section> <section class="bottom-form dedic-section">
				<div class="subtitle subtitle-custom">
					 Остались вопросы? Оставьте номер телефона и мы подробно на них ответим
				</div>
				<form class="any-quest-form">
					<div class="row">
						<div class="col-xs-12 col-lg-6 dedic-form">
 <label for="name">Имя</label> <input type="text" name="name" placeholder="Введите имя">
							<p>
							</p>
 <label for="phone">Телефон</label> <input type="tel" name="phone" placeholder="+7(___)___-__-__">
							<p>
							</p>
 <input class="btn-green hidden-xs hidden-sm hidden-md" type="submit" name="web_form_submit" value="Получить доступ к тестовому серверу">
						</div>
						<div class="col-xs-12 col-lg-6 dedic-form">
 <label for="message">Ваш вопрос</label> <textarea name="message" id="" cols="30" rows="10" placeholder="Введите текс вопроса"></textarea> <br>
 <input class="btn-green hidden-lg" type="submit" name="web_form_submit" value="Получить доступ к тестовому серверу">
						</div>
					</div>
				</form>
 </section> <section class="in-service dedic-section">
				<div class="in-service__ttl-wrp">
					<div class="subtitle subtitle-custom">
						 Что входит в услугу аренды выделенного сервера
					</div>
					<div class="underline">
					</div>
					<div class="show-content-link">
 <a href="#">Показать</a>
					</div>
				</div>
				<div class="in-service__content">
					<div class="row">
						<div class="col-xs-12 col-lg-6">
							<ul class="dedic-list">
								<li>Размещение в крупнейшем дата-центре Москвы</li>
								<li>Современное серверное оборудование</li>
								<li>Настройка и установка операционной системы и приложений</li>
								<li>Высокоскоростной доступ, безлимитный трафик</li>
								<li>Мониторинг через SMS и E-mail</li>
								<li>Профессиональное администрирование</li>
								<li>Удобная панель управления серверным ПО</li>
							</ul>
							<div class="subtitle subtitle-custom" style="font-size: 20px; padding-top: 50px; padding-bottom: 30px">
								 ОСОБЕННОСТИ
							</div>
							<p>
								 Арендуя выделенный сервер в нашем дата-центре, вы экономите значительную сумму на приобретении, размещении и обслуживании собственного компьютерного оборудования, а также получаете
							</p>
							<ul class="dedic-list">
								<li>Полный контроль над ресурсами, процессами и задачами, надежное бесперебойное подключение к сети Интернет</li>
								<li>Возможность установить любую ОС: Windows, Linux и др., любые необходимые приложения</li>
								<li>Возможность разместить самые ресурсоемкие сайты в практически неограниченном количестве</li>
								<li>Профессиональная техническая поддержка, круглосуточный контроль, надежная защита от несанкционированного доступа</li>
							</ul>
 <br>
							 Выделенный сервер Dedicated Server – это не просто дорогое, качественное и высокопроизводительное оборудование, но и развитая сетевая инфраструктура, надлежаще оснащенное помещение с оптимальными климатическими условиями, стабильное бесперебойное питание, обеспечение безопасности пользовательских данных, регулярное техническое обслуживание
							<p>
							</p>
						</div>
						<div class="col-xs-12 col-lg-6">
							<div class="subtitle subtitle-custom" style="font-size: 20px; padding-top: 0px; padding-bottom: 30px">
								 ЧТО МЫ ПРЕДЛАГАЕМ
							</div>
							<p>
								 На этой странице вы найдете готовые решения конфигураций выделенных серверов, которые являются наиболее распространенными и востребованными. При необходимости вы также можете самостоятельно настроить конфигурацию под свои задачи, указав следующие характеристики:
							</p>
							<ul class="dedic-list">
								<li>Поколение и модель процессора </li>
								<li>Объем оперативной памяти</li>
								<li>Тип и объем жестких дисков</li>
								<li>Наличие RAID-контроллера</li>
							</ul>
							<p>
							</p>
							<div class="subtitle subtitle-custom" style="font-size: 20px; padding-top: 50px; padding-bottom: 30px">
								 ПРЕИМУЩЕСТВА СОТРУДНИЧЕСТВА
							</div>
							<ul class="dedic-list">
								<li>Гибкие настройки Dedicated Server</li>
								<li>Высококачественный сервис</li>
								<li>Персональный подход к каждому клиенту</li>
								<li>Собственный дата-центр последнего поколения в Москве</li>
								<li>Новейшее оборудование</li>
								<li>Мощная система терморегуляции</li>
								<li>Бесперебойное электропитание и подключение к сети Интернет</li>
								<li>Круглосуточное обслуживание</li>
								<li>Резервное копирование данных</li>
								<li>Гарантия безопасности и конфиденциальности данных</li>
								<li>Профессиональная техподдержка</li>
							</ul>
						</div>
					</div>
				</div>
 </section>
				<!--.center--> <? elseif ($step == 2): ?><?$APPLICATION->IncludeComponent(
	"itin:dedicated.advance.options",
	"",
	Array(
		"IBLOCK_ID" => "50",
		"IBLOCK_TYPE" => "catalog"
	)
);?><? endif ?> <? if ($_REQUEST['isAjax'] == 'Y')
    {
    die();
    } ?>
			</div>
		</div>
	</div>
</div>
 <!--/#page-dedic--><?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>