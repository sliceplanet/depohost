<?php
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
 * Date: 07.10.18
 * Time: 16:41
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
if (!$GLOBALS['USER']->IsAdmin())
{
  LocalRedirect('/');
}
?>
<div class="container">
<?
\Bitrix\Main\Loader::includeModule('clients');

$arClients = \Itin\Depohost\Clients::getList(['filter'=>['ACTIVE'=>'Y']]);
foreach ($arClients as $index => $arClient)
{
  $bFound = false;
  $arServices = \Itin\Depohost\Clients::getClientServicesListRequirePay($arClient['ID']);
  $client = new \Itin\Depohost\Clients($arClient['CC_CLIENT']['VALUE']);
  foreach ($arServices as $index => $arService)
  {
    if (intval($arService['UF_SUMM']) === 0 && intval($arService['UF_PRICE'])>0 && intval($arService['UF_QUANTITY'])>0)
    {
      $bFound = true;
      echo '<a target="_blank" style="color:red" href="https://www.depohost.ru/bitrix/admin/client_edit.php?IBLOCK_ID=34&type=personal&ID='.$arClient['ID'].'&lang=ru&PROFILE_ID='.$client->getProfileId().'&find_section_section=0&WF=Y">Client №'.$arClient['NAME'].'</a> <br>';
      echo 'Service '.$arService['NAME'].' quantity='.$arService['UF_QUANTITY']. ' price='.$arService['UF_PRICE'].' summ='.$arService['UF_SUMM'].'<br>';
    }
  }
  if (!$bFound)
  {
    echo '<a target="_blank" style="color:green" href="https://www.depohost.ru/bitrix/admin/client_edit.php?IBLOCK_ID=34&type=personal&ID='.$arClient['ID'].'&lang=ru&PROFILE_ID='.$client->getProfileId().'&find_section_section=0&WF=Y">Client №'.$arClient['NAME'].' - OK</a> <br>';
  }
}
?>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
