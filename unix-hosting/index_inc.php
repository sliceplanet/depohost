<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>

<section class="page-section how-ssl-works-section dns-hosting-text">
	
	<div class="container">
	
	<div class="hsw__wrapper">
			
			<div class="hsw-block">
				<div class="hsw-block__content">
    <p>Хостинг Unix сейчас является наиболее распространенным и удобным для большинства пользователей. Это касается и
      стоимости хостинга, и его других преимуществ: работоспособность, быстродействие, безопасность, работа с большим
      количеством данных. Хостинг с php и mysql позволяет любому сайту работать в оптимальном режиме. Кроме того, базы
      данных mysql отлично совмещаются с архитектурой Unix.</p>
				</div>
				
			</div>
			<div class="hsw-block">
				<div class="hsw-block__content">
    <p>Хостинг php позволяет
      прикрепить SSL-сертификаты, различные панели управления и многое другое. На Unix и Linux хостинг прекрасно
      обеспечивает работу в полном функционале любых проектов, поэтому особенно актуален для небольшого и среднего
      бизнеса, создаваемого в Интернете. Для старта нового бизнес-проекта он считает наиболее оптимальным. </p>

    <p>Mysql хостинг дает возможность гарантировать полную безопасность всех баз данных. При этом мы гарантируем
      надежность серверов и их качественное обслуживание, что не могли не отметить многие пользователи, имеющие
      собственные бизнес-проекты в сети. </p>
				</div>
				<div class="hsw-block_img">
					<img src="/images/down/ssl-2.png">
				</div>
			</div>
			<div class="hsw-block">
				<div class="hsw-block__h">Что мы можем предложить</div>
				<div class="hsw-block__content">
    <p> Хостинг с php и
      mysql дает возможность разместить зарегистрированный домен и управлять ним различными аппаратными и софтверными
      средствами. Так же, как и аренда физического выделенного сервера предоставляемый нами хостинг позволяет получить
      пользователю электронную почту, доступ к управлению программным обеспечением и большое количество других
      инструментов, которые позволят настроить систему под конкретные цели. </p>

    <p>Кроме того, мы обеспечиваем для своих клиентов максимальную скорость при обмене данными, отличную
      производительность и выгодные тарифы. На наших серверах установлены SSD-диски, которые работают в RAID-массиве,
      благодаря этому хостинг php имеет достаточно высокую скорость, а специальные настройки сервера дают возможность
      загружать данные из оперативной памяти, что делает работу хостинга еще быстрее. </p>

    <p>Также работа хостинга осуществляется в режиме CGI Fast, то есть, выключенный APC-акселератор загружает ресурс
      клиента практически моментально. </p>
				</div>
				<div class="hsw-block_img">
					<img src="/images/down/xosting.png">
				</div>
			</div>


	</div>
	</div>
</section>



<section class="page-section anyq-section anyq-black">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	