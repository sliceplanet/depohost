<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Лучший UNIX хостинг сайтов с поддержкой PHP, Perl, MySQL и phpMyAdmin от ArtexTelecom. Предоставляем услуги Web-хостинга и аренды серверов с 2007 года");
$APPLICATION->SetPageProperty("keywords", "LINUX хостинг с поддержкой PHP и MySQL");
$APPLICATION->SetPageProperty("title", "UNIX хостинг с поддержкой PHP, MySQL от ArtexTelecom");
$APPLICATION->SetTitle("LINUX хостинг с поддержкой PHP и MySQL");
?>
<section class="page-section whyssl-section why-vvds">
		
	<div class="page-section__h">ДЛЯ ЧЕГО НУЖЕН UNIX ХОСТИНГ</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds1.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Быстрый старт</div>
					<p>Доступ к услуге в течении 15 минут после оплаты</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds2.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Широкие возможности</div>
					<p>Все возможности хостинга плюс бесплатно панель управления ISPmanager Lite</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/vds3.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Поддержка PHP, MySQL и т.д.</div>
					<p>Все тарифы поддерживают Весь джентельменский набор PHP хостинга</p>
				</div>
			</div>			
		</div>
		
	</div>
	
</section>

<section class="page-section section-graybg">
	<div class="container">
    <? $APPLICATION->IncludeComponent(
      "bitrix:catalog",
      "hosting2020",
      array(
        "IBLOCK_TYPE" => "xmlcatalog",
        "IBLOCK_ID" => "32",
        "HIDE_NOT_AVAILABLE" => "N",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/unix-hosting/",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_STATUS_404" => "Y",
        "SET_TITLE" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "ADD_ELEMENT_CHAIN" => "N",
        "USE_ELEMENT_COUNTER" => "Y",
        "USE_FILTER" => "N",
        "FILTER_VIEW_MODE" => "HORIZONTAL",
        "USE_REVIEW" => "N",
        "USE_COMPARE" => "Y",
        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        "COMPARE_FIELD_CODE" => array(
          0 => "",
          1 => "",
        ),
        "COMPARE_PROPERTY_CODE" => array(
          0 => "",
          1 => "",
        ),
        "COMPARE_ELEMENT_SORT_FIELD" => "sort",
        "COMPARE_ELEMENT_SORT_ORDER" => "asc",
        "DISPLAY_ELEMENT_SELECT_BOX" => "N",
        "PRICE_CODE" => array(
          0 => "Розничная",
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "CONVERT_CURRENCY" => "N",
        "BASKET_URL" => "/personal/cart/",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "USE_PRODUCT_QUANTITY" => "Y",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRODUCT_PROPERTIES" => array(),
        "SHOW_TOP_ELEMENTS" => "Y",
        "TOP_ELEMENT_COUNT" => "30",
        "TOP_LINE_ELEMENT_COUNT" => "1",
        "TOP_ELEMENT_SORT_FIELD" => "sort",
        "TOP_ELEMENT_SORT_ORDER" => "asc",
        "TOP_ELEMENT_SORT_FIELD2" => "id",
        "TOP_ELEMENT_SORT_ORDER2" => "desc",
        "TOP_PROPERTY_CODE" => array(
          0 => "",
          1 => "",
        ),
        "SECTION_COUNT_ELEMENTS" => "Y",
        "SECTION_TOP_DEPTH" => "2",
        "SECTIONS_VIEW_MODE" => "TEXT",
        "SECTIONS_SHOW_PARENT_NAME" => "Y",
        "PAGE_ELEMENT_COUNT" => "30",
        "LINE_ELEMENT_COUNT" => "1",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER2" => "desc",
        "LIST_PROPERTY_CODE" => array(
          0 => "CMN_MB",
          1 => "CMN_TRF",
          2 => "CMN_EML",
          3 => "CMN_DMN",
          4 => "CMN_WBS",
          5 => "CMN_SDM",
          6 => "CMN_CPU",
          7 => "CMN_MMR",
          8 => "CMN_PRC",
          9 => "PLS_PHP5",
          10 => "PLS_PHP4",
          11 => "PLS_ZND",
          12 => "PLS_BIN",
          13 => "PLS_CGI",
          14 => "PLS_PRL",
          15 => "ADD_BDM",
          16 => "PLS_PMA",
          17 => "PLS_SSI",
          18 => "PLS_FTP",
          19 => "PLS_MFTP",
          20 => "PLS_PSW",
          21 => "PLS_HTC",
          22 => "PLS_ISP",
          23 => "PLS_WBE",
          24 => "PLS_ANTI",
          25 => "PLS_ERR",
          26 => "PLS_SSL",
          27 => "",
        ),
        "INCLUDE_SUBSECTIONS" => "Y",
        "LIST_META_KEYWORDS" => "-",
        "LIST_META_DESCRIPTION" => "-",
        "LIST_BROWSER_TITLE" => "-",
        "DETAIL_PROPERTY_CODE" => array(
          0 => "CMN_MB",
          1 => "CMN_TRF",
          2 => "CMN_EML",
          3 => "CMN_DMN",
          4 => "CMN_WBS",
          5 => "CMN_SDM",
          6 => "CMN_CPU",
          7 => "CMN_MMR",
          8 => "CMN_PRC",
          9 => "PLS_PHP5",
          10 => "PLS_PHP4",
          11 => "PLS_ZND",
          12 => "PLS_BIN",
          13 => "PLS_CGI",
          14 => "PLS_PRL",
          15 => "ADD_BDM",
          16 => "PLS_PMA",
          17 => "PLS_SSI",
          18 => "PLS_FTP",
          19 => "PLS_MFTP",
          20 => "PLS_PSW",
          21 => "PLS_HTC",
          22 => "PLS_ISP",
          23 => "PLS_WBE",
          24 => "PLS_ANTI",
          25 => "PLS_ERR",
          26 => "PLS_SSL",
          27 => "",
        ),
        "DETAIL_META_KEYWORDS" => "-",
        "DETAIL_META_DESCRIPTION" => "-",
        "DETAIL_BROWSER_TITLE" => "-",
        "LINK_IBLOCK_TYPE" => "",
        "LINK_IBLOCK_ID" => "",
        "LINK_PROPERTY_SID" => "",
        "LINK_ELEMENTS_URL" => "",
        "USE_ALSO_BUY" => "Y",
        "ALSO_BUY_ELEMENT_COUNT" => "5",
        "ALSO_BUY_MIN_BUYES" => "2",
        "USE_STORE" => "N",
        "PAGER_TEMPLATE" => "",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "ADD_PICT_PROP" => "-",
        "LABEL_PROP" => "-",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "DETAIL_SHOW_MAX_QUANTITY" => "N",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "Заказать",
        "MESS_BTN_COMPARE" => "Сравнение",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "DETAIL_USE_VOTE_RATING" => "N",
        "DETAIL_USE_COMMENTS" => "N",
        "DETAIL_BRAND_USE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
        "SEF_URL_TEMPLATES" => array(
          "sections" => "",
          "section" => "",
          "element" => "",
          "compare" => "compare.php?action=#ACTION_CODE#",
        ),
        "VARIABLE_ALIASES" => array(
          "compare" => array(
            "ACTION_CODE" => "action",
          ),
        )
      ),
      false
    ); ?>
	</div>
  </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>