<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "Хостинг 1С. ArtexTelecom - предоставляем услуги аренды сервера в Москве в собственном дата-центре с 2007 г. Аренда выделенного сервера, хостинг");
$APPLICATION->SetPageProperty("title", "Хостинг 1С Битрикс — купить, тарифы, цены | Аренда сервера для 1С");
$APPLICATION->SetTitle("Хостинг 1С");
?>
<div class="container">
  
<p>Хостинг 1С — это выгодный и удобный вариант для размещения большого количества информации. С его помощью сотрудники вашего предприятия смогут работать с бухгалтерскими и налоговыми отчетами из любой точки мира. Купив виртуальный сервер в компании ArtexTelecom, вы защитите корпоративную информацию от потери и недоброжелателей.
</p>

<p align="center"><img src="/images/pages/hosting-1c/1.png" title="Хостинг 1С" alt="Хостинг 1С"></p>

<h2>Преимущества аренды хостинга для работы с 1С</h2>

<ol class="normal_ol">
  <li>Снижение затрат. Благодаря аренде вы сможете сэкономить значительную часть средств на покупке и обслуживании личного сервера. </li>
  <li>Высокая производительность. Все предлагаемые нами конфигурации протестированы и справляются с повышенными нагрузками. </li>
  <li>Удаленный доступ к данным 24/7. Вы можете получать и делиться информацией из базы «1С:Предприятие» с другими сотрудниками организации, находясь дома или в командировке в любой точке мира. </li>
  <li>Возможность увеличения производительности и изменения конфигурации.</li>
  <li>Безлимитный трафик. Вам будет доступно бесперебойное подключение к Сети на высоких скоростях. </li>
  <li>Возможность восстановления данных. Хостинг позволяет сохранить важную информацию и вернуть ее при случайном удалении или сбое программы.</li>
  <li>Круглосуточная защита. Все сведения надёжно защищены от несанкционированного доступа в соответствии с ФЗ №152. </li>
</ol>

<h2>Преимущества обращения в нашу компанию</h2>

<div class="advantage-img-wrap">
  <ul class="advantage" style="padding-bottom: 20px;">
    <li class="advantage__item">
      <div class="advantage__item_icon calc"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Мы ценим долговременное сотрудничество, поэтому наши постоянные клиенты получают приятные скидки при внесении предоплаты за 3–6–12 месяцев сотрудничества.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon free-settings"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description">При покупке выделенного сервера для 1С в нашей компании мы выполним бесплатный перенос данных и настройку, подробно проконсультируем по работе с хостингом.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon mouse"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Вы получаете 15 дней размещения данных на нашем облачном сервисе после внесения аванса за пользование хостингом ArtexTelecom.
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon check-list"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Вы вносите только абонентскую плату – мы не начисляем никаких дополнительных комиссий. В исключительных случаях возможна рассрочка платежа в течение одного месяца. 
        </div>
      </div>
    </li>

    <li class="advantage__item">
      <div class="advantage__item_icon day"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> Возникла ошибка? Не знаете, как работать в панели администратора? Специалист техподдержки поможет сразу после вашего обращения!
        </div>
      </div>
    </li>
    
    <li class="advantage__item">
      <div class="advantage__item_icon path"></div>
      <div class="advantage__item_teaser pt-0">
        <div class="advantage__item_description"> При заключении договора вы получаете бесплатные интернет-трафик и 100 Гб для хранения резервных копий на внутреннем FTP-сервере.
        </div>
      </div>
    </li>
  </ul>
</div>

<p>Чтобы взять в аренду 1С сервер, позвоните по круглосуточному номеру 8 (495) 797-8-500 или обратитесь к менеджеру с помощью формы обратной связи на странице «Контакты». Он сориентирует вас по тарифам и поможет выбрать оптимальный по цене и опциям вариант. </p>

</div>
<div class="band-blue wrapper wide visible-lg" style="margin-top: 0px !important;"></div>
<section class="section callback_bottom">
  <div class="container">
    <div class="subtitle subtitle-custom text-center">
      Остались вопросы? Оставьте номер телефона и мы подробно на них ответим
    </div>
    <div class="row">
      <?
      $APPLICATION->IncludeComponent(
              "bitrix:form.result.new",
              "new_any-quest-form",
              Array(
                  "SEF_MODE" => "N",
                  "WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
                  "LIST_URL" => "result_list.php",
                  "EDIT_URL" => "result_edit.php",
                  "SUCCESS_URL" => "",
                  "CHAIN_ITEM_TEXT" => "",
                  "CHAIN_ITEM_LINK" => "",
                  "IGNORE_CUSTOM_TEMPLATE" => "Y",
                  "USE_EXTENDED_ERRORS" => "Y",
                  "CACHE_TYPE" => "A",
                  "CACHE_TIME" => "3600",
                  "AJAX_MODE" => "Y",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "N",
                  "AJAX_OPTION_HISTORY" => "N",
                  "SEF_FOLDER" => "/",
                  "VARIABLE_ALIASES" => Array(
                  )
              )
          );?>
    </div>
  </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>