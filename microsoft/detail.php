<?php
/**
 * @author Aleksandr Terentev <alvteren@gmail.com>
* Date: 30.08.18
* Time: 21:45
*/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<link href="/local/templates/depohost/css/pages/microsoft.css" type="text/css" rel="stylesheet">
<link href="/local/templates/depohost/css/pages/microsoft-media.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src=" /local/js/phpjs/strings/number_format.js"></script>
<script type="text/javascript"
        src="/local/templates/depohost/components/bitrix/catalog/ssl/bitrix/catalog.element/style-v2/script.js"></script>

<div class="microsoft">
 <div class="center">

 <div class="bg-left"></div>
 <div class="bg-right"></div>
  <div class="container">
   <div class="row">
    <div class="col-xs-12 ">
     <div class="microsoft-wrap">
      <aside class="sidebar">

       <div class="menu">

        <div class="menu-item">
         <div class="menu-ttl" id="bx_1847241719_159"><i></i><a
          href="/microsoft/microsoft-windows-server-na-protsessor/">Microsoft
          Windows Server - на процессор (1 лиц. на два ядра)</a></div>
         <a class="currentITEM" href="/microsoft/microsoft-windows-server-na-protsessor/350/">Windows Web Server
          2008/2012
          R2/2016</a><a href="/microsoft/microsoft-windows-server-na-protsessor/371/">Windows Server 2008/2012 R2/2016
         Standard</a><a href="/microsoft/microsoft-windows-server-na-protsessor/373/">Windows Server 2008/2012 R2/2016
         Datacenter</a>
         <div class="menu-ttl" id="bx_1847241719_167"><i></i><a
          href="/microsoft/microsoft-sql-server-na-protsessor-1-lits-na-dva-yadra/">Microsoft SQL Server - на процессор
          (1
          лиц. на два ядра)</a></div>
         <a href="/microsoft/microsoft-sql-server-na-protsessor-1-lits-na-dva-yadra/374/">SQL Server 2008-2016 Web
          Edition</a><a href="/microsoft/microsoft-sql-server-na-protsessor-1-lits-na-dva-yadra/375/">SQL Server
         2008-2016
         Standard Edition</a>
         <div class="menu-ttl" id="bx_1847241719_224"><i></i><a
          href="/microsoft/arenda-ms-sql-server-na-polzovatelya-/">Аренда
          MS SQL Server на пользователя </a></div>
         <a href="/microsoft/arenda-ms-sql-server-na-polzovatelya-/2409/">SQL Server 2008-2016 Standard Edition
          UserCal</a>
         <div class="menu-ttl" id="bx_1847241719_168"><i></i><a
          href="/microsoft/microsoft-windows-remote-desktop-services-usrcal-na-polzovatelya/">Microsoft Windows Remote
          Desktop Services UsrCAL - на пользователя</a></div>
         <a href="/microsoft/microsoft-windows-remote-desktop-services-usrcal-na-polzovatelya/419/">Windows Remote
          Desktop
          Services UsrCAL</a>
         <div class="menu-ttl" id="bx_1847241719_169"><i></i><a
          href="/microsoft/microsoft-office-standart-2010-na-polzovatelya/">Microsoft Office Standart 2010 - на
          пользователя</a></div>
         <a href="/microsoft/microsoft-office-standart-2010-na-polzovatelya/420/">Office Standart 2010</a><a
         href="/microsoft/microsoft-office-standart-2010-na-polzovatelya/421/">Office Professional Plus 2010</a><a
         href="/microsoft/microsoft-office-standart-2010-na-polzovatelya/12321/">Excel из пакета MS Office Standart
         2010</a><a href="/microsoft/microsoft-office-standart-2010-na-polzovatelya/12322/">Word из пакета MS Office
         Standart 2010</a></div>
        <div style="clear: both;"></div>
       </div>
      </aside>
      <div class="content" id="bx_117848907_350">
       <h1 class="h1">
        Windows Web Server 2008/2012 R2/2016 <span>Microsoft Windows Server - на процессор (1 лиц. на два ядра)</span>
       </h1>
       <div class="wrapper element">
        <div class="server element">
         <div class="n-server-top__wrap">
          <div class="n-server-top__title">Стоимость тарифа Windows Web Server 2008/2012 R2/2016</div>
          <div class="item_price">
           <div class="item_old_price" id="bx_117848907_350_old_price" style="display: none"></div>
           <div class="item_current_price" id="bx_117848907_350_price">290 <span>руб./месяц</span></div>
           <div class="item_economy_price" id="bx_117848907_350_price_discount" style="display: none"></div>
          </div>
         </div>
         <div class="server-info">
          <p>Windows Web Server 2008 R2, расширяет базовые возможности операционной системы и предоставляет новые мощные
           средства, помогая организациям всех размеров, повышать управляемость, доступность и гибкость.</p>
         </div>
        </div>
        <div class="item_info_section">

         <div class="server-opis-price microsoft-element">
          <p class="server-opis-price-title">Цена:</p>
          <div class="server-opis-price-num">
           <div class="item_old_price" id="bx_117848907_350_old_price" style="display: none"></div>
           <div class="item_current_price" id="bx_117848907_350_price">290</div>
           <div class="item_economy_price" id="bx_117848907_350_price_discount" style="display: none"></div>
          </div>
          <div class="server-opis-price-txt"><span>/</span>рублей<br>в месяц</div>
         </div>
         <div class="item_buttons vam">
                        <span class="item_buttons_counter_block">

                            <input id="bx_117848907_350_quantity" type="text" class="tac transparent_input" value="1">

                            <div>
                                <a href="javascript:void(0)" class="bx_bt_white bx_small bx_fwb"
                                   id="bx_117848907_350_quant_up">+</a>
                                <a href="javascript:void(0)" class="bx_bt_white bx_small bx_fwb"
                                   id="bx_117848907_350_quant_down">-</a>
                            </div>

                            <span id="bx_117848907_350_quant_measure"></span>
                        </span>
          <span class="item_buttons_counter_block">
                            <a href="/microsoft/?action=BUYIT&amp;id[]=350&amp;quantity[]=1"
                               class="bx_big bx_bt_blue bx_cart btn" id="bx_117848907_350_buy_link"><span></span>Заказать</a>
                                                        </span>
         </div>

        </div>

        <div class="description">
         <div class="h1 description-title">Описание<span>Windows Web Server 2008/2012 R2/2016</span></div>
         <p style="text-align: justify;">Предлагаем Вашему вниманию помесячную аренду наиболее актуальной версии ПО <a
          href="http://www.depohost.ru/microsoft/">Microsoft Windows</a> Web Server 2008 R2. Данный программный продукт
          обладает более широкими возможностями в сравнении со стандартной версией Server 2008. Поэтому для растущих
          предприятий, крупных компаний и прогрессирующих бизнес-проектов такое решение является более предпочтительным.
         </p>

         <p style="text-align: justify;">Windows Web Server 2008 R2 позволяет организовывать наиболее важные рабочие
          процессы и повышать их управляемость, делая более доступными для своих сотрудников. Помимо прочего, повышается
          степень гибкости всех проводимых операций с документами и инфо-данными. Ведение современного бизнеса требует
          соответствия определенным стандартам, с чем прекрасно справляется данное программное обеспечение. </p>

         <p style="text-align: justify;">В отношении безопасной работы данный продукт признан одним из наиболее
          оптимальных, а надежность работы лицензионных версий системы гарантирована самим производителем.
          Быстродействие
          –
          еще один немаловажный показатель, который отличает данный дистрибутив от аналогов в данном сегменте.</p>

         <p style="text-align: justify;">Наша компания предлагает аренду Windows Web Server 2008 R2 на постоянной основе
          по
          выгодным ежемесячным тарифам. Также наши специалисты готовы оказать Вам помощь в настройке сервера и переносе
          Ваших баз данных в рабочую сферу нового проекта. При этом мы гарантируем максимальную степень
          отказоустойчивости
          всего оборудования и программного обеспечения. Впоследствии возможен переход на более современные версии
          Windows
          Server, к примеру, на дистрибутив 2012 года в различных его модификациях. На данный момент это наиболее
          оптимальный и доступный в финансовом отношении вариант, который является хорошим подспорьем на старте
          масштабного
          бизнес-проекта.</p>
        </div>

       </div>

       <div class="bx_item_container">
        <div class="bx_lt">
         <div class="bx_item_slider" id="bx_117848907_350_big_slider">
          <div class="bx_bigimages" style="display:none;">
           <div class="bx_bigimages_imgcontainer">
            <span class="bx_bigimages_aligner"></span>
            <img id="bx_117848907_350_pict" src="" alt="" title="">
           </div>
          </div>
         </div>
        </div>

        <div class="bx_rt">
         <div class="item_info_section" style="display:none;">
          <p>Windows Web Server 2008 R2, расширяет базовые возможности операционной системы и предоставляет новые мощные
           средства, помогая организациям всех размеров, повышать управляемость, доступность и гибкость.</p></div>
         <div class="clb"></div>
        </div>

        <div class="bx_md">
         <div class="item_info_section">
         </div>
        </div>
        <div class="bx_lb">
         <div class="tac ovh">
         </div>
         <div class="tab-section-container">
         </div>
        </div>
        <div style="clear: both;"></div>
       </div>
       <div class="clb"></div>
      </div>
     </div>
    </div>
   </div>
  </div>


 </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
