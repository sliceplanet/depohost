<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "аренда windows сервера, windows в аренду, аренда программного обеспечения microsoft, аренда лицензий microsoft, аренда microsoft, windows server аренда, аренда сервера windows server");
$APPLICATION->SetPageProperty("title", "Аренда программного обеспечения Microsoft для серверов Windows - ArtexTelecom");
$APPLICATION->SetPageProperty("description", "Заинтересовала аренда windows сервера? ArtexTelecom оказывает услуги аренды windows server и лицензий программного обеспечения Microsoft.");
$APPLICATION->SetTitle("Аренда сервера Windows и ПО Microsoft");
?>

<div id="microsoft-page">

<div class="page-top-desc">
	<div class="page-top-desc__wrapper">
			<div class="ptd-icon">
				<img src="/images/2020/icon-microsoft.png">
			</div>
			<h1>Програмное обеспечение Microsoft</h1>
			<div class="ptd-desc">Все конфигурации протестированы, высокая<br>производительность гарантирована</div>			
			<div class="ptd-content">
<p>Данный вид хостинга подразумевает предоставление клиенту отдельного полноценного физического веб сервера (Dedicated
Server) в дата-центре. Это оптимальное решение для размещения крупных и стремительно развивающихся проектов, для которых
не будет достаточно ресурсов виртуального сервера, а также в тех случаях, если предъявляются серьезные требования к безопасности, бесперебойности и производительности работы серверного оборудования.</p>
			</div>
		</div>	
		
	</div>
	

<div class="page-section section-graybg">
	<div class="container">
		
	
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"microsoft2020",
	array(
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
		"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "2",
		"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_TYPE" => "N",	// Тип кеширования
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
		"DETAIL_BRAND_USE" => "N",
		"DETAIL_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
		"DETAIL_META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
		"DETAIL_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
		"DETAIL_PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем товары в разделе
		"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки товаров в разделе
		"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки товаров в разделе
		"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки товаров в разделе
		"FILTER_VIEW_MODE" => "HORIZONTAL",
		"HIDE_NOT_AVAILABLE" => "N",	// Недоступные товары
		"IBLOCK_ID" => "28",	// Инфоблок
		"IBLOCK_TYPE" => "xmlcatalog",	// Тип инфоблока
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "1",	// Количество элементов, выводимых в одной строке таблицы
		"LINK_ELEMENTS_URL" => "",	// URL на страницу, где будет показан список связанных элементов
		"LINK_IBLOCK_ID" => "",	// ID инфоблока, элементы которого связаны с текущим элементом
		"LINK_IBLOCK_TYPE" => "",	// Тип инфоблока, элементы которого связаны с текущим элементом
		"LINK_PROPERTY_SID" => "",	// Свойство, в котором хранится связь
		"LIST_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства раздела
		"LIST_META_DESCRIPTION" => "UF_SEO_DESCRIPTION",	// Установить описание страницы из свойства раздела
		"LIST_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства раздела
		"LIST_PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"MESS_BTN_ADD_TO_BASKET" => "Заказать",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Товары",	// Название категорий
		"PAGE_ELEMENT_COUNT" => "30",	// Количество элементов на странице
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
		"PRICE_CODE" => array(	// Тип цены
			0 => "Розничная",
		),
		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
		"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
		"PRODUCT_PROPERTIES" => "",	// Характеристики товара, добавляемые в корзину
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTION_COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
		"SECTION_TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
		"SEF_FOLDER" => "/microsoft/",	// Каталог ЧПУ (относительно корня сайта)
		"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_ID#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
		),
		"SET_STATUS_404" => "Y",	// Устанавливать статус 404
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
		"SHOW_TOP_ELEMENTS" => "Y",	// Выводить топ элементов
		"TOP_ELEMENT_COUNT" => "30",	// Количество выводимых элементов
		"TOP_ELEMENT_SORT_FIELD" => "id",	// По какому полю сортируем товары в разделе
		"TOP_ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки товаров в разделе
		"TOP_ELEMENT_SORT_ORDER" => "desc",	// Порядок сортировки товаров в разделе
		"TOP_ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки товаров в разделе
		"TOP_LINE_ELEMENT_COUNT" => "1",	// Количество элементов, выводимых в одной строке таблицы
		"TOP_PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"USE_ALSO_BUY" => "Y",	// Показывать блок "С этим товаром покупают"
		"USE_COMPARE" => "Y",	// Разрешить сравнение товаров
		"USE_ELEMENT_COUNTER" => "Y",	// Использовать счетчик просмотров
		"USE_FILTER" => "N",	// Показывать фильтр
		"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
		"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
		"USE_REVIEW" => "N",	// Разрешить отзывы
		"USE_STORE" => "N",	// Показывать блок "Количество товара на складе"
		"VARIABLE_ALIASES" => array(
			"sections" => "",
			"section" => "",
			"element" => "",
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	)
);?>
		

</div>

</div>


<section class="page-section whyssl-section isp-can">
	
	<div class="page-section__h">Программное обеспечение Microsoft -<br>аренда Windows сервера</div>
	
	<div class="container">
		
		<div class="wug__wrapper">
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft1.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатная поддержка<br>и консультация</div>
					<p>Проконсультируем Вас по любым вопросам связанным с выбором ПО, поможем сделать правильный выбор</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft2.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Бесплатная установка<br>и настройка</div>
					<p>Бесплатно установим и настроим арендованный Windows сервер или любое другое ПО Microsoft</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft3.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Преимущество<br>использования</div>
					<p>Экономия, большой выбор, доступ к новым приложениям в течении часа при наличии заключенного договора</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft4.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Большой выбор</div>
					<p>Для заказа аренды приложений Microsoft доступны почти вся линейка продуктов компании</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft5.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Большие возможности</div>
					<p>Вы можете попробовать арендовать любой продукт Microsoft прежде чем его приобрести</p>
				</div>
			</div>
			<div class="wug__item">
				<div class="wug__item_left">
					<img src="/images/2020/microsoft6.png">
				</div>
				<div class="wug__item_right">
					<div class="wug__item_h">Выгодная экономия</div>
					<p>Аренда Windows сервера выгодно отличается от лицензии приобретенной в розницу, экономьте заказывайте аренду сервера Windows</p>
				</div>
			</div>
		</div>
		
	</div>
	
</section>

	
<section class="page-section anyq-section anyq-black">
	
	<div class="container">
	
		<div class="anyq-block">
		
			<div class="anyq-block__h">Остались вопросы?<br>
				Оставьте номер телефона и мы подробно на них ответим
			</div>
		
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new",
				"new_any-quest-form2020",
				Array(
					"SEF_MODE" => "N",
					"WEB_FORM_ID" => "ANY_QUESTIONS_FOOTER",
					"LIST_URL" => "result_list.php",
					"EDIT_URL" => "result_edit.php",
					"SUCCESS_URL" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CHAIN_ITEM_LINK" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"USE_EXTENDED_ERRORS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"SEF_FOLDER" => "/",
					"VARIABLE_ALIASES" => Array(
					)
				)
			);?>
			
		
		</div>
	
	</div>
	
</section>	


</div>












<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>