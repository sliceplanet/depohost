<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("generate-pdf");



// массив параметров
$params = Array(
		'CONTRACT_NUMBER' => "55555557777777",
		'DATE_DAY' => "2",
		'DATE_MONTH' => "ноября",
		'DATE_YEAR' => "2014",
		'CLIENT_NAME' => "ОАО \"ОАО\"",
		'CLIENT_PERSON' => "Иванов Иван Иванович",
		'CLIENT_GROUND' => "приказа №768",
		'CLIENT_ADDRESS' => "ул. Пушкина, д.4, к.2, офис 29.",
		'CLIENT_POST_ADDRESS' => "ул. Пушкина, д.4, к.2, офис 29.",
		'CLIENT_INN_KPP' => "inn123456789 / kpp1876765",
		'CLIENT_BANK_NAME' => "Банк заказчика",
		'CLIENT_BANK_SETTLEMENT_ACCOUNT' => "set189178682",
		'CLIENT_BANK_CORRESPONDENT_ACCOUNT' => "cor189178682",
		'CLIENT_BANK_BIK' => "bik189178682",
		'PERSONAL_PHONE' => "+7 (189) 178 68 22",
		'CLIENT_FAX' => "+7 (189) 178 68 22",
		'CLIENT_EMAIL' => "client@client.ru",
		'CLIENT_MAIL_ADDRESS' => "ул. Пушкина, д.4, к.2, офис 29.",
);

// генерируем пдф (функция make_contract находится в init.php)
// первый аргумент - массив параметров замены
// второй аргумент - куда сохраняем файл
echo make_contract($params,  '/generate-pdf/contract_');


?>






<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>