<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Выделенный сервер");
$APPLICATION->SetTitle("Выделенный сервер");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/dedic.css');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.prettyCheckboxes.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-ui-1.10.4.custom.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/data-server.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/scripts_bootstrap.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/dedic.js');
?>

<section class="server-opis font-idealist">
    <div class="wrapper clearFix">
        <div class="server-container clearFix">
            <div class="server-tabs">
                <div class="active admin">
                    Администрирование
                </div>
                <div class="free">
                    Бесплатно вы получаете
                </div>
                <div class="sms-email">
                    SMS – Email, Мониторинг
                </div>
                <div class="sas-ssd">
                    Преимущество SAS и SSD
                </div>
            </div>
            <div class="server-content vissible">
                <ul>
                    <li>1Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                    <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                    <li>Регулярный анализ системных журналов;</li>
                    <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                    <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                </ul>
                <div class="server-opis-price clearFix">
                    <div class="server-opis-price-num">1 000</div>
                    <div class="server-opis-price-txt"><span>/</span>рублей
                        <br/>в месяц</div>
                </div>
            </div>
            <div class="server-content">
                <ul>
                    <li>2Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                    <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                    <li>Регулярный анализ системных журналов;</li>
                    <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                    <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                </ul>
                <div class="server-opis-price clearFix">
                    <div class="server-opis-price-num">2 000</div>
                    <div class="server-opis-price-txt"><span>/</span>рублей
                        <br/>в месяц</div>
                </div>
            </div>
            <div class="server-content">
                <ul>
                    <li>3Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                    <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                    <li>Регулярный анализ системных журналов;</li>
                    <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                    <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                </ul>
                <div class="server-opis-price clearFix">
                    <div class="server-opis-price-num">3 000</div>
                    <div class="server-opis-price-txt"><span>/</span>рублей
                        <br/>в месяц</div>
                </div>
            </div>
            <div class="server-content">
                <ul>
                    <li>4Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                    <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                    <li>Регулярный анализ системных журналов;</li>
                    <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                    <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                </ul>
                <div class="server-opis-price clearFix">
                    <div class="server-opis-price-num">4 000</div>
                    <div class="server-opis-price-txt"><span>/</span>рублей
                        <br/>в месяц</div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--.server-opis-->
<div class="wrapper server-container">
    <menu class="type-server" data-spy="affix">
        <ul class="nav">
            <li class="configuration"><i class="icon icon-configuration"></i><a href="#configuration">Конфигурация сервера</a>
            </li>
            <li class="ready-made"><i class="icon icon-ready-made"></i><a href="#ready-made">Готовые решения</a>
            </li>
            <li class="opportunities"><i class="icon icon-opportunities"></i><a href="#opportunities">Возможности</a>
            </li>
        </ul>
    </menu>
    <div class="clearFix"></div>
    <?

    $APPLICATION->IncludeComponent("itin:dedicated.calc", "", array(
        "IBLOCK_TYPE" => "dedicated",
        "IBLOCK_ID" => "44",
            ), false
    );
    ?>
    <section id="ready-made" class="clearfix">
        <div class="head clearfix">
            <h2>Готовые решения</h2>
            <div class="descr-wrapper clearfix">
                <i class="icon-info"></i>
                <div class="descr">
                    Вы также можете выбрать <strong>готовые решения</strong>, которые уже настроены и готовы к работе.
                </div>
            </div>
        </div>
        <!--.head-->
        <div id="filter" class="blue-block clearfix">
            <h3>Параметры подбора</h3>
            <div class="group-checkbox-stylish clearfix">
                <div class="sale active checkbox"></div>
                <div class="budget checkbox"></div>
                <div class="professional active checkbox"></div>
            </div>
            <!--.group-checkbox-stylish-->
            <div class="group-inputs clearfix">
                <div class="slide-block cpu-slide">
                    <div class="caption">Ядра</div>
                    <div class="slide-sm" id="cpu-slide"></div>
                    <div class="capture"><span class="from">2</span>-<span class="to">24</span> шт</div>
                </div>
                <div class="slide-block ram-slide">
                    <div class="caption">Память</div>
                    <div class="slide-sm" id="ram-slide"></div>
                    <div class="capture"><span class="from">4</span>-<span class="to">32</span> Гб</div>
                </div>
                <div class="checkbox-block">
                    <div class="caption">Диски</div>
                    <div class="checkbox-filter checked">
                        <input type="checkbox" value="sata" checked="checked"/>
                        <label>Sata</label>
                    </div>
                    <div class="checkbox-filter checked">
                        <input type="checkbox" value="ssd" checked="checked"/>
                        <label>SSD</label>
                    </div>
                    <div class="checkbox-filter">
                        <input type="checkbox" value="sas"checked="checked"/>
                        <label>SAS</label>
                    </div>
                </div>
                <!--.checkbox-block-->
                <div class="slide-block price-slide">
                    <div class="caption">Цена</div>
                    <div class="slide-sm" id="price-slide"></div>
                    <div class="capture"><span class="from">1200</span>-<span class="to">32000</span> Р</div>
                </div>
                <button class="btn-blue">Подобрать</button>
                <button class="refresh">Сбросить фильтр</button>
            </div>
            <!--.group-inputs-->
        </div>
        <!--#filter-->
        <div id="servers-list">
            <div class="section" id="sale">
                <h2>Распродажа серверов</h2>
                <div class="section-wrapper clearfix">
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                </div>
                <!--.section-wrapper-->
            </div>
            <!--#sale-->
            <div class="section" id="budget">
                <h2>Бюджетные решения</h2>
                <div class="section-wrapper clearfix">
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                </div>
                <!--.section-wrapper-->
            </div>
            <!--#budget-->
            <div class="section" id="professional">
                <h2>Профессиональные сервера</h2>
                <div class="section-wrapper clearfix">
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                    <div class="server-info clearfix">
                        <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                        <div class="main-parameters clearfix">
                            <div class="parameter-block cpu clearfix">
                                <div class="caption">Процессоры</div>
                                <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                            </div>
                            <div class="parameter-block ram clearfix">
                                <div class="caption">RAM</div>
                                <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                            </div>
                            <div class="parameter-block hdd clearfix">
                                <div class="caption">HDD</div>
                                <div class="descr">
                                    2 x SATA 1000 ГБ
                                    <br/>
                                    2 x SSD 480 ГБ
                                </div>
                            </div>
                        </div>
                        <div class="advance-param">
                            <div class="caption">Дополнительные возможности</div>
                            <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                        </div>
                        <div class="order-block small clearfix">
                            <div class="terms-block">
                                <div class="term-block" data-value="1">
                                    <div class="top">
                                        <div class="term">1 месяц</div>
                                        <div class="price">3 000 Р</div>
                                        <div class="discount">экономия 0 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">3 000 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block active" data-value="3">
                                    <div class="top">
                                        <div class="term">3 месяца</div>
                                        <div class="price">8 700 Р</div>
                                        <div class="discount">экономия 300 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 900 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="6">
                                    <div class="top">
                                        <div class="term">6 месяцев</div>
                                        <div class="price">16 800 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                                <div class="term-block" data-value="13">
                                    <div class="top">
                                        <div class="term">13 месяц</div>
                                        <div class="price">32 400 Р</div>
                                        <div class="discount">экономия 1 200 Р</div>
                                    </div>
                                    <div class="bottom">
                                        <div class="price">2 700 Р</div>
                                        за сервер
                                    </div>
                                </div>
                            </div>
                            <div class="buy-block clearfix">
                                <a class="btn-green sm" href="#">Заказать</a>
                                <div class="descr-wrapper">
                                    <div class="descr">3 сервера в наличии</div>
                                </div>
                            </div>
                        </div>
                        <!--.order-block.small-->
                    </div>
                    <!--.server-info-->
                </div>
                <!--.section-wrapper-->
            </div>
            <!--#professional-->
        </div>
        <!--.server-list-->
    </section>
    <!-- END #ready-made -->
    <section id="opportunities" class="clearfix">

                <div class="list-items">
            <div class="item-block" id="unlimit-trafic">
                <div class="name">
                    Неограниченный трафик<br/>
                    на скорости 100 МБит
                </div>
                <div class="descr">Максимально быстрый отклик для популярных и высоконагруженных сайтов. 3 варианта учета трафика.</div>
            </div>
            <!--#unlimit-trafic-->
            <div class="item-block" id="professional-admin">
                <div class="name">
                    Профессиональное<br/>
                    администрирование
                </div>
                <div class="descr">Помощь высококвалифицированных системных администраторов 24 часа 7 дней в неделю!</div>
            </div>

            <div class="item-block" id="cool-panels">
                <div class="name">
                    Панели управления<br/>
                    ISP Manager и DCI Manager
                </div>
                <div class="descr">Удобные панели для управления серверным ПО и непосредственно работой сервера</div>
            </div>

            <div class="item-block" id="dedic-in-5min">
                <div class="name">
                    Выделенный сервер<br/>
                    за 5 минут
                </div>
                <div class="descr">Большинство серверов имеются в наличии и запускаются после оплаты счёта почти моментально!</div>
            </div>
        </div>


    </section>
    <!-- END #opportunities -->
</div>
<div class="up"></div>
<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>