<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Выделенный сервер");
$APPLICATION->SetTitle("Выделенный сервер");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/jquery.prettyCheckboxes.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/dedic.css');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.placeholder.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.cycle.all.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.prettyCheckboxes.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui-1.10.4.custom.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/data-server.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/scripts_bootstrap.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/dedic.js');

?>
        <section class="server-opis font-idealist">
            <div class="wrapper clearFix">
                <div class="server-container clearFix">
                    <div class="server-tabs">
                        <div class="active admin">
                            Администрирование
                        </div>
                        <div class="free">
                            Бесплатно вы получаете
                        </div>
                        <div class="sms-email">
                            SMS – Email, Мониторинг
                        </div>
                        <div class="sas-ssd">
                            Преимущество SAS и SSD
                        </div>
                    </div>
                    <div class="server-content vissible">
                        <ul>
                            <li>1Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                            <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                            <li>Регулярный анализ системных журналов;</li>
                            <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                            <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                        </ul>
                        <div class="server-opis-price clearFix">
                            <div class="server-opis-price-num">1 000</div>
                            <div class="server-opis-price-txt"><span>/</span>рублей
                                <br/>в месяц</div>
                        </div>
                    </div>
                    <div class="server-content">
                        <ul>
                            <li>2Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                            <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                            <li>Регулярный анализ системных журналов;</li>
                            <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                            <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                        </ul>
                        <div class="server-opis-price clearFix">
                            <div class="server-opis-price-num">2 000</div>
                            <div class="server-opis-price-txt"><span>/</span>рублей
                                <br/>в месяц</div>
                        </div>
                    </div>
                    <div class="server-content">
                        <ul>
                            <li>3Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                            <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                            <li>Регулярный анализ системных журналов;</li>
                            <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                            <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                        </ul>
                        <div class="server-opis-price clearFix">
                            <div class="server-opis-price-num">3 000</div>
                            <div class="server-opis-price-txt"><span>/</span>рублей
                                <br/>в месяц</div>
                        </div>
                    </div>
                    <div class="server-content">
                        <ul>
                            <li>4Отслеживание и применение критических обновлений ОС и других компонентов системного ПО сервера;</li>
                            <li>Установка, настройка и системное администрирование ПО, обеспечивающего контроль над аппаратным комплексом сервера и его программным обеспечением;</li>
                            <li>Регулярный анализ системных журналов;</li>
                            <li>Проведение профилактических действий в целях повышения производительности и надежности сервера;</li>
                            <li>Настройка и ведение политики прав доступа к ресурсам сервера и т. д.</li>
                        </ul>
                        <div class="server-opis-price clearFix">
                            <div class="server-opis-price-num">4 000</div>
                            <div class="server-opis-price-txt"><span>/</span>рублей
                                <br/>в месяц</div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
        <!--.server-opis-->
        <div class="wrapper server-container">

            <menu class="type-server" data-spy="affix">
                <ul class="nav">
                    <li class="configuration"><i class="icon icon-configuration"></i><a href="#configuration">Конфигурация сервера</a>
                    </li>
                    <li class="ready-made"><i class="icon icon-ready-made"></i><a href="#ready-made">Готовые решения</a>
                    </li>
                    <li class="opportunities"><i class="icon icon-opportunities"></i><a href="#opportunities">Возможности</a>
                    </li>
                </ul>
                    
            </menu>
            <div class="clearFix"></div>
            <section id="configuration" class="clearfix">
                <div class="left-block">
                    <div class="presentation-block">
                        <div id="server-formfactor" class="type-server-header">
                            Вид сервера Smartmicro 2u
                            <div class="hint-block icon-white">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Основной принцип заключается в разделении физического сервера на несколько самостоятельных виртуальных "машин". VDS хостинг он же VDS сервер имеет собственную конфигурацию, использует свои выделенные ресурсы индивидуально. VDS сервер позволяет использовать возможности физического выделенного сервера с возможностью добавлять и изменять любые файлы в корневых директориях, устанавливать необходимое ПО для работы, настраивать собственное программное обеспечение.
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                        </div>
                        <div class="blue-block clearfix">
                            <div id="server-img" class="server-img type2u">
                                <div id="map-hdd">
                                    <div class="active"></div>
                                    <div class="active"></div>
                                    <div class="active"></div>
                                    <div class="active"></div>
                                    <div class="active"></div>
                                    <div class="active"></div>
                                    <div class="deactive"></div>
                                    <div class="deactive"></div>
                                </div>
                            </div>
                            <h4 class="underline-title">Параметры сервера</h4>
                            <div>
                                <div id="parameters" class="parameters-block">
                                    <div id="param-cpu" class="parameter-block">
                                        <div class="caption">Процессоры</div>
                                        <div class="parameter-value">2 x Intel Xeon Quad Core 5420</div>
                                    </div>
                                    <div id="param-ram" class="parameter-block">
                                        <div class="caption">RAM</div>
                                        <div class="parameter-value">32Гб RAM PC 6400 DDR2</div>
                                    </div>
                                    <div id="param-hdd" class="parameter-block">
                                        <div class="caption">HDD</div>
                                        <div class="parameter-value">2 x Sata 1000 Гб<br />1 x SSD 40 Гб</div>
                                    </div>
                                    <div id="param-raid" class="parameter-block">
                                        <div class="caption">Raid Контроллер</div>
                                        <div class="parameter-value">Intel RS2BL080</div>
                                    </div>
                                    <div id="param-hdd" class="parameter-block">
                                        <div class="caption">HDD</div>
                                        <div class="parameter-value">2 x Sata 1000 Гб<br />1 x SSD 40 Гб</div>
                                    </div>
                                </div>
                            </div>
                                
                        </div>  <!-- END .blue-block-->
                       
                    </div><!-- . presentation-block-->
                    <div id="summary" class="summary-block">
                        <div class="server-parameters">
                            <div class="caption underline-title">
                                Характеристики
                            </div>
                            <div class="descr">Dedicated server - Intel Core 2 Duo E7500 2.93ГГц 32кБ x 2/3МБ, Wolfdale, 1066МГц, Core 2 Duo, EM64T, s775, 4Gb PC6400 DDR2, 2x500Gb SATA II</div>
                        </div>
                        <div class="summ">
                            <div class="caption">
                                Стоимость сервера
                            </div>
                            <div class="price">
                                87&nbsp;600&nbsp;р
                            </div>
                        </div>
                    </div>  <!--.summary-block -->
                </div>
                <div id="calc" class="right-block">
                    <h4>Конфигурация сервера</h4>
                    <div id="calc-cpu" class="calc-block">
                        <div class="h5">
                            Поколение процессора
                            <div class="hint-block">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Содержат несколько процессорных ядер в одном корпусе (на одном или нескольких кристаллах).

                                        Процессоры, предназначенные для работы одной копии операционной системы на нескольких ядрах, представляют собой высокоинтегрированную реализацию мультипроцессорности.

                                        Первым многоядерным микропроцессором стал POWER4 от IBM, появившийся в 2001 году и имевший два ядра.
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                            <!--.hint-block-->
                        </div>
                        <div class="radio-group" id="cpu">
                            <table>
                                <tr>
                                    <td class="circle checked"></td><td class="option checked" data-value="1" data-params="Intel Core 2 Duo E3">Сервер E3</td>
                                    <td class="circle"></td><td class="option" data-value="2" data-params="Intel Core 2 Duo E5">Сервер E5</td>
                                    <td class="circle"></td><td  class="option" data-value="3" data-params="Intel Core 2 Duo 2xE65">Сервер 2xE65</td>
                                </tr>
                            </table>  
                            <table class="half">
                                <tr>
                                    <td class="circle"></td><td class="option" data-value="4" data-params="Intel Core 2 Duo E6">Сервер E6</td>
                                    <td class="circle"></td><td class="option" data-value="5" data-params="Intel Core 2 Duo 2xE67">Сервер 2xE67</td>
                                </tr>
                            </table>                                                     
                        </div>
                        <!--#cpu-->
                        <input type="hidden" name="cpu" value="1" />
                    </div>
                    <!--#calc-cpu-->
                    <div id="calc-freq-cpu" class="calc-block">
                        <div class="h5">
                            Частота процессора
                            <div class="hint-block">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Содержат несколько процессорных ядер в одном корпусе (на одном или нескольких кристаллах).
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                            <!--.hint-block-->
                        </div>
                        <div class="radio-group" id="freq-cpu">
                            <table>
                                <tr>
                                    <td class="circle checked"></td><td class="option checked" data-value="1" data-params="3.7ГГц">E3-1240<div class="descr">3.7 ГГц 4 ядра</div></td>
                                    <td class="circle"></td><td class="option" data-value="2" data-params="3.6ГГц">E3-1230<div class="descr">3.6 ГГц 4 ядра</div></td>
                                    <td class="circle"></td><td  class="option" data-value="3" data-params="4.0ГГц">E3-1290<div class="descr">4.0 ГГц 4 ядра</div></td>
                                </tr>
                            </table>                                                      
                        </div>
                        <!--#cpu-->
                        <input type="hidden" name="freq-cpu" value="1" />
                    </div>
                    <!--#calc-cpu-->
                    <div id="calc-ram" class="calc-block">
                        <div class="h5">
                            Оперативная память
                            <div class="hint-block">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Содержат несколько процессорных ядер в одном корпусе (на одном или нескольких кристаллах).
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                            <!--.hint-block-->
                        </div>
                        <div id="ram-view" class="ram-view">4 Гб</div>
                        <input type="hidden" name="prop[ram]" id="ram" value="4"/>
                        <div class="slider-wrapper clearfix">
                            <script type="text/javascript">
                                $(function() {
                                    $("#sliderRAM").slider({
                                        range: "min",
                                        value: 4,
                                        min: 4,
                                        max: 32,
                                        step: 4,
                                        slide: function(event, ui) {
                                            $('#ram-view').text(ui.value+' Гб');
                                            $('#ram').val(ui.value);                              
                                        }
                                    });                                
                                });
                            </script>
                            <div class="slide" id="sliderRAM">
                                <div class="slide-grath">
                                    <div style="width: 12.5%;">4</div>
                                    <div style="width: 12.5%;">8</div>
                                    <div style="width: 12.5%;">12</div>
                                    <div style="width: 12.5%;">16</div>
                                    <div style="width: 12.5%;">20</div>
                                    <div style="width: 12.5%;">24</div>
                                    <div style="width: 12.5%;">28</div>
                                    <div style="width: 12.5%;">32</div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!--#calc-ram-->
                    <div id="calc-raid" class="calc-block">
                        <div class="h5">
                            RAID Контролер
                            <div class="hint-block">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Пояснение для RAID Контролера
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                            <!--.hint-block-->
                        </div>
                        <div class="radio-group" id="raid">
                            <table>
                                <tr>
                                    <td class="circle checked"></td><td class="option checked" data-value="1" data-params="3.7ГГц">Intel<div class="descr">RS2DB080</div></td>
                                    <td class="circle"></td><td class="option" data-value="2" data-params="3.6ГГц">Intel<div class="descr">RS2DB080</div></td>
                                    <td class="circle"></td><td  class="option" data-value="3" data-params="4.0ГГц">Intel<div class="descr">RS2DB080</div></td>
                                </tr>
                            </table>                                                      
                        </div>
                        <!--#raid-->
                        <input type="hidden" name="raid" value="1" />
                    </div>
                    <!--#calc-raid-->
                    <div id="calc-hdd" class="calc-block">
                        <div class="h5">
                            Жесткие диски
                            <div class="hint-block">
                                <div class="hint-text">
                                    <div class="hint-close"></div>
                                        Пояснение для Жестких дисков
                                </div>
                                <div class="hint-corner"></div>
                            </div>
                            <!--.hint-block-->
                        </div>
                        <div class="select-checkbox-group" id="hdd">
                            <div class="input-block">
                                <input id="hdd_1" name="hdd[1]" type="checkbox" value="1" checked="checked"/>
                                <label for="hdd_1">Диск 1</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="input-block disable">
                                <input id="hdd_2" name="hdd[2]" type="checkbox" value="2"/>
                                <label for="hdd_2">Диск 2</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled" >
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <input id="hdd_3" name="hdd[3]" type="checkbox" value="3" checked="checked" />
                                <label for="hdd_3">Диск 3</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block disable">
                                <input id="hdd_4" name="hdd[4]" type="checkbox" value="4"/>
                                <label for="hdd_4">Диск 4</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block disable">
                                <input id="hdd_5" name="hdd[4]" type="checkbox" value="4"/>
                                <label for="hdd_5">Диск 5</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block disable">
                                <input id="hdd_6" name="hdd[4]" type="checkbox" value="4"/>
                                <label for="hdd_6">Диск 6</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block disable">
                                <input id="hdd_7" name="hdd[4]" type="checkbox" value="4"/>
                                <label for="hdd_7">Диск 7</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block disable">
                                <input id="hdd_8" name="hdd[4]" type="checkbox" value="4"/>
                                <label for="hdd_8">Диск 8</label>
                                <div class="clearfix"></div>
                                <div class="select select-hdd">
                                    <select name="hdd-type" class="small" disabled="disabled">
                                        <option value="1" selected="selected">Sata 10k:300 Гб</option>
                                        <option value="2">SSD:120 Гб</option>
                                        <option value="3">SAS 15k:300 Гб</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--#hdd-->
                    </div>
                    <!--#calc-hdd-->                    
                </div>  
                <!--#calc-->
                <div class="clearfix"></div>
                <div class="order-block">
                    <div class="terms-block">
                        <div class="term-block" id="term_1" data-value="1">
                            <div class="top">
                                <div class="term">1 месяц</div>
                                <div class="price">3 000 Р</div>
                                <div class="discount">экономия 0 Р</div>
                            </div> 
                            <div class="bottom">
                                <div class="price">3 000 Р</div>
                                за сервер
                            </div> 
                        </div>
                        <div class="term-block active" id="term_2" data-value="3">
                            <div class="top">
                                <div class="term">3 месяца</div>
                                <div class="price">8 700 Р</div>
                                <div class="discount">экономия 300 Р</div>
                            </div> 
                            <div class="bottom">
                                <div class="price">2 900 Р</div>
                                за сервер
                            </div> 
                        </div>
                        <div class="term-block" id="term_3" data-value="6">
                            <div class="top">
                                <div class="term">6 месяцев</div>
                                <div class="price">16 800 Р</div>
                                <div class="discount">экономия 1 200 Р</div>
                            </div> 
                            <div class="bottom">
                                <div class="price">2 700 Р</div>
                                за сервер
                            </div> 
                        </div>
                        <div class="term-block" id="term_4" data-value="13">
                            <div class="top">
                                <div class="term">13 месяц</div>
                                <div class="price">32 400 Р</div>
                                <div class="discount">экономия 1 200 Р</div>
                            </div> 
                            <div class="bottom">
                                <div class="price">2 700 Р</div>
                                за сервер
                            </div> 
                        </div>
                    </div>
                    <div class="buy-block">
                        <a class="btn-green" href="#">Заказать</a>
                    </div>
                </div>
                <!--.order-block-->
            </section>
            <!-- END #configuration -->
            <section id="ready-made" class="clearfix">
                <div class="head clearfix">
                    <h2>Готовые решения</h2>
                    <div class="descr-wrapper clearfix">
                        <i class="icon-info"></i>
                        <div class="descr">
                            Вы также можете выбрать <strong>готовые решения</strong>, которые уже настроены и готовы к работе.                            
                        </div>                        
                    </div>
                </div>
                <!--.head--> 
                <div id="filter" class="blue-block clearfix">
                    <h3>Параметры подбора</h3>
                    <div class="group-checkbox-stylish clearfix">
                        <div class="sale active checkbox"></div>
                        <div class="budget checkbox"></div>
                        <div class="professional active checkbox"></div>
                    </div>
                    <!--.group-checkbox-stylish-->
                    <div class="group-inputs clearfix">
                        <div class="slide-block cpu-slide">
                            <div class="caption">Ядра</div>
                            <div class="slide-sm" id="cpu-slide"></div>
                            <div class="capture"><span class="from">2</span>-<span class="to">24</span> шт</div>
                        </div>
                        <div class="slide-block ram-slide">
                            <div class="caption">Память</div>
                            <div class="slide-sm" id="ram-slide"></div>
                            <div class="capture"><span class="from">4</span>-<span class="to">32</span> Гб</div>
                        </div>  
                        <div class="checkbox-block">
                            <div class="caption">Диски</div>
                            <div class="checkbox-filter checked">
                                <input type="checkbox" value="sata" checked="checked"/>
                                <label>Sata</label>
                            </div>
                            <div class="checkbox-filter checked">
                                <input type="checkbox" value="ssd" checked="checked"/>
                                <label>SSD</label>
                            </div>
                            <div class="checkbox-filter">
                                <input type="checkbox" value="sas"checked="checked"/>
                                <label>SAS</label>
                            </div>
                        </div>
                        <!--.checkbox-block-->
                        <div class="slide-block price-slide">
                            <div class="caption">Цена</div>
                            <div class="slide-sm" id="price-slide"></div>
                            <div class="capture"><span class="from">1200</span>-<span class="to">32000</span> Р</div>
                        </div> 
                        <button class="btn-blue">Подобрать</button>
                        <button class="refresh">Сбросить фильтр</button>                        
                    </div>                    
                    <!--.group-inputs-->
                </div>
                <!--#filter-->   
                <div id="servers-list">
                    <div class="section" id="sale">
                        <h2>Распродажа серверов</h2>
                        <div class="section-wrapper clearfix">
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                        </div>
                        <!--.section-wrapper-->
                    </div>
                    <!--#sale-->
                    <div class="section" id="budget">
                        <h2>Бюджетные решения</h2>
                        <div class="section-wrapper clearfix">
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                        </div>
                        <!--.section-wrapper-->
                    </div>
                    <!--#budget-->
                    <div class="section" id="professional">
                        <h2>Профессиональные сервера</h2>
                        <div class="section-wrapper clearfix">
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                            <div class="server-info clearfix">
                                <div class="caption">2XE5 2680 3.6 ГГц (8 ядер)</div>
                                <div class="main-parameters clearfix">
                                    <div class="parameter-block cpu clearfix">
                                        <div class="caption">Процессоры</div>
                                        <div class="descr">2 x INTEL XEON QUAD CORE 5420</div>
                                    </div>
                                    <div class="parameter-block ram clearfix">
                                        <div class="caption">RAM</div>
                                        <div class="descr">32 ГБ RAM PC 6400 DDR2</div>
                                    </div>
                                    <div class="parameter-block hdd clearfix">
                                        <div class="caption">HDD</div>
                                        <div class="descr">
                                            2 x SATA 1000 ГБ
                                            <br/>
                                            2 x SSD 480 ГБ
                                        </div>
                                    </div>
                                </div>
                                <div class="advance-param">
                                    <div class="caption">Дополнительные возможности</div>
                                    <div class="descr">Возможности расширения RAM до 196 ГБ Доп. сетевой порт 1 шт.</div>
                                </div>
                                <div class="order-block small clearfix">
                                    <div class="terms-block">
                                        <div class="term-block" data-value="1">
                                            <div class="top">
                                                <div class="term">1 месяц</div>
                                                <div class="price">3 000 Р</div>
                                                <div class="discount">экономия 0 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">3 000 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block active" data-value="3">
                                            <div class="top">
                                                <div class="term">3 месяца</div>
                                                <div class="price">8 700 Р</div>
                                                <div class="discount">экономия 300 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 900 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="6">
                                            <div class="top">
                                                <div class="term">6 месяцев</div>
                                                <div class="price">16 800 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                        <div class="term-block" data-value="13">
                                            <div class="top">
                                                <div class="term">13 месяц</div>
                                                <div class="price">32 400 Р</div>
                                                <div class="discount">экономия 1 200 Р</div>
                                            </div> 
                                            <div class="bottom">
                                                <div class="price">2 700 Р</div>
                                                за сервер
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="buy-block clearfix">
                                        <a class="btn-green sm" href="#">Заказать</a>
                                        <div class="descr-wrapper">
                                            <div class="descr">3 сервера в наличии</div>
                                        </div>
                                    </div>
                                </div>
                                <!--.order-block.small-->
                            </div>
                            <!--.server-info-->
                        </div>
                        <!--.section-wrapper-->
                    </div>
                    <!--#professional-->
                </div>
                <!--.server-list-->
            </section>
            <!-- END #ready-made -->
            <section id="opportunities" class="clearfix">
                <div class="list-items">
                    <div class="item-block" id="unlimit-trafic">
                        <div class="name">
                            Неограниченный трафик<br/>
                            на скорости 100 МБит
                        </div>
                        <div class="descr">Максимально быстрый отклик для популярных и высоконагруженных сайтов. 3 варианта учета трафика.</div>
                    </div>
                    <!--#unlimit-trafic-->
                    <div class="item-block" id="professional-admin">
                        <div class="name">
                            Профессиональное<br/>
                            администрирование
                        </div>
                        <div class="descr">Помощь высококвалифицированных системных администраторов 24 часа 7 дней в неделю!</div>
                    </div>
                    
                    <div class="item-block" id="cool-panels">
                        <div class="name">
                            Панели управления<br/>
                            ISP Manager и DCI Manager
                        </div>
                        <div class="descr">Удобные панели для управления серверным ПО и непосредственно работой сервера</div>
                    </div>
                    
                    <div class="item-block" id="dedic-in-5min">
                        <div class="name">
                            Выделенный сервер<br/>
                            за 5 минут
                        </div>
                        <div class="descr">Большинство серверов имеются в наличии и запускаются после оплаты счёта почти моментально!</div>
                    </div>
                </div>
            </section>
            <!-- END #opportunities -->
            <section class="center">
                <div class="seo-txt">
                    <p>Основной принцип заключается в разделении физического сервера на несколько самостоятельных виртуальных "машин". VDS хостинг он же VDS сервер имеет собственную конфигурацию, использует свои выделенные ресурсы индивидуально. VDS сервер позволяет использовать возможности физического выделенного сервера с возможностью добавлять и изменять любые файлы в корневых директориях, устанавливать необходимое ПО для работы, настраивать собственное программное обеспечение.</p>
                    <p>Это лучшее решение для активно растущего интернет-бизнеса или бизнес проекта, выделенный сервер позволяет достичь стабильности процессов и не думать
                        <br/>о том, что работа тех или иных приложений или рост количества посетителей может вызвать перегрузку оборудования. Выделенный сервер (Dedicated server) устанавливается на специально выделенной для этого технологической площадке (в data-центре), где поддерживаются оптимальные условия для работы оборудования и практически исключены перепады напряжения и какие-либо проблемы с электропитанием. Строго говоря, наша задача при оказании услуги «аренда выделенного сервера» заключается именно в том, чтобы обеспечить надежную и стабильную работу сервера, на котором хранится Ваша информация. Каждый выделенный сервер находится в отдельном закрытом серверном шкафу и обеспечивает бесперебойную круглосуточную работу Вашего бизнеса.</p>
                    <p>Выделенный сервер или dedicated server - оборудование сконфигурированное под нужды заказчика и предоставляемое ему в аренду. Выделенный сервер или dedicated server - незаменимое решение для крупных интернет-проектов, предъявляющих повышенные требования к производительности оборудования
                        <br/>и безопасности данных.</p>
                </div>
            </section>
            <!--.center-->
        </div> 
<div class="up"></div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>