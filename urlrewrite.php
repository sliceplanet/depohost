<?php
$arUrlRewrite=array (
  10 => 
  array (
    'CONDITION' => '#^/arenda-virtualnogo-vydelennogo-servera-vds/(\\d+)/.*#',
    'RULE' => 'ELEMENT_ID=$1',
    'ID' => 'bitrix:catalog.element',
    'PATH' => '/arenda-virtualnogo-vydelennogo-servera-vds/detail.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#',
    'RULE' => 'alias=$1',
    'ID' => '',
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/arenda-dedicated_server/(\\d+)/.*#',
    'RULE' => 'ELEMENT_ID=$1',
    'ID' => 'bitrix:catalog.element',
    'PATH' => '/arenda-dedicated_server/detail.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/personal/order/getoffer/#',
    'RULE' => '',
    'ID' => 'bitrix:form.result.new',
    'PATH' => '/personal/order/getoffer/index.php',
    'SORT' => '100',
  ),
  3 => 
  array (
    'CONDITION' => '#^/microsoft-exchange/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/microsoft-exchange/index.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/online/(/?)([^/]*)#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  5 => 
  array (
    'CONDITION' => '#^/1c-server/(\\d+)/.*#',
    'RULE' => 'ELEMENT_ID=$1',
    'ID' => 'bitrix:catalog.element',
    'PATH' => '/1c-server/detail.php',
    'SORT' => 100,
  ),
  6 => 
  array (
    'CONDITION' => '#^/stssync/calendar/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/calendar/index.php',
    'SORT' => 100,
  ),
  22 => 
  array (
    'CONDITION' => '#^/ssl-sertifikat/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/ssl-sertifikat/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/personal/lists/#',
    'RULE' => '',
    'ID' => 'bitrix:lists',
    'PATH' => '/personal/lists/index.php',
    'SORT' => '100',
  ),
  8 => 
  array (
    'CONDITION' => '#^/unix-hosting/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/unix-hosting/index.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/hosting-dns/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/hosting-dns/index.php',
    'SORT' => 100,
  ),
  12 => 
  array (
    'CONDITION' => '#^/max/images/#',
    'RULE' => '',
    'ID' => 'bitrix:photo',
    'PATH' => '/local/modules/clients/iblock/install/components/bitrix/news/help/index.php',
    'SORT' => '100',
  ),
  13 => 
  array (
    'CONDITION' => '#^/max/images/#',
    'RULE' => '',
    'ID' => 'bitrix:photo',
    'PATH' => '/local/modules/clients/iblock/install/components/bitrix/photo/help/index.php',
    'SORT' => '100',
  ),
  11 => 
  array (
    'CONDITION' => '#^/ispmanager/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/ispmanager/index.php',
    'SORT' => 100,
  ),
  14 => 
  array (
    'CONDITION' => '#^/microsoft/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/microsoft/index.php',
    'SORT' => 100,
  ),
  15 => 
  array (
    'CONDITION' => '#^/hosting/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/hosting/index.php',
    'SORT' => 100,
  ),
  16 => 
  array (
    'CONDITION' => '#^/domain/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/domain/index.php',
    'SORT' => 100,
  ),
  17 => 
  array (
    'CONDITION' => '#^/cloud/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/cloud/index.php',
    'SORT' => '100',
  ),
  21 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  23 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
  20 => 
  array (
    'CONDITION' => '#^/1c/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/1c/index.php',
    'SORT' => 100,
  ),
);
